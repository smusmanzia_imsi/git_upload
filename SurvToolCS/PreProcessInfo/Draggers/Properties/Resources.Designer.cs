﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.225
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::Microsoft.VisualBasic.HideModuleNameAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SurvCoord.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static System.Drawing.Bitmap bmp1001 {
            get {
                object obj = ResourceManager.GetObject("bmp1001", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap bmp1002 {
            get {
                object obj = ResourceManager.GetObject("bmp1002", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap bmp1003 {
            get {
                object obj = ResourceManager.GetObject("bmp1003", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click point on the Contour.
        /// </summary>
        internal static string str101 {
            get {
                return ResourceManager.GetString("str101", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click Directional Point..
        /// </summary>
        internal static string str102 {
            get {
                return ResourceManager.GetString("str102", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Modify|Chain Polyline.
        /// </summary>
        internal static string str103 {
            get {
                return ResourceManager.GetString("str103", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &amp;Chain Polyline.
        /// </summary>
        internal static string str104 {
            get {
                return ResourceManager.GetString("str104", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chain Polyline.
        /// </summary>
        internal static string str105 {
            get {
                return ResourceManager.GetString("str105", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select New Direction or Finish..
        /// </summary>
        internal static string str106 {
            get {
                return ResourceManager.GetString("str106", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clicked Graphic can&apos;t take part in operation..
        /// </summary>
        internal static string str107 {
            get {
                return ResourceManager.GetString("str107", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clicked Graphic - Polyline with Width can&apos;t take part in operation..
        /// </summary>
        internal static string str108 {
            get {
                return ResourceManager.GetString("str108", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clicked Graphic has thickness - can&apos;t take part in operation..
        /// </summary>
        internal static string str109 {
            get {
                return ResourceManager.GetString("str109", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete Original Objects.
        /// </summary>
        internal static string str111 {
            get {
                return ResourceManager.GetString("str111", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to One Step Back.
        /// </summary>
        internal static string str114 {
            get {
                return ResourceManager.GetString("str114", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Finish.
        /// </summary>
        internal static string str115 {
            get {
                return ResourceManager.GetString("str115", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Finish Chain Polyline.
        /// </summary>
        internal static string str116 {
            get {
                return ResourceManager.GetString("str116", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string str117 {
            get {
                return ResourceManager.GetString("str117", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel Chain Polyline.
        /// </summary>
        internal static string str118 {
            get {
                return ResourceManager.GetString("str118", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clicked Graphic is a group. Explode it and try again..
        /// </summary>
        internal static string str120 {
            get {
                return ResourceManager.GetString("str120", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CIRCLE.
        /// </summary>
        internal static string str121 {
            get {
                return ResourceManager.GetString("str121", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ARC.
        /// </summary>
        internal static string str122 {
            get {
                return ResourceManager.GetString("str122", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GRAPHIC.
        /// </summary>
        internal static string str123 {
            get {
                return ResourceManager.GetString("str123", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3D Polyline.
        /// </summary>
        internal static string str124 {
            get {
                return ResourceManager.GetString("str124", resourceCulture);
            }
        }
    }
}
