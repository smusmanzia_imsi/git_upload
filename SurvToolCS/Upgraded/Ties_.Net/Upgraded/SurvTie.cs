using System;
using System.Drawing;
using UpgradeHelpers.Helpers;

namespace Surveying
{
	public class CoordTie
	: TieServerLib.ITurboCADSmartTieServer
	{


		//Private Declare Function TCWDrawingActive Lib "TCAPI80.dll" () As Long
		//Private Declare Function TCWGraphicAt Lib "TCAPI80.dll" (ByVal d As Long, ByVal Index As Long) As Long
		//Private Declare Function TCWVertexAt Lib "TCAPI80.dll" (ByVal g As Long, ByVal Index As Long) As Long

		//Private Declare Sub prVertexSetSomeFlags Lib "DBAPI80.dll" (ByVal hVer As Long, ByVal FlagOn As Long, ByVal FlagOff As Long)

		//Private Declare Function GraphicGetMatrix Lib "DBAPI80.dll" (ByVal hGr As Long) As Long
		//Private Declare Function GraphicSetMatrix Lib "DBAPI80.dll" (ByVal hGr As Long, ByVal hMat As Long) As Boolean
		//Private Declare Function MatrixEqual Lib "DBAPI80.dll" (ByVal hM1 As Long, ByVal hM2 As Long) As Boolean

		//Useful math constants
		const double Pi = 3.14159265358979d;
		const double Eps = 0.000001d;


		public struct VertexTieData
		{
			public short TypeSeg; // 1 - Line
			// 2 - Arc
			// 3 - ArcSegment
			public double xLocRelPrev; // Previous local coordinate on line segment
			public double fiLocRelPrev; // Previous Angle coordinate on Arc segment

			public int ConVerCount; // count of vertex on base Graphic
			public int ConVertex; // Connected Vertex
			public int NumSegment; // Connected Segment
			public double CoordSum; // sum of Coordinates
			public double NorthAngle; // Angle of North direction Symbol
		}

		private string idVTie = "";

		//UPGRADE_ISSUE: (2068) TieSets object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.TieSets TSets = null;
		//UPGRADE_ISSUE: (2068) TieSet object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.TieSet TSet = null;
		//UPGRADE_ISSUE: (2068) Tie object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.Tie Tie = null;
		//UPGRADE_ISSUE: (2068) Drawing object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.Drawing ActDr = null;
		private VertexTieData tieData = new VertexTieData();

		//UPGRADE_ISSUE: (2068) Vertices object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.Vertices VersSbj = null;
		//UPGRADE_ISSUE: (2068) Vertices object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.Vertices VersSbd = null;

		//UPGRADE_ISSUE: (2068) Graphics object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.Graphics Grs = null;
		//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.Graphic GrSbj = null;
		//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private UpgradeStubs.Graphic GrSbd = null;

		private int hDr = 0;
		private int hGrSurText = 0;
		private int hGrBaseLine = 0;
		private int hMatSurText = 0;
		private int hMatBaseLine = 0;


		private object Clear()
		{

			VersSbj = null;
			VersSbd = null;

			GrSbj = null;
			GrSbd = null;
			Grs = null;
			Tie = null;
			TSet = null;
			TSets = null;
			ActDr = null;

			return null;
		}


		public bool AddTie(object pIDispDwg, int idSbj, int idSbd, int lType)
		{
			//MsgBox ("AddTie")
			bool result = false;
			object varData = null;
			ActDr = (UpgradeStubs.Drawing) pIDispDwg;
			GrSbj = null;
			GrSbd = null;
			GrSbj = ReflectionHelper.Invoke<UpgradeStubs.Graphic>(ReflectionHelper.GetMember(ActDr, "Graphics"), "GraphicFromID", new object[]{idSbj});
			GrSbd = ReflectionHelper.Invoke<UpgradeStubs.Graphic>(ReflectionHelper.GetMember(ActDr, "Graphics"), "GraphicFromID", new object[]{idSbd});

			int ConVerCount = 0;
			double CoordSum = 0;
			int ConVertex = 0;
			double xLocRelPrev = 0;
			double fiLocRelPrev = 0;
			int NumSegment = 0;
			int TypeSeg = 0;
			double NorthAngle = Pi / 2;

			if (ReflectionHelper.GetMember<string>(GrSbj, "Type") == "North.Direction" || ReflectionHelper.GetMember<string>(GrSbd, "Type") == "North.Direction")
			{
				if (ReflectionHelper.GetMember<string>(GrSbj, "Type") == "North.Direction")
				{
					//UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
					UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
					NorthAngle = ReflectionHelper.Invoke<double>(GrSbj, "Properties", new object[]{"NorthAngle"});
				}
				else
				{
					//UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
					UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
					NorthAngle = ReflectionHelper.Invoke<double>(GrSbd, "Properties", new object[]{"NorthAngle"});
				}
				goto FILLTSET;
			}

			//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			UpgradeStubs.Graphic GrText = null;
			//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			UpgradeStubs.Graphic GrCon = null;
			if (ReflectionHelper.GetMember<string>(GrSbj, "Type") == "Surveying.CoordReg")
			{
				GrText = GrSbj;
				GrCon = GrSbd;
			}
			else
			{
				GrText = GrSbd;
				GrCon = GrSbj;
			}

			//Dim ConVerCount As Long, CoordSum#
			CoordSum = 0;
			int i = 0;
			double xi = 0, yi = 0;
			ConVerCount = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(GrCon, "Vertices"), "Count");
			for (i = 0; i <= ConVerCount - 1; i++)
			{
				xi = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i}), "X");
				yi = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i}), "Y");
				CoordSum = CoordSum + xi + yi;
			}

			//Dim ConVertex As Long
			ConVertex = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"ConVertex"});
			//   ConVertex = lType
			//MsgBox ("ConVertex=" & CStr(ConVertex))

			//Dim NumSegment As Long
			NumSegment = 0;
			string GrConType = "";
			GrConType = ReflectionHelper.GetMember<string>(GrCon, "Type");
			//Dim TypeSeg%
			TypeSeg = 1;
			//    If GrConType = TcLoadLangString(104) Then
			//UPGRADE_ISSUE: (2068) Vertex object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			UpgradeStubs.Vertex VerNext = null;
			//UPGRADE_ISSUE: (2068) Vertex object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			UpgradeStubs.Vertex Ver = null;
			//UPGRADE_ISSUE: (2068) Vertices object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			UpgradeStubs.Vertices Vers = null;
			if (GrConType == "ARC")
			{ //# NLS#'
				TypeSeg = 2; // Arc
				NumSegment = 1;
			}
			else
			{
				if (!ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(GrCon, "Vertices", new object[]{ConVertex + 1}), "Bulge"))
				{
					TypeSeg = 1; // Line segment
				}
				else
				{
					TypeSeg = 3; // Arc segment
				}
				//        If GrConType = TcLoadLangString(102) Then
				if (GrConType == "GRAPHIC")
				{ //# NLS#'
					NumSegment = ConVertex;
				}
				else
				{

					Vers = ReflectionHelper.GetMember<UpgradeStubs.Vertices>(GrCon, "Vertices");
					NumSegment = 0;
					i = 0;
					Ver = (UpgradeStubs.Vertex) Vers(i);
					VerNext = (UpgradeStubs.Vertex) Vers(i);
					if (i == ConVertex)
					{
						NumSegment++;
						goto LEND;
					}
					if (!ReflectionHelper.GetMember<bool>(VerNext, "Bulge"))
					{
						i++;
						NumSegment++;
						if (i == ConVerCount - 1)
						{
							goto LEND;
						}
					}
					else
					{
						i += 3;
						NumSegment++;
						if (i == ConVerCount - 1)
						{
							goto LEND;
						}
					}
LEND:;
				}
			}

			double x0 = 0, y0 = 0;
			double xDir = 0, xc = 0, xE = 0, xB = 0, yB = 0, yE = 0, yc = 0, yDir = 0;
			double cosa = 0, L = 0, sina = 0, x0Loc = 0, R = 0;
			//Dim xLocRelPrev#
			xLocRelPrev = 0;
			x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "X");
			y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "Y");
			//MsgBox ("TypeSeg=" & CStr(TypeSeg) & "  ConVertex=" & CStr(ConVertex))
			if (TypeSeg == 1)
			{ // Line segm
				xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "X");
				yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "Y");
				xE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "X");
				yE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "Y");
				L = Math.Sqrt((xE - xB) * (xE - xB) + (yE - yB) * (yE - yB));
				if (L < Eps)
				{
					L = 1;
				}
				sina = (yE - yB) / L;
				cosa = (xE - xB) / L;
				x0Loc = (y0 - yB) * sina + (x0 - xB) * cosa;
				xLocRelPrev = x0Loc;
			}

			double fiE = 0, fiB = 0, fi0 = 0;
			double fiELoc = 0, fiBLoc = 0, fi0Loc = 0;
			//Dim fiLocRelPrev#
			fiLocRelPrev = 0;
			if (TypeSeg == 2)
			{ // Arc
				xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{0}), "X");
				yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{0}), "Y");
				xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{1}), "X");
				yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{1}), "Y");
				xE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{2}), "X");
				yE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{2}), "Y");
				xDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{3}), "X");
				yDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{3}), "Y");
				R = Math.Sqrt((xB - xc) * (xB - xc) + (yB - yc) * (yB - yc));
				if (R < Eps)
				{
					R = 1;
				}
				sina = (yB - yc) / R;
				cosa = (xB - xc) / R;
				fiB = Angle(sina, cosa);
				sina = (yE - yc) / R;
				cosa = (xE - xc) / R;
				fiE = Angle(sina, cosa);

				if (fiB - fiE > Eps)
				{
					fiB -= 2 * Pi;
				}

				sina = (y0 - yc) / R;
				cosa = (x0 - xc) / R;
				fi0 = Angle(sina, cosa);

				fiBLoc = fiB - fiB;
				fiELoc = fiE - fiB;
				fi0Loc = fi0 - fiB;
				fiLocRelPrev = fi0Loc;
			}
			int Rot = 0;
			if (TypeSeg == 3)
			{ // Arc segment
				xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "X");
				yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "Y");
				xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "X");
				yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "Y");
				xE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 3}), "X");
				yE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 3}), "Y");
				xDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 2}), "X");
				yDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 2}), "Y");
				R = Math.Sqrt((xB - xc) * (xB - xc) + (yB - yc) * (yB - yc));
				if (R < Eps)
				{
					R = 1;
				}
				Rot = ArcRotation(xB, yB, xDir, yDir, xc, yc, R);
				sina = (yB - yc) / R;
				cosa = (xB - xc) / R;
				fiB = Angle(sina, cosa);
				sina = (yE - yc) / R;
				cosa = (xE - xc) / R;
				fiE = Angle(sina, cosa);

				if (fiB - fiE > Eps)
				{
					fiB -= 2 * Pi;
				}

				sina = (y0 - yc) / R;
				cosa = (x0 - xc) / R;
				fi0 = Angle(sina, cosa);

				fiBLoc = fiB - fiB;
				fiELoc = fiE - fiB;
				fi0Loc = fi0 - fiB;
				fiLocRelPrev = fi0Loc;

			}

FILLTSET:
			TSets = ReflectionHelper.GetMember<UpgradeStubs.TieSets>(ActDr, "TieSets");
			TSet = ReflectionHelper.Invoke<UpgradeStubs.TieSet>(TSets, "TieSet", new object[]{idSbj, idSbd});
			tieData.ConVerCount = ConVerCount;
			tieData.CoordSum = CoordSum;
			tieData.fiLocRelPrev = fiLocRelPrev;
			tieData.TypeSeg = (short) TypeSeg;
			tieData.xLocRelPrev = xLocRelPrev;
			tieData.ConVertex = ConVertex;
			tieData.NumSegment = NumSegment;
			tieData.NorthAngle = NorthAngle;
			varData = tieData;
			ReflectionHelper.Invoke(TSet, "AddTie", new object[]{idVTie, 1, varData});

			TSet = ReflectionHelper.Invoke<UpgradeStubs.TieSet>(TSets, "TieSet", new object[]{idSbd, idSbj});
			tieData.ConVerCount = ConVerCount;
			tieData.CoordSum = CoordSum;
			tieData.fiLocRelPrev = fiLocRelPrev;
			tieData.TypeSeg = (short) TypeSeg;
			tieData.xLocRelPrev = xLocRelPrev;
			tieData.ConVertex = ConVertex;
			tieData.NumSegment = NumSegment;
			tieData.NorthAngle = NorthAngle;
			varData = tieData;
			ReflectionHelper.Invoke(TSet, "AddTie", new object[]{idVTie, 1, varData});

			GrText = null;
			GrCon = null;
			result = true;
			Clear();

			return result;
		}

		public bool CopyTie(object pIDispDwg, int idSbjSrc, int idSbdSrc, int idSbjTrg, int idSbdTrg)
		{

			ActDr = (UpgradeStubs.Drawing) pIDispDwg;
			Grs = ReflectionHelper.GetMember<UpgradeStubs.Graphics>(ActDr, "Graphics");
			GrSbj = ReflectionHelper.Invoke<UpgradeStubs.Graphic>(Grs, "GraphicFromID", new object[]{idSbjTrg});
			GrSbd = ReflectionHelper.Invoke<UpgradeStubs.Graphic>(Grs, "GraphicFromID", new object[]{idSbdTrg});

			TSets = ReflectionHelper.GetMember<UpgradeStubs.TieSets>(ActDr, "TieSets");
			TSet = ReflectionHelper.Invoke<UpgradeStubs.TieSet>(TSets, "TieSet", new object[]{idSbjSrc, idSbdSrc});
			Tie = (UpgradeStubs.Tie) TSet(0);
			tieData = ReflectionHelper.GetMember<VertexTieData>(Tie, "Data");

			//    TSets.Add idVTie, GrSbj, GrSbd, tieData.idVrtSbj, 0, TSet

			Tie = (UpgradeStubs.Tie) TSet(0);
			ReflectionHelper.LetMember(Tie, "Data", ReflectionHelper.GetPrimitiveValue<object>(tieData));

			Clear();

			return true;
		}

		public bool DeleteTie(object pIDispDwg, int idSbj, int idSbd, object pIDsipGr)
		{
			//    Set ActDr = pIDispDwg
			//    Set GrSbj = Nothing
			//    On Error Resume Next
			//    Set GrSbj = ActDr.Graphics.GraphicFromID(idSbj)
			//    Set GrSbd = Nothing
			//    On Error Resume Next
			//    Set GrSbd = ActDr.Graphics.GraphicFromID(idSbd)
			//    If GrSbj Is Nothing = False And GrSbd Is Nothing = True Then
			//        If GrSbj.Type = "Surveying.CoordReg" Then
			//            If GrSbj.Properties("Selected") = 0 Then
			//                Set TSets = ActDr.TieSets
			//                Set TSet = TSets.TieSet(idSbj, idSbd)
			//                TSet.Flags = imsiTieDirty
			//            End If
			//        End If
			//    End If
			return true;
			//    Clear
		}

		public string Description
		{
			get
			{
				return "CurveText_Tie"; //# NLS#'
			}
		}


		public object GetIDs(object pIDispDwg, int idSbj, int idSbd)
		{
			// It is not need to do something here because this method doesn't use Graphic's DataBase ID
			return null;
		}

		public int GetTypesInfo(ref object Names, ref object Types)
		{
			return 0;
		}

		public bool Initialize(object ThisTieMethod)
		{
			//UPGRADE_ISSUE: (2068) TieMethod object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			UpgradeStubs.TieMethod gxTMethod = (UpgradeStubs.TieMethod) ThisTieMethod;
			idVTie = ReflectionHelper.GetMember<string>(gxTMethod, "Name");
			return true;
		}


		public void SetIDs(object pIDispDwg, int idSbj, int idSbd, ref object pvarIDs)
		{
			// It is not need to do something here because this method doesn't use Graphic's DataBase ID
		}

		public bool TieCanNODEED(object pIDispDwg, int idSbj, int idSbd)
		{
			return true;
		}

		short TieServerLib._ITurboCADSmartTieServer.UpdateTie(object pIDispDwg, int idSbj, int idSbd)
		{
			//MsgBox ("UpdateTie")

			short result = 0;
			ActDr = (UpgradeStubs.Drawing) pIDispDwg;

			TSets = ReflectionHelper.GetMember<UpgradeStubs.TieSets>(ActDr, "TieSets");
			TSet = ReflectionHelper.Invoke<UpgradeStubs.TieSet>(TSets, "TieSet", new object[]{idSbj, idSbd});

			UpdateTie(ref TSet, ref TSets, idSbj, idSbd);


			Clear();

			return result;
		}


		private double Angle(double sinb, double cosb)
		{
			double result = 0;
			if (Math.Abs(cosb) < 0.0001d)
			{
				if (sinb > 0)
				{
					result = Pi / 2;
				}
				else
				{
					result = 3 * Pi / 2;
				}
			}
			else
			{
				if (sinb >= 0 && cosb > 0)
				{
					result = Math.Atan(sinb / cosb);
				}
				if (sinb >= 0 && cosb < 0)
				{
					result = Pi + Math.Atan(sinb / cosb);
				}
				if (sinb < 0 && cosb < 0)
				{
					result = Pi + Math.Atan(sinb / cosb);
				}
				if (sinb < 0 && cosb > 0)
				{
					result = 2 * Pi + Math.Atan(sinb / cosb);
				}
			}
			return result;
		}

		// Define the direction of Arc rotation
		// return ArcRotation = 1 if CounterClockwise
		// return ArcRotation = -1 if Clockwise

		private int ArcRotation(double xB, double yB, double xDir, double yDir, double xc, double yc, double R)
		{
			double sina = (yB - yc) / R; //, Alp0#, Alp1#
			double cosa = (xB - xc) / R; //, Alp0#, Alp1#
			double xLoc = (yDir - yc) * sina + (xDir - xc) * cosa;
			double yLoc = (yDir - yc) * cosa - (xDir - xc) * sina;
			if (yLoc > 0)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}






		private object UpdateTie(ref UpgradeStubs.TieSet TSet, ref UpgradeStubs.TieSets TSets, int idSbj, int idSbd)
		{
			object imsiModelSpace = null;
			//MsgBox ("UpdateTie")
			object varData = null;
			ActDr = ReflectionHelper.GetMember<UpgradeStubs.Drawing>(TSets, "Drawing");
			GrSbj = null;
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				GrSbj = ReflectionHelper.Invoke<UpgradeStubs.Graphic>(ReflectionHelper.GetMember(ActDr, "Graphics"), "GraphicFromID", new object[]{idSbj});
				if (GrSbj == null)
				{
					return null;
				}

				GrSbd = null;
				GrSbd = ReflectionHelper.Invoke<UpgradeStubs.Graphic>(ReflectionHelper.GetMember(ActDr, "Graphics"), "GraphicFromID", new object[]{idSbd});
				if (GrSbd == null)
				{
					return null;
				}
				//MsgBox ("1111UpdateTie")

				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.Graphic GrText = null;
				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.Graphic GrCon = null;


				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.Graphic GrNorth = null;
				double NorthAngleNew = 0, NorthAngleOld = 0;
				int ChildCount = 0;
				//UPGRADE_ISSUE: (2068) TieSet object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.TieSet TSet1 = null;
				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.Graphic GrCh = null;
				if (ReflectionHelper.GetMember<string>(GrSbj, "Type") == "North.Direction")
				{
					NorthAngleNew = ReflectionHelper.Invoke<double>(GrSbj, "Properties", new object[]{"NorthAngle"});
					Tie = (UpgradeStubs.Tie) TSet(0);
					varData = ReflectionHelper.GetMember(Tie, "Data");
					tieData = ReflectionHelper.GetPrimitiveValue<VertexTieData>(((Array) varData).GetValue(0));
					NorthAngleOld = tieData.NorthAngle;
					if (Math.Abs(NorthAngleNew - NorthAngleOld) > 0.001d)
					{
						if (ReflectionHelper.GetMember<string>(GrSbd, "Type") == "Surveying.CoordReg")
						{
							ChildCount = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(GrSbd, "Graphics"), "Count");
							for (int i = 1; i <= ChildCount - 1; i++)
							{
								GrCh = ReflectionHelper.Invoke<UpgradeStubs.Graphic>(ReflectionHelper.GetMember(GrSbd, "Graphics"), "Item", new object[]{i});
								if (ReflectionHelper.GetMember<string>(GrCh, "Type") == "TEXT")
								{
									ReflectionHelper.LetMember(GrCh, "Visible", false);
									ReflectionHelper.Invoke(GrCh, "Draw", new object[]{});
								}
							}
							ReflectionHelper.Invoke(GrSbd, "Update", new object[]{});
						}
						TSet1 = ReflectionHelper.Invoke<UpgradeStubs.TieSet>(TSets, "TieSet", new object[]{idSbj, idSbd});
						tieData.NorthAngle = NorthAngleNew;
						varData = tieData;
						ReflectionHelper.Invoke(TSet1, "AddTie", new object[]{idVTie, 1, varData});

						TSet1 = ReflectionHelper.Invoke<UpgradeStubs.TieSet>(TSets, "TieSet", new object[]{idSbd, idSbj});
						tieData.NorthAngle = NorthAngleNew;
						varData = tieData;
						ReflectionHelper.Invoke(TSet1, "AddTie", new object[]{idVTie, 1, varData});
						TSet1 = null;
					}
					return null;
				}




				string EditGr = "";
				if (ReflectionHelper.GetMember<string>(GrSbj, "Type") == "Surveying.CoordReg")
				{
					GrText = GrSbj;
					GrCon = GrSbd;
					EditGr = "Text"; //# NLS#'
				}
				else
				{
					GrText = GrSbd;
					GrCon = GrSbj;
					EditGr = "Base"; //# NLS#'
				}


				Tie = (UpgradeStubs.Tie) TSet(0);
				varData = ReflectionHelper.GetMember(Tie, "Data");
				tieData = ReflectionHelper.GetPrimitiveValue<VertexTieData>(((Array) varData).GetValue(0));

				int ConVerCount = 0;
				double fiLocRelPrev = 0, CoordSum = 0, xLocRelPrev = 0;
				int TypeSeg = 0;
				int ConVertex = 0, NumSegment = 0;
				ConVerCount = tieData.ConVerCount;
				CoordSum = tieData.CoordSum;
				fiLocRelPrev = tieData.fiLocRelPrev;
				TypeSeg = tieData.TypeSeg;
				xLocRelPrev = tieData.xLocRelPrev;
				ConVertex = tieData.ConVertex;
				NumSegment = tieData.NumSegment;

				//#################################################################
				//#################################################################
				//#################################################################
				// if was edited - TEXT
				ConVertex = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"ConVertex"});
				//UPGRADE_ISSUE: (2068) Vertex object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.Vertex Ver = null;
				//UPGRADE_ISSUE: (2068) Vertex object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.Vertex VerNext = null;
				double x0 = 0, y0 = 0;
				double xMid = 0, xDir = 0, xc = 0, xE = 0, xB = 0, yB = 0, yE = 0, yc = 0, yDir = 0, yMid = 0;
				double y0Loc = 0, cosa = 0, L = 0, sina = 0, x0Loc = 0, R = 0;
				//UPGRADE_ISSUE: (2068) ImsiSpaceModeType object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.ImsiSpaceModeType Space = null;
				//UPGRADE_WARNING: (1068) Space of type ImsiSpaceModeType is being forced to Scalar. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
				ReflectionHelper.SetPrimitiveValue(Space, ReflectionHelper.Invoke(ReflectionHelper.GetMember(ReflectionHelper.GetMember(TSets, "Application"), "ActiveDrawing"), "Properties", new object[]{"TileMode"}));
				double fiE = 0, fiB = 0, fi0 = 0;
				double fiELoc = 0, fiBLoc = 0, fi0Loc = 0;
				int Rot = 0; // Arc
				if (EditGr == "Text")
				{ //# NLS#'
					if (Space == imsiModelSpace)
					{
						ReflectionHelper.LetMember(GrText, "UCS", ReflectionHelper.GetMember(GrCon, "UCS"));
					}
					Ver = null;
					Ver = ReflectionHelper.Invoke<UpgradeStubs.Vertex>(GrCon, "Vertices", new object[]{ConVertex});
					if (Ver == null)
					{
						return null;
					}

					VerNext = null;
					VerNext = ReflectionHelper.Invoke<UpgradeStubs.Vertex>(GrCon, "Vertices", new object[]{ConVertex + 1});
					if (VerNext == null)
					{
						return null;
					}



					//        If GrCon.Type = TcLoadLangString(104) Then
					if (ReflectionHelper.GetMember<string>(GrCon, "Type") == "ARC")
					{
						TypeSeg = 2;
					}
					else
					{
						if (ReflectionHelper.GetMember<bool>(VerNext, "Bulge"))
						{
							TypeSeg = 3;
						}
						else
						{
							TypeSeg = 1;
						}
					}

					x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "X");
					y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "Y");
					// ***********************************************************

					if (TypeSeg == 1)
					{
						xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "X");
						yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "Y");
						xE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "X");
						yE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "Y");
						L = Math.Sqrt((xE - xB) * (xE - xB) + (yE - yB) * (yE - yB));
						if (L < Eps)
						{
							sina = 0;
							cosa = 1;
						}
						else
						{
							sina = (yE - yB) / L;
							cosa = (xE - xB) / L;
						}
						x0Loc = (y0 - yB) * sina + (x0 - xB) * cosa;
						xLocRelPrev = x0Loc;
					}
					// ***********************************************************
					if (TypeSeg == 2)
					{
						xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{0}), "X");
						yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{0}), "Y");
						xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{1}), "X");
						yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{1}), "Y");
						xE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{2}), "X");
						yE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{2}), "Y");
						xDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{3}), "X");
						yDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{3}), "Y");
						R = Math.Sqrt((xB - xc) * (xB - xc) + (yB - yc) * (yB - yc));
						if (R < Eps)
						{
							R = 1;
						}
						sina = (yB - yc) / R;
						cosa = (xB - xc) / R;
						fiB = Angle(sina, cosa);
						sina = (yE - yc) / R;
						cosa = (xE - xc) / R;
						fiE = Angle(sina, cosa);

						if (fiB - fiE > Eps)
						{
							fiB -= 2 * Pi;
						}

						sina = (y0 - yc) / R;
						cosa = (x0 - xc) / R;
						fi0 = Angle(sina, cosa);

						fiBLoc = fiB - fiB;
						fiELoc = fiE - fiB;
						fi0Loc = fi0 - fiB;
						fiLocRelPrev = fi0Loc;
					}
					// ***********************************************************
					if (TypeSeg == 3)
					{ // Arc segment
						xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "X");
						yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "Y");
						xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "X");
						yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "Y");
						xE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 3}), "X");
						yE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 3}), "Y");
						xDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 2}), "X");
						yDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 2}), "Y");
						R = Math.Sqrt((xB - xc) * (xB - xc) + (yB - yc) * (yB - yc));
						if (R < Eps)
						{
							R = 1;
						}
						Rot = ArcRotation(xB, yB, xDir, yDir, xc, yc, R);
						sina = (yB - yc) / R;
						cosa = (xB - xc) / R;
						fiB = Angle(sina, cosa);
						sina = (yE - yc) / R;
						cosa = (xE - xc) / R;
						fiE = Angle(sina, cosa);

						if (fiB - fiE > Eps)
						{
							fiB -= 2 * Pi;
						}

						sina = (y0 - yc) / R;
						cosa = (x0 - xc) / R;
						fi0 = Angle(sina, cosa);

						fiBLoc = fiB - fiB;
						fiELoc = fiE - fiB;
						fi0Loc = fi0 - fiB;
						fiLocRelPrev = fi0Loc;
					}

					// Write New Data into TSet Structures
					TSets = ReflectionHelper.GetMember<UpgradeStubs.TieSets>(ActDr, "TieSets");
					TSet = ReflectionHelper.Invoke<UpgradeStubs.TieSet>(TSets, "TieSet", new object[]{idSbj, idSbd});
					tieData.ConVerCount = ConVerCount;
					tieData.CoordSum = CoordSum;
					tieData.fiLocRelPrev = fiLocRelPrev;
					tieData.TypeSeg = (short) TypeSeg;
					tieData.xLocRelPrev = xLocRelPrev;
					tieData.ConVertex = ConVertex;
					tieData.NumSegment = NumSegment;
					varData = tieData;
					ReflectionHelper.Invoke(TSet, "AddTie", new object[]{idVTie, 1, varData});

					TSet = ReflectionHelper.Invoke<UpgradeStubs.TieSet>(TSets, "TieSet", new object[]{idSbd, idSbj});
					tieData.ConVerCount = ConVerCount;
					tieData.CoordSum = CoordSum;
					tieData.fiLocRelPrev = fiLocRelPrev;
					tieData.TypeSeg = (short) TypeSeg;
					tieData.xLocRelPrev = xLocRelPrev;
					tieData.ConVertex = ConVertex;
					tieData.NumSegment = NumSegment;
					varData = tieData;
					ReflectionHelper.Invoke(TSet, "AddTie", new object[]{idVTie, 1, varData});
				}
				//#################################################################
				//#################################################################
				//#################################################################

				//#################################################################
				//#################################################################
				//#################################################################
				// if was edited - BASE-graphic


				double xmin = 0, xmax = 0, ymax = 0, ymin = 0;
				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				UpgradeStubs.Graphic GrRect = null;
				if (EditGr == "Base")
				{ //# NLS#'

					if (Space == imsiModelSpace)
					{
						ReflectionHelper.LetMember(GrText, "UCS", ReflectionHelper.GetMember(GrCon, "UCS"));
					}
					Ver = null;
					Ver = ReflectionHelper.Invoke<UpgradeStubs.Vertex>(GrCon, "Vertices", new object[]{ConVertex});
					if (Ver == null)
					{
						return null;
					}
					VerNext = null;
					VerNext = ReflectionHelper.Invoke<UpgradeStubs.Vertex>(GrCon, "Vertices", new object[]{ConVertex + 1});
					if (VerNext == null)
					{
						return null;
					}
					xmax = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "X");
					ymax = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "Y");
					xmin = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "X");
					ymin = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "Y");
					GrRect = ReflectionHelper.Invoke<UpgradeStubs.Graphic>(ReflectionHelper.GetMember(ActDr, "Graphics"), "Add", new object[]{11});
					ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrRect, "Vertices"), "Add", new object[]{xmin, ymin, 0});
					ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrRect, "Vertices"), "Add", new object[]{xmax, ymin, 0});
					ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrRect, "Vertices"), "Add", new object[]{xmax, ymax, 0});
					ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrRect, "Vertices"), "Add", new object[]{xmin, ymax, 0});
					ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrRect, "Vertices"), "AddClose", new object[]{});
					ReflectionHelper.LetMember(GrRect, "Properties", Color.FromArgb(255, 255, 255), "PenColor");
					ReflectionHelper.Invoke(GrRect, "Draw", new object[]{});


					//        If GrCon.Type = TcLoadLangString(104) Then
					if (ReflectionHelper.GetMember<string>(GrCon, "Type") == "ARC")
					{
						TypeSeg = 2;
					}
					else
					{
						if (ReflectionHelper.GetMember<bool>(VerNext, "Bulge"))
						{
							TypeSeg = 3;
						}
						else
						{
							TypeSeg = 1;
						}
					}

					x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "X");
					y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "Y");

					// ***********************************************************
					if (TypeSeg == 1)
					{
						xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "X");
						yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "Y");
						xE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "X");
						yE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "Y");
						L = Math.Sqrt((xE - xB) * (xE - xB) + (yE - yB) * (yE - yB));
						if (L < Eps)
						{
							sina = 0;
							cosa = 1;
						}
						else
						{
							sina = (yE - yB) / L;
							cosa = (xE - xB) / L;
						}
						x0Loc = xLocRelPrev;
						y0Loc = 0;
						x0 = xB + x0Loc * cosa - y0Loc * sina;
						y0 = yB + x0Loc * sina + y0Loc * cosa;
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "X", x0);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "Y", y0);
						ReflectionHelper.Invoke(GrText, "Update", new object[]{});
						ReflectionHelper.Invoke(GrText, "Draw", new object[]{});
					}
					// *******************************************************
					if (TypeSeg == 2)
					{ // Arc
						xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{0}), "X");
						yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{0}), "Y");
						xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{1}), "X");
						yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{1}), "Y");
						R = Math.Sqrt((xB - xc) * (xB - xc) + (yB - yc) * (yB - yc));
						if (R < Eps)
						{
							R = 1;
						}
						sina = (yB - yc) / R;
						cosa = (xB - xc) / R;
						fiB = Angle(sina, cosa);

						if (fiB - fiE > Eps)
						{
							fiB -= 2 * Pi;
						}

						fi0 = fiB + fiLocRelPrev;

						x0 = xc + R * Math.Cos(fi0);
						y0 = yc + R * Math.Sin(fi0);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "X", x0);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "Y", y0);
						ReflectionHelper.Invoke(GrText, "Update", new object[]{});
						ReflectionHelper.Invoke(GrText, "Draw", new object[]{});
					}
					// ***********************************************************
					if (TypeSeg == 3)
					{ // Arc segment
						xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "X");
						yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 1}), "Y");
						xB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "X");
						yB = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex}), "Y");
						xE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 3}), "X");
						yE = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 3}), "Y");
						xDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 2}), "X");
						yDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{ConVertex + 2}), "Y");
						R = Math.Sqrt((xB - xc) * (xB - xc) + (yB - yc) * (yB - yc));
						if (R < Eps)
						{
							R = 1;
						}
						Rot = ArcRotation(xB, yB, xDir, yDir, xc, yc, R);
						sina = (yB - yc) / R;
						cosa = (xB - xc) / R;
						fiB = Angle(sina, cosa);

						if (fiB - fiE > Eps)
						{
							fiB -= 2 * Pi;
						}

						fi0 = fiB + fiLocRelPrev;

						x0 = xc + R * Math.Cos(fi0);
						y0 = yc + R * Math.Sin(fi0);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "X", x0);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrText, "Vertices"), "Item", new object[]{0}), "Y", y0);
						ReflectionHelper.Invoke(GrText, "Update", new object[]{});
						ReflectionHelper.Invoke(GrText, "Draw", new object[]{});

					}
					//'''''''        GrRect.Deleted = True
					ReflectionHelper.Invoke(GrRect, "Delete", new object[]{});

				}


				//    GrText.Properties("GrConID") = -1
				//    GrText.Draw
			}
			catch (Exception exc)
			{
				NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}
			return null;
		}
	}
}