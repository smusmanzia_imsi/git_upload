using System.Runtime.InteropServices;

namespace survtoolSupport.PInvoke.SafeNative
{
	public static class langextvb
	{

		public static object LoadLangBSTRString(ref string strModule, int nID, int wLanguage)
		{
			return survtoolSupport.PInvoke.UnsafeNative.langextvb.LoadLangBSTRString(ref strModule, nID, wLanguage);
		}
	}
}