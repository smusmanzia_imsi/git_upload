using System;
using System.Runtime.InteropServices;
using UpgradeHelpers.Helpers;

namespace Surveying
{
	internal static class CommonLocaliz
	{

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("LangExtVB.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static object LoadLangBSTRString([MarshalAs(UnmanagedType.VBByRefStr)] ref string strModule, int nID, int wLanguage);

		internal static string TcLoadLangString(int id)
		{
			string result = "";
			string strMod = Global_Loc.strModule;
			string strResult = "";
			try
			{
				strResult = ReflectionHelper.GetPrimitiveValue<string>(survtoolSupport.PInvoke.SafeNative.langextvb.LoadLangBSTRString(ref strMod, id, 0));
			}
			catch
			{
				//UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
				strResult = App.Resources.Resources.ResourceManager.GetString("str" + id.ToString());
			}
			finally
			{
				result = strResult;
			}
			return result;
		}
	}
}