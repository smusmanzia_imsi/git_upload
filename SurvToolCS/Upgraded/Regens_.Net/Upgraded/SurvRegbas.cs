using System;

namespace Surveying
{
	internal static class Module1
	{

		///******************************************************************/
		///*                                                                */
		///*                      TurboCAD for Windows                      */
		///*                   Copyright (c) 1993 - 2001                    */
		///*             International Microcomputer Software, Inc.         */
		///*                            (IMSI)                              */
		///*                      All rights reserved.                      */
		///*                                                                */
		///******************************************************************/

		public static string DrawingUnit = "";

		public const string frmCaption = "Surveyor Dimension Properties"; //# NLS#'
		// DLL Entry point
		//UPGRADE_WARNING: (1044) Sub Main in a DLL won't get called. More Information: http://www.vbtonet.com/ewis/ewi1044.aspx
		public static void Main()
		{
		}
		//UPGRADE_NOTE: (7013) Constructor is just executed once. Please review if Component contains SingleUse classes because they have a different behaviour. More Information: http://www.vbtonet.com/ewis/ewi7013.aspx
		static Module1()
		{
			Main();
		}

		//This method is used to execute the static constructor, which should only be done once.  No code should go in this method.
		public static void TriggerClassInitialization()
		{
		}
	}
}