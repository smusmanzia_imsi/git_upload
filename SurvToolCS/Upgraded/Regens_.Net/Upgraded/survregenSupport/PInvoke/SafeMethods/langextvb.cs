using System.Runtime.InteropServices;

namespace survregenSupport.PInvoke.SafeNative
{
	public static class langextvb
	{

		public static object LoadLangBSTRString(ref string strModule, int nID, int wLanguage)
		{
			return survregenSupport.PInvoke.UnsafeNative.langextvb.LoadLangBSTRString(ref strModule, nID, wLanguage);
		}
	}
}