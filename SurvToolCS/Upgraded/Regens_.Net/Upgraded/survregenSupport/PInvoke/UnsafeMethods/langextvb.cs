using System.Runtime.InteropServices;

namespace survregenSupport.PInvoke.UnsafeNative
{
	[System.Security.SuppressUnmanagedCodeSecurity]
	public static class langextvb
	{

		[DllImport("LangExtVB.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static object LoadLangBSTRString([MarshalAs(UnmanagedType.VBByRefStr)] ref string strModule, int nID, int wLanguage);
	}
}