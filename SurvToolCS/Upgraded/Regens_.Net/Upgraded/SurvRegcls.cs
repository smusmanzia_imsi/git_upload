using Microsoft.VisualBasic;
using System;
using System.Drawing;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using IMSIGX;
using TCDotNetInterfaces;

namespace Surveying
{
	public class CoordReg : ITurboCADRegen
	{

		///******************************************************************/
		///*                                                                */
		///*                      TurboCAD for Windows                      */
		///*                   Copyright (c) 1993 - 2001                    */
		///*             International Microcomputer Software, Inc.         */
		///*                            (IMSI)                              */
		///*                      All rights reserved.                      */
		///*                                                                */
		///******************************************************************/

		//Private Declare Function TCWDrawingActive Lib "TCAPI90.dll" () As Long
		//Private Declare Function TCWGraphicAt Lib "TCAPI90.dll" (ByVal d As Long, ByVal Index As Long) As Long
		//Private Declare Function TCWVertexAt Lib "TCAPI90.dll" (ByVal g As Long, ByVal Index As Long) As Long


		//DBAPI constants
		const int gkGraphic = 11;
		const int gkArc = 2;
		const int gkText = 6;
		const int gfCosmetic = 128;

		//Useful math constants
		const double Pi = 3.14159265358979d;
		const double Eps = 0.000001d;

		//Real variant types!
		const int typeEmpty = 0;
		const int typeInteger = 2;
		const int typeLong = 3;
		const int typeSingle = 4;
		const int typeDouble = 5;
		const int typeCurrency = 6;
		const int typeDate = 7;
		const int typeString = 8;
		const int typeObject = 9;
		const int typeBoolean = 11;
		const int typeVariant = 12;
		const int typeIntegerEnum = typeInteger + 100;
		const int typeLongEnum = typeLong + 100;
		const int typeStringEnum = typeString + 100;

		//Stock property pages
		const int ppStockPen = 1;
		const int ppStockBrush = 2;
		const int ppStockText = 4;
		const int ppStockInsert = 8;
		const int ppStockViewport = 16;
		const int ppStockAuto = 32;

		//Property Ids
		const int idSurvText = 1;
		const int idGrConID = 2;
		const int idTextGap = 3;
		const int idTextLocation = 4;
		const int idGrConIDOld = 5;
		const int idConVertex = 6;
		const int idHeightText = 7;
		const int idGrConType = 8;
		const int idUnits = 9;


		const int idAppendLenUnit = 10;
		const int idLenUnit = 11;
		const int idLenPrecision = 12;

		const int idPrefix = 13;
		const int idSuffix = 14;

		const int idLinkSurText = 15;

		const int idFontText = 16;

		const int idTextColor = 17;

		const int idDirection = 18;

		const int idTextHorizontal = 19;

		const int idTextStorey = 20;

		const int idAngularSystem = 21;

		const int idLineSystem = 22;

		const int idArcSystem = 23;

		const int idNorthAngle = 24;


		//Property enums

		//Number of properties, pages, wizards
		const int NUM_PROPERTIES = 23;
		const int NUM_PAGES = 1;
		const int NUM_WIZARDS = 0;

		public CoordReg()
		{
			//Initialize class variables
		}

		//Returns the user-visible description of this RegenMethod
		public string Description
		{
			get
			{
				return "SDK_SurvCoord"; //# NLS#'
			}
		}


		//Returns the persistent class id for this RegenMethod's property section
		public string ClassID
		{
			get
			{
				return "{D25185FF-6A20-11d0-A115-00A024158DAF}";
			}
		}


		//Retrieve types and names
		public int GetPropertyInfo(ref string[] Names, ref int[] Types, ref int[] IDs, ref double[] Defaults)
		{
			Names = ArraysHelper.InitializeArray<string>(NUM_PROPERTIES + 1);
			Types = new int[NUM_PROPERTIES + 1];
			IDs = new int[NUM_PROPERTIES + 1];
			Defaults = new double[NUM_PROPERTIES + 1];
			Names[0] = "SurvText"; //# NLS#'
			Types[0] = typeString;
			IDs[0] = idSurvText;
			Defaults[0] = Double.Parse("String"); //# NLS#'

			//'''''''    Names(1) = "GrConID"
			//'''''''    Types(1) = typeLong
			//'''''''    IDs(1) = idGrConID
			//'''''''    Defaults(1) = -1

			Names[1] = "TextGap"; //# NLS#'
			Types[1] = typeDouble;
			IDs[1] = idTextGap;
			Defaults[1] = 0d;

			Names[2] = "TextLocation"; //# NLS#'
			Types[2] = typeInteger;
			IDs[2] = idTextLocation;
			Defaults[2] = 1; // 1 - Above
			// 0 - Middle
			// -1 - Bottom
			//'''''''    Names(4) = "GrConIDOld"
			//'''''''    Types(4) = typeLong
			//'''''''    IDs(4) = idGrConIDOld
			//'''''''    Defaults(4) = -1

			Names[3] = "ConVertex"; //# NLS#'
			Types[3] = typeLong;
			IDs[3] = idConVertex;
			Defaults[3] = -1;


			Names[4] = "HeightText"; //# NLS#'
			Types[4] = typeDouble;
			IDs[4] = idHeightText;
			Defaults[4] = 0.25d;

			Names[5] = "GrConType"; //# NLS#'
			Types[5] = typeString;
			IDs[5] = idGrConType;
			Defaults[5] = Double.Parse("");

			Names[6] = "Units"; // Drawing Units'# NLS#'
			Types[6] = typeString;
			IDs[6] = idUnits;
			Defaults[6] = Double.Parse("");


			Names[7] = "AppendLenUnit"; // add dimension length unit to dimension'# NLS#'
			Types[7] = typeInteger; // 1-add
			IDs[7] = idAppendLenUnit; // 0- absent
			Defaults[7] = 1;

			Names[8] = "LenUnit"; // dimension's length unit'# NLS#'
			Types[8] = typeString;
			IDs[8] = idLenUnit;
			Defaults[8] = Double.Parse("in");

			Names[9] = "LenPrecision"; // dimension's length unit'# NLS#'
			Types[9] = typeInteger;
			IDs[9] = idLenPrecision;
			Defaults[9] = 2;

			Names[10] = "Prefix"; //# NLS#'
			Types[10] = typeString;
			IDs[10] = idPrefix;
			Defaults[10] = Double.Parse("");

			Names[11] = "Suffix"; //# NLS#'
			Types[11] = typeString;
			IDs[11] = idSuffix;
			Defaults[11] = Double.Parse("");

			Names[12] = "LinkSurText"; //# NLS#'
			Types[12] = typeInteger;
			IDs[12] = idLinkSurText;
			Defaults[12] = 1;

			Names[13] = "FontText"; //# NLS#'
			Types[13] = typeString;
			IDs[13] = idFontText;
			Defaults[13] = Double.Parse("Times New Roman");

			Names[14] = "TextColor"; //# NLS#'
			Types[14] = typeLong;
			IDs[14] = idTextColor;
			Defaults[14] = ColorTranslator.ToOle(Color.FromArgb(0, 0, 0));

			Names[15] = "Direction"; //# NLS#'
			Types[15] = typeInteger;
			IDs[15] = idDirection;
			Defaults[15] = 1;

			Names[16] = "TextHorizontal"; //# NLS#'
			Types[16] = typeInteger;
			IDs[16] = idTextHorizontal;
			Defaults[16] = 0;

			Names[17] = "TextStorey"; //# NLS#'
			Types[17] = typeInteger;
			IDs[17] = idTextStorey;
			Defaults[17] = 0;

			Names[18] = "AngularSystem"; //# NLS#'
			Types[18] = typeInteger;
			IDs[18] = idAngularSystem;
			Defaults[18] = 0;

			Names[19] = "LineSystem"; //# NLS#'
			Types[19] = typeInteger;
			IDs[19] = idLineSystem;
			Defaults[19] = 0;

			Names[20] = "ArcSystem"; //# NLS#'
			Types[20] = typeInteger;
			IDs[20] = idArcSystem;
			Defaults[20] = 0;

			Names[21] = "NorthAngle"; //# NLS#'
			Types[21] = typeDouble;
			IDs[21] = idNorthAngle;
			Defaults[21] = Pi / 2;

			Names[22] = "GrConIDOld"; //# NLS#'
			Types[22] = typeLong;
			IDs[22] = idGrConIDOld;
			Defaults[22] = -1;

			return NUM_PROPERTIES;
		}

		//Get the number of property pages supporting this RegenMethod
        public int GetPageInfo(IMSIGX.Graphic Context, out ImsiStockPages StockPages, out string[] Names)
		{
			Names = ArraysHelper.InitializeArray<string>(NUM_PAGES + 1);

			//Need the form
			//'    Load frmSample
			//'    Names(0) = frmSample.Caption
			//'    Unload frmSample
			// load from resources
			Names[0] = App.Resources.Resources.str106;
			StockPages = ImsiStockPages.ppStockBrush | ImsiStockPages.ppStockPen | ImsiStockPages.ppStockText | ImsiStockPages.ppStockAuto;
			return NUM_PAGES;
		}

		public int GetWizardInfo(ref object[] Names)
		{
			Names = new object[NUM_WIZARDS + 1];
			return NUM_WIZARDS;
		}

		//Enumerate the names and values of a specified property
		public int GetEnumNames(int PropID, object Names, object Values)
		{
			return 0;
		}

		public bool PageControls(RegenMethod Context, IMSIGX.Graphic Graphic, int PageNumber, bool SaveProperties)
		{
			//Set up error function
			//UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (Failed)");

			string SurvText = "";
			int Location = 0;
			string TextLocation = "";
			double HeightText = 0;
			CheckState AppendLenUnit = CheckState.Unchecked;
			string LenUnit = "";
			int LenPrecision = 0;
			double TextGap = 0;
			string Prefix = "";
			string Suffix = "";
			int LinkSurText = 0;
			int Col = 0;
			string FontText = "";
			CheckState TextHorizontal = CheckState.Unchecked;
			CheckState TextStorey = CheckState.Unchecked;
			int AngularSystem = 0;
			int LineSystem = 0;
			int ArcSystem = 0;

			int Space = 0;
			Space = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(Graphic, "Drawing"), "Properties", new object[]{"TileMode"}); // Modal or Paper space
			if (Space != 1)
			{
				Module1.DrawingUnit = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(Graphic, "Drawing"), "Properties", new object[]{"PaperLinearUnitName"});
			}
			else
			{
				Module1.DrawingUnit = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(Graphic, "Drawing"), "Properties", new object[]{"LinearUnitName"});
			}
			GXTIESLib.Selection Sel = null;
			int SelCount = 0;
			Sel = ReflectionHelper.GetMember<GXTIESLib.Selection>(ReflectionHelper.GetMember(Graphic, "Drawing"), "Selection");
			SelCount = Sel.Count;
			int SelNum = 0;
			SelNum = 0;
			for (int i = 0; i <= SelCount - 1; i++)
			{
				object tempRefParam = i;
				if (Sel.get_Item(ref tempRefParam).Type == "Surveying.CoordReg")
				{
					i = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam);
					SelNum++;
				}
				else
				{
					i = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam);
				}
			}

			int GrConID = 0;
			//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			IMSIGX.Graphic GrTied = null;
			if (SaveProperties)
			{
				//OK button on property page was clicked
				//Form is still loaded
				//Need On Error statement for the case where you have
				//RRect Turbo Shape and ahother "shape" selected
				//UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
				//When the property page is closed, transfer the numeric

				if (SelNum < 2)
				{

					SurvText = frmSample.DefInstance.SurvText.Text;
					ReflectionHelper.LetMember(Graphic, "Properties", SurvText, "SurvText");

					Prefix = frmSample.DefInstance.Prefix.Text;
					ReflectionHelper.LetMember(Graphic, "Properties", Prefix, "Prefix");

					Suffix = frmSample.DefInstance.Suffix.Text;
					ReflectionHelper.LetMember(Graphic, "Properties", Suffix, "Suffix");

					TextLocation = frmSample.DefInstance.Location_Renamed.Text;
					if (TextLocation == "Above")
					{ //# NLS#'
						Location = 1;
					}
					else
					{
						Location = -1;
					}
					//                If TextLocation = "Bottom" Then Location = -1
					//                If TextLocation = "Middle" Then Location = 0
					ReflectionHelper.LetMember(Graphic, "Properties", Location, "TextLocation");

					TextGap = StringToSize(frmSample.DefInstance.TextGap.Text);
					ReflectionHelper.LetMember(Graphic, "Properties", TextGap, "TextGap");

					HeightText = StringToSize(frmSample.DefInstance.HeightText.Text);
					if (HeightText > 0.00001d)
					{
						ReflectionHelper.LetMember(Graphic, "Properties", HeightText, "HeightText");
					}

					AppendLenUnit = frmSample.DefInstance.AppendLenUnits.CheckState;
					ReflectionHelper.LetMember(Graphic, "Properties", (int) AppendLenUnit, "AppendLenUnit");

					LenUnit = frmSample.DefInstance.LenUnits.Text;
					ReflectionHelper.LetMember(Graphic, "Properties", LenUnit, "LenUnit");

					LenPrecision = Convert.ToInt32(Double.Parse(frmSample.DefInstance.LenPrecision.Text));
					ReflectionHelper.LetMember(Graphic, "Properties", LenPrecision, "LenPrecision");

					LinkSurText = (int) frmSample.DefInstance.LinkSurText.CheckState;
					ReflectionHelper.LetMember(Graphic, "Properties", LinkSurText, "LinkSurText");

					ReflectionHelper.LetMember(Graphic, "Properties", ColorTranslator.ToOle(frmSample.DefInstance.Picture1.BackColor), "TextColor");

					FontText = frmSample.DefInstance.FontText.Text;
					ReflectionHelper.LetMember(Graphic, "Properties", FontText, "FontText");

					TextHorizontal = frmSample.DefInstance.TextHorizontal.CheckState;
					ReflectionHelper.LetMember(Graphic, "Properties", (int) TextHorizontal, "TextHorizontal");

					TextStorey = frmSample.DefInstance.TextStorey.CheckState;
					ReflectionHelper.LetMember(Graphic, "Properties", (int) TextStorey, "TextStorey");

					AngularSystem = frmSample.DefInstance.AngularSystem.SelectedIndex;
					ReflectionHelper.LetMember(Graphic, "Properties", AngularSystem, "AngularSystem");

					LineSystem = frmSample.DefInstance.LineSystem.SelectedIndex;
					ReflectionHelper.LetMember(Graphic, "Properties", LineSystem, "LineSystem");

					ArcSystem = frmSample.DefInstance.ArcSystem.SelectedIndex;
					ReflectionHelper.LetMember(Graphic, "Properties", ArcSystem, "ArcSystem");

				}
				else
				{
					// if MultySelection

					if (frmSample.DefInstance.Prefix.Text != "")
					{
						Prefix = frmSample.DefInstance.Prefix.Text;
						ReflectionHelper.LetMember(Graphic, "Properties", Prefix, "Prefix");
					}

					if (frmSample.DefInstance.Suffix.Text != "")
					{
						Suffix = frmSample.DefInstance.Suffix.Text;
						ReflectionHelper.LetMember(Graphic, "Properties", Suffix, "Suffix");
					}

					if (frmSample.DefInstance.Location_Renamed.Text != "")
					{
						TextLocation = frmSample.DefInstance.Location_Renamed.Text;
						if (TextLocation == "Above")
						{ //# NLS#'
							Location = 1;
						}
						else
						{
							Location = -1;
						}
						ReflectionHelper.LetMember(Graphic, "Properties", Location, "TextLocation");
					}

					if (frmSample.DefInstance.TextGap.Text != "")
					{
						TextGap = StringToSize(frmSample.DefInstance.TextGap.Text);
						ReflectionHelper.LetMember(Graphic, "Properties", TextGap, "TextGap");
					}

					if (frmSample.DefInstance.HeightText.Text != "")
					{
						HeightText = StringToSize(frmSample.DefInstance.HeightText.Text);
						if (HeightText > 0.00001d)
						{
							ReflectionHelper.LetMember(Graphic, "Properties", HeightText, "HeightText");
						}
					}

					if (frmSample.DefInstance.LenPrecision.Text != "")
					{
						LenPrecision = Convert.ToInt32(Double.Parse(frmSample.DefInstance.LenPrecision.Text));
						ReflectionHelper.LetMember(Graphic, "Properties", LenPrecision, "LenPrecision");
					}

					if (frmSample.DefInstance.Picture1.BackColor != Color.FromArgb(255, 255, 255))
					{
						ReflectionHelper.LetMember(Graphic, "Properties", ColorTranslator.ToOle(frmSample.DefInstance.Picture1.BackColor), "TextColor");
					}

					if (frmSample.DefInstance.FontText.Text != "")
					{
						FontText = frmSample.DefInstance.FontText.Text;
						ReflectionHelper.LetMember(Graphic, "Properties", FontText, "FontText");
					}

					if (frmSample.DefInstance.AngularSystem.Text != "")
					{
						AngularSystem = frmSample.DefInstance.AngularSystem.SelectedIndex;
						ReflectionHelper.LetMember(Graphic, "Properties", AngularSystem, "AngularSystem");
					}

					if (frmSample.DefInstance.LineSystem.Text != "")
					{
						LineSystem = frmSample.DefInstance.LineSystem.SelectedIndex;
						ReflectionHelper.LetMember(Graphic, "Properties", LineSystem, "LineSystem");
					}

					if (frmSample.DefInstance.ArcSystem.Text != "")
					{
						ArcSystem = frmSample.DefInstance.ArcSystem.SelectedIndex;
						ReflectionHelper.LetMember(Graphic, "Properties", ArcSystem, "ArcSystem");
					}

				}
				if (frmSample.DefInstance.ChangeDirection.Checked)
				{
					ReflectionHelper.LetMember(Graphic, "Properties", -Convert.ToDouble(Graphic.Properties.get_Item("Direction")), "Direction");
				}
			}
			else
			{
				//Property page is about to be opened
				//Make sure the form is loaded
				frmSample tempLoadForm = frmSample.DefInstance;
				try
				{
					if (SelNum < 2)
					{
						//'''''''                    GrConID = Graphic.Properties("GrConID")
						GrTied = GetProperGraphic(Graphic);
						if (GrTied == null)
						{
							GrConID = -1;
						}
						else
						{
							GrConID = ReflectionHelper.GetMember<int>(GrTied, "id");
						}
						frmSample.DefInstance.LinkSurText.Enabled = GrConID != -1;
					}

					//When the property page is opening, transfer the numeric
					if (SelNum < 2)
					{
						SurvText = ReflectionHelper.Invoke<string>(Graphic, "Properties", new object[]{"SurvText"});
						frmSample.DefInstance.SurvText.Text = SurvText;
						frmSample.DefInstance.Label12.Enabled = true;
						frmSample.DefInstance.SurvText.BackColor = Color.FromArgb(255, 255, 255);

						Prefix = ReflectionHelper.Invoke<string>(Graphic, "Properties", new object[]{"Prefix"});
						frmSample.DefInstance.Prefix.Text = Prefix;

						Suffix = ReflectionHelper.Invoke<string>(Graphic, "Properties", new object[]{"Suffix"});
						frmSample.DefInstance.Suffix.Text = Suffix;

						Location = ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"TextLocation"});
						if (Location == 1)
						{
							frmSample.DefInstance.Location_Renamed.Text = "Above"; //# NLS#'
						}
						if (Location == 0)
						{
							frmSample.DefInstance.Location_Renamed.Text = "Middle"; //# NLS#'
						}
						if (Location == -1)
						{
							frmSample.DefInstance.Location_Renamed.Text = "Bottom"; //# NLS#'
						}

						TextGap = ReflectionHelper.Invoke<double>(Graphic, "Properties", new object[]{"TextGap"});
						//                    .TextGap.Text = Format(TextGap, "###0.00") & DrawingUnit
						frmSample.DefInstance.TextGap.Text = SizeToString(TextGap);

						HeightText = ReflectionHelper.Invoke<double>(Graphic, "Properties", new object[]{"HeightText"});
						//                    .HeightText.Text = Format(HeightText, "###0.00") & DrawingUnit
						frmSample.DefInstance.HeightText.Text = SizeToString(HeightText);

						//UPGRADE_WARNING: (6021) Casting 'Variant' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
						AppendLenUnit = (CheckState) ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"AppendLenUnit"});
						frmSample.DefInstance.AppendLenUnits.CheckState = AppendLenUnit;

						LenUnit = ReflectionHelper.Invoke<string>(Graphic, "Properties", new object[]{"LenUnit"});
						frmSample.DefInstance.LenUnits.Text = LenUnit;

						frmSample.DefInstance.LenUnits.Enabled = AppendLenUnit == CheckState.Checked;

						LenPrecision = ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"LenPrecision"});
						frmSample.DefInstance.LenPrecision.Text = LenPrecision.ToString();
						frmSample.DefInstance.VScroll1.Value = LenPrecision;

						LinkSurText = ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"LinkSurText"});
						frmSample.DefInstance.LinkSurText.Enabled = true;
						//UPGRADE_WARNING: (6021) Casting 'int' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
						frmSample.DefInstance.LinkSurText.CheckState = (CheckState) LinkSurText;
						if (LinkSurText == 1)
						{
							frmSample.DefInstance.LenUnits.Enabled = true;
							frmSample.DefInstance.Label8.Enabled = true;
						}
						else
						{
							frmSample.DefInstance.LenUnits.Enabled = false;
							frmSample.DefInstance.Label8.Enabled = false;
						}

						Col = ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"TextColor"});
						frmSample.DefInstance.Picture1.BackColor = ColorTranslator.FromOle(Col);

						FontText = ReflectionHelper.Invoke<string>(Graphic, "Properties", new object[]{"FontText"});
						frmSample.DefInstance.FontText.Text = FontText;

						//UPGRADE_WARNING: (6021) Casting 'Variant' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
						TextHorizontal = (CheckState) ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"TextHorizontal"});
						frmSample.DefInstance.TextHorizontal.CheckState = TextHorizontal;

						//UPGRADE_WARNING: (6021) Casting 'Variant' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
						TextStorey = (CheckState) ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"TextStorey"});
						frmSample.DefInstance.TextStorey.CheckState = TextStorey;

						AngularSystem = ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"AngularSystem"});
						frmSample.DefInstance.AngularSystem.SelectedIndex = AngularSystem;

						LineSystem = ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"LineSystem"});
						frmSample.DefInstance.LineSystem.SelectedIndex = LineSystem;

						ArcSystem = ReflectionHelper.Invoke<int>(Graphic, "Properties", new object[]{"ArcSystem"});
						frmSample.DefInstance.ArcSystem.SelectedIndex = ArcSystem;

					}
					else
					{
						frmSample.DefInstance.SurvText.Text = "";
						frmSample.DefInstance.SurvText.Enabled = false;
						frmSample.DefInstance.Label12.Enabled = false;
						frmSample.DefInstance.SurvText.BackColor = Color.Gray;

						frmSample.DefInstance.Prefix.Text = "";

						frmSample.DefInstance.Suffix.Text = "";

						frmSample.DefInstance.Location_Renamed.Text = "";

						frmSample.DefInstance.TextGap.Text = "";

						frmSample.DefInstance.HeightText.Text = "";

						frmSample.DefInstance.AppendLenUnits.CheckState = CheckState.Indeterminate;

						frmSample.DefInstance.LenPrecision.Text = "";

						frmSample.DefInstance.LinkSurText.Enabled = false;

						frmSample.DefInstance.Picture1.BackColor = Color.FromArgb(255, 255, 255);

						frmSample.DefInstance.FontText.Text = "";

						frmSample.DefInstance.AngularSystem.Text = "";

						frmSample.DefInstance.LineSystem.Text = "";

						frmSample.DefInstance.ArcSystem.Text = "";

					}
					frmSample.DefInstance.ChangeDirection.Checked = false;
				}
				catch
				{
				}


			}

			return true;

Failed:
			//For debugging purposes, report that an error occurred
			//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			if (Information.Err().Number != 0)
			{
				MessageBox.Show("Error in PageControls: " + Information.Err().Description, System.Windows.Forms.Application.ProductName);
			}

			//Return false if an error occurred
			return false;
		}

		public void PageDone(RegenMethod Context, ref int PageNumber)
		{
			//Done with form
			frmSample.DefInstance.Close();
			//return null;
		}

        public bool PropertyPages(RegenMethod Context, ref int PageNumber)
		{
			object frmBolt3D = null;
			int[] GetLastActivePopup = null;
			int[, ] FindWindow = null;
			int[, ] GetWindowLong = null;
			object[] GetCurrentProcessId = null;
			byte GWL_HWNDPARENT = 0;
			object[] BringWindowToTop = null;
			byte GWL_STYLE = 0;
			object[, , ] SetWindowLong = null;
			int WS_POPUP = 0;
			object[, ] SetParent = null;

			int hwndParent = 0;
			int hwndMain = 0;

			//UPGRADE_ISSUE: (2068) XApplication object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			IMSIGX.Application gxApp = ReflectionHelper.GetMember<IMSIGX.Application>(Context, "Application");

			int CurrentStyle = 0;
			if (ReflectionHelper.Invoke(gxApp, "GetProcessId", new object[]{}) != GetCurrentProcessId)
			{

				hwndMain = FindWindow[Convert.ToInt32(Double.Parse("")), ReflectionHelper.GetMember<int>(gxApp, "Caption")];

				hwndParent = GetLastActivePopup[hwndMain];


				CurrentStyle = GetWindowLong[ReflectionHelper.GetMember<int>(frmBolt3D, "hwnd"), GWL_STYLE];

				if (hwndParent == 0)
				{

					hwndParent = hwndMain;
					object tempAuxVar = BringWindowToTop[hwndParent];

					object tempAuxVar2 = SetWindowLong[frmSample.DefInstance.Handle.ToInt32(), GWL_STYLE, CurrentStyle | WS_POPUP];

					object tempAuxVar3 = SetParent[frmSample.DefInstance.Handle.ToInt32(), hwndMain];

				}
				else
				{

					object tempAuxVar4 = SetWindowLong[frmSample.DefInstance.Handle.ToInt32(), GWL_STYLE, CurrentStyle | WS_POPUP];
					object tempAuxVar5 = SetWindowLong[frmSample.DefInstance.Handle.ToInt32(), GWL_HWNDPARENT, hwndParent];

				}


			}

			frmSample.DefInstance.ShowDialog();
			return !frmSample.DefInstance.DialogCanceled;
		}

        public bool Wizard(IMSIGX.RegenMethod Context, object WizardNumber = null)
		{
			return false;
		}

		//Called when vertex has been moved, or other geometry change
		public object OnGeometryChanged(object Graphic, int GeomID, object paramOld, object paramNew)
		{
			//Do nothing
			//Regen Graphic
			return null;
		}

		//Called when vertex is moved, or other geometry change
		public bool OnGeometryChanging(object Graphic, int GeomID, object paramOld, object paramNew)
		{
			//OK to continue with change
			return true;
		}

		public bool OnNewGraphic(IMSIGX.Graphic grfThis, bool boolCopy)
		{
			bool result = false;
			if (boolCopy)
			{
				//Vertices are already added for us...
				return true;
			}
			//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			IMSIGX.Graphic Gr = grfThis;

			try
			{

				//New Graphic being created
				//X, Y, Z, PenDown, Selectable, Snappable, Editable, Linkable
				ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Add", new object[]{0, 0, 0, false});
				ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Add", new object[]{-1, 0, 0, false}); //, False, False, False, False
				ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Add", new object[]{1, 0, 0, false}); //, False, False, False, False
				ReflectionHelper.LetMember(Gr, "Properties", 3, "LimitVertices");
				return true;
			}
			catch
			{

				//Return false on failure
				result = false;
			}
			return result;
		}

		//Function called whenever a copy of a graphic is being made
		public bool OnCopyGraphic(Graphic grfCopy, Graphic grfSource, Matrix Matrix)
		{
			//Return false on failure
			return true;
		}

		//Notification function called after graphic property is saved
		public void OnPropertyChanged(IMSIGX.Graphic Context, int PropID, ref object ValueOld, ref object ValueNew)
		{
			//Do nothing
			//return null;
		}

		//Notification function called when graphic property is saved
		public bool OnPropertyChanging(IMSIGX.Graphic Context, int PropID, ref object ValueOld, ref object ValueNew)
		{
			//OK to proceed
			return true;
		}

		//Notification function called when graphic property is retrieved
		public void OnPropertyGet(IMSIGX.Graphic Context, int PropID)
		{
			//Do nothing
			//return null;
		}

        //Called when we need to update our object
        public void Regen(IMSIGX.Graphic grfThis)
        {
            //MsgBox ("Regen")
            //Setup error handler
            //UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (Failed)");
            //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            IMSIGX.Graphic Gr = null;
            //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            IMSIGX.Graphic GrCon = null;
            Gr = grfThis;
            //Set up lock (prevent recursion)
            int LockCount = 0;
            LockCount = ReflectionHelper.GetMember<int>(grfThis, "RegenLock");

            //Setup error handler (make sure lock is removed)
            //UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (FailedLock)");
            //###################################################################
            bool NewGraphic = false;
            string UnitName = "", UnitNameOld = "";
            int Space = 0;
            double HText = 0;
            double UnitScale = 0;
            int CosmCount = 0;
            double x4 = 0, x3 = 0, y3 = 0, y4 = 0;
            double hTReal = 0;
            double x2 = 0, x1 = 0, x0 = 0, y0 = 0, y1 = 0, y2 = 0;
            int GrConID = 0;
            string GrConType = "";
            int ConVertex = 0;
            int Res = 0;
            bool TieWasted = false;
            int GrConIDOld = 0;
            string SurvText = "";
            string Prefix = "";
            string Suffix = "";
            int LinkSurText = 0;
            string FontText = "";
            int TextStorey = 0;
            string CommonText = "";
            int StrLen = 0;
            double xcText = 0, LText = 0, ycText = 0;
            double xmid = 0, ymid = 0;
            bool ChildArcExist = false;
            int ChildCount = 0;
            //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            IMSIGX.Graphic GrArc = null;
            //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            IMSIGX.Graphic GrText = null;
            //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            IMSIGX.Graphic GrTied = null;
            //UPGRADE_ISSUE: (2068) Vertex object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            IMSIGX.Vertex VerCon = null;
            //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            IMSIGX.Graphic GrCosmText = null;
            if (LockCount == 0)
            {
                NewGraphic = false;
                Space = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(Gr, "Drawing"), "Properties", new object[] { "TileMode" }); // Modal or Paper space
                if (Space != 1)
                {
                    UnitName = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(Gr, "Drawing"), "Properties", new object[] { "PaperLinearUnitName" });
                }
                else
                {
                    UnitName = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(Gr, "Drawing"), "Properties", new object[] { "LinearUnitName" });
                }
                UnitNameOld = ReflectionHelper.Invoke<string>(Gr, "Properties", new object[] { "Units" });

                HText = ReflectionHelper.Invoke<double>(Gr, "Properties", new object[] { "HeightText" });

                if (UnitNameOld == "")
                {
                    //If New graphic
                    NewGraphic = true;
                    //            UnitScale = UnitToUnit(UnitName, "in")
                    //            Gr.Properties("HeightText") = HText * UnitScale
                    ReflectionHelper.LetMember(Gr, "Properties", UnitName, "LenUnit");
                }
                else
                {
                    NewGraphic = false;
                    if (UnitNameOld != UnitName)
                    {

                        CosmCount = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(Gr, "Graphics"), "Count");
                        for (int i = 0; i <= CosmCount - 1; i++)
                        {
                            if (ReflectionHelper.GetMember<string>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Graphics"), "Item", new object[] { i }), "Type") == "TEXT")
                            {
                                GrCosmText = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "Item", new object[] { i });
                                break;
                            }
                        }
                        x3 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCosmText, "Vertices"), "Item", new object[] { 3 }), "x");
                        y3 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCosmText, "Vertices"), "Item", new object[] { 3 }), "y");
                        x4 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCosmText, "Vertices"), "Item", new object[] { 4 }), "x");
                        y4 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCosmText, "Vertices"), "Item", new object[] { 4 }), "y");
                        hTReal = Math.Sqrt(Math.Pow(x4 - x3, 2) + Math.Pow(y4 - y3, 2));
                        if (Math.Abs(HText - hTReal) > 1000 * Eps)
                        {
                            UnitScale = UnitToUnit(UnitName, UnitNameOld);
                            ReflectionHelper.LetMember(Gr, "Properties", HText * UnitScale, "HeightText");
                        }
                        ReflectionHelper.LetMember(Gr, "Properties", UnitName, "LenUnit");
                        GrCosmText = null;
                    }
                }

                ReflectionHelper.LetMember(Gr, "Properties", UnitName, "Units");
                // Delete all Cosmetic
                ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Graphics"), "Clear", new object[] { gfCosmetic });

                x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 0 }), "x");
                y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 0 }), "y");
                x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 1 }), "x");
                y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 1 }), "y");
                x2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 2 }), "x");
                y2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 2 }), "y");


                //'''''''        GrConID = Gr.Properties("GrConID")
                GrTied = GetProperGraphic(Gr);
                if (GrTied == null)
                {
                    GrConID = -1;
                }
                else
                {
                    GrConID = ReflectionHelper.GetMember<int>(GrTied, "id");
                }

                //MsgBox ("000000 GrConid=" & CStr(GrConID))
                if (GrConID != -1)
                {
                    GrCon = null;
                    //UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                    UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
                    //'''''''            Set GrCon = Gr.Drawing.Graphics.GraphicFromID(GrConID)
                    GrCon = GrTied;
                    GrTied = null;
                    if (GrCon == null)
                    {
                        GrConID = -1;
                    }
                    else
                    {
                        Res = ConnectionExist(Gr, GrConID, ref TieWasted);
                        GrConIDOld = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[] { "GrConIDOld" });

                        if (Res == -1 && GrConIDOld != -1)
                        {
                            ReflectionHelper.LetMember(Gr, "Properties", -1, "GrConIDOld");
                            GrConID = -1;
                        }

                        if (Res == -1)
                        {
                            GrConID = -1;
                        }

                        if (Res != -1 && TieWasted)
                        {
                            GrConID = -1;
                        }
                    }
                    if (GrConID != -1)
                    {
                        ConVertex = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[] { "ConVertex" });
                        VerCon = null;
                        //UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                        UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
                        VerCon = ReflectionHelper.Invoke<IMSIGX.Vertex>(GrCon, "Vertices", new object[] { ConVertex });
                        if (VerCon == null)
                        {
                            GrConID = -1;
                        }
                        else
                        {
                            if (ConVertex != 0)
                            {
                                if (ReflectionHelper.GetMember<bool>(VerCon, "Bulge") && !ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(GrCon, "Vertices", new object[] { ConVertex - 1 }), "Bulge"))
                                {
                                    ConVertex--;
                                }
                                else
                                {
                                    if (ReflectionHelper.GetMember<bool>(VerCon, "Bulge") && ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(GrCon, "Vertices", new object[] { ConVertex - 1 }), "Bulge"))
                                    {
                                        ConVertex -= 2;
                                    }
                                }
                            }
                        }

                        VerCon = null;
                        //UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                        UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
                        VerCon = ReflectionHelper.Invoke<IMSIGX.Vertex>(GrCon, "Vertices", new object[] { ConVertex + 1 });
                        if (VerCon == null)
                        {
                            GrConID = -1;
                        }

                    }
                }

                GrConType = ReflectionHelper.Invoke<string>(Gr, "Properties", new object[] { "GrConType" });
                SurvText = ReflectionHelper.Invoke<string>(Gr, "Properties", new object[] { "SurvText" });
                Prefix = ReflectionHelper.Invoke<string>(Gr, "Properties", new object[] { "Prefix" });
                Suffix = ReflectionHelper.Invoke<string>(Gr, "Properties", new object[] { "Suffix" });
                HText = ReflectionHelper.Invoke<double>(Gr, "Properties", new object[] { "HeightText" });
                LinkSurText = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[] { "LinkSurText" });
                FontText = ReflectionHelper.Invoke<string>(Gr, "Properties", new object[] { "FontText" });
                TextStorey = -1;
                //UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
                TextStorey = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[] { "TextStorey" });

                //####################################################################
                //MsgBox (GrConType & "    " & GrConID & "  " & ConVertex)
                if (GrConID == -1)
                {
                }
                else
                {
                    if (LinkSurText == 1)
                    {
                        //                If GrConType = TcLoadLangString(104) Then
                        if (GrConType == "ARC")
                        { //# NLS#'
                            SurvText = ArcSurveourString(Gr, GrCon);
                            ReflectionHelper.LetMember(Gr, "Properties", SurvText, "SurvText");
                        }
                        //                If GrConType = "TCW50IntProp" Or GrConType = TcLoadLangString(102) Or GrConType = "TCW50Polyline" Then
                        if (GrConType == "TCW50IntProp" || GrConType == "GRAPHIC" || GrConType == "TCW50Polyline")
                        { //# NLS#'
                            SurvText = PolylineSurveourString(Gr, GrCon, ConVertex);
                            ReflectionHelper.LetMember(Gr, "Properties", SurvText, "SurvText");
                        }
                    }
                }
                CommonText = "";
                CommonText = Prefix + SurvText + Suffix;
                SurvText = CommonText;

                //####################################################################
                // Begin Add Symbols from string as Cosmetic
                StrLen = Strings.Len(SurvText);

                LText = 0;
                GrText = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "AddText", new object[] { SurvText, 0, 0, 0, HText });
                ReflectionHelper.LetMember(GrText, "Properties", HText, "TextSize");

                ReflectionHelper.LetMember(GrText, "Properties", "Times New Roman", "TextFont");
                //UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
                ReflectionHelper.LetMember(GrText, "Properties", FontText, "TextFont");

                ReflectionHelper.LetMember(GrText, "Properties", ReflectionHelper.Invoke(Gr, "Properties", new object[] { "TextColor" }), "PenColor");
                ReflectionHelper.LetMember(GrText, "Cosmetic", true);
                LText = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x");
                xcText = (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x")) / 2;
                ycText = (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "y")) / 2;


                //######################################################################
                // begin if new graphic
                if (NewGraphic)
                {
                    ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 1 }), "x", x0 - LText / 2);
                    ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 1 }), "y", y0);
                    ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 1 }), "Editable", false);
                    ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 2 }), "x", x0 + LText / 2);
                    ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 2 }), "y", y0);
                    ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[] { 2 }), "Editable", false);
                    xmid = x0;
                    ymid = y0;
                    ReflectionHelper.Invoke(GrText, "MoveRelative", new object[] { xmid - xcText, ymid - ycText, 0 });
                }
                // end if new graphic
                //######################################################################
                if (GrConID == -1 && !NewGraphic)
                {
                    ChildArcExist = false;
                    ChildCount = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(Gr, "Graphics"), "Count");
                    for (int i = 0; i <= ChildCount - 1; i++)
                    {
                        //                    If .Item(i).Type = TcLoadLangString(104) Then
                        if (ReflectionHelper.GetMember<string>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Graphics"), "Item", new object[] { i }), "Type") == "ARC")
                        {
                            GrArc = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "Item", new object[] { i });
                            ChildArcExist = true;
                            break;
                        }
                    }
                    if (!ChildArcExist)
                    {
                        LineText(Gr, GrText, LText, HText, xcText, ycText);
                    }
                    else
                    {
                        TextAlongCosmeticCircle(Gr, ref GrText, ref GrArc, HText);
                    }
                }
                else
                {
                    GrConType = ReflectionHelper.GetMember<string>(GrCon, "Type");
                    //            If GrConType = TcLoadLangString(104) Then
                    if (GrConType == "ARC")
                    { //# NLS#'
                        ReflectionHelper.LetMember(GrText, "Deleted", true);
                        TextAlongCircle(Gr, GrCon, SurvText, HText, 1);
                    }
                    //            If GrConType = "TCW50IntProp" Or GrConType = TcLoadLangString(102) Or GrConType = "TCW50Polyline" Then
                    if (GrConType == "TCW50IntProp" || GrConType == "GRAPHIC" || GrConType == "TCW50Polyline")
                    { //# NLS#'
                        TextAlongPolyline(Gr, GrCon, GrText, SurvText, HText, ref LText, xcText, ycText, ref ConVertex);
                    }
                }

                if (GrConID != -1)
                {
                    ReflectionHelper.LetMember(Gr, "Properties", GrConID, "GrConIDOld");
                }
                GrText = null;
            }
            //###################################################################


            ReflectionHelper.Invoke(Gr, "RegenUnlock", new object[] { });

        //return null;

        FailedLock:
            //Remove lock
            ReflectionHelper.Invoke(grfThis, "RegenUnlock", new object[] { });

        //        Set Grs = Nothing

			//        If Err.Number <> 0 Then
			//            MsgBox "Regen error: " & Err.Description
			//        End If
			//return null;
		}

		public bool Draw(IMSIGX.Graphic grfThis, IMSIGX.View view, Matrix mat)
		{
			double DrBaseAngle = ReflectionHelper.Invoke<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(grfThis, "Application"), "ActiveDrawing"), "Properties", new object[]{"AngleBase"});
			DrBaseAngle = DrBaseAngle / 180 * Pi + Pi / 2;
			double NorthAngle = ReflectionHelper.Invoke<double>(grfThis, "Properties", new object[]{"NorthAngle"});
			if (Math.Abs(DrBaseAngle - NorthAngle) > Eps)
			{
				ReflectionHelper.LetMember(grfThis, "Properties", DrBaseAngle, "NorthAngle");
				Regen(grfThis);
			}
			//Return True if we did the redraw (no further processing necessary, no children will be drawn).
			//Since this is just a test, we return False to let TurboCAD do the drawing operation.
			return false;
		}

		private double Angle(double sinb, double cosb)
		{

			double result = 0;
			if (Math.Abs(cosb) < 0.0001d)
			{
				if (sinb > 0)
				{
					result = Pi / 2;
				}
				else
				{
					result = 3 * Pi / 2;
				}
			}
			else
			{
				if (sinb >= 0 && cosb > 0)
				{
					result = Math.Atan(sinb / cosb);
				}
				if (sinb >= 0 && cosb < 0)
				{
					result = Pi + Math.Atan(sinb / cosb);
				}
				if (sinb < 0 && cosb < 0)
				{
					result = Pi + Math.Atan(sinb / cosb);
				}
				if (sinb < 0 && cosb > 0)
				{
					result = 2 * Pi + Math.Atan(sinb / cosb);
				}
			}
			return result;
		}
		//UPGRADE_NOTE: (7001) The following declaration (Sign) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private double Sign(double y)
		//{
			//if (Math.Abs(y) < 0.001d)
			//{
				//return 0;
			//}
			//if (y < 0)
			//{
				//return -1d;
			//}
			//if (y > 0)
			//{
				//return 1d;
			//}
			//return 0;
		//}

		private void LineText(IMSIGX.Graphic Gr, IMSIGX.Graphic GrText, double LText, double HText, double xcText, double ycText)
		{
			//MsgBox ("TextAlongLine")


			double x2 = 0, x1 = 0, x0 = 0, y0 = 0, y1 = 0, y2 = 0;
			x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "x");
			y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "y");
			x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x");
			y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y");
			x2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x");
			y2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y");

			// Define if was moved the center vertex of the text
			double sina = 0, cosa = 0;
			double L = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
			if (L > Eps)
			{
				sina = (y2 - y1) / L;
				cosa = (x2 - x1) / L;
			}
			else
			{
				sina = 0;
				cosa = 1;
			}
			double Alpha = Angle(sina, cosa);
			x1 = x0 + LText / 2 * Math.Cos(Alpha + Pi);
			y1 = y0 + LText / 2 * Math.Sin(Alpha + Pi);
			x2 = x0 + LText / 2 * Math.Cos(Alpha);
			y2 = y0 + LText / 2 * Math.Sin(Alpha);

			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x", x1);
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y", y1);
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x", x2);
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y", y2);

			ReflectionHelper.Invoke(GrText, "RotateAxis", new object[]{Alpha, 0, 0, 1, xcText, ycText, 0});
			ReflectionHelper.Invoke(GrText, "MoveRelative", new object[]{x0 - xcText, y0 - ycText, 0});

		}



		// Update text Along Connected Arc

		private void TextAlongCircle(IMSIGX.Graphic Gr, IMSIGX.Graphic GrCirc, string SurvText, double HText, int Dictinct)
		{
			//MsgBox ("TextAlongCircle")
			// Define the Parameters of the text
			int TextStorey = -1;
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				TextStorey = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"TextStorey"});
				if (TextStorey == -1)
				{
					TextStorey = 0;
				}

				int TextHorizontal = -1;
				TextHorizontal = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"TextHorizontal"});
				if (TextHorizontal == -1)
				{
					TextHorizontal = 0;
				}

				if (TextStorey == 1 && TextHorizontal == 0)
				{
					TextHorizontal = 1;
					ReflectionHelper.LetMember(Gr, "Properties", 1, "TextHorizontal");
				}

				int TextLocation = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"TextLocation"});
				double TextGap = ReflectionHelper.Invoke<double>(Gr, "Properties", new object[]{"TextGap"});
				string FontText = ReflectionHelper.Invoke<string>(Gr, "Properties", new object[]{"FontText"});
				int GrConIDOld = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"GrConIDOld"});
				int Col = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"TextColor"});

				double LText = 0;
				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				IMSIGX.Graphic GrText = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "AddText", new object[]{SurvText, 0, 0, 0, HText});
				ReflectionHelper.LetMember(GrText, "Properties", HText, "TextSize");
				ReflectionHelper.LetMember(GrText, "Properties", Col, "PenColor");

				ReflectionHelper.LetMember(GrText, "Properties", "Times New Roman", "TextFont");
				ReflectionHelper.LetMember(GrText, "Properties", FontText, "TextFont");
				double HTextReal = 0;
				LText = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x");
				HTextReal = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "y") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "y");
				//        GrText.Deleted = True
				int StrLen = Strings.Len(SurvText);

				double Offset = 0;
				if (TextLocation == 1)
				{ // If Text outside circle
					Offset = Dictinct * (TextGap + HTextReal / 2);
				}
				if (TextLocation == 0)
				{ // If Text on circle
					Offset = TextGap;
				}
				if (TextLocation == -1)
				{ // If Text inside circle
					Offset = Dictinct * (-TextGap - HTextReal / 2);
				}
				int ChildCount = 0;
				ChildCount = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(Gr, "Graphics"), "Count");
				for (int i = 0; i <= ChildCount - 1; i++)
				{
					//                If .Item(i).Type = TcLoadLangString(104) Then
					if (ReflectionHelper.GetMember<string>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Graphics"), "Item", new object[]{i}), "Type") == "ARC")
					{
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Graphics"), "Item", new object[]{i}), "Deleted", true);
						break;
					}
				}
				// Difine the parameters of circle
				double x3C = 0, x2C = 0, x1C = 0, xc = 0, yc = 0, y1C = 0, y2C = 0, y3C = 0;
				double x0Loc = 0, y0Loc = 0;
				double Alpha = 0;
				xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{0}), "x");
				yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{0}), "y");
				x1C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{1}), "x");
				y1C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{1}), "y");
				x2C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{2}), "x");
				y2C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{2}), "y");
				double R = Math.Sqrt((x1C - xc) * (x1C - xc) + (y1C - yc) * (y1C - yc));
				if (R < Eps)
				{
					R = 1;
				}
				double sina = (y1C - yc) / R;
				double cosa = (x1C - xc) / R;
				double fiBegC = Angle(sina, cosa);
				sina = (y2C - yc) / R;
				cosa = (x2C - xc) / R;
				double fiEndC = Angle(sina, cosa);
				if (fiBegC - fiEndC > Eps)
				{
					fiBegC -= 2 * Pi;
				}
				double LArc = (R + Offset) * (fiEndC - fiBegC);


				double x2 = 0, x1 = 0, x0 = 0, y0 = 0, y1 = 0, y2 = 0;
				x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "x");
				y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "y");
				ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "Editable", true);
				x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x");
				y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y");
				ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "Editable", false);
				x2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x");
				y2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y");
				ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "Editable", false);
				double L = Math.Sqrt((x0 - xc) * (x0 - xc) + (y0 - yc) * (y0 - yc));
				sina = (y0 - yc) / L;
				cosa = (x0 - xc) / L;
				double fiMidT = Angle(sina, cosa);

				L = Math.Sqrt((x1 - xc) * (x1 - xc) + (y1 - yc) * (y1 - yc));
				sina = (y1 - yc) / L;
				cosa = (x1 - xc) / L;
				double fiBegT = Angle(sina, cosa);

				L = Math.Sqrt((x2 - xc) * (x2 - xc) + (y2 - yc) * (y2 - yc));
				sina = (y2 - yc) / L;
				cosa = (x2 - xc) / L;
				double fiEndT = Angle(sina, cosa);

				if (Math.Sin(fiBegT) > 0 && Math.Sin(fiEndT) < 0)
				{
					fiEndT -= 2 * Pi;
				}

				double dfiText = LText / (R + Offset);

				double fiEndCLoc = 0, fiBegCLoc = 0, fiMidTLoc = 0;
				if (LText < LArc)
				{
					fiBegCLoc = fiBegC - fiBegC;
					fiEndCLoc = fiEndC - fiBegC;
					fiMidTLoc = fiMidT - fiBegC;
					if (fiMidTLoc > 2 * Pi)
					{
						fiMidTLoc -= 2 * Pi;
					}
					if (fiMidTLoc > fiEndCLoc)
					{
						fiMidTLoc = fiEndCLoc - dfiText / 2;
					}
					else
					{
						if (fiMidTLoc > fiEndCLoc - dfiText / 2)
						{
							fiMidTLoc = fiEndCLoc - dfiText / 2;
						}
						if (fiMidTLoc < fiBegCLoc + dfiText / 2)
						{
							fiMidTLoc = fiBegCLoc + dfiText / 2;
						}
					}
					fiMidT = fiMidTLoc + fiBegC;
				}
				else
				{
					fiMidT = (fiBegC + fiEndC) / 2;
				}
				//first connection to the Circle
				if (GrConIDOld == -1 || LText > LArc)
				{
					fiMidT = (fiBegC + fiEndC) / 2;
				}
				if (LText > LArc)
				{
					fiMidT = (fiBegC + fiEndC) / 2;
				}

				//?????????????????????????????????????????????????????????????????
				if (GrConIDOld == -1 && TextHorizontal == 1)
				{
					if (Math.Cos(fiMidT) > 0)
					{
						TextGap = (LText / 2 - HText) * Math.Abs(Math.Cos(fiMidT));
					}
					else
					{
						TextGap = (LText / 2 - 1.5d * HText) * Math.Abs(Math.Cos(fiMidT));
					}

					ReflectionHelper.LetMember(Gr, "Properties", TextGap, "TextGap");
					if (TextLocation == 1)
					{ // If Text outside circle
						Offset = Dictinct * (TextGap + HTextReal / 2);
					}
					if (TextLocation == 0)
					{ // If Text on circle
						Offset = TextGap;
					}
					if (TextLocation == -1)
					{ // If Text inside circle
						Offset = Dictinct * (-TextGap - HTextReal / 2);
					}
				}
				//?????????????????????????????????????????????????????????????????

				fiBegT = fiMidT - dfiText / 2;
				fiEndT = fiMidT + dfiText / 2;

				x1 = xc + R * Math.Cos(fiBegT);
				y1 = yc + R * Math.Sin(fiBegT);
				x2 = xc + R * Math.Cos(fiEndT);
				y2 = yc + R * Math.Sin(fiEndT);

				x0 = xc + (R + Offset) * Math.Cos((fiBegT + fiEndT) / 2);
				y0 = yc + (R + Offset) * Math.Sin((fiBegT + fiEndT) / 2);

				x1 = xc + (R + Offset) * Math.Cos(fiBegT);
				y1 = yc + (R + Offset) * Math.Sin(fiBegT);

				x2 = xc + (R + Offset) * Math.Cos(fiEndT);
				y2 = yc + (R + Offset) * Math.Sin(fiEndT);

				ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "x", x0);
				ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "y", y0);
				if (TextHorizontal == 1)
				{
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x", x0 - LText / 2);
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y", y0);
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x", x0 + LText / 2);
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y", y0);
				}
				else
				{
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x", x1);
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y", y1);
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x", x2);
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y", y2);
				}

				double xcText = 0, ycText = 0;
				if (TextHorizontal == 1)
				{
					ReflectionHelper.LetMember(GrText, "Cosmetic", true);
					xcText = (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x")) / 2;
					ycText = (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "y")) / 2;
					ReflectionHelper.Invoke(GrText, "MoveRelative", new object[]{x0 - xcText, y0 - ycText, 0});
					GrText = null;
					return;
				}
				else
				{
					ReflectionHelper.LetMember(GrText, "Deleted", true);
				}

				double dfi = dfiText / (StrLen - 1);

				double ycChar = 0, wChar = 0, hChar = 0, xcChar = 0, wCharBase = 0;
				double yi = 0, ri = 0, fi = 0, xi = 0, fi0 = 0;
				if (Math.Sin(fiMidT) > 0)
				{
					fi = fiEndT;
					fi0 = fi;
				}
				else
				{
					fi = fiBegT;
					fi0 = fi;
				}

				string Char = SurvText.Substring(0, Math.Min(1, SurvText.Length));
				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				IMSIGX.Graphic GrBaseSym = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "AddText", new object[]{"2", 0, 0, 0, HText});

				ReflectionHelper.LetMember(GrBaseSym, "Properties", "Times New Roman", "TextFont");
				ReflectionHelper.LetMember(GrBaseSym, "Properties", FontText, "TextFont");

				ReflectionHelper.LetMember(GrBaseSym, "Properties", Col, "PenColor");
				wCharBase = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrBaseSym, "CalcBoundingBox"), "Max"), "x") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrBaseSym, "CalcBoundingBox"), "Min"), "x");
				ReflectionHelper.LetMember(GrBaseSym, "Cosmetic", true);
				double fii = 0;
				for (int i = 0; i <= StrLen - 1; i++)
				{
					Char = SurvText.Substring(i, Math.Min(1, SurvText.Length - i));
					GrText = ReflectionHelper.GetMember<IMSIGX.Graphic>(GrBaseSym, "Duplicate");
					ReflectionHelper.LetMember(GrText, "Properties", Char, "Info");

					wChar = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x");
					if (wChar < wCharBase)
					{
						wChar = wCharBase;
					}
					xcChar = (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x")) / 2;
					ycChar = (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "y")) / 2;
					dfi = wChar / (R + Offset);
					if (Math.Sin(fiMidT) > 0)
					{
						fi -= dfi;
						fii = fi - dfi / 2;
					}
					else
					{
						fi += dfi;
						fii = fi + dfi / 2;
					}

					xi = xc + (R + Offset) * Math.Cos(fi);
					yi = yc + (R + Offset) * Math.Sin(fi);
					if (Math.Sin(fiMidT) > 0)
					{
						ReflectionHelper.Invoke(GrText, "RotateAxis", new object[]{fi - Pi / 2, 0, 0, 1, xcChar, ycChar, 0});
					}
					else
					{
						ReflectionHelper.Invoke(GrText, "RotateAxis", new object[]{fi + Pi / 2, 0, 0, 1, xcChar, ycChar, 0});
					}
					ReflectionHelper.Invoke(GrText, "MoveRelative", new object[]{xi - xcChar, yi - ycChar, 0});
				}

				double fik = fi;

				x0 = xc + (R + Offset) * Math.Cos(fi0);
				y0 = yc + (R + Offset) * Math.Sin(fi0);
				x1 = xc + (R + Offset) * Math.Cos(fik);
				y1 = yc + (R + Offset) * Math.Sin(fik);
				x2 = xc + (R + Offset) * Math.Cos(fiMidT);
				y2 = yc + (R + Offset) * Math.Sin(fiMidT);
				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				IMSIGX.Graphic GrArc = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "AddArcTriplePoint", new object[]{x0, y0, 0, x2, y2, 0, x1, y1, 0});
				//        GrArc.Editable = False
				//        GrArc.Unbounded = True
				//        GrArc.Cosmetic = True
				ReflectionHelper.LetMember(GrArc, "Visible", false);

				ReflectionHelper.LetMember(GrBaseSym, "Deleted", true);
			}
			catch (Exception exc)
			{
				NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}

		}



		// Update text Along Cosmetic Arc

		private void TextAlongCosmeticCircle(IMSIGX.Graphic Gr, ref IMSIGX.Graphic GrText, ref IMSIGX.Graphic GrCirc, double HText)
		{
			//MsgBox ("TextAlongCosmeticCircle")
			string FontText = ReflectionHelper.Invoke<string>(Gr, "Properties", new object[]{"FontText"});

			// Difine the parameters of circle
			double x2C = 0, x1C = 0, xc = 0, yc = 0, y1C = 0, y2C = 0;
			xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{0}), "x");
			yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{0}), "y");
			x1C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{1}), "x");
			y1C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{1}), "y");
			x2C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{2}), "x");
			y2C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCirc, "Vertices"), "Item", new object[]{2}), "y");
			double R = Math.Sqrt((x1C - xc) * (x1C - xc) + (y1C - yc) * (y1C - yc));
			if (R < Eps)
			{
				R = 1;
			}
			double sina = (y1C - yc) / R;
			double cosa = (x1C - xc) / R;
			double fiBegC = Angle(sina, cosa);
			sina = (y2C - yc) / R;
			cosa = (x2C - xc) / R;
			double fiEndC = Angle(sina, cosa);

			if (fiBegC - fiEndC > Eps)
			{
				fiBegC -= 2 * Pi;
			}
			double LArc = R * (fiEndC - fiBegC);

			// Define the Parameters of the text
			string SurvText = ReflectionHelper.Invoke<string>(GrText, "Properties", new object[]{"Info"});
			double LText = 0;
			LText = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x");
			ReflectionHelper.LetMember(GrText, "Deleted", true);

			int StrLen = Strings.Len(SurvText);

			double x2 = 0, x1 = 0, x0 = 0, y0 = 0, y1 = 0, y2 = 0;
			x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "x");
			y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "y");
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "Editable", true);
			x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x");
			y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y");
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "Editable", false);
			x2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x");
			y2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y");
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "Editable", false);
			double L = Math.Sqrt((x0 - xc) * (x0 - xc) + (y0 - yc) * (y0 - yc));
			sina = (y0 - yc) / L;
			cosa = (x0 - xc) / L;
			double fiMidT = Angle(sina, cosa);

			L = Math.Sqrt((x1 - xc) * (x1 - xc) + (y1 - yc) * (y1 - yc));
			sina = (y1 - yc) / L;
			cosa = (x1 - xc) / L;
			double fiBegT = Angle(sina, cosa);

			L = Math.Sqrt((x2 - xc) * (x2 - xc) + (y2 - yc) * (y2 - yc));
			sina = (y2 - yc) / L;
			cosa = (x2 - xc) / L;
			double fiEndT = Angle(sina, cosa);

			if (Math.Sin(fiBegT) > 0 && Math.Sin(fiEndT) < 0)
			{
				fiEndT -= 2 * Pi;
			}

			double dfiText = LText / R;

			fiMidT = (fiBegC + fiEndC) / 2;


			x1 = xc + R * Math.Cos(fiBegT);
			y1 = yc + R * Math.Sin(fiBegT);
			x2 = xc + R * Math.Cos(fiEndT);
			y2 = yc + R * Math.Sin(fiEndT);

			x0 = xc + R * Math.Cos((fiBegT + fiEndT) / 2);
			y0 = yc + R * Math.Sin((fiBegT + fiEndT) / 2);

			x1 = xc + R * Math.Cos(fiBegT);
			y1 = yc + R * Math.Sin(fiBegT);

			x2 = xc + R * Math.Cos(fiEndT);
			y2 = yc + R * Math.Sin(fiEndT);

			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "x", x0);
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "y", y0);
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x", x1);
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y", y1);
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x", x2);
			ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y", y2);
			double dfi = dfiText / (StrLen - 1);


			double ycChar = 0, wChar = 0, xcChar = 0, wCharBase = 0;
			double yi = 0, fi = 0, xi = 0, fi0 = 0;
			if (Math.Sin(fiMidT) > 0)
			{
				fi = fiEndT;
				fi0 = fi;
			}
			else
			{
				fi = fiBegT;
				fi0 = fi;
			}

			string Char = SurvText.Substring(0, Math.Min(1, SurvText.Length));
			//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			IMSIGX.Graphic GrBaseSym = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "AddText", new object[]{"2", 0, 0, 0, HText});

			ReflectionHelper.LetMember(GrText, "Properties", "Times New Roman", "TextFont");
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				ReflectionHelper.LetMember(GrText, "Properties", FontText, "TextFont");

				wCharBase = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrBaseSym, "CalcBoundingBox"), "Max"), "x") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrBaseSym, "CalcBoundingBox"), "Min"), "x");
				ReflectionHelper.LetMember(GrBaseSym, "Cosmetic", true);
				double fii = 0;
				for (int i = 0; i <= StrLen - 1; i++)
				{
					Char = SurvText.Substring(i, Math.Min(1, SurvText.Length - i));
					GrText = ReflectionHelper.GetMember<IMSIGX.Graphic>(GrBaseSym, "Duplicate");
					ReflectionHelper.LetMember(GrText, "Properties", Char, "Info");

					wChar = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x");
					if (wChar < wCharBase)
					{
						wChar = wCharBase;
					}
					xcChar = (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x")) / 2;
					ycChar = (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "y")) / 2;
					dfi = wChar / R;
					if (Math.Sin(fiMidT) > 0)
					{
						if (i == 0)
						{
							fi -= dfi / 2;
							fii = fi0;
						}
						else
						{
							fi -= dfi;
							fii = fi + dfi / 2;
						}
					}
					else
					{
						if (i == 0)
						{
							fi += dfi / 2;
							fii = fi0;
						}
						else
						{
							fi += dfi;
							fii = fi - dfi / 2;
						}
					}
					xi = xc + R * Math.Cos(fii);
					yi = yc + R * Math.Sin(fii);
					if (Math.Sin(fiMidT) > 0)
					{
						ReflectionHelper.Invoke(GrText, "RotateAxis", new object[]{fii - Pi / 2, 0, 0, 1, xcChar, ycChar, 0});
					}
					else
					{
						ReflectionHelper.Invoke(GrText, "RotateAxis", new object[]{fii + Pi / 2, 0, 0, 1, xcChar, ycChar, 0});
					}
					ReflectionHelper.Invoke(GrText, "MoveRelative", new object[]{xi - xcChar, yi - ycChar, 0});

				}

				double fik = fii;

				ReflectionHelper.LetMember(GrCirc, "Deleted", true);

				x0 = xc + R * Math.Cos(fi0);
				y0 = yc + R * Math.Sin(fi0);
				x1 = xc + R * Math.Cos(fik);
				y1 = yc + R * Math.Sin(fik);
				x2 = xc + R * Math.Cos(fiMidT);
				y2 = yc + R * Math.Sin(fiMidT);
				GrCirc = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "AddArcTriplePoint", new object[]{x0, y0, 0, x2, y2, 0, x1, y1, 0});
				//        GrCirc.Cosmetic = True
				ReflectionHelper.LetMember(GrCirc, "Visible", false);
				ReflectionHelper.LetMember(GrBaseSym, "Deleted", true);
			}
			catch (Exception exc)
			{
				NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}

		}


		// Define Surveuor string for Arc Segment
		private string ArcSurveourString(IMSIGX.Graphic GrText, IMSIGX.Graphic GrArc)
		{
			string result = "";
			Module1.DrawingUnit = ReflectionHelper.Invoke<string>(GrText, "Properties", new object[]{"Units"});
			int TextStorey = 0;
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				TextStorey = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"TextStorey"});

				int AppendLenUnit = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"AppendLenUnit"});
				string LenUnit = ReflectionHelper.Invoke<string>(GrText, "Properties", new object[]{"LenUnit"});
				double UnitScale = 1; //""""
				if (LenUnit == "in" || LenUnit == "''" || LenUnit == "ft" || LenUnit == "'" || LenUnit == "yd" || LenUnit == "mi" || LenUnit == "mm" || LenUnit == "cm" || LenUnit == "m" || LenUnit == "km")
				{
					UnitScale = UnitToUnit(LenUnit, Module1.DrawingUnit);
				}
				if (AppendLenUnit == 0)
				{
					LenUnit = "";
				}
				int LenPrecision = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"LenPrecision"});
				string FormatString = "";
				if (LenPrecision == 0)
				{
					FormatString = "###0";
				}
				else
				{
					FormatString = "###0.";
				}
				for (int i = 1; i <= LenPrecision; i++)
				{
					FormatString = FormatString + "0";
				}

				Module1.DrawingUnit = ReflectionHelper.Invoke<string>(GrText, "Properties", new object[]{"Units"});

				double NorthAngle = Pi / 2;
				NorthAngle = ReflectionHelper.Invoke<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "Application"), "ActiveDrawing"), "Properties", new object[]{"AngleBase"}) / 180 * Pi + Pi / 2;
				int Dir = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"Direction"});

				int AngularSystem = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"AngularSystem"});
				int ArcSystem = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"ArcSystem"});

				double x2 = 0, x1 = 0, xc = 0, yc = 0, y1 = 0, y2 = 0;
				xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrArc, "Vertices"), "Item", new object[]{0}), "x");
				yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrArc, "Vertices"), "Item", new object[]{0}), "y");
				x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrArc, "Vertices"), "Item", new object[]{1}), "x");
				y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrArc, "Vertices"), "Item", new object[]{1}), "y");
				x2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrArc, "Vertices"), "Item", new object[]{2}), "x");
				y2 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrArc, "Vertices"), "Item", new object[]{2}), "y");

				//###############################################################
				//###############################################################
				//###############################################################
				double cosa = 0, L = 0, sina = 0, Alpha = 0;
				int BetaSec = 0;
				int Sec = 0, Deg = 0, Min = 0, MinSec = 0;
				string MinStr = "", DegStr = "", SecStr = "";
				string AlphaStr = "";
				string LStr = "";
				int StrLenL = 0, StrLenAlp = 0, dLen = 0;
				double Beta = 0;
				string Direction = "";
				if (ArcSystem > 3)
				{

					L = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
					if (L < Eps)
					{
						sina = 0;
						cosa = 1;
					}
					else
					{
						if (Dir == 1)
						{
							sina = (y2 - y1) / L;
							cosa = (x2 - x1) / L;
						}
						else
						{
							sina = (y1 - y2) / L;
							cosa = (x1 - x2) / L;
						}
					}
					Alpha = Angle(sina, cosa);

					// Taking into account real direction of NorthDirection Symbol
					Alpha += (Pi / 2 - NorthAngle);
					sina = Math.Sin(Alpha);
					cosa = Math.Cos(Alpha);

					if (cosa == 0 && sina > 0)
					{
						Direction = "NE";
						Beta = 0;
					}
					else
					{
						if (cosa == 0 && sina < 0)
						{
							Direction = "SE";
							Beta = 0;
						}
						else
						{
							if (cosa > 0 && sina == 0)
							{
								Direction = "NE";
								Beta = Pi / 2;
							}
							else
							{
								if (cosa < 0 && sina == 0)
								{
									Direction = "SE";
									Beta = Pi / 2;
								}
								else
								{

									if (sina > 0 && cosa > 0)
									{
										Direction = "NE";
										Beta = Pi / 2 - Alpha;
										if (Beta < 0)
										{
											Beta = 2 * Pi + Beta;
										}
									}
									else
									{
										if (sina > 0 && cosa < 0)
										{
											Direction = "NW";
											Beta = Alpha - Pi / 2;
											if (Beta < 0)
											{
												Beta = 2 * Pi + Beta;
											}
										}
										else
										{
											if (sina < 0 && cosa < 0)
											{
												Direction = "SW";
												Beta = 3 * Pi / 2 - Alpha;
												if (Beta < 0)
												{
													Beta = 2 * Pi + Beta;
												}
											}
											else
											{
												if (sina < 0 && cosa > 0)
												{
													Direction = "SE";
													Beta = Alpha - 3 * Pi / 2;
													if (Beta < 0)
													{
														Beta = 2 * Pi + Beta;
													}
												}
											}
										}
									}
								}
							}
						}
					}


					BetaSec = Convert.ToInt32(Math.Floor(Beta * 180 * 3600 / Pi)); // Angle in second
					if (Beta * 180 * 3600 / Pi - BetaSec > 0.4999999d)
					{
						BetaSec++;
					}

					Deg = Convert.ToInt32(Math.Floor((double) (BetaSec / 3600)));
					MinSec = BetaSec - Deg * 3600;
					Min = Convert.ToInt32(Math.Floor((double) (MinSec / 60)));
					Sec = Convert.ToInt32(Math.Floor((double) (MinSec - Min * 60)));
					if (Deg > 9)
					{
						DegStr = Deg.ToString();
					}
					else
					{
						if (Deg == 0)
						{
							DegStr = "00";
						}
						else
						{
							DegStr = "0" + Deg.ToString();
						}
					}
					if (Min > 9)
					{
						MinStr = Min.ToString();
					}
					else
					{
						if (Min == 0)
						{
							MinStr = "00";
						}
						else
						{
							MinStr = "0" + Min.ToString();
						}
					}
					if (Sec > 9)
					{
						SecStr = Sec.ToString();
					}
					else
					{
						if (Sec == 0)
						{
							SecStr = "00";
						}
						else
						{
							SecStr = "0" + Sec.ToString();
						}
					}

					if (AngularSystem == 0)
					{
						AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'" + SecStr + "''"; //""""'# NLS#'
					}

					if (AngularSystem == 1)
					{
						if (Sec < 30)
						{
							AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'"; //# NLS#'
						}
						else
						{
							Min++;
							if (Min > 59)
							{
								Deg++;
								Min -= 60;
							}

							if (Deg > 9)
							{
								DegStr = Deg.ToString();
							}
							else
							{
								if (Deg == 0)
								{
									DegStr = "00";
								}
								else
								{
									DegStr = "0" + Deg.ToString();
								}
							}
							if (Min > 9)
							{
								MinStr = Min.ToString();
							}
							else
							{
								if (Min == 0)
								{
									MinStr = "00";
								}
								else
								{
									MinStr = "0" + Min.ToString();
								}
							}
							AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'"; //# NLS#'
						}
					}

					if (AngularSystem == 2)
					{
						if (Min < 30)
						{
							AlphaStr = DegStr + Strings.Chr(186).ToString();
						}
						else
						{
							Deg++;
							if (Deg > 9)
							{
								DegStr = Deg.ToString();
							}
							else
							{
								if (Deg == 0)
								{
									DegStr = "00";
								}
								else
								{
									DegStr = "0" + Deg.ToString();
								}
							}
							AlphaStr = DegStr + Strings.Chr(186).ToString();
						}
					}

					if (Direction == "NE")
					{
						AlphaStr = "N" + AlphaStr + "E";
					}
					if (Direction == "NW")
					{
						AlphaStr = "N" + AlphaStr + "W";
					}
					if (Direction == "SE")
					{
						AlphaStr = "S" + AlphaStr + "E";
					}
					if (Direction == "SW")
					{
						AlphaStr = "S" + AlphaStr + "W";
					}

					LStr = StringsHelper.Format(L * UnitScale, FormatString);
					LStr = "Ch=" + LStr + LenUnit; //# NLS#'

					if (TextStorey == 0)
					{
						if (ArcSystem == 4)
						{
							result = AlphaStr + "   " + LStr;
						}
						if (ArcSystem == 5)
						{
							result = AlphaStr;
						}
						if (ArcSystem == 6)
						{
							result = LStr;
						}
					}
					else
					{
						if (ArcSystem == 4)
						{
							result = AlphaStr + "\r" + "\n" + LStr;
						}
						if (ArcSystem == 5)
						{
							result = AlphaStr;
						}
						if (ArcSystem == 6)
						{
							result = LStr;
						}
					}


					return result;
				}
				//###############################################################
				//###############################################################
				//###############################################################


				double fiBeg = 0, R = 0, fiEnd = 0;
				R = Math.Sqrt((x1 - xc) * (x1 - xc) + (y1 - yc) * (y1 - yc));
				if (R < Eps)
				{
					R = 1;
				}
				sina = (y1 - yc) / R;
				cosa = (x1 - xc) / R;
				fiBeg = Angle(sina, cosa);
				sina = (y2 - yc) / R;
				cosa = (x2 - xc) / R;
				fiEnd = Angle(sina, cosa);
				if (fiBeg > fiEnd)
				{
					fiEnd += 2 * Pi;
				}

				Beta = fiEnd - fiBeg;
				L = R * Beta;

				BetaSec = Convert.ToInt32(Math.Floor(Beta * 180 * 3600 / Pi)); // Angle in second
				if (Beta * 180 * 3600 / Pi - BetaSec > 0.499999d)
				{
					BetaSec++;
				}
				Deg = Convert.ToInt32(Math.Floor((double) (BetaSec / 3600)));
				MinSec = BetaSec - Deg * 3600;
				Min = Convert.ToInt32(Math.Floor((double) (MinSec / 60)));
				Sec = Convert.ToInt32(Math.Floor((double) (MinSec - Min * 60)));
				if (Deg > 9)
				{
					DegStr = Deg.ToString();
				}
				else
				{
					if (Deg == 0)
					{
						DegStr = "00";
					}
					else
					{
						DegStr = "0" + Deg.ToString();
					}
				}
				if (Min > 9)
				{
					MinStr = Min.ToString();
				}
				else
				{
					if (Min == 0)
					{
						MinStr = "00";
					}
					else
					{
						MinStr = "0" + Min.ToString();
					}
				}
				if (Sec > 9)
				{
					SecStr = Sec.ToString();
				}
				else
				{
					if (Sec == 0)
					{
						SecStr = "00";
					}
					else
					{
						SecStr = "0" + Sec.ToString();
					}
				}

				if (AngularSystem == 0)
				{
					AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'" + SecStr + "''"; //""""'# NLS#'
				}
				if (AngularSystem == 1)
				{
					if (Sec < 30)
					{
						AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'"; //# NLS#'
					}
					else
					{
						Min++;
						if (Min > 59)
						{
							Deg++;
							Min -= 60;
						}
						if (Deg > 9)
						{
							DegStr = Deg.ToString();
						}
						else
						{
							if (Deg == 0)
							{
								DegStr = "00";
							}
							else
							{
								DegStr = "0" + Deg.ToString();
							}
						}
						if (Min > 9)
						{
							MinStr = Min.ToString();
						}
						else
						{
							if (Min == 0)
							{
								MinStr = "00";
							}
							else
							{
								MinStr = "0" + Min.ToString();
							}
						}
						AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'"; //# NLS#'
					}
				}
				if (AngularSystem == 2)
				{
					if (Min < 30)
					{
						AlphaStr = DegStr + Strings.Chr(186).ToString();
					}
					else
					{
						Deg++;
						if (Deg > 9)
						{
							DegStr = Deg.ToString();
						}
						else
						{
							if (Deg == 0)
							{
								DegStr = "00";
							}
							else
							{
								DegStr = "0" + Deg.ToString();
							}
						}
						AlphaStr = DegStr + Strings.Chr(186).ToString();
					}
				}

				string RStr = "";
				RStr = StringsHelper.Format(R * UnitScale, FormatString);

				LStr = StringsHelper.Format(L * UnitScale, FormatString);
				RStr = RStr + LenUnit;
				LStr = LStr + LenUnit;

				if (TextStorey == 0)
				{
					if (ArcSystem == 0)
					{ // Angle-Radius-Length
						result = AlphaStr + " R=" + RStr + " A=" + LStr;
					}
					if (ArcSystem == 1)
					{ // Angle-Radius
						result = AlphaStr + " R=" + RStr;
					}
					if (ArcSystem == 2)
					{ // Angle-Length
						result = AlphaStr + " A=" + LStr;
					}
					if (ArcSystem == 3)
					{ //Radius-Length
						result = "R=" + RStr + " A=" + LStr;
					}
					if (ArcSystem == 4)
					{ // ChordBearing-ChordDistance
						result = AlphaStr + " R=" + RStr + " A=" + LStr;
					}
					if (ArcSystem == 5)
					{ // ChordBearing
						result = AlphaStr + " R=" + RStr + " A=" + LStr;
					}
					if (ArcSystem == 6)
					{ //ChordDistance
						result = AlphaStr + " R=" + RStr + " A=" + LStr;
					}

				}
				else
				{
					if (ArcSystem == 0)
					{ // Angle-Radius-Length
						result = AlphaStr + "\r" + "\n" + "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 1)
					{ // Angle-Radius
						result = AlphaStr + "\r" + "\n" + "R=" + RStr;
					}
					if (ArcSystem == 2)
					{ // Angle-Length
						result = AlphaStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 3)
					{ // Radius-Length
						result = "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 4)
					{ // ChordBearing-ChordDistance
						result = AlphaStr + "\r" + "\n" + "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 5)
					{ // ChordBearing
						result = AlphaStr + "\r" + "\n" + "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 6)
					{ //ChordDistance
						result = AlphaStr + "\r" + "\n" + "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
				}
				//MsgBox (ArcSurveourString)
			}
			catch (Exception exc)
			{
				NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}

			return result;
		}



		// Define Surveuor string for Polyline Segment
		private string PolylineSurveourString(IMSIGX.Graphic GrText, IMSIGX.Graphic GrPoly, int ConVertex)
		{
			string result = "";
			// Define parameters of the NorthDirection Symbol
			//Dim GrSet As GraphicSet
			//    Set GrSet = GrPoly.Drawing.Graphics.QuerySet("Type = ""NorthDirection""")
			//Dim SetCount As Long
			double NorthAngle = Pi / 2;
			//    SetCount = GrSet.Count
			//    If SetCount > 0 Then
			//Dim GrNorth As Graphic
			//        Set GrNorth = GrSet(0)
			//        NorthAngle = GrNorth.Properties("NorthAngle")
			//    Else
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				NorthAngle = ReflectionHelper.Invoke<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "Application"), "ActiveDrawing"), "Properties", new object[]{"AngleBase"}) / 180 * Pi + Pi / 2;
				//    End If
				//    GrSet.Clear
				//    Set GrSet = Nothing
				//    Set GrNorth = Nothing
				int Dir = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"Direction"});
				int TextStorey = 0;
				TextStorey = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"TextStorey"});

				int ArcSystem = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"ArcSystem"});
				int AngularSystem = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"AngularSystem"});


				Module1.DrawingUnit = ReflectionHelper.Invoke<string>(GrText, "Properties", new object[]{"Units"});
				int AppendLenUnit = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"AppendLenUnit"});
				string LenUnit = ReflectionHelper.Invoke<string>(GrText, "Properties", new object[]{"LenUnit"});
				double UnitScale = 1;
				//""""
				if (LenUnit == "in" || LenUnit == "''" || LenUnit == "ft" || LenUnit == "'" || LenUnit == "yd" || LenUnit == "mi" || LenUnit == "mm" || LenUnit == "cm" || LenUnit == "m" || LenUnit == "km")
				{
					UnitScale = UnitToUnit(LenUnit, Module1.DrawingUnit);
				}

				if (AppendLenUnit == 0)
				{
					LenUnit = "";
				}
				int LenPrecision = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"LenPrecision"});
				string FormatString = "";
				if (LenPrecision == 0)
				{
					FormatString = "###0";
				}
				else
				{
					FormatString = "###0.";
				}
				for (int i = 1; i <= LenPrecision; i++)
				{
					FormatString = FormatString + "0";
				}

				Module1.DrawingUnit = ReflectionHelper.Invoke<string>(GrText, "Properties", new object[]{"Units"});


				bool IfArcSegment = false;

				if (ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(GrPoly, "Vertices", new object[]{ConVertex + 1}), "Bulge"))
				{
					IfArcSegment = true;
					if (ArcSystem < 4)
					{
						goto ARCSEG;
					}
				}


				// if Line Segment
				double x1 = 0, x0 = 0, y0 = 0, y1 = 0;
				x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex}), "x");
				y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex}), "y");
				if (!IfArcSegment)
				{
					x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 1}), "x");
					y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 1}), "y");
				}
				else
				{
					x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 3}), "x");
					y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 3}), "y");
				}
				double cosa = 0, L = 0, sina = 0, Alpha = 0;
				L = Math.Sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
				if (L < Eps)
				{
					sina = 0;
					cosa = 1;
				}
				else
				{
					if (Dir == 1)
					{
						sina = (y1 - y0) / L;
						cosa = (x1 - x0) / L;
					}
					else
					{
						sina = (y0 - y1) / L;
						cosa = (x0 - x1) / L;
					}
				}
				Alpha = Angle(sina, cosa);

				// Taking into account real direction of NorthDirection Symbol
				Alpha += (Pi / 2 - NorthAngle);
				sina = Math.Sin(Alpha);
				cosa = Math.Cos(Alpha);

				double Beta = 0;
				string Direction = "";
				if (cosa == 0 && sina > 0)
				{
					Direction = "NE";
					Beta = 0;
				}
				else
				{
					if (cosa == 0 && sina < 0)
					{
						Direction = "SE";
						Beta = 0;
					}
					else
					{
						if (cosa > 0 && sina == 0)
						{
							Direction = "NE";
							Beta = Pi / 2;
						}
						else
						{
							if (cosa < 0 && sina == 0)
							{
								Direction = "SE";
								Beta = Pi / 2;
							}
							else
							{

								if (sina > 0 && cosa > 0)
								{
									Direction = "NE";
									Beta = Pi / 2 - Alpha;
									if (Beta < 0)
									{
										Beta = 2 * Pi + Beta;
									}
								}
								else
								{
									if (sina > 0 && cosa < 0)
									{
										Direction = "NW";
										Beta = Alpha - Pi / 2;
										if (Beta < 0)
										{
											Beta = 2 * Pi + Beta;
										}
									}
									else
									{
										if (sina < 0 && cosa < 0)
										{
											Direction = "SW";
											Beta = 3 * Pi / 2 - Alpha;
											if (Beta < 0)
											{
												Beta = 2 * Pi + Beta;
											}
										}
										else
										{
											if (sina < 0 && cosa > 0)
											{
												Direction = "SE";
												Beta = Alpha - 3 * Pi / 2;
												if (Beta < 0)
												{
													Beta = 2 * Pi + Beta;
												}
											}
										}
									}
								}
							}
						}
					}
				}

				int BetaSec = 0;
				BetaSec = Convert.ToInt32(Math.Floor(Beta * 180 * 3600 / Pi)); // Angle in second
				if (Beta * 180 * 3600 / Pi - BetaSec > 0.4999999d)
				{
					BetaSec++;
				}

				int Sec = 0, Deg = 0, Min = 0, MinSec = 0;
				Deg = Convert.ToInt32(Math.Floor((double) (BetaSec / 3600)));
				MinSec = BetaSec - Deg * 3600;
				Min = Convert.ToInt32(Math.Floor((double) (MinSec / 60)));
				Sec = Convert.ToInt32(Math.Floor((double) (MinSec - Min * 60)));
				string MinStr = "", DegStr = "", SecStr = "";
				if (Deg > 9)
				{
					DegStr = Deg.ToString();
				}
				else
				{
					if (Deg == 0)
					{
						DegStr = "00";
					}
					else
					{
						DegStr = "0" + Deg.ToString();
					}
				}
				if (Min > 9)
				{
					MinStr = Min.ToString();
				}
				else
				{
					if (Min == 0)
					{
						MinStr = "00";
					}
					else
					{
						MinStr = "0" + Min.ToString();
					}
				}
				if (Sec > 9)
				{
					SecStr = Sec.ToString();
				}
				else
				{
					if (Sec == 0)
					{
						SecStr = "00";
					}
					else
					{
						SecStr = "0" + Sec.ToString();
					}
				}

				string AlphaStr = "";
				if (AngularSystem == 0)
				{
					AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'" + SecStr + "''"; //""""'# NLS#'
				}

				if (AngularSystem == 1)
				{
					if (Sec < 30)
					{
						AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'"; //# NLS#'
					}
					else
					{
						Min++;
						if (Min > 59)
						{
							Deg++;
							Min -= 60;
						}

						if (Deg > 9)
						{
							DegStr = Deg.ToString();
						}
						else
						{
							if (Deg == 0)
							{
								DegStr = "00";
							}
							else
							{
								DegStr = "0" + Deg.ToString();
							}
						}
						if (Min > 9)
						{
							MinStr = Min.ToString();
						}
						else
						{
							if (Min == 0)
							{
								MinStr = "00";
							}
							else
							{
								MinStr = "0" + Min.ToString();
							}
						}
						AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'"; //# NLS#'
					}
				}

				if (AngularSystem == 2)
				{
					if (Min < 30)
					{
						AlphaStr = DegStr + Strings.Chr(186).ToString();
					}
					else
					{
						Deg++;
						if (Deg > 9)
						{
							DegStr = Deg.ToString();
						}
						else
						{
							if (Deg == 0)
							{
								DegStr = "00";
							}
							else
							{
								DegStr = "0" + Deg.ToString();
							}
						}
						AlphaStr = DegStr + Strings.Chr(186).ToString();
					}
				}

				if (Direction == "NE")
				{
					AlphaStr = "N" + AlphaStr + "E";
				}
				if (Direction == "NW")
				{
					AlphaStr = "N" + AlphaStr + "W";
				}
				if (Direction == "SE")
				{
					AlphaStr = "S" + AlphaStr + "E";
				}
				if (Direction == "SW")
				{
					AlphaStr = "S" + AlphaStr + "W";
				}
				string LStr = "";
				LStr = StringsHelper.Format(L * UnitScale, FormatString);
				LStr = LStr + LenUnit;

				int LineSystem = 0;
				int StrLenL = 0, StrLenAlp = 0, dLen = 0;
				if (!IfArcSegment)
				{
					LineSystem = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"LineSystem"});
					if (TextStorey == 0)
					{
						if (LineSystem == 0)
						{
							result = AlphaStr + "   " + LStr;
						}
						if (LineSystem == 1)
						{
							result = AlphaStr;
						}
						if (LineSystem == 2)
						{
							result = LStr;
						}
					}
					else
					{
						if (LineSystem == 0)
						{
							StrLenAlp = Strings.Len(AlphaStr);
							StrLenL = Strings.Len(LStr);
							dLen = Convert.ToInt32(Math.Abs(Math.Floor((double) (StrLenAlp - StrLenL))));
							if (StrLenAlp > StrLenL)
							{
								result = AlphaStr + "\r" + "\n";
								for (int i = 1; i <= dLen; i++)
								{
									result = result + " ";
								}
								result = result + LStr;
							}
							else
							{
								for (int i = 1; i <= dLen; i++)
								{
									result = result + " ";
								}
								result = result + AlphaStr + "\r" + "\n" + LStr;
							}
						}
						if (LineSystem == 1)
						{
							result = AlphaStr;
						}
						if (LineSystem == 2)
						{
							result = LStr;
						}
					}
				}
				else
				{
					// if Arc Segment
					LStr = "Ch=" + LStr; //# NLS#'
					if (TextStorey == 0)
					{
						if (ArcSystem == 4)
						{
							result = AlphaStr + " " + LStr;
						}
						if (ArcSystem == 5)
						{
							result = AlphaStr;
						}
						if (ArcSystem == 6)
						{
							result = LStr;
						}
					}
					else
					{
						if (ArcSystem == 4)
						{
							result = AlphaStr + "\r" + "\n" + LStr;
						}
						if (ArcSystem == 5)
						{
							result = AlphaStr;
						}
						if (ArcSystem == 6)
						{
							result = LStr;
						}
					}



				}

				//MsgBox (SurveourString)
				return result;
ARCSEG:
				//##########################################################################
				//##########################################################################

				double xDir = 0, xMidC = 0, x2C = 0, x1C = 0, xc = 0, yc = 0, y1C = 0, y2C = 0, yMidC = 0, yDir = 0;
				xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 1}), "x");
				yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 1}), "y");
				x1C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex}), "x");
				y1C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex}), "y");
				x2C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 3}), "x");
				y2C = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 3}), "y");
				xDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 2}), "x");
				yDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 2}), "y");
				//Define current Arc-segment and middle point on it


				double fiEndC = 0, R = 0, fiBegC = 0, fiMidC = 0;
				R = Math.Sqrt((x1C - xc) * (x1C - xc) + (y1C - yc) * (y1C - yc));
				if (R < Eps)
				{
					R = 1;
				}
				int Rot = 0;
				Rot = ArcRotation(x1C, y1C, xDir, yDir, xc, yc, R);
				ArcMiddlePoint(xc, yc, x1C, y1C, x2C, y2C, R, -Rot, ref xMidC, ref yMidC);

				sina = (y1C - yc) / R;
				cosa = (x1C - xc) / R;
				fiBegC = Angle(sina, cosa);
				sina = (y2C - yc) / R;
				cosa = (x2C - xc) / R;
				fiEndC = Angle(sina, cosa);
				sina = (yMidC - yc) / R;
				cosa = (xMidC - xc) / R;
				fiMidC = Angle(sina, cosa);

				if (Rot == 1)
				{
					if (fiBegC > fiEndC)
					{
						fiBegC -= 2 * Pi;
					}
					Beta = fiEndC - fiBegC;
				}
				else
				{
					if (fiEndC > fiBegC)
					{
						fiEndC -= 2 * Pi;
					}
					Beta = fiBegC - fiEndC;
				}

				L = R * Beta;

				BetaSec = Convert.ToInt32(Math.Floor(Beta * 180 * 3600 / Pi)); // Angle in second
				if (Beta * 180 * 3600 / Pi - BetaSec > 0.499999d)
				{
					BetaSec++;
				}
				Deg = Convert.ToInt32(Math.Floor((double) (BetaSec / 3600)));
				MinSec = BetaSec - Deg * 3600;
				Min = Convert.ToInt32(Math.Floor((double) (MinSec / 60)));
				Sec = Convert.ToInt32(Math.Floor((double) (MinSec - Min * 60)));
				if (Deg > 9)
				{
					DegStr = Deg.ToString();
				}
				else
				{
					if (Deg == 0)
					{
						DegStr = "00";
					}
					else
					{
						DegStr = "0" + Deg.ToString();
					}
				}
				if (Min > 9)
				{
					MinStr = Min.ToString();
				}
				else
				{
					if (Min == 0)
					{
						MinStr = "00";
					}
					else
					{
						MinStr = "0" + Min.ToString();
					}
				}
				if (Sec > 9)
				{
					SecStr = Sec.ToString();
				}
				else
				{
					if (Sec == 0)
					{
						SecStr = "00";
					}
					else
					{
						SecStr = "0" + Sec.ToString();
					}
				}

				AngularSystem = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"AngularSystem"});

				if (AngularSystem == 0)
				{
					AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'" + SecStr + "''"; //""""'# NLS#'
				}
				if (AngularSystem == 1)
				{
					if (Sec < 30)
					{
						AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'"; //# NLS#'
					}
					else
					{
						Min++;
						if (Min > 59)
						{
							Deg++;
							Min -= 60;
						}
						if (Deg > 9)
						{
							DegStr = Deg.ToString();
						}
						else
						{
							if (Deg == 0)
							{
								DegStr = "00";
							}
							else
							{
								DegStr = "0" + Deg.ToString();
							}
						}
						if (Min > 9)
						{
							MinStr = Min.ToString();
						}
						else
						{
							if (Min == 0)
							{
								MinStr = "00";
							}
							else
							{
								MinStr = "0" + Min.ToString();
							}
						}
						AlphaStr = DegStr + Strings.Chr(186).ToString() + MinStr + "'"; //# NLS#'
					}
				}
				if (AngularSystem == 2)
				{
					if (Min < 30)
					{
						AlphaStr = DegStr + Strings.Chr(186).ToString();
					}
					else
					{
						Deg++;
						if (Deg > 9)
						{
							DegStr = Deg.ToString();
						}
						else
						{
							if (Deg == 0)
							{
								DegStr = "00";
							}
							else
							{
								DegStr = "0" + Deg.ToString();
							}
						}
						AlphaStr = DegStr + Strings.Chr(186).ToString();
					}
				}

				string RStr = "";
				RStr = StringsHelper.Format(R * UnitScale, FormatString);

				LStr = StringsHelper.Format(L * UnitScale, FormatString);
				RStr = RStr + LenUnit;
				LStr = LStr + LenUnit;


				if (TextStorey == 0)
				{
					if (ArcSystem == 0)
					{ // Angle-Radius-Length
						result = AlphaStr + " R=" + RStr + " A=" + LStr;
					}
					if (ArcSystem == 1)
					{ // Angle-Radius
						result = AlphaStr + " R=" + RStr;
					}
					if (ArcSystem == 2)
					{ // Angle-Length
						result = AlphaStr + " A=" + LStr;
					}
					if (ArcSystem == 3)
					{ //Radius-Length
						result = "R=" + RStr + " A=" + LStr;
					}
					if (ArcSystem == 4)
					{ // ChordBearing-ChordDistance
						result = AlphaStr + " R=" + RStr + " A=" + LStr;
					}
					if (ArcSystem == 5)
					{ // ChordBearing
						result = AlphaStr + " R=" + RStr + " A=" + LStr;
					}
					if (ArcSystem == 6)
					{ // ChordDistance
						result = AlphaStr + " R=" + RStr + " A=" + LStr;
					}

				}
				else
				{
					if (ArcSystem == 0)
					{ // Angle-Radius-Length
						result = AlphaStr + "\r" + "\n" + "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 1)
					{ // Angle-Radius
						result = AlphaStr + "\r" + "\n" + "R=" + RStr;
					}
					if (ArcSystem == 2)
					{ // Angle-Length
						result = AlphaStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 3)
					{ // Radius-Length
						result = "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 4)
					{ // ChordBearing-ChordDistance
						result = AlphaStr + "\r" + "\n" + "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 5)
					{ // ChordBearing
						result = AlphaStr + "\r" + "\n" + "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
					if (ArcSystem == 6)
					{ // ChordDistance
						result = AlphaStr + "\r" + "\n" + "R=" + RStr + "\r" + "\n" + "L=" + LStr;
					}
				}


				//MsgBox (PolylineSurveourString)
			}
			catch (Exception exc)
			{
				NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}

			return result;
		}


		// Update the text along connected Polyline with arc segments

		private void TextAlongPolyline(IMSIGX.Graphic Gr, IMSIGX.Graphic GrPoly, IMSIGX.Graphic GrText, string SurvText, double HText, ref double LText, double xcText, double ycText, ref int ConVertex)
		{
			//MsgBox ("TextAlongLine")
			//UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (ErrEnd)");
			int TextLocation = 0;
			TextLocation = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"TextLocation"});
			double TextGap = 0;
			TextGap = ReflectionHelper.Invoke<double>(Gr, "Properties", new object[]{"TextGap"});
			int TextStorey = 0;
			TextStorey = 0;
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				TextStorey = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"TextStorey"});

				int TextHorizontal = 0;
				TextHorizontal = 0;
				TextHorizontal = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"TextHorizontal"});
				//MsgBox ("TextHorizontal=" & CStr(TextHorizontal))
				if (ConVertex == -1)
				{
					ConVertex = 0;
				}
				int GrConIDOld = 0;
				GrConIDOld = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"GrConIDOld"});

				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				double HTextReal = 0;
				int ChildCount = 0;
				double x1L = 0, x0L = 0, y0L = 0, y1L = 0;
				double sinaL = 0, lL = 0, cosaL = 0;
				double AlphaL = 0;
				double xLoc2 = 0, xLoc1 = 0, xLoc0 = 0, yLoc0 = 0, yLoc1 = 0, yLoc2 = 0;
				double x2 = 0, x1 = 0, x0 = 0, y0 = 0, y1 = 0, y2 = 0;
				double yTem = 0, cosa = 0, sina = 0, xTem = 0, L = 0;
				double Alpha = 0;
				if (!(ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(GrPoly, "Vertices", new object[]{ConVertex + 1}), "Bulge")))
				{


					HTextReal = 0;
					HTextReal = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "y") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "y");
					LText = ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Max"), "x") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "CalcBoundingBox"), "Min"), "x");
					ChildCount = 0;
					ChildCount = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(Gr, "Graphics"), "Count");
					for (int i = 0; i <= ChildCount - 1; i++)
					{
						//                If .Item(i).Type = TcLoadLangString(104) Then
						if (ReflectionHelper.GetMember<string>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Graphics"), "Item", new object[]{i}), "Type") == "ARC")
						{
							ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Graphics"), "Item", new object[]{i}), "Deleted", true);
							break;
						}
					}


					// If text connected to line segment
					//############################################################
					// Define the geometry parameters of Connected line
					//############################################################
					// Define the geometry parameters of Connected line
					x1L = 0;
					x0L = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex}), "x");
					y0L = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex}), "y");
					x1L = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 1}), "x");
					y1L = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 1}), "y");
					sinaL = 0;
					AlphaL = 0;
					lL = Math.Sqrt((x1L - x0L) * (x1L - x0L) + (y1L - y0L) * (y1L - y0L));
					if (lL < Eps)
					{
						sinaL = 0;
						cosaL = 1;
					}
					else
					{
						sinaL = (y1L - y0L) / lL;
						cosaL = (x1L - x0L) / lL;
					}
					AlphaL = Angle(sinaL, cosaL);
					//############################################################


					xLoc2 = 0;
					x2 = 0;
					x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "x"); // Center point of the text
					y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "y");
					yLoc0 = 0;
					yLoc1 = 0;
					yLoc2 = 0;
					// Begin If first connection text to line
					GrConIDOld = ReflectionHelper.Invoke<int>(Gr, "Properties", new object[]{"GrConIDOld"});
					if (GrConIDOld == -1 || lL < LText)
					{
						x0 = (x1L + x0L) / 2;
						y0 = (y1L + y0L) / 2;
						xLoc1 = lL / 2 - LText / 2;
						yLoc1 = 0;
						xLoc2 = lL / 2 + LText / 2;
						yLoc2 = 0;
						xLoc0 = lL / 2;
						yLoc0 = 0;
						// End If first connection text to line
					}
					else
					{
						xLoc0 = (y0 - y0L) * sinaL + (x0 - x0L) * cosaL;
						xLoc1 = xLoc0 - LText / 2;
						xLoc2 = xLoc0 + LText / 2;
						if (xLoc1 < 0)
						{
							xLoc0 = LText / 2;
							xLoc1 = 0;
							xLoc2 = LText;
						}
						if (xLoc2 > lL)
						{
							xLoc0 = lL - LText / 2;
							xLoc1 = lL - LText;
							xLoc2 = lL;
						}

					}
					x1 = x0L + xLoc1 * cosaL - yLoc1 * sinaL;
					y1 = y0L + xLoc1 * sinaL + yLoc1 * cosaL;

					x2 = x0L + xLoc2 * cosaL - yLoc2 * sinaL;
					y2 = y0L + xLoc2 * sinaL + yLoc2 * cosaL;

					if (TextHorizontal == 0 && TextStorey == 1 && Math.Abs(TextGap) < 1.001d * HText)
					{
						TextGap = -HText;
						ReflectionHelper.LetMember(Gr, "Properties", TextGap, "TextGap");
					}

					if (GrConIDOld == -1 && TextHorizontal == 1)
					{
						TextGap = (LText / 2 - HText) * Math.Abs(sinaL);
						ReflectionHelper.LetMember(Gr, "Properties", TextGap, "TextGap");
					}


					//########################################################################
					if (TextLocation == 1)
					{ // If text above line
						if (cosaL < -0.01d)
						{
							yLoc0 = (-HTextReal) / 2 - TextGap;
						}
						else
						{
							yLoc0 = HTextReal / 2 + TextGap;
						}
					}
					if (TextLocation == 0)
					{ // If text Middle line
						yLoc0 = 0 + TextGap;
					}
					if (TextLocation == -1)
					{ // If text Middle line
						if (cosaL < -0.01d)
						{
							yLoc0 = HTextReal / 2 + TextGap;
						}
						else
						{
							yLoc0 = (-HTextReal) / 2 - TextGap;
						}
					}
					yLoc1 = yLoc0;
					yLoc2 = yLoc0;


					//########################################################################
					//new coordinates for the Base Text vertices
					x0 = x0L + xLoc0 * cosaL - yLoc0 * sinaL;
					y0 = y0L + xLoc0 * sinaL + yLoc0 * cosaL;

					x1 = x0L + xLoc1 * cosaL - yLoc1 * sinaL;
					y1 = y0L + xLoc1 * sinaL + yLoc1 * cosaL;

					x2 = x0L + xLoc2 * cosaL - yLoc2 * sinaL;
					y2 = y0L + xLoc2 * sinaL + yLoc2 * cosaL;

					// Begin examine the location of the limits text vertices
					yTem = 0;
					Alpha = 0;
					L = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
					if (L < Eps)
					{
						sina = 0;
						cosa = 1;
					}
					else
					{
						sina = (y2 - y1) / L;
						cosa = (x2 - x1) / L;
					}
					Alpha = Angle(sina, cosa);
					if (cosa < -0.01d)
					{
						xTem = x1;
						yTem = y1;
						x1 = x2;
						y1 = y2;
						x2 = xTem;
						y2 = yTem;
						xTem = xLoc1;
						xLoc1 = xLoc2;
						xLoc2 = xTem;
						Alpha += Pi;
					}
					// End examine the location of the limits text vertices

					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "x", x0);
					ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{0}), "y", y0);
					if (TextHorizontal == 1)
					{
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x", x0 - LText / 2);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y", y0);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x", x0 + LText / 2);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y", y0);
					}
					else
					{
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "x", x1);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{1}), "y", y1);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "x", x2);
						ReflectionHelper.LetMember(ReflectionHelper.Invoke(ReflectionHelper.GetMember(Gr, "Vertices"), "Item", new object[]{2}), "y", y2);
					}
					// correct location of the center child graphic

					// locate text between 2 limits vertices
					if (TextHorizontal == 0)
					{
						ReflectionHelper.Invoke(GrText, "RotateAxis", new object[]{Alpha, 0, 0, 1, xcText, ycText, 0});
						ReflectionHelper.Invoke(GrText, "MoveRelative", new object[]{x0 - xcText, y0 - ycText, 0});
					}
					else
					{
						ReflectionHelper.Invoke(GrText, "MoveRelative", new object[]{x0 - xcText, y0 - ycText, 0});
					}

					return;
				}


				ReflectionHelper.LetMember(GrText, "Deleted", true);
				double xDir = 0, xMidC = 0, xEC = 0, xBC = 0, xc = 0, yc = 0, yBC = 0, yEC = 0, yMidC = 0, yDir = 0;
				xc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 1}), "x");
				yc = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 1}), "y");
				xBC = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex}), "x");
				yBC = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex}), "y");
				xEC = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 3}), "x");
				yEC = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 3}), "y");
				xDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 2}), "x");
				yDir = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrPoly, "Vertices"), "Item", new object[]{ConVertex + 2}), "y");
				//Define current Arc-segment and middle point on it


				double R = 0;
				R = Math.Sqrt((xBC - xc) * (xBC - xc) + (yBC - yc) * (yBC - yc));
				if (R < Eps)
				{
					R = 1;
				}
				int Rot = 0;
				Rot = ArcRotation(xBC, yBC, xDir, yDir, xc, yc, R);
				ArcMiddlePoint(xc, yc, xBC, yBC, xEC, yEC, R, Rot, ref xMidC, ref yMidC);
				int Dictinct = 0;
				Dictinct = ArcDictinct(xBC, yBC, xEC, yEC, xMidC, yMidC, Rot);
				//MsgBox ("Rot=" & CStr(Rot) & " xMidC=" & CStr(xMidC) & " yMidC=" & CStr(yMidC))
				//MsgBox ("Rot=" & CStr(Rot) & "  Dictinct=" & CStr(Dictinct))

				//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				IMSIGX.Graphic GrCirc = null;
				if (Rot == -1)
				{
					GrCirc = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "AddArcTriplePoint", new object[]{xBC, yBC, 0, xMidC, yMidC, 0, xEC, yEC, 0});
				}
				else
				{
					GrCirc = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Gr, "Graphics"), "AddArcTriplePoint", new object[]{xEC, yEC, 0, xMidC, yMidC, 0, xBC, yBC, 0});
				}
				//MsgBox ("Rot=" & CStr(Rot) & "  Dictinct=" & CStr(Dictinct))
				TextAlongCircle(Gr, GrCirc, SurvText, HText, Dictinct);
				ReflectionHelper.LetMember(GrCirc, "Deleted", true);
				GrCirc = null;
				return;
ErrEnd:
				GrCirc = null;
			}
			catch (Exception exc)
			{
				NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}
		}



		// Define the middle point of Base Arc
		// from Beg-End Points and Rot
		// Rot=1 - Counter Clockwise
		// Rot=-1 - Clockwise
		private void ArcMiddlePoint(double xc, double yc, double xB, double yB, double xE, double yE, double R, int Rot, ref double xmid, ref double ymid)
		{
			if (Math.Abs(R) < Eps)
			{
				xmid = xc;
				ymid = yc;
				return;
			}

			double sina = (yB - yc) / R;
			double cosa = (xB - xc) / R;
			double fiBeg = Angle(sina, cosa);

			sina = (yE - yc) / R;
			cosa = (xE - xc) / R;
			double fiEnd = Angle(sina, cosa);

			if (Rot == 1)
			{ // Counter Clockwise
				if (fiBeg > fiEnd)
				{
					fiEnd -= 2 * Pi;
				}
			}
			else
			{
				//Clockwise
				if (fiBeg < fiEnd)
				{
					fiEnd -= 2 * Pi;
				}
			}

			double fiMid = (fiBeg + fiEnd) / 2;
			xmid = xc + R * Math.Cos(fiMid);
			ymid = yc + R * Math.Sin(fiMid);


		}


		// Define the direction of Arc rotation
		// return ArcRotation = 1 if CounterClockwise
		// return ArcRotation = -1 if Clockwise

		private int ArcRotation(double xB, double yB, double xDir, double yDir, double xc, double yc, double R)
		{
			double sina = (yB - yc) / R; //, Alp0#, Alp1#
			double cosa = (xB - xc) / R; //, Alp0#, Alp1#
			double xLoc = (yDir - yc) * sina + (xDir - xc) * cosa;
			double yLoc = (yDir - yc) * cosa - (xDir - xc) * sina;
			if (yLoc > 0)
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}


		// Define distinct of Arc
		private int ArcDictinct(double xB, double yB, double xE, double yE, double xj, double yj, int Rot)
		{
			int result = 0;
			double L = Math.Sqrt((xE - xB) * (xE - xB) + (yE - yB) * (yE - yB));
			if (L < Eps)
			{
				L = 1;
			}
			double sina = (yE - yB) / L;
			double cosa = (xE - xB) / L;
			double yLoc = (yj - yB) * cosa - (xj - xB) * sina;
			if (Rot == -1)
			{
				if (yLoc > 0)
				{
					result = 1;
				}
				else
				{
					result = -1;
				}
			}
			if (Rot == 1)
			{
				if (yLoc > 0)
				{
					result = -1;
				}
				else
				{
					result = 1;
				}
			}

			return result;
		}

		private int ConnectionExist(IMSIGX.Graphic Gr, int GrConID, ref bool TieWasted)
		{
			int result = 0;
			result = GrConID;
			//MsgBox ("ConnectionExist grID=" & CStr(Gr.ID))
			GXTIESLib.TieSets TSets = null;
			GXTIESLib.TieSet TSet = null;
			TieWasted = true;
			//UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (ErrHandler)");
			TSets = ReflectionHelper.GetMember<GXTIESLib.TieSets>(ReflectionHelper.GetMember(Gr, "Drawing"), "TieSets");
			TSet = null;
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				object tempRefParam = GrConID;
				object tempRefParam2 = ReflectionHelper.GetMember(Gr, "id");
				TSet = TSets.get_TieSet(ref tempRefParam, ref tempRefParam2);
				GrConID = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam);
				if (TSet == null)
				{
					result = -1;
				}
				else
				{
					TieWasted = TSet.get_flag(GXTIESLib.imsiTieFlag.imsiTieWasted) != 0;
				}
				return result;
ErrHandler:;
				//    MsgBox "Error: " + Err.Description
			}
			catch (Exception exc)
			{
				NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}
			return result;
		}

		//Calculate the Units scale if it changes

		private double UnitToUnit(string UnitName, string UnitNameOld)
		{
			double result = 0;
			result = 1d;
			//MsgBox ("UnitNameOld=" & UnitNameOld & "   UnitNameNew=" & UnitName)
			//""""
			if (UnitNameOld == "in" || UnitNameOld == "''")
			{
				//""""
				if (UnitName == "''" || UnitName == "in")
				{
					return 1d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1d / 36d;
				}
				if (UnitName == "mi")
				{
					return 1d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 25.4d;
				}
				if (UnitName == "cm")
				{
					return 2.54d;
				}
				if (UnitName == "m")
				{
					return 2.54d / 100d;
				}
				if (UnitName == "km")
				{
					return 2.54d / 100d / 1000d;
				}
			}


			if (UnitNameOld == "ft" || UnitNameOld == "'")
			{
				//""""
				if (UnitName == "''" || UnitName == "in")
				{
					return 12d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1d;
				}
				if (UnitName == "yd")
				{
					return 1d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 12d * 25.4d;
				}
				if (UnitName == "cm")
				{
					return 12d * 2.54d;
				}
				if (UnitName == "m")
				{
					return 12d * 2.54d / 100d;
				}
				if (UnitName == "km")
				{
					return 12d * 2.54d / 100d / 1000d;
				}
			}

			if (UnitNameOld == "yd")
			{
				//""""
				if (UnitName == "''" || UnitName == "in")
				{
					return 3d * 12d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 3d;
				}
				if (UnitName == "yd")
				{
					return 1d;
				}
				if (UnitName == "mi")
				{
					return 1d / 5280d / 3d;
				}
				if (UnitName == "mm")
				{
					return 3d * 12d * 25.4d;
				}
				if (UnitName == "cm")
				{
					return 3d * 12d * 2.54d;
				}
				if (UnitName == "m")
				{
					return 3d * 12d * 2.54d / 100d;
				}
				if (UnitName == "km")
				{
					return 3d * 12d * 2.54d / 100d / 1000d;
				}
			}
			// Add Miles
			//########################################################

			if (UnitNameOld == "mi")
			{
				//""""
				if (UnitName == "''" || UnitName == "in")
				{
					return 5280d * 12d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 5280d;
				}
				if (UnitName == "yd")
				{
					return 1760d;
				}
				if (UnitName == "mi")
				{
					return 1d;
				}
				if (UnitName == "mm")
				{
					return 1.609344d * 1000d * 1000d;
				}
				if (UnitName == "cm")
				{
					return 1.609344d * 1000d * 100d;
				}
				if (UnitName == "m")
				{
					return 1.609344d * 1000d;
				}
				if (UnitName == "km")
				{
					return 1.609344d;
				}
			}
			//########################################################

			if (UnitNameOld == "mm")
			{
				//""""
				if (UnitName == "''" || UnitName == "in")
				{
					return 1d / 25.4d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1d / 25.4d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1d / 25.4d / 12d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1d / 25.4d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 1d;
				}
				if (UnitName == "cm")
				{
					return 1d / 10d;
				}
				if (UnitName == "m")
				{
					return 1d / 1000d;
				}
				if (UnitName == "km")
				{
					return 1d / 1000d / 1000d;
				}
			}

			if (UnitNameOld == "cm")
			{
				//""""
				if (UnitName == "''" || UnitName == "in")
				{
					return 1d / 2.54d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1d / 2.54d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1d / 2.54d / 12d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1d / 2.54d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 10d;
				}
				if (UnitName == "cm")
				{
					return 1d;
				}
				if (UnitName == "m")
				{
					return 1d / 100d;
				}
				if (UnitName == "km")
				{
					return 1d / 100d / 1000d;
				}
			}


			if (UnitNameOld == "m")
			{
				//""""
				if (UnitName == "''" || UnitName == "in")
				{
					return 1000d / 25.4d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1000d / 25.4d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1000d / 25.4d / 12d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1000d / 25.4d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 1000d;
				}
				if (UnitName == "cm")
				{
					return 100d;
				}
				if (UnitName == "m")
				{
					return 1d;
				}
				if (UnitName == "km")
				{
					return 1d / 1000d;
				}
			}

			if (UnitNameOld == "km")
			{
				//""""
				if (UnitName == "''" || UnitName == "in")
				{
					return 1000d * 1000d / 25.4d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1000d * 1000d / 25.4d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1000d * 1000d / 25.4d / 12d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1000d * 1000d / 25.4d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 1000d * 1000d;
				}
				if (UnitName == "cm")
				{
					return 100d * 1000d;
				}
				if (UnitName == "m")
				{
					return 1d * 1000d;
				}
				if (UnitName == "km")
				{
					return 1d;
				}
			}

			return 1;

		}

		// Form string from string with symbols
		private double StringToSize(string Str)
		{
			string[] CharInt = ArraysHelper.InitializeArray<string>(11);
			CharInt[0] = "0";
			CharInt[1] = "1";
			CharInt[2] = "2";
			CharInt[3] = "3";
			CharInt[4] = "4";
			CharInt[5] = "5";
			CharInt[6] = "6";
			CharInt[7] = "7";
			CharInt[8] = "8";
			CharInt[9] = "9";
			string Char = "";
			string ResStr = "";
			int StrLen = Strings.Len(Str);
			for (int i = 1; i <= StrLen; i++)
			{
				Char = Str.Substring(i - 1, Math.Min(1, Str.Length - (i - 1)));
				if (Char == "0" || Char == "1" || Char == "2" || Char == "3" || Char == "4" || Char == "5" || Char == "6" || Char == "7" || Char == "8" || Char == "9" || Char == "." || Char == ",")
				{
					if (Char == ",")
					{
						Char = ".";
					}
					ResStr = ResStr + Char;
				}
			}
			for (int i = 1; i <= StrLen; i++)
			{
				Char = Str.Substring(i - 1, Math.Min(1, Str.Length - (i - 1)));
				if (Char == "-")
				{
					ResStr = Char + ResStr;
					break;
				}
			}

			return Double.Parse(ResStr);
		}

		// Form string from string with symbols
		private string SizeToString(double x)
		{
			string result = "";
			result = StringsHelper.Format(x, "###0.0000") + Module1.DrawingUnit;
			if (Math.Abs(x) >= 1)
			{
				return StringsHelper.Format(x, "###0.00") + Module1.DrawingUnit;
			}

			if (Math.Abs(x) < 1 && Math.Abs(x) >= 0.1d)
			{
				return StringsHelper.Format(x, "###0.000") + Module1.DrawingUnit;
			}

			if (Math.Abs(x) < 0.1d && Math.Abs(x) >= 0.01d)
			{
				return StringsHelper.Format(x, "###0.000") + Module1.DrawingUnit;
			}

			if (Math.Abs(x) < 0.01d && Math.Abs(x) >= 0.001d)
			{
				return StringsHelper.Format(x, "###0.0000") + Module1.DrawingUnit;
			}

			if (Math.Abs(x) < 0.001d && Math.Abs(x) >= 0.0001d)
			{
				return StringsHelper.Format(x, "###0.00000") + Module1.DrawingUnit;
			}


			return result;
		}

		private IMSIGX.Graphic GetProperGraphic(IMSIGX.Graphic Gr)
		{

			IMSIGX.Graphic result = null;

			GXTIESLib.TieSets TSets = null;
			GXTIESLib.TieSet TSet = null;

			int Cnt = 0;

			TSets = null;
			TSet = null;

			//UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (ErrorHandler)");
			if (ReflectionHelper.GetMember<double>(Gr, "id") != 0)
			{ //It means that graphic was added to the drawing Graphics collection
				TSets = ReflectionHelper.GetMember<GXTIESLib.TieSets>(ReflectionHelper.GetMember(Gr, "Drawing"), "TieSets");
				Cnt = TSets.Count;
				for (int Ind = 0; Ind <= Cnt - 1; Ind++)
				{
					object tempRefParam = Ind;
					TSet = TSets.get_Item(ref tempRefParam);
					Ind = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam);
					if (ReflectionHelper.GetMember(Gr, "id") == ReflectionHelper.GetMember(TSet.Subject, "id"))
					{
						result = (IMSIGX.Graphic) TSet.Subordinate;
						break;
					}
ContFindNext:;
				}
			}


			TSets = null;
			TSet = null;

			return result;

ErrorHandler:
			result = null;
			//UPGRADE_TODO: (1065) Error handling statement (ContFindNext) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Label (ContFindNext)");

			return result;
		}
		static CoordReg()
		{
			Module1.TriggerClassInitialization();
		}
	}
}