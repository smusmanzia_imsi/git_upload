using Microsoft.VisualBasic;
using System;
using System.Drawing;
using System.IO;
using System.Media;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using IMSIGX;
using TCDotNetInterfaces;

namespace SurvCoord
{
	public class InsertTool : ITurboCADTool
    {
        #region ITurboCADTool Members

        //Number of tools in this server
		const int NUM_TOOLS = 1;

        
		// *******************************************************************
		//    Const EventMask = 268435456 + 536870912
		//    Const EventMask = 1024 + 268435456 + 536870912
		//    Const EventMask = 1 + 2 + 4 + 8 + 16 + 32 + 64 + 1024 + 4096 + 262144 + 268435456 + 536870912
        static readonly double EventMask = Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventBeforeExit) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventDrawingNew)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventDrawingOpen)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventDrawingActivate)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventDrawingDeactivate)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventDrawingBeforeClose)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventDrawingBeforeSave)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventMouseDown)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventMouseMove)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventRunTool)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventCancel)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventUpdateUndo)) + ReflectionHelper.GetPrimitiveValue<double>(IMSIGX.ImsiEventMask.imsiEventViewAfterRedraw));
        
		private int iConnectId = 0;
		private int iActTool = 0;
		
		private  IMSIGX.ToolEvents theToolEvents;// = null;


		// *******************************************************************
		//Toggle this to test loading buttons from .Bmp/.Res
		const bool boolLoadFromBmp = false;
		const bool boolDebug = false;
		//##########################################################################
		//##########################################################################
		//Private Declare Function TCWUndoRecordEnd Lib "TCAPI80.dll" (ByVal d As Long) As Long
		//Private Declare Function TCWUndoRecordStart Lib "TCAPI80.dll" (ByVal d As Long, ByRef Title As String) As Long
		//Private Declare Function TCWUndoRecordAddGraphic Lib "TCAPI80.dll" (ByVal d As Long, ByVal g As Long) As Long
		//Private Declare Function TCWUndo Lib "TCAPI80.dll" (ByVal N As Long) As Long

		//Private Declare Function TCWDrawingActive Lib "TCAPI80.dll" () As Long
		//Private Declare Function TCWGraphicAt Lib "TCAPI80.dll" (ByVal d As Long, ByVal Index As Long) As Long
		//Private Declare Function TCWVertexAt Lib "TCAPI80.dll" (ByVal g As Long, ByVal Index As Long) As Long

		//Private Declare Sub prVertexSetSomeFlags Lib "DBAPI80.dll" (ByVal hVer As Long, ByVal FlagOn As Long, ByVal FlagOff As Long)

		//Private Declare Function GraphicGetMatrix Lib "DBAPI80.dll" (ByVal hGr As Long) As Long
		//Private Declare Function GraphicSetMatrix Lib "DBAPI80.dll" (ByVal hGr As Long, ByVal hMat As Long) As Boolean
		//Private Declare Function MatrixEqual Lib "DBAPI80.dll" (ByVal hM1 As Long, ByVal hM2 As Long) As Boolean

		//Private Declare Function TCWViewRedraw Lib "TCAPI80.dll" () As Long

		//Private Declare Function RegenMethodDoAutoPage Lib "DBAPI80.dll" (ByVal hApp As Long, ByVal Rm As Long, ByVal lPage As Long, ByVal gr As Long, ByVal flags As Long, ByVal Children As Boolean, ByVal hWndParent As Long) As Boolean
		//Private Declare Function AppGetCurrentApp Lib "DBAPI80.dll" () As Long
		//Private Declare Function TCWGraphicHandleFromID Lib "DBAPI80.dll" (ByVal id As Long) As Long
		//Private Declare Function RegenMethodFromName Lib "DBAPI80.dll" (ByVal hApp As Long, ByVal rmName As String) As Long

		//Useful math constants


		//Private hDr As Long
		
		private IMSIGX.Graphic GrCon = null;
		
		private IMSIGX.Graphic GrSuText = null;
		
		private IMSIGX.Graphic GrDirection = null;
		
		private IMSIGX.Graphic GrText = null;
		
		private IMSIGX.GraphicSet GxSet = null;

		private int CountAdd = 0;
		private bool OneStepBack = false;
		private bool ChangeDirection = false;
		private int GrParIDBase = 0;
		private string UnitBase = "";
		
		private IMSIGX.ImsiSpaceModeType Space1 = 0;
		private int SnapModeInit = 0;

        private IMSIGX.Tool theXTool = null;
				
		//Copy a windows bitmap of the requested size to the clipboard
		//Bitmaps returned should contain NUM_TOOLS images
		//Size of entire bitmap:
		//Normal:  (NUM_TOOLS*16) wide x 15 high
		//Large:   (NUM_TOOLS*24) wide x 23 high
		//Mono bitmap should be 1-bit (black or white)
		public bool CopyBitmap(bool LargeImage, bool MonoImage)
		{
			try
			{

				Image TheImage = new Bitmap(1, 1);
				if (GetButtonPicture(LargeImage, MonoImage, ref TheImage))
				{
					//Put the image on the Windows clipboard
					Clipboard.SetData(DataFormats.Dib, TheImage);
					return true;
				}
			}
			catch
			{
			}


			return false;
		}

		//Return a Picture object for the requested size
		//Apparently, returning references to StdPicture objects doesn't work for .EXE servers
		//Bitmaps returned should contain NUM_TOOLS images
		//Size of entire image:
		//Normal:  (NUM_TOOLS*16) wide x 15 high
		//Large:   (NUM_TOOLS*24) wide x 23 high
		//Mono image should be 1-bit (black or white)
		public Image GetPicture(bool LargeImage, bool MonoImage)
		{
			try
			{

				Image TheImage = new Bitmap(1, 1);
				if (GetButtonPicture(LargeImage, MonoImage, ref TheImage))
				{
					return TheImage;
				}
			}
			catch
			{
			}


			return null;
		}

		//Implementation specific stuff
		//Private function to return the bitmap from .Res file or .Bmp file
		private bool GetButtonPicture(bool LargeImage, bool MonoImage, ref Image TheImage)
		{
			bool result = false;
			try
			{

				//There are two ways to load images:  from .Bmp file(s) or from .RES resource.
				//In this demo, we control the loading by a private variable.

				//Note that if you are loading from .Bmp, or if you are running this tool as a
				//.VBP for debugging, you must place the .Res or .Bmp files in the Draggers subdirectory
				//of the directory in which TCW40.EXE (or IMSIGX40.DLL) is located.

				string strFileName = "";
				int idBitmap = 0; //BITMAP resource id in .Res file //File name of .Bmp file to load
				if (boolLoadFromBmp)
				{
					//Load from .Bmp file

					if (LargeImage)
					{
						strFileName = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\button24.bmp"; //# NLS#'
					}
					else
					{
                        strFileName = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\button16.bmp"; //# NLS#'
					}
					TheImage = Image.FromFile(strFileName);
				}
				else
				{
					//Load from .Res file

					if (LargeImage)
					{
						idBitmap = 1002;
					}
					else
					{
						idBitmap = 1001;
					}
					TheImage = (Bitmap) App.Resources.Resources.ResourceManager.GetObject("bmp" + idBitmap.ToString());
				}

				//Return the image
				return true;
			}
			catch
			{

				if (boolDebug)
				{
					//        MsgBox "Error loading bitmap: " & Err.Description
				}
				result = false;
			}
			return result;
		}


		//Private Function GetLMPicture() As Object
		public Image GetLMPicture()
		{
			//MsgBox ("1111")
			Image TheImage = App.Resources.Resources.bmp1003;
			return TheImage;
		}
		//##########################################################################
		//##########################################################################
		//##########################################################################


		//Return a description string for this package of tools
		public string Description
		{
			get
			{
				return "SurvCoordTool"; //# NLS#'
			}
		}

		public object ViewAfterRedraw(IMSIGX.Drawing WhichDrawing, IMSIGX.View WhichView)
		{
			if (GrDirection != null)
			{
				ReflectionHelper.LetMember(GrDirection, "Visible", true);
				ReflectionHelper.Invoke(GrDirection, "Draw", new object[]{WhichView});
			}
			return null;
		}
		//Called to perform tool function
		public bool Run(IMSIGX.Tool Tool)
		{
			bool result = false;
			try
			{
				
                theXTool = Tool;
				//    theXTool.Application.ActiveDrawing.Graphics.Unselect
				Module1.Grs = ReflectionHelper.GetMember<IMSIGX.Graphics>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(theXTool, "Application"), "ActiveDrawing"), "Graphics");
				SnapModeInit = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(Module1.Grs, "Application"), "SnapModes");
				GrCon = null;
				double ViL = 0, ViH = 0, ViW = 0, ViT = 0;
				double xcV = 0, ycV = 0;
				double xcW = 0, ycW = 0;
				int SetCount = 0;
				string TextLocation = "";
				double HeightText = 0;
				int AppendLenUnit = 0;
				string LenUnit = "";
				int LenPrecision = 0;
				double TextGap = 0;
				string Prefix = "";
				string Suffix = "";
				int Col = 0;
				string FontText = "";
				int TextHorizontal = 0;
				int TextStorey = 0;
				int AngularSystem = 0;
				int LineSystem = 0;
				int ArcSystem = 0;
				object GrPar = null;
				int Space = 0;
				
				IMSIGX.Graphic GrSurPrev = null;
				
				IMSIGX.GraphicSet GrSet = null;
				if (iConnectId != -1)
				{
					ReflectionHelper.Invoke(Module1.objApp, "DisconnectEvents", new object[]{iConnectId});
					AddLM(false);
					iConnectId = -1;
					iActTool = -1;
					ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
				}
				else
				{
					Module1.Vi = ReflectionHelper.GetMember<IMSIGX.View>(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView");
					ViH = ReflectionHelper.GetMember<double>(Module1.Vi, "ViewHeight");
					ViW = ReflectionHelper.GetMember<double>(Module1.Vi, "ViewWidth");
					ViL = ReflectionHelper.GetMember<double>(Module1.Vi, "ViewLeft");
					ViT = ReflectionHelper.GetMember<double>(Module1.Vi, "ViewTop");
					xcV = ViL + ViW / 2;
					ycV = ViT - ViH / 2;
					ReflectionHelper.Invoke(ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView"), "ViewToWorld", new object[]{xcV, ycV, 0, xcW, ycW, 0});
					GrSet = ReflectionHelper.Invoke<IMSIGX.GraphicSet>(Module1.Grs, "QuerySet", new object[]{"Type=SDK_SurvCoord"}); //# NLS#'
					SetCount = ReflectionHelper.GetMember<int>(GrSet, "Count");
					if (SetCount > 0)
					{
						GrSurPrev = (IMSIGX.Graphic) GrSet.get_Item(SetCount - 1);
						TextLocation = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"TextLocation"}); //# NLS#'
						HeightText = ReflectionHelper.Invoke<double>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"HeightText"}); //# NLS#'
						AppendLenUnit = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"AppendLenUnit"}); //# NLS#'
						LenUnit = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"LenUnit"}); //# NLS#'
						LenPrecision = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"LenPrecision"}); //# NLS#'
						TextGap = ReflectionHelper.Invoke<double>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"TextGap"}); //# NLS#'
						Prefix = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"Prefix"}); //# NLS#'
						Suffix = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"Suffix"}); //# NLS#'
						Col = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"TextColor"}); //# NLS#'
						FontText = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"FontText"}); //# NLS#'
						TextHorizontal = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"TextHorizontal"}); //# NLS#'
						TextStorey = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"TextStorey"}); //# NLS#'
						AngularSystem = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"AngularSystem"}); //# NLS#'
						LineSystem = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"LineSystem"}); //# NLS#'
						ArcSystem = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GrSurPrev, "Properties"), "Item", new object[]{"ArcSystem"}); //# NLS#'
					}

					ReflectionHelper.Invoke(GrSet, "Clear", new object[]{});
					GrSet = null;
					GrSurPrev = null;

					GxSet = ReflectionHelper.Invoke<IMSIGX.GraphicSet>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "GraphicSets"), "Add", new object[]{"SurvDim", false}); //# NLS#'
					GrSuText = ReflectionHelper.Invoke<IMSIGX.Graphic>(GxSet, "Add", new object[]{null, "Surveying.CoordReg"}); //# NLS#'
					//'''''''        Set GrSuText = Grs.Add(, "Surveying.CoordReg") 'Surveying"SurvReg.CoordReg"
					if (SetCount == 0)
					{
						ReflectionHelper.LetMember(GrSuText, "Properties", ViH * 0.025d, "HeightText");
					}
					else
					{
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", TextLocation, "TextLocation"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", HeightText, "HeightText"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", AppendLenUnit, "AppendLenUnit"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", LenUnit, "LenUnit"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", LenPrecision, "LenPrecision"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", TextGap, "TextGap"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", Prefix, "Prefix"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", Suffix, "Suffix"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", Col, "TextColor"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", FontText, "FontText"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", TextHorizontal, "TextHorizontal"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", TextStorey, "TextStorey"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", AngularSystem, "AngularSystem"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", LineSystem, "LineSystem"); //# NLS#'
						ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSuText, "Properties"), "Item", ArcSystem, "ArcSystem"); //# NLS#'


					}
					ReflectionHelper.Invoke(GrSuText, "MoveAbsolute", new object[]{xcW, ycW, 0});
					ReflectionHelper.Invoke(GrSuText, "Update", new object[]{});
					ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSuText, "Graphics"), "Clear", new object[]{128});
					CountAdd = 0;
					ChangeDirection = false;
					OneStepBack = false;
					//        Set GrPar = GrSuText.Parent
					//        GrParIDBase = GrPar.id
					GrPar = null;
					//'''''''        Space = GrSuText.Drawing.Properties("TileMode") ' Modal or Paper space
					Space = ReflectionHelper.Invoke<int>(ReflectionHelper.GetMember(GxSet, "Drawing"), "Properties", new object[]{"TileMode"}); // Modal or Paper space

					//UPGRADE_WARNING: (1068) Space1 of type ImsiSpaceModeType is being forced to Scalar. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
					ReflectionHelper.SetPrimitiveValue(Space1, Space);
					if (Space != 1)
					{
						//'''''''            UnitBase = GrSuText.Drawing.Properties("PaperLinearUnitName")
						UnitBase = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(GxSet, "Drawing"), "Properties", new object[]{"PaperLinearUnitName"});
					}
					else
					{
						//'''''''            UnitBase = GrSuText.Drawing.Properties("LinearUnitName")
						UnitBase = ReflectionHelper.Invoke<string>(ReflectionHelper.GetMember(GxSet, "Drawing"), "Properties", new object[]{"LinearUnitName"});
					}
					frmSample tempLoadForm = frmSample.DefInstance;

					ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
					iConnectId = ReflectionHelper.Invoke<int>(Module1.objApp, "ConnectEvents", new object[]{this, EventMask});
					iActTool = ReflectionHelper.GetMember<int>(theXTool, "Index");
					AddLM(true);
					ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "Click on some graphic", false});
				}
				Module1.Grs = null;

				return true;
			}
			catch
			{
				//    MsgBox Err.Msg
				result = false;
				Finish();
			}
			return result;
		}




		//Fill arrays with information about tools in the package
		//Return the number of tools in the package
		public bool GetToolInfo(out TurboCADToolInfo ToolInfo)//ref string[, ] CommandNames, ref string[, ] MenuCaptions, ref string[, ] StatusPrompts, ref string[] ToolTips, ref bool[] Enabled, ref bool[] WantsUpdates)
		{

            ToolInfo = new TurboCADToolInfo();

            ToolInfo.CommandName.Insert(0,"Tools\nDotNet Tools\nSurv Tool CS");
            ToolInfo.CommandName.Insert(1, "nGeneric Properties CS");
            ToolInfo.CommandName.Insert(2, "nPolyline CS");
            ToolInfo.CommandName.Insert(3, "nInternal CS");
            ToolInfo.InternalCommand = "CMD_F06ED632_BFEE_4F88_9523_B3E713D9F9C2";
            ToolInfo.MenuCaption = "&Surv Tool CS";
            ToolInfo.ToolbarName = "Surv Tool CS";
            ToolInfo.ToolTip = "Surv Tool CS";
            ToolInfo.bEnabled = true;
            ToolInfo.bWantsUpdates = true;

            //CommandNames = ArraysHelper.InitializeArray<string[, ]>(new int[]{NUM_TOOLS + 1, 5}, new int[]{0, 0});
            //MenuCaptions = ArraysHelper.InitializeArray<string[, ]>(new int[]{NUM_TOOLS + 1, 3}, new int[]{0, 0});
            //StatusPrompts = ArraysHelper.InitializeArray<string[, ]>(new int[]{NUM_TOOLS + 1, 3}, new int[]{0, 0});
            //ToolTips = ArraysHelper.InitializeArray<string>(NUM_TOOLS + 1);
            //Enabled = new bool[NUM_TOOLS + 1];
            //WantsUpdates = new bool[NUM_TOOLS + 1];
            //CommandNames[0, 0] = "&AddOns|&Special Tools|&Insert|&Surveyor Dimension"; // TcLoadLangString(103)
            //CommandNames[0, 1] = "Generic Properties";
            //CommandNames[0, 2] = "Polyline";
            //CommandNames[0, 3] = "Internal";

            //MenuCaptions[0, 0] = "&Surveyor Dimension"; //TcLoadLangString(104)
            //MenuCaptions[0, 1] = "&Surveyor Dimension"; //TcLoadLangString(104)
            //StatusPrompts[0, 0] = "Surveyor Dimension"; //TcLoadLangString(105)
            //StatusPrompts[0, 1] = "29520";
            //// TurboCadHelpID= StatusPrompts(0, 1) + 65536
            //// StatusPrompts(0, 1) - SDKHelpID
            //// Example for offset: TurboCadHelpID = 29520 + 65536 = 95056

            //ToolTips[0] = "Surveyor Dimension"; //TcLoadLangString(104)
            //Enabled[0] = true;
            //WantsUpdates[0] = false;
            return true;

		}



		//Returns true if tool is correctly initialized
		public bool Initialize(Tool Context)
		{
			bool result = false;
			try
			{
				Module1.objApp = ReflectionHelper.GetMember<IMSIGX.Application>(theXTool, "Application");
				theToolEvents = (IMSIGX.ToolEvents) Module1.objApp;
				iConnectId = -1;
				iActTool = -1;
				result = true;

				Module1.CIRCLETYPE = "CIRCLE"; //TcLoadLangString(121)'# NLS#'
				Module1.ARCTYPE = "ARC"; //TcLoadLangString(122)'# NLS#'
				Module1.GRAPHICTYPE = "GRAPHIC"; //TcLoadLangString(123)'# NLS#'
			}
			catch
			{
				//    MsgBox Err.Description
			}

			return result;
		}

		//Returns true if tool is correctly initialized
		public bool UpdateToolStatus(Tool Context, ref bool Enabled, ref bool Checked)
		{

			
			IMSIGX.ImsiSpaceModeType SpaceCur = 0;
			if (iConnectId > -1)
			{
				
				ReflectionHelper.SetPrimitiveValue(SpaceCur, ReflectionHelper.Invoke(ReflectionHelper.GetMember(ReflectionHelper.GetMember(theXTool, "Application"), "ActiveDrawing"), "Properties", new object[]{"TileMode"}));
				if (!SpaceCur.Equals(Space1))
				{
					Finish();
				}
			}

			Enabled = true; //Could do a test here to determine whether to disable the button/menu item
			Checked = iConnectId != -1;
			return true;
		}


		public object RunTool(IMSIGX.Tool WhichTool)
		{
			Finish();
			return null;
		}



		public object BeforeExit(IMSIGX.Application WhichApplication, bool Cancel)
		{
			Finish();

			return null;
		}
		public object DrawingNew(IMSIGX.Drawing WhichDrawing)
		{

			Finish();
			return null;
		}
		public object DrawingOpen(IMSIGX.Drawing WhichDrawing)
		{
			Finish();
			return null;
		}
		public object DrawingActivate(IMSIGX.Drawing WhichDrawing)
		{
			Finish();
			return null;
		}
		public object DrawingDeactivate(IMSIGX.Drawing WhichDrawing)
		{
			Finish();
			return null;
		}
		public object DrawingBeforeClose(IMSIGX.Drawing WhichDrawing, bool Cancel)
		{
			Finish();
			return null;
		}
		public object DrawingBeforeSave(IMSIGX.Drawing WhichDrawing, bool SaveAs, bool Cancel)
		{
			Finish();
			return null;
		}



		public object MouseDown(IMSIGX.Drawing WhichDrawing, IMSIGX.View WhichView, IMSIGX.Window WhichWindow, IMSIGX.ImsiMouseButton Button, int Shift, int x, int y, ref bool Cancel)
		{
			object imsiLeftButton = null;
			double xView = 0, yView = 0;
			try
			{
				Module1.ActDr = WhichDrawing;
				Module1.Grs = ReflectionHelper.GetMember<IMSIGX.Graphics>(Module1.ActDr, "Graphics");
				Module1.Vi = WhichView;
				bool Res = false;
				ReflectionHelper.Invoke(Module1.Vi, "ScreenToView", new object[]{x, y, xView, yView});

				if (Button.Equals(imsiLeftButton))
				{
					Cancel = true;
					// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					Res = SelectCurve(xView, yView);
					if (!Res)
					{
						MessageBox.Show("The selection object is not valid for this operation.", "Surveyor Dimension");
					}
					// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
					ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "Click on graphic", false});
					Cancel = true;
					return null;

				}
				else
				{
					Cancel = false;
				}
			}
			catch
			{
				Finish();
				//   MsgBox Err.Description
			}
			return null;
		}
		public object MouseMove(IMSIGX.Drawing WhichDrawing, IMSIGX.View WhichView, IMSIGX.Window WhichWindow, int Shift, int x, int y, bool Cancel)
		{
			string UnitName = "";
			int Space = ReflectionHelper.Invoke<int>(WhichDrawing, "Properties", new object[]{"TileMode"}); // Modal or Paper space
			if (Space != 1)
			{
				UnitName = ReflectionHelper.Invoke<string>(WhichDrawing, "Properties", new object[]{"PaperLinearUnitName"});
			}
			else
			{
				UnitName = ReflectionHelper.Invoke<string>(WhichDrawing, "Properties", new object[]{"LinearUnitName"});
			}

			if (UnitName != UnitBase)
			{
				ReflectionHelper.LetMember(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes", SnapModeInit); // Nearest to graphic
				Finish();
			}
			return null;
		}

		public object Cancel(bool DoItPlease, ref bool CanCancel)
		{
			if (DoItPlease)
			{
				Finish();
			}

			CanCancel = true;
			return null;
		}

		public object UpdateUndo(ref bool AllowsUndo)
		{
			AllowsUndo = false;
			return null;
		}

		//UPGRADE_NOTE: (7001) The following declaration (AddActToolLM) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private object AddActToolLM(int iTool, bool bAdd)
		//{
			//    AddLM bAdd
			//return null;
		//}
		~InsertTool()
		{
			if (theToolEvents != null)
			{
				theToolEvents = null;
			}
			if (Module1.objApp != null)
			{
				Module1.objApp = null;
			}

		}



		private object AddLM(bool bAdd)
		{
			string[] Captions = new string[]{"", "", "", ""};
			string[] Prompts = new string[]{"", "", "", ""};
			bool[] Enabled = new bool[4];
			bool[] Checked = new bool[4];

			string[] varCaptions = null;
			string[] varPrompts = null;
			bool[] varEnabled = null;
			bool[] varChecked = null;

			if (bAdd)
			{
				Captions[0] = "Surveyor Dimension Properties";
				Prompts[0] = "Surveyor Dimension Properties";
				Enabled[0] = true;
				Checked[0] = false;

				Captions[1] = "One Step Back";
				Prompts[1] = "One Step Back";
				Enabled[1] = OneStepBack;
				Checked[1] = false;

				Captions[2] = "Change Last Segment Direction";
				Prompts[2] = "Change Last Segment Direction";
				Enabled[2] = ChangeDirection;
				Checked[2] = false;

				Captions[3] = "Finish";
				Prompts[3] = "Finish";
				Enabled[3] = true;
				Checked[3] = false;

				varCaptions = Captions;
				varPrompts = Prompts;
				varEnabled = Enabled;
				varChecked = Checked;

				ReflectionHelper.Invoke(theToolEvents, "ToolChangeCommands", new object[]{this, 4, varCaptions, varPrompts, varEnabled, varChecked, true});
			}
			else
			{
				ReflectionHelper.Invoke(theToolEvents, "ToolChangeCommands", new object[]{this, 0, null, null, null, null, false});
			}
			return null;
		}

		public object DoLMCommand(int CmdInd)
		{
			//MsgBox ("CmdInd=" & CStr(CmdInd))
			string SurvText = "";
			int Location = 0;
			string TextLocation = "";
			double HeightText = 0;
			int AppendLenUnit = 0;
			string LenUnit = "";
			int LenPrecision = 0;
			double TextGap = 0;
			string Prefix = "";
			string Suffix = "";
			int LinkSurText = 0;
			int Col = 0;
			string FontText = "";
			int TextHorizontal = 0;
			int TextStorey = 0;
			int AngularSystem = 0;
			int LineSystem = 0;
			int ArcSystem = 0;
			int Space = 0;
			
			IMSIGX.Properties Props = null;
			//UPGRADE_ISSUE: (2068) RegenMethod object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			if (CmdInd == 0)
			{
				//        hApp = 0
				//        hApp = AppGetCurrentApp()
				//        Set Rm = GrSuText.RegenType
				//        hRm = Rm.Index
				//        hGr = TCWGraphicHandleFromID(GrSuText.id)
				//        hRm = RegenMethodFromName(hApp, Rm.Name)
				//       bres = RegenMethodDoAutoPage(hApp, hRm, 0, hGr, 1, False, 0)

				Module1.FormCancel = true;
				//        Load frmSample

				// ????????????????????????????????????????????????????????????????
				if (Module1.ActDr == null)
				{
					Module1.ActDr = ReflectionHelper.GetMember<IMSIGX.Drawing>(Module1.objApp, "ActiveDrawing");
				}

				Space = ReflectionHelper.Invoke<int>(Module1.ActDr, "Properties", new object[]{"TileMode"}); // Modal or Paper space
				if (Space != 1)
				{
					Module1.DrawingUnit = ReflectionHelper.Invoke<string>(Module1.ActDr, "Properties", new object[]{"PaperLinearUnitName"});
				}
				else
				{
					Module1.DrawingUnit = ReflectionHelper.Invoke<string>(Module1.ActDr, "Properties", new object[]{"LinearUnitName"});
				}

				Props = ReflectionHelper.GetMember<IMSIGX.Properties>(GrSuText, "Properties");
				// Read Properties from Base Graphic
				for (int i = 0; i <= frmSample.DefInstance.Picture2.Length - 1; i++)
				{
					frmSample.DefInstance.Picture2[i].Visible = false;
				}

				frmSample.DefInstance.SurvText.Text = "";
				frmSample.DefInstance.Label12.Enabled = false;
				frmSample.DefInstance.SurvText.Enabled = false;
				frmSample.DefInstance.SurvText.BackColor = Color.Gray;

				Prefix = ReflectionHelper.GetPrimitiveValue<string>(Props.get_Item("Prefix")); //# NLS#'
				frmSample.DefInstance.Prefix.Text = Prefix;

				Suffix = ReflectionHelper.GetPrimitiveValue<string>(Props.get_Item("Suffix")); //# NLS#'
				frmSample.DefInstance.Suffix.Text = Suffix;

				Location = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("TextLocation")); //# NLS#'
				if (Location == 1)
				{
					frmSample.DefInstance.Location_Renamed.Text = "Above"; //# NLS#'
				}
				if (Location == 0)
				{
					frmSample.DefInstance.Location_Renamed.Text = "Middle"; //# NLS#'
				}
				if (Location == -1)
				{
					frmSample.DefInstance.Location_Renamed.Text = "Bottom"; //# NLS#'
				}

				TextGap = ReflectionHelper.GetPrimitiveValue<double>(Props.get_Item("TextGap")); //# NLS#'
				frmSample.DefInstance.TextGap.Text = StringsHelper.Format(TextGap, "###0.00") + Module1.DrawingUnit;

				HeightText = ReflectionHelper.GetPrimitiveValue<double>(Props.get_Item("HeightText")); //# NLS#'
				frmSample.DefInstance.HeightText.Text = StringsHelper.Format(HeightText, "###0.00") + Module1.DrawingUnit;

				AppendLenUnit = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("AppendLenUnit")); //# NLS#'
				//UPGRADE_WARNING: (6021) Casting 'int' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
				frmSample.DefInstance.AppendLenUnits.CheckState = (CheckState) AppendLenUnit;

				LenUnit = ReflectionHelper.GetPrimitiveValue<string>(Props.get_Item("LenUnit")); //# NLS#'
				frmSample.DefInstance.LenUnits.Text = LenUnit;

				frmSample.DefInstance.LenUnits.Enabled = AppendLenUnit == 1;

				LenPrecision = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("LenPrecision")); //# NLS#'
				frmSample.DefInstance.LenPrecision.Text = LenPrecision.ToString();
				frmSample.DefInstance.VScroll1.Value = LenPrecision;

				frmSample.DefInstance.LinkSurText.CheckState = CheckState.Checked;
				frmSample.DefInstance.LinkSurText.Enabled = false;
				frmSample.DefInstance.LenUnits.Enabled = true;
				frmSample.DefInstance.Label8.Enabled = true;

				Col = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("TextColor")); //# NLS#'
				frmSample.DefInstance.Picture1.BackColor = ColorTranslator.FromOle(Col);

				FontText = ReflectionHelper.GetPrimitiveValue<string>(Props.get_Item("FontText")); //# NLS#'
				frmSample.DefInstance.FontText.Text = FontText;

				TextHorizontal = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("TextHorizontal")); //# NLS#'
				//UPGRADE_WARNING: (6021) Casting 'int' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
				frmSample.DefInstance.TextHorizontal.CheckState = (CheckState) TextHorizontal;

				TextStorey = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("TextStorey")); //# NLS#'
				//UPGRADE_WARNING: (6021) Casting 'int' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
				frmSample.DefInstance.TextStorey.CheckState = (CheckState) TextStorey;

				AngularSystem = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("AngularSystem")); //# NLS#'
				frmSample.DefInstance.AngularSystem.SelectedIndex = AngularSystem;

				LineSystem = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("LineSystem")); //# NLS#'
				frmSample.DefInstance.LineSystem.SelectedIndex = LineSystem;

				ArcSystem = ReflectionHelper.GetPrimitiveValue<int>(Props.get_Item("ArcSystem")); //# NLS#'
				frmSample.DefInstance.ArcSystem.SelectedIndex = ArcSystem;

				//?????????????????????????????????????????????????????????????????
				//?????????????????????????????????????????????????????????????????

				frmSample.DefInstance.ShowDialog();

				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				// Write Properties from Base Graphic
				if (!Module1.FormCancel)
				{

					Prefix = frmSample.DefInstance.Prefix.Text;
					
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("Prefix"), Prefix); //# NLS#'

					Suffix = frmSample.DefInstance.Suffix.Text;
					
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("Suffix"), Suffix); //# NLS#'

					TextLocation = frmSample.DefInstance.Location_Renamed.Text;
					if (TextLocation == "Above")
					{ //# NLS#'
						Location = 1;
					}
					else
					{
						Location = -1;
					}
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("TextLocation"), Location); //# NLS#'

					TextGap = StringToSize(frmSample.DefInstance.TextGap.Text);
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("TextGap"), TextGap); //# NLS#'

					HeightText = StringToSize(frmSample.DefInstance.HeightText.Text);
					if (HeightText > 0.00001d)
					{
						 
						ReflectionHelper.SetPrimitiveValue(Props.get_Item("HeightText"), HeightText); //# NLS#'
					}

					AppendLenUnit = (int) frmSample.DefInstance.AppendLenUnits.CheckState;
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("AppendLenUnit"), AppendLenUnit); //# NLS#'

					LenUnit = frmSample.DefInstance.LenUnits.Text;
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("LenUnit"), LenUnit); //# NLS#'

					LenPrecision = Convert.ToInt32(Double.Parse(frmSample.DefInstance.LenPrecision.Text));
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("LenPrecision"), LenPrecision); //# NLS#'

					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("TextColor"), frmSample.DefInstance.Picture1.BackColor); //# NLS#'

					FontText = frmSample.DefInstance.FontText.Text;
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("FontText"), FontText); //# NLS#'

					TextHorizontal = (int) frmSample.DefInstance.TextHorizontal.CheckState;
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("TextHorizontal"), TextHorizontal); //# NLS#'

					TextStorey = (int) frmSample.DefInstance.TextStorey.CheckState;
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("TextStorey"), TextStorey); //# NLS#'

					AngularSystem = frmSample.DefInstance.AngularSystem.SelectedIndex;
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("AngularSystem"), AngularSystem); //# NLS#'

					LineSystem = frmSample.DefInstance.LineSystem.SelectedIndex;
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("LineSystem"), LineSystem); //# NLS#'

					ArcSystem = frmSample.DefInstance.ArcSystem.SelectedIndex;
					 
					ReflectionHelper.SetPrimitiveValue(Props.get_Item("ArcSystem"), ArcSystem); //# NLS#'

				}

				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

				//        Unload frmSample
				ReflectionHelper.Invoke(GrSuText, "Update", new object[]{});
				ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSuText, "Graphics"), "Clear", new object[]{128});

				//        Set Rm = Nothing
				Props = null;
			}
			if (CmdInd == 3)
			{
				if (GrDirection != null)
				{
					ReflectionHelper.LetMember(GrDirection, "Visible", false);
					ReflectionHelper.Invoke(GrDirection, "Draw", new object[]{ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView")});
					ReflectionHelper.LetMember(GrDirection, "Deleted", true);
					GrDirection = null;
				}
				Finish();

			}

			if (CmdInd == 1)
			{ // One Step Back
				if (GrDirection != null)
				{
					ReflectionHelper.LetMember(GrDirection, "Visible", false);
					ReflectionHelper.Invoke(GrDirection, "Draw", new object[]{ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView")});
					ReflectionHelper.LetMember(GrDirection, "Deleted", true);
					GrDirection = null;
				}
				if (CountAdd > 0)
				{
					CountAdd--;
					//            TCWUndo 1
					ReflectionHelper.Invoke(Module1.ActDr, "Undo", new object[]{1});
					if (CountAdd < 1)
					{
						CountAdd = 0;
						OneStepBack = false;
						ChangeDirection = false;
						AddLM(false);
						AddLM(true);
					}
				}
			}

			int Dir = 0;
			int iPrev = 0, iNext = 0;
			if (CmdInd == 2)
			{ // ChangeDirection
				if (GrDirection != null)
				{
					ReflectionHelper.LetMember(GrDirection, "Visible", false);
					ReflectionHelper.Invoke(GrDirection, "Draw", new object[]{ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView")});
					ReflectionHelper.LetMember(GrDirection, "Deleted", true);
					GrDirection = null;
				}

				if (GrText != null)
				{
					Dir = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"Direction"});
					iPrev = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"ConVertex"});
					Dir = -Dir;
					ReflectionHelper.LetMember(GrText, "Properties", Dir, "Direction");
					ReflectionHelper.Invoke(GrText, "Update", new object[]{});
					ReflectionHelper.Invoke(GrText, "Draw", new object[]{});
					if (ReflectionHelper.GetMember<string>(GrCon, "Type") == Module1.ARCTYPE)
					{
						iPrev = 1;
						iNext = 2;
					}
					else
					{
						if (!ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(GrCon, "Vertices", new object[]{iPrev + 1}), "Bulge"))
						{
							iNext = iPrev + 1;
						}
						else
						{
							iNext = iPrev + 3;
						}
					}
					CreateGrDirection(GrCon, iPrev, iNext, Dir);
					ReflectionHelper.LetMember(GrSuText, "Properties", Dir, "Direction");
					ReflectionHelper.Invoke(GrSuText, "Update", new object[]{});
					ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSuText, "Graphics"), "Clear", new object[]{128});
				}
			}

			return null;
		}
		private void Finish()
		{
			if (GrText != null)
			{
				GrText = null;
			}

			if (GrDirection != null)
			{
				ReflectionHelper.LetMember(GrDirection, "Visible", false);
				ReflectionHelper.Invoke(GrDirection, "Draw", new object[]{ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView")});
				ReflectionHelper.LetMember(GrDirection, "Deleted", true);
				GrDirection = null;
			}
			
			try
			{

				if (GrSuText != null)
				{
					ReflectionHelper.LetMember(GrSuText, "Deleted", true);
					GrSuText = null;
				}

				if (iConnectId != -1)
				{
					ReflectionHelper.Invoke(Module1.objApp, "DisconnectEvents", new object[]{iConnectId}); //????
					iConnectId = -1; //????
					ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false}); //????
					AddLM(false);
				}

				GrCon = null;
				Module1.Grs = null;
				Module1.Vi = null;
				Module1.ActDr = null;
				frmSample.DefInstance.Close();
				//   Set objApp = Nothing
			}
			catch (Exception exc)
			{
				NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}
		}




		//########################################################################
		//########################################################################
		//########################################################################
		// Select Curve at Mouse click

		private bool SelectCurve(double xCl, double yCl)
		{
			//UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
			bool result = false;
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (ErrorHandler)");
			if (GrDirection != null)
			{
				ReflectionHelper.LetMember(GrDirection, "Visible", false);
				ReflectionHelper.Invoke(GrDirection, "Draw", new object[]{ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView")});
				ReflectionHelper.LetMember(GrDirection, "Deleted", true);
				GrDirection = null;
			}

			int Space = 0;
			Space = ReflectionHelper.Invoke<int>(Module1.ActDr, "Properties", new object[]{"TileMode"});
			if (ReflectionHelper.GetPrimitiveValue<double>(Space1) != Space)
			{
				SystemSounds.Beep.Play();
				ReflectionHelper.LetMember(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes", SnapModeInit); // Nearest to graphic
				result = true;
				Finish();
				return result;
			} // If Space1 <> Space

			
			IMSIGX.PickResult PicRes = null;
			int PicCount = 0;
			ReflectionHelper.LetMember(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes", 256); // Nearest to graphic
			PicRes = ReflectionHelper.Invoke<IMSIGX.PickResult>(Module1.Vi, "PickPoint", new object[]{xCl, yCl});
			PicCount = ReflectionHelper.GetMember<int>(PicRes, "Count");

			if (PicCount == 0)
			{
				ReflectionHelper.LetMember(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes", SnapModeInit); // Nearest to graphic
				result = true;
				PicRes = null;
				return result;
			}

			string GrConType = "";
			int GrConID = 0;
			double Thick1 = 0;
			double[] ArcData = new double[10];
			int iSel = 0;
			iSel = -1;

			for (int i = 0; i <= PicCount - 1; i++)
			{
				GrCon = ReflectionHelper.GetMember<IMSIGX.Graphic>(PicRes.get_Item(i), "Graphic");

				GrConType = ReflectionHelper.GetMember<string>(GrCon, "Type");
				if (GrConType == Module1.GRAPHICTYPE || GrConType == Module1.ARCTYPE || GrConType == "TCW50Polyline" || GrConType == "TCW50IntProp")
				{ //# NLS#'
					GrConID = ReflectionHelper.GetMember<int>(GrCon, "id");
				}
				else
				{
					goto ENDSEL;
				}

				if (GrConType == Module1.GRAPHICTYPE && ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(GrCon, "Graphics"), "Count") > 0)
				{
					goto ENDSEL;
				}

				if (GrConType == Module1.GRAPHICTYPE && ReflectionHelper.Invoke<double>(Module1.ActDr, "Properties", new object[]{"TileMode"}) == 1)
				{
					Thick1 = -1000000d;
					//UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
					UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
					Thick1 = ReflectionHelper.Invoke<double>(GrCon, "Properties", new object[]{"Thickness"});
					if (Thick1 == -1000000)
					{ // For 3D-Polyline
						goto ENDSEL;
					}
				}

				if (GrConType == "TCW50Polyline")
				{ //# NLS#'
					if (ReflectionHelper.GetMember<string>(ReflectionHelper.Invoke(GrCon, "Graphics", new object[]{0}), "Name") == "ChildWithWidth")
					{ // Or GrCon.Graphics(0).Name = "Line_Width_Cosmetic" Then'# NLS#'
						goto ENDSEL;
					}
				}

				if (ReflectionHelper.Invoke<double>(Module1.ActDr, "Properties", new object[]{"TileMode"}) == 1)
				{
					Thick1 = ReflectionHelper.Invoke<double>(GrCon, "Properties", new object[]{"Thickness"});
					if (Thick1 != 0)
					{
						goto ENDSEL;
					}
				}

				if (ReflectionHelper.GetMember<bool>(GrCon, "Unbounded"))
				{
					goto ENDSEL;
				}

				if (ReflectionHelper.GetMember<string>(GrCon, "Type") == Module1.ARCTYPE)
				{
					ReflectionHelper.Invoke(GrCon, "GetArcData", new object[]{ArcData});
					if (Math.Abs(ArcData[6] - 1) > Module1.Eps)
					{
						goto ENDSEL;
					}
				}

				iSel = i;
				break;
ENDSEL:;
			}

			if (iSel == -1)
			{
				ReflectionHelper.LetMember(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes", SnapModeInit); // Nearest to graphic
				result = false;
				PicRes = null;
				return result;
			}
			GrCon = ReflectionHelper.GetMember<IMSIGX.Graphic>(PicRes.get_Item(iSel), "Graphic");
			GrConID = ReflectionHelper.GetMember<int>(GrCon, "id");

			GrText = ReflectionHelper.GetMember<IMSIGX.Graphic>(GrSuText, "Duplicate");
			ReflectionHelper.LetMember(GrText, "Layer", ReflectionHelper.GetMember(GrCon, "Layer"));
			ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSuText, "Graphics"), "Clear", new object[]{128});

			//GrText.UCS = GrCon.UCS

			double xPic = 0, yPic = 0;
			
			IMSIGX.Vertex Ver = null;
			Ver = ReflectionHelper.GetMember<IMSIGX.Vertex>(PicRes.get_Item(iSel), "ClosestVertex");
			xPic = ReflectionHelper.GetMember<double>(Ver, "x");
			yPic = ReflectionHelper.GetMember<double>(Ver, "y");

			int nV = 0;
			double xi1 = 0, xi = 0, yi = 0, yi1 = 0;
			double sina = 0, L = 0, cosa = 0;
			double yLoc = 0, xLoc = 0, yLocMin = 0;
			yLocMin = 10000000;
			int iPrev = 0;
			iPrev = 0;
			if (GrConType == Module1.GRAPHICTYPE || GrConType == "TCW50IntProp")
			{ //# NLS#'
				nV = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(GrCon, "Vertices"), "Count");
				if (nV == 2)
				{
					ReflectionHelper.LetMember(GrText, "Properties", 0, "ConVertex");
				}
				else
				{
					for (int i = 0; i <= nV - 2; i++)
					{
						xi = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i}), "x");
						yi = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i}), "y");
						xi1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i + 1}), "x");
						yi1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i + 1}), "y");
						L = Math.Sqrt((xi1 - xi) * (xi1 - xi) + (yi1 - yi) * (yi1 - yi));
						if (!(Math.Abs(L) < Module1.Eps))
						{
							sina = (yi1 - yi) / L;
							cosa = (xi1 - xi) / L;
							yLoc = (yPic - yi) * cosa - (xPic - xi) * sina;
							xLoc = (yPic - yi) * sina + (xPic - xi) * cosa;
							if (xLoc > 0 && xLoc < L && Math.Abs(yLoc) < yLocMin)
							{
								yLocMin = Math.Abs(yLoc);
								iPrev = i;
								//                        Exit For
							}
						}

					}
					ReflectionHelper.LetMember(GrText, "Properties", iPrev, "ConVertex");
				}
			}
			if (GrConType == Module1.ARCTYPE)
			{
				ReflectionHelper.LetMember(GrText, "Properties", 1, "ConVertex");
				iPrev = 1;
			}




			//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			if (GrConType == "TCW50Polyline")
			{ //# NLS#'
				iPrev = PreviousVertex(GrConType, GrCon, xPic, yPic);
				ReflectionHelper.LetMember(GrText, "Properties", iPrev, "ConVertex");
			}
			//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

			double DrBaseAngle = 0;
			DrBaseAngle = ReflectionHelper.Invoke<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrText, "Application"), "ActiveDrawing"), "Properties", new object[]{"AngleBase"});
			DrBaseAngle = DrBaseAngle / 180 * Module1.Pi + Module1.Pi / 2;

			ReflectionHelper.LetMember(GrText, "Properties", DrBaseAngle, "NorthAngle");
			//'''''''    GrText.Properties("GrConID") = GrConID
			ReflectionHelper.LetMember(GrText, "Properties", GrConType, "GrConType");
			//'''''''    GrText.Update
			ReflectionHelper.Invoke(GrText, "Draw", new object[]{ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView")});

			ReflectionHelper.LetMember(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes", SnapModeInit); // Nearest to graphic

			int iNext = 0;
			int Dir = 0;
			//    iPrev = GrText.Properties("ConVertex")
			Dir = ReflectionHelper.Invoke<int>(GrText, "Properties", new object[]{"Direction"});
			if (GrConType == Module1.ARCTYPE)
			{
				iPrev = 1;
				iNext = 2;
				CreateGrDirection(GrCon, iPrev, iNext, Dir);
			}
			else
			{
				//MsgBox ("iPrev=" & CStr(iPrev))
				if (!ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iPrev + 1}), "Bulge"))
				{
					iNext = iPrev + 1;
					CreateGrDirection(GrCon, iPrev, iNext, Dir);
				}
				else
				{
					iNext = iPrev + 3;
					CreateGrDirection(GrCon, iPrev, iNext, Dir);
				}
			}


			ReflectionHelper.Invoke(Module1.Grs, "AddGraphic", new object[]{GrText});
			
            GXTIES.TieSets TSets = null;
			//UpgradeStubs.TieSets TSets = null;
			TSets = ReflectionHelper.GetMember<UpgradeStubs.TieSets>(Module1.ActDr, "TieSets");
			ReflectionHelper.Invoke(TSets, "Add", new object[]{"Surveying.CoordTie", GrCon, GrText, 0, 0}); //# NLS#'
			ReflectionHelper.Invoke(GrText, "Draw", new object[]{});
			//'''''''    GrText.Draw Grs.Drawing.ActiveView


			// Insert NorthDirection Symbol
			//Dim GrSet As GraphicSet
			//    Set GrSet = Grs.QuerySet("Type = ""NorthDirection""")
			//Dim SetCount As Long
			//    SetCount = GrSet.Count
			//    If SetCount = 0 Then
			//Dim GrNorth As Graphic
			//Dim hGrNorth As Long
			//        Set GrNorth = Grs.Add(, "North.Direction")
			//Dim GrCount As Long
			//Dim GrChild As Graphic
			//Dim xc#, yc#
			//        xc = -111111111111#
			//        yc = -111111111111#
			//        GrCount = GrNorth.Graphics.Count
			//        For i = 0 To GrCount - 1
			//            If GrNorth.Graphics(i).Type = "CIRCLE" Then
			//                xc = GrNorth.Graphics(i).Vertices(0).x
			//                yc = GrNorth.Graphics(i).Vertices(0).y
			//            End If
			//        Next i
			//        If xc <> -111111111111# Then
			//            On Error Resume Next
			//            GrNorth.ReferencePoint.x = xc
			//            On Error Resume Next
			//            GrNorth.ReferencePoint.y = yc
			//        End If
			
			IMSIGX.UndoRecord UndoRec = null;
			//    Set UndoRec = Grs.Drawing.AddUndoRecord("NorthDirection Symbol")
			//    UndoRec.AddGraphic GrNorth
			//    UndoRec.Close


			double ViH = 0, ViTop = 0, ViLeft = 0, ViW = 0;
			ViTop = ReflectionHelper.GetMember<double>(Module1.Vi, "ViewTop");
			ViLeft = ReflectionHelper.GetMember<double>(Module1.Vi, "ViewLeft");
			ViH = ReflectionHelper.GetMember<double>(Module1.Vi, "ViewHeight");
			ViW = ReflectionHelper.GetMember<double>(Module1.Vi, "ViewWidth");
			double Vixc = 0, Viyc = 0;
			Vixc = ViLeft + ViW / 2;
			Viyc = ViTop - ViH / 2;
			double xcW = 0, ycW = 0;
			ReflectionHelper.Invoke(Module1.Vi, "ViewToWorld", new object[]{Vixc, Viyc, 0, xcW, ycW, 0});
			//Dim x0#, y0#
			//        With GrNorth.Vertices
			//            .UseWorldCS = True
			//            x0 = .Item(0).x
			//            y0 = .Item(0).y
			//        End With
			//        GrNorth.MoveRelative xcW - x0, ycW - y0, 0
			//        GrNorth.Update

			//'        hGrNorth = TCWGraphicAt(hDr, GrNorth.Index)
			//'        GraphicSetMatrix hGrNorth, hMat
			//        GrNorth.UCS = GrCon.UCS

			//Dim NorthSize#, NorthAngle#
			//        NorthSize = ViH / 20
			//        NorthAngle = Pi / 2
			//        GrNorth.Properties("NorthSize") = NorthSize
			//        GrNorth.Properties("NorthAngle") = ActDr.Properties("AngleBase") / 180 * Pi + Pi / 2 'NorthAngle
			//        GrNorth.Update
			//        GrNorth.Draw
			//    Else
			//        Set GrNorth = GrSet(0)
			//    End If
			//    TSets.Add "Surveying.CoordTie", GrNorth, GrText, 0, 0


			//'    TCWUndoRecordStart hDr, "Surveyor Coordinate"
			//'    TCWUndoRecordAddGraphic hDr, hGrText
			//'    TCWUndoRecordEnd hDr
			//''''    Grs.AddGraphic GrText
			ReflectionHelper.Invoke(GrText, "Update", new object[]{});
			ReflectionHelper.Invoke(GrText, "Draw", new object[]{});

			UndoRec = ReflectionHelper.Invoke<IMSIGX.UndoRecord>(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "AddUndoRecord", new object[]{"Surveyor Coordinate"}); //# NLS#'
			//'''''''''    UndoRec.AddGraphic GrCon
			ReflectionHelper.Invoke(UndoRec, "AddGraphic", new object[]{GrText});
			ReflectionHelper.Invoke(UndoRec, "Close", new object[]{});
			UndoRec = null;

			CountAdd++;
			OneStepBack = true;
			ChangeDirection = true;
			AddLM(false);
			AddLM(true);

			//    Set GrNorth = Nothing
			//    GrSet.Clear
			//    Set GrSet = Nothing
			PicRes = null;
			TSets = null;

			return true;
ErrorHandler:
			GrText = null;
			PicRes = null;
			TSets = null;
			ReflectionHelper.LetMember(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes", SnapModeInit); // Nearest to graphic

			return result;
		}

		// Define number of previous vertices for Base graphic

		private int PreviousVertex(string GrConType, IMSIGX.Graphic GrCon, double xPic, double yPic)
		{
			
			int result = 0;
			IMSIGX.Graphic GrDup = ReflectionHelper.GetMember<IMSIGX.Graphic>(GrCon, "Duplicate");
			ReflectionHelper.LetMember(GrDup, "Properties", 0, "PenWidth");
			ReflectionHelper.Invoke(GrDup, "Update", new object[]{});
			int nVCon = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(GrDup, "Vertices"), "Count");
			int nVCosm = ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(ReflectionHelper.Invoke(GrDup, "Graphics", new object[]{0}), "Vertices"), "Count");

			Module1.GrInfo GrConInfo = new Module1.GrInfo();
			GrConInfo.nV = nVCon;
			GrConInfo.xVer = new double[nVCon + 1];
			GrConInfo.yVer = new double[nVCon + 1];
			int[] iBaseArcBeg = new int[101];
			int[] iBaseArcEnd = new int[101];
			int ArcCount = 0;
			for (int i = 0; i <= nVCon - 1; i++)
			{
				GrConInfo.xVer[i] = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i}), "x");
				GrConInfo.yVer[i] = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i}), "y");
			}
			for (int i = 0; i <= nVCon - 2; i++)
			{
				if (!ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i}), "Bulge") && ReflectionHelper.GetMember<bool>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{i + 1}), "Bulge"))
				{
					iBaseArcBeg[ArcCount] = i;
					iBaseArcEnd[ArcCount] = i + 3;
					ArcCount++;
				}
			}

			Module1.GrInfo GrCosmInfo = new Module1.GrInfo();
			GrCosmInfo.nV = nVCosm;
			GrCosmInfo.xVer = new double[nVCosm + 1];
			GrCosmInfo.yVer = new double[nVCosm + 1];
			for (int i = 0; i <= nVCosm - 1; i++)
			{
				GrCosmInfo.xVer[i] = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(ReflectionHelper.Invoke(GrDup, "Graphics", new object[]{0}), "Vertices"), "Item", new object[]{i}), "x");
				GrCosmInfo.yVer[i] = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(ReflectionHelper.Invoke(GrDup, "Graphics", new object[]{0}), "Vertices"), "Item", new object[]{i}), "y");
			}

			int[] iCosmArcBeg = new int[101];
			int[] iCosmArcEnd = new int[101];
			double xj = 0, xi = 0, yi = 0, yj = 0;

			for (int i = 0; i <= ArcCount - 1; i++)
			{
				xi = GrConInfo.xVer[iBaseArcBeg[i]];
				yi = GrConInfo.yVer[iBaseArcBeg[i]];
				for (int j = 0; j <= nVCosm - 1; j++)
				{
					xj = GrCosmInfo.xVer[j];
					yj = GrCosmInfo.yVer[j];
					if (Math.Abs(xi - xj) < Module1.Eps && Math.Abs(yi - yj) < Module1.Eps)
					{
						iCosmArcBeg[i] = j;
						break;
					}
				}

				xi = GrConInfo.xVer[iBaseArcEnd[i]];
				yi = GrConInfo.yVer[iBaseArcEnd[i]];
				for (int j = 0; j <= nVCosm - 1; j++)
				{
					xj = GrCosmInfo.xVer[j];
					yj = GrCosmInfo.yVer[j];
					if (Math.Abs(xi - xj) < Module1.Eps && Math.Abs(yi - yj) < Module1.Eps)
					{
						iCosmArcEnd[i] = j;
						break;
					}
				}
			}


			double xi1 = 0, yi1 = 0;
			double sina = 0, L = 0, cosa = 0;
			double xLoc = 0, yLoc = 0;
			int iPrevCosm = 0, iPrev = 0;
			double yLocMin = 10000000;

			// define number of segment of the  Arc cosmetic graphic
			for (int i = 0; i <= nVCosm - 2; i++)
			{
				xi = GrCosmInfo.xVer[i];
				yi = GrCosmInfo.yVer[i];
				xi1 = GrCosmInfo.xVer[i + 1];
				yi1 = GrCosmInfo.yVer[i + 1];
				L = Math.Sqrt((xi1 - xi) * (xi1 - xi) + (yi1 - yi) * (yi1 - yi));
				if (!(Math.Abs(L) < Module1.Eps))
				{
					sina = (yi1 - yi) / L;
					cosa = (xi1 - xi) / L;
					yLoc = (yPic - yi) * cosa - (xPic - xi) * sina;
					xLoc = (yPic - yi) * sina + (xPic - xi) * cosa;
					if (xLoc > 0 && xLoc < L)
					{
						if (Math.Abs(yLoc) < yLocMin)
						{
							iPrevCosm = i;
							yLocMin = Math.Abs(yLoc);
						}
					}
				}

			}

			iPrev = -1;
			for (int i = 0; i <= ArcCount - 1; i++)
			{
				if (iPrevCosm > iCosmArcBeg[i] && iPrevCosm < iCosmArcEnd[i])
				{
					iPrev = iBaseArcBeg[i];
				}
			}

			if (iPrev == -1)
			{
				yLocMin = 10000000;

				// define number of segment of the  Arc cosmetic graphic
				for (int i = 0; i <= nVCon - 2; i++)
				{
					xi = GrConInfo.xVer[i];
					yi = GrConInfo.yVer[i];
					xi1 = GrConInfo.xVer[i + 1];
					yi1 = GrConInfo.yVer[i + 1];
					L = Math.Sqrt((xi1 - xi) * (xi1 - xi) + (yi1 - yi) * (yi1 - yi));
					if (!(Math.Abs(L) < Module1.Eps))
					{
						sina = (yi1 - yi) / L;
						cosa = (xi1 - xi) / L;
						yLoc = (yPic - yi) * cosa - (xPic - xi) * sina;
						xLoc = (yPic - yi) * sina + (xPic - xi) * cosa;
						if (xLoc > 0 && xLoc < L)
						{
							if (Math.Abs(yLoc) < yLocMin)
							{
								iPrev = i;
								yLocMin = Math.Abs(yLoc);
							}
						}
					}

				}
			}
			//MsgBox ("2222iPrev=" & CStr(iPrev))

			result = iPrev;
			ReflectionHelper.LetMember(GrDup, "Deleted", true);
			return result;
		}


		private void CreateGrDirection(IMSIGX.Graphic GrCon, int iPrev, int iNext, int Dir)
		{
			object imsiGroup = null;
			object imsiModelSpace = null;

			double x1 = 0, x0 = 0, y0 = 0, y1 = 0;
			if (Dir == 1)
			{
				x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iPrev}), "x");
				y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iPrev}), "y");
				x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iNext}), "x");
				y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iNext}), "y");
			}
			else
			{
				x0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iNext}), "x");
				y0 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iNext}), "y");
				x1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iPrev}), "x");
				y1 = ReflectionHelper.GetMember<double>(ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCon, "Vertices"), "Item", new object[]{iPrev}), "y");
			}

			double L = Math.Sqrt(Math.Pow(x1 - x0, 2) + Math.Pow(y1 - y0, 2));
			double sina = (y1 - y0) / L;
			double cosa = (x1 - x0) / L;

			
			IMSIGX.Graphic GrChild = null;
			double LDir = L / 3;
			double xE = x0 + (x1 - x0) * LDir / L;
			double yE = y0 + (y1 - y0) * LDir / L;
			//'''''''    Set GrDirection = Grs.Add(imsiGroup)
			GrDirection = ReflectionHelper.Invoke<IMSIGX.Graphic>(GxSet, "Add", new object[]{imsiGroup});
			if (ReflectionHelper.GetMember<string>(GrCon, "Type") == Module1.ARCTYPE || Math.Abs(iNext - iPrev) > 1)
			{
				GrChild = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(GrDirection, "Graphics"), "Add", new object[]{11});
				if (Space1.Equals(imsiModelSpace))
				{
					ReflectionHelper.Invoke(GrChild, "Transform", new object[]{ReflectionHelper.GetMember(GrCon, "UCS")});
				}
				ReflectionHelper.LetMember(GrChild, "Properties", Color.LightBlue, "PenColor");
				ReflectionHelper.LetMember(GrChild, "Properties", 0, "PenWidth");
				ReflectionHelper.LetMember(GrChild, "Properties", "HIDDEN", "PenStyle");
				ReflectionHelper.LetMember(GrChild, "Properties", 0.3d, "PenScale");
				ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{x0, y0, 0});
				ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{x1, y1, 0});
			}

			GrChild = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(GrDirection, "Graphics"), "AddCircleCenterAndPoint", new object[]{x0, y0, 0, x0 + LDir / 50, y0, 0});
			if (Space1.Equals(imsiModelSpace))
			{
				ReflectionHelper.Invoke(GrChild, "Transform", new object[]{ReflectionHelper.GetMember(GrCon, "UCS")});
			}
			ReflectionHelper.LetMember(GrChild, "Properties", Color.FromArgb(255, 0, 0), "PenColor");
			ReflectionHelper.LetMember(GrChild, "Properties", "Solid", "BrushStyle");
			ReflectionHelper.LetMember(GrChild, "Properties", 0.015d, "PenWidth");
			GrChild = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(GrDirection, "Graphics"), "Add", new object[]{11});
			if (Space1.Equals(imsiModelSpace))
			{
				ReflectionHelper.Invoke(GrChild, "Transform", new object[]{ReflectionHelper.GetMember(GrCon, "UCS")});
			}
			ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{x0, y0, 0});
			ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{xE, yE, 0});
			ReflectionHelper.LetMember(GrChild, "Properties", Color.FromArgb(255, 0, 0), "PenColor");
			GrChild = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(GrDirection, "Graphics"), "Add", new object[]{11});
			if (Space1.Equals(imsiModelSpace))
			{
				ReflectionHelper.Invoke(GrChild, "Transform", new object[]{ReflectionHelper.GetMember(GrCon, "UCS")});
			}
			ReflectionHelper.LetMember(GrChild, "Properties", Color.FromArgb(255, 0, 0), "PenColor");
			ReflectionHelper.LetMember(GrChild, "Properties", "Solid", "BrushStyle");
			ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{xE, yE, 0});
			double xLoc = LDir - LDir / 5;
			double yLoc = LDir / 20;
			double xi = x0 + xLoc * cosa - yLoc * sina;
			double yi = y0 + xLoc * sina + yLoc * cosa;
			ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{xi, yi, 0});

			xLoc = LDir - LDir / 5;
			yLoc = (-LDir) / 20;
			xi = x0 + xLoc * cosa - yLoc * sina;
			yi = y0 + xLoc * sina + yLoc * cosa;
			ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{xi, yi, 0});

			ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "AddClose", new object[]{});

			//    Grs.AddGraphic GrDirection
			//    Dim UndoRec As UndoRecord
			//    Set UndoRec = Grs.Drawing.AddUndoRecord("Surveyor Dim Line")
			//    UndoRec.AddGraphic GrDirection
			//    UndoRec.Close
			//    Set UndoRec = Nothing

			//'''''''    GrDirection.Draw
			ReflectionHelper.Invoke(GrDirection, "Draw", new object[]{ReflectionHelper.GetMember(ReflectionHelper.GetMember(Module1.Grs, "Drawing"), "ActiveView")});
		}


		// Form string from string with symbols
		private double StringToSize(string Str)
		{
			string[] CharInt = ArraysHelper.InitializeArray<string>(11);
			CharInt[0] = "0";
			CharInt[1] = "1";
			CharInt[2] = "2";
			CharInt[3] = "3";
			CharInt[4] = "4";
			CharInt[5] = "5";
			CharInt[6] = "6";
			CharInt[7] = "7";
			CharInt[8] = "8";
			CharInt[9] = "9";
			string Char = "";
			string ResStr = "";
			int StrLen = Strings.Len(Str);
			for (int i = 1; i <= StrLen; i++)
			{
				Char = Str.Substring(i - 1, Math.Min(1, Str.Length - (i - 1)));
				if (Char == "0" || Char == "1" || Char == "2" || Char == "3" || Char == "4" || Char == "5" || Char == "6" || Char == "7" || Char == "8" || Char == "9" || Char == "." || Char == ",")
				{
					if (Char == ",")
					{
						Char = ".";
					}
					ResStr = ResStr + Char;
				}
			}
			for (int i = 1; i <= StrLen; i++)
			{
				Char = Str.Substring(i - 1, Math.Min(1, Str.Length - (i - 1)));
				if (Char == "-")
				{
					ResStr = Char + ResStr;
					break;
				}
			}

			return Double.Parse(ResStr);
        }
        public void Terminate(Tool Context)
        {
        }
        #endregion
    }
}