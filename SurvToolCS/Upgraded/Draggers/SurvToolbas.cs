using System;

namespace SurvCoord
{
	internal static class Module1
	{

		public struct GrInfo
		{
			public int nV;
			public double[] xVer;
			public double[] yVer;
		}


		//UPGRADE_ISSUE: (2068) XApplication object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		public static IMSIGX.Application objApp = null;
		//UPGRADE_ISSUE: (2068) Drawing object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		public static IMSIGX.Drawing ActDr = null;
		//UPGRADE_ISSUE: (2068) Graphics object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		public static IMSIGX.Graphics Grs = null;
		//UPGRADE_ISSUE: (2068) View object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		public static IMSIGX.View Vi = null;

		public const double Pi = 3.14159265358979d;
		public const double Eps = 0.000001d;
		public static string DrawingUnit = "";
		public static bool FormCancel = false;

		public static string GRAPHICTYPE = "";
		public static string ARCTYPE = "";
		public static string CIRCLETYPE = "";


		internal static void Main_Renamed()
		{
		}
	}
}