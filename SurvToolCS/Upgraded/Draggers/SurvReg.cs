using System;
using System.Drawing;
using System.Windows.Forms;
using UpgradeHelpers.Gui;

namespace SurvCoord
{
	internal partial class frmSample
		: System.Windows.Forms.Form
	{

		public frmSample()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint != null && System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}




		private void cmdCancel_Click(Object eventSender, EventArgs eventArgs)
		{
			Module1.FormCancel = true;
			this.Hide();
		}

		private void cmdOK_Click(Object eventSender, EventArgs eventArgs)
		{
			Module1.FormCancel = false;
			this.Hide();
		}


		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load method and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void Form_Load()
		{
			frmSample.DefInstance.FontText.AddItem("Arial");
			frmSample.DefInstance.FontText.AddItem("Arial Baltic");
			frmSample.DefInstance.FontText.AddItem("Arial Black");
			frmSample.DefInstance.FontText.AddItem("Arial CE");
			frmSample.DefInstance.FontText.AddItem("Arial CYR");
			frmSample.DefInstance.FontText.AddItem("Arial Greek");
			frmSample.DefInstance.FontText.AddItem("Arial Narrow");
			frmSample.DefInstance.FontText.AddItem("Arial TUR");

			frmSample.DefInstance.FontText.AddItem("BankGothic Lt BT");
			frmSample.DefInstance.FontText.AddItem("BankGothic Md BT");
			frmSample.DefInstance.FontText.AddItem("Bookman Old Style");
			frmSample.DefInstance.FontText.AddItem("Bookshelf Symbol 1");
			frmSample.DefInstance.FontText.AddItem("Bookshelf Symbol 2");
			frmSample.DefInstance.FontText.AddItem("Bookshelf Symbol 3");

			frmSample.DefInstance.FontText.AddItem("CityBlueprint");
			frmSample.DefInstance.FontText.AddItem("Comic Sans MS");
			frmSample.DefInstance.FontText.AddItem("CommercialPi BT");
			frmSample.DefInstance.FontText.AddItem("CommercialScript BT");
			frmSample.DefInstance.FontText.AddItem("Complex");
			frmSample.DefInstance.FontText.AddItem("complex.shx");
			frmSample.DefInstance.FontText.AddItem("CountryBlueprint");

			frmSample.DefInstance.FontText.AddItem("Courier New");
			frmSample.DefInstance.FontText.AddItem("Courier New Baltic");
			frmSample.DefInstance.FontText.AddItem("Courier New CE");
			frmSample.DefInstance.FontText.AddItem("Courier New CYR");
			frmSample.DefInstance.FontText.AddItem("Courier New Greek");
			frmSample.DefInstance.FontText.AddItem("Courier New TUR");

			frmSample.DefInstance.FontText.AddItem("Dutch801 Rm BT");
			frmSample.DefInstance.FontText.AddItem("Dutch801 XBd BT");

			frmSample.DefInstance.FontText.AddItem("EuroRoman");

			frmSample.DefInstance.FontText.AddItem("FractionsRoman");
			frmSample.DefInstance.FontText.AddItem("FractionsSans");

			frmSample.DefInstance.FontText.AddItem("Garamond");
			frmSample.DefInstance.FontText.AddItem("GDT");
			frmSample.DefInstance.FontText.AddItem("gdt.shx");
			frmSample.DefInstance.FontText.AddItem("GothicE");
			frmSample.DefInstance.FontText.AddItem("gothice.shx");
			frmSample.DefInstance.FontText.AddItem("GothicG");
			frmSample.DefInstance.FontText.AddItem("gothicg.shx");
			frmSample.DefInstance.FontText.AddItem("GothicI");
			frmSample.DefInstance.FontText.AddItem("gothici.shx");

			frmSample.DefInstance.FontText.AddItem("GreekC");
			frmSample.DefInstance.FontText.AddItem("greekc.shx");
			frmSample.DefInstance.FontText.AddItem("GreekS");
			frmSample.DefInstance.FontText.AddItem("greeks.shx");

			frmSample.DefInstance.FontText.AddItem("Impact");
			frmSample.DefInstance.FontText.AddItem("ISOCP");
			frmSample.DefInstance.FontText.AddItem("isocp.shx");
			frmSample.DefInstance.FontText.AddItem("ISOCP2");
			frmSample.DefInstance.FontText.AddItem("isocp2.shx");
			frmSample.DefInstance.FontText.AddItem("ISOCP3");
			frmSample.DefInstance.FontText.AddItem("isocp3.shx");
			frmSample.DefInstance.FontText.AddItem("ISOCPEUR");

			frmSample.DefInstance.FontText.AddItem("ISOCT");
			frmSample.DefInstance.FontText.AddItem("isoct.shx");
			frmSample.DefInstance.FontText.AddItem("ISOCT2");
			frmSample.DefInstance.FontText.AddItem("isoct2.shx");
			frmSample.DefInstance.FontText.AddItem("ISOCT3");
			frmSample.DefInstance.FontText.AddItem("isoct3.shx");
			frmSample.DefInstance.FontText.AddItem("ISOCTEUR");

			frmSample.DefInstance.FontText.AddItem("Italic");
			frmSample.DefInstance.FontText.AddItem("italic.shx");
			frmSample.DefInstance.FontText.AddItem("ItalicC");
			frmSample.DefInstance.FontText.AddItem("italicc.shx");
			frmSample.DefInstance.FontText.AddItem("ItalicT");
			frmSample.DefInstance.FontText.AddItem("italict.shx");

			frmSample.DefInstance.FontText.AddItem("LetterGothic");
			frmSample.DefInstance.FontText.AddItem("Lucida Concole");

			frmSample.DefInstance.FontText.AddItem("Marlett");
			frmSample.DefInstance.FontText.AddItem("Monotxt");
			frmSample.DefInstance.FontText.AddItem("monotxt.shx");
			frmSample.DefInstance.FontText.AddItem("MS Outlook");

			frmSample.DefInstance.FontText.AddItem("PanRoman");
			frmSample.DefInstance.FontText.AddItem("Proxy 1");
			frmSample.DefInstance.FontText.AddItem("Proxy 2");
			frmSample.DefInstance.FontText.AddItem("Proxy 3");
			frmSample.DefInstance.FontText.AddItem("Proxy 4");
			frmSample.DefInstance.FontText.AddItem("Proxy 5");
			frmSample.DefInstance.FontText.AddItem("Proxy 6");
			frmSample.DefInstance.FontText.AddItem("Proxy 7");
			frmSample.DefInstance.FontText.AddItem("Proxy 8");
			frmSample.DefInstance.FontText.AddItem("Proxy 9");

			frmSample.DefInstance.FontText.AddItem("RomanC");
			frmSample.DefInstance.FontText.AddItem("romanc.shx");
			frmSample.DefInstance.FontText.AddItem("RomanD");
			frmSample.DefInstance.FontText.AddItem("romand.shx");
			frmSample.DefInstance.FontText.AddItem("RomanS");
			frmSample.DefInstance.FontText.AddItem("romans.shx");
			frmSample.DefInstance.FontText.AddItem("RomanT");
			frmSample.DefInstance.FontText.AddItem("romant.shx");

			frmSample.DefInstance.FontText.AddItem("SansSerif");

			frmSample.DefInstance.FontText.AddItem("ScriptC");
			frmSample.DefInstance.FontText.AddItem("scriptc.shx");
			frmSample.DefInstance.FontText.AddItem("ScriptS");
			frmSample.DefInstance.FontText.AddItem("scripts.shx");

			frmSample.DefInstance.FontText.AddItem("Simplex");
			frmSample.DefInstance.FontText.AddItem("simplex.shx");

			frmSample.DefInstance.FontText.AddItem("Swis 721 Blk BT");

			frmSample.DefInstance.FontText.AddItem("Symap");
			frmSample.DefInstance.FontText.AddItem("Symath");
			frmSample.DefInstance.FontText.AddItem("Symbol");

			frmSample.DefInstance.FontText.AddItem("Tahoma");
			frmSample.DefInstance.FontText.AddItem("Technic");
			frmSample.DefInstance.FontText.AddItem("TechnicBold");
			frmSample.DefInstance.FontText.AddItem("TechnicLite");

			frmSample.DefInstance.FontText.AddItem("Times New Roman");
			frmSample.DefInstance.FontText.AddItem("Times New Roman Baltic");
			frmSample.DefInstance.FontText.AddItem("Times New Roman CE");
			frmSample.DefInstance.FontText.AddItem("Times New Roman CYR");
			frmSample.DefInstance.FontText.AddItem("Times New Roman Greek");
			frmSample.DefInstance.FontText.AddItem("Times New Roman TUR");

			frmSample.DefInstance.FontText.AddItem("Txt");
			frmSample.DefInstance.FontText.AddItem("txt.shx");

			frmSample.DefInstance.FontText.AddItem("Vendana");
			frmSample.DefInstance.FontText.AddItem("Webdings");

			frmSample.DefInstance.LenUnits.AddItem("in");
			frmSample.DefInstance.LenUnits.AddItem("\"");
			frmSample.DefInstance.LenUnits.AddItem("'");
			frmSample.DefInstance.LenUnits.AddItem("ft");
			frmSample.DefInstance.LenUnits.AddItem("yd");
			frmSample.DefInstance.LenUnits.AddItem("mi");
			frmSample.DefInstance.LenUnits.AddItem("mm");
			frmSample.DefInstance.LenUnits.AddItem("cm");
			frmSample.DefInstance.LenUnits.AddItem("m");
			frmSample.DefInstance.LenUnits.AddItem("km");
			frmSample.DefInstance.Location_Renamed.AddItem("Above");
			frmSample.DefInstance.Location_Renamed.AddItem("Bottom");
			if (LinkSurText.CheckState == CheckState.Unchecked)
			{
				AppendLenUnits.Enabled = false;
				LenUnits.Enabled = false;
				Label8.Enabled = false;
				LenPrecision.Enabled = false;
				Label9.Enabled = false;
				VScroll1.Enabled = false;
				SurvText.Enabled = true;
			}
			else
			{
				AppendLenUnits.Enabled = true;
				LenUnits.Enabled = true;
				Label8.Enabled = true;
				LenPrecision.Enabled = true;
				Label9.Enabled = true;
				VScroll1.Enabled = true;
				SurvText.Enabled = false;
			}

			if (AppendLenUnits.CheckState == CheckState.Checked)
			{
				Label8.Enabled = true;
				LenUnits.Enabled = true;
			}
			else
			{
				Label8.Enabled = false;
				LenUnits.Enabled = false;
			}


			for (int i = 0; i <= 34; i++)
			{
				Picture2[i].Visible = false;
			}

			frmSample.DefInstance.AngularSystem.Items.Clear();
			frmSample.DefInstance.AngularSystem.AddItem("Degrees-Minutes-Seconds");
			frmSample.DefInstance.AngularSystem.AddItem("Degrees-Minutes");
			frmSample.DefInstance.AngularSystem.AddItem("Degrees");
			frmSample.DefInstance.AngularSystem.SelectedIndex = 0;

			frmSample.DefInstance.LineSystem.Items.Clear();
			frmSample.DefInstance.LineSystem.AddItem("Bearing-Distance");
			frmSample.DefInstance.LineSystem.AddItem("Bearing");
			frmSample.DefInstance.LineSystem.AddItem("Distance");
			frmSample.DefInstance.LineSystem.SelectedIndex = 0;

			frmSample.DefInstance.ArcSystem.Items.Clear();
			frmSample.DefInstance.ArcSystem.AddItem("Angle-Radius-Length");
			frmSample.DefInstance.ArcSystem.AddItem("Angle-Radius");
			frmSample.DefInstance.ArcSystem.AddItem("Angle-Length");
			frmSample.DefInstance.ArcSystem.AddItem("Radius-Length");
			frmSample.DefInstance.ArcSystem.AddItem("ChordBearing-ChordDistance");
			frmSample.DefInstance.ArcSystem.AddItem("ChordBearing");
			frmSample.DefInstance.ArcSystem.AddItem("ChordDistance");
			frmSample.DefInstance.ArcSystem.SelectedIndex = 0;


		}

		//UPGRADE_WARNING: (2074) ComboBox event LenUnits.Change was upgraded to LenUnits.TextChanged which has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2074.aspx
		private bool isInitializingComponent;
		private void LenUnits_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			string LenUnit = LenUnits.Text;
			double UnitScale = 1;
			if (LenUnit == "in" || LenUnit == "\"" || LenUnit == "ft" || LenUnit == "'" || LenUnit == "yd" || LenUnit == "mi" || LenUnit == "mm" || LenUnit == "cm" || LenUnit == "m" || LenUnit == "km")
			{
				UnitScale = UnitToUnit(LenUnit, Module1.DrawingUnit);
			}


		}

		private void LinkSurText_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (LinkSurText.CheckState == CheckState.Unchecked)
			{
				AppendLenUnits.Enabled = false;
				LenUnits.Enabled = false;
				LenPrecision.Enabled = false;
				Label8.Enabled = false;
				Label9.Enabled = false;
				VScroll1.Enabled = false;
				SurvText.Enabled = true;
			}
			else
			{
				AppendLenUnits.Enabled = true;
				if (AppendLenUnits.CheckState == CheckState.Checked)
				{
					Label8.Enabled = true;
					LenUnits.Enabled = true;
				}
				else
				{
					Label8.Enabled = false;
					LenUnits.Enabled = false;
				}
				LenPrecision.Enabled = true;
				Label9.Enabled = true;
				VScroll1.Enabled = true;
				SurvText.Enabled = false;
			}
		}

		private void Picture1_Click(Object eventSender, EventArgs eventArgs)
		{
			for (int i = 0; i <= 34; i++)
			{
				Picture2[i].Visible = true;
			}
		}

		private void Picture2_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.Picture2, eventSender);
			Picture1.BackColor = Picture2[Index].BackColor;
			for (int i = 0; i <= 34; i++)
			{
				Picture2[i].Visible = false;
			}
		}

		private void AppendLenUnits_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (AppendLenUnits.CheckState == CheckState.Checked)
			{
				Label8.Enabled = true;
				LenUnits.Enabled = true;
			}
			else
			{
				Label8.Enabled = false;
				LenUnits.Enabled = false;
			}
		}

		private void VScroll1_ValueChanged(Object eventSender, EventArgs eventArgs)
		{
			LenPrecision.Text = VScroll1.Value.ToString();
		}

		//Calculate the Units scale if it changes

		private double UnitToUnit(string UnitName, string UnitNameOld)
		{
			double result = 0;
			result = 1d;
			//MsgBox ("UnitNameOld=" & UnitNameOld & "   UnitNameNew=" & UnitName)
			if (UnitNameOld == "in" || UnitNameOld == "\"")
			{
				if (UnitName == "\"" || UnitName == "in")
				{
					return 1d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1d / 36d;
				}
				if (UnitName == "mi")
				{
					return 1d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 25.4d;
				}
				if (UnitName == "cm")
				{
					return 2.54d;
				}
				if (UnitName == "m")
				{
					return 2.54d / 100d;
				}
				if (UnitName == "km")
				{
					return 2.54d / 100d / 1000d;
				}
			}


			if (UnitNameOld == "ft" || UnitNameOld == "'")
			{
				if (UnitName == "\"" || UnitName == "in")
				{
					return 12d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1d;
				}
				if (UnitName == "yd")
				{
					return 1d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 12d * 25.4d;
				}
				if (UnitName == "cm")
				{
					return 12d * 2.54d;
				}
				if (UnitName == "m")
				{
					return 12d * 2.54d / 100d;
				}
				if (UnitName == "km")
				{
					return 12d * 2.54d / 100d / 1000d;
				}
			}

			if (UnitNameOld == "yd")
			{
				if (UnitName == "\"" || UnitName == "in")
				{
					return 3d * 12d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 3d;
				}
				if (UnitName == "yd")
				{
					return 1d;
				}
				if (UnitName == "mi")
				{
					return 1d / 5280d / 3d;
				}
				if (UnitName == "mm")
				{
					return 3d * 12d * 25.4d;
				}
				if (UnitName == "cm")
				{
					return 3d * 12d * 2.54d;
				}
				if (UnitName == "m")
				{
					return 3d * 12d * 2.54d / 100d;
				}
				if (UnitName == "km")
				{
					return 3d * 12d * 2.54d / 100d / 1000d;
				}
			}
			// Add Miles
			//########################################################

			if (UnitNameOld == "mi")
			{
				if (UnitName == "\"" || UnitName == "in")
				{
					return 5280d * 12d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 5280d;
				}
				if (UnitName == "yd")
				{
					return 1760d;
				}
				if (UnitName == "mi")
				{
					return 1d;
				}
				if (UnitName == "mm")
				{
					return 1.609344d * 1000d * 1000d;
				}
				if (UnitName == "cm")
				{
					return 1.609344d * 1000d * 100d;
				}
				if (UnitName == "m")
				{
					return 1.609344d * 1000d;
				}
				if (UnitName == "km")
				{
					return 1.609344d;
				}
			}
			//########################################################




			if (UnitNameOld == "mm")
			{
				if (UnitName == "\"" || UnitName == "in")
				{
					return 1d / 25.4d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1d / 25.4d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1d / 25.4d / 12d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1d / 25.4d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 1d;
				}
				if (UnitName == "cm")
				{
					return 1d / 10d;
				}
				if (UnitName == "m")
				{
					return 1d / 1000d;
				}
				if (UnitName == "km")
				{
					return 1d / 1000d / 1000d;
				}
			}

			if (UnitNameOld == "cm")
			{
				if (UnitName == "\"" || UnitName == "in")
				{
					return 1d / 2.54d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1d / 2.54d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1d / 2.54d / 12d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1d / 2.54d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 10d;
				}
				if (UnitName == "cm")
				{
					return 1d;
				}
				if (UnitName == "m")
				{
					return 1d / 100d;
				}
				if (UnitName == "km")
				{
					return 1d / 100d / 1000d;
				}
			}


			if (UnitNameOld == "m")
			{
				if (UnitName == "\"" || UnitName == "in")
				{
					return 1000d / 25.4d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1000d / 25.4d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1000d / 25.4d / 12d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1000d / 25.4d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 1000d;
				}
				if (UnitName == "cm")
				{
					return 100d;
				}
				if (UnitName == "m")
				{
					return 1d;
				}
				if (UnitName == "km")
				{
					return 1d / 1000d;
				}
			}

			if (UnitNameOld == "km")
			{
				if (UnitName == "\"" || UnitName == "in")
				{
					return 1000d * 1000d / 25.4d;
				}
				if (UnitName == "ft" || UnitName == "'")
				{
					return 1000d * 1000d / 25.4d / 12d;
				}
				if (UnitName == "yd")
				{
					return 1000d * 1000d / 25.4d / 12d / 3d;
				}
				if (UnitName == "mi")
				{
					return 1000d * 1000d / 25.4d / 12d / 5280d;
				}
				if (UnitName == "mm")
				{
					return 1000d * 1000d;
				}
				if (UnitName == "cm")
				{
					return 100d * 1000d;
				}
				if (UnitName == "m")
				{
					return 1d * 1000d;
				}
				if (UnitName == "km")
				{
					return 1d;
				}
			}

			return 1;

		}
		private void frmSample_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}