using System.Runtime.InteropServices;

namespace SurvToolSupport.PInvoke.SafeNative
{
	public static class langextvb
	{

		public static object LoadLangBSTRString(ref string strModule, int nID, int wLanguage)
		{
			return SurvToolSupport.PInvoke.UnsafeNative.langextvb.LoadLangBSTRString(ref strModule, nID, wLanguage);
		}
	}
}