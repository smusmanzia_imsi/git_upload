
namespace Surveying
{
	partial class frmSample
	{

		#region "Upgrade Support "
		private static frmSample m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmSample DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = CreateInstance();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		public static frmSample CreateInstance()
		{
			frmSample theInstance = new frmSample();
			theInstance.Form_Load();
			return theInstance;
		}
		private string[] visualControls = new string[]{"components", "ToolTipMain", "ArcSystem", "LineSystem", "_Picture2_39", "_Picture2_38", "_Picture2_37", "_Picture2_36", "_Picture2_35", "AngularSystem", "TextStorey", "TextHorizontal", "ChangeDirection", "_Picture2_34", "_Picture2_33", "_Picture2_32", "_Picture2_31", "_Picture2_30", "_Picture2_29", "_Picture2_28", "_Picture2_27", "_Picture2_26", "_Picture2_25", "_Picture2_24", "_Picture2_23", "_Picture2_22", "_Picture2_21", "_Picture2_20", "_Picture2_19", "_Picture2_18", "_Picture2_17", "_Picture2_16", "_Picture2_15", "_Picture2_14", "_Picture2_13", "_Picture2_12", "_Picture2_11", "_Picture2_10", "_Picture2_9", "_Picture2_8", "_Picture2_7", "_Picture2_6", "_Picture2_5", "_Picture2_4", "_Picture2_3", "_Picture2_2", "_Picture2_1", "_Picture2_0", "Picture1", "Suffix", "Prefix", "LinkSurText", "LenPrecision", "VScroll1", "LenUnits", "AppendLenUnits", "TextGap", "FontText", "HeightText", "Location_Renamed", "SurvText", "cmdOK", "cmdCancel", "Label16", "Label15", "Line6", "Label14", "Label13", "Label12", "Label11", "Label10", "Label9", "Label8", "Label7", "Line3", "Label6", "Label5", "Line2", "Label4", "Label3", "Line1", "Label2", "Label1", "Picture2", "listBoxComboBoxHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public System.Windows.Forms.ComboBox ArcSystem;
		public System.Windows.Forms.ComboBox LineSystem;
		private System.Windows.Forms.PictureBox _Picture2_39;
		private System.Windows.Forms.PictureBox _Picture2_38;
		private System.Windows.Forms.PictureBox _Picture2_37;
		private System.Windows.Forms.PictureBox _Picture2_36;
		private System.Windows.Forms.PictureBox _Picture2_35;
		public System.Windows.Forms.ComboBox AngularSystem;
		public System.Windows.Forms.CheckBox TextStorey;
		public System.Windows.Forms.CheckBox TextHorizontal;
		public System.Windows.Forms.RadioButton ChangeDirection;
		private System.Windows.Forms.PictureBox _Picture2_34;
		private System.Windows.Forms.PictureBox _Picture2_33;
		private System.Windows.Forms.PictureBox _Picture2_32;
		private System.Windows.Forms.PictureBox _Picture2_31;
		private System.Windows.Forms.PictureBox _Picture2_30;
		private System.Windows.Forms.PictureBox _Picture2_29;
		private System.Windows.Forms.PictureBox _Picture2_28;
		private System.Windows.Forms.PictureBox _Picture2_27;
		private System.Windows.Forms.PictureBox _Picture2_26;
		private System.Windows.Forms.PictureBox _Picture2_25;
		private System.Windows.Forms.PictureBox _Picture2_24;
		private System.Windows.Forms.PictureBox _Picture2_23;
		private System.Windows.Forms.PictureBox _Picture2_22;
		private System.Windows.Forms.PictureBox _Picture2_21;
		private System.Windows.Forms.PictureBox _Picture2_20;
		private System.Windows.Forms.PictureBox _Picture2_19;
		private System.Windows.Forms.PictureBox _Picture2_18;
		private System.Windows.Forms.PictureBox _Picture2_17;
		private System.Windows.Forms.PictureBox _Picture2_16;
		private System.Windows.Forms.PictureBox _Picture2_15;
		private System.Windows.Forms.PictureBox _Picture2_14;
		private System.Windows.Forms.PictureBox _Picture2_13;
		private System.Windows.Forms.PictureBox _Picture2_12;
		private System.Windows.Forms.PictureBox _Picture2_11;
		private System.Windows.Forms.PictureBox _Picture2_10;
		private System.Windows.Forms.PictureBox _Picture2_9;
		private System.Windows.Forms.PictureBox _Picture2_8;
		private System.Windows.Forms.PictureBox _Picture2_7;
		private System.Windows.Forms.PictureBox _Picture2_6;
		private System.Windows.Forms.PictureBox _Picture2_5;
		private System.Windows.Forms.PictureBox _Picture2_4;
		private System.Windows.Forms.PictureBox _Picture2_3;
		private System.Windows.Forms.PictureBox _Picture2_2;
		private System.Windows.Forms.PictureBox _Picture2_1;
		private System.Windows.Forms.PictureBox _Picture2_0;
		public System.Windows.Forms.PictureBox Picture1;
		public System.Windows.Forms.TextBox Suffix;
		public System.Windows.Forms.TextBox Prefix;
		public System.Windows.Forms.CheckBox LinkSurText;
		public System.Windows.Forms.TextBox LenPrecision;
		public System.Windows.Forms.VScrollBar VScroll1;
		public System.Windows.Forms.ComboBox LenUnits;
		public System.Windows.Forms.CheckBox AppendLenUnits;
		public System.Windows.Forms.TextBox TextGap;
		public System.Windows.Forms.ComboBox FontText;
		public System.Windows.Forms.TextBox HeightText;
		public System.Windows.Forms.ComboBox Location_Renamed;
		public System.Windows.Forms.TextBox SurvText;
		public System.Windows.Forms.Button cmdOK;
		public System.Windows.Forms.Button cmdCancel;
		public System.Windows.Forms.Label Label16;
		public System.Windows.Forms.Label Label15;
		public System.Windows.Forms.Label Line6;
		public System.Windows.Forms.Label Label14;
		public System.Windows.Forms.Label Label13;
		public System.Windows.Forms.Label Label12;
		public System.Windows.Forms.Label Label11;
		public System.Windows.Forms.Label Label10;
		public System.Windows.Forms.Label Label9;
		public System.Windows.Forms.Label Label8;
		public System.Windows.Forms.Label Label7;
		public System.Windows.Forms.Label Line3;
		public System.Windows.Forms.Label Label6;
		public System.Windows.Forms.Label Label5;
		public System.Windows.Forms.Label Line2;
		public System.Windows.Forms.Label Label4;
		public System.Windows.Forms.Label Label3;
		public System.Windows.Forms.Label Line1;
		public System.Windows.Forms.Label Label2;
		public System.Windows.Forms.Label Label1;
		public System.Windows.Forms.PictureBox[] Picture2 = new System.Windows.Forms.PictureBox[40];
		private UpgradeHelpers.Gui.ListControlHelper listBoxComboBoxHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSample));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.ArcSystem = new System.Windows.Forms.ComboBox();
			this.LineSystem = new System.Windows.Forms.ComboBox();
			this._Picture2_39 = new System.Windows.Forms.PictureBox();
			this._Picture2_38 = new System.Windows.Forms.PictureBox();
			this._Picture2_37 = new System.Windows.Forms.PictureBox();
			this._Picture2_36 = new System.Windows.Forms.PictureBox();
			this._Picture2_35 = new System.Windows.Forms.PictureBox();
			this.AngularSystem = new System.Windows.Forms.ComboBox();
			this.TextStorey = new System.Windows.Forms.CheckBox();
			this.TextHorizontal = new System.Windows.Forms.CheckBox();
			this.ChangeDirection = new System.Windows.Forms.RadioButton();
			this._Picture2_34 = new System.Windows.Forms.PictureBox();
			this._Picture2_33 = new System.Windows.Forms.PictureBox();
			this._Picture2_32 = new System.Windows.Forms.PictureBox();
			this._Picture2_31 = new System.Windows.Forms.PictureBox();
			this._Picture2_30 = new System.Windows.Forms.PictureBox();
			this._Picture2_29 = new System.Windows.Forms.PictureBox();
			this._Picture2_28 = new System.Windows.Forms.PictureBox();
			this._Picture2_27 = new System.Windows.Forms.PictureBox();
			this._Picture2_26 = new System.Windows.Forms.PictureBox();
			this._Picture2_25 = new System.Windows.Forms.PictureBox();
			this._Picture2_24 = new System.Windows.Forms.PictureBox();
			this._Picture2_23 = new System.Windows.Forms.PictureBox();
			this._Picture2_22 = new System.Windows.Forms.PictureBox();
			this._Picture2_21 = new System.Windows.Forms.PictureBox();
			this._Picture2_20 = new System.Windows.Forms.PictureBox();
			this._Picture2_19 = new System.Windows.Forms.PictureBox();
			this._Picture2_18 = new System.Windows.Forms.PictureBox();
			this._Picture2_17 = new System.Windows.Forms.PictureBox();
			this._Picture2_16 = new System.Windows.Forms.PictureBox();
			this._Picture2_15 = new System.Windows.Forms.PictureBox();
			this._Picture2_14 = new System.Windows.Forms.PictureBox();
			this._Picture2_13 = new System.Windows.Forms.PictureBox();
			this._Picture2_12 = new System.Windows.Forms.PictureBox();
			this._Picture2_11 = new System.Windows.Forms.PictureBox();
			this._Picture2_10 = new System.Windows.Forms.PictureBox();
			this._Picture2_9 = new System.Windows.Forms.PictureBox();
			this._Picture2_8 = new System.Windows.Forms.PictureBox();
			this._Picture2_7 = new System.Windows.Forms.PictureBox();
			this._Picture2_6 = new System.Windows.Forms.PictureBox();
			this._Picture2_5 = new System.Windows.Forms.PictureBox();
			this._Picture2_4 = new System.Windows.Forms.PictureBox();
			this._Picture2_3 = new System.Windows.Forms.PictureBox();
			this._Picture2_2 = new System.Windows.Forms.PictureBox();
			this._Picture2_1 = new System.Windows.Forms.PictureBox();
			this._Picture2_0 = new System.Windows.Forms.PictureBox();
			this.Picture1 = new System.Windows.Forms.PictureBox();
			this.Suffix = new System.Windows.Forms.TextBox();
			this.Prefix = new System.Windows.Forms.TextBox();
			this.LinkSurText = new System.Windows.Forms.CheckBox();
			this.LenPrecision = new System.Windows.Forms.TextBox();
			this.VScroll1 = new System.Windows.Forms.VScrollBar();
			this.LenUnits = new System.Windows.Forms.ComboBox();
			this.AppendLenUnits = new System.Windows.Forms.CheckBox();
			this.TextGap = new System.Windows.Forms.TextBox();
			this.FontText = new System.Windows.Forms.ComboBox();
			this.HeightText = new System.Windows.Forms.TextBox();
			this.Location_Renamed = new System.Windows.Forms.ComboBox();
			this.SurvText = new System.Windows.Forms.TextBox();
			this.cmdOK = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.Label16 = new System.Windows.Forms.Label();
			this.Label15 = new System.Windows.Forms.Label();
			this.Line6 = new System.Windows.Forms.Label();
			this.Label14 = new System.Windows.Forms.Label();
			this.Label13 = new System.Windows.Forms.Label();
			this.Label12 = new System.Windows.Forms.Label();
			this.Label11 = new System.Windows.Forms.Label();
			this.Label10 = new System.Windows.Forms.Label();
			this.Label9 = new System.Windows.Forms.Label();
			this.Label8 = new System.Windows.Forms.Label();
			this.Label7 = new System.Windows.Forms.Label();
			this.Line3 = new System.Windows.Forms.Label();
			this.Label6 = new System.Windows.Forms.Label();
			this.Label5 = new System.Windows.Forms.Label();
			this.Line2 = new System.Windows.Forms.Label();
			this.Label4 = new System.Windows.Forms.Label();
			this.Label3 = new System.Windows.Forms.Label();
			this.Line1 = new System.Windows.Forms.Label();
			this.Label2 = new System.Windows.Forms.Label();
			this.Label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			this.listBoxComboBoxHelper1 = new UpgradeHelpers.Gui.ListControlHelper(this.components);
			((System.ComponentModel.ISupportInitialize) this.listBoxComboBoxHelper1).BeginInit();
			// 
			// ArcSystem
			// 
			this.ArcSystem.BackColor = System.Drawing.SystemColors.Window;
			this.ArcSystem.CausesValidation = true;
			this.ArcSystem.Cursor = System.Windows.Forms.Cursors.Default;
			this.ArcSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.ArcSystem.Enabled = true;
			this.ArcSystem.ForeColor = System.Drawing.SystemColors.WindowText;
			this.ArcSystem.IntegralHeight = true;
			this.ArcSystem.Location = new System.Drawing.Point(104, 128);
			this.ArcSystem.Name = "ArcSystem";
			this.ArcSystem.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ArcSystem.Size = new System.Drawing.Size(193, 20);
			this.ArcSystem.Sorted = false;
			this.ArcSystem.TabIndex = 76;
			this.ArcSystem.TabStop = true;
			this.ArcSystem.Text = "Angle-Radius-Length";
			this.ArcSystem.Visible = true;
			this.ArcSystem.Items.AddRange(new object[]{"Angle-Radius-Length", "Angle-Radius", "Angle-Length", "Radius-Length", "ChordBearing-ChordDistance", "ChordBearing", "ChordDistance"});
			// 
			// LineSystem
			// 
			this.LineSystem.BackColor = System.Drawing.SystemColors.Window;
			this.LineSystem.CausesValidation = true;
			this.LineSystem.Cursor = System.Windows.Forms.Cursors.Default;
			this.LineSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.LineSystem.Enabled = true;
			this.LineSystem.ForeColor = System.Drawing.SystemColors.WindowText;
			this.LineSystem.IntegralHeight = true;
			this.LineSystem.Location = new System.Drawing.Point(104, 104);
			this.LineSystem.Name = "LineSystem";
			this.LineSystem.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LineSystem.Size = new System.Drawing.Size(193, 20);
			this.LineSystem.Sorted = false;
			this.LineSystem.TabIndex = 74;
			this.LineSystem.TabStop = true;
			this.LineSystem.Text = "Bearing-Distance";
			this.LineSystem.Visible = true;
			this.LineSystem.Items.AddRange(new object[]{"Bearing-Distance", "Bearing", "Distance"});
			// 
			// _Picture2_39
			// 
			this._Picture2_39.BackColor = System.Drawing.Color.FromArgb(64, 64, 64);
			this._Picture2_39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_39.CausesValidation = true;
			this._Picture2_39.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_39.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_39.Enabled = true;
			this._Picture2_39.Location = new System.Drawing.Point(176, 400);
			this._Picture2_39.Name = "_Picture2_39";
			this._Picture2_39.Size = new System.Drawing.Size(17, 17);
			this._Picture2_39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_39.TabIndex = 72;
			this._Picture2_39.TabStop = true;
			this._Picture2_39.Visible = true;
			this._Picture2_39.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_38
			// 
			this._Picture2_38.BackColor = System.Drawing.Color.Maroon;
			this._Picture2_38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_38.CausesValidation = true;
			this._Picture2_38.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_38.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_38.Enabled = true;
			this._Picture2_38.Location = new System.Drawing.Point(192, 400);
			this._Picture2_38.Name = "_Picture2_38";
			this._Picture2_38.Size = new System.Drawing.Size(17, 17);
			this._Picture2_38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_38.TabIndex = 71;
			this._Picture2_38.TabStop = true;
			this._Picture2_38.Visible = true;
			this._Picture2_38.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_37
			// 
			this._Picture2_37.BackColor = System.Drawing.Color.FromArgb(128, 64, 0);
			this._Picture2_37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_37.CausesValidation = true;
			this._Picture2_37.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_37.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_37.Enabled = true;
			this._Picture2_37.Location = new System.Drawing.Point(208, 400);
			this._Picture2_37.Name = "_Picture2_37";
			this._Picture2_37.Size = new System.Drawing.Size(17, 17);
			this._Picture2_37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_37.TabIndex = 70;
			this._Picture2_37.TabStop = true;
			this._Picture2_37.Visible = true;
			this._Picture2_37.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_36
			// 
			this._Picture2_36.BackColor = System.Drawing.Color.Olive;
			this._Picture2_36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_36.CausesValidation = true;
			this._Picture2_36.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_36.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_36.Enabled = true;
			this._Picture2_36.Location = new System.Drawing.Point(224, 400);
			this._Picture2_36.Name = "_Picture2_36";
			this._Picture2_36.Size = new System.Drawing.Size(17, 17);
			this._Picture2_36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_36.TabIndex = 69;
			this._Picture2_36.TabStop = true;
			this._Picture2_36.Visible = true;
			this._Picture2_36.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_35
			// 
			this._Picture2_35.BackColor = System.Drawing.Color.Navy;
			this._Picture2_35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_35.CausesValidation = true;
			this._Picture2_35.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_35.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_35.Enabled = true;
			this._Picture2_35.Location = new System.Drawing.Point(272, 400);
			this._Picture2_35.Name = "_Picture2_35";
			this._Picture2_35.Size = new System.Drawing.Size(17, 17);
			this._Picture2_35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_35.TabIndex = 68;
			this._Picture2_35.TabStop = true;
			this._Picture2_35.Visible = true;
			this._Picture2_35.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// AngularSystem
			// 
			this.AngularSystem.BackColor = System.Drawing.SystemColors.Window;
			this.AngularSystem.CausesValidation = true;
			this.AngularSystem.Cursor = System.Windows.Forms.Cursors.Default;
			this.AngularSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.AngularSystem.Enabled = true;
			this.AngularSystem.ForeColor = System.Drawing.SystemColors.WindowText;
			this.AngularSystem.IntegralHeight = true;
			this.AngularSystem.Location = new System.Drawing.Point(104, 80);
			this.AngularSystem.Name = "AngularSystem";
			this.AngularSystem.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.AngularSystem.Size = new System.Drawing.Size(193, 20);
			this.AngularSystem.Sorted = false;
			this.AngularSystem.TabIndex = 67;
			this.AngularSystem.TabStop = true;
			this.AngularSystem.Text = "Degrees-Minutes-Seconds";
			this.AngularSystem.Visible = true;
			this.AngularSystem.Items.AddRange(new object[]{"Degrees-Minutes-Seconds", "Degrees-Minutes", "Degrees"});
			// 
			// TextStorey
			// 
			this.TextStorey.Appearance = System.Windows.Forms.Appearance.Normal;
			this.TextStorey.BackColor = System.Drawing.SystemColors.Control;
			this.TextStorey.CausesValidation = true;
			this.TextStorey.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.TextStorey.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.TextStorey.Cursor = System.Windows.Forms.Cursors.Default;
			this.TextStorey.Enabled = true;
			this.TextStorey.ForeColor = System.Drawing.SystemColors.ControlText;
			this.TextStorey.Location = new System.Drawing.Point(160, 248);
			this.TextStorey.Name = "TextStorey";
			this.TextStorey.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TextStorey.Size = new System.Drawing.Size(121, 17);
			this.TextStorey.TabIndex = 65;
			this.TextStorey.TabStop = true;
			this.TextStorey.Text = "Force Text Storey";
			this.TextStorey.Visible = true;
			// 
			// TextHorizontal
			// 
			this.TextHorizontal.Appearance = System.Windows.Forms.Appearance.Normal;
			this.TextHorizontal.BackColor = System.Drawing.SystemColors.Control;
			this.TextHorizontal.CausesValidation = true;
			this.TextHorizontal.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.TextHorizontal.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.TextHorizontal.Cursor = System.Windows.Forms.Cursors.Default;
			this.TextHorizontal.Enabled = true;
			this.TextHorizontal.ForeColor = System.Drawing.SystemColors.ControlText;
			this.TextHorizontal.Location = new System.Drawing.Point(16, 248);
			this.TextHorizontal.Name = "TextHorizontal";
			this.TextHorizontal.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TextHorizontal.Size = new System.Drawing.Size(121, 17);
			this.TextHorizontal.TabIndex = 64;
			this.TextHorizontal.TabStop = true;
			this.TextHorizontal.Text = "Force Text Horizontal";
			this.TextHorizontal.Visible = true;
			// 
			// ChangeDirection
			// 
			this.ChangeDirection.Appearance = System.Windows.Forms.Appearance.Normal;
			this.ChangeDirection.BackColor = System.Drawing.SystemColors.Control;
			this.ChangeDirection.CausesValidation = true;
			this.ChangeDirection.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ChangeDirection.Checked = false;
			this.ChangeDirection.Cursor = System.Windows.Forms.Cursors.Default;
			this.ChangeDirection.Enabled = true;
			this.ChangeDirection.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ChangeDirection.Location = new System.Drawing.Point(88, 264);
			this.ChangeDirection.Name = "ChangeDirection";
			this.ChangeDirection.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ChangeDirection.Size = new System.Drawing.Size(121, 17);
			this.ChangeDirection.TabIndex = 63;
			this.ChangeDirection.TabStop = true;
			this.ChangeDirection.Text = "Change Direction";
			this.ChangeDirection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ChangeDirection.Visible = true;
			// 
			// _Picture2_34
			// 
			this._Picture2_34.BackColor = System.Drawing.Color.Olive;
			this._Picture2_34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_34.CausesValidation = true;
			this._Picture2_34.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_34.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_34.Enabled = true;
			this._Picture2_34.Location = new System.Drawing.Point(240, 400);
			this._Picture2_34.Name = "_Picture2_34";
			this._Picture2_34.Size = new System.Drawing.Size(17, 17);
			this._Picture2_34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_34.TabIndex = 61;
			this._Picture2_34.TabStop = true;
			this._Picture2_34.Visible = true;
			this._Picture2_34.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_33
			// 
			this._Picture2_33.BackColor = System.Drawing.Color.Teal;
			this._Picture2_33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_33.CausesValidation = true;
			this._Picture2_33.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_33.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_33.Enabled = true;
			this._Picture2_33.Location = new System.Drawing.Point(256, 400);
			this._Picture2_33.Name = "_Picture2_33";
			this._Picture2_33.Size = new System.Drawing.Size(17, 17);
			this._Picture2_33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_33.TabIndex = 60;
			this._Picture2_33.TabStop = true;
			this._Picture2_33.Visible = true;
			this._Picture2_33.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_32
			// 
			this._Picture2_32.BackColor = System.Drawing.Color.Purple;
			this._Picture2_32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_32.CausesValidation = true;
			this._Picture2_32.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_32.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_32.Enabled = true;
			this._Picture2_32.Location = new System.Drawing.Point(288, 400);
			this._Picture2_32.Name = "_Picture2_32";
			this._Picture2_32.Size = new System.Drawing.Size(17, 17);
			this._Picture2_32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_32.TabIndex = 59;
			this._Picture2_32.TabStop = true;
			this._Picture2_32.Visible = true;
			this._Picture2_32.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_31
			// 
			this._Picture2_31.BackColor = System.Drawing.Color.FromArgb(192, 0, 192);
			this._Picture2_31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_31.CausesValidation = true;
			this._Picture2_31.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_31.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_31.Enabled = true;
			this._Picture2_31.Location = new System.Drawing.Point(288, 384);
			this._Picture2_31.Name = "_Picture2_31";
			this._Picture2_31.Size = new System.Drawing.Size(17, 17);
			this._Picture2_31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_31.TabIndex = 58;
			this._Picture2_31.TabStop = true;
			this._Picture2_31.Visible = true;
			this._Picture2_31.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_30
			// 
			this._Picture2_30.BackColor = System.Drawing.Color.FromArgb(0, 0, 192);
			this._Picture2_30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_30.CausesValidation = true;
			this._Picture2_30.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_30.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_30.Enabled = true;
			this._Picture2_30.Location = new System.Drawing.Point(272, 384);
			this._Picture2_30.Name = "_Picture2_30";
			this._Picture2_30.Size = new System.Drawing.Size(17, 17);
			this._Picture2_30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_30.TabIndex = 57;
			this._Picture2_30.TabStop = true;
			this._Picture2_30.Visible = true;
			this._Picture2_30.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_29
			// 
			this._Picture2_29.BackColor = System.Drawing.Color.FromArgb(0, 192, 192);
			this._Picture2_29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_29.CausesValidation = true;
			this._Picture2_29.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_29.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_29.Enabled = true;
			this._Picture2_29.Location = new System.Drawing.Point(256, 384);
			this._Picture2_29.Name = "_Picture2_29";
			this._Picture2_29.Size = new System.Drawing.Size(17, 17);
			this._Picture2_29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_29.TabIndex = 56;
			this._Picture2_29.TabStop = true;
			this._Picture2_29.Visible = true;
			this._Picture2_29.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_28
			// 
			this._Picture2_28.BackColor = System.Drawing.Color.FromArgb(0, 192, 0);
			this._Picture2_28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_28.CausesValidation = true;
			this._Picture2_28.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_28.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_28.Enabled = true;
			this._Picture2_28.Location = new System.Drawing.Point(240, 384);
			this._Picture2_28.Name = "_Picture2_28";
			this._Picture2_28.Size = new System.Drawing.Size(17, 17);
			this._Picture2_28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_28.TabIndex = 55;
			this._Picture2_28.TabStop = true;
			this._Picture2_28.Visible = true;
			this._Picture2_28.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_27
			// 
			this._Picture2_27.BackColor = System.Drawing.Color.FromArgb(192, 192, 0);
			this._Picture2_27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_27.CausesValidation = true;
			this._Picture2_27.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_27.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_27.Enabled = true;
			this._Picture2_27.Location = new System.Drawing.Point(224, 384);
			this._Picture2_27.Name = "_Picture2_27";
			this._Picture2_27.Size = new System.Drawing.Size(17, 17);
			this._Picture2_27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_27.TabIndex = 54;
			this._Picture2_27.TabStop = true;
			this._Picture2_27.Visible = true;
			this._Picture2_27.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_26
			// 
			this._Picture2_26.BackColor = System.Drawing.Color.FromArgb(192, 64, 0);
			this._Picture2_26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_26.CausesValidation = true;
			this._Picture2_26.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_26.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_26.Enabled = true;
			this._Picture2_26.Location = new System.Drawing.Point(208, 384);
			this._Picture2_26.Name = "_Picture2_26";
			this._Picture2_26.Size = new System.Drawing.Size(17, 17);
			this._Picture2_26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_26.TabIndex = 53;
			this._Picture2_26.TabStop = true;
			this._Picture2_26.Visible = true;
			this._Picture2_26.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_25
			// 
			this._Picture2_25.BackColor = System.Drawing.Color.FromArgb(192, 0, 0);
			this._Picture2_25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_25.CausesValidation = true;
			this._Picture2_25.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_25.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_25.Enabled = true;
			this._Picture2_25.Location = new System.Drawing.Point(192, 384);
			this._Picture2_25.Name = "_Picture2_25";
			this._Picture2_25.Size = new System.Drawing.Size(17, 17);
			this._Picture2_25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_25.TabIndex = 52;
			this._Picture2_25.TabStop = true;
			this._Picture2_25.Visible = true;
			this._Picture2_25.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_24
			// 
			this._Picture2_24.BackColor = System.Drawing.Color.Gray;
			this._Picture2_24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_24.CausesValidation = true;
			this._Picture2_24.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_24.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_24.Enabled = true;
			this._Picture2_24.Location = new System.Drawing.Point(176, 384);
			this._Picture2_24.Name = "_Picture2_24";
			this._Picture2_24.Size = new System.Drawing.Size(17, 17);
			this._Picture2_24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_24.TabIndex = 51;
			this._Picture2_24.TabStop = true;
			this._Picture2_24.Visible = true;
			this._Picture2_24.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_23
			// 
			this._Picture2_23.BackColor = System.Drawing.Color.Fuchsia;
			this._Picture2_23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_23.CausesValidation = true;
			this._Picture2_23.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_23.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_23.Enabled = true;
			this._Picture2_23.Location = new System.Drawing.Point(288, 368);
			this._Picture2_23.Name = "_Picture2_23";
			this._Picture2_23.Size = new System.Drawing.Size(17, 17);
			this._Picture2_23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_23.TabIndex = 50;
			this._Picture2_23.TabStop = true;
			this._Picture2_23.Visible = true;
			this._Picture2_23.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_22
			// 
			this._Picture2_22.BackColor = System.Drawing.Color.Blue;
			this._Picture2_22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_22.CausesValidation = true;
			this._Picture2_22.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_22.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_22.Enabled = true;
			this._Picture2_22.Location = new System.Drawing.Point(272, 368);
			this._Picture2_22.Name = "_Picture2_22";
			this._Picture2_22.Size = new System.Drawing.Size(17, 17);
			this._Picture2_22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_22.TabIndex = 49;
			this._Picture2_22.TabStop = true;
			this._Picture2_22.Visible = true;
			this._Picture2_22.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_21
			// 
			this._Picture2_21.BackColor = System.Drawing.Color.Aqua;
			this._Picture2_21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_21.CausesValidation = true;
			this._Picture2_21.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_21.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_21.Enabled = true;
			this._Picture2_21.Location = new System.Drawing.Point(256, 368);
			this._Picture2_21.Name = "_Picture2_21";
			this._Picture2_21.Size = new System.Drawing.Size(17, 17);
			this._Picture2_21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_21.TabIndex = 48;
			this._Picture2_21.TabStop = true;
			this._Picture2_21.Visible = true;
			this._Picture2_21.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_20
			// 
			this._Picture2_20.BackColor = System.Drawing.Color.Lime;
			this._Picture2_20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_20.CausesValidation = true;
			this._Picture2_20.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_20.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_20.Enabled = true;
			this._Picture2_20.Location = new System.Drawing.Point(240, 368);
			this._Picture2_20.Name = "_Picture2_20";
			this._Picture2_20.Size = new System.Drawing.Size(17, 17);
			this._Picture2_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_20.TabIndex = 47;
			this._Picture2_20.TabStop = true;
			this._Picture2_20.Visible = true;
			this._Picture2_20.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_19
			// 
			this._Picture2_19.BackColor = System.Drawing.Color.Yellow;
			this._Picture2_19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_19.CausesValidation = true;
			this._Picture2_19.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_19.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_19.Enabled = true;
			this._Picture2_19.Location = new System.Drawing.Point(224, 368);
			this._Picture2_19.Name = "_Picture2_19";
			this._Picture2_19.Size = new System.Drawing.Size(17, 17);
			this._Picture2_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_19.TabIndex = 46;
			this._Picture2_19.TabStop = true;
			this._Picture2_19.Visible = true;
			this._Picture2_19.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_18
			// 
			this._Picture2_18.BackColor = System.Drawing.Color.FromArgb(255, 128, 0);
			this._Picture2_18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_18.CausesValidation = true;
			this._Picture2_18.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_18.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_18.Enabled = true;
			this._Picture2_18.Location = new System.Drawing.Point(208, 368);
			this._Picture2_18.Name = "_Picture2_18";
			this._Picture2_18.Size = new System.Drawing.Size(17, 17);
			this._Picture2_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_18.TabIndex = 45;
			this._Picture2_18.TabStop = true;
			this._Picture2_18.Visible = true;
			this._Picture2_18.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_17
			// 
			this._Picture2_17.BackColor = System.Drawing.Color.Red;
			this._Picture2_17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_17.CausesValidation = true;
			this._Picture2_17.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_17.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_17.Enabled = true;
			this._Picture2_17.Location = new System.Drawing.Point(192, 368);
			this._Picture2_17.Name = "_Picture2_17";
			this._Picture2_17.Size = new System.Drawing.Size(17, 17);
			this._Picture2_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_17.TabIndex = 44;
			this._Picture2_17.TabStop = true;
			this._Picture2_17.Visible = true;
			this._Picture2_17.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_16
			// 
			this._Picture2_16.BackColor = System.Drawing.Color.Silver;
			this._Picture2_16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_16.CausesValidation = true;
			this._Picture2_16.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_16.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_16.Enabled = true;
			this._Picture2_16.Location = new System.Drawing.Point(176, 368);
			this._Picture2_16.Name = "_Picture2_16";
			this._Picture2_16.Size = new System.Drawing.Size(17, 17);
			this._Picture2_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_16.TabIndex = 43;
			this._Picture2_16.TabStop = true;
			this._Picture2_16.Visible = true;
			this._Picture2_16.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_15
			// 
			this._Picture2_15.BackColor = System.Drawing.Color.FromArgb(255, 128, 255);
			this._Picture2_15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_15.CausesValidation = true;
			this._Picture2_15.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_15.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_15.Enabled = true;
			this._Picture2_15.Location = new System.Drawing.Point(288, 352);
			this._Picture2_15.Name = "_Picture2_15";
			this._Picture2_15.Size = new System.Drawing.Size(17, 17);
			this._Picture2_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_15.TabIndex = 42;
			this._Picture2_15.TabStop = true;
			this._Picture2_15.Visible = true;
			this._Picture2_15.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_14
			// 
			this._Picture2_14.BackColor = System.Drawing.Color.FromArgb(128, 128, 255);
			this._Picture2_14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_14.CausesValidation = true;
			this._Picture2_14.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_14.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_14.Enabled = true;
			this._Picture2_14.Location = new System.Drawing.Point(272, 352);
			this._Picture2_14.Name = "_Picture2_14";
			this._Picture2_14.Size = new System.Drawing.Size(17, 17);
			this._Picture2_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_14.TabIndex = 41;
			this._Picture2_14.TabStop = true;
			this._Picture2_14.Visible = true;
			this._Picture2_14.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_13
			// 
			this._Picture2_13.BackColor = System.Drawing.Color.FromArgb(128, 255, 255);
			this._Picture2_13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_13.CausesValidation = true;
			this._Picture2_13.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_13.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_13.Enabled = true;
			this._Picture2_13.Location = new System.Drawing.Point(256, 352);
			this._Picture2_13.Name = "_Picture2_13";
			this._Picture2_13.Size = new System.Drawing.Size(17, 17);
			this._Picture2_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_13.TabIndex = 40;
			this._Picture2_13.TabStop = true;
			this._Picture2_13.Visible = true;
			this._Picture2_13.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_12
			// 
			this._Picture2_12.BackColor = System.Drawing.Color.FromArgb(128, 255, 128);
			this._Picture2_12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_12.CausesValidation = true;
			this._Picture2_12.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_12.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_12.Enabled = true;
			this._Picture2_12.Location = new System.Drawing.Point(240, 352);
			this._Picture2_12.Name = "_Picture2_12";
			this._Picture2_12.Size = new System.Drawing.Size(17, 17);
			this._Picture2_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_12.TabIndex = 39;
			this._Picture2_12.TabStop = true;
			this._Picture2_12.Visible = true;
			this._Picture2_12.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_11
			// 
			this._Picture2_11.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
			this._Picture2_11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_11.CausesValidation = true;
			this._Picture2_11.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_11.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_11.Enabled = true;
			this._Picture2_11.Location = new System.Drawing.Point(224, 352);
			this._Picture2_11.Name = "_Picture2_11";
			this._Picture2_11.Size = new System.Drawing.Size(17, 17);
			this._Picture2_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_11.TabIndex = 38;
			this._Picture2_11.TabStop = true;
			this._Picture2_11.Visible = true;
			this._Picture2_11.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_10
			// 
			this._Picture2_10.BackColor = System.Drawing.Color.FromArgb(255, 192, 128);
			this._Picture2_10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_10.CausesValidation = true;
			this._Picture2_10.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_10.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_10.Enabled = true;
			this._Picture2_10.Location = new System.Drawing.Point(208, 352);
			this._Picture2_10.Name = "_Picture2_10";
			this._Picture2_10.Size = new System.Drawing.Size(17, 17);
			this._Picture2_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_10.TabIndex = 37;
			this._Picture2_10.TabStop = true;
			this._Picture2_10.Visible = true;
			this._Picture2_10.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_9
			// 
			this._Picture2_9.BackColor = System.Drawing.Color.FromArgb(255, 128, 128);
			this._Picture2_9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_9.CausesValidation = true;
			this._Picture2_9.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_9.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_9.Enabled = true;
			this._Picture2_9.Location = new System.Drawing.Point(192, 352);
			this._Picture2_9.Name = "_Picture2_9";
			this._Picture2_9.Size = new System.Drawing.Size(17, 17);
			this._Picture2_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_9.TabIndex = 36;
			this._Picture2_9.TabStop = true;
			this._Picture2_9.Visible = true;
			this._Picture2_9.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_8
			// 
			this._Picture2_8.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
			this._Picture2_8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_8.CausesValidation = true;
			this._Picture2_8.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_8.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_8.Enabled = true;
			this._Picture2_8.Location = new System.Drawing.Point(176, 352);
			this._Picture2_8.Name = "_Picture2_8";
			this._Picture2_8.Size = new System.Drawing.Size(17, 17);
			this._Picture2_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_8.TabIndex = 35;
			this._Picture2_8.TabStop = true;
			this._Picture2_8.Visible = true;
			this._Picture2_8.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_7
			// 
			this._Picture2_7.BackColor = System.Drawing.Color.FromArgb(255, 192, 255);
			this._Picture2_7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_7.CausesValidation = true;
			this._Picture2_7.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_7.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_7.Enabled = true;
			this._Picture2_7.Location = new System.Drawing.Point(288, 336);
			this._Picture2_7.Name = "_Picture2_7";
			this._Picture2_7.Size = new System.Drawing.Size(17, 17);
			this._Picture2_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_7.TabIndex = 34;
			this._Picture2_7.TabStop = true;
			this._Picture2_7.Visible = true;
			this._Picture2_7.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_6
			// 
			this._Picture2_6.BackColor = System.Drawing.Color.FromArgb(192, 192, 255);
			this._Picture2_6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_6.CausesValidation = true;
			this._Picture2_6.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_6.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_6.Enabled = true;
			this._Picture2_6.Location = new System.Drawing.Point(272, 336);
			this._Picture2_6.Name = "_Picture2_6";
			this._Picture2_6.Size = new System.Drawing.Size(17, 17);
			this._Picture2_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_6.TabIndex = 33;
			this._Picture2_6.TabStop = true;
			this._Picture2_6.Visible = true;
			this._Picture2_6.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_5
			// 
			this._Picture2_5.BackColor = System.Drawing.Color.FromArgb(192, 255, 255);
			this._Picture2_5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_5.CausesValidation = true;
			this._Picture2_5.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_5.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_5.Enabled = true;
			this._Picture2_5.Location = new System.Drawing.Point(256, 336);
			this._Picture2_5.Name = "_Picture2_5";
			this._Picture2_5.Size = new System.Drawing.Size(17, 17);
			this._Picture2_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_5.TabIndex = 32;
			this._Picture2_5.TabStop = true;
			this._Picture2_5.Visible = true;
			this._Picture2_5.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_4
			// 
			this._Picture2_4.BackColor = System.Drawing.Color.FromArgb(192, 255, 192);
			this._Picture2_4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_4.CausesValidation = true;
			this._Picture2_4.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_4.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_4.Enabled = true;
			this._Picture2_4.Location = new System.Drawing.Point(240, 336);
			this._Picture2_4.Name = "_Picture2_4";
			this._Picture2_4.Size = new System.Drawing.Size(17, 17);
			this._Picture2_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_4.TabIndex = 31;
			this._Picture2_4.TabStop = true;
			this._Picture2_4.Visible = true;
			this._Picture2_4.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_3
			// 
			this._Picture2_3.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this._Picture2_3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_3.CausesValidation = true;
			this._Picture2_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_3.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_3.Enabled = true;
			this._Picture2_3.Location = new System.Drawing.Point(224, 336);
			this._Picture2_3.Name = "_Picture2_3";
			this._Picture2_3.Size = new System.Drawing.Size(17, 17);
			this._Picture2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_3.TabIndex = 30;
			this._Picture2_3.TabStop = true;
			this._Picture2_3.Visible = true;
			this._Picture2_3.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_2
			// 
			this._Picture2_2.BackColor = System.Drawing.Color.FromArgb(255, 224, 192);
			this._Picture2_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_2.CausesValidation = true;
			this._Picture2_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_2.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_2.Enabled = true;
			this._Picture2_2.Location = new System.Drawing.Point(208, 336);
			this._Picture2_2.Name = "_Picture2_2";
			this._Picture2_2.Size = new System.Drawing.Size(17, 17);
			this._Picture2_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_2.TabIndex = 29;
			this._Picture2_2.TabStop = true;
			this._Picture2_2.Visible = true;
			this._Picture2_2.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_1
			// 
			this._Picture2_1.BackColor = System.Drawing.Color.FromArgb(255, 192, 192);
			this._Picture2_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_1.CausesValidation = true;
			this._Picture2_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_1.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_1.Enabled = true;
			this._Picture2_1.Location = new System.Drawing.Point(192, 336);
			this._Picture2_1.Name = "_Picture2_1";
			this._Picture2_1.Size = new System.Drawing.Size(17, 17);
			this._Picture2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_1.TabIndex = 28;
			this._Picture2_1.TabStop = true;
			this._Picture2_1.Visible = true;
			this._Picture2_1.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// _Picture2_0
			// 
			this._Picture2_0.BackColor = System.Drawing.Color.Black;
			this._Picture2_0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Picture2_0.CausesValidation = true;
			this._Picture2_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._Picture2_0.Dock = System.Windows.Forms.DockStyle.None;
			this._Picture2_0.Enabled = true;
			this._Picture2_0.Location = new System.Drawing.Point(176, 336);
			this._Picture2_0.Name = "_Picture2_0";
			this._Picture2_0.Size = new System.Drawing.Size(17, 17);
			this._Picture2_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this._Picture2_0.TabIndex = 27;
			this._Picture2_0.TabStop = true;
			this._Picture2_0.Visible = true;
			this._Picture2_0.Click += new System.EventHandler(this.Picture2_Click);
			// 
			// Picture1
			// 
			this.Picture1.BackColor = System.Drawing.Color.Black;
			this.Picture1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Picture1.CausesValidation = true;
			this.Picture1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Picture1.Dock = System.Windows.Forms.DockStyle.None;
			this.Picture1.Enabled = true;
			this.Picture1.Location = new System.Drawing.Point(152, 336);
			this.Picture1.Name = "Picture1";
			this.Picture1.Size = new System.Drawing.Size(17, 17);
			this.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.Picture1.TabIndex = 26;
			this.Picture1.TabStop = true;
			this.Picture1.Visible = true;
			this.Picture1.Click += new System.EventHandler(this.Picture1_Click);
			// 
			// Suffix
			// 
			this.Suffix.AcceptsReturn = true;
			this.Suffix.BackColor = System.Drawing.SystemColors.Window;
			this.Suffix.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Suffix.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.Suffix.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Suffix.Location = new System.Drawing.Point(208, 48);
			this.Suffix.MaxLength = 0;
			this.Suffix.Name = "Suffix";
			this.Suffix.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Suffix.Size = new System.Drawing.Size(89, 20);
			this.Suffix.TabIndex = 23;
			// 
			// Prefix
			// 
			this.Prefix.AcceptsReturn = true;
			this.Prefix.BackColor = System.Drawing.SystemColors.Window;
			this.Prefix.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Prefix.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.Prefix.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Prefix.Location = new System.Drawing.Point(56, 48);
			this.Prefix.MaxLength = 0;
			this.Prefix.Name = "Prefix";
			this.Prefix.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Prefix.Size = new System.Drawing.Size(89, 20);
			this.Prefix.TabIndex = 21;
			// 
			// LinkSurText
			// 
			this.LinkSurText.Appearance = System.Windows.Forms.Appearance.Normal;
			this.LinkSurText.BackColor = System.Drawing.SystemColors.Control;
			this.LinkSurText.CausesValidation = true;
			this.LinkSurText.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.LinkSurText.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.LinkSurText.Cursor = System.Windows.Forms.Cursors.Default;
			this.LinkSurText.Enabled = true;
			this.LinkSurText.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LinkSurText.Location = new System.Drawing.Point(8, 336);
			this.LinkSurText.Name = "LinkSurText";
			this.LinkSurText.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LinkSurText.Size = new System.Drawing.Size(137, 17);
			this.LinkSurText.TabIndex = 20;
			this.LinkSurText.TabStop = true;
			this.LinkSurText.Text = "Associative Dimension";
			this.LinkSurText.Visible = true;
			this.LinkSurText.CheckStateChanged += new System.EventHandler(this.LinkSurText_CheckStateChanged);
			// 
			// LenPrecision
			// 
			this.LenPrecision.AcceptsReturn = true;
			this.LenPrecision.BackColor = System.Drawing.SystemColors.Window;
			this.LenPrecision.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.LenPrecision.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.LenPrecision.ForeColor = System.Drawing.SystemColors.WindowText;
			this.LenPrecision.Location = new System.Drawing.Point(256, 296);
			this.LenPrecision.MaxLength = 0;
			this.LenPrecision.Name = "LenPrecision";
			this.LenPrecision.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LenPrecision.Size = new System.Drawing.Size(25, 20);
			this.LenPrecision.TabIndex = 18;
			this.LenPrecision.Text = "2";
			// 
			// VScroll1
			// 
			this.VScroll1.CausesValidation = true;
			this.VScroll1.Cursor = System.Windows.Forms.Cursors.Default;
			this.VScroll1.Enabled = true;
			this.VScroll1.LargeChange = 1;
			this.VScroll1.Location = new System.Drawing.Point(280, 296);
			this.VScroll1.Maximum = 0;
			this.VScroll1.Minimum = 10;
			this.VScroll1.Name = "VScroll1";
			this.VScroll1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.VScroll1.Size = new System.Drawing.Size(17, 20);
			this.VScroll1.SmallChange = 1;
			this.VScroll1.TabIndex = 17;
			this.VScroll1.TabStop = true;
			this.VScroll1.Value = 10;
			this.VScroll1.Visible = true;
			this.VScroll1.ValueChanged += new System.EventHandler(this.VScroll1_ValueChanged);
			// 
			// LenUnits
			// 
			this.LenUnits.BackColor = System.Drawing.SystemColors.Window;
			this.LenUnits.CausesValidation = true;
			this.LenUnits.Cursor = System.Windows.Forms.Cursors.Default;
			this.LenUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.LenUnits.Enabled = true;
			this.LenUnits.ForeColor = System.Drawing.SystemColors.WindowText;
			this.LenUnits.IntegralHeight = true;
			this.LenUnits.Location = new System.Drawing.Point(144, 296);
			this.LenUnits.Name = "LenUnits";
			this.LenUnits.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LenUnits.Size = new System.Drawing.Size(41, 20);
			this.LenUnits.Sorted = false;
			this.LenUnits.TabIndex = 15;
			this.LenUnits.TabStop = true;
			this.LenUnits.Text = "in";
			this.LenUnits.Visible = true;
			this.LenUnits.TextChanged += new System.EventHandler(this.LenUnits_TextChanged);
			// 
			// AppendLenUnits
			// 
			this.AppendLenUnits.Appearance = System.Windows.Forms.Appearance.Normal;
			this.AppendLenUnits.BackColor = System.Drawing.SystemColors.Control;
			this.AppendLenUnits.CausesValidation = true;
			this.AppendLenUnits.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.AppendLenUnits.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.AppendLenUnits.Cursor = System.Windows.Forms.Cursors.Default;
			this.AppendLenUnits.Enabled = true;
			this.AppendLenUnits.ForeColor = System.Drawing.SystemColors.ControlText;
			this.AppendLenUnits.Location = new System.Drawing.Point(16, 296);
			this.AppendLenUnits.Name = "AppendLenUnits";
			this.AppendLenUnits.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.AppendLenUnits.Size = new System.Drawing.Size(89, 17);
			this.AppendLenUnits.TabIndex = 14;
			this.AppendLenUnits.TabStop = true;
			this.AppendLenUnits.Text = "Append Units";
			this.AppendLenUnits.Visible = true;
			this.AppendLenUnits.CheckStateChanged += new System.EventHandler(this.AppendLenUnits_CheckStateChanged);
			// 
			// TextGap
			// 
			this.TextGap.AcceptsReturn = true;
			this.TextGap.BackColor = System.Drawing.SystemColors.Window;
			this.TextGap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.TextGap.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.TextGap.ForeColor = System.Drawing.SystemColors.WindowText;
			this.TextGap.Location = new System.Drawing.Point(200, 224);
			this.TextGap.MaxLength = 0;
			this.TextGap.Name = "TextGap";
			this.TextGap.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TextGap.Size = new System.Drawing.Size(81, 20);
			this.TextGap.TabIndex = 12;
			this.TextGap.Text = "Text1";
			// 
			// FontText
			// 
			this.FontText.BackColor = System.Drawing.SystemColors.Window;
			this.FontText.CausesValidation = true;
			this.FontText.Cursor = System.Windows.Forms.Cursors.Default;
			this.FontText.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.FontText.Enabled = true;
			this.FontText.ForeColor = System.Drawing.SystemColors.WindowText;
			this.FontText.IntegralHeight = true;
			this.FontText.Location = new System.Drawing.Point(144, 176);
			this.FontText.Name = "FontText";
			this.FontText.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FontText.Size = new System.Drawing.Size(153, 20);
			this.FontText.Sorted = false;
			this.FontText.TabIndex = 7;
			this.FontText.TabStop = true;
			this.FontText.Text = "Times New Roman";
			this.FontText.Visible = true;
			// 
			// HeightText
			// 
			this.HeightText.AcceptsReturn = true;
			this.HeightText.BackColor = System.Drawing.SystemColors.Window;
			this.HeightText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.HeightText.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.HeightText.ForeColor = System.Drawing.SystemColors.WindowText;
			this.HeightText.Location = new System.Drawing.Point(56, 176);
			this.HeightText.MaxLength = 0;
			this.HeightText.Name = "HeightText";
			this.HeightText.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.HeightText.Size = new System.Drawing.Size(49, 20);
			this.HeightText.TabIndex = 5;
			this.HeightText.Text = "0.2";
			// 
			// Location_Renamed
			// 
			this.Location_Renamed.BackColor = System.Drawing.SystemColors.Window;
			this.Location_Renamed.CausesValidation = true;
			this.Location_Renamed.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location_Renamed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.Location_Renamed.Enabled = true;
			this.Location_Renamed.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Location_Renamed.IntegralHeight = true;
			this.Location_Renamed.Location = new System.Drawing.Point(64, 224);
			this.Location_Renamed.Name = "Location_Renamed";
			this.Location_Renamed.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Location_Renamed.Size = new System.Drawing.Size(73, 20);
			this.Location_Renamed.Sorted = false;
			this.Location_Renamed.TabIndex = 3;
			this.Location_Renamed.TabStop = true;
			this.Location_Renamed.Text = "Above";
			this.Location_Renamed.Visible = true;
			// 
			// SurvText
			// 
			this.SurvText.AcceptsReturn = true;
			this.SurvText.BackColor = System.Drawing.Color.White;
			this.SurvText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.SurvText.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.SurvText.ForeColor = System.Drawing.SystemColors.WindowText;
			this.SurvText.Location = new System.Drawing.Point(48, 8);
			this.SurvText.MaxLength = 0;
			this.SurvText.Multiline = true;
			this.SurvText.Name = "SurvText";
			this.SurvText.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.SurvText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.SurvText.Size = new System.Drawing.Size(249, 33);
			this.SurvText.TabIndex = 2;
			// 
			// cmdOK
			// 
			this.cmdOK.BackColor = System.Drawing.SystemColors.Control;
			this.cmdOK.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdOK.Location = new System.Drawing.Point(56, 360);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdOK.Size = new System.Drawing.Size(73, 25);
			this.cmdOK.TabIndex = 0;
			this.cmdOK.Text = "OK";
			this.cmdOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cmdOK.UseVisualStyleBackColor = false;
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.BackColor = System.Drawing.SystemColors.Control;
			this.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdCancel.Location = new System.Drawing.Point(56, 392);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdCancel.Size = new System.Drawing.Size(73, 25);
			this.cmdCancel.TabIndex = 1;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cmdCancel.UseVisualStyleBackColor = false;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// Label16
			// 
			this.Label16.AutoSize = true;
			this.Label16.BackColor = System.Drawing.SystemColors.Control;
			this.Label16.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label16.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label16.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label16.Location = new System.Drawing.Point(16, 128);
			this.Label16.Name = "Label16";
			this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label16.Size = new System.Drawing.Size(55, 13);
			this.Label16.TabIndex = 75;
			this.Label16.Text = "Arc System";
			// 
			// Label15
			// 
			this.Label15.AutoSize = true;
			this.Label15.BackColor = System.Drawing.SystemColors.Control;
			this.Label15.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label15.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label15.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label15.Location = new System.Drawing.Point(16, 104);
			this.Label15.Name = "Label15";
			this.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label15.Size = new System.Drawing.Size(59, 13);
			this.Label15.TabIndex = 73;
			this.Label15.Text = "Line System";
			// 
			// Line6
			// 
			this.Line6.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line6.Enabled = false;
			this.Line6.Location = new System.Drawing.Point(24, 328);
			this.Line6.Name = "Line6";
			this.Line6.Size = new System.Drawing.Size(280, 1);
			this.Line6.Visible = true;
			// 
			// Label14
			// 
			this.Label14.AutoSize = true;
			this.Label14.BackColor = System.Drawing.SystemColors.Control;
			this.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label14.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label14.Location = new System.Drawing.Point(16, 80);
			this.Label14.Name = "Label14";
			this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label14.Size = new System.Drawing.Size(76, 13);
			this.Label14.TabIndex = 66;
			this.Label14.Text = "Angular System";
			// 
			// Label13
			// 
			this.Label13.BackColor = System.Drawing.SystemColors.Control;
			this.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label13.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label13.Location = new System.Drawing.Point(176, 336);
			this.Label13.Name = "Label13";
			this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label13.Size = new System.Drawing.Size(97, 17);
			this.Label13.TabIndex = 62;
			this.Label13.Text = "Pen Color";
			// 
			// Label12
			// 
			this.Label12.AutoSize = true;
			this.Label12.BackColor = System.Drawing.SystemColors.Control;
			this.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label12.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label12.Location = new System.Drawing.Point(16, 8);
			this.Label12.Name = "Label12";
			this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label12.Size = new System.Drawing.Size(21, 13);
			this.Label12.TabIndex = 25;
			this.Label12.Text = "Text";
			// 
			// Label11
			// 
			this.Label11.AutoSize = true;
			this.Label11.BackColor = System.Drawing.SystemColors.Control;
			this.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label11.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label11.Location = new System.Drawing.Point(168, 48);
			this.Label11.Name = "Label11";
			this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label11.Size = new System.Drawing.Size(25, 13);
			this.Label11.TabIndex = 24;
			this.Label11.Text = "Suffix";
			// 
			// Label10
			// 
			this.Label10.AutoSize = true;
			this.Label10.BackColor = System.Drawing.SystemColors.Control;
			this.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label10.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label10.Location = new System.Drawing.Point(16, 48);
			this.Label10.Name = "Label10";
			this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label10.Size = new System.Drawing.Size(27, 13);
			this.Label10.TabIndex = 22;
			this.Label10.Text = "Prefix";
			// 
			// Label9
			// 
			this.Label9.AutoSize = true;
			this.Label9.BackColor = System.Drawing.SystemColors.Control;
			this.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label9.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label9.Location = new System.Drawing.Point(208, 296);
			this.Label9.Name = "Label9";
			this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label9.Size = new System.Drawing.Size(45, 13);
			this.Label9.TabIndex = 19;
			this.Label9.Text = "Precision";
			// 
			// Label8
			// 
			this.Label8.AutoSize = true;
			this.Label8.BackColor = System.Drawing.SystemColors.Control;
			this.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label8.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label8.Location = new System.Drawing.Point(112, 296);
			this.Label8.Name = "Label8";
			this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label8.Size = new System.Drawing.Size(24, 13);
			this.Label8.TabIndex = 16;
			this.Label8.Text = "Units";
			// 
			// Label7
			// 
			this.Label7.AutoSize = true;
			this.Label7.BackColor = System.Drawing.Color.Transparent;
			this.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label7.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Label7.Location = new System.Drawing.Point(16, 280);
			this.Label7.Name = "Label7";
			this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label7.Size = new System.Drawing.Size(61, 13);
			this.Label7.TabIndex = 13;
			this.Label7.Text = "Primary Unts";
			// 
			// Line3
			// 
			this.Line3.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line3.Enabled = false;
			this.Line3.Location = new System.Drawing.Point(96, 288);
			this.Line3.Name = "Line3";
			this.Line3.Size = new System.Drawing.Size(208, 1);
			this.Line3.Visible = true;
			// 
			// Label6
			// 
			this.Label6.AutoSize = true;
			this.Label6.BackColor = System.Drawing.SystemColors.Control;
			this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label6.Location = new System.Drawing.Point(152, 224);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(42, 13);
			this.Label6.TabIndex = 11;
			this.Label6.Text = "TextGap";
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.BackColor = System.Drawing.Color.Transparent;
			this.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Label5.Location = new System.Drawing.Point(16, 200);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(62, 13);
			this.Label5.TabIndex = 10;
			this.Label5.Text = "Text Position";
			// 
			// Line2
			// 
			this.Line2.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line2.Enabled = false;
			this.Line2.Location = new System.Drawing.Point(96, 208);
			this.Line2.Name = "Line2";
			this.Line2.Size = new System.Drawing.Size(208, 1);
			this.Line2.Visible = true;
			// 
			// Label4
			// 
			this.Label4.AutoSize = true;
			this.Label4.BackColor = System.Drawing.SystemColors.Control;
			this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(112, 176);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(21, 13);
			this.Label4.TabIndex = 9;
			this.Label4.Text = "Font";
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.BackColor = System.Drawing.Color.Transparent;
			this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Label3.Location = new System.Drawing.Point(16, 152);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(21, 13);
			this.Label3.TabIndex = 8;
			this.Label3.Text = "Text";
			// 
			// Line1
			// 
			this.Line1.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line1.Enabled = false;
			this.Line1.Location = new System.Drawing.Point(56, 160);
			this.Line1.Name = "Line1";
			this.Line1.Size = new System.Drawing.Size(248, 1);
			this.Line1.Visible = true;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.BackColor = System.Drawing.SystemColors.Control;
			this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(16, 176);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(32, 13);
			this.Label2.TabIndex = 6;
			this.Label2.Text = "Height";
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.BackColor = System.Drawing.SystemColors.Control;
			this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(16, 224);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(36, 13);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "Vertical";
			// 
			// frmSample
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8, 16);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(317, 423);
			this.Controls.Add(this.ArcSystem);
			this.Controls.Add(this.LineSystem);
			this.Controls.Add(this._Picture2_39);
			this.Controls.Add(this._Picture2_38);
			this.Controls.Add(this._Picture2_37);
			this.Controls.Add(this._Picture2_36);
			this.Controls.Add(this._Picture2_35);
			this.Controls.Add(this.AngularSystem);
			this.Controls.Add(this.TextStorey);
			this.Controls.Add(this.TextHorizontal);
			this.Controls.Add(this.ChangeDirection);
			this.Controls.Add(this._Picture2_34);
			this.Controls.Add(this._Picture2_33);
			this.Controls.Add(this._Picture2_32);
			this.Controls.Add(this._Picture2_31);
			this.Controls.Add(this._Picture2_30);
			this.Controls.Add(this._Picture2_29);
			this.Controls.Add(this._Picture2_28);
			this.Controls.Add(this._Picture2_27);
			this.Controls.Add(this._Picture2_26);
			this.Controls.Add(this._Picture2_25);
			this.Controls.Add(this._Picture2_24);
			this.Controls.Add(this._Picture2_23);
			this.Controls.Add(this._Picture2_22);
			this.Controls.Add(this._Picture2_21);
			this.Controls.Add(this._Picture2_20);
			this.Controls.Add(this._Picture2_19);
			this.Controls.Add(this._Picture2_18);
			this.Controls.Add(this._Picture2_17);
			this.Controls.Add(this._Picture2_16);
			this.Controls.Add(this._Picture2_15);
			this.Controls.Add(this._Picture2_14);
			this.Controls.Add(this._Picture2_13);
			this.Controls.Add(this._Picture2_12);
			this.Controls.Add(this._Picture2_11);
			this.Controls.Add(this._Picture2_10);
			this.Controls.Add(this._Picture2_9);
			this.Controls.Add(this._Picture2_8);
			this.Controls.Add(this._Picture2_7);
			this.Controls.Add(this._Picture2_6);
			this.Controls.Add(this._Picture2_5);
			this.Controls.Add(this._Picture2_4);
			this.Controls.Add(this._Picture2_3);
			this.Controls.Add(this._Picture2_2);
			this.Controls.Add(this._Picture2_1);
			this.Controls.Add(this._Picture2_0);
			this.Controls.Add(this.Picture1);
			this.Controls.Add(this.Suffix);
			this.Controls.Add(this.Prefix);
			this.Controls.Add(this.LinkSurText);
			this.Controls.Add(this.LenPrecision);
			this.Controls.Add(this.VScroll1);
			this.Controls.Add(this.LenUnits);
			this.Controls.Add(this.AppendLenUnits);
			this.Controls.Add(this.TextGap);
			this.Controls.Add(this.FontText);
			this.Controls.Add(this.HeightText);
			this.Controls.Add(this.Location_Renamed);
			this.Controls.Add(this.SurvText);
			this.Controls.Add(this.cmdOK);
			this.Controls.Add(this.cmdCancel);
			this.Controls.Add(this.Label16);
			this.Controls.Add(this.Label15);
			this.Controls.Add(this.Line6);
			this.Controls.Add(this.Label14);
			this.Controls.Add(this.Label13);
			this.Controls.Add(this.Label12);
			this.Controls.Add(this.Label11);
			this.Controls.Add(this.Label10);
			this.Controls.Add(this.Label9);
			this.Controls.Add(this.Label8);
			this.Controls.Add(this.Label7);
			this.Controls.Add(this.Line3);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Line2);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Line1);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSample";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Surveyor Dimension Properties";
			this.Location = new System.Drawing.Point(428, 193);
			this.listBoxComboBoxHelper1.SetItemData(this.ArcSystem, new int[]{0, 0, 0, 0, 0, 0, 0});
			this.listBoxComboBoxHelper1.SetItemData(this.LineSystem, new int[]{0, 0, 0});
			this.listBoxComboBoxHelper1.SetItemData(this.AngularSystem, new int[]{0, 0, 0});
			this.Closed += new System.EventHandler(this.frmSample_Closed);
			((System.ComponentModel.ISupportInitialize) this.listBoxComboBoxHelper1).EndInit();
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializePicture2();
		}
		void InitializePicture2()
		{
			this.Picture2 = new System.Windows.Forms.PictureBox[40];
			this.Picture2[39] = _Picture2_39;
			this.Picture2[38] = _Picture2_38;
			this.Picture2[37] = _Picture2_37;
			this.Picture2[36] = _Picture2_36;
			this.Picture2[35] = _Picture2_35;
			this.Picture2[34] = _Picture2_34;
			this.Picture2[33] = _Picture2_33;
			this.Picture2[32] = _Picture2_32;
			this.Picture2[31] = _Picture2_31;
			this.Picture2[30] = _Picture2_30;
			this.Picture2[29] = _Picture2_29;
			this.Picture2[28] = _Picture2_28;
			this.Picture2[27] = _Picture2_27;
			this.Picture2[26] = _Picture2_26;
			this.Picture2[25] = _Picture2_25;
			this.Picture2[24] = _Picture2_24;
			this.Picture2[23] = _Picture2_23;
			this.Picture2[22] = _Picture2_22;
			this.Picture2[21] = _Picture2_21;
			this.Picture2[20] = _Picture2_20;
			this.Picture2[19] = _Picture2_19;
			this.Picture2[18] = _Picture2_18;
			this.Picture2[17] = _Picture2_17;
			this.Picture2[16] = _Picture2_16;
			this.Picture2[15] = _Picture2_15;
			this.Picture2[14] = _Picture2_14;
			this.Picture2[13] = _Picture2_13;
			this.Picture2[12] = _Picture2_12;
			this.Picture2[11] = _Picture2_11;
			this.Picture2[10] = _Picture2_10;
			this.Picture2[9] = _Picture2_9;
			this.Picture2[8] = _Picture2_8;
			this.Picture2[7] = _Picture2_7;
			this.Picture2[6] = _Picture2_6;
			this.Picture2[5] = _Picture2_5;
			this.Picture2[4] = _Picture2_4;
			this.Picture2[3] = _Picture2_3;
			this.Picture2[2] = _Picture2_2;
			this.Picture2[1] = _Picture2_1;
			this.Picture2[0] = _Picture2_0;
		}
		#endregion
	}
}