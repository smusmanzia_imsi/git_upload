using System;
using System.Collections.Generic;
using System.Text;
using IMSIGX;

namespace SDKTest
{
    class View : TestBase
    {
        IMSIGX.View view = null;

        public View(IMSIGX.View _view) { view = _view; testObj = _view; }

     
        protected override void TestMethods()
        {
            IntPtr bmpDC = view.OpenBitmapDC();
            if (bmpDC.ToInt32() == 0) throw new SDKTestExeption("Cannot open bitmap DC");
            
            byte[] bmpBytes = view.CloseBitmapBitsDC();
            if (bmpBytes.Length == 0) throw new SDKTestExeption("Cannot close bitmap DC");

            IntPtr bmpDC1 = view.OpenBitmapDC();
            IntPtr bmpPnt = view.CloseBitmapDC();
            if (bmpPnt.ToInt32() == 0) throw new SDKTestExeption("Cannot close bitmap DC");

            IntPtr mfDC = view.OpenMetafileDC();
            if (mfDC.ToInt32() == 0) throw new SDKTestExeption("Cannot open metafile DC");
            byte[] mfBytes = view.CloseMetafileBitsDC();
            if (mfBytes.Length == 0) throw new SDKTestExeption("Cannot close metafile DC");

            IntPtr mfDC1 = view.OpenMetafileDC();
            IntPtr mfPtr = view.CloseMetafileDC();
            if (mfPtr.ToInt32() == 0) throw new SDKTestExeption("Cannot close metafile DC");

            view.Refresh();

            System.Double x, y, z;
            view.ScreenToView(10, 10, out x, out y);
            view.ViewToScreen(10, 10, out x, out y);
            view.ViewToWorld(10, 10, 10, out x, out y, out z);
            view.WorldToView(10, 10, 10, out x, out y, out z);
            view.ZoomToExtents();
            //view.ZoomToNamedView();

            view.PickPoint(10, 10, 10, true, false, true, false, true, false);
            view.PickRect(10, 10, 10, 10, false, true, false, true, false, true);

            System.Double[] Arr = new Double[6];
            Arr[0] = 0;
            Arr[1] = 1;
            Arr[2] = 2;
            Arr[3] = 3;
            Arr[4] = 4;
            Arr[5] = 5;
            view.DragOutline(Arr, 10, 10, ImsiDragState.imsiDragBegin);
        }

        public void TestXAppMethods(ref IntPtr hwnd, ref IntPtr hdc)
        {
            //IMSIGX.XApplication xapp = new XApplication();

            //if (xapp == null) throw new SDKTestExeption("Cannot create XApplication");

            //IMSIGX.Filter filter = null;
            //foreach (IMSIGX.Filter f in xapp.Filters)
            //{
            //    if (f.FilterString.Contains("tcw"))
            //        filter = f;
            //}
            //xapp.Drawings.Open("d:\\collada\\Drawing1.tcw", true, filter);
            //IMSIGX.View view = xapp.ActiveDrawing.Views.Add(hwnd.ToInt32(), hdc.ToInt32());

            //IntPtr bmpDC2 = view.OpenBitmapDC();
            //if (bmpDC2.ToInt32() == 0) throw new SDKTestExeption("Cannot open bitmsp DC");
            //System.Drawing.Bitmap bmpObj = view.CloseBitmapPictDC();

            //bmpObj.Save("d:\\test.bmp");

            //IntPtr mfDC1 = view.OpenMetafileDC();
            //if (mfDC1.ToInt32() == 0) throw new SDKTestExeption("Cannot create metafile DC");
            //System.Drawing.Imaging.Metafile mfobj = view.CloseMetafilePictDC();

            //xapp.Quit();
        }

    }
}
