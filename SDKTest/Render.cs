using System;
using IMSIGX;

namespace SDKTest
{
    public class Render : TestBase
    {
        GXRENDERS.Renders renders = null;
        GXRENDERS.Render render = null;
        IMSIGX.Drawing drawing = null;

        public Render(GXRENDERS.Renders r, IMSIGX.Drawing drw)
        { 
            renders = r; 
            testObj = r;
            drawing = drw;
        }

        protected override void TestMethods()
        {
            bool val = renders.GetAutoUpdate();
            renders.SetAutoUpdate(val);

            if (drawing != null)
            {
                bool val2 = renders.GetAutoUpdate(drawing);
                renders.SetAutoUpdate(drawing, val2);
            }

            if (renders.Count > 0)
                render = renders[0];

            CreateTest<GXRENDERS.Render> rendTest = new CreateTest<GXRENDERS.Render>(render);
            System.Collections.ArrayList noWrite = new System.Collections.ArrayList();
            noWrite.Add("DefaultMode");
            rendTest.SkipWriteProperties = noWrite;
            System.Collections.ArrayList noRead = new System.Collections.ArrayList();
            noRead.Add("RadExtendedMode");
            rendTest.SkipReadProperties = noRead;
            rendTest.SetParentProperties(this);
            rendTest.Test();

           // GXRENDERS.ImsiRenderMode mode = render.RadExtendedMode;

            render.Run(drawing.ActiveView);
        }
    }

    public class RenderView : TestBase
    {
        GXRENDERS.RenderView rview = null;

        public RenderView(GXRENDERS.RenderView rv) { rview = rv; testObj = rv; }

        protected override void TestMethods()
        {
            //rview.BeginRender(1);
            //rview.DoRender();
            //rview.EndRender();
        }
    }
}