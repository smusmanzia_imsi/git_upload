using System;
using IMSIGX;

namespace SDKTest
{
    public class NamedUCS : TestBase
    {
        GXNAMEDUCS.NamedUCSs nus = null;
        GXNAMEDUCS.NamedUCS nu = null;

        public NamedUCS(GXNAMEDUCS.NamedUCSs _ns) { nus = _ns; testObj = _ns; }

        protected override void TestMethods()
        {
            nu = nus.Add("newUCS");
            CheckObjectZero(nu);

            CheckObjectZero(nus.Add("newUCS_copy",(IMSIGX.Matrix)nu.Matrix));

            CreateTest<GXNAMEDUCS.NamedUCS> nTest = new CreateTest<GXNAMEDUCS.NamedUCS>(nu);
            nTest.SetParentProperties(this);
            System.Collections.ArrayList noWrite = new System.Collections.ArrayList();
            noWrite.Add("Matrix");
            nTest.SkipWriteProperties = noWrite;
            nTest.Test();

         //   nu.Activate();
            nu.Delete();
        }
    }
}