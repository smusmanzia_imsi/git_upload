using System;
using IMSIGX;

namespace SDKTest
{
    public class Boolean3D : TestBase
    {
        GXEXT.Boolean3D bool3d = null;

        IMSIGX.Graphic gr1 = null;
        IMSIGX.Graphic gr2 = null;
        IMSIGX.Application app = null;

        public Boolean3D(IMSIGX.Application Application)
        {
            bool3d = new GXEXT.Boolean3D(Application);
            testObj = bool3d;
            app = Application;
        }

        protected override void TestMethods()
        {
            app.Drawings.Open( this.TestFilesDirectory + "CubeSphera.tcw");

            if (app.ActiveDrawing != null && app.ActiveDrawing.Graphics.Count > 1)
            {
                gr1 = (IMSIGX.Graphic)app.ActiveDrawing.Graphics[0];
                gr2 = (IMSIGX.Graphic)app.ActiveDrawing.Graphics[1];
            }

            if( gr1 == null || gr2 == null)
            {
                WriteToLog("No graphics to test Boolean3D");
                return;
            }

            IMSIGX.Graphic gr = bool3d.Add(gr1, gr2);
            CheckObjectZero(gr);

            CheckObjectZero( bool3d.Subtract(gr, gr1) );

            CheckObjectZero( bool3d.Intersection(gr1, gr2) );

            WriteToLog("Boolean3D test passed");
        }
    }

    public class DwgExt : TestBase
    {
        GXEXT.DwgEx dwg = null;
        IMSIGX.Drawing drawing = null;

        public DwgExt(IMSIGX.Application app)
        {
            dwg = new GXEXT.DwgEx(app);
            testObj = dwg;
            drawing = (IMSIGX.Drawing)app.ActiveDrawing;
        }

        protected override void TestMethods()
        {
            dwg.OpenFile( this.TestFilesDirectory + "Cube.tcw");
            dwg.GetPartTree(drawing, drawing.Graphics[0]);
            IMSIGX.Graphics grs = dwg.GetSpaceGraphics(drawing, ImsiSpaceModeType.imsiModelSpace);
           // dwg.InsertSymbol( drawing, this.TestFilesDirectory + "CubeSphera.tcw", ,true);
            dwg.UpdatePartTree(drawing, drawing.Graphics[0],drawing.Graphics[1], true); 
            //throw new SDKTestExeption("Test is not implemented");

            WriteToLog("DwgExt test passed");
        }
    }

    public class Graphic3GOp : TestBase
    {
        GXEXT.GraphicG3Op graphicOp = null;
        IMSIGX.Graphic gr1 = null;
        IMSIGX.Graphic gr2 = null;
        IMSIGX.Application app = null;

        public Graphic3GOp(IMSIGX.Application Application)
        {
            graphicOp = new GXEXT.GraphicG3Op(Application);
            testObj = graphicOp;
            app = Application;
        }

        protected override void TestMethods()
        {
            app.Drawings.Open( this.TestFilesDirectory + "CubeSphera.tcw");
            
            if (app.ActiveDrawing != null && app.ActiveDrawing.Graphics.Count > 1)
            {
                gr1 = (IMSIGX.Graphic)app.ActiveDrawing.Graphics[0];
                gr2 = (IMSIGX.Graphic)app.ActiveDrawing.Graphics[1];
            }

            if (gr1 == null || gr2 == null)
            {
                WriteToLog("No graphics to test Graphic3GOp");
                return;
            }

            CheckObjectZero(graphicOp.Slice(gr1, gr2));

            WriteToLog("GraphicOp test passed");
        }
    }
}