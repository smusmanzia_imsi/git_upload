using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using IMSIGX;
using System.IO;

namespace SDKTest
{
    public partial class TestApplication: TestBase
    {
        IMSIGX.Application app = null;
        bool appInProcServer = true;
        
        public bool RunAsInProcServer
        {
            set { appInProcServer = value; }
            get { return appInProcServer; }
        }

        public  TestApplication(){}

        public override void TestProperties()
        {
            //check properties
            //just get  
            String sCaption = app.Caption;
            CommandBars cbs = app.CommandBars;
            Object envs = app.Environments;
            Filters flts = app.Filters;
            Object lums = app.Luminances;
            Object mats = app.Materials;
            String sName = app.Name;
            NamedColors ncols = app.NamedColors;
            IMSIGX.Application parent = app.Parent;
            IMSIGX.Properties prs = app.Properties;
            RegenMethods rgs = app.RegenMethods;
            Object renders = app.Renders;
            int nSnap = app.SnapModes;
            Object tms = app.TieMethods;
            Object tes = app.ToolEvents;
            Tools tls = app.Tools;

            if (!Mode)
            {
                bool bUserControl = app.UserControl;
                app.UserControl = bUserControl;
                bool bVisible = app.Visible;
            }

            //set properties
            app.SnapModes = 3;            

            string build = app.Build;
            string path = app.Path;
            string version = app.Version;
            string prName = app.ProfileName;
            app.ProfileName = prName;
        }

        protected override void TestMethods()
        {
            if (!bConsoleMode)
            {
                app.MessageBox("My Message", MessageBoxButtons.OK);
            }
           
            
            TestProfileFunc();
            TestDefaults();
            TestLISP();

            TestCamera();
            CommadBarTest();
            FilterTest();
            NamedColorTest();
            RegenMethodTest();
            TestOpenDrawing();

            PropertyTest(); ///!!!!

            if (!bConsoleMode)
            {
                ToolTest(); //status property    
                TestEvents();
            }
        }

        protected override void TestAlgorithm()
        {
            if (app == null)
                app = new IMSIGX.Application(appInProcServer);

            if (!appInProcServer)
                app.Visible = true;

            if (app == null)
                throw new SDKTestExeption("Cannot create Application");
            testObj = app;

            if (!bConsoleMode || !RunAsInProcServer )
                app.Visible = true;

            base.TestAlgorithm();

            DrawingTest();
            TestRender(); //ActiveDrawing is used. Should be called after creating a drawing
            TestBooleanExtentionsCo();
            TestUndoRedo(); //starts after some actions

            SurfaceTest();
            TestTie();

            TestShaders();

            Filter filter = app.Filters[0];
            for (int i = 0; i < app.Filters.Count; ++i)
            {
                if (app.Filters[i].FilterString.Contains("TCW") == true)
                {
                    filter = app.Filters[i];
                    break;
                }
            }
            string fname = app.GetOpenFilename(filter.FilterString, "Title text", true);
            if (fname.Contains("tcw") != true)
                WriteToLog("Application.GetOpenFileName return incorrect file");

            string saveFName = app.GetSaveAsFilename("SaveFileName", filter, "title text");

            app.Quit();

            IMSIGX.Application.DetachCore();
            app.Dispose();
            app = null;
        }

        private void TestEvents()
        {
            //IMSIGX.IAppEvents es = new TestEvent();

            //IMSIGX.ImsiEventMask em = IMSIGX.ImsiEventMask.imsiEventBeforeExit |
            //    IMSIGX.ImsiEventMask.imsiEventDrawingNew;
            //int nEventID = app.ConnectEvents(es, em);

            //if (nEventID <= 0) throw new SDKTestExeption("Cannot connect event to Application");
            //app.DisconnectEvents(nEventID);
            //WriteToLog("Event test passed");
        }
        private void TestProfileFunc()
        {
            const int iVAL = 222;
            const string sVAL = "Test Profile";
            app.PutProfileInt("Sect1", "Int", iVAL);
            app.PutProfileString("Sect2", "Str", sVAL);

            int val = app.GetProfileInt("Sect1", "Int", 0);
            if (val != iVAL) throw new SDKTestExeption("Incorrect profile saving for int");

            string sval = app.GetProfileString("Sect2", "Str", "null value");
            if (sval != sVAL) throw new SDKTestExeption("Incorrect profile saving for string");
        }

        private void TestUndoRedo()
        {
            bool res = app.Undo(1);
            if (res)
                res = app.Redo(1);

            if (!res) throw new SDKTestExeption("Cannot make undo\redo for Application");
        }
        private void TestLISP()
        {
            string script = "";
            WriteToLog("LISP test is not implemented");
           // string res = app.RunTurboLISP(ref script);
        }
        private void TestDefaults()
        {
            int prevDef, prevVertexDef;
            prevDef = app.PushGraphicDefaults(GraphicDefaults.Editable);
            prevVertexDef = app.PushVertexDefaults(VertexDefaults.Calculated);
            app.PopGraphicDefaults(prevDef);
            app.PopVertexDefaults(prevVertexDef);            
        }

        private void TestCamera()
        {
            IMSIGX.Drawing camdrawing = app.Drawings.Open(TestFilesDirectory + "Camera.TCW");//for camera test

            for (int i = 0; i < camdrawing.Views.Count; i++)
            {
                IMSIGX.View view = camdrawing.Views[i];

                IMSIGX.Camera cam = (IMSIGX.Camera)view.Camera;
                if (cam == null) continue;

                IMSIGX.Graphic gr = camdrawing.Graphics[0];
               

                SDKTest.Camera camtest = new SDKTest.Camera(cam, gr);
                camtest.SetParentProperties(this);
                camtest.Test();

                gr.Dispose();
                cam.Dispose();
                view.Dispose();
            }
            camdrawing.Close(false, TestFilesDirectory +"Camera.TCW");
            camdrawing.Dispose();
            WriteToLog("Test Camera passed");
        }

        protected void TestWindow()
        {
            ;
        }
    }
}
