using System;
using System.Collections.Generic;
using System.Text;
using IMSIGX;

namespace SDKTest
{
    class Drawing: TestBase
    {
        IMSIGX.Drawing drawing=null;

        public Drawing(IMSIGX.Drawing dr) { drawing = dr; testObj = dr; }

        protected override void TestAlgorithm()
        {
            base.TestAlgorithm();
            
            TestActiveSwitch();             
            TestView();

            TestGraphics();

            TestSelection();
            //TestTie();
            TestPaperSpace();
            TestNamedUCS();

            CreateTest<IMSIGX.BrushStyle> bsTest = new CreateTest<IMSIGX.BrushStyle>(drawing.BrushStyles[0]);
            bsTest.Log = Log;
            bsTest.Test();

            CreateTest<IMSIGX.LineStyle> lsTest = new CreateTest<LineStyle>(drawing.LineStyles[0]);
            lsTest.Log = Log;
            lsTest.Test();

            System.Collections.ArrayList skipProps = new System.Collections.ArrayList();
            skipProps.Add("Scale");
            skipProps.Add("Rows");
            skipProps.Add("Columns");
            skipProps.Add("PrinterWidth");
            skipProps.Add("PrinterHeight");

            CreateTest<IMSIGX.PageSetup> psTest = new CreateTest<PageSetup>(drawing.PageSetup);
            psTest.Log = Log;
            psTest.SkipWriteProperties = skipProps;
            psTest.Test();

            CreateTest<IMSIGX.Style> styleTest = new CreateTest<Style>(drawing.Styles[0]);
            styleTest.Log = Log;
            styleTest.Test();
        }

        private void TestGraphics()
        {
            SDKTest.Graphics grTest = new SDKTest.Graphics(drawing.Graphics);
            grTest.SetParentProperties(this);
            grTest.Test();

            IMSIGX.Filter fbmp = null;
            for (int i = 0; i < drawing.Application.Filters.Count; i++)
            {
                if (drawing.Application.Filters[i].FilterString.Contains("BMP"))
                {
                    fbmp = drawing.Application.Filters[i];
                    break;
                }
            }
            IMSIGX.GraphicSets gss = drawing.GraphicSets;
            CheckObjectZero(gss);
            //IMSIGX.GraphicSet gs = drawing.GraphicSets.Add
            if (gss.Count > 0)
            {
                IMSIGX.GraphicSet gs = gss[0];
                CreateTest<IMSIGX.GraphicSet> gsTest = new CreateTest<GraphicSet>(gs);
                gsTest.SetParentProperties(this);
                gsTest.Test();
                //gs.Add(
            }
            CreateTest<IMSIGX.GraphicSets> gssTest = new CreateTest<GraphicSets>(gss);
            gssTest.SetParentProperties(this);
            gssTest.Test();
            string path = TestFilesDirectory.Substring(0, TestFilesDirectory.LastIndexOf("templates"));
            drawing.SaveAs(path+"Graphic test result.bmp", fbmp);         

        }

        protected override void TestMethods()
        {
            TestMacro();
            TestPickResult();

            drawing.InsertFile(TestFilesDirectory + "Line.tcw", true);
            drawing.InsertFile(TestFilesDirectory + "Circle.tcw", false);

            //drawing.RenderScenesGraphicsUpdate();
            drawing.RenderScenesUpdateChanges();

            drawing.SelectAll();
            drawing.UnselectAll();

            TestNamedView();
			TestBlocks();
            TestLayers();
            TestUndoRecord();
            

            IMSIGX.Filter filter = drawing.Application.Filters[0];
            int index = 0;
            for (; index < drawing.Application.Filters.Count; index++)
            {
                if (drawing.Application.Filters[index].FilterString.Contains("BMP"))
                {
                    filter = drawing.Application.Filters[index];
                    break;
                }
            }
            
            string path = TestFilesDirectory.Substring(0,TestFilesDirectory.LastIndexOf("templates"));
            drawing.SaveAs(path+"FirstSaveAscopy.tcw");
            drawing.SaveAs(path+"SecondSaveAscopy.bmp", filter);             //drawing.SaveAs("ThirdSaveAscopy.bmp", filter.Name); 
            drawing.SaveCopyAs(path+"FirstSaveCopyAscopy.tcw");             //drawing.SaveCopyAs("SecondSaveCopyAscopy.bmp", index);            
            drawing.Undo(1);
            drawing.Redo(1);
            drawing.UndoClear();


            drawing.Activate();
        }

        private void TestView()
        {
            IMSIGX.View view = drawing.ActiveView;

            SDKTest.View viewTest = new SDKTest.View(view);
            viewTest.SetParentProperties(this);

            System.Collections.ArrayList skipWriteProp = new System.Collections.ArrayList();
            skipWriteProp.Add("HWND");
            skipWriteProp.Add("Camera");
            skipWriteProp.Add("DC");
            skipWriteProp.Add("SpaceMode");
            viewTest.SkipWriteProperties = skipWriteProp;

            viewTest.Test();

            WriteToLog("View test passed");

            IMSIGX.TCObject obj = view.RenderView;
            GXRENDERS.RenderView rv = (GXRENDERS.RenderView)obj;

            if (rv == null)
            {
                WriteToLog("No RenderView to test");
                return;
            }
            System.Collections.ArrayList noWrite = new System.Collections.ArrayList();
            noWrite.Add("RadQuality");

            SDKTest.RenderView rvTest = new SDKTest.RenderView(rv);
            rvTest.SetParentProperties(this);
            rvTest.SkipWriteProperties = noWrite;
            rvTest.Test();
        }

        private void TestNamedView()
        {
            IMSIGX.NamedViews namedViews = drawing.NamedViews;
            IMSIGX.NamedView namedView = namedViews.Add("TestView", 0, 0, 1000, 1000);

            CreateTest<IMSIGX.NamedViews> nvs = new CreateTest<NamedViews>(namedViews);
            nvs.SetParentProperties(this);
            nvs.Test();

            System.Collections.ArrayList noWrite = new System.Collections.ArrayList();
            noWrite.Add("BoundingBox");
            CreateTest<IMSIGX.NamedView> nv = new CreateTest<NamedView>(namedView);
            nv.SetParentProperties(this);
            nv.SkipWriteProperties = noWrite;
            nv.Test();

            SDKTest.BoundingBox bbTest = new SDKTest.BoundingBox(namedView.BoundingBox);
            bbTest.SetParentProperties(this);
            bbTest.Test();

            namedView.Delete();

            WriteToLog("Named View test passed");
        }


        protected void TestActiveSwitch()
        {
            IMSIGX.Application app = drawing.Application;
            IMSIGX.Drawing drw = null;
            if (app.Drawings.Count == 1)
            {
                drw=(IMSIGX.Drawing)app.Drawings.Add("AaAa");
            }
            if (app.Drawings.Count > 1)
            {
                IMSIGX.Drawing active = (IMSIGX.Drawing)app.ActiveDrawing;
                for(int i=0;i<app.Drawings.Count;i++)
                {
                    IMSIGX.Drawing dr = (IMSIGX.Drawing)app.Drawings[i];
                    if(!dr.Equals(active))
                    {
                        dr.Activate();
                        break;
                    }
                }
                IMSIGX.Drawing newActive = (IMSIGX.Drawing)app.ActiveDrawing;
                if (active.Equals(newActive))
                    throw new SDKTestExeption("Cannot switch drawings");

                WriteToLog("Drawings switch test passed");
            }
            if (drw != null)
                drw.Dispose();
		}

		 protected void TestBlocks()
        {
            IMSIGX.Blocks blocks = drawing.Blocks;
            SDKTest.CreateTest<IMSIGX.Blocks> blocksTest = new CreateTest<IMSIGX.Blocks>(blocks);
            blocksTest.SetParentProperties(this);
            blocksTest.Test();

            int idx = blocks.Count;

            IMSIGX.Graphic grBlock = null;
            IMSIGX.Block block1 = blocks.Add("DummyBlock1", drawing.Graphics.AddCircle(0.0, 1.0, 0.0), 0.0, 0.0, 0.0);
            CheckObjectZero(block1);
            if ((blocks.Count - idx) != 1)
                throw new SDKTest.SDKTestExeption("Blocks Add test failed!");

            IMSIGX.Block block2 = blocks.AddXRef("XRef", TestFilesDirectory+"Line.tcw", 2.5, 2.5, 2.5);
            CheckObjectZero(block2);
            if ((blocks.Count - idx) != 2)
                throw new SDKTest.SDKTestExeption("Blocks AddXRef test failed!");

            SDKTest.Block testBlock1 = new SDKTest.Block(block1);
            testBlock1.SetParentProperties(this);
            testBlock1.Test();

            block2.Anchor = block2.Anchor;
            block2.XRefPath = block2.XRefPath;

            block1.Delete();
            if ((blocks.Count - idx) != 1)
                throw new SDKTest.SDKTestExeption("Block Delete test failed!");

            block2.Delete();
            if ((blocks.Count - idx) != 0)
                throw new SDKTest.SDKTestExeption("Block Delete test failed!");

            WriteToLog("Blocks test passed");
        }

        protected void TestLayers()
        {
            SDKTest.Layers layersTest = new SDKTest.Layers(drawing.Layers);
            layersTest.SetParentProperties(this);
            layersTest.Test();
            WriteToLog("Layers test passed");

            IMSIGX.LayersSets lss = drawing.LayersSets;//new IMSIGX.LayersSet();
            if (lss.Count > 0)
            {
                IMSIGX.LayersSet ls = lss[0];
                ls.Activate();
                if (!ls.Active)
                    throw new SDKTest.SDKTestExeption("Layerset test failed");
                ls.AddLayer(drawing.Layers[0].Name);
                if (ls.Count == 0)
                    throw new SDKTest.SDKTestExeption("Layerset test failed");
                ls.RemoveLayer(drawing.Layers[0].Name);
                ls.Clear();
                ls.Delete();
                CreateTest<IMSIGX.LayersSet> lsTest = new CreateTest<LayersSet>(ls);
                lsTest.SetParentProperties(this);
                lsTest.Test();
            }
            CreateTest<IMSIGX.LayersSets> lssTest = new CreateTest<LayersSets>(lss);
            lssTest.SetParentProperties(this);
            lssTest.Test();
        }

        protected void TestSelection()
        {
            //drawing.SelectAll(); //! failed
            //drawing.Graphics.Select();//! failed
            drawing.Graphics.Unselect();


            IMSIGX.Drawing drw = drawing.Application.Drawings.Add();
            IMSIGX.Graphic gr = drw.Graphics.AddCircle(0, 0, 0);
            gr.Select();

            CheckObjectZero(drw.Selection);
            CreateTest<IMSIGX.Selection> selTest = new CreateTest<IMSIGX.Selection>(drw.Selection);
            selTest.SetParentProperties(this);
            selTest.Test();
            if (drw.Selection.Count != 1)
                WriteToLog("        Drawing.Selection test failed");
            else
                WriteToLog("Selection test passed");
            drw.Close(false);
        }

        protected void TestMacro()
        {
            int idx = drawing.Macros.Count;
            drawing.Macros.Add("name", "path");
            if (drawing.Macros[idx].Path != "path")
                throw new SDKTest.SDKTestExeption("Macro test failed");
            drawing.Macros[idx].Delete();
            if ((drawing.Macros.Count - idx) != 0)
                throw new SDKTest.SDKTestExeption("Macros test failed");
            WriteToLog("Macros test passed");

        }

        protected void TestUndoRecord()
        {
            IMSIGX.UndoRecord urec = drawing.AddUndoRecord("test undo");
            if (urec.MenuText != "test undo")
                throw new SDKTest.SDKTestExeption("UndoRecord test failed");
            IMSIGX.Graphic gr = drawing.Graphics.AddCircle(1.0, 2.0, 0.3);
            urec.AddGraphic(gr);
            gr.Name = "name1";
            //urec.DeleteGraphic(gr);

            //urec.AddGraphicForModify(gr);
            //urec.DeleteGraphics(gr);
            //urec.DeleteGraphics(//I cannot add graphics!

            urec.Close();
            //urec.
            WriteToLog("Undo record test passed");
            
        }

        protected void TestPickResult()
        {
            drawing.Graphics.AddLineSingle(0.0, 0.0, 0.0, 1.0, 1.0, 0.0);
            drawing.Graphics.AddLineSingle(0.0, 0.0, 0.0, 2.0, 1.0, 0.0);
            drawing.Graphics.AddLineSingle(0.0, 0.0, 0.0, 1.0, 2.0, 0.0);
            double x; double y; double z;
            drawing.ActiveView.WorldToView(0.0, 0.0, 0.0,out x,out y,out z);
            IMSIGX.PickResult pir = drawing.ActiveView.PickPoint(x, y, 3.0, true, true, true, true, true, false);

            if (pir.Count > 0)
            {
                CreateTest<IMSIGX.PickResult> PiRtest = new CreateTest<PickResult>(pir);
                PiRtest.SetParentProperties(this);
                PiRtest.Test();
                CreateTest<IMSIGX.PickEntry> PiTest = new CreateTest<PickEntry>(pir[0]);
                PiTest.SetParentProperties(this);
                PiTest.Test();
            }
            else
            {
                WriteToLog("Pick Result test failed");
            }


        }

        private void TestTie()
        {
            IMSIGX.TCObject obj = drawing.TieSets;
            GXTIES.TieSets tss = (GXTIES.TieSets)obj;

            IMSIGX.Graphic g1 = drawing.Graphics[0];
            IMSIGX.Graphic g2 = drawing.Graphics[1];

            SDKTest.TieSet tsTest = new SDKTest.TieSet(tss,g1,g2);
            tsTest.SetParentProperties(this);
            tsTest.Test();
        }

        private void TestPaperSpace()
        {
            IMSIGX.TCObject obj = drawing.PaperSpaces;
            GXMPS.PaperSpaces pss = (GXMPS.PaperSpaces)obj;

            if (pss == null)
            {
                WriteToLog("No PaperSpace objcet to test");
                return;
            }

            SDKTest.PaperSpace psTest = new SDKTest.PaperSpace(pss);
            psTest.SetParentProperties(this);
            psTest.Test();
        }

        private void TestNamedUCS()
        {
            IMSIGX.TCObject obj = drawing.NamedUCSs;
            GXNAMEDUCS.NamedUCSs ucs = (GXNAMEDUCS.NamedUCSs)obj;

            if (ucs == null)
            {
                WriteToLog("No NamedUCS to test");
                return;
            }
            SDKTest.NamedUCS test = new SDKTest.NamedUCS(ucs);
            test.SetParentProperties(this);
            test.Test();
        }

    }
}
