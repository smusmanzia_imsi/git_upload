using System;
using System.Collections.Generic;
using System.Text;

namespace SDKTest
{
    class Layers: TestBase
    {
        IMSIGX.Layers layers_ = null;

        public Layers(IMSIGX.Layers layers)
        {
            layers_ = layers;
            testObj = layers;
        }

        protected override void TestMethods()
        {
            //bool bVisible = true;
            //bool bEditable = true;
            //bool bFrozen = false;

            //layers_.Add("TestLayer1", ref bVisible, ref bEditable,ref bFrozen
            if (layers_.Count > 0)
            {
                SDKTest.Layer layerTest = new SDKTest.Layer(layers_[0]);
                layerTest.SetParentProperties(this);
                layerTest.Test();
            }
            WriteToLog("Test Layers passed");
        }
    }

    class Layer : TestBase
    {
        IMSIGX.Layer layer_ = null;

        public Layer(IMSIGX.Layer layer)
        {
            layer_ = layer;
            testObj = layer;

            System.Collections.ArrayList skipWriteProp = new System.Collections.ArrayList();
            System.Collections.ArrayList skipReadProp = new System.Collections.ArrayList();

            skipWriteProp.Add("PrintStyle");//!
            skipWriteProp.Add("LineStyle");//!
            skipWriteProp.Add("BrushStyle");//!

            skipReadProp.Add("PrintStyle");//!
            
            SkipWriteProperties = skipWriteProp;
            SkipReadProperties = skipReadProp;
        }

        protected override void TestMethods()
        {
            //layer_
            TestAdd();
            IMSIGX.Graphic gr = layer_.AddLineSingle(5.0, 5.0, 5.0, 11.0, 11.0, 11.0);
            if (layer_.GraphicFromID(gr.ID).Vertices.Count != gr.Vertices.Count)
                throw new SDKTest.SDKTestExeption("Layer.GraphicFromID test failed");
            //layer_.Select();
            //if (layer_
            int cnt = layer_.Count;
            layer_.Remove(0);
            if ((cnt - layer_.Count) != 1)
                throw new SDKTest.SDKTestExeption("Layer.Remove test failed");
            cnt = layer_.Count;
            /*layer_.Unselect();
            layer_[0].Select();
            layer_.Clear(IMSIGX.ImsiGraphicFlags.imsiGfSelected);
            if ((cnt - layer_.Count) != 1)
                throw new SDKTest.SDKTestExeption("Layer.Clear test failed");*/

            
            WriteToLog("Layer test passed");
        }

        private void TestAdd()
        {
            IMSIGX.Graphic gr = layer_.AddLineSingle(1.0, 1.0, 1.0, 11.0, 11.0, 11.0);
            //CheckObjectZero(layer_.Add((int)gr.TypeByValue, gr.RegenType, false, null, null, null));

            CheckObjectZero(layer_.AddDot(0.0, 10, 10));
            CheckObjectZero(layer_.AddArcCenterAndPoint(10, 10, 0, 20, 20, 0, 0, 3));
            CheckObjectZero(layer_.AddArcDoublePoint(20, 20, 0, 30, 30, 0, 1.5, 3));
            CheckObjectZero(layer_.AddArcElliptical(30, 30, 0, 40, 40, 0, 0, 1.5));
            CheckObjectZero(layer_.AddArcEllipticalFixedRatio(34, 34, 0, 67, 54, 0, 2, 0, 3.14));
            CheckObjectZero(layer_.AddArcRotatedElliptical(100, 100, 0, 122, 122, 0, 144, 144, 0, 0, 3.14));
            CheckObjectZero(layer_.AddArcTriplePoint(10, 0, 0, -10, 0, 0, 0, 10, 0));
            CheckObjectZero(layer_.AddCircle(60, 70, 0));
            CheckObjectZero(layer_.AddCircleCenterAndPoint(40, 40, 0, 60, 60, 0));
            CheckObjectZero(layer_.AddCircleDoublePoint(60, 90, 0, 30, 60, 0));
            CheckObjectZero(layer_.AddCircleTriplePoint(70, 80, 0, 50, 30, 0, 30, 39, 0));
            //CheckObjectZero(layer_.AddConstructionAngularLine(70, 46, 0, 20, 21, 0));//!
            //CheckObjectZero(layer_.AddConstructionCenterAndPointCircle(200, 200, 0, 300, 300, 0));//!
            //CheckObjectZero(layer_.AddConstructionDoublePointCircle(150, 150, 0, 300, 300, 0));//!
            //CheckObjectZero(layer_.AddConstructionHorizontalLine(200, 200, 0));//!
            //CheckObjectZero(layer_.AddConstructionTriplePointCircle(10, 0, 0, -10, 0, 0, 0, 10, 0));//!
            //CheckObjectZero(layer_.AddConstructionVerticalLine(60, 60, 60));//!
            CheckObjectZero(layer_.AddCross(40, 40, 40));
            CheckObjectZero(layer_.AddCurveBezier(70, 80, 0));
            CheckObjectZero(layer_.AddCurveSpline(69, 69, 0));
            CheckObjectZero(layer_.AddEllipse(20, 20, 0, 50, 50, 0));
            CheckObjectZero(layer_.AddEllipseFixedRatio(60, 70, 0, 70, 90, 0, 2));
            CheckObjectZero(layer_.AddEllipseRotatedEllipse(0, 0, 0, 10, 10, 10, 20, 20, 20));
            CheckObjectZero(layer_.AddLineMultiline(90, 90, 0));
            CheckObjectZero(layer_.AddLinePolygon(10, 10, 0, 20, 20, 0, 4));
            CheckObjectZero(layer_.AddLineRectangle(30, 30, 0, 50, 50, 0));
            CheckObjectZero(layer_.AddLineRotatedRectangle(20, 20, 0, 30, 30, 0, 40));
            CheckObjectZero(layer_.AddLineSingle(60, 80, 0, 70, 10, 0));
            CheckObjectZero(layer_.AddSquare(20, 20, 0));
            CheckObjectZero(layer_.AddStar(100, 100, 0));
            CheckObjectZero(layer_.AddText("text", 50, 50, 0, 40, 0, 100, IMSIGX.ImsiTextJustification.imsiAlignCenter));

            //layer_.AddGraphic(gr, null, null);

            WriteToLog("Graphic Add test passed");
        }
    }
}
