using System;
using System.Collections.Generic;
using System.Text;
using IMSIGX;

namespace SDKTest
{
    class Block : TestBase
    {
        IMSIGX.Block block_ = null;

        public Block(IMSIGX.Block block)
        { 
            block_ = block; 
            testObj = block;
            System.Collections.ArrayList skipWriteProp = new System.Collections.ArrayList();

            skipWriteProp.Add("Anchor");//!
            skipWriteProp.Add("Application");
            skipWriteProp.Add("Drawing");
            skipWriteProp.Add("Index");
            skipWriteProp.Add("Parent");
            skipWriteProp.Add("XRef");
            skipWriteProp.Add("XRefPath");//!
            SkipWriteProperties = skipWriteProp;
        }

        protected override void TestMethods()
        {
            IMSIGX.Graphic gr0 = block_.AddAttributeDefinition("Tag", 0.0, 0.0, 0.0, 2.0);
            if ((gr0 == null))
                WriteToLog("Block AddAttributeDefinition test failed!");

            IMSIGX.Graphic gr = block_.AddAttributeDefinition("Tag", 1.0, 1.0, 1.0, 2.0, 30.0, 15.0, 0.25, 0, 0, "Default", "Prompt");
            if ((gr == null))
                WriteToLog("Block AddAttributeDefinition (2) test failed!");
        }
    }

    /*class Blocks : TestBase
    {
        IMSIGX.Blocks blocks_ = null;

        public Blocks(IMSIGX.Blocks blocks) { blocks_ = blocks; testObj = blocks; }

        protected override void TestMethods()
        {
            int cnt = blocks_.Count;
        }
    }*/
}
