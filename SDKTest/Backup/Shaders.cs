using System;
using IMSIGX;

namespace SDKTest
{
    public class ShaderSet : TestBase
    {
        GXLWSHADERS.ShaderSet sset = null;
        GXLWSHADERS.Shader shader = null;

        public ShaderSet(GXLWSHADERS.ShaderSet shaderSet) { sset = shaderSet; testObj = shaderSet; }

        protected override void TestMethods()
        {
            for (int i = 0; i < sset.Count; i++)
            {
                if (shader == null)
                    shader = sset[i];
                else if (sset[i].PropertyCount > 0)
                    shader = sset[i];
            }

            if (shader == null)
            {
                WriteToLog("No Shader object to test");
                return;
            }

            System.Collections.ArrayList noWrite = new System.Collections.ArrayList();
            noWrite.Add("Type");

            CreateTest<GXLWSHADERS.Shader> shaderTest = new CreateTest<GXLWSHADERS.Shader>(shader);
            shaderTest.SetParentProperties(this);
            shaderTest.SkipWriteProperties = noWrite;
            shaderTest.Test();

            if (shader.PropertyCount == 0)
            {
                WriteToLog("No shader properties to test");
                return;
            }

            string name = shader.GetPropertyName(0);
            string type = shader.GetPropertyType(name);
            GXLWSHADERS.ImsiShaderPropType propType = shader.GetPropertyTypeByValue(name);
            object value = shader.GetItem(0);
            shader.SetItem(0, value);
        }
    }

    public class Environment : TestBase
    {
        GXLWSHADERS.Environments env = null;

        public Environment(GXLWSHADERS.Environments environments) 
        {
            env = environments;
            testObj = environments;
        }

        protected override void TestMethods()
        {
            GXLWSHADERS.ShaderSet sset = env.GetItem(0, 0);
        }
    }

    public class Lunimance : TestBase
    {
        GXLWSHADERS.Luminances lum = null;

        public Lunimance(GXLWSHADERS.Luminances luminance)
        {
            lum = luminance;
            testObj = luminance;
        }

        protected override void TestMethods()
        {
            GXLWSHADERS.ShaderSet sset = lum.GetItem(0,0);
        }
    }

    public class Material : TestBase
    {
        GXLWSHADERS.Materials mat = null;

        public Material(GXLWSHADERS.Materials material)
        {
            mat = material;
            testObj = material;
        }

        protected override void TestMethods()
        {
            GXLWSHADERS.ShaderSet sset = mat.GetItem(0, 0);

            SDKTest.ShaderSet ssetTest = new SDKTest.ShaderSet(sset);
            ssetTest.SetParentProperties(this);
            ssetTest.Test();
        }
    }
}