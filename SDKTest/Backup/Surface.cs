using System;
using System.Collections.Generic;
using System.Text;
using GXSURFACE;

namespace SDKTest
{
    class Surface : TestBase
    {
        GXSURFACE.Surface surface = null;

        public Surface(GXSURFACE.Surface srf) { surface = srf; testObj = srf; }

        protected override void TestMethods()
        {
            surface.Smooth(ImsiSmoothQuality.imsiSurfNotSmooth);

            GXSURFACE.Facets facets = surface.Facets;
            GXSURFACE.Edges edges = surface.Edges;
            GXSURFACE.Nodes nodes = surface.Nodes;

            TestFacets(facets);
            TestEdges(edges);
            TestNodes(nodes);
        }

        private void TestFacets(GXSURFACE.Facets facets)
        {
            GXSURFACE.Facet facet = null;
            for (int i = 0; i < facets.Count; i++)
                facet = facets[i];

            if (facet == null) throw new SDKTestExeption("No facet to test");

            CreateTest<GXSURFACE.Facet> ft = new CreateTest<Facet>(facet);
            System.Collections.ArrayList nr = new System.Collections.ArrayList();
            nr.Add("Plane");
            ft.SkipReadProperties = nr;
            ft.SetParentProperties(this);
            ft.Test();

            double[]  plane = facet.Plane;

            Point3D[] norm = new Point3D[] { new Point3D(0, 0, 1), new Point3D(0, 0, 1), new Point3D(0, 0, 1) };
            Point2D[] tex = new Point2D[] { new Point2D(0, 0), new Point2D(0, 0), new Point2D(0, 0) };

            int[] ind = new int[] { 0, 1, 2 };
            CheckObjectZero(facets.Add(ind, tex, norm, false, false));

            Point3D[] point = new Point3D[] { new Point3D(0, 0, 0), new Point3D(10, 10, 0), new Point3D(0, 10, 0) };
            GXSURFACE.Facet f = facets.Add(point, tex, norm, true, true);

            TestContours(f.Contours);

            f.Delete();

            WriteToLog("GXSURFACE.Facet test passed");            
        }

        private void TestEdges(GXSURFACE.Edges edges)
        { 
            GXSURFACE.Edge edge = null;
            for (int i = 0; i < edges.Count; i++)
                edge = edges[i];

            if (edge == null) throw new SDKTestExeption("No edge to test");

            CreateTest<GXSURFACE.Edge> edgeTest = new CreateTest<Edge>(edge);
            edgeTest.SetParentProperties(this);
            edgeTest.Test();

            Edge e = null;
            Point3D[] point = new Point3D[] { new Point3D(0,0,0), new Point3D(10,10,10)};
         
            int[] ind = new int[2]{0,1};

            Point3D[] norm = new Point3D[] { new Point3D(0, 0, 1), new Point3D(0, 0, 1) };
            Point2D[] tex = new Point2D[] { new Point2D(0, 0), new Point2D(0, 0) };

            e = edges.Add(point, tex,norm);

            e = edges.Add(ind, tex, norm);
            e.Delete();

            //GXSURFACE.Surface surf = edges.Surface;
            //if (surf.Nodes.Count >= 2)
            //{
            //    GXSURFACE.Node node1 = surf.Nodes[0];
            //    GXSURFACE.Node node2 = surf.Nodes[1];
            //    IntPtr n1 = System.Runtime.InteropServices.Marshal.AllocHGlobal(System.Runtime.InteropServices.Marshal.SizeOf(node1));
            //    IntPtr n2 = System.Runtime.InteropServices.Marshal.AllocHGlobal(System.Runtime.InteropServices.Marshal.SizeOf(node2));
            //    System.Runtime.InteropServices.Marshal.StructureToPtr( node1, n1, false );
            //    System.Runtime.InteropServices.Marshal.StructureToPtr( node2, n2, false );

            //    IntPtr[] nodes = new IntPtr[] { n1, n2 };
            //    e = edges.Add(nodes, tex, norm);
            //}
            //else WriteToLog("Add edge by node ptr skipped");
            WriteToLog("GXSURFACE.Edge test passed");
        }

        private void TestNodes(GXSURFACE.Nodes nodes)
        {
            GXSURFACE.Node node = null;
            for (int i = 0; i < nodes.Count; i++)
                node = nodes[i];

            if (node == null) throw new SDKTestExeption("No node to test");

            CreateTest<GXSURFACE.Node> ntest = new CreateTest<Node>(node);
            ntest.SetParentProperties(this);
            ntest.Test();

            GXSURFACE.Node node1 = nodes.Add(0, 0, 0);
            CheckObjectZero(node1);

            GXSURFACE.Node node2 = nodes.Add(10, 10, 0, 0, 0);
            CheckObjectZero(node2);

            node2 = nodes.Add(20, 20, 0, 0, 0, 1);
            CheckObjectZero(node2);

            GXSURFACE.Facet facet = node.Surface.Facets[0];
            GXSURFACE.Node fn = facet.Nodes[0];
            double X=0, Y=0, Z=0;
            //fn.GetNorm(facet, ref X, ref Y, ref Z);
            fn.GetNormEx(facet, ref X, ref Y, ref Z);
            //fn.GetUV(facet, ref X, ref Y );
            fn.SetNorm(facet, 0, 0, 1);
            //fn.SetUV(facet, 0, 0);

            fn.Delete();

            WriteToLog("GXSURFACE.Node test passed");
        }

        private void TestContours(GXSURFACE.Contours contours)
        {
            GXSURFACE.Contour cont = null;
            for (int i = 0; i < contours.Count; i++)
                cont = contours[i];

            if (cont == null) throw new SDKTestExeption("No contour to test");

            CreateTest<GXSURFACE.Contour> ctest = new CreateTest<Contour>(cont);
            ctest.SetParentProperties(this);
            ctest.Test();

            CreateTest<GXSURFACE.Contours> cstest = new CreateTest<Contours>(contours);
            cstest.SetParentProperties(this);
            cstest.Test();

            WriteToLog("GXSURFACE.Contour test passed");
        }
    }
}