using System;
using System.Collections.Generic;
using System.Text;

namespace SDKTest
{
    public partial class TestApplication : TestBase
    {
        private void CommadBarTest()
        {
            IMSIGX.CommandBars cbs = app.CommandBars;
            if (cbs == null) return;

            for (int i = 0; i < app.CommandBars.Count; i++)
            {
                IMSIGX.CommandBar cb = app.CommandBars[i];
            }
            
            if (cbs.Count > 0)
            {
                IMSIGX.CommandBar cb = cbs[0];
                CreateTest<IMSIGX.CommandBar> cbTest = new CreateTest<IMSIGX.CommandBar>(cb);
                cbTest.SetParentProperties(this);
                cbTest.Test();
                cb.Dispose();
            }
            cbs.Dispose();
        }

        private void FilterTest()
        {
            for (int i = 0; i < app.Filters.Count; i++)
            {
                IMSIGX.Filter filter = app.Filters[i];
            }

            CreateTest<IMSIGX.Filter> fTest = new CreateTest<IMSIGX.Filter>(app.Filters[0]);
            fTest.SetParentProperties(this);
            fTest.Test();            
        }

        private void NamedColorTest()
        {
            if (app.NamedColors.Count == 0) return;

            for (int i = 0; i < app.NamedColors.Count; i++)
            {
                IMSIGX.NamedColor nc = app.NamedColors[i];
            }

            CreateTest<IMSIGX.NamedColor> ncTest = new CreateTest<IMSIGX.NamedColor>(app.NamedColors[0]);
            ncTest.SetParentProperties(this);
            ncTest.Test();        
        }

        private void PropertyTest()
        {
            IMSIGX.Properties props = app.Properties;

            for (int i = 0; i < app.Properties.Count; i++)
            {
                IMSIGX.Property pr = app.Properties[i];
            }
           
            CreateTest<IMSIGX.Property> prTest = new CreateTest<IMSIGX.Property>(props[0]);
            prTest.SetParentProperties(this);
            prTest.Mode = true;
            prTest.Test();     
        }

        private void RegenMethodTest()
        {
            if (app.RegenMethods.Count == 0) return;

            for (int i = 0; i < app.RegenMethods.Count; i++)
            {
                IMSIGX.RegenMethod rm = app.RegenMethods[i];
            }

            CreateTest<IMSIGX.RegenMethod> rmTest = new CreateTest<IMSIGX.RegenMethod>(app.RegenMethods[0]);
            rmTest.SetParentProperties(this);
            rmTest.Test();            
        }

        private void ToolTest()
        {
            if (app.Tools.Count == 0) return;

            IMSIGX.Tool tool = app.Tools[0];

            for (int i = 0; i < app.Tools.Count; i++)
            {
                IMSIGX.Tool t = app.Tools[i];
            }

            CreateTest<IMSIGX.Tool> toolTest = new CreateTest<IMSIGX.Tool>(tool);
            toolTest.SetParentProperties(this);
            toolTest.Test();            
        }

        private void TestOpenDrawing()
        {
            string filename1 = TestFilesDirectory+"Empty.tcw";
            string filename2 = TestFilesDirectory+"Line.tcw";

            IMSIGX.Drawing drawing = app.Drawings.Open(filename1);//, true, filter);

            if (drawing == null)
                throw new SDKTestExeption("Cannot open drawing");

            IMSIGX.Drawing drawing2 = app.Drawings.Open(filename2);//, true, filter);

            if (drawing2 == null)
                throw new SDKTestExeption("Cannot open second drawing");

            drawing.Close(false, filename1);
            drawing2.Close(true, filename2);

            drawing.Dispose(); //Deletes the line to test CLR & TC coworking
            drawing2.Dispose(); //Deletes the line to test CLR & TC coworking
           
            WriteToLog("Open Drawing test passed");
        }

        private void DrawingTest()
        {
            IMSIGX.Drawing drawing = app.Drawings.Add("New Drawing");

            SDKTest.Drawing drawingTest = new SDKTest.Drawing(drawing);
            drawingTest.SetParentProperties(this);
            drawingTest.Test();
        }

        private void SurfaceTest()
        {
            string filename = TestFilesDirectory + "cube.tcw";
            IMSIGX.Drawing drawing = (IMSIGX.Drawing)app.Drawings.Open(filename);

            IMSIGX.Graphic gr = null;

            for (int i = 0; i < drawing.Graphics.Count; i++)
            {
                gr = (IMSIGX.Graphic)drawing.Graphics[i];
                IMSIGX.TCObject obj = gr.Surface;
                GXSURFACE.Surface surface = (GXSURFACE.Surface)obj;
                if (surface != null)
                {
                    SDKTest.Surface stest = new SDKTest.Surface(surface);
                    stest.SetParentProperties(this);;
                    stest.Test();
                    break;
                }
            }

            drawing.Close(false, filename);
            drawing.Dispose();
        }

        private void TestTie()
        {
            IMSIGX.TCObject obj = app.TieMethods;
            GXTIES.TieMethods tm = (GXTIES.TieMethods)obj;
            if (tm == null)
            {
                WriteToLog("Cannot test tie methods");
                return;
            }
            SDKTest.TieMethos tmTest = new SDKTest.TieMethos(tm);
            tmTest.SetParentProperties(this);
            tmTest.Test();
        }
        
        private void TestRender()
        {
            IMSIGX.TCObject obj = app.Renders;
            GXRENDERS.Renders renders = (GXRENDERS.Renders)obj;
            if (renders == null)
            {
                WriteToLog("No Render to test");
                return;
            }
            IMSIGX.Drawing drw = app.Drawings.Add();

            SDKTest.Render rendTest = new SDKTest.Render(renders, drw);
            rendTest.SetParentProperties(this);
            rendTest.Test();
        }

        private void TestBooleanExtentionsCo()
        {
            SDKTest.Boolean3D boolTest = new SDKTest.Boolean3D(app);
            boolTest.SetParentProperties(this);
            boolTest.Test();

            SDKTest.Graphic3GOp grOp = new SDKTest.Graphic3GOp(app);
            grOp.SetParentProperties(this);
            grOp.Test();

            //SDKTest.DwgExt dwgTest = new SDKTest.DwgExt(app);
            //dwgTest.SetParentProperties(this);
            //dwgTest.Test();
         }

        private void TestShaders()
        {
            IMSIGX.TCObject obj=null;
            obj = app.Environments;
            GXLWSHADERS.Environments env = (GXLWSHADERS.Environments)obj;

            if (env != null)
            {
                SDKTest.Environment envTest = new SDKTest.Environment(env);
                envTest.SetParentProperties(this);
                envTest.Test();
            }

            obj = app.Luminances;
            GXLWSHADERS.Luminances lum = (GXLWSHADERS.Luminances)obj;

            if (lum != null)
            {
                SDKTest.Lunimance lumTest = new SDKTest.Lunimance(lum);
                lumTest.SetParentProperties(this);
                lumTest.Test();
            }

            obj = app.Materials;
            GXLWSHADERS.Materials mat = (GXLWSHADERS.Materials)obj;

            if (mat != null)
            {
                SDKTest.Material matTest = new SDKTest.Material(mat);
                matTest.SetParentProperties(this);
                matTest.Test();
            }
        }
    }
}
