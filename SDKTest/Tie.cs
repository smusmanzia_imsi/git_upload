using System;
using System.Collections.Generic;
using IMSIGX;

namespace SDKTest
{
    public class TieMethos : TestBase
    {
        GXTIES.TieMethods tms = null;
        GXTIES.TieMethod tm = null;

        public TieMethos(GXTIES.TieMethods t) { tms = t; testObj = t; }

        protected override void TestMethods()
        {
            for (int i = 0; i < tms.Count; i++)
                tm = tms[i];

            if (tm == null)
            {
                WriteToLog("TieMethod is null");
                return;
            }

            CreateTest<GXTIES.TieMethod> tmTest = new CreateTest<GXTIES.TieMethod>(tm);
            tmTest.SetParentProperties(this);
            tmTest.Test();
        }
    }

    public class TieSet : TestBase
    {
        GXTIES.TieSets tss = null;
        GXTIES.TieSet ts = null;

        IMSIGX.Graphic graphic1 = null;
        IMSIGX.Graphic graphic2 = null;

        public TieSet(GXTIES.TieSets t, IMSIGX.Graphic gr1, IMSIGX.Graphic gr2) 
        { 
            tss = t; 
            testObj = t;
            graphic1 = gr1;
            graphic2 = gr2;
        }

        protected override void TestMethods()
        {
            CheckObjectZero(tss.Add("NewTie", graphic1, graphic2, GXTIES.imsiTieFlag.imsiTieAll));   

            for (int i = 0; i < tss.Count; i++)
                ts = tss[i];
           
            if (ts == null)
            {
                WriteToLog("No TieSet to test");
                return;
            }                   

            CreateTest<GXTIES.TieSet> tsTest = new CreateTest<GXTIES.TieSet>(ts);
            tsTest.SetParentProperties(this);
            tsTest.Test();
        }
    }
}