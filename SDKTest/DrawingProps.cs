using System;
using System.Collections.Generic;
using System.Text;
using IMSIGX;

namespace SDKTest
{
    class BoundingBox : TestBase
    {
        IMSIGX.BoundingBox bbox = null;
        IMSIGX.BoundingBox bbox2 = null;

        public BoundingBox(IMSIGX.BoundingBox bb) { bbox = bb; bbox2 = bb; testObj = bb; }

        protected override void TestMethods()
        {
            bool testOk = true;
            IMSIGX.ImsiDirection dir = bbox.DirToNearestFace(bbox.Max, true);
            if (dir == ImsiDirection.imsiDirNone)
            {
                WriteToLog("BoundingBox test cannot defines Direction to nearest object");
                testOk = false;
            }

            bbox.Intersect(bbox2);
            if(bbox.Min.X != 0)
            {
                WriteToLog("BoundingBox test cannot intersect");
                testOk = false;
            }

            bbox2.Min.X += 100;

            bbox.Subtract(bbox2);

            bbox2.Set(bbox.Min, 100);
            if (bbox2.Max.X != bbox.Min.X + 100)
            {
                WriteToLog("BoundingBox test cannot set BB value");
                testOk = false;
            }
            bbox2.Union(bbox);
          
            if (testOk == false)
                throw new SDKTestExeption("BoundingBox test has above problem(s)");
        }
    }

    class Vertex : TestBase
    {
        IMSIGX.Vertices vertexs = null;

        public Vertex(IMSIGX.Vertices vert) { vertexs = vert; testObj = vert; }

        protected override void TestMethods()
        {
            IMSIGX.Vertex vert = null;
            for (int i = 0; i < vertexs.Count; i++)
                vert = (IMSIGX.Vertex)vertexs[i];

            if (vert == null) throw new SDKTestExeption("Has no Vertex to test");

            CreateTest<IMSIGX.Vertex> vtest = new CreateTest<IMSIGX.Vertex>(vert);
            vtest.SetParentProperties(this);
            vtest.Test();

            IMSIGX.Vertex vertCopy = (IMSIGX.Vertex)vert.Duplicate();
            vertCopy.Delete();

            vert = (IMSIGX.Vertex)vertexs.Add(0, 0, 0);
        //    CheckObjectZero(vert);
            //vertexs.AddVertex(vert);

            vert = (IMSIGX.Vertex)vertexs.Remove(vertexs.Count-1);
            CheckObjectZero(vertexs.AddClose(true, true, true, true, false, false));
        }
    }

    public class Matrix : TestBase
    {
        IMSIGX.Matrix mat = null;

        public Matrix(IMSIGX.Matrix m) { mat = m; testObj = m; }

        protected override void TestMethods()
        {
            bool testOk = true;
            IMSIGX.Matrix mat2 = (IMSIGX.Matrix)mat.Duplicate();
            CheckObjectZero(mat2);

            System.Double[] values = new double[16];
            mat.GetArray(ref values);
            if (values.GetLength(0) == 0)
            {
                WriteToLog("Matrix test cannot get array");
                testOk = false;
            }

            mat2.Identity();
            mat2.GetEntries(ref values[0], ref values[1], ref values[2], ref values[3], 
                ref values[4], ref values[5], ref values[6], ref values[7], 
                ref values[8], ref values[9], ref values[10], ref values[11], 
                ref values[12], ref values[13], ref values[14], ref values[15]);

            if (values[0] != mat2.GetEntry(0, 0))
            {
                WriteToLog("Matrix test cannot get values");
                testOk = false;
            }

            mat.Invert();
            mat.Rotate(1,1,1,0,0,0,0,true);
            mat.Scale(1,1,1,0,0,0,true);
            mat.SetArray(ref values);
            mat.SetEntries(2,4,6,9,8,8,0,8,0,7,9,6,6,6,7,8);
            mat.SetEntry(0,0,99.0);
            mat.Translate(0,0,0,false);
            mat.Transpose();
            mat.TranslateScaleAndRotateZ(0,0,0,1,1,1,0,0,0,0,false);

            if(testOk==false)
                throw new SDKTestExeption("Matrix test has above problem(s)");
        }
    }
}