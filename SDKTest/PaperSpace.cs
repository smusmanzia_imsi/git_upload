using System;
using System.Collections.Generic;
using IMSIGX;

namespace SDKTest
{
    public class PaperSpace : TestBase
    {
        GXMPS.PaperSpaces pss = null;
        GXMPS.PaperSpace ps = null;

        public PaperSpace(GXMPS.PaperSpaces p) { pss = p; testObj = p; }

        protected override void TestMethods()
        {
            ps = pss.Add("NewPageSpace");
            CheckObjectZero(ps);

            CheckObjectZero(pss[0]);

            CreateTest<GXMPS.PaperSpace> psTest = new CreateTest<GXMPS.PaperSpace>(ps);
            psTest.SetParentProperties(this);
            psTest.Test();

            ps.SelectAll();
            ps.UnselectAll();

            //ps.Activate();
            //if (ps.Active == false)
            //    throw new SDKTestExeption("Cannot activate PaperSpace");

            ps.Delete();
        }
    }
}