using System;
using System.Collections.Generic;
using System.Text;
using IMSIGX;

namespace SDKTest
{
    class Graphics : TestBase
    {
        IMSIGX.Graphics grs = null;

        public Graphics(IMSIGX.Graphics _grs) { grs = _grs; testObj = _grs; }

        protected override void TestMethods()
        {
            IMSIGX.Graphic before = null, after = null, curGr = null;

            before = grs[1];
            after = grs[0];
            curGr = grs[grs.Count - 1];

            IMSIGX.Graphic line = grs.AddLineParallel(curGr, 30, 30, 0);
            CheckObjectZero(line);

            line = grs.AddLinePerpendicular(line, 50, 50, 0, 60, 60, 0);
            CheckObjectZero(line);

            IMSIGX.Matrix mat = grs.Drawing.UCS;
            mat.Identity();
            //IMSIGX.BoundingBox box = grs.CalcBoundingBox(mat);
            //CheckObjectZero(box);

            //IMSIGX.Block block = grs.CreateBlock("TestBlock", 0, 0, 0);
            //Graphic iGr = null;
            //IMSIGX.Block block = grs.CreateBlock("TestBlock", 0, 0, 0, false, false, iGr);
            //IMSIGX.Block block1 = grs.CreateBlock("TestBlock", 0, 0, 0, false, false, line);
            //CheckObjectZero(block);

            //grs.AddBlockInsertion(block, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0);

            //SDKTest.Block blockTest = new SDKTest.Block(block);
            //blockTest.SetParentProperties(this);
            //blockTest.Test();


            System.Collections.Generic.List<double> list = new List<double>(12);
            grs.CreateDragOutline(list.ToArray());

            //IMSIGX.Matrix 
            mat = grs.MoveRelative(10, 10, 10);
           // mat = grs.MoveAbsolute(0, 0, 0);

            mat = grs.RotateAxis(3.14, 0, 0, 1, 0, 1, 1);
//            mat = grs.Scale(1, 1, 1);
            grs.Transform(mat);
            grs.Select();
            grs.Unselect();

            System.Collections.ArrayList noWrite = new System.Collections.ArrayList();
            noWrite.Add("LineStyle");
            noWrite.Add("Layer");
            noWrite.Add("BrushStyle");
            noWrite.Add("Block");
            SDKTest.Graphic grTest = new Graphic((IMSIGX.Graphic)grs[0]);
            grTest.Log = Log;
            grTest.SkipWriteProperties = noWrite;
            grTest.Test();

            grs.Remove(grs.Count - 1);

            SDKTest.Matrix matTest = new SDKTest.Matrix((IMSIGX.Matrix)mat);
            matTest.SetParentProperties(this);
            matTest.Test();
        }

        private void TestAdd()
        {
            CheckObjectZero(grs.AddDot(0.0, 10, 10));
            CheckObjectZero(grs.AddArcCenterAndPoint(10, 10, 0, 20, 20, 0, 0, 3));
            CheckObjectZero(grs.AddArcDoublePoint(20, 20, 0, 30, 30, 0, 1.5, 3));
            CheckObjectZero(grs.AddArcElliptical(30, 30, 0, 40, 40, 0, 0, 1.5));
            CheckObjectZero(grs.AddArcEllipticalFixedRatio(34, 34, 0, 67, 54, 0, 2, 0, 3.14));
            CheckObjectZero(grs.AddArcRotatedElliptical(100, 100, 0, 122, 122, 0, 144, 144, 0, 0, 3.14));
            CheckObjectZero(grs.AddArcTriplePoint(10, 0, 0, -10, 0, 0, 0, 10, 0));
            CheckObjectZero(grs.AddCircle(60, 70, 0));
            CheckObjectZero(grs.AddCircleCenterAndPoint(40, 40, 0, 60, 60, 0));
            CheckObjectZero(grs.AddCircleDoublePoint(60, 90, 0, 30, 60, 0));
            CheckObjectZero(grs.AddCircleTriplePoint(70, 80, 0, 50, 30, 0, 30, 39, 0));
            CheckObjectZero(grs.AddConstructionAngularLine(70, 46, 0, 20, 21, 0));
            CheckObjectZero(grs.AddConstructionCenterAndPointCircle(200, 200, 0, 300, 300, 0));
            CheckObjectZero(grs.AddConstructionDoublePointCircle(150, 150, 0, 300, 300, 0));
            CheckObjectZero(grs.AddConstructionHorizontalLine(200, 200, 0));
            CheckObjectZero(grs.AddConstructionTriplePointCircle(10, 0, 0, -10, 0, 0, 0, 10, 0));
            CheckObjectZero(grs.AddConstructionVerticalLine(60, 60, 60));
            CheckObjectZero(grs.AddCross(40, 40, 40));
            CheckObjectZero(grs.AddCurveBezier(70, 80, 0));
            CheckObjectZero(grs.AddCurveSpline(69, 69, 0));
            CheckObjectZero(grs.AddEllipse(20, 20, 0, 50, 50, 0));
            CheckObjectZero(grs.AddEllipseFixedRatio(60, 70, 0, 70, 90, 0, 2));
            CheckObjectZero(grs.AddEllipseRotatedEllipse(0, 0, 0, 10, 10, 10, 20, 20, 20));
            CheckObjectZero(grs.AddLineMultiline(90, 90, 0));
            CheckObjectZero(grs.AddLinePolygon(10, 10, 0, 20, 20, 0, 4));
            CheckObjectZero(grs.AddLineRectangle(30, 30, 0, 50, 50, 0));
            CheckObjectZero(grs.AddLineRotatedRectangle(20, 20, 0, 30, 30, 0, 40));
            CheckObjectZero(grs.AddLineSingle(60, 80, 0, 70, 10, 0));
            CheckObjectZero(grs.AddSquare(20, 20, 0));
            CheckObjectZero(grs.AddStar(100, 100, 0));
            CheckObjectZero(grs.AddText("text", 50, 50, 0, 40, 0, 100, IMSIGX.ImsiTextJustification.imsiAlignCenter));

            WriteToLog("Graphic Add test passed");
        }

        protected override void TestAlgorithm()
        {
            TestAdd();
            base.TestAlgorithm();
        }
    }

    class Graphic:TestBase
    {
        IMSIGX.Graphic graphic = null;

        public Graphic(IMSIGX.Graphic gr) { graphic = gr; testObj = gr; }

        protected override void TestMethods()
        {
            graphic.ArcSet(0, 0, 0, 10, 5, 0, 3.14, 0);

            graphic.BringToFront();
            graphic.SendToBack();

            IMSIGX.Graphic grcopy = (IMSIGX.Graphic)graphic.Clone(ImsiGraphicCopyFlags.imsiGceCopyRootVertices);
            grcopy.Close();

            System.Collections.Generic.List<double> list = new List<double>(12);
            graphic.CreateDragOutline(list.ToArray());

            IMSIGX.Graphic gr = (IMSIGX.Graphic)graphic.Duplicate();
            gr.Delete();

            IMSIGX.GraphicSet set = graphic.Explode();

            //IMSIGX.ArcData arcdata = graphic.GetArcData();
            double dist = graphic.GetDistance(0, 0, 0);

            System.Collections.Generic.List<double> verts = new List<double>();
            System.Collections.Generic.List<int> indeces = new List<int>();
            System.Collections.Generic.List<bool> flags = new List<bool>();
            //  graphic.GetFaceData(verts, indeces.ToArray(), flags.ToArray());

            //int m = 1, n = 1, count = 0;
            // graphic.GetFaceDimension(ref m,ref n, ref count);

            IMSIGX.Graphic link = graphic.GetSubjectLink(0);

            IMSIGX.Matrix mat = graphic.MoveAbsolute(1, 1, 1);
            mat = graphic.MoveRelative(1, 1, 1);

            int val = graphic.RegenLock();
            graphic.RegenUnlock(val);

            mat = graphic.RotateAxis(1, 0, 10, 0, 10, 0, 0);
            mat = graphic.Scale(1, 1, 1);
            //graphic.Select();
            //bool selval = graphic.Selected;
            //if (selval == false)
            //    throw new SDKTestExeption("Cannot select Graphic");
            //graphic.Unselect();

            graphic.Transform(mat);

            //graphic.TextSet("Text", 0, 0, 0, 100, 20, 0);
            graphic.Update();

            SDKTest.Vertex vertexTest = new SDKTest.Vertex(graphic.Vertices);
            vertexTest.SetParentProperties(this);
            vertexTest.Test();
        }
    }
}
