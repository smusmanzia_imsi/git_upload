using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using IMSIGX;
using System.IO;

namespace SDKTest
{
    public class TestTemplates
    {
       
    }

    public class CreateTest<T> : TestBase
    {
        public CreateTest(T t) { testObj = t; }
    }

    public class SDKTestExeption : ApplicationException
    {
        public SDKTestExeption() : base() { }
        public SDKTestExeption(String message) : base(message) { }
    }

    public class TestBase
    {
        const string strValue = "StrValue";

        //if true test no test UI properties and methods
        protected System.Object testObj = null;
        protected bool bConsoleMode = true;
        StreamWriter log = null;

        System.Collections.ArrayList noWriteProps = null;
        System.Collections.ArrayList noReadProps = null;

        string testFilesDir = "TestFiles\\templates\\";

        public string TestFilesDirectory
        {
            set { testFilesDir = value; }
            get { return testFilesDir; }
        }

        public System.Collections.ArrayList SkipWriteProperties
        {
            set { noWriteProps = value;  }
        }

        public System.Collections.ArrayList SkipReadProperties
        {
            set { noReadProps = value; }
        }

        protected void WriteToLog(string str)
        {
            if (log != null)
                log.WriteLine(str);
            else
                System.Console.WriteLine(str);
        }

        protected  void CheckObjectZero<T>(T obj)
        {
            if (obj == null)
            {
                WriteToLog("Cannot create TC object: "+obj.GetType().FullName);
            }
        }

        public void SetParentProperties(TestBase obj)
        {
            this.Log = obj.Log;
            this.Mode = obj.Mode;
            this.TestFilesDirectory = obj.TestFilesDirectory;
        }

        public virtual void TestProperties()
        {
            System.Collections.ArrayList errorListR = new System.Collections.ArrayList();
            System.Collections.ArrayList errorListW = new System.Collections.ArrayList();

            Type t = testObj.GetType();

            foreach (System.Reflection.PropertyInfo pi in t.GetProperties())
            {
                if (pi.CanRead)
                {
                    try
                    {
                        if (noReadProps != null && noReadProps.Contains(pi.Name))
                            continue;
                                               
                        object val = null;
                        object[] ind = { 0 };

                        System.Reflection.ParameterInfo[] paramsInfo = pi.GetIndexParameters();
                        if (paramsInfo.Length != 0)
                            val = pi.GetValue(testObj, ind);
                        else
                            val = pi.GetValue(testObj, null);
                    }
                    catch (Exception)
                    {
                        errorListR.Add(pi.Name);
                        //throw new SDKTestExeption("Cannot read property:"+pi.Name);
                    }

                }
                if ( !Mode && pi.CanWrite)
                {
                    try
                    {
                        if (noWriteProps != null && noWriteProps.Contains(pi.Name))
                            continue;

                        Type propType = pi.PropertyType;

                        System.Object defValue = null;
                        if (propType.Equals(typeof(string)))
                            defValue = strValue;
                        else
                            defValue = System.Activator.CreateInstance(propType);

                        object[] ind = { 0 };

                        System.Reflection.ParameterInfo[] paramsInfo = pi.GetIndexParameters();

                        if (paramsInfo.Length != 0)
                            pi.SetValue(testObj, defValue, ind);
                        else
                            pi.SetValue(testObj, defValue, null);
                    }
                    catch (Exception)
                    {
                        errorListW.Add(pi.Name);
                        //throw new SDKTestExeption("Cannot set property value: "+pi.Name);
                    }
                }
            }
            if (errorListW.Count > 0 || errorListR.Count > 0)
            {
                if (errorListR.Count > 0)
                {
                    WriteToLog("Cannot read properties:\n");
                    foreach (string name in errorListR)
                        WriteToLog(name+"\n");
                }
                if (errorListW.Count > 0)
                {
                    WriteToLog("Cannot write properties:\n");
                    foreach (string name in errorListW)
                        WriteToLog(name+"\n");
                }
                throw new SDKTestExeption("Exeption in read/write properties of "+testObj.ToString());
            }
        }

        protected virtual void TestMethods() { }

        protected virtual void TestAlgorithm()
        {
            TestProperties();
            TestMethods();
        }

        public void Test()
        {
            try
            {
                TestAlgorithm();
            }
            catch (SDKTestExeption e)
            {
                WriteToLog("SDK exeption: " + e.ToString());
                WriteToLog(this.GetType().ToString() + " test failed\n");
                return;
            }
            catch (Exception e)
            {
                WriteToLog("Unknown exeption: " + e.ToString());
                WriteToLog(this.GetType().ToString() + " test failed\n");
                return;
            }

            WriteToLog(this.GetType().ToString() + " test passed\n");
        }

        public bool Mode
        {
            get { return bConsoleMode; }
            set { bConsoleMode = value; }
        }

        public StreamWriter Log
        {
            set { log = value; }
            get { return log; }
        }
    }

}