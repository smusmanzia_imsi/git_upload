using System;
using System.Collections.Generic;
using System.Text;

namespace SDKTest
{
    class Camera : TestBase
    {
        IMSIGX.Camera cam_ = null;
        IMSIGX.Graphic gr_ = null; //vertex adding;

        public Camera(IMSIGX.Camera cam, IMSIGX.Graphic gr)
        {
            cam_ = cam;
            gr_ = gr;
            testObj = cam;


            System.Collections.ArrayList skipWriteProp = new System.Collections.ArrayList();
            skipWriteProp.Add("Up");//!
            skipWriteProp.Add("Right");//!
            skipWriteProp.Add("Parent");
            skipWriteProp.Add("Application");
            skipWriteProp.Add("LookAt");//!
            skipWriteProp.Add("Location");//!
            skipWriteProp.Add("Direction");//!
            SkipWriteProperties = skipWriteProp;
        }

        protected override void TestMethods()
        {
            ////TODO: Add testing after vertex fixing
            IMSIGX.Vertex vtPos = gr_.Vertices.Add(1.0, 1.0, 1.0);
            IMSIGX.Vertex vtLook = gr_.Vertices.Add(10.0, 1.0, 1.0);
            IMSIGX.Vertex vtUp = gr_.Vertices.Add(10.0, 4.0, 2.0);
            cam_.CameraSetSpaceParameters(vtPos, vtLook, vtUp);
            //if (!(CompareVertex(cam_.LookAt, vtLook)
            //    && CompareVertex(cam_.Up, vtUp)
            //    && CompareVertex(cam_.Location, vtPos)))
            //     throw new SDKTest.SDKTestExeption("Camera.CameraSetSpaceParameters test failed");

            //cam_.Up = vtPos;
            //if (!CompareVertex(cam_.Up, vtPos))
            //    throw new SDKTest.SDKTestExeption("Camera.Up set test failed");

            //cam_.Right = vtPos;
            //if (!CompareVertex(cam_.Right, vtPos))
            //    throw new SDKTest.SDKTestExeption("Camera.Right set test failed");

            //cam_.LookAt = vtPos;
            //if (!CompareVertex(cam_.LookAt, vtPos))
            //    throw new SDKTest.SDKTestExeption("Camera.LookAt set test failed");

            //cam_.Location = vtPos;
            //if (!CompareVertex(cam_.Location, vtPos))
            //    throw new SDKTest.SDKTestExeption("Camera.Location set test failed");

            //cam_.Direction = vtPos;
            //if (!CompareVertex(cam_.Direction, vtPos))
            //    throw new SDKTest.SDKTestExeption("Camera.Direction set test failed");

            vtPos.Dispose();
            vtLook.Dispose();
            vtUp.Dispose();

        }


        public static bool CompareVertex(IMSIGX.Vertex v1, IMSIGX.Vertex v2) 
        {
            return CompareVertex(v1, v2, 0.0001);
        }

        public static bool CompareVertex(IMSIGX.Vertex v1, IMSIGX.Vertex v2, double prec)
        {
            if ((Math.Abs(v1.X - v2.X) < prec)
                && (Math.Abs(v1.Y - v2.Y) < prec)
                && (Math.Abs(v1.Z - v2.Z) < prec))
                return true;
            return false;

        }
    }
}
