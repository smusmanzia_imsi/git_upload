Attribute VB_Name = "Module1"
Option Explicit

Type SelInfo
    nV As Long
    xV() As Double
    yV() As Double
    zV() As Double
End Type


Type ArcData
    NumCen As Long
    ArcCen(100) As Long
    xMid(100) As Double
    yMid(100) As Double
End Type




Global objApp As XApplication
Global Drs As Drawings
Global actDr As Drawing
Global Sel As Selection
Global Grs As Graphics
Global Gr As Graphic
Global GrRes As Graphic
Global Vis As Views
Global Vi As View
Global PRes As PickResult
Global PEnt As PickEntry
Global Const Pi = 3.14159265358979
Global Const Eps = 0.000001
Global GRAPHICTYPE As String
Global ARCTYPE As String
Public Function MakeGraphic() As Graphic
    Set MakeGraphic = New XGraphic 'Graphic - for Debug and XGraphic - for correct work
End Function
Public Function MakeVertex() As Vertex
    Set MakeVertex = New XVertex 'Vertex - for Debug and XVertex - for correct work
End Function
  
Sub Main()
End Sub

