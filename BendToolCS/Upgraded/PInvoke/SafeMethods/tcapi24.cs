using System.Runtime.InteropServices;

namespace BendToolSupport.PInvoke.SafeNative
{
	public static class tcapi24
	{

		public static int ACISGraphicGetNormalAtPoint(int Gr, int vEyePosition, int vPointOfView)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.ACISGraphicGetNormalAtPoint(Gr, vEyePosition, vPointOfView);
		}
		public static int TCWBendBody(int gBody, int hVfirstpoint, int hVsecondpoint, int hVbenddir, int hVlockpoint, double BendRad, double BendAngle, double NeutralDepth, double BendWidth, int BendPlaneFixed)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWBendBody(gBody, hVfirstpoint, hVsecondpoint, hVbenddir, hVlockpoint, BendRad, BendAngle, NeutralDepth, BendWidth, BendPlaneFixed);
		}
		public static int TCWDrawingActive()
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWDrawingActive();
		}
		public static double TCWGetX(int vH)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWGetX(vH);
		}
		public static double TCWGetY(int vH)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWGetY(vH);
		}
		public static double TCWGetZ(int vH)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWGetZ(vH);
		}
		public static int TCWGraphicAt(int d, int Index)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWGraphicAt(d, Index);
		}
		public static int TCWGraphicDraw(int hG, int mode)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWGraphicDraw(hG, mode);
		}
		public static int TCWSetXYZ(int hVer, double x, double y, double z)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWSetXYZ(hVer, x, y, z);
		}
		public static int TCWVertexAppend(int hG1, double x, double y, double z)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWVertexAppend(hG1, x, y, z);
		}
		public static int TCWVertexAt(int g, int Index)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWVertexAt(g, Index);
		}
		public static int TCWVertexDispose(int hVer)
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWVertexDispose(hVer);
		}
		public static int TCWViewActive()
		{
			return BendToolSupport.PInvoke.UnsafeNative.tcapi24.TCWViewActive();
		}
	}
}