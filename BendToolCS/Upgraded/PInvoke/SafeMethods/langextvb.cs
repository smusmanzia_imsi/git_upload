using System.Runtime.InteropServices;

namespace BendToolSupport.PInvoke.SafeNative
{
	public static class langextvb
	{

		public static object LoadLangBSTRString(ref string strModule, int nID, int wLanguage)
		{
			return BendToolSupport.PInvoke.UnsafeNative.langextvb.LoadLangBSTRString(ref strModule, nID, wLanguage);
		}
	}
}