using System.Runtime.InteropServices;

namespace BendToolSupport.PInvoke.SafeNative
{
	public static class dbapi24
	{

		public static int DrawingGetActiveViewport(int hDr)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.DrawingGetActiveViewport(hDr);
		}
		public static int DrawingGetWorkPlaneMatrix(int hDr)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.DrawingGetWorkPlaneMatrix(hDr);
		}
		public static void GraphicDispose(ref int hGr)
		{
			BendToolSupport.PInvoke.UnsafeNative.dbapi24.GraphicDispose(ref hGr);
		}
		public static double GraphicGetClosestFacet(int hVer, int hRoot, ref int FacLevel, ref int IntCount, ref int hgAct, int hgHit, int hvHit, int hvHitNor, int hVp, int Flags, int hVpCurvAxis1, int hVpCurvAxis2)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.GraphicGetClosestFacet(hVer, hRoot, ref FacLevel, ref IntCount, ref hgAct, hgHit, hvHit, hvHitNor, hVp, Flags, hVpCurvAxis1, hVpCurvAxis2);
		}
		public static int GraphicGetID(int hGr)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.GraphicGetID(hGr);
		}
		public static int GraphicSetMatrix(int hGr, int hMat, int norm)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.GraphicSetMatrix(hGr, hMat, norm);
		}
		public static void GraphicSetPenColor(int hGr, int color)
		{
			BendToolSupport.PInvoke.UnsafeNative.dbapi24.GraphicSetPenColor(hGr, color);
		}
		public static void GraphicSetPenStyle(int hGr, int penStyle)
		{
			BendToolSupport.PInvoke.UnsafeNative.dbapi24.GraphicSetPenStyle(hGr, penStyle);
		}
		public static void GraphicSetPenWidth(int hGr, double penWidth)
		{
			BendToolSupport.PInvoke.UnsafeNative.dbapi24.GraphicSetPenWidth(hGr, penWidth);
		}
		public static void GraphicSetSelected(int hGr, int Selected)
		{
			BendToolSupport.PInvoke.UnsafeNative.dbapi24.GraphicSetSelected(hGr, Selected);
		}
		public static void MatrixDispose(ref int hMat)
		{
			BendToolSupport.PInvoke.UnsafeNative.dbapi24.MatrixDispose(ref hMat);
		}
		public static int MatrixGetPlane(int hM, ref double a, ref double b, ref double c, ref double d, short orient)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.MatrixGetPlane(hM, ref a, ref b, ref c, ref d, orient);
		}
		public static int MatrixMatrixMultiplyEx(int hMat1, int hMat2)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.MatrixMatrixMultiplyEx(hMat1, hMat2);
		}
		public static int MatrixNewEx()
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.MatrixNewEx();
		}
		public static int MatrixOrthoInvertEx(int hMat)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.MatrixOrthoInvertEx(hMat);
		}
		public static int MatrixPlaneFrom(int hv0, int hVx, int hVy)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.MatrixPlaneFrom(hv0, hVx, hVy);
		}
		public static void VertexDispose(ref int hVer)
		{
			BendToolSupport.PInvoke.UnsafeNative.dbapi24.VertexDispose(ref hVer);
		}
		public static int VertexNewEx(double x, double y, double y_Renamed)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.VertexNewEx(x, y, y_Renamed);
		}
		public static int VertexNewFromEx(int hVer)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.VertexNewFromEx(hVer);
		}
		public static int VertexTransform(int hVer, int hMat)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.VertexTransform(hVer, hMat);
		}
		public static int ViewportGetMatrix(int hVi)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.ViewportGetMatrix(hVi);
		}
		public static void WorkPlaneSetMatrix(int hDr, int hMat)
		{
			BendToolSupport.PInvoke.UnsafeNative.dbapi24.WorkPlaneSetMatrix(hDr, hMat);
		}
		public static int WorldToWorkPlane(int hDr, int hVer1, int hVer2, int hmVp, int hVp)
		{
			return BendToolSupport.PInvoke.UnsafeNative.dbapi24.WorldToWorkPlane(hDr, hVer1, hVer2, hmVp, hVp);
		}
	}
}