using System.Runtime.InteropServices;

namespace BendToolSupport.PInvoke.SafeNative
{
	public static class user32
	{

		public static int GetClassLong(int hwnd, int nIndex)
		{
			return BendToolSupport.PInvoke.UnsafeNative.user32.GetClassLong(hwnd, nIndex);
		}
		public static int SetClassLong(int hwnd, int nIndex, int dwNewLong)
		{
			return BendToolSupport.PInvoke.UnsafeNative.user32.SetClassLong(hwnd, nIndex, dwNewLong);
		}
	}
}