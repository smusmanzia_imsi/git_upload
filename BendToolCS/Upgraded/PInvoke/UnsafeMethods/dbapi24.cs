using System.Runtime.InteropServices;

namespace BendToolSupport.PInvoke.UnsafeNative
{
	[System.Security.SuppressUnmanagedCodeSecurity]
	public static class dbapi24
	{

		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int AppGetCurrentApp();
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int DrawingGetActiveViewport(int hDr);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int DrawingGetWorkPlaneMatrix(int hDr);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void GraphicDispose(ref int hGr);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void GraphicDrawEx2(int hGr, int hMat, int hVp, int DrawMode, int Flags);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static double GraphicGetClosestFacet(int hVer, int hRoot, ref int FacLevel, ref int IntCount, ref int hgAct, int hgHit, int hvHit, int hvHitNor, int hVp, int Flags, int hVpCurvAxis1, int hVpCurvAxis2);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GraphicGetID(int hGr);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GraphicGetMatrix(int hGr);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GraphicGetPenColor(int hGr);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GraphicNewEx(int hApp, int rm, int kind, int hGrBase, int MakeVert);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GraphicSetMatrix(int hGr, int hMat, int norm);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void GraphicSetPenColor(int hGr, int color);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void GraphicSetPenStyle(int hGr, int penStyle);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void GraphicSetPenWidth(int hGr, double penWidth);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void GraphicSetSelected(int hGr, int Selected);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void MatrixDispose(ref int hMat);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short MatrixGetPlane(int hM, ref double a, ref double b, ref double c, ref double d, short orient);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int MatrixMatrixMultiplyEx(int hMat1, int hMat2);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int MatrixNewEx();
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int MatrixOrthoInvertEx(int hMat);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int MatrixPlaneFrom(int hv0, int hVx, int hVy);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void VertexDispose(ref int hVer);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int VertexNewEx(double x, double y, double y_Renamed);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int VertexNewFromEx(int hVer);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int VertexTransform(int hVer, int hMat);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int ViewportGetMatrix(int hVi);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void WorkPlaneSetMatrix(int hDr, int hMat);
		[DllImport("DBAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int WorldToWorkPlane(int hDr, int hVer1, int hVer2, int hmVp, int hVp);
	}
}