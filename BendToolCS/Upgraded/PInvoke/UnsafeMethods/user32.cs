using System.Runtime.InteropServices;

namespace BendToolSupport.PInvoke.UnsafeNative
{
	[System.Security.SuppressUnmanagedCodeSecurity]
	public static class user32
	{

		[DllImport("User32.dll", EntryPoint = "GetClassLongA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetClassLong(int hwnd, int nIndex);
		[DllImport("User32.dll", EntryPoint = "LoadCursorA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int LoadCursor(int hInstance, int lpCursorName);
		[DllImport("User32.dll", EntryPoint = "SetClassLongA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int SetClassLong(int hwnd, int nIndex, int dwNewLong);
		[DllImport("User32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int SetCursor(int lCursor);
	}
}