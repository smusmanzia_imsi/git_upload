using System.Runtime.InteropServices;

namespace BendToolSupport.PInvoke.UnsafeNative
{
	[System.Security.SuppressUnmanagedCodeSecurity]
	public static class tcapi24
	{

		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int ACISGraphicGetNormalAtPoint(int Gr, int vEyePosition, int vPointOfView);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWBendBody(int gBody, int hVfirstpoint, int hVsecondpoint, int hVbenddir, int hVlockpoint, double BendRad, double BendAngle, double NeutralDepth, double BendWidth, int BendPlaneFixed);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWDrawingActive();
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static double TCWGetX(int vH);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static double TCWGetY(int vH);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static double TCWGetZ(int vH);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWGraphicAppend(int hG1, int hG2);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWGraphicAt(int d, int Index);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWGraphicCreate(int kind, [MarshalAs(UnmanagedType.VBByRefStr)] ref string rm);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWGraphicDispose(int hG);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWGraphicDraw(int hG, int mode);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWGraphicHandleFromID(int Id);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWSetXYZ(int hVer, double x, double y, double z);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWVertexAppend(int hG1, double x, double y, double z);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWVertexAt(int g, int Index);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWVertexDispose(int hVer);
		[DllImport("TCAPI24.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TCWViewActive();
	}
}