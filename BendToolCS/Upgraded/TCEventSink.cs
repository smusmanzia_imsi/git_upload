using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using IMSIGX;
using TCDotNetInterfaces;
using UpgradeHelpers.Helpers;


namespace Bend
{
    class TCEventSink : IMSIGX.IAppEvents
    {
        BendTool m_Tool;
        

        public TCEventSink(BendTool Tool)
        {
            this.m_Tool = Tool;
        }

        #region IAppEvents Members
        public void BeforeDoubleClick(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, Selection Sel, ref bool Cancel)
        {

        }

        //public void BeforeExit(IMSIGX.Application TheApp, ref bool Cancel)
        //{
        //    System.Windows.Forms.MessageBox.Show("Before Exit");
        //}

        public void BeforeRightClick(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, Selection Sel, ref bool Cancel)
        {

        }

        public void CommandBarControlDone(CommandBarControl WhichControl)
        {

        }

        public void CommandBarControlHit(CommandBarControl WhichControl, ref bool Cancel)
        {

        }

        public void CommandBarControlStatus(CommandBarControl WhichControl)
        {

        }

        public void DrawingAfterSave(Drawing WhichDrawing)
        {

        }

        //public void DrawingBeforeClose(Drawing WhichDrawing, ref bool Cancel)
        //{

        //}

        //public void DrawingBeforeSave(Drawing WhichDrawing, ref bool SaveAs, ref bool Cancel)
        //{

        //}

        void IAppEvents.DrawingNew(Drawing WhichDrawing)
        {
            System.Windows.Forms.MessageBox.Show("Drawing New");
            try
            {
                m_Tool.Finish();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Drawing New Failed.");
            }
            return;
        }

        public void Drop(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, Selection Sel)
        {

        }

        public void MouseUp(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, ImsiMouseButton Button, int Shift, int X, int Y, ref bool Cancel)
        {

        }

        public void PointPick(Drawing WhichDrawing, IMSIGX.View WhichView, PickResult Result, bool PickWasCanceled)
        {
            System.Windows.Forms.MessageBox.Show("PointPicked");
        }

        public void PointSnapped(Drawing WhichDrawing, IMSIGX.View WhichView, int X, int Y, Vertex PointRaw, Vertex PointSnapped)
        {

        }

        public void PolygonPick(Drawing WhichDrawing, IMSIGX.View WhichView, PickResult Result, bool PickWasCanceled)
        {

        }

        public void RectanglePick(Drawing WhichDrawing, IMSIGX.View WhichView, PickResult Result, bool PickWasCanceled)
        {
            System.Windows.Forms.MessageBox.Show("Rectangle Pick");
        }

        public void SelectionChange(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, Selection Sel)
        {

        }

        //public void ViewAfterRedraw(Drawing WhichDrawing, IMSIGX.View WhichView)
        //{

        //}

        public void ViewBeforeRedraw(Drawing WhichDrawing, IMSIGX.View WhichView)
        {

        }

        public void VirtualIntersectionPick(Drawing WhichDrawing, IMSIGX.View WhichView, PickResult Result, bool PickWasCanceled)
        {

        }

        public void WindowActivate(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow)
        {
            System.Windows.Forms.MessageBox.Show("Window Activate");
        }

        public void WindowDeactivate(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow)
        {

        }

        public void WindowResize(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow)
        {

        }
        void IAppEvents.DrawingDeactivate(Drawing WhichDrawing)
        {
            try
            {
                int hwndVw = 0;
                IMSIGX.View WhichView = Module1.objApp.ActiveDrawing.ActiveView;
                try
                {
                    hwndVw = Convert.ToInt32(WhichView.HWND);
                    if (hwndVw == m_Tool.hwndVwCur)
                    {
                        BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(m_Tool.hwndVwCur, BendTool.GCL_HCURSOR, m_Tool.hCursorCur);
                        m_Tool.hwndVwCur = 0;
                        m_Tool.hCursorCur = 0;
                    }
                    return;
                }
                catch (Exception exc)
                {
                    //NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
                    System.Windows.Forms.MessageBox.Show(exc.ToString());
                }

                return;
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Drawing Deactivate Failed.");
            }
            return;
        }
        void IAppEvents.DrawingBeforeClose(Drawing WhichDrawing, ref bool Cancel)
        {
            try
            {
                m_Tool.Finish();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Drawing Before Close Failed.");
            }
            return;
        }
        void IAppEvents.DrawingBeforeSave(Drawing WhichDrawing, ref bool SaveAs, ref bool Cancel)
        {
            try
            {
                m_Tool.Finish();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Drawing Before Save Failed.");
            }
            return;
        }
        void IAppEvents.ViewAfterRedraw(IMSIGX.Drawing WhichDrawing, IMSIGX.View WhichView)
        {
            
        }

        void IAppEvents.MouseDown(IMSIGX.Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, ImsiMouseButton Button, int Shift, int x, int y, ref bool Cancel)
        {
            //object imsiLeftButton = null;
            double yV = 0, xV = 0, zV = 0;
            int hDr = 0;
            int hmDr = 0;
            int hmInv = 0;
            int hV = 0;
            
            try
            {
                Module1.ActDr = WhichDrawing;
                Module1.Grs = Module1.ActDr.Graphics; //ReflectionHelper.GetMember<GXTIESLib.Graphics>(Module1.ActDr, "Graphics");
                //Set Vis = ActDr.Views
                Module1.Vi = WhichView;
                hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                Module1.Vi.ScreenToView(x, y, out xV, out yV);

                PickResult PRes = null;
                int PicCount = 0;
                int ChildCount = 0;
                string GrType = "";
                int hVy = 0, hv0 = 0, hVx = 0, hVz = 0;
                int hMNewPlane = 0;
                double yW0 = 0, xW0 = 0, zW0 = 0;
                int Snap = 0;
                double ya = 0, xa = 0, za = 0;
                int m_gKind = 0;
                int m_rm = 0;
                int hDrMat = 0;
                double cDr = 0, aDr = 0, bDr = 0, dDr = 0;
                double yDrLoc = 0, xDrLoc = 0, zDrLoc = 0;
                double yW = 0, xW = 0, zW = 0;
                double dl = 0;
                double L1 = 0;
                int hvPosView = 0;
                int Res = 0;
                
                IMSIGX.Graphic GrLine = null;
                IMSIGX.Vertex Ver = null;
                IMSIGX.Graphic GrChild = null;
                IMSIGX.Graphic Gr = null;
                if (Button == ImsiMouseButton.imsiLeftButton)
                {
                    Cancel = true;
                    if (m_Tool.state == 0)
                    { // Select ACIS solid graphic

                        //object tempRefParam = Type.Missing;
                        //object tempRefParam2 = Type.Missing;
                        //object tempRefParam3 = Type.Missing;
                        //object tempRefParam4 = Type.Missing;
                        //object tempRefParam5 = Type.Missing;
                        //object tempRefParam6 = Type.Missing;
                        //object tempRefParam7 = Type.Missing;
                        PRes = Module1.Vi.PickPoint(xV, yV, 1d,true,false,false,false,false,false);//, ref tempRefParam, ref tempRefParam2, ref tempRefParam3, ref tempRefParam4, ref tempRefParam5, ref tempRefParam6, ref tempRefParam7);
                        PicCount = PRes.Count;
                        if (PicCount == 0)
                        {
                            return;
                        }
                        hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                        for (int i = 0; i <= PicCount - 1; i++)
                        {
                            //object tempRefParam8 = i;
                            Gr = PRes.get_Item(i).Graphic;
                            //i = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam8);
                            GrType = Gr.Type;// ReflectionHelper.GetMember<string>(Gr, "Type");
                            if (GrType == "TCW60ACISSOLID")
                            {
                                m_Tool.GrBody = Gr;
                                m_Tool.state = 1;
                                break;
                            }
                            else
                            {
                                ChildCount = Gr.Graphics.Count;// ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(Gr, "Graphics"), "Count");
                                for (int j = 0; j <= ChildCount - 1; j++)
                                {
                                    GrChild = Gr.Graphics.get_Item(j); //ReflectionHelper.Invoke<IMSIGX.Graphic>(Gr, "Graphics", new object[] { j });
                                    GrType = GrChild.Type;// ReflectionHelper.GetMember<string>(GrChild, "Type");
                                    if (GrType == "TCW60ACISSOLID")
                                    {
                                        m_Tool.GrBody = Gr;
                                        m_Tool.state = 1;
                                        break;
                                    }
                                }
                            }
                            if (m_Tool.state == 1)
                            {
                                break;
                            }
                        }
                        if (m_Tool.state == 1)
                        {
                            //OldBodyCol = GrBody.Properties("PenColor")
                            //GrBody.Properties("PenColor") = RGB(255, 0, 0)
                            //GrBody.Draw
                            m_Tool.hGrBody = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, m_Tool.GrBody.Index);// ReflectionHelper.GetMember<int>(m_Tool.GrBody, "Index"));
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetSelected(m_Tool.hGrBody, 1);
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(m_Tool.hGrBody, 0);
                            m_Tool.CreateCoordinateGraphic();
                            //m_Tool.AddLM(false);
                            m_Tool.AddLM(true);
                            //ReflectionHelper.Invoke(m_Tool.theToolEvents, "ToolChangePrompt", new object[] { this, "", false });
                            //ReflectionHelper.Invoke(m_Tool.theToolEvents, "ToolChangePrompt", new object[] { this, CommonLocaliz.TcLoadLangString(139), false });
                            //Dim hwndVw As Long
                            //    On Error Resume Next
                            //    hwndVw = WhichView.hwnd
                            //    If (hwndVw <> hwndVwCur) Then
                            //        If (hwndVwCur <> 0) Then
                            //            SetClassLong hwndVwCur, GCL_HCURSOR, hCursorCur
                            //        End If
                            //        hwndVwCur = hwndVw
                            //        hCursorCur = GetClassLong(hwndVwCur, GCL_HCURSOR)
                            //        If (hCursorCur <> 0) Then
                            //            SetClassLong hwndVwCur, GCL_HCURSOR, 0
                            //        End If
                            //    End If

                            //''''''    SetCursor hToolCursor

                        }
                        return;
                    }

                    if (m_Tool.state == 1)
                    { // Set drawing work plane by face
                        hv0 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(0, 0, 0);
                        hVx = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(0, 0, 0);
                        hVy = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(0, 0, 0);
                        hVz = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(0, 0, 0);
                        if (!m_Tool.CreateFaceGraphicEx(xV, yV, hv0, hVx, hVy, hVz))
                        {
                            return;
                        }
                        Module1.Vi.Refresh(); // for grid
                        System.Windows.Forms.MessageBox.Show("4");
                        hMNewPlane = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixPlaneFrom(hv0, hVx, hVy);
                        hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                        BendToolSupport.PInvoke.SafeNative.dbapi24.WorkPlaneSetMatrix(hDr, hMNewPlane);

                        BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hv0);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVx);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVy);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVz);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref hMNewPlane);
                        m_Tool.state = 2;
                        //AddLM False
                        //AddLM True
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, "", false });
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, CommonLocaliz.TcLoadLangString(136), false });
                        m_Tool.hGrFace = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, m_Tool.GrFace.Index);// ReflectionHelper.GetMember<int>(m_Tool.GrFace, "Index"));
                        System.Windows.Forms.MessageBox.Show("5");
                        return;
                    }

                    if (m_Tool.state == 2)
                    { // select first point of bending line
                        hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                        Snap = Module1.ActDr.Application.SnapModes;// ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes");
                        //MsgBox ("Snap=" & CStr(Snap))
                        //object tempRefParam9 = Type.Missing;
                        //object tempRefParam10 = Type.Missing;
                        //object tempRefParam11 = Type.Missing;
                        //object tempRefParam12 = Type.Missing;
                        //object tempRefParam13 = Type.Missing;
                        //object tempRefParam14 = Type.Missing;
                        //object tempRefParam15 = Type.Missing;
                        PRes = Module1.Vi.PickPoint(xV, yV, 0.0001d, true, false, false, true, true, false);//, ref tempRefParam9, ref tempRefParam10, ref tempRefParam11, ref tempRefParam12, ref tempRefParam13, ref tempRefParam14, ref tempRefParam15);
                        PicCount = PRes.Count;
                        if (PicCount == 0 || Snap < 2)
                        {
                            m_Tool.ViewToDrUCS(xV, yV, zV, ref xW0, ref yW0, ref zW0);
                        }
                        else
                        {
                            //object tempRefParam16 = 0;
                            Ver = PRes.get_Item(0).ClosestVertex;
                            xa = Ver.X;// ReflectionHelper.GetMember<double>(Ver, "x");
                            ya = Ver.Y;// ReflectionHelper.GetMember<double>(Ver, "y");
                            za = Ver.Z;// ReflectionHelper.GetMember<double>(Ver, "z");
                            //object tempRefParam17 = 0;
                            m_Tool.TransformUcsPointToWorld(PRes.get_Item(0).Graphic.UCS, xa, ya, za, ref xW0, ref yW0, ref zW0);

                            hV = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(xW0, yW0, zW0);
                            hmDr = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetWorkPlaneMatrix(hDr);
                            hmInv = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixOrthoInvertEx(hmDr);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hV, hmInv);
                            xW0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hV);
                            yW0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hV);
                            zW0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hV);
                            //int tempRefParam18 = (hV);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hV);
                            //int tempRefParam19 = (hmInv);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref hmInv);

                        }
                        //MsgBox ("xW0=" & CStr(xW0) & " yW0 = " & CStr(yW0) & " zW0=" & CStr(zW0))

                        //ViewToWorldUCS ActDr, Vi, xV, yV, zV, xW0, yW0, zW0
                        m_gKind = 11; //GK_GRAPHIC
                        m_rm = 0; //RM_INTERNAL
                                  //hGLine=GraphicNew(GK_GRAPHIC,NULL)
                                  //Dim hApp As Long
                                  //            hApp = AppGetCurrentApp
                                  //hGrLine = GraphicNewEx(App, 0, 11, 0, 1)
                                  //hGrLine = TCWGraphicCreate(m_gKind, "")
                                  //Dim GrSet As GraphicSet
                                  //Dim Grs0 As Graphics
                                  //            Set GrSet = ActDr.GraphicSets.Add
                                  //            Set Grs0 = GrSet.AddGraphic
                        GrLine = Module1.ActDr.Graphics.Add(11);// ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Module1.ActDr, "Graphics"), "Add", new object[] { 11 });

                        m_Tool.hGrLine = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrLine.Index);// ReflectionHelper.GetMember<int>(GrLine, "Index"));


                        BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetPenColor(m_Tool.hGrLine, 0);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetPenWidth(m_Tool.hGrLine, 0);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetPenStyle(m_Tool.hGrLine, 1);


                        hDrMat = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetWorkPlaneMatrix(hDr);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(m_Tool.hGrLine, hDrMat, 0);
                        //TCWGraphicAppend 0, hGrLine
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAppend(m_Tool.hGrLine, xW0, yW0, zW0);
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAppend(m_Tool.hGrLine, xW0, yW0, zW0);
                        m_Tool.hVLine0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(m_Tool.hGrLine, 0);
                        m_Tool.hVLine1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(m_Tool.hGrLine, 1);
                        m_Tool.state = 3;
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, "", false });
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, CommonLocaliz.TcLoadLangString(137), false });

                        return;
                    }

                    if (m_Tool.state == 3)
                    { // select second point of bending line
                        //object tempRefParam20 = Type.Missing;
                        //object tempRefParam21 = Type.Missing;
                        //object tempRefParam22 = Type.Missing;
                        //object tempRefParam23 = Type.Missing;
                        //object tempRefParam24 = Type.Missing;
                        //object tempRefParam25 = Type.Missing;
                        //object tempRefParam26 = Type.Missing;
                        PRes = Module1.Vi.PickPoint(xV, yV, 0.1d, true, false, false, true, true, false);//, ref tempRefParam20, ref tempRefParam21, ref tempRefParam22, ref tempRefParam23, ref tempRefParam24, ref tempRefParam25, ref tempRefParam26);
                        Snap = Module1.ActDr.Application.SnapModes;// ReflectionHelper.GetMember<int>(ReflectionHelper.GetMember(Module1.ActDr, "Application"), "SnapModes");
                        PicCount = PRes.Count;
                        if (PicCount == 0 || Snap < 2)
                        {
                            m_Tool.ViewToDrUCS(xV, yV, zV, ref xW0, ref yW0, ref zW0);
                        }
                        else
                        {
                            object tempRefParam27 = 0;
                            Ver = PRes.get_Item(0).ClosestVertex;
                            xa = Ver.X;// ReflectionHelper.GetMember<double>(Ver, "x");
                            ya = Ver.Y;// ReflectionHelper.GetMember<double>(Ver, "y");
                            za = Ver.Z;// ReflectionHelper.GetMember<double>(Ver, "z");
                            //object tempRefParam28 = 0;
                            m_Tool.TransformUcsPointToWorld(PRes.get_Item(0).Graphic.UCS, xa, ya, za, ref xW0, ref yW0, ref zW0);

                            hV = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(xW0, yW0, zW0);
                            hmDr = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetWorkPlaneMatrix(hDr);
                            hmInv = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixOrthoInvertEx(hmDr);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hV, hmInv);
                            xW0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hV);
                            yW0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hV);
                            zW0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hV);
                            //int tempRefParam29 = (hV);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hV);
                            //int tempRefParam30 = (hmInv);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref hmInv);

                        }
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(m_Tool.hGrLine, 1); //inverted
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(m_Tool.hVLine1, xW0, yW0, zW0);
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(m_Tool.hGrLine, 0); //normal
                        m_Tool.state = 4;
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, "", false });
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, CommonLocaliz.TcLoadLangString(138), false });
                        BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(Convert.ToInt32(WhichView.HWND), BendTool.GCL_HCURSOR, m_Tool.hToolFixCursor);
                        return;
                    }

                    if (m_Tool.state == 4)
                    { // select fixed point on body
                        hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                        hmDr = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetWorkPlaneMatrix(hDr);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixGetPlane(hmDr, ref aDr, ref bDr, ref cDr, ref dDr, 0);

                        m_Tool.ViewToDrUCS(xV, yV, zV, ref xDrLoc, ref yDrLoc, ref zDrLoc);
                        m_Tool.hVlockpoint = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(xDrLoc, yDrLoc, zDrLoc);

                        hmInv = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixOrthoInvertEx(hmDr);
                        BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(m_Tool.hVlockpoint, hmDr);

                        xW = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(m_Tool.hVlockpoint);
                        yW = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(m_Tool.hVlockpoint);
                        zW = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(m_Tool.hVlockpoint);
                        dl = 0.001d;
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(m_Tool.hVlockpoint, xW + dl * aDr, yW + dl * bDr, zW + dl * cDr);
                        L1 = 100;
                        hvPosView = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(xW - L1 * aDr, yW - L1 * bDr, zW - L1 * cDr);
                        Res = BendToolSupport.PInvoke.SafeNative.tcapi24.ACISGraphicGetNormalAtPoint(m_Tool.hGrBody, m_Tool.hVlockpoint, hvPosView);
                        //VertexDispose hvPosView
                        BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref hmInv);
                        if (Res == 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref m_Tool.hVlockpoint);
                            m_Tool.hVlockpoint = 0;
                            return;
                        }

                        m_Tool.state = 5;
                        m_Tool.AddLM(false);
                        m_Tool.AddLM(true);
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, "", false });
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, CommonLocaliz.TcLoadLangString(140), false });
                        BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(Convert.ToInt32(WhichView.HWND), BendTool.GCL_HCURSOR, m_Tool.hToolCursor);

                        return;
                    }


                } // If Button = imsiLeftButton
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Mouse Down Failed");
                m_Tool.Finish();
                //   MsgBox Err.Description
            }
            return;
        }

        void IAppEvents.MouseMove(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, int Shift, int x, int y, ref bool Cancel)
        {
            try
            {
                if (m_Tool.state == 0)
                {
                    BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(Convert.ToInt32(WhichView.HWND), BendTool.GCL_HCURSOR, m_Tool.hToolCursor);
                    return;
                }

                double xV = 0, yV = 0;
                Module1.Vi = WhichView;
                Module1.ActDr = WhichDrawing;

                //***********************************************************
                int hwndVw = Convert.ToInt32(WhichView.HWND);
                if (hwndVw != m_Tool.hwndVwCur)
                {
                    if (m_Tool.hwndVwCur != 0)
                    {
                        BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(m_Tool.hwndVwCur, BendTool.GCL_HCURSOR, m_Tool.hCursorCur);
                    }
                    m_Tool.hwndVwCur = hwndVw;
                    m_Tool.hCursorCur = BendToolSupport.PInvoke.SafeNative.user32.GetClassLong(m_Tool.hwndVwCur, BendTool.GCL_HCURSOR);
                    if (m_Tool.hCursorCur != 0)
                    {
                        BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(m_Tool.hwndVwCur, BendTool.GCL_HCURSOR, 0);
                    }
                }
                //***********************************************************

                Module1.Vi.ScreenToView(x, y, out xV, out yV);
                double zV = 0;
                if (m_Tool.state == 1)
                {
                    m_Tool.CreateFaceGraphicEx(xV, yV, 0, 0, 0, 0);
                } // If NumGr = 2 Then

                double yW0 = 0, xW0 = 0, zW0 = 0;
                if (m_Tool.state == 3 && m_Tool.hGrLine > 0)
                {
                    m_Tool.ViewToDrUCS(xV, yV, zV, ref xW0, ref yW0, ref zW0);
                    //ViewToWorldUCS ActDr, Vi, xV, yV, zV, xW0, yW0, zW0
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(m_Tool.hGrLine, 1); //inverted
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(m_Tool.hVLine1, xW0, yW0, zW0);
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(m_Tool.hGrLine, 1); // normal
                }

                return;
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Mouse Move failed");
            }

        }

        void IAppEvents.RunTool(Tool WhichTool)
        {
            try
            {
                m_Tool.Finish();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Failed RunTool.");
            }
            return;
        }

        void IAppEvents.DrawingOpen(Drawing WhichDrawing)
        {
            try
            {
                m_Tool.Finish();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Drawing Open Failed.");
            }
            return;
        }
        void IAppEvents.DrawingActivate(Drawing WhichDrawing)
        {
            try
            {
                m_Tool.Finish();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Drawing Activate Failed.");
            }
            return;
        }
        void IAppEvents.BeforeExit(IMSIGX.Application TheApp, ref bool Cancel)//BeforeExit(IMSIGX.Application WhichApplication, bool Cancel)
        {
            try
            {
                m_Tool.Finish();
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Before Exit Failed.");
            }
            return;
        }

        #endregion
    }
}
