using System;

namespace Bend
{
	internal class CreateBend
	{



		private const double Pi = 3.14159265359d;
		private const double Eps = 0.000001d;

		public int BendBody(int hGrBody, int hGrLine, int hVlockpoint, double IntRadius, double BendAngle, double NeutralDepth)
		{
            int result = 0;
            try
            {
                int hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                int hVp = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetActiveViewport(hDr);
                int hmVp = BendToolSupport.PInvoke.SafeNative.dbapi24.ViewportGetMatrix(hVp);
                //Dim hmVpInv As Long
                //    hmVpInv = MatrixOrthoInvertEx(hmVp)
                int hmDr = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetWorkPlaneMatrix(hDr);

                double cVp = 0, aVp = 0, bVp = 0, dVp = 0;
                BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixGetPlane(hmVp, ref aVp, ref bVp, ref cVp, ref dVp, 0);
                double cDr = 0, aDr = 0, bDr = 0, dDr = 0;
                BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixGetPlane(hmDr, ref aDr, ref bDr, ref cDr, ref dDr, 0);

                //Dim a#, b#, c#, d#
                //    If GetPlaneByViewLine(hGrLine, a, b, c, d) = False Then
                //        Exit Function
                //    End If




                int hVl0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrLine, 0);
                int hVl1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrLine, 1);
                int hv0 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewFromEx(hVl0);
                int hv1 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewFromEx(hVl1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hv0, hmDr);
                BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hv1, hmDr);
                double x0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv0);
                double y0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv0);
                double z0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv0);
                double x1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv1);
                double y1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv1);
                double z1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv1);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hv0);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hv1);
                hv0 = 0;
                hv1 = 0;




                double xMid = 0.5d * (x0 + x1);
                double yMid = 0.5d * (y0 + y1);
                double zMid = 0.5d * (z0 + z1);

                //Dim aOrt#, bOrt#, cOrt#, dOrt#
                //    Call PlanePerpPlaneByLine(x0, y0, z0, x1, y1, z1, a, b, c, aOrt, bOrt, cOrt, dOrt)


                double L = Math.Sqrt(Math.Pow(x0 - x1, 2) + Math.Pow(y0 - y1, 2) + Math.Pow(z0 - z1, 2));
                if (L < Eps)
                {
                    return result;
                }


                double xB = (x1 - x0) / L;
                double yB = (y1 - y0) / L;
                double zB = (z1 - z0) / L;



                double BendRad = IntRadius;
                //If NeutralDepth > Eps Then
                //    BendRad = BendRad + NeutralDepth
                //End If
                //If BendRad < 0.001 Then
                //    BendRad = 0.001
                //End If


                double BendAng = BendAngle / 180 * Pi;
                double BendWidth = 0; // bendrad * bendangle
                int BendPlaneFixed = 0;
                //Dim n_points As Long
                //    n_points = 1
                //Dim positions() As Long
                //    If n_points > 0 Then
                //ReDim positions(n_points - 1)
                //        positions(n_points - 1) = VertexNewEx(x1, y1, z1)
                //    Else
                //ReDim positions(0)
                //    End If


                //Dim hVbendaxis As Long
                int hVbasepoint = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(x0, y0, z0);
                int hVlastpoint = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(x1, y1, z1);

                //If NeutralDepth > Eps Then
                //    TCWSetXYZ hVbasepoint, x0 - aDr * NeutralDepth, y0 - bDr * NeutralDepth, z0 - cDr * NeutralDepth
                //End If

                //hVbendaxis = VertexNewEx(xB, yB, zB)
                //hVbenddir = VertexNewEx(0, 1, 0)
                //hVbenddir = VertexNewEx(aOrt, bOrt, cOrt)
                int hVbenddir = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(aDr, bDr, cDr);

                int hGrNew = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWBendBody(hGrBody, hVbasepoint, hVlastpoint, hVbenddir, hVlockpoint, BendRad, BendAng, NeutralDepth, BendWidth, BendPlaneFixed);
                result = hGrNew;

                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVbasepoint);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVlastpoint);
                //TCWVertexDispose hVbendaxis
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVbenddir);
                //Dim i As Long
                //    If n_points > 0 Then
                //        For i = 0 To n_points - 1
                //            TCWVertexDispose positions(i)
                //        Next i
                //    End If

            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Failed BendBody");
            }
            return result;
        }





		//UPGRADE_NOTE: (7001) The following declaration (GetPlaneByViewLine) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private bool GetPlaneByViewLine(int hGrLine, ref double a, ref double b, ref double c, ref double d)
		//{
			//bool result = false;
			//result = true;
			//int hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
			//int hmDr = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetWorkPlaneMatrix(hDr);
			//
			//Dim Vers As Vertices
			//    Set Vers = GrLine.Vertices
			//    With Vers
			//        .UseWorldCS = True
			//        x0 = .Item(0).x
			//        y0 = .Item(0).y
			//        z0 = .Item(0).z
			//        x1 = .Item(1).x
			//        y1 = .Item(1).y
			//        z1 = .Item(1).z
			//    End With
			//int hVl0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrLine, 0);
			//int hVl1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrLine, 1);
			//    x0 = TCWGetX(hVl0)
			//    y0 = TCWGetY(hVl0)
			//    z0 = TCWGetZ(hVl0)
			//    x1 = TCWGetX(hVl1)
			//    y1 = TCWGetY(hVl1)
			//    z1 = TCWGetZ(hVl1)
			//int hv0 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewFromEx(hVl0);
			//int hv1 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewFromEx(hVl1);
			//
			//WorldToWorkPlane hDr, hV0, hV1, hmDr, hDr
			//BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hv0, hmDr);
			//BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hv1, hmDr);
			//double x0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv0);
			//double y0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv0);
			//double z0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv0);
			//double x1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv1);
			//double y1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv1);
			//double z1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv1);
			//BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hv0);
			//BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hv1);
			//hv0 = 0;
			//hv1 = 0;
			//
			//
			//double L = Math.Sqrt(Math.Pow(x0 - x1, 2) + Math.Pow(y0 - y1, 2) + Math.Pow(z0 - z1, 2));
			//if (L < Eps)
			//{
				//return false;
			//}
			//
			//int hVp = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetActiveViewport(hDr);
			//int hmVp = BendToolSupport.PInvoke.SafeNative.dbapi24.ViewportGetMatrix(hVp);
			//int hmVpInv = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixOrthoInvertEx(hmVp);
			//Dim hmDr As Long
			//    hmDr = DrawingGetWorkPlaneMatrix(hDr)
			//
			//hv0 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(x0, y0, z0);
			//hv1 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(x1, y1, z1);
			//int hv2 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(x1, y1, z1);
			//int hvTr = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hv2, hmVp);
			//double x2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv2);
			//double y2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv2);
			//double z2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv2) * Math.Sqrt(Math.Pow(x2, 2) + Math.Pow(y2, 2));
			//BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hv2, x2, y2, z2);
			//hvTr = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hv2, hmVpInv);
			//x2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv2);
			//y2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv2);
			//z2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv2);
			//
			//
			//a = (y1 - y0) * (z2 - z0) - (y2 - y0) * (z1 - z0);
			//b = (z1 - z0) * (x2 - x0) - (z2 - z0) * (x1 - x0);
			//c = (x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0);
			//
			//L = Math.Sqrt(a * a + b * b + c * c);
			//d = -(a * x0 + b * y0 + c * z0);
			//BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref hmVpInv);
			//BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hv0);
			//BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hv1);
			//BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hv2);
			//
			//if (Math.Abs(L) > Eps)
			//{
				//a /= L;
				//b /= L;
				//c /= L;
				//d /= L;
			//}
			//else
			//{
				//result = false;
			//}
			//
			//return result;
		//}


		//UPGRADE_NOTE: (7001) The following declaration (PlanePerpPlaneByLine) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void PlanePerpPlaneByLine(double x0, double y0, double z0, double x1, double y1, double z1, double a0, double b0, double c0, ref double a, ref double b, ref double c, ref double d)
		//{
			//double dx = x1 - x0;
			//double dy = y1 - y0;
			//double dz = z1 - z0;
			//double k1 = dy * c0 - dz * b0;
			//double k2 = dz * a0 - dx * c0;
			//double k3 = dx * b0 - dy * a0;
			//a = k1;
			//b = k2;
			//c = k3;
			//d = -(k1 * x0 + k2 * y0 + k3 * z0);
			//double L = Math.Sqrt(a * a + b * b + c * c);
			//a /= L;
			//b /= L;
			//c /= L;
			//d /= L;
			//
			//
			//
		//}
	}
}