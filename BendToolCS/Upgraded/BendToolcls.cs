using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using IMSIGX;
using TCDotNetInterfaces;
using UpgradeHelpers.Helpers;

namespace Bend
{
	public class BendTool : ITurboCADTool
	{

		public const int GCL_HCURSOR = -12;

		//Number of tools in this server
		const int NUM_TOOLS = 1;

        public int hwndVwCur = 0;
        public int hCursorCur = 0;
        public int hToolCursor = 0;
        public int hToolFixCursor = 0;
        public Image TheCursorImage = null;
        public int hCursorOld = 0;
        public int hCursorInit = 0;


		// *******************************************************************
		//    Const EventMask = 268435456 + 536870912
		//    Const EventMask = 1024 + 268435456 + 536870912
		//Const EventMask = 1 + 2 + 4 + 8 + 16 + 32 + 64 + 1024 + 4096 + 262144 + 268435456 + 536870912
//		static readonly double EventMask = Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(Convert.ToDouble(ReflectionHelper.GetPrimitiveValue<double>(imsiEventBeforeExit) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventDrawingNew)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventDrawingOpen)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventDrawingActivate)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventDrawingDeactivate)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventDrawingBeforeClose)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventDrawingBeforeSave)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventMouseDown)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventMouseUp)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventMouseMove)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventBeforeDoubleClick)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventRunTool)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventCancel)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventUpdateUndo)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventWindowDeactivate)) + ReflectionHelper.GetPrimitiveValue<double>(imsiEventSelectionChange));
        IMSIGX.ImsiEventMask Eventmask = ImsiEventMask.imsiEventDrawingNew | ImsiEventMask.imsiEventDrawingOpen | ImsiEventMask.imsiEventDrawingActivate | ImsiEventMask.imsiEventDrawingDeactivate | ImsiEventMask.imsiEventDrawingBeforeClose | ImsiEventMask.imsiEventDrawingBeforeSave | ImsiEventMask.imsiEventMouseDown | ImsiEventMask.imsiEventMouseMove | ImsiEventMask.imsiEventBeforeDoubleClick | ImsiEventMask.imsiEventRunTool | ImsiEventMask.imsiEventCancel | ImsiEventMask.imsiEventUpdateUndo | ImsiEventMask.imsiEventWindowDeactivate | ImsiEventMask.imsiEventSelectionChange | ImsiEventMask.imsiEventBeforeExit;
		private int iConnectId = 0;
		private int iActTool = 0;
		//UPGRADE_ISSUE: (2068) IToolEvents object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		private Bend.TCEventSink theToolEvents = null;


		// *******************************************************************
		//Toggle this to test loading buttons from .Bmp/.Res
		const bool boolLoadFromBmp = false;
		const bool boolDebug = false;

		private Tool gxMe = null;
		//##########################################################################
		//##########################################################################




		//UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		public IMSIGX.Graphic GrBody = null;
		int OldBodyCol = 0;
        //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        public IMSIGX.Graphic GrFace = null;
        //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        public IMSIGX.Graphic GrCoord = null;
        //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        public IMSIGX.Graphic GrSymX = null;
        //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        public IMSIGX.Graphic GrSymY = null;
        //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        public IMSIGX.Graphic GrSymZ = null;




		public int state = 0;
		double SumCoordPrev = 0;
		double SumCoordNew = 0;
        public int hVLine0 = 0;
        public int hVLine1 = 0;
        public int hGrLine = 0;
        public int hGrFace = 0;
		public int hGrBody = 0;
        public int hGrBodyNew = 0;
        public int hVlockpoint = 0;
        public int hGrCoord = 0;
        public int hGrCoordX = 0;
        public int hGrCoordY = 0;
        public int hGrCoordZ = 0;

		int hGrSymX = 0;
		int hGrSymY = 0;
		int hGrSymZ = 0;

		double IntRadius = 0;
		double BendAngle = 0;
		double NeutralDepth = 0;

		int UndoCount = 0;

		//Copy a windows bitmap of the requested size to the clipboard
		//Bitmaps returned should contain NUM_TOOLS images
		//Size of entire bitmap:
		//Normal:  (NUM_TOOLS*16) wide x 15 high
		//Large:   (NUM_TOOLS*24) wide x 23 high
		//Mono bitmap should be 1-bit (black or white)
		public bool CopyBitmap(bool LargeImage, bool MonoImage)
		{
			try
			{

				Image TheImage = new Bitmap(1, 1);
				if (GetButtonPicture(LargeImage, MonoImage, ref TheImage))
				{
					//Put the image on the Windows clipboard
					Clipboard.SetData(DataFormats.Dib, TheImage);
					return true;
				}
			}
			catch
			{
                MessageBox.Show("CopyBitmap Failed");
			}


			return false;
		}

		//Return a Picture object for the requested size
		//Apparently, returning references to StdPicture objects doesn't work for .EXE servers
		//Bitmaps returned should contain NUM_TOOLS images
		//Size of entire image:
		//Normal:  (NUM_TOOLS*16) wide x 15 high
		//Large:   (NUM_TOOLS*24) wide x 23 high
		//Mono image should be 1-bit (black or white)
		public Image GetPicture(bool LargeImage, bool MonoImage)
		{
			try
			{

				Image TheImage = new Bitmap(1, 1);
				if (GetButtonPicture(LargeImage, MonoImage, ref TheImage))
				{
					return TheImage;
				}
			}
			catch
			{
                MessageBox.Show("GetPicture Failed");
			}


			return null;
		}

		//Implementation specific stuff
		//Private function to return the bitmap from .Res file or .Bmp file
		private bool GetButtonPicture(bool LargeImage, bool MonoImage, ref Image TheImage)
		{
			bool result = false;
			try
			{

				//There are two ways to load images:  from .Bmp file(s) or from .RES resource.
				//In this demo, we control the loading by a private variable.

				//Note that if you are loading from .Bmp, or if you are running this tool as a
				//.VBP for debugging, you must place the .Res or .Bmp files in the Draggers subdirectory
				//of the directory in which TCW40.EXE (or IMSIGX40.DLL) is located.

				string strFileName = "";
				int idBitmap = 0; //BITMAP resource id in .Res file //File name of .Bmp file to load
				if (boolLoadFromBmp)
				{
					//Load from .Bmp file

					if (LargeImage)
					{
						strFileName = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\button24.bmp"; //# NLS#'
					}
					else
					{
						strFileName = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\button16.bmp"; //# NLS#'
					}
					TheImage = Image.FromFile(strFileName);
				}
				else
				{
					//Load from .Res file

					if (LargeImage)
					{
						idBitmap = 1002;
					}
					else
					{
						idBitmap = 1001;
					}
					TheImage = (Bitmap) App.Resources.Resources.ResourceManager.GetObject("bmp" + idBitmap.ToString());
				}

				//Return the image
				return true;
			}
			catch
			{

				if (boolDebug)
				{
                    MessageBox.Show("GetButton Picture Failed");//        MsgBox "Error loading bitmap: " & Err.Description
				}
				result = false;
			}
			return result;
		}

		//Private Function GetLMPicture() As Object
		public Image GetLMPicture()
		{
			//MsgBox ("1111")
			Image TheImage = App.Resources.Resources.bmp1003;
			return TheImage;
		}


		//##########################################################################
		//##########################################################################
		//##########################################################################


		//Return a description string for this package of tools
		public string Description
		{
			get
			{
				//    Description = "SectionAlongPath"
				return CommonLocaliz.TcLoadLangString(130);
			}
		}
        		
		//Fill arrays with information about tools in the package
		//Return the number of tools in the package
		public bool GetToolInfo(out TurboCADToolInfo ToolInfo)//string ref[] CommandNames, ref string[, ] MenuCaptions, ref string[] StatusPrompts, ref string[] ToolTips, ref bool[] Enabled, ref bool[] WantsUpdates)
		{
            ToolInfo = new TurboCADToolInfo();

            ToolInfo.CommandName = "Tools\nDotNet Tools\nBend Tool CS";
            ToolInfo.InternalCommand = "CMD_89F89ACB-B673-47C7-AD92-E37610D3C030";
            ToolInfo.MenuCaption = "&Bend Tool CS";
            ToolInfo.ToolbarName = "Bend Tool CS";
            ToolInfo.ToolTip = "Bend Tool CS";
            ToolInfo.bEnabled = true;
            ToolInfo.bWantsUpdates = true;

            try
            {
                ToolInfo.ToolbarImage = App.Resources.Resources.bmp1001;

                ToolInfo.ToolbarImageL = App.Resources.Resources.bmp1002;

                ToolInfo.ToolbarImageBW = App.Resources.Resources.bmp1001;

                ToolInfo.ToolbarImageLBW = App.Resources.Resources.bmp1002;
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("ToolBar Failed");
            }

            return true;
		}

		//Returns true if tool is correctly initialized
		public bool Initialize(Tool Context)
		{
            bool result = false;
			gxMe = Context;
            try
			{
				Module1.objApp = gxMe.Application;
                theToolEvents = new TCEventSink(this);// (IToolEvents) Module1.objApp;
				iConnectId = -1;
				iActTool = -1;
				result = true;
                hwndVwCur = 0;
				hCursorCur = 0;

				TheCursorImage = (Image) App.Resources.Resources.cur101.ToBitmap();
				//UPGRADE_WARNING: (7003) The Hdc should be released once it is used for safety More Information: http://www.vbtonet.com/ewis/ewi7003.aspx
				hToolCursor = System.Drawing.Graphics.FromImage(TheCursorImage).GetHdc().ToInt32();
                TheCursorImage = (Image) App.Resources.Resources.cur102.ToBitmap();
				//UPGRADE_WARNING: (7003) The Hdc should be released once it is used for safety More Information: http://www.vbtonet.com/ewis/ewi7003.aspx
				hToolFixCursor = System.Drawing.Graphics.FromImage(TheCursorImage).GetHdc().ToInt32();
            }
			catch
			{
                MessageBox.Show("Initialize Failed");
                //   MsgBox Err.Description
            }

			return result;
		}

        //Called to perform tool function
        public bool Run(Tool Tool)
        {
            try
            {
                // initialize
                state = 0;
                GrBody = null;
                GrFace = null;
                GrCoord = null;
                GrSymX = null;
                GrSymY = null;
                GrSymZ = null;
                SumCoordPrev = 0;
                hVLine0 = 0;
                hVLine1 = 0;
                hGrLine = 0;
                hGrBody = 0;
                hGrFace = 0;

                hVlockpoint = 0;
                hGrBodyNew = 0;
                hGrCoord = 0;
                hGrCoordX = 0;
                hGrCoordY = 0;
                hGrCoordZ = 0;
                hGrSymX = 0;
                hGrSymY = 0;
                hGrSymZ = 0;


                SumCoordPrev = 0;
                UndoCount = 0;

                Tool theXTool = null;
                theXTool = Tool;
                //    theXTool.Application.ActiveDrawing.Graphics.Unselect
                Properties withVar = null;
                int hwndVw = 0;
                if (iConnectId != -1)
                {
                    //ReflectionHelper.Invoke(Module1.objApp, "DisconnectEvents", new object[] { iConnectId });
                    Module1.objApp.DisconnectEvents(iConnectId);
                    AddLM(false);
                    iConnectId = -1;
                    iActTool = -1;
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                }
                else
                {
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                    //       theToolEvents.ToolChangePrompt Me, "Input Data", False
                    //        theToolEvents.ToolChangePrompt Me, TcLoadLangString(124), False
                    //iConnectId = ReflectionHelper.Invoke<int>(Module1.objApp, "ConnectEvents", new object[]{this, EventMask});
                    iConnectId = Module1.objApp.ConnectEvents(theToolEvents, Eventmask);
                    iActTool = theXTool.Index;
                    //AddInspectorBar(true);
                    //withVar = gxMe.Properties;
                    IntRadius = 0d;// Convert.ToDouble(withVar.get_Item(CommonLocaliz.TcLoadLangString(143)).Value);
                    ////               MessageBox.Show("here");
                    ////               object tempRefParam2 = CommonLocaliz.TcLoadLangString(144);
                    BendAngle = 90d;// Convert.ToDouble(withVar.get_Item(CommonLocaliz.TcLoadLangString(144)).Value);
                    ////object tempRefParam3 = CommonLocaliz.TcLoadLangString(145);
                    NeutralDepth = 0d;// Convert.ToDouble(withVar.get_Item(CommonLocaliz.TcLoadLangString(145)).Value);
                    AddLM(true);
                    //        theToolEvents.ToolChangePrompt Me, "Select the Section graphic", False
                    //        theToolEvents.ToolChangePrompt Me, "Select the Graphic", False
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, CommonLocaliz.TcLoadLangString(125), false});

                    hwndVw = Convert.ToInt32(gxMe.Application.ActiveDrawing.ActiveView.HWND);
                    hCursorInit = BendToolSupport.PInvoke.SafeNative.user32.GetClassLong(hwndVw, GCL_HCURSOR);
                    //MessageBox.Show(gxMe.Application.ActiveDrawing.Selection.Count.ToString());
                    
                }
            }
            catch
            {
                MessageBox.Show("Run Failed");
                //    MsgBox Err.Msg
                Finish();
            }

            return false;
        }

        //Returns true if tool is correctly initialized
        public bool UpdateToolStatus(Tool Tool, ref bool Enabled, ref bool Checked)
		{
            try
            {
                Tool theXTool = Tool;
                Enabled = true;
                int Space = Convert.ToInt32(theXTool.Application.ActiveDrawing.Properties.get_Item("TileMode").Value);
                if (Space == 1)
                {
                    Enabled = theXTool.Application.ActiveDrawing.Graphics.Count > 0;
                }
                else
                {
                    if (iConnectId != -1)
                    {
                        Finish();
                    }
                    Enabled = false;
                }
                Checked = iConnectId != -1;
                return true;
            }
            catch
            {
                MessageBox.Show("UpdateTool Failed");
            }
            return false;
		}
        
		public void Cancel(bool DoItPlease, ref bool CanCancel)
		{
            try
            {
                if (DoItPlease)
                {
                    Finish();
                }

                CanCancel = true;
                return;
            }
            catch
            {
                MessageBox.Show("Failed 876");
            }
		}

		public void UpdateUndo(ref bool AllowsUndo)
		{
            try
            {
                AllowsUndo = false;
                return;
            }
            catch
            {
                MessageBox.Show("Failed 8234");
            }
        }


		//UPGRADE_NOTE: (7001) The following declaration (AddActToolLM) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private object AddActToolLM(int iTool, bool bAdd)
		//{
			//    AddLM bAdd
			//return null;
		//}


		~BendTool()
		{
            try
            {
                if (theToolEvents != null)
                {
                    theToolEvents = null;
                }

                Module1.Vi = null;
                //Set Vis = Nothing

                Module1.Grs = null;
                Module1.ActDr = null;

                //Set Drs = Nothing

                if (Module1.objApp != null)
                {
                    Module1.objApp = null;
                }
            }
            catch
            {
                MessageBox.Show("Destructor Failed");
            }

		}



		public void AddLM(bool bAdd)
		{
            try
            {
                string[] Captions = new string[] { "", "", "", "" };
                string[] Prompts = new string[] { "", "", "", "" };
                bool[] Enabled = new bool[4];
                bool[] Checked = new bool[4];

                string[] varCaptions = null;
                string[] varPrompts = null;
                bool[] varEnabled = null;
                bool[] varChecked = null;

                if (bAdd)
                {


                    //        Captions(1) = "One Step Back"
                    //        Prompts(1) = "One Step Back"
                    Captions[0] = CommonLocaliz.TcLoadLangString(133);
                    Prompts[0] = CommonLocaliz.TcLoadLangString(133);
                    Enabled[0] = state > 0 || (state == 0 && UndoCount > 0);
                    Checked[0] = false;

                    //        Captions(1) = "Apply bend"
                    //        Prompts(1) = "Apply bend"
                    Captions[1] = CommonLocaliz.TcLoadLangString(141);
                    Prompts[1] = CommonLocaliz.TcLoadLangString(141);
                    Enabled[1] = state == 5;
                    Checked[1] = false;




                    //        Captions(3) = "Finish SectionAlongPath"
                    //        Prompts(3) = "Finish SectionAlongPath"
                    //        Captions(3) = "Finish Profile Along Path"
                    //        Prompts(3) = "Finish Profile Along Path"
                    Captions[2] = CommonLocaliz.TcLoadLangString(135);
                    Prompts[2] = CommonLocaliz.TcLoadLangString(135);
                    Enabled[2] = state == 5;
                    Checked[2] = false;


                    Captions[3] = CommonLocaliz.TcLoadLangString(117); // Cancel
                    Prompts[3] = CommonLocaliz.TcLoadLangString(117);
                    Enabled[3] = true;
                    Checked[3] = false;


                    varCaptions = Captions;
                    varPrompts = Prompts;
                    varEnabled = Enabled;
                    varChecked = Checked;

                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangeCommands", new object[]{this, 4, varCaptions, varPrompts, varEnabled, varChecked, true});
                }
                else
                {
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangeCommands", new object[]{this, 0, null, null, null, null, false});
                }
                return;
            }
            catch
            {
                MessageBox.Show("failed 98");
            }
		}

		public void DoLMCommand(int CmdInd)
		{
            try
            {
                //MsgBox ("CmdInd=" & CStr(CmdInd))
                if (CmdInd == 0)
                { //OneStepBack
                    if (state == 0 && UndoCount > 0)
                    {
                        //object tempRefParam = 1;
                        gxMe.Application.ActiveDrawing.Undo(1);
                        UndoCount--;
                    }
                    if (state == 1)
                    {
                        if (hGrBody > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetSelected(hGrBody, 0);
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrBody, 0);
                            hGrBody = 0;
                        }
                        GrBody = null;
                        state = 0;
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, CommonLocaliz.TcLoadLangString(125), false});
                    }
                    if (state == 2)
                    {
                        if (hGrFace > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrFace, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrFace);
                            hGrFace = 0;
                        }
                        GrFace = null;
                        if (hGrCoord > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrCoord, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrCoord);
                            hGrCoord = 0;
                        }
                        GrCoord = null;
                        if (hGrSymX > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymX, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymX);
                            hGrSymX = 0;
                        }
                        GrSymX = null;
                        if (hGrSymY > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymY, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymY);
                            hGrSymY = 0;
                        }
                        GrSymY = null;
                        if (hGrSymZ > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymZ, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymZ);
                            hGrSymZ = 0;
                        }
                        GrSymZ = null;

                        state = 1;
                        SumCoordPrev = 0;
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, CommonLocaliz.TcLoadLangString(139), false});
                    }
                    if (state == 3 || state == 4)
                    {
                        if (hGrLine > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrLine, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrLine);
                            hGrLine = 0;
                        }
                        state = 2;
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, CommonLocaliz.TcLoadLangString(136), false});
                        BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(Convert.ToInt32(gxMe.Application.ActiveDrawing.ActiveView.HWND), GCL_HCURSOR, hToolCursor);

                    }
                    if (state == 5)
                    {
                        if (hGrBodyNew > 0)
                        {
                            if (hGrBodyNew > 0)
                            {
                                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrBodyNew, 1);
                                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrBodyNew);
                                hGrBodyNew = 0;
                            }
                            if (hGrBody > 0)
                            {
                                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrBody, 0);
                            }
                            if (hGrFace > 0)
                            {
                                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrFace, 0);
                            }
                            if (hGrLine > 0)
                            {
                                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrLine, 0);
                            }

                            state = 5;
                            //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                            //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, CommonLocaliz.TcLoadLangString(140), false});

                        }
                        else
                        {
                            if (hVlockpoint > 0)
                            {
                                BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVlockpoint);
                                hVlockpoint = 0;
                            }
                            state = 4;
                            //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                            //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, CommonLocaliz.TcLoadLangString(138), false});
                            BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(Convert.ToInt32(gxMe.Application.ActiveDrawing.ActiveView.HWND), GCL_HCURSOR, hToolFixCursor);
                        }
                    }

                    //AddLM(false);
                    AddLM(true);
                }

                CreateBend AddBend = new CreateBend();

                if (CmdInd == 1)
                { // Apply bend
                    if (state == 5 && hGrBody > 0 && hGrFace > 0 && hGrLine > 0 && hVlockpoint > 0)
                    {
                        object tempRefParam2 = CommonLocaliz.TcLoadLangString(143);
                        IntRadius = Convert.ToDouble(gxMe.Properties.get_Item(ref tempRefParam2).Value);
                        object tempRefParam3 = CommonLocaliz.TcLoadLangString(144);
                        BendAngle = Convert.ToDouble(gxMe.Properties.get_Item(ref tempRefParam3).Value);
                        object tempRefParam4 = CommonLocaliz.TcLoadLangString(145);
                        NeutralDepth = Convert.ToDouble(gxMe.Properties.get_Item(ref tempRefParam4).Value);

                        hGrBodyNew = AddBend.BendBody(hGrBody, hGrLine, hVlockpoint, IntRadius, BendAngle, NeutralDepth);
                    }
                }


                int BodyOldID = 0;
                int BodyNewID = 0;

                UndoRecord Urec = null;
                //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
                IMSIGX.Graphic GrBodyNew = null;
                //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
                IMSIGX.Graphic GrBodyOld = null;
                if (CmdInd == 2)
                { // Finish


                    if (state == 5 && hGrBody > 0 && hGrFace > 0 && hGrLine > 0 && hVlockpoint > 0)
                    {

                        if (hGrBodyNew == 0)
                        {
                            object tempRefParam5 = CommonLocaliz.TcLoadLangString(143);
                            IntRadius = Convert.ToDouble(gxMe.Properties.get_Item(ref tempRefParam5).Value);
                            object tempRefParam6 = CommonLocaliz.TcLoadLangString(144);
                            BendAngle = Convert.ToDouble(gxMe.Properties.get_Item(ref tempRefParam6).Value);
                            object tempRefParam7 = CommonLocaliz.TcLoadLangString(145);
                            NeutralDepth = Convert.ToDouble(gxMe.Properties.get_Item(ref tempRefParam7).Value);

                            hGrBodyNew = AddBend.BendBody(hGrBody, hGrLine, hVlockpoint, IntRadius, BendAngle, NeutralDepth);
                        }
                        if (hGrBodyNew == 0)
                        {
                            return;
                        }
                        BodyOldID = BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicGetID(hGrBody);
                        BodyNewID = BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicGetID(hGrBodyNew);
                        if (BodyOldID > 0)
                        {
                            //GrBodyOld = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Module1.ActDr, "Graphics"), "GraphicFromID", new object[]{BodyOldID});
                            GrBodyOld = Module1.ActDr.Graphics.GraphicFromID(BodyOldID);
                        }
                        if (BodyNewID > 0)
                        {
                            //GrBodyNew = ReflectionHelper.Invoke<IMSIGX.Graphic>(ReflectionHelper.GetMember(Module1.ActDr, "Graphics"), "GraphicFromID", new object[]{BodyNewID});
                            GrBodyNew = Module1.ActDr.Graphics.GraphicFromID(BodyNewID);
                        }
                        if (GrBodyOld != null && GrBodyNew != null)
                        {
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetSelected(hGrBody, 0);
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrBody, 0);
                            //Urec = ReflectionHelper.Invoke<UndoRecord>(Module1.ActDr, "AddUndoRecord", new object[]{CommonLocaliz.TcLoadLangString(142)});
                            Urec = Module1.ActDr.AddUndoRecord(CommonLocaliz.TcLoadLangString(142));
                            Urec.AddGraphic(GrBodyNew);
                            Urec.DeleteGraphic(GrBodyOld);
                            Urec.Close();
                            UndoCount++;
                            hGrBody = 0;
                        }
                        else
                        {
                            if (hGrBodyNew > 0)
                            {
                                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrBodyNew, 1);
                                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrBodyNew);
                                hGrBodyNew = 0;
                            }
                        }
                        if (hGrBody > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetSelected(hGrBody, 0);
                            hGrBody = 0;
                        }

                        if (hGrCoord > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrCoord, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrCoord);
                            hGrCoord = 0;
                        }
                        if (hGrSymX > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymX, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymX);
                            hGrSymX = 0;
                        }
                        if (hGrSymY > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymY, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymY);
                            hGrSymY = 0;
                        }
                        if (hGrSymZ > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymZ, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymZ);
                            hGrSymZ = 0;
                        }
                        if (hGrFace > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrFace, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrFace);
                            hGrFace = 0;
                        }
                        if (hGrLine > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrLine, 1);
                            BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrLine);
                            hGrLine = 0;
                        }
                        if (hVlockpoint > 0)
                        {
                            BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVlockpoint);
                            hVlockpoint = 0;
                        }


                        state = 0;
                        hGrBodyNew = 0;
                        SumCoordPrev = 0;

                        GrBody = null;
                        GrFace = null;
                        GrCoord = null;
                        GrSymX = null;
                        GrSymY = null;
                        GrSymZ = null;

                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                        //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, CommonLocaliz.TcLoadLangString(125), false});
                        //AddLM(false);
                        AddLM(true);

                    }
                }

                if (CmdInd == 3)
                { // cancel
                    Finish();

                }
                return;
            }
            catch
            {
                MessageBox.Show("failed 7");
            }
		}

		public void Finish()
		{
            try
            {
                if (iConnectId != -1)
                {
                    //ReflectionHelper.Invoke(Module1.objApp, "DisconnectEvents", new object[] { iConnectId });
                    Module1.objApp.DisconnectEvents(iConnectId);
                    iConnectId = -1;
                    AddLM(false);
                    //AddInspectorBar(false);
                }

                //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, "", false });

                state = 0;
                UndoCount = 0;
                SumCoordPrev = 0;

                if (hGrCoord > 0)
                {
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrCoord, 1);
                    BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrCoord);
                    GrCoord = null;
                    hGrCoord = 0;
                }
                if (hGrSymX > 0)
                {
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymX, 1);
                    BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymX);
                    GrSymX = null;
                    hGrSymX = 0;
                }

                if (hGrSymY > 0)
                {
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymY, 1);
                    BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymY);
                    GrSymY = null;
                    hGrSymY = 0;
                }

                if (hGrSymZ > 0)
                {
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymZ, 1);
                    BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrSymZ);
                    GrSymZ = null;
                    hGrSymZ = 0;
                }

                if (hGrFace > 0)
                {
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrFace, 1);
                    BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrFace);
                    GrFace = null;
                    hGrFace = 0;
                }

                if (hGrLine > 0)
                {
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrLine, 1);
                    BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrLine);
                    hGrLine = 0;
                }

                if (hGrBodyNew > 0)
                {
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrBodyNew, 1);
                    BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicDispose(ref hGrBodyNew);
                    hGrBodyNew = 0;
                }

                if (hVlockpoint > 0)
                {
                    BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVlockpoint);
                    hVlockpoint = 0;
                }

                if (hGrBody > 0)
                {
                    //GrBody.Properties("PenColor") = OldBodyCol
                    //GrBody.Draw
                    BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetSelected(hGrBody, 0);
                    BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrBody, 0);
                    GrBody = null;
                    hGrBody = 0;
                }

                if (hwndVwCur != 0)
                {
                    //UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                    //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
                    BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(hwndVwCur, GCL_HCURSOR, hCursorCur);
                    BendToolSupport.PInvoke.SafeNative.user32.SetClassLong(hwndVwCur, GCL_HCURSOR, hCursorInit);

                    hwndVwCur = 0;
                    hCursorCur = 0;
                    hCursorInit = 0;
                }

                Module1.Grs = null;
                Module1.Vi = null;
                //Set Vis = Nothing
                Module1.ActDr = null;
                //   Set Drs = Nothing
                //   Set objApp = Nothing
            }
            catch
            {
                MessageBox.Show("Finish Failed");
            }
		}

		//public object MouseMove(IMSIGX.Drawing WhichDrawing, GXTIESLib.View WhichView, GXTIESLib.Window WhichWindow, int Shift, int x, int y, bool Cancel)
		//{
			
		//}


		//UPGRADE_NOTE: (7001) The following declaration (ViewToDrUCSOld) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void ViewToDrUCSOld(GXTIESLib.View Vi, double xVi, double yVi, double zVi, ref double xDrUcs, ref double yDrUcs, ref double zDrUcs)
		//{
			//
			//Vi.ViewToWorld(xVi, yVi, zVi, out xDrUcs, out yDrUcs, out zDrUcs);
		//}

		public void ViewToDrUCS(double xV, double yV, double zV, ref double xDrUcs, ref double yDrUcs, ref double zDrUcs)
		{
            try
            {
                //Vi.ViewToWorld xVi, yVi, zVi, xDrUcs, yDrUcs, zDrUcs
                int hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                int hVp = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetActiveViewport(hDr);

                int hmVp = BendToolSupport.PInvoke.SafeNative.dbapi24.ViewportGetMatrix(hVp);
                int hVer1 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(xV, yV, zV);
                int hVer2 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(xV, yV, zV);

                BendToolSupport.PInvoke.SafeNative.dbapi24.WorldToWorkPlane(hDr, hVer1, hVer2, hmVp, hVp);

                xDrUcs = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hVer2);
                yDrUcs = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hVer2);
                zDrUcs = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hVer2);

                BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVer1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVer2);
            }
            catch
            {
                MessageBox.Show("Failed 6");
            }


		}

		public void ViewToWorldUCS(double xV, double yV, double zV, ref double xW, ref double yW, ref double zW)
		{
            try
            {
                //Vi.ViewToWorld xVi, yVi, zVi, xDrUcs, yDrUcs, zDrUcs
                int hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                int hVi = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetActiveViewport(hDr);
                int hViMat = BendToolSupport.PInvoke.SafeNative.dbapi24.ViewportGetMatrix(hVi);
                int hViMatInv = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixNewEx();
                hViMatInv = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixOrthoInvertEx(hViMat);
                int hVer = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(xV, yV, zV);
                BendToolSupport.PInvoke.SafeNative.dbapi24.VertexTransform(hVer, hViMatInv);
                xW = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hVer);
                yW = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hVer);
                zW = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hVer);

                BendToolSupport.PInvoke.SafeNative.dbapi24.VertexDispose(ref hVer);
                BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref hViMatInv);
            }
            catch
            {
                MessageBox.Show("Failed 5");
            }

		}

		//UPGRADE_NOTE: (7001) The following declaration (CurUcsToWorldUCS) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void CurUcsToWorldUCS(UpgradeStubs.Matrix CurUcs, double xUcs, double yUcs, double zUcs, ref double xW, ref double yW, ref double zW)
		//{
			//
			//
			//double f32 = 0, f30 = 0, f22 = 0, f20 = 0, f12 = 0, f10 = 0, f02 = 0, f00 = 0, f01 = 0, f03 = 0, f11 = 0, f13 = 0, f21 = 0, f23 = 0, f31 = 0, f33 = 0;
			//ReflectionHelper.Invoke(CurUcs, "GetEntries", new object[]{f00, f01, f02, f03, f10, f11, f12, f13, f20, f21, f22, f23, f30, f31, f32, f33});
			//
			//xW = f30 + xUcs * f00 + yUcs * f10 + zUcs * f20;
			//yW = f31 + xUcs * f01 + yUcs * f11 + zUcs * f21;
			//zW = f32 + xUcs * f02 + yUcs * f12 + zUcs * f22;
			//
		//}

		public bool CreateFaceGraphicEx(double xVi, double yVi, int hv0, int hVx, int hVy, int hVz)
		{
            try
            {
                bool result = false;
                //Dim hGr As Long
                double zVi = 0;
                int hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                int hVp = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWViewActive();
                //hGr = TCWGraphicAt(hDr, GrBody.Index)
                double yW = 0, xW = 0, zW = 0;
                //Vi.ViewToWorld xClick, yClick, 0, xW, yW, zW
                ViewToWorldUCS(xVi, yVi, zVi, ref xW, ref yW, ref zW);
                //MsgBox ("xW=" & CStr(xW) & " yW=" & CStr(yW) & "zW=" & CStr(zW))

                int hVer = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(xW, yW, zW);

                //Dim hRoot As Long
                //            hRoot = hGrBody
                int FacLevel = 1;
                int IntCount = 0;
                int hgAct = 0;

                
                IMSIGX.Graphic GrHit = Module1.Grs.Add((int)ImsiGraphicType.imsiSurface);//, ref tempRefParam2, ref tempRefParam3, ref tempRefParam4, ref tempRefParam5, ref tempRefParam6);
                int hgHit = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrHit.Index);
                Module1.Grs.Remove(GrHit.Index);
                int hvHit = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(0, 0, 0);

                int hvHitNor = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(0, 0, 0);

                int Flags = 3;

                int hVCurvAxis1 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(0, 0, 0);

                int hVCurvAxis2 = BendToolSupport.PInvoke.SafeNative.dbapi24.VertexNewEx(0, 0, 0);
                double Dist = 0;
                Dist = BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicGetClosestFacet(hVer, hGrBody, ref FacLevel, ref IntCount, ref hgAct, hgHit, hvHit, hvHitNor, hVp, Flags, hVCurvAxis1, hVCurvAxis2);

                //UPGRADE_ISSUE: (2068) Graphic object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
                IMSIGX.Graphic GrFaceNew = null;
                GrFaceNew = null;
                double yMax = 0, xMax = 0, zMax = 0;
                double yMin = 0, xMin = 0, zMin = 0;
                double yc = 0, xc = 0, zc = 0;
                double dy = 0, dx = 0, dz = 0;
                if (Dist > Module1.Eps / 100)
                {
                    if (hGrCoord > 0)
                    {
                        UpdateCoordinateGraphic(hvHit, hVCurvAxis1, hVCurvAxis2, hvHitNor);
                    }
                    //object tempRefParam8 = Type.Missing;
                    //object tempRefParam9 = Type.Missing;
                    Module1.Grs.AddGraphic(GrHit, 1, 0);
                    GrFaceNew = GrHit; // CreateFaceGraphic(GrHit)
                                       //GrFaceNew.Properties("PenWidth") = 0.02
                                       //GrFaceNew.Properties("PenColor") = RGB(0, 0, 255)
                                       //GrFaceNew.Draw

                    xMax = GrFaceNew.CalcBoundingBox(GrFaceNew.UCS).Max.X; // ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrFaceNew, "CalcBoundingBox"), "Max"), "x");
                    yMax = GrFaceNew.CalcBoundingBox(GrFaceNew.UCS).Max.Y; //ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrFaceNew, "CalcBoundingBox"), "Max"), "y");
                    zMax = GrFaceNew.CalcBoundingBox(GrFaceNew.UCS).Max.Z; //ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrFaceNew, "CalcBoundingBox"), "Max"), "z");
                    xMin = GrFaceNew.CalcBoundingBox(GrFaceNew.UCS).Min.X; //ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrFaceNew, "CalcBoundingBox"), "Min"), "x");
                    yMin = GrFaceNew.CalcBoundingBox(GrFaceNew.UCS).Min.Y; //ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrFaceNew, "CalcBoundingBox"), "Min"), "y");
                    zMin = GrFaceNew.CalcBoundingBox(GrFaceNew.UCS).Min.Z; //ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrFaceNew, "CalcBoundingBox"), "Min"), "z");
                    xc = (xMax + xMin) / 2;
                    yc = (yMax + yMin) / 2;
                    zc = (zMax + zMin) / 2;
                    dx = xMax - xMin;
                    dy = yMax - yMin;
                    dz = zMax - zMin;
                    SumCoordNew = xMax + yMax + zMax + xMin + yMin + zMin + xc + yc + zc + dx + dy + dz;

                }
                else
                {
                    if (hGrCoord > 0)
                    {

                        //TCWGraphicDraw hGrCoord, 1
                        //GrCoord.Visible = False
                        //GrCoord.Draw

                    }
                }

                if (GrFaceNew == null)
                {
                    if (GrFace != null)
                    {
                        GrFace.Visible = false;
                        GrFace.Draw();
                        //ReflectionHelper.LetMember(GrFace, "Visible", false);
                        //ReflectionHelper.Invoke(GrFace, "Draw", new object[] { });
                        //UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                        //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
                        Module1.ActDr.Graphics.Remove(GrFace.Index);
                        GrFace.Delete();
                        //ReflectionHelper.Invoke(ReflectionHelper.GetMember(Module1.ActDr, "Graphics"), "Remove", new object[] { ReflectionHelper.GetMember(GrFace, "Index") });
                        //ReflectionHelper.Invoke(GrFace, "Delete", new object[] { });
                        GrFace = null;
                        hGrFace = 0;
                    }
                    SumCoordPrev = 0;
                }
                else
                {

                    result = true;
                    //MsgBox ("SumCoordNew=" & CStr(SumCoordNew) & "  SumCoordPrev=" & CStr(SumCoordPrev))

                    if (Math.Abs(SumCoordNew - SumCoordPrev) > 1000 * Module1.Eps)
                    {
                        if (GrFace != null)
                        {
                            GrFace.Visible = false;
                            GrFace.Draw();
                            //ReflectionHelper.LetMember(GrFace, "Visible", false);
                            //ReflectionHelper.Invoke(GrFace, "Draw", new object[] { });
                            //UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                            //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
                            Module1.ActDr.Graphics.Remove(GrFace.Index);
                            GrFace.Delete();
                            //ReflectionHelper.Invoke(ReflectionHelper.GetMember(Module1.ActDr, "Graphics"), "Remove", new object[] { ReflectionHelper.GetMember(GrFace, "Index") });
                            //ReflectionHelper.Invoke(GrFace, "Delete", new object[] { });
                            GrFace = null;
                            hGrFace = 0;
                        }
                        GrFace = GrFaceNew;
                        if (GrFace != null)
                        {
                            Module1.ActDr.Graphics.AddGraphic(GrFace, null, null);
                            GrFace.Properties.get_Item("PenWidth").Value = 0.02d;
                            GrFace.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 255);
                            GrFace.Properties.get_Item("PenStyle").Value = "CONTINUOUS";
                            GrFace.Draw();
                            //ReflectionHelper.Invoke(ReflectionHelper.GetMember(Module1.ActDr, "Graphics"), "AddGraphic", new object[] { GrFace });
                            //ReflectionHelper.LetMember(GrFace, "Properties", 0.02d, "PenWidth");
                            //ReflectionHelper.LetMember(GrFace, "Properties", Color.FromArgb(0, 0, 255), "PenColor");
                            //ReflectionHelper.LetMember(GrFace, "Properties", "CONTINUOUS", "PenStyle");
                            //ReflectionHelper.Invoke(GrFace, "Draw", new object[] { });
                            hGrFace = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, ReflectionHelper.GetMember<int>(GrFace, "Index"));

                        }
                        SumCoordPrev = SumCoordNew;
                        result = true;
                    }
                    else
                    {
                        GrFaceNew.Delete();
                        //ReflectionHelper.Invoke(GrFaceNew, "Delete", new object[] { });
                        GrFaceNew = null;
                    }
                }
                //GrHit.Delete
                //Set GrHit = Nothing


                int hRes = 0;
                double yHit = 0, xHit = 0, zHit = 0;
                double yNor = 0, xNor = 0, zNor = 0;
                double yAx1 = 0, xAx1 = 0, zAx1 = 0;
                double yAx2 = 0, xAx2 = 0, zAx2 = 0;

                if (IntCount == 0)
                {
                    hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVer);
                    hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hvHit);
                    hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hvHitNor);
                    hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVCurvAxis1);
                    hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVCurvAxis2);
                    //hRes = TCWGraphicDispose(hgAct)
                    return result;
                }
                else
                {

                    xHit = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hvHit);
                    yHit = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hvHit);
                    zHit = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hvHit);
                    xNor = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hvHitNor);
                    yNor = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hvHitNor);
                    zNor = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hvHitNor);
                    xAx1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hVCurvAxis1);
                    yAx1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hVCurvAxis1);
                    zAx1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hVCurvAxis1);
                    xAx2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hVCurvAxis2);
                    yAx2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hVCurvAxis2);
                    zAx2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hVCurvAxis2);
                    if (hv0 > 0 && hVx > 0 && hVy > 0 && hVz > 0)
                    {
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hv0, xHit, yHit, zHit);
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hVz, xHit + xNor, yHit + yNor, zHit + zNor);
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hVx, xHit + xAx1, yHit + yAx1, zHit + zAx1);
                        BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hVy, xHit + xAx2, yHit + yAx2, zHit + zAx2);
                        result = true;
                    }
                }
                hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVer);
                hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hvHit);
                hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hvHitNor);
                hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVCurvAxis1);
                hRes = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexDispose(hVCurvAxis2);
                //hRes = TCWGraphicDispose(hgAct)

                return result;
            }
            catch
            {
                MessageBox.Show("creategraphic failed");
                
            }
            return false;
		}

		private void AddInspectorBar(bool bAdd)
		{
            Properties gxProps = gxMe.Properties;
            int PropCount = Convert.ToInt32(gxProps.get_Item("Count"));
            if (bAdd)
			{
                if (PropCount == 0)
				{
                    MessageBox.Show("Here");
                    object tempRefParam = 0d;
                    //gxProps.Add("IntRadius", ref tempRefParam);
                    //object tempRefParam2 = null;
                    //object tempRefParam3 = 1;
                    gxProps.Add("IntRadius", ref tempRefParam);
                    //gxProps.get_Item(Convert.ToInt32("IntRadius")).Value =  0d;//, null, 1); //CommonLocaliz.TcLoadLangString(143)
                    MessageBox.Show("Here2");
                    object tempRefParam4 = 90d;
                    //object tempRefParam5 = null;
                    //object tempRefParam6 = 2;
                    gxProps.get_Item("BendAngle").Value = 90d;//, ref tempRefParam4);//, ref tempRefParam5, ref tempRefParam6); // CommonLocaliz.TcLoadLangString(144)
                    MessageBox.Show("Here 3");
                    object tempRefParam7 = 0d;
					//object tempRefParam8 = null;
					//object tempRefParam9 = 3;
                    gxProps.get_Item("neutralDepth").Value = 0d;//, ref tempRefParam8, ref tempRefParam9); // CommonLocaliz.TcLoadLangString(145)
                }
                
				IntRadius = Convert.ToDouble(gxProps.get_Item(CommonLocaliz.TcLoadLangString(143)).Value);
				BendAngle = Convert.ToDouble(gxProps.get_Item(CommonLocaliz.TcLoadLangString(144)).Value);
				NeutralDepth = Convert.ToDouble(gxProps.get_Item(CommonLocaliz.TcLoadLangString(145)).Value);
                MessageBox.Show("AddInspectorBar 3");
            }
            
            IMSIGX.ToolEvents xdbToolEvents = (IMSIGX.ToolEvents)gxMe.Application.ToolEvents;
            //ReflectionHelper.Invoke(xdbToolEvents, "ToolChangeProperties", new object[]{gxMe, lFlags});
        }


		//Returns true if tool is correctly initialized
		public void Terminate(Tool Context)
		{
            try
            {
                Finish();
                gxMe = null;
                //Set TheCursorImage = Nothing

                return;
            }
            catch
            {
                MessageBox.Show("Failed 4");
            }
		}


		public void CreateCoordinateGraphic()
		{
            try
            {

                double ViH = gxMe.Application.ActiveDrawing.ActiveView.ViewHeight;
                ViH = 0.2d * ViH;
                int hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();
                GrCoord = Module1.Grs.Add(11);//ref tempRefParam, ref tempRefParam2, ref tempRefParam3, ref tempRefParam4, ref tempRefParam5, ref tempRefParam6);
                hGrCoord = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrCoord.Index);// ReflectionHelper.GetMember<int>(GrCoord, "Index"));

                IMSIGX.Graphic GrChild = Module1.Grs.Add(11);//ref tempRefParam7, ref tempRefParam8, ref tempRefParam9, ref tempRefParam10, ref tempRefParam11, ref tempRefParam12);
                GrChild.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0);
                GrChild.Properties.get_Item("PenWidth").Value = 0;
                GrChild.Properties.get_Item("PenStyle").Value = "CONTINUOUS";
                //ReflectionHelper.LetMember(GrChild, "Properties", Color.FromArgb(0, 0, 0), "PenColor");
                //         ReflectionHelper.LetMember(GrChild, "Properties", 0, "PenWidth");
                //ReflectionHelper.LetMember(GrChild, "Properties", "CONTINUOUS", "PenStyle");
                hGrCoordX = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrChild.Index);// ReflectionHelper.GetMember<int>(GrChild, "Index"));
                Module1.Grs.Remove(GrChild.Index);
                GrCoord.Graphics.AddGraphic(GrChild, null, null);
                //GrCoord.Graphics.AddGraphic(GrChild, GrChild.Index+1, GrChild.Index);
                GrChild.Vertices.UseWorldCS = true;
                GrChild.Vertices.Add(0, 0, 0);
                GrChild.Vertices.Add(ViH, 0, 0);
                //         ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrChild, "Vertices"), "UseWorldCS", true);
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{0, 0, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{ViH, 0, 0});

                GrChild = Module1.Grs.Add(11);//ref tempRefParam14, ref tempRefParam15, ref tempRefParam16, ref tempRefParam17, ref tempRefParam18, ref tempRefParam19);
                GrChild.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0);
                GrChild.Properties.get_Item("PenWidth").Value = 0;
                GrChild.Properties.get_Item("PenStyle").Value = "CONTINUOUS";

                //         ReflectionHelper.LetMember(GrChild, "Properties", Color.FromArgb(0, 0, 0), "PenColor");
                //ReflectionHelper.LetMember(GrChild, "Properties", 0, "PenWidth");
                //ReflectionHelper.LetMember(GrChild, "Properties", "CONTINUOUS", "PenStyle");
                hGrCoordY = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrChild.Index);// ReflectionHelper.GetMember<int>(GrChild, "Index"));


                Module1.Grs.Remove(GrChild.Index);
                GrCoord.Graphics.AddGraphic(GrChild, null, null);
                GrChild.Vertices.UseWorldCS = true;
                GrChild.Vertices.Add(0, 0, 0);
                GrChild.Vertices.Add(0, ViH, 0);
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCoord, "Graphics"), "AddGraphic", new object[]{GrChild});
                //         ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrChild, "Vertices"), "UseWorldCS", true);
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{0, 0, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{0, ViH, 0});

                GrChild = (IMSIGX.Graphic)Module1.Grs.Add(11);//ref tempRefParam21, ref tempRefParam22, ref tempRefParam23, ref tempRefParam24, ref tempRefParam25, ref tempRefParam26);
                GrChild.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0);
                GrChild.Properties.get_Item("PenWidth").Value = 0;
                GrChild.Properties.get_Item("PenStyle").Value = "CONTINUOUS";
                //         ReflectionHelper.LetMember(GrChild, "Properties", Color.FromArgb(0, 0, 0), "PenColor");
                //ReflectionHelper.LetMember(GrChild, "Properties", 0, "PenWidth");
                //ReflectionHelper.LetMember(GrChild, "Properties", "CONTINUOUS", "PenStyle");

                hGrCoordZ = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrChild.Index);// ReflectionHelper.GetMember<int>(GrChild, "Index"));
                Module1.Grs.Remove(GrChild.Index);
                GrCoord.Graphics.AddGraphic(GrChild, null, null);
                GrChild.Vertices.UseWorldCS = true;
                GrChild.Vertices.Add(0, 0, 0);
                GrChild.Vertices.Add(0, 0, ViH);

                //         ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrCoord, "Graphics"), "AddGraphic", new object[]{GrChild});
                //ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrChild, "Vertices"), "UseWorldCS", true);
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{0, 0, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrChild, "Vertices"), "Add", new object[]{0, 0, ViH});
                //ReflectionHelper.LetMember(GrCoord, "Visible", true);
                GrCoord.Visible = true;
                double HSym = ViH / 8;
                double WSym = ViH / 10;
                GrSymX = (IMSIGX.Graphic)Module1.Grs.Add(11);//ref tempRefParam28, ref tempRefParam29, ref tempRefParam30, ref tempRefParam31, ref tempRefParam32, ref tempRefParam33);
                GrSymX.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0);
                GrSymX.Properties.get_Item("PenWidth").Value = 0;
                GrSymX.Properties.get_Item("PenStyle").Value = "CONTINUOUS";
                //         ReflectionHelper.LetMember(GrSymX, "Properties", Color.FromArgb(0, 0, 0), "PenColor");
                //ReflectionHelper.LetMember(GrSymX, "Properties", 0, "PenWidth");
                //ReflectionHelper.LetMember(GrSymX, "Properties", "CONTINUOUS", "PenStyle");

                hGrSymX = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrSymX.Index);// ReflectionHelper.GetMember<int>(GrSymX, "Index"));
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymX, 0, 1);
                GrSymX.Vertices.UseWorldCS = true;
                GrSymX.Vertices.Add((-WSym) / 2, (-HSym) / 2, 0);
                GrSymX.Vertices.Add(WSym / 2, HSym / 2, 0);
                GrSymX.Vertices.Add(WSym / 2, (-HSym) / 2, 0);//, false);
                GrSymX.Vertices.Add((-WSym) / 2, HSym / 2, 0);
                GrSymX.Visible = true;
                //         ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSymX, "Vertices"), "UseWorldCS", true);
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymX, "Vertices"), "Add", new object[]{(-WSym) / 2, (-HSym) / 2, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymX, "Vertices"), "Add", new object[]{WSym / 2, HSym / 2, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymX, "Vertices"), "Add", new object[]{WSym / 2, (-HSym) / 2, 0, false});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymX, "Vertices"), "Add", new object[]{(-WSym) / 2, HSym / 2, 0});
                //ReflectionHelper.LetMember(GrSymX, "Visible", true);
                GrSymY = (IMSIGX.Graphic)Module1.Grs.Add(11);//ref tempRefParam34, ref tempRefParam35, ref tempRefParam36, ref tempRefParam37, ref tempRefParam38, ref tempRefParam39);
                GrSymY.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0);
                GrSymY.Properties.get_Item("PenWidth").Value = 0;
                GrSymY.Properties.get_Item("PenStyle").Value = "CONTINUOUS";
                //         ReflectionHelper.LetMember(GrSymY, "Properties", Color.FromArgb(0, 0, 0), "PenColor");
                //ReflectionHelper.LetMember(GrSymY, "Properties", 0, "PenWidth");
                //ReflectionHelper.LetMember(GrSymY, "Properties", "CONTINUOUS", "PenStyle");
                hGrSymY = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrSymY.Index);// ReflectionHelper.GetMember<int>(GrSymY, "Index"));
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymY, 0, 1);
                GrSymY.Vertices.UseWorldCS = true;
                GrSymY.Vertices.Add(0, (-HSym) / 2, 0);
                GrSymY.Vertices.Add(0, 0, 0);
                GrSymY.Vertices.Add(WSym / 2, HSym / 2, 0);
                GrSymY.Vertices.Add((-WSym) / 2, HSym / 2, 0);//, false);
                GrSymY.Vertices.Add(0, 0, 0);
                GrSymY.Visible = true;
                //         ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSymY, "Vertices"), "UseWorldCS", true);
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymY, "Vertices"), "Add", new object[]{0, (-HSym) / 2, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymY, "Vertices"), "Add", new object[]{0, 0, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymY, "Vertices"), "Add", new object[]{WSym / 2, HSym / 2, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymY, "Vertices"), "Add", new object[]{(-WSym) / 2, HSym / 2, 0, false});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymY, "Vertices"), "Add", new object[]{0, 0, 0});
                //ReflectionHelper.LetMember(GrSymY, "Visible", true);

                GrSymZ = (IMSIGX.Graphic)Module1.Grs.Add(11);//ref tempRefParam40, ref tempRefParam41, ref tempRefParam42, ref tempRefParam43, ref tempRefParam44, ref tempRefParam45);
                GrSymZ.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0);
                GrSymZ.Properties.get_Item("PenWidth").Value = 0;
                GrSymZ.Properties.get_Item("PenStyle").Value = "CONTINUOUS";
                //         ReflectionHelper.LetMember(GrSymZ, "Properties", Color.FromArgb(0, 0, 0), "PenColor");
                //ReflectionHelper.LetMember(GrSymZ, "Properties", 0, "PenWidth");
                //ReflectionHelper.LetMember(GrSymZ, "Properties", "CONTINUOUS", "PenStyle");
                hGrSymZ = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicAt(hDr, GrSymZ.Index);// ReflectionHelper.GetMember<int>(GrSymZ, "Index"));
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymZ, 0, 1);
                GrSymZ.Vertices.UseWorldCS = true;
                GrSymZ.Vertices.Add(WSym / 2, (-HSym) / 2, 0);
                GrSymZ.Vertices.Add((-WSym) / 2, (-HSym) / 2, 0);
                GrSymZ.Vertices.Add(WSym / 2, HSym / 2, 0);
                GrSymZ.Vertices.Add(WSym / 2, HSym / 2, 0);//, false);
                GrSymZ.Vertices.Add((-WSym) / 2, HSym / 2, 0);
                GrSymZ.Visible = true;
                //         ReflectionHelper.LetMember(ReflectionHelper.GetMember(GrSymZ, "Vertices"), "UseWorldCS", true);
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymZ, "Vertices"), "Add", new object[]{WSym / 2, (-HSym) / 2, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymZ, "Vertices"), "Add", new object[]{(-WSym) / 2, (-HSym) / 2, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymZ, "Vertices"), "Add", new object[]{WSym / 2, HSym / 2, 0});
                //ReflectionHelper.Invoke(ReflectionHelper.GetMember(GrSymZ, "Vertices"), "Add", new object[]{(-WSym) / 2, HSym / 2, 0});
                //ReflectionHelper.LetMember(GrSymZ, "Visible", true);

                int hVp = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetActiveViewport(hDr);
                int hmVp = BendToolSupport.PInvoke.SafeNative.dbapi24.ViewportGetMatrix(hVp);
                int hmVpInv = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixOrthoInvertEx(hmVp);
                int mTmp = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixMatrixMultiplyEx(hmVpInv, 0);
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymX, mTmp, 1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymY, mTmp, 1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymZ, mTmp, 1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref hmVpInv);
                BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref mTmp);
                //Matrix mtempox = null; mtempox.SetEntry(0, 0, 1); mtempox.SetEntry(0, 1, 0); mtempox.SetEntry(0, 2, 0); mtempox.SetEntry(1, 0, 0); mtempox.SetEntry(1, 1, 1); mtempox.SetEntry(1, 2, 0); mtempox.SetEntry(2, 0, 0); mtempox.SetEntry(2, 1, 0); mtempox.SetEntry(2, 2, 1);
                //Matrix mtempoy = null; mtempoy.SetEntry(0, 0, 1); mtempoy.SetEntry(0, 1, 0); mtempoy.SetEntry(0, 2, 0); mtempoy.SetEntry(1, 0, 0); mtempoy.SetEntry(1, 1, 1); mtempoy.SetEntry(1, 2, 0); mtempoy.SetEntry(2, 0, 0); mtempoy.SetEntry(2, 1, 0); mtempoy.SetEntry(2, 2, 1);
                //Matrix mtempoz = null; mtempoz.SetEntry(0, 0, 1); mtempoz.SetEntry(0, 1, 0); mtempoz.SetEntry(0, 2, 0); mtempoz.SetEntry(1, 0, 0); mtempoz.SetEntry(1, 1, 1); mtempoz.SetEntry(1, 2, 0); mtempoz.SetEntry(2, 0, 0); mtempoz.SetEntry(2, 1, 0); mtempoz.SetEntry(2, 2, 1);
                double yc = 0, xc = 0, zc = 0;
                xc = 0.5d * GrSymX.CalcBoundingBox(GrSymX.UCS).Max.X + GrSymX.CalcBoundingBox(GrSymX.UCS).Min.X; // (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Min"), "x"));
                yc = 0.5d * GrSymY.CalcBoundingBox(GrSymY.UCS).Max.Y + GrSymY.CalcBoundingBox(GrSymY.UCS).Min.Y; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Min"), "y"));
                zc = 0.5d * GrSymZ.CalcBoundingBox(GrSymZ.UCS).Max.Z + GrSymZ.CalcBoundingBox(GrSymZ.UCS).Min.Z; // (ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Max"), "z") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Min"), "z"));

                int hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordX, 1);
                double x1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv1);
                double y1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv1);
                double z1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv1);
                GrSymX.MoveRelative(x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc);
                //ReflectionHelper.Invoke(GrSymX, "MoveRelative", new object[]{x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc});
                //MsgBox ("xc=" & CStr(xc) & " yc=" & CStr(yc) & " zc=" & CStr(zc) & " x1=" & CStr(x1) & " y1=" & CStr(y1) & " z1=" & CStr(z1))

                xc = 0.5d * GrSymX.CalcBoundingBox(GrSymX.UCS).Max.X + GrSymX.CalcBoundingBox(GrSymX.UCS).Min.X;
                yc = 0.5d * GrSymY.CalcBoundingBox(GrSymY.UCS).Max.Y + GrSymY.CalcBoundingBox(GrSymY.UCS).Min.Y;
                zc = 0.5d * GrSymZ.CalcBoundingBox(GrSymZ.UCS).Max.Z + GrSymZ.CalcBoundingBox(GrSymZ.UCS).Min.Z;
                hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordY, 1);
                x1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv1);
                y1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv1);
                z1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv1);

                GrSymY.MoveRelative(x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc);
                //ReflectionHelper.Invoke(GrSymY, "MoveRelative", new object[]{x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc});

                xc = 0.5d * GrSymX.CalcBoundingBox(GrSymX.UCS).Max.X + GrSymX.CalcBoundingBox(GrSymX.UCS).Min.X; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Min"), "x"));
                yc = 0.5d * GrSymY.CalcBoundingBox(GrSymY.UCS).Max.Y + GrSymY.CalcBoundingBox(GrSymY.UCS).Min.Y; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Min"), "y"));
                zc = 0.5d * GrSymZ.CalcBoundingBox(GrSymZ.UCS).Max.Z + GrSymZ.CalcBoundingBox(GrSymZ.UCS).Min.Z; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Max"), "z") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Min"), "z"));
                hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordZ, 1);
                x1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv1);
                y1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv1);
                z1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv1);
                GrSymZ.MoveRelative(x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc);
                //ReflectionHelper.Invoke(GrSymZ, "MoveRelative", new object[]{x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc});
                //GrSymX.MoveRelative ViH, HSym, 0
                //GrSymY.MoveRelative WSym, ViH, 0
                //GrSymZ.MoveRelative WSym, HSym, ViH

                //TCWGraphicDraw hGrCoord, 1
            }
            catch
            {
                MessageBox.Show("Failed 3");
            }

        }


		public void UpdateCoordinateGraphic(int hv0, int hVx, int hVy, int hVz)
		{
            try
            {
                if (GrCoord == null || hGrCoord < 1)
                {
                    return;
                }

                double ViH = gxMe.Application.ActiveDrawing.ActiveView.ViewHeight;
                ViH = 0.2d * ViH;
                //GrCoord.Visible = False
                //GrCoord.Draw
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrCoord, 1);

                //Dim hVp As Long
                //    hVp = TCWViewActive
                //GraphicDrawEx2 hGrCoord, 0, hVp, 4, 1



                double x0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv0);
                double y0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv0);
                double z0 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv0);

                double xx = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hVx) * ViH;
                double yx = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hVx) * ViH;
                double zx = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hVx) * ViH;

                double xy = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hVy) * ViH;
                double yy = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hVy) * ViH;
                double zy = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hVy) * ViH;

                double xz = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hVz) * ViH;
                double yz = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hVz) * ViH;
                double zz = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hVz) * ViH;

                int hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoord, 0);
                int hv2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoord, 1);


                //TCWGraphicDraw hGrCoordX, 0
                //TCWGraphicDraw hGrCoordY, 0
                //TCWGraphicDraw hGrCoordZ, 0

                hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordX, 0);
                hv2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordX, 1);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hv1, x0, y0, z0);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hv2, x0 + xx, y0 + yx, z0 + zx);

                hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordY, 0);
                hv2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordY, 1);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hv1, x0, y0, z0);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hv2, x0 + xy, y0 + yy, z0 + zy);


                hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordZ, 0);
                hv2 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordZ, 1);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hv1, x0, y0, z0);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWSetXYZ(hv2, x0 + xz, y0 + yz, z0 + zz);

                //TCWGraphicDraw hGrCoordX, 1
                //TCWGraphicDraw hGrCoordY, 1
                //TCWGraphicDraw hGrCoordZ, 1

                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrCoord, 1);
                //GraphicDrawEx2 hGrCoord, 0, hVp, 3, 1
                //GrCoord.Visible = True
                //GrCoord.Draw

                double HSym = ViH / 8;
                double WSym = ViH / 10;

                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymX, 1);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymY, 1);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymZ, 1);

                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymX, 0, 1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymY, 0, 1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymZ, 0, 1);

                double dy = 0;
                dy = GrSymY.CalcBoundingBox(GrSymY.UCS).Max.Y - GrSymY.CalcBoundingBox(GrSymY.UCS).Min.Y; //ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Max"), "y") - ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Min"), "y");
                double ScaleSym = 1;
                ScaleSym = HSym / dy;
                //UPGRADE_ISSUE: (2068) Matrix object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
                IMSIGX.Matrix mat = null;
                if (Math.Abs(1 - ScaleSym) > 0.01d)
                {
                    mat = GrSymX.Scale(ScaleSym, ScaleSym, 1);
                    mat = GrSymY.Scale(ScaleSym, ScaleSym, 1);
                    mat = GrSymZ.Scale(ScaleSym, ScaleSym, 1);
                }

                int hDr = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWDrawingActive();

                int hVp = BendToolSupport.PInvoke.SafeNative.dbapi24.DrawingGetActiveViewport(hDr);
                int hmVp = BendToolSupport.PInvoke.SafeNative.dbapi24.ViewportGetMatrix(hVp);
                int hmVpInv = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixOrthoInvertEx(hmVp);
                int mTmp = BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixMatrixMultiplyEx(hmVpInv, 0);
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymX, mTmp, 1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymY, mTmp, 1);
                BendToolSupport.PInvoke.SafeNative.dbapi24.GraphicSetMatrix(hGrSymZ, mTmp, 1);

                BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref hmVpInv);
                BendToolSupport.PInvoke.SafeNative.dbapi24.MatrixDispose(ref mTmp);


                double yc = 0, xc = 0, zc = 0;

                xc = 0.5d * GrSymX.CalcBoundingBox(GrSymX.UCS).Max.X + GrSymX.CalcBoundingBox(GrSymX.UCS).Min.X; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Min"), "x"));
                yc = 0.5d * GrSymY.CalcBoundingBox(GrSymY.UCS).Max.Y + GrSymY.CalcBoundingBox(GrSymY.UCS).Min.Y; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Min"), "y"));
                zc = 0.5d * GrSymZ.CalcBoundingBox(GrSymZ.UCS).Max.Z + GrSymZ.CalcBoundingBox(GrSymZ.UCS).Min.Z; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Max"), "z") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymX, "CalcBoundingBox"), "Min"), "z"));

                hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordX, 1);
                double x1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv1);
                double y1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv1);
                double z1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv1);
                GrSymX.MoveRelative(x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc);
                //ReflectionHelper.Invoke(GrSymX, "MoveRelative", new object[]{x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc});
                //MsgBox ("xc=" & CStr(xc) & " yc=" & CStr(yc) & " zc=" & CStr(zc) & " x1=" & CStr(x1) & " y1=" & CStr(y1) & " z1=" & CStr(z1))

                xc = 0.5d * GrSymX.CalcBoundingBox(GrSymX.UCS).Max.X + GrSymX.CalcBoundingBox(GrSymX.UCS).Min.X; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymY, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymY, "CalcBoundingBox"), "Min"), "x"));
                yc = 0.5d * GrSymY.CalcBoundingBox(GrSymY.UCS).Max.Y + GrSymY.CalcBoundingBox(GrSymY.UCS).Min.Y; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymY, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymY, "CalcBoundingBox"), "Min"), "y"));
                zc = 0.5d * GrSymZ.CalcBoundingBox(GrSymZ.UCS).Max.Z + GrSymZ.CalcBoundingBox(GrSymZ.UCS).Min.Z; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymY, "CalcBoundingBox"), "Max"), "z") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymY, "CalcBoundingBox"), "Min"), "z"));
                hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordY, 1);
                x1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv1);
                y1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv1);
                z1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv1);
                GrSymY.MoveRelative(x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc);
                //ReflectionHelper.Invoke(GrSymY, "MoveRelative", new object[]{x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc});

                xc = 0.5d * GrSymX.CalcBoundingBox(GrSymX.UCS).Max.X + GrSymX.CalcBoundingBox(GrSymX.UCS).Min.X; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Max"), "x") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Min"), "x"));
                yc = 0.5d * GrSymY.CalcBoundingBox(GrSymY.UCS).Max.Y + GrSymY.CalcBoundingBox(GrSymY.UCS).Min.Y; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Max"), "y") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Min"), "y"));
                zc = 0.5d * GrSymZ.CalcBoundingBox(GrSymZ.UCS).Max.Z + GrSymZ.CalcBoundingBox(GrSymZ.UCS).Min.Z; //(ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Max"), "z") + ReflectionHelper.GetMember<double>(ReflectionHelper.GetMember(ReflectionHelper.GetMember(GrSymZ, "CalcBoundingBox"), "Min"), "z"));
                hv1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWVertexAt(hGrCoordZ, 1);
                x1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetX(hv1);
                y1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetY(hv1);
                z1 = BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGetZ(hv1);
                GrSymZ.MoveRelative(x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc);
                //ReflectionHelper.Invoke(GrSymZ, "MoveRelative", new object[]{x1 - xc + HSym / 2, y1 - yc + HSym / 2, z1 - zc});

                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymX, 1);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymY, 1);
                BendToolSupport.PInvoke.SafeNative.tcapi24.TCWGraphicDraw(hGrSymZ, 1);
            }
            catch
            {
                MessageBox.Show("Failed 2");
            }

		}

		public void TransformUcsPointToWorld(IMSIGX.Matrix Ucs, double x, double y, double z, ref double xW, ref double yW, ref double zW)
		{
            try
            {
                double f02 = 0, f00 = 0, f01 = 0, f03 = 0;
                double f12 = 0, f10 = 0, f11 = 0, f13 = 0;
                double f22 = 0, f20 = 0, f21 = 0, f23 = 0;
                double f32 = 0, f30 = 0, f31 = 0, f33 = 0;
                //ReflectionHelper.Invoke(Ucs, "GetEntries", new object[]{f00, f01, f02, f03, f10, f11, f12, f13, f20, f21, f22, f23, f30, f31, f32, f33});
                Ucs.GetEntries(ref f00, ref f01, ref f02, ref f03, ref f10, ref f11, ref f12, ref f13, ref f20, ref f21, ref f22, ref f23, ref f30, ref f31, ref f32, ref f33);
                xW = f30 + x * f00 + y * f10 + z * f20;
                yW = f31 + x * f01 + y * f11 + z * f21;
                zW = f32 + x * f02 + y * f12 + z * f22;
            }
            catch
            {
                MessageBox.Show("Failed 1");
            }

		}
        public void AddToolPrompt(string strPrompt)
        {

            IMSIGX.Application gxApp = gxMe.Application;
            IMSIGX.ToolEvents xdbToolEvents = gxApp.ToolEvents;
            ReflectionHelper.Invoke(xdbToolEvents, "ToolChangePrompt", new object[] { this, "", false });
            if (strPrompt != "")
            {
                ReflectionHelper.Invoke(xdbToolEvents, "ToolChangePrompt", new object[] { this, strPrompt, false });
                //xdbToolEvents.ToolChangePrompt(xdbToolEvents, strPrompt, false);
            }

        }
    }
}