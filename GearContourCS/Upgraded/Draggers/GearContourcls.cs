using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using TCDotNetInterfaces;
using IMSIGX;

namespace Gear
{
	public class Contour : ITurboCADTool
    {
        #region ITurboCADTool Members
        //Number of tools in this server
		const int NUM_TOOLS = 1;
		public double xcWorld = 0;
		public double ycWorld = 0;

		// *******************************************************************
		//    Const EventMask = 268435456 + 536870912
		//    Const EventMask = 1024 + 268435456 + 536870912
		IMSIGX.ImsiEventMask EventMask = ( IMSIGX.ImsiEventMask.imsiEventBeforeExit) | (IMSIGX.ImsiEventMask.imsiEventDrawingNew) | ( IMSIGX.ImsiEventMask.imsiEventDrawingOpen) | ( IMSIGX.ImsiEventMask.imsiEventDrawingActivate) | ( IMSIGX.ImsiEventMask.imsiEventDrawingDeactivate) | ( IMSIGX.ImsiEventMask.imsiEventDrawingBeforeClose) | ( IMSIGX.ImsiEventMask.imsiEventDrawingBeforeSave) | ( IMSIGX.ImsiEventMask.imsiEventMouseDown) | ( IMSIGX.ImsiEventMask.imsiEventMouseMove) | ( IMSIGX.ImsiEventMask.imsiEventRunTool) | ( IMSIGX.ImsiEventMask.imsiEventCancel) | ( IMSIGX.ImsiEventMask.imsiEventUpdateUndo) | ( IMSIGX.ImsiEventMask.imsiEventViewAfterRedraw);


		private int iConnectId = 0;
		private int iActTool = 0;
        //private IMSIGX.ToolEvents theToolEvents = null;
        private Gear.TCEventSink theToolEvents = null;
        
        

		// *******************************************************************
		//Toggle this to test loading buttons from .Bmp/.Res
		const bool boolLoadFromBmp = false;
		const bool boolDebug = false;
		//##########################################################################
		//##########################################################################
		//Private Declare Function TCWUndoClear Lib "TCAPI90.dll" () As Long
		//Private Declare Function TCWUndoRecordEnd Lib "TCAPI90.dll" (ByVal d As Long) As Long
		//Private Declare Function TCWUndoRecordStart Lib "TCAPI90.dll" (ByVal d As Long, ByRef Title As String) As Long
		//Private Declare Function TCWUndoRecordAddGraphic Lib "TCAPI90.dll" (ByVal d As Long, ByVal g As Long) As Long
		//Private Declare Function TCWUndo Lib "TCAPI90.dll" (ByVal N As Long) As Long

		//Private Declare Function TCWDrawingActive Lib "TCAPI90.dll" () As Long
		//Private Declare Function TCWGraphicAt Lib "TCAPI90.dll" (ByVal d As Long, ByVal Index As Long) As Long
		//Private Declare Function TCWVertexAt Lib "TCAPI90.dll" (ByVal g As Long, ByVal Index As Long) As Long

		//Private Declare Function TCWGraphicReferencePointGet Lib "TCAPI90.dll" (ByVal g As Long) As Long
		//Private Declare Function TCWGraphicReferencePointSet Lib "TCAPI90.dll" (ByVal g As Long, ByVal v As Long) As Long


		//Private Declare Function GraphicGetMatrix Lib "DBAPI90.dll" (ByVal hGr As Long) As Long
		//Private Declare Function GraphicSetMatrix Lib "DBAPI90.dll" (ByVal hGr As Long, ByVal hMat As Long) As Boolean
		//Private Declare Function MatrixEqual Lib "DBAPI90.dll" (ByVal hM1 As Long, ByVal hM2 As Long) As Boolean

		//Private Declare Function TCWViewRedraw Lib "TCAPI90.dll" () As Long

		public bool ParExist = false;
		public IMSIGX.Graphic GrMoved = null;

		public IMSIGX.GraphicSet ResSet = null;

		public IMSIGX.Tool gxMe = null;
		public IMSIGX.ImsiSpaceModeType Space = IMSIGX.ImsiSpaceModeType.imsiPaperSpace;

		//Copy a windows bitmap of the requested size to the clipboard
		//Bitmaps returned should contain NUM_TOOLS images
		//Size of entire bitmap:
		//Normal:  (NUM_TOOLS*16) wide x 15 high
		//Large:   (NUM_TOOLS*24) wide x 23 high
		//Mono bitmap should be 1-bit (black or white)
        //public bool CopyBitmap(bool LargeImage, bool MonoImage)
        //{
        //    try
        //    {

        //        Image TheImage = new Bitmap(1, 1);
        //        if (GetButtonPicture(LargeImage, MonoImage, ref TheImage))
        //        {
        //            //Put the image on the Windows clipboard
        //            Clipboard.SetData(DataFormats.Dib, TheImage);
        //            return true;
        //        }
        //    }
        //    catch
        //    {
        //    }


        //    return false;
        //}

		//Return a Picture object for the requested size
		//Apparently, returning references to StdPicture objects doesn't work for .EXE servers
		//Bitmaps returned should contain NUM_TOOLS images
		//Size of entire image:
		//Normal:  (NUM_TOOLS*16) wide x 15 high
		//Large:   (NUM_TOOLS*24) wide x 23 high
		//Mono image should be 1-bit (black or white)
        //public object GetPicture(bool LargeImage, bool MonoImage)
        //{
        //    try
        //    {

        //        Image TheImage = new Bitmap(1, 1);
        //        if (GetButtonPicture(LargeImage, MonoImage, ref TheImage))
        //        {
        //            return TheImage;
        //        }
        //    }
        //    catch
        //    {
        //    }


        //    return null;
        //}

		//Implementation specific stuff
		//Private function to return the bitmap from .Res file or .Bmp file
        //private bool GetButtonPicture(bool LargeImage, bool MonoImage, ref Image TheImage)
        //{
        //    bool result = false;
        //    try
        //    {

        //        //There are two ways to load images:  from .Bmp file(s) or from .RES resource.
        //        //In this demo, we control the loading by a private variable.

        //        //Note that if you are loading from .Bmp, or if you are running this tool as a
        //        //.VBP for debugging, you must place the .Res or .Bmp files in the Draggers subdirectory
        //        //of the directory in which TCW40.EXE (or IMSIGX40.DLL) is located.

        //        string strFileName = "";
        //        int idBitmap = 0; //BITMAP resource id in .Res file //File name of .Bmp file to load
        //        if (boolLoadFromBmp)
        //        {
        //            //Load from .Bmp file

        //            if (LargeImage)
        //            {
        //                strFileName = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\button24.bmp"; //# NLS#'
        //            }
        //            else
        //            {
        //                strFileName = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\button16.bmp"; //# NLS#'
        //            }
        //            TheImage = Image.FromFile(strFileName);
        //        }
        //        else
        //        {
        //            //Load from .Res file

        //            if (LargeImage)
        //            {
        //                idBitmap = 1002;
        //            }
        //            else
        //            {
        //                idBitmap = 1001;
        //            }
        //            TheImage = (Bitmap) App.Resources.Resources.ResourceManager.GetObject("bmp" + idBitmap.ToString());
        //        }

        //        //Return the image
        //        return true;
        //    }
        //    catch
        //    {

        //        if (boolDebug)
        //        {
        //            //        MsgBox "Error loading bitmap: " & Err.Description
        //        }
        //        result = false;
        //    }
        //    return result;
        //}


		//##########################################################################
		//##########################################################################
		//##########################################################################


		//Return a description string for this package of tools
		public string Description
		{
			get
			{
				//    Description = "AddGearContour"
				return "Add Gear Contour";
			}
		}


        //Called to perform tool function
        public bool Initialize(Tool Context)
        {
            try
            {
                gxMe = Context;
                Module1.objApp = gxMe.Application; //ReflectionHelper.GetMember<IMSIGX.Application>(Context, "Application");
                theToolEvents = new TCEventSink(this);//Module1.objApp;
                iConnectId = -1;
                iActTool = -1;
                
                return true;
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Initialize Failed");
                //   MsgBox Err.Description

            }
            return false;
        }

        public bool Run(Tool Context)
		{
            try
            {
                IMSIGX.Tool theXTool = null;
                theXTool = Context;
//                object tempRefParam = "TileMode";
                Space = (IMSIGX.ImsiSpaceModeType)Convert.ToInt32(theXTool.Application.ActiveDrawing.Properties.get_Item("TileMode").Value);
                ParExist = false;
                GrMoved = null;
                //    theXTool.Application.ActiveDrawing.Graphics.Unselect
                int hwndParent = 0;
                int CurrentStyle = 0;
                if (iConnectId != -1)
                {
                    Module1.objApp.DisconnectEvents(iConnectId);
                    AddLM(false);
                    iConnectId = -1;
                    iActTool = -1;
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, "", false });
                }
                else
                {
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, "", false });
                    //        theToolEvents.ToolChangePrompt Me, "Input Data", False
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, CommonLocaliz.TcLoadLangString(102), false });
                    Module1.ParamCanc = true;
                    //        AddInspectorBar True
                    if (Module1.objApp.GetProcessId() != GearContourSupport.PInvoke.SafeNative.kernel32.GetCurrentProcessId())
                    {


                        string tempRefParam2 = null;
                        string tempRefParam3 = Module1.objApp.Caption;
                        hwndParent = GearContourSupport.PInvoke.SafeNative.user32.FindWindow(ref tempRefParam2, ref tempRefParam3);

                        GearContourSupport.PInvoke.SafeNative.user32.BringWindowToTop(hwndParent);


                        CurrentStyle = GearContourSupport.PInvoke.SafeNative.user32.GetWindowLong(Form1.DefInstance.Handle.ToInt32(), Module1.GWL_STYLE);
                        GearContourSupport.PInvoke.SafeNative.user32.SetWindowLong(Form1.DefInstance.Handle.ToInt32(), Module1.GWL_STYLE, CurrentStyle | Module1.WS_POPUP);

                        //        SetParent frmToolReg.hwnd, hwndParent
                        GearContourSupport.PInvoke.SafeNative.user32.SetWindowLong(Form1.DefInstance.Handle.ToInt32(), Module1.GWL_HWNDPARENT, hwndParent);

                    }

                    Form1.DefInstance.ShowDialog();

                    if (Module1.ParamCanc)
                    {
                        Finish();
                        return false;
                    }
                    //object tempRefParam4 = EventMask;
                   
                    iConnectId = Module1.objApp.ConnectEvents(theToolEvents, EventMask);
                    iActTool = theXTool.Index;

                    AddLM(true);
                    //        theToolEvents.ToolChangePrompt Me, "Click the center of the Gear Contour", False
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[] { this, CommonLocaliz.TcLoadLangString(103), false });
                    ParExist = true;
                    
                }
                Module1.ActDr = theXTool.Application.ActiveDrawing;// ReflectionHelper.GetMember<IMSIGX.Drawing>(ReflectionHelper.GetMember(theXTool, "Application"), "ActiveDrawing");
                Module1.Grs = Module1.ActDr.Graphics;
                //object tempRefParam5 = "TempState";
                //object tempRefParam6 = false;
                Module1.GrsSet = Module1.ActDr.GraphicSets.Add("TempState", false); //# NLS#'
                return true;
            }
            catch
            {
                //    MsgBox Err.Msg
                System.Windows.Forms.MessageBox.Show("Run Failed");
                Finish();
            }


            return false;
        }

		//Fill arrays with information about tools in the package
		//Return the number of tools in the package
		public bool GetToolInfo(out TurboCADToolInfo ToolInfo)//Tool Context,ref object CommandNames, ref object MenuCaptions, ref object StatusPrompts, ref object ToolTips, ref object Enabled, ref object WantsUpdates)
		{
            ToolInfo = new TurboCADToolInfo();

            ToolInfo.CommandName = "Tools\nDotNet Tools\nGear Contour CS";
            ToolInfo.InternalCommand = "CMD_494747D3-9C5D-45A2-9032-7F65A00B3C16";
            ToolInfo.MenuCaption = "&Gear Contour CS";
            ToolInfo.ToolbarName = "Gear Contour CS";
            ToolInfo.ToolTip = "Gear Contour CS";
            ToolInfo.bEnabled = true;
            ToolInfo.bWantsUpdates = true;

            System.Reflection.Assembly thisApp;
            System.IO.Stream file;
            thisApp = System.Reflection.Assembly.GetExecutingAssembly();
            try
            {
                file = thisApp.GetManifestResourceStream("SmallIcon.bmp");
                ToolInfo.ToolbarImage = (Bitmap)Image.FromStream(file);
                System.Windows.Forms.MessageBox.Show("3");
                //file = thisApp.GetManifestResourceStream("GearContour_x64.bmp2.LargeIcon.bmp");
                //ToolInfo.ToolbarImageL = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

                //file = thisApp.GetManifestResourceStream("GearContour_x64.bmp2.SmallIconBW.bmp");
                //ToolInfo.ToolbarImageBW = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

                //file = thisApp.GetManifestResourceStream("GearContour_x64.bmp2.LargeIconBW.bmp");
                //ToolInfo.ToolbarImageLBW = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("ToolBar Failed");
            }

            return true;
		}



		//Returns true if tool is correctly initialized
        
		//Returns true if tool is correctly initialized
        public bool UpdateToolStatus(Tool Context, ref bool Enabled, ref bool Checked)
		{
            IMSIGX.ImsiSpaceModeType SpaceCur = IMSIGX.ImsiSpaceModeType.imsiPaperSpace;
			if (iConnectId > -1)
			{
				//object tempRefParam = "TileMode";
				SpaceCur = (IMSIGX.ImsiSpaceModeType) Convert.ToInt32(gxMe.Application.ActiveDrawing.Properties.get_Item("TileMode").Value);
				if (SpaceCur != Space)
				{
					Finish();
				}
			}

			Enabled = true; //Could do a test here to determine whether to disable the button/menu item
			Checked = iConnectId != -1;

			return true;
		}


        


		void BeforeExit(IMSIGX.Application TheApp, ref bool Cancel)
		{
			Finish();
		}

		
	
		public object Cancel(bool DoItPlease, ref bool CanCancel)
		{
			if (DoItPlease)
			{
				Finish();
			}

			CanCancel = true;
			return null;
		}

		public object UpdateUndo(ref bool AllowsUndo)
		{
			AllowsUndo = false;
			return null;
		}

		//UPGRADE_NOTE: (7001) The following declaration (AddActToolLM) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private object AddActToolLM(int iTool, bool bAdd)
		//{
			//    AddLM bAdd
			//return null;
		//}
		~Contour()
		{

            if (theToolEvents != null)
			{
				theToolEvents = null;
			}
			if (Module1.objApp != null)
			{
				Module1.objApp = null;
			}

		}



		private void AddLM(bool bAdd)
		{
            
            string[] Captions = new string[]{""};
			string[] Prompts = new string[]{""};
			bool[] Enabled = new bool[1];
			bool[] Checked = new bool[1];

			string[] varCaptions = null;
			string[] varPrompts = null;
			bool[] varEnabled = null;
			bool[] varChecked = null;
            try
            {
                if (bAdd)
                {
                    Captions[0] = CommonLocaliz.TcLoadLangString(109); //"Finish GearContour"
                    Prompts[0] = CommonLocaliz.TcLoadLangString(109); //"Finish GearContour"
                    Enabled[0] = true;
                    Checked[0] = false;
                    varCaptions = Captions;
                    varPrompts = Prompts;
                    varEnabled = Enabled;
                    varChecked = Checked;
                    
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangeCommands", new object[]{this, 1, varCaptions, varPrompts, varEnabled, varChecked, true});
                }
                else
                {
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangeCommands", new object[]{this, 0, null, null, null, null, false});
                }
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("AddLM failed");
            }
		}

		public object DoLMCommand(int CmdInd)
		{

            if (CmdInd == 0)
			{
				Finish();
			}

			return null;
		}
		public void Finish()
		{
            try
            {
                Form1.DefInstance.Close();


                IMSIGX.Graphic TempGr = null;
                int Num = 0;
                if (GrMoved != null)
                {
                    TempGr = GrMoved;

                    GrMoved.Visible = false;
                    //				object tempRefParam = Module1.ActDr.ActiveView;
                    GrMoved.Draw(Module1.ActDr.ActiveView);

                    GrMoved = null;

                    Num = Module1.GrsSet.Count;
                    for (int cnt = 0; cnt <= Num - 1; cnt++)
                    {
                        //object tempRefParam2 = cnt;
                        if (TempGr == Module1.GrsSet.get_Item(cnt))
                        {
                            //						cnt = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam2);
                            //						object tempRefParam3 = cnt;
                            Module1.GrsSet.Remove(cnt);
                            //						cnt = ReflectionHelper.GetPrimitiveValue<int>(cnt);
                            TempGr.Delete();
                            TempGr = null;
                            break;
                        }
                        else
                        {
                            //						cnt = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam2);
                        }
                    }
                }

                if (Module1.GrsSet != null)
                {
                    //				object tempRefParam4 = Type.Missing;
                    Module1.GrsSet.Clear(ImsiGraphicFlags.imsiGfGraphicDirty);
                    Module1.GrsSet = null;
                }

                if (ResSet != null)
                {
                    //				object tempRefParam5 = Type.Missing;
                    ResSet.Clear(ImsiGraphicFlags.imsiGfGraphicDirty);
                    ResSet = null;
                }

                if (iConnectId != -1)
                {
                    Module1.objApp.DisconnectEvents(iConnectId);
                    //ReflectionHelper.Invoke(theToolEvents, "ToolChangePrompt", new object[]{this, "", false});
                    AddLM(false);
                    iConnectId = -1;
                }

                GrMoved = null;
                Module1.Grs = null;
                Module1.Vi = null;
                Module1.Vis = null;
                Module1.ActDr = null;
                Module1.Drs = null;
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("FINISH FAILED");
            }
		}





		private void Polyline50(IMSIGX.Graphic GrSel, bool UnDirection, double xBeg0, double yBeg0, IMSIGX.Graphic GrRes)
		{
            int SelCount = GrSel.Graphics.Count;
			double yj1 = 0, zj = 0, xj = 0, yj = 0, xj1 = 0, zj1 = 0;
			IMSIGX.Vertices Vers = null;
			IMSIGX.Vertex Ver0 = null;
			double xEnd = 0, yEnd = 0;

			GrRes.Vertices.UseWorldCS = true;

			//object tempRefParam = 0;
			IMSIGX.Graphic Gr = GrSel.Graphics.get_Item(0);
			string GrType = Gr.Type;
			//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			// GRAPHIC  GRAPHIC  GRAPHIC  GRAPHIC  GRAPHIC  GRAPHIC  GRAPHIC
			if (GrType == "GRAPHIC")
			{
				Vers = Gr.Vertices;
				//object tempRefParam2 = 0;
				xj = Vers.get_Item(0).X;
				//object tempRefParam3 = 0;
				yj = Vers.get_Item(0).Y;
				//object tempRefParam4 = 1;
				xj1 = Vers.get_Item(1).X;
				//object tempRefParam5 = 1;
				yj1 = Vers.get_Item(1).Y;
				if (Math.Abs(xj - xBeg0) < Module1.Eps && Math.Abs(yj - yBeg0) < Module1.Eps)
				{
					//object tempRefParam6 = xj;
					//object tempRefParam7 = yj;
					//object tempRefParam8 = 0;
					//object tempRefParam9 = Type.Missing;
					//object tempRefParam10 = Type.Missing;
					//object tempRefParam11 = Type.Missing;
					//object tempRefParam12 = Type.Missing;
					//object tempRefParam13 = Type.Missing;
					//object tempRefParam14 = Type.Missing;
					//object tempRefParam15 = Type.Missing;
					//object tempRefParam16 = Type.Missing;
                    GrRes.Vertices.Add(xj, yj, 0);//ref tempRefParam6, ref tempRefParam7, ref tempRefParam8, ref tempRefParam9, ref tempRefParam10, ref tempRefParam11, ref tempRefParam12, ref tempRefParam13, ref tempRefParam14, ref tempRefParam15, ref tempRefParam16);
                                                  //yj = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam7);
                                                  //xj = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam6);
                                                  //object tempRefParam17 = xj1;
                                                  //object tempRefParam18 = yj1;
                                                  //object tempRefParam19 = 0;
                                                  //object tempRefParam20 = Type.Missing;
                                                  //object tempRefParam21 = Type.Missing;
                                                  //object tempRefParam22 = Type.Missing;
                                                  //object tempRefParam23 = Type.Missing;
                                                  //object tempRefParam24 = Type.Missing;
                                                  //object tempRefParam25 = Type.Missing;
                                                  //object tempRefParam26 = Type.Missing;
                                                  //object tempRefParam27 = Type.Missing;
                    GrRes.Vertices.Add(xj1, yj1, 0);//ref tempRefParam17, ref tempRefParam18, ref tempRefParam19, ref tempRefParam20, ref tempRefParam21, ref tempRefParam22, ref tempRefParam23, ref tempRefParam24, ref tempRefParam25, ref tempRefParam26, ref tempRefParam27);
					//yj1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam18);
					//xj1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam17);

					xEnd = xj1;
					yEnd = yj1;
				}
				else
				{
                    //object tempRefParam28 = xj1;
                    //object tempRefParam29 = yj1;
                    //object tempRefParam30 = zj1;
                    //object tempRefParam31 = Type.Missing;
                    //object tempRefParam32 = Type.Missing;
                    //object tempRefParam33 = Type.Missing;
                    //object tempRefParam34 = Type.Missing;
                    //object tempRefParam35 = Type.Missing;
                    //object tempRefParam36 = Type.Missing;
                    //object tempRefParam37 = Type.Missing;
                    //object tempRefParam38 = Type.Missing;
                    GrRes.Vertices.Add(xj1, yj1, zj1);//ref tempRefParam28, ref tempRefParam29, ref tempRefParam30, ref tempRefParam31, ref tempRefParam32, ref tempRefParam33, ref tempRefParam34, ref tempRefParam35, ref tempRefParam36, ref tempRefParam37, ref tempRefParam38);
                                                      //zj1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam30);
                                                      //yj1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam29);
                                                      //xj1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam28);
                                                      //object tempRefParam39 = xj;
                                                      //object tempRefParam40 = yj;
                                                      //object tempRefParam41 = zj;
                                                      //object tempRefParam42 = Type.Missing;
                                                      //object tempRefParam43 = Type.Missing;
                                                      //object tempRefParam44 = Type.Missing;
                                                      //object tempRefParam45 = Type.Missing;
                                                      //object tempRefParam46 = Type.Missing;
                                                      //object tempRefParam47 = Type.Missing;
                                                      //object tempRefParam48 = Type.Missing;
                                                      //object tempRefParam49 = Type.Missing;
                    GrRes.Vertices.Add(xj, yj, zj);//ref tempRefParam39, ref tempRefParam40, ref tempRefParam41, ref tempRefParam42, ref tempRefParam43, ref tempRefParam44, ref tempRefParam45, ref tempRefParam46, ref tempRefParam47, ref tempRefParam48, ref tempRefParam49);
					//zj = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam41);
					//yj = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam40);
					//xj = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam39);

					xEnd = xj;
					yEnd = yj;
				}
			}
			//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			// ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC
			// Circle Arc

			double xDir = 0, xMid = 0, y2 = 0, y1 = 0, yc = 0, xc = 0, x1 = 0, x2 = 0, R = 0, yMid = 0, yDir = 0;
			int Rot = 0;
			if (GrType == "ARC")
			{
				Vers = Gr.Vertices;
				//object tempRefParam50 = 1;
				x1 = Vers.get_Item(1).X;
				//object tempRefParam51 = 1;
				y1 = Vers.get_Item(1).Y;
				//object tempRefParam52 = 2;
				x2 = Vers.get_Item(2).X;
				//object tempRefParam53 = 2;
				y2 = Vers.get_Item(2).Y;
				//object tempRefParam54 = 0;
				xc = Vers.get_Item(0).X;
				//object tempRefParam55 = 0;
				yc = Vers.get_Item(0).Y;
				//object tempRefParam56 = 3;
				xDir = Vers.get_Item(0).X;
				//object tempRefParam57 = 3;
				yDir = Vers.get_Item(3).Y;

				R = Math.Sqrt((x1 - xc) * (x1 - xc) + (y1 - yc) * (y1 - yc));
				Rot = ArcRotation(x1, y1, xDir, yDir, xc, yc, R);
				ArcMiddlePoint(xc, yc, x1, y1, x2, y2, R, Rot, ref xMid, ref yMid);

				if (Math.Abs(x1 - xBeg0) < Module1.Eps && Math.Abs(y1 - yBeg0) < Module1.Eps)
				{
                    //object tempRefParam58 = x1;
                    //object tempRefParam59 = y1;
                    //object tempRefParam60 = 0;
                    //object tempRefParam61 = Type.Missing;
                    //object tempRefParam62 = Type.Missing;
                    //object tempRefParam63 = Type.Missing;
                    //object tempRefParam64 = Type.Missing;
                    //object tempRefParam65 = Type.Missing;
                    //object tempRefParam66 = Type.Missing;
                    //object tempRefParam67 = Type.Missing;
                    //object tempRefParam68 = Type.Missing;
                    GrRes.Vertices.Add(x1, y1, 0);//ref tempRefParam58, ref tempRefParam59, ref tempRefParam60, ref tempRefParam61, ref tempRefParam62, ref tempRefParam63, ref tempRefParam64, ref tempRefParam65, ref tempRefParam66, ref tempRefParam67, ref tempRefParam68);
                                                  //y1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam59);
                                                  //x1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam58);

                    //object tempRefParam69 = xc;
                    //object tempRefParam70 = yc;
                    //object tempRefParam71 = 0;
                    //object tempRefParam72 = Type.Missing;
                    //object tempRefParam73 = Type.Missing;
                    //object tempRefParam74 = Type.Missing;
                    //object tempRefParam75 = Type.Missing;
                    //object tempRefParam76 = Type.Missing;
                    //object tempRefParam77 = Type.Missing;
                    //object tempRefParam78 = Type.Missing;
                    //object tempRefParam79 = Type.Missing;
                    Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(xc, yc, 0);//ref tempRefParam69, ref tempRefParam70, ref tempRefParam71, ref tempRefParam72, ref tempRefParam73, ref tempRefParam74, ref tempRefParam75, ref tempRefParam76, ref tempRefParam77, ref tempRefParam78, ref tempRefParam79);
					//yc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam70);
					//xc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam69);
					Ver0.Bulge = true;

                    //object tempRefParam80 = xMid;
                    //object tempRefParam81 = yMid;
                    //object tempRefParam82 = 0;
                    //object tempRefParam83 = Type.Missing;
                    //object tempRefParam84 = Type.Missing;
                    //object tempRefParam85 = Type.Missing;
                    //object tempRefParam86 = Type.Missing;
                    //object tempRefParam87 = Type.Missing;
                    //object tempRefParam88 = Type.Missing;
                    //object tempRefParam89 = Type.Missing;
                    //object tempRefParam90 = Type.Missing;
                    Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(xMid, yMid, 0);//ref tempRefParam80, ref tempRefParam81, ref tempRefParam82, ref tempRefParam83, ref tempRefParam84, ref tempRefParam85, ref tempRefParam86, ref tempRefParam87, ref tempRefParam88, ref tempRefParam89, ref tempRefParam90);
					//yMid = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam81);
					//xMid = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam80);
					Ver0.Bulge = true;

                    //object tempRefParam91 = x2;
                    //object tempRefParam92 = y2;
                    //object tempRefParam93 = 0;
                    //object tempRefParam94 = Type.Missing;
                    //object tempRefParam95 = Type.Missing;
                    //object tempRefParam96 = Type.Missing;
                    //object tempRefParam97 = Type.Missing;
                    //object tempRefParam98 = Type.Missing;
                    //object tempRefParam99 = Type.Missing;
                    //object tempRefParam100 = Type.Missing;
                    //object tempRefParam101 = Type.Missing;
                    Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(x2, y2, 0);//ref tempRefParam91, ref tempRefParam92, ref tempRefParam93, ref tempRefParam94, ref tempRefParam95, ref tempRefParam96, ref tempRefParam97, ref tempRefParam98, ref tempRefParam99, ref tempRefParam100, ref tempRefParam101);
					//y2 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam92);
					//x2 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam91);

					xEnd = x2;
					yEnd = y2;
				}
				else
				{
                    //object tempRefParam102 = x2;
                    //object tempRefParam103 = y2;
                    //object tempRefParam104 = 0;
                    //object tempRefParam105 = Type.Missing;
                    //object tempRefParam106 = Type.Missing;
                    //object tempRefParam107 = Type.Missing;
                    //object tempRefParam108 = Type.Missing;
                    //object tempRefParam109 = Type.Missing;
                    //object tempRefParam110 = Type.Missing;
                    //object tempRefParam111 = Type.Missing;
                    //object tempRefParam112 = Type.Missing;
                    GrRes.Vertices.Add(x2, y2, 0);//ref tempRefParam102, ref tempRefParam103, ref tempRefParam104, ref tempRefParam105, ref tempRefParam106, ref tempRefParam107, ref tempRefParam108, ref tempRefParam109, ref tempRefParam110, ref tempRefParam111, ref tempRefParam112);
                                                  //y2 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam103);
                                                  //x2 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam102);

                    //object tempRefParam113 = xc;
                    //object tempRefParam114 = yc;
                    //object tempRefParam115 = 0;
                    //object tempRefParam116 = Type.Missing;
                    //object tempRefParam117 = Type.Missing;
                    //object tempRefParam118 = Type.Missing;
                    //object tempRefParam119 = Type.Missing;
                    //object tempRefParam120 = Type.Missing;
                    //object tempRefParam121 = Type.Missing;
                    //object tempRefParam122 = Type.Missing;
                    //object tempRefParam123 = Type.Missing;
                    Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(xc, yc, 0);//ref tempRefParam113, ref tempRefParam114, ref tempRefParam115, ref tempRefParam116, ref tempRefParam117, ref tempRefParam118, ref tempRefParam119, ref tempRefParam120, ref tempRefParam121, ref tempRefParam122, ref tempRefParam123);
					//yc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam114);
					//xc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam113);
					Ver0.Bulge = true;

                    //object tempRefParam124 = xMid;
                    //object tempRefParam125 = yMid;
                    //object tempRefParam126 = 0;
                    //object tempRefParam127 = Type.Missing;
                    //object tempRefParam128 = Type.Missing;
                    //object tempRefParam129 = Type.Missing;
                    //object tempRefParam130 = Type.Missing;
                    //object tempRefParam131 = Type.Missing;
                    //object tempRefParam132 = Type.Missing;
                    //object tempRefParam133 = Type.Missing;
                    //object tempRefParam134 = Type.Missing;
                    Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(xMid, yMid, 0);//ref tempRefParam124, ref tempRefParam125, ref tempRefParam126, ref tempRefParam127, ref tempRefParam128, ref tempRefParam129, ref tempRefParam130, ref tempRefParam131, ref tempRefParam132, ref tempRefParam133, ref tempRefParam134);
					//yMid = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam125);
					//xMid = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam124);
					Ver0.Bulge = true;

                    //object tempRefParam135 = x1;
                    //object tempRefParam136 = y1;
                    //object tempRefParam137 = 0;
                    //object tempRefParam138 = Type.Missing;
                    //object tempRefParam139 = Type.Missing;
                    //object tempRefParam140 = Type.Missing;
                    //object tempRefParam141 = Type.Missing;
                    //object tempRefParam142 = Type.Missing;
                    //object tempRefParam143 = Type.Missing;
                    //object tempRefParam144 = Type.Missing;
                    //object tempRefParam145 = Type.Missing;
                    Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(x1, y1, 0);//ref tempRefParam135, ref tempRefParam136, ref tempRefParam137, ref tempRefParam138, ref tempRefParam139, ref tempRefParam140, ref tempRefParam141, ref tempRefParam142, ref tempRefParam143, ref tempRefParam144, ref tempRefParam145);
					//y1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam136);
					//x1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam135);

					xEnd = x1;
					yEnd = y1;
				}
			}
			//#####################################################################
			//#####################################################################
			//#####################################################################

			for (int i = 1; i <= SelCount - 1; i++)
			{
				//object tempRefParam146 = i;
				Gr = (IMSIGX.Graphic) GrSel.Graphics.get_Item(i);
				//i = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam146);
				//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
				// GRAPHIC GRAPHIC GRAPHIC GRAPHIC GRAPHIC GRAPHIC GRAPHIC GRAPHIC
				if (Gr.Type == "GRAPHIC")
				{
					Vers = Gr.Vertices;
					//object tempRefParam147 = 0;
					xj = Vers.get_Item(0).X;
					//object tempRefParam148 = 0;
					yj = Vers.get_Item(0).Y;
					//object tempRefParam149 = 1;
					xj1 = Vers.get_Item(1).X;
					//object tempRefParam150 = 1;
					yj1 = Vers.get_Item(1).Y;
					if (Math.Abs(xj - xEnd) < Module1.Eps && Math.Abs(yj - yEnd) < Module1.Eps)
					{
                        //object tempRefParam151 = xj1;
                        //object tempRefParam152 = yj1;
                        //object tempRefParam153 = 0;
                        //object tempRefParam154 = Type.Missing;
                        //object tempRefParam155 = Type.Missing;
                        //object tempRefParam156 = Type.Missing;
                        //object tempRefParam157 = Type.Missing;
                        //object tempRefParam158 = Type.Missing;
                        //object tempRefParam159 = Type.Missing;
                        //object tempRefParam160 = Type.Missing;
                        //object tempRefParam161 = Type.Missing;
                        GrRes.Vertices.Add(xj1, yj1, 0);//ref tempRefParam151, ref tempRefParam152, ref tempRefParam153, ref tempRefParam154, ref tempRefParam155, ref tempRefParam156, ref tempRefParam157, ref tempRefParam158, ref tempRefParam159, ref tempRefParam160, ref tempRefParam161);
						//yj1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam152);
						//xj1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam151);
						xEnd = xj1;
						yEnd = yj1;
					}
					else
					{
                        //object tempRefParam162 = xj;
                        //object tempRefParam163 = yj;
                        //object tempRefParam164 = 0;
                        //object tempRefParam165 = Type.Missing;
                        //object tempRefParam166 = Type.Missing;
                        //object tempRefParam167 = Type.Missing;
                        //object tempRefParam168 = Type.Missing;
                        //object tempRefParam169 = Type.Missing;
                        //object tempRefParam170 = Type.Missing;
                        //object tempRefParam171 = Type.Missing;
                        //object tempRefParam172 = Type.Missing;
                        GrRes.Vertices.Add(xj, yj, 0);//ref tempRefParam162, ref tempRefParam163, ref tempRefParam164, ref tempRefParam165, ref tempRefParam166, ref tempRefParam167, ref tempRefParam168, ref tempRefParam169, ref tempRefParam170, ref tempRefParam171, ref tempRefParam172);
						//yj = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam163);
						//xj = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam162);
						xEnd = xj;
						yEnd = yj;
					}
				}
				//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
				// ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC  ARC
				if (Gr.Type == "ARC")
				{

					Vers = Gr.Vertices;
					//object tempRefParam173 = 1;
					x1 = Vers.get_Item(1).X;
					//object tempRefParam174 = 1;
					y1 = Vers.get_Item(1).Y;
					//object tempRefParam175 = 2;
					x2 = Vers.get_Item(2).X;
					//object tempRefParam176 = 2;
					y2 = Vers.get_Item(2).Y;
					//object tempRefParam177 = 0;
					xc = Vers.get_Item(0).X;
					//object tempRefParam178 = 0;
					yc = Vers.get_Item(0).Y;
					//object tempRefParam179 = 3;
					xDir = Vers.get_Item(3).X;
					//object tempRefParam180 = 3;
					yDir = Vers.get_Item(3).Y;

					R = Math.Sqrt((x1 - xc) * (x1 - xc) + (y1 - yc) * (y1 - yc));
					Rot = ArcRotation(x1, y1, xDir, yDir, xc, yc, R);

					ArcMiddlePoint(xc, yc, x1, y1, x2, y2, R, Rot, ref xMid, ref yMid);

					if (Math.Abs(x1 - xEnd) < Module1.Eps && Math.Abs(y1 - yEnd) < Module1.Eps)
					{

                        //object tempRefParam181 = xc;
                        //object tempRefParam182 = yc;
                        //object tempRefParam183 = 0;
                        //object tempRefParam184 = Type.Missing;
                        //object tempRefParam185 = Type.Missing;
                        //object tempRefParam186 = Type.Missing;
                        //object tempRefParam187 = Type.Missing;
                        //object tempRefParam188 = Type.Missing;
                        //object tempRefParam189 = Type.Missing;
                        //object tempRefParam190 = Type.Missing;
                        //object tempRefParam191 = Type.Missing;
                        Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(xc, yc, 0);//ref tempRefParam181, ref tempRefParam182, ref tempRefParam183, ref tempRefParam184, ref tempRefParam185, ref tempRefParam186, ref tempRefParam187, ref tempRefParam188, ref tempRefParam189, ref tempRefParam190, ref tempRefParam191);
						//yc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam182);
						//xc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam181);
						Ver0.Bulge = true;

                        //object tempRefParam192 = xMid;
                        //object tempRefParam193 = yMid;
                        //object tempRefParam194 = 0;
                        //object tempRefParam195 = Type.Missing;
                        //object tempRefParam196 = Type.Missing;
                        //object tempRefParam197 = Type.Missing;
                        //object tempRefParam198 = Type.Missing;
                        //object tempRefParam199 = Type.Missing;
                        //object tempRefParam200 = Type.Missing;
                        //object tempRefParam201 = Type.Missing;
                        //object tempRefParam202 = Type.Missing;
                        Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(xMid, yMid, 0);//ref tempRefParam192, ref tempRefParam193, ref tempRefParam194, ref tempRefParam195, ref tempRefParam196, ref tempRefParam197, ref tempRefParam198, ref tempRefParam199, ref tempRefParam200, ref tempRefParam201, ref tempRefParam202);
						//yMid = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam193);
						//xMid = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam192);
						Ver0.Bulge = true;

                        //object tempRefParam203 = x2;
                        //object tempRefParam204 = y2;
                        //object tempRefParam205 = 0;
                        //object tempRefParam206 = Type.Missing;
                        //object tempRefParam207 = Type.Missing;
                        //object tempRefParam208 = Type.Missing;
                        //object tempRefParam209 = Type.Missing;
                        //object tempRefParam210 = Type.Missing;
                        //object tempRefParam211 = Type.Missing;
                        //object tempRefParam212 = Type.Missing;
                        //object tempRefParam213 = Type.Missing;
                        Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(x2, y2, 0);//ref tempRefParam203, ref tempRefParam204, ref tempRefParam205, ref tempRefParam206, ref tempRefParam207, ref tempRefParam208, ref tempRefParam209, ref tempRefParam210, ref tempRefParam211, ref tempRefParam212, ref tempRefParam213);
						//y2 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam204);
						//x2 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam203);

						xEnd = x2;
						yEnd = y2;
					}
					else
					{

                        //object tempRefParam214 = xc;
                        //object tempRefParam215 = yc;
                        //object tempRefParam216 = 0;
                        //object tempRefParam217 = Type.Missing;
                        //object tempRefParam218 = Type.Missing;
                        //object tempRefParam219 = Type.Missing;
                        //object tempRefParam220 = Type.Missing;
                        //object tempRefParam221 = Type.Missing;
                        //object tempRefParam222 = Type.Missing;
                        //object tempRefParam223 = Type.Missing;
                        //object tempRefParam224 = Type.Missing;
                        Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(xc, yc, 0);//ref tempRefParam214, ref tempRefParam215, ref tempRefParam216, ref tempRefParam217, ref tempRefParam218, ref tempRefParam219, ref tempRefParam220, ref tempRefParam221, ref tempRefParam222, ref tempRefParam223, ref tempRefParam224);
						//yc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam215);
						//xc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam214);
						Ver0.Bulge = true;

                        //object tempRefParam225 = xMid;
                        //object tempRefParam226 = yMid;
                        //object tempRefParam227 = 0;
                        //object tempRefParam228 = Type.Missing;
                        //object tempRefParam229 = Type.Missing;
                        //object tempRefParam230 = Type.Missing;
                        //object tempRefParam231 = Type.Missing;
                        //object tempRefParam232 = Type.Missing;
                        //object tempRefParam233 = Type.Missing;
                        //object tempRefParam234 = Type.Missing;
                        //object tempRefParam235 = Type.Missing;
                        Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(xMid, yMid, 0);//ref tempRefParam225, ref tempRefParam226, ref tempRefParam227, ref tempRefParam228, ref tempRefParam229, ref tempRefParam230, ref tempRefParam231, ref tempRefParam232, ref tempRefParam233, ref tempRefParam234, ref tempRefParam235);
                        //yMid = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam226);
                        //xMid = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam225);
                        Ver0.Bulge = true;

                        //object tempRefParam236 = x1;
                        //object tempRefParam237 = y1;
                        //object tempRefParam238 = 0;
                        //object tempRefParam239 = Type.Missing;
                        //object tempRefParam240 = Type.Missing;
                        //object tempRefParam241 = Type.Missing;
                        //object tempRefParam242 = Type.Missing;
                        //object tempRefParam243 = Type.Missing;
                        //object tempRefParam244 = Type.Missing;
                        //object tempRefParam245 = Type.Missing;
                        //object tempRefParam246 = Type.Missing;
                        Ver0 = (IMSIGX.Vertex)GrRes.Vertices.Add(x1, y1, 0);//ref tempRefParam236, ref tempRefParam237, ref tempRefParam238, ref tempRefParam239, ref tempRefParam240, ref tempRefParam241, ref tempRefParam242, ref tempRefParam243, ref tempRefParam244, ref tempRefParam245, ref tempRefParam246);
						//y1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam237);
						//x1 = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam236);

						xEnd = x1;
						yEnd = y1;
					}
				}

				//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


			}
			//object tempRefParam247 = 0;
			double xB = GrRes.Vertices.get_Item(0).X;
			//object tempRefParam248 = 0;
			double yB = GrRes.Vertices.get_Item(0).Y;
			//object tempRefParam249 = GrRes.Vertices.Count - 1;
			double xE = GrRes.Vertices.get_Item(0).X;
			//object tempRefParam250 = GrRes.Vertices.Count - 1;
			double yE = GrRes.Vertices.get_Item(0).Y;
			if (Math.Abs(xB - xE) < 1 * Module1.Eps && Math.Abs(yB - yE) < 1 * Module1.Eps)
			{
				//object tempRefParam251 = GrRes.Vertices.Count - 1;
				GrRes.Vertices.get_Item(GrRes.Vertices.Count - 1).X = xB;
				//object tempRefParam252 = GrRes.Vertices.Count - 1;
				GrRes.Vertices.get_Item(GrRes.Vertices.Count - 1).Y = yB;
				GrRes.Closed = true;
			}

			GrRes.Update();
			//'''''''
			//object tempRefParam253 = Type.Missing;
			//object tempRefParam254 = Type.Missing;
            Module1.ActDr.Graphics.AddGraphic(GrRes,1,0);//, ref tempRefParam253, ref tempRefParam254);
			GrRes.Transform((IMSIGX.Matrix) Module1.ActDr.UCS);
			GrRes.UCS = Module1.ActDr.UCS;
			//'''''''

			IMSIGX.UndoRecord UndoRec = Module1.ActDr.AddUndoRecord(CommonLocaliz.TcLoadLangString(101));
			UndoRec.AddGraphic((IMSIGX.Graphic) GrRes);
			UndoRec.Close();
			UndoRec = null;
			GrRes.Update();
			//object tempRefParam255 = Type.Missing;
			GrRes.Draw();


			//'''''''    GrSel.Deleted = True




		}
		//This macros defines  index of all Polyline's arcs Middle points
		//UPGRADE_NOTE: (7001) The following declaration (PolyLineArcCenterPoints) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void PolyLineArcCenterPoints(IMSIGX.Graphic Gr, ref Module1.ArcData PolyArc)
		//{
			//
			//
			//int i = 0;
			//double xi3 = 0, xi2 = 0, yi2 = 0, yi3 = 0;
			//
			//double xj1 = 0, xj0 = 0, yj0 = 0, yj1 = 0;
			//IMSIGX.Vertices Vers = Gr.Vertices;
			//Vers.UseWorldCS = true;
			//int nV = Vers.Count;
			//PolyArc.NumCen = 0;
			//object tempRefParam = 0;
			//IMSIGX.Graphic GrCosm = (IMSIGX.Graphic) Gr.Graphics.get_Item(ref tempRefParam);
			//IMSIGX.Vertices VersCosm = GrCosm.Vertices;
			//int nVCosm = VersCosm.Count;
			//Module1.SelInfo VerBase = new Module1.SelInfo();
			//VerBase.nV = nV;
			//VerBase.xV = new double[nV + 1];
			//VerBase.yV = new double[nV + 1];
			//for (i = 0; i <= nV - 1; i++)
			//{
				//object tempRefParam2 = i;
				//VerBase.xV[i] = Vers.get_Item(ref tempRefParam2).X;
				//i = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam2);
				//object tempRefParam3 = i;
				//VerBase.yV[i] = Vers.get_Item(ref tempRefParam3).Y;
				//i = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam3);
			//}
			//
			//
			//Module1.SelInfo VerCosm = new Module1.SelInfo();
			//VerCosm.nV = nVCosm;
			//VerCosm.xV = new double[nVCosm + 1];
			//VerCosm.yV = new double[nVCosm + 1];
			//for (i = 0; i <= nVCosm - 1; i++)
			//{
				//object tempRefParam4 = i;
				//VerCosm.xV[i] = VersCosm.get_Item(ref tempRefParam4).X;
				//i = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam4);
				//object tempRefParam5 = i;
				//VerCosm.yV[i] = VersCosm.get_Item(ref tempRefParam5).Y;
				//i = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam5);
			//}
			//int kMid = 0;
			//double xMid = 0, yMid = 0;
			//
			//i = 0;
//LBEG://
			//double xi0 = VerBase.xV[i];
			//double yi0 = VerBase.yV[i];
			//double xi1 = VerBase.xV[i + 1];
			//double yi1 = VerBase.yV[i + 1];
			//for (int j = 0; j <= nVCosm - 2; j++)
			//{
				//xj0 = VerCosm.xV[j];
				//yj0 = VerCosm.yV[j];
				//xj1 = VerCosm.xV[j + 1];
				//yj1 = VerCosm.yV[j + 1];
				//if (Math.Abs(xi0 - xj0) < Module1.Eps && Math.Abs(yi0 - yj0) < Module1.Eps)
				//{
					//if (Math.Abs(xi1 - xj1) < Module1.Eps && Math.Abs(yi1 - yj1) < Module1.Eps)
					//{
						// Line Segment
						//i++;
						//if (i == VerBase.nV - 1)
						//{
							//return;
						//}
						//else
						//{
							//goto LBEG;
						//}
					//}
					//else
					//{
						// Arc Segment
						//xi2 = VerBase.xV[i + 2];
						//yi2 = VerBase.yV[i + 2];
						//xi3 = VerBase.xV[i + 3];
						//yi3 = VerBase.yV[i + 3];
						//PolyArc.ArcCen[PolyArc.NumCen] = i + 1;
						//                   NumEnds = NumEnds + 1
						//i += 3;
						//for (int k = 0; k <= nVCosm - 1; k++)
						//{
							//xj0 = VerCosm.xV[k];
							//yj0 = VerCosm.yV[k];
							//if (Math.Abs(xi3 - xj0) < Module1.Eps && Math.Abs(yi3 - yj0) < Module1.Eps)
							//{
								//kMid = (k + j) / 2;
								//xMid = VerCosm.xV[kMid];
								//yMid = VerCosm.yV[kMid];
							//}
						//}
						//PolyArc.xMid[PolyArc.NumCen] = xMid;
						//PolyArc.yMid[PolyArc.NumCen] = yMid;
						//PolyArc.NumCen++;
						//
						//if (i == VerBase.nV - 1)
						//{
							//return;
						//}
						//else
						//{
							//goto LBEG;
						//}
					//}
				//}
			//}
			//
			//
		//}


		public void TeethInvol()
		{
            IMSIGX.Graphic Gr = null;

			double PressureAngle = Double.Parse(Form1.DefInstance.PressureAngle.Text) / 180 * Module1.Pi; // Pressure Angle
			double DiaPitch = Double.Parse(Form1.DefInstance.DiamPitch.Text); //Diametral Pitch
			int NumTeeth = 20; //Number of Teeth on Gear
			NumTeeth = Convert.ToInt32(Double.Parse(Form1.DefInstance.TeethCount.Text));

			bool BulgePoly = false;
			IMSIGX.Graphic GrGroup = null;
			if (Form1.DefInstance.Option1.Checked)
			{
				BulgePoly = false;
			}
			else
			{
				BulgePoly = true;
                //'''''''        Set GrGroup = Grs.Add(11)
                //object tempRefParam = 11;
                //object tempRefParam2 = Type.Missing;
                //object tempRefParam3 = Type.Missing;
                //object tempRefParam4 = Type.Missing;
                //object tempRefParam5 = Type.Missing;
                //object tempRefParam6 = Type.Missing;
                GrGroup = Module1.GrsSet.Add(11, "REGENMETHOD_POLYLINE", false,-4,1,0);//ref tempRefParam, ref tempRefParam2, ref tempRefParam3, ref tempRefParam4, ref tempRefParam5, ref tempRefParam6);
			}

			// Rb - Base Circle Radius
			// R - Pitch Radius
			// R0 - Outside Radius
			// RR - Root Radius
			// b - Dedendum

			double R = NumTeeth / (2 * DiaPitch);
			//    R = 3
			double Rb = R * Math.Cos(PressureAngle);

			double b = 1.25d / DiaPitch;
			double R0 = (NumTeeth + 2) / (2 * DiaPitch);
			double RR = R - b;
			double xc = xcWorld;
			double yc = ycWorld;
			//xc = 0
			//yc = 0

			//    Set Gr = Grs.AddCircleCenterAndPoint(xc, yc, 0, xc + R, yc, 0) 'Pitch Circle
			//    Gr.Properties("PenColor") = RGB(255, 0, 0)
			//    Gr.Draw
			//    Set Gr = Grs.AddCircleCenterAndPoint(xc, yc, 0, xc + R0, yc, 0) 'Outside Circle
			//    Gr.Properties("PenColor") = RGB(0, 255, 0)
			//    Gr.Draw
			//    Set Gr = Grs.AddCircleCenterAndPoint(xc, yc, 0, xc + Rb, yc, 0) 'Base Circle
			//    Gr.Properties("PenColor") = RGB(0, 0, 255)
			//    Gr.Draw
			//    Set Gr = Grs.AddCircleCenterAndPoint(xc, yc, 0, xc + RR, yc, 0) 'Root Circle
			//    Gr.Properties("PenColor") = RGB(0, 0, 0)
			//    Gr.Draw


			// Calculate points of involute

			double fiPrev = 0, fi = 0, fiNext = 0;
			double fi0 = 0;
			double dfi = Module1.Pi / 10000;
			double ri = 0, xi = 0, yi = 0, rNext = 0;
			double rPrev = Rb;

			if (RR > Rb)
			{ // Define fi0
				for (fi = 0; (dfi < 0) ? fi >= Module1.Pi / 2 : fi <= Module1.Pi / 2; fi += dfi)
				{ // Define fiE
					xi = xc + Rb * (Math.Cos(fi) + fi * Math.Sin(fi));
					yi = yc + Rb * (Math.Sin(fi) - fi * Math.Cos(fi));
					ri = Math.Sqrt((xi - xc) * (xi - xc) + (yi - yc) * (yi - yc));
					if (ri > RR)
					{
						rNext = ri;
						fiNext = fi;
						fiPrev = fiNext - dfi;
						break;
					}
					rPrev = ri;
				}
				fi0 = fiPrev + dfi * (RR - rPrev) / (rNext - rPrev);
			}

			for (fi = fi0; (dfi < 0) ? fi >= Module1.Pi / 2 : fi <= Module1.Pi / 2; fi += dfi)
			{ // Define fiE
				xi = xc + Rb * (Math.Cos(fi) + fi * Math.Sin(fi));
				yi = yc + Rb * (Math.Sin(fi) - fi * Math.Cos(fi));
				ri = Math.Sqrt((xi - xc) * (xi - xc) + (yi - yc) * (yi - yc));
				if (ri > R0)
				{
					rNext = ri;
					fiNext = fi;
					fiPrev = fiNext - dfi;
					break;
				}
				rPrev = ri;
			}
			double fiE = fiPrev + dfi * (R0 - rPrev) / (rNext - rPrev);


			int nDiv = 10;
			double[] xT = null;
			xT = new double[nDiv + 1];
			double[] yT = null;
			yT = new double[nDiv + 1];

			dfi = (fiE - fi0) / nDiv;
			for (int i = 0; i <= nDiv; i++)
			{
				fi = fi0 + dfi * i;
				xi = xc + Rb * (Math.Cos(fi) + fi * Math.Sin(fi));
				yi = yc + Rb * (Math.Sin(fi) - fi * Math.Cos(fi));
				xT[i] = xi;
				yT[i] = yi;
			}

			//############################################################################
			//############################################################################
			// Approximate Involute as Arc
			double xInt2 = 0, xInt1 = 0, sina = 0, yE = 0, yB = 0, xB = 0, xE = 0, L = 0, cosa = 0, yInt1 = 0, yInt2 = 0;
			double L1 = 0, L2 = 0;
			int iMax = 0;
			double yLocInv = 0, yLocMax = 0;
			IMSIGX.Graphic GrInv0 = null;
			if (BulgePoly)
			{
				yLocMax = -100000000;
				xB = xT[0];
				yB = yT[0];
				xE = xT[nDiv];
				yE = yT[nDiv];

				L = Math.Sqrt(Math.Pow(xE - xB, 2) + Math.Pow(yE - yB, 2));
				sina = (yE - yB) / L;
				cosa = (xE - xB) / L;
				for (int i = 1; i <= nDiv - 1; i++)
				{
					yLocInv = (yT[i] - yB) * cosa - (xT[i] - xB) * sina;
					if (Math.Abs(yLocInv) > yLocMax)
					{
						yLocMax = Math.Abs(yLocInv);
						iMax = i;
					}
				}
				//'''''''        Set GrInv0 = Grs.AddArcTriplePoint(xT(0), yT(0), 0, xT(iMax), yT(iMax), 0, xT(nDiv), yT(nDiv), 0)
				GrInv0 = (IMSIGX.Graphic) Module1.GrsSet.AddArcTriplePoint(xT[0], yT[0], 0, xT[iMax], yT[iMax], 0, xT[nDiv], yT[nDiv], 0);
				//object tempRefParam7 = "PenColor";
				//object tempRefParam8 = Color.FromArgb(255, 0, 0);
				GrInv0.Properties.get_Item("PenColor").Value = Color.FromArgb(255, 0, 0);
			}
			//############################################################################
			//############################################################################

			// Add line segment from involute to Root Circle
			IMSIGX.Graphic GrLine0 = null;
			if (RR < Rb)
			{
				xB = xT[0];
				yB = yT[0];
				xE = xT[1];
				yE = yT[1];
				L = Math.Sqrt((xE - xB) * (xE - xB) + (yE - yB) * (yE - yB));
				sina = (yE - yB) / L;
				cosa = (xE - xB) / L;
				LineCircIntersection(xB, yB, sina, cosa, xc, yc, RR, ref xInt1, ref yInt1, ref xInt2, ref yInt2);
				L1 = Math.Sqrt((xInt1 - xB) * (xInt1 - xB) + (yInt1 - yB) * (yInt1 - yB));
				L2 = Math.Sqrt((xInt2 - xB) * (xInt2 - xB) + (yInt2 - yB) * (yInt2 - yB));
				if (L1 > L2)
				{
					xB = xInt2;
					yB = yInt2;
				}
				else
				{
					xB = xInt1;
					yB = yInt1;
				}
				xT = ArraysHelper.RedimPreserve(xT, new int[]{nDiv + 2});
				yT = ArraysHelper.RedimPreserve(yT, new int[]{nDiv + 2});
				for (int i = nDiv; i >= 0; i--)
				{
					xT[i + 1] = xT[i];
					yT[i + 1] = yT[i];
				}
				xT[0] = xB;
				yT[0] = yB;
				nDiv++;
				// #############################################################################
				// #############################################################################
				if (BulgePoly)
				{
					//Add First Line
					//'''''''            Set GrLine0 = Grs.AddLineSingle(xT(0), yT(0), 0, xT(1), yT(1), 0)
					GrLine0 = (IMSIGX.Graphic) Module1.GrsSet.AddLineSingle(xT[0], yT[0], 0, xT[1], yT[1], 0);
					//object tempRefParam9 = "PenColor";
					//object tempRefParam10 = Color.FromArgb(255, 0, 0);
					GrLine0.Properties.get_Item("PenColor").Value = Color.FromArgb(255, 0, 0);
				}
				// #############################################################################
				// #############################################################################

			}


			// Add Bottom Arc Points
			double CircPitch = 2 * Module1.Pi * R / NumTeeth; // Circular Pitch
			double AngPitch = 2 * Module1.Pi / NumTeeth;
			sina = (yT[0] - yc) / RR;
			cosa = (xT[0] - xc) / RR;
			double Ang0 = Angle(sina, cosa);
			double xiNext = 0, xiPrev = 0, yiPrev = 0, yiNext = 0;
			dfi = Module1.Pi / 10000;
			for (fi = fi0; (dfi < 0) ? fi >= Module1.Pi / 2 : fi <= Module1.Pi / 2; fi += dfi)
			{ // Define fiE
				xi = xc + Rb * (Math.Cos(fi) + fi * Math.Sin(fi));
				yi = yc + Rb * (Math.Sin(fi) - fi * Math.Cos(fi));
				ri = Math.Sqrt((xi - xc) * (xi - xc) + (yi - yc) * (yi - yc));
				if (ri > R)
				{
					rNext = ri;
					xiNext = xi;
					yiNext = yi;
					break;
				}
				rPrev = ri;
				xiPrev = xi;
				yiPrev = yi;
			}
			double xMid = xiPrev + (xiNext - xiPrev) * (R - rPrev) / (rNext - rPrev);
			double yMid = yiPrev + (yiNext - yiPrev) * (R - rPrev) / (rNext - rPrev);
			sina = (yMid - yc) / R;
			cosa = (xMid - xc) / R;
			double AngMid = Angle(sina, cosa);
			if (Ang0 > AngMid)
			{
				Ang0 -= 2 * Module1.Pi;
			}
			double Ang1 = AngMid - AngPitch / 4;
			int ArcDiv = 2;
			dfi = (Ang0 - Ang1) / ArcDiv;
			for (int i = 1; i <= ArcDiv; i++)
			{
				fi = Ang0 - i * dfi;
				xi = xc + RR * Math.Cos(fi);
				yi = yc + RR * Math.Sin(fi);
				xT = ArraysHelper.RedimPreserve(xT, new int[]{nDiv + 2});
				yT = ArraysHelper.RedimPreserve(yT, new int[]{nDiv + 2});
				for (int j = nDiv; j >= 0; j--)
				{
					xT[j + 1] = xT[j];
					yT[j + 1] = yT[j];
				}
				xT[0] = xi;
				yT[0] = yi;
				nDiv++;
			}

			//##########################################################################
			//##########################################################################
			IMSIGX.Graphic GrArcBot = null;
			IMSIGX.Graphic GrLine1 = null;
			IMSIGX.Graphic GrInv1 = null;
			if (BulgePoly)
			{
				//Add Bottom arc
				xB = xc + RR * Math.Cos(Ang0);
				yB = yc + RR * Math.Sin(Ang0);
				xMid = xc + RR * Math.Cos(Ang1);
				yMid = yc + RR * Math.Sin(Ang1);
				xE = xc + RR * Math.Cos(Ang1 - Math.Abs(Ang0 - Ang1));
				yE = yc + RR * Math.Sin(Ang1 - Math.Abs(Ang0 - Ang1));

				//'''''''        Set GrArcBot = Grs.AddArcTriplePoint(xB, yB, 0, xMid, yMid, 0, xE, yE, 0)
				GrArcBot = Module1.GrsSet.AddArcTriplePoint(xB, yB, 0, xMid, yMid, 0, xE, yE, 0);
//				object tempRefParam11 = "PenColor";
//				object tempRefParam12 = Color.FromArgb(255, 0, 0);
				GrArcBot.Properties.get_Item("PenColor").Value = Color.FromArgb(255, 0, 0);

				// Duplicate Line and InvoluteCurve
				if (GrLine0 != null)
				{
					//'''''''            Set GrLine1 = GrLine0.Duplicate
//					object tempRefParam13 = Type.Missing;
					GrLine1 = (IMSIGX.Graphic) GrLine0.Clone(ImsiGraphicCopyFlags.imsiGceCopyIds);
					//object tempRefParam14 = Math.Cos(Ang1);
					//object tempRefParam15 = Math.Sin(Ang1);
					//object tempRefParam16 = 0;
					//object tempRefParam17 = xc;
					//object tempRefParam18 = yc;
					//object tempRefParam19 = 0;
					GrLine1.RotateAxis(Module1.Pi, Math.Cos(Ang1) , Math.Sin(Ang1), 0, xc, yc, 0);
					//yc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam18);
					//xc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam17);
				}
				if (GrInv0 != null)
				{
					//'''''''            Set GrInv1 = GrInv0.Duplicate
					object tempRefParam20 = Type.Missing;
					GrInv1 = (IMSIGX.Graphic) GrInv0.Clone(ImsiGraphicCopyFlags.imsiGceCopyIds);
					//object tempRefParam21 = Math.Cos(Ang1);
					//object tempRefParam22 = Math.Sin(Ang1);
					//object tempRefParam23 = 0;
					//object tempRefParam24 = xc;
					//object tempRefParam25 = yc;
					//object tempRefParam26 = 0;
					GrInv1.RotateAxis(Module1.Pi, Math.Cos(Ang1), Math.Sin(Ang1), 0, xc, yc, 0);
					//yc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam25);
					//xc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam24);
				}
			}
			//##########################################################################
			//##########################################################################

			// Add Top Arc Points
			sina = (yT[nDiv] - yc) / R0;
			cosa = (xT[nDiv] - xc) / R0;
			Ang0 = Angle(sina, cosa);
			Ang1 = AngMid + AngPitch / 4;
			dfi = (Ang1 - Ang0) / ArcDiv;
			for (int i = 1; i <= ArcDiv; i++)
			{
				fi = Ang0 + i * dfi;
				xi = xc + R0 * Math.Cos(fi);
				yi = yc + R0 * Math.Sin(fi);
				xT = ArraysHelper.RedimPreserve(xT, new int[]{nDiv + 2});
				yT = ArraysHelper.RedimPreserve(yT, new int[]{nDiv + 2});
				nDiv++;
				xT[nDiv] = xi;
				yT[nDiv] = yi;
			}

			//##########################################################################
			//##########################################################################
			IMSIGX.Graphic GrArcTop = null;
			if (BulgePoly)
			{
				//Add Top arc
				xB = xc + R0 * Math.Cos(Ang0);
				yB = yc + R0 * Math.Sin(Ang0);
				xMid = xc + R0 * Math.Cos(Ang1);
				yMid = yc + R0 * Math.Sin(Ang1);
				xE = xc + R0 * Math.Cos(Ang1 + Math.Abs(Ang0 - Ang1));
				yE = yc + R0 * Math.Sin(Ang1 + Math.Abs(Ang0 - Ang1));

				//'''''''        Set GrArcTop = Grs.AddArcTriplePoint(xB, yB, 0, xMid, yMid, 0, xE, yE, 0)
				GrArcTop = (IMSIGX.Graphic) Module1.GrsSet.AddArcTriplePoint(xB, yB, 0, xMid, yMid, 0, xE, yE, 0);
				//object tempRefParam27 = "PenColor";
				//object tempRefParam28 = Color.FromArgb(255, 0, 0);
				GrArcTop.Properties.get_Item("PenColor").Value = Color.FromArgb(255, 0, 0);

				if (GrInv1 != null)
				{
					//'''''''            Grs.Remove GrInv1.Index
					//object tempRefParam29 = Type.Missing;
					//object tempRefParam30 = Type.Missing;
                    GrGroup.Graphics.AddGraphic(GrInv1,1,0);//, ref tempRefParam29, ref tempRefParam30);
				}

				if (GrLine1 != null)
				{
					//'''''''            Grs.Remove GrLine1.Index
					//object tempRefParam31 = Type.Missing;
					//object tempRefParam32 = Type.Missing;
                    GrGroup.Graphics.AddGraphic(GrLine1,1,0);//, ref tempRefParam31, ref tempRefParam32);
				}

				if (GrArcBot != null)
				{
                    //'''''''            Grs.Remove GrArcBot.Index
                    //object tempRefParam33 = Type.Missing;
                    //object tempRefParam34 = Type.Missing;
                    GrGroup.Graphics.AddGraphic(GrArcBot,1,0);//, ref tempRefParam33, ref tempRefParam34);
				}

				if (GrLine0 != null)
				{
                    //'''''''            Grs.Remove GrLine0.Index
                    //object tempRefParam35 = Type.Missing;
                    //object tempRefParam36 = Type.Missing;
                    GrGroup.Graphics.AddGraphic(GrLine0,1,0);//, ref tempRefParam35, ref tempRefParam36);
				}

				if (GrInv0 != null)
				{
                    //'''''''            Grs.Remove GrInv0.Index
                    //object tempRefParam37 = Type.Missing;
                    //object tempRefParam38 = Type.Missing;
                    GrGroup.Graphics.AddGraphic(GrInv0,1,0);//, ref tempRefParam37, ref tempRefParam38);
				}

				if (GrArcTop != null)
				{
                    //'''''''            Grs.Remove GrArcTop.Index
                    //object tempRefParam39 = Type.Missing;
                    //object tempRefParam40 = Type.Missing;
                    GrGroup.Graphics.AddGraphic(GrArcTop,1,0);//, ref tempRefParam39, ref tempRefParam40);
				}


				GrArcTop = null;
				GrInv0 = null;
				GrLine0 = null;
				GrLine1 = null;
				GrInv1 = null;
				GrArcBot = null;
			}
			//##########################################################################
			//##########################################################################

			//###########################################################################
			// Define the bottom fillet
			double rFillet = 0;
			double x1 = xT[1];
			double y1 = yT[1];
			double x2 = xT[2];
			double y2 = yT[2];
			double x3 = xT[3];
			double y3 = yT[3];
			L1 = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
			L2 = Math.Sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));
			if (L2 > L1)
			{
				rFillet = L1 / 2;
			}
			else
			{
				if (L2 > L1 / 2)
				{
					rFillet = L1 / 2;
				}
				else
				{
					rFillet = 0.8d * L2;
				}
			}
			sina = (y1 - y2) / L1;
			cosa = (x1 - x2) / L1;
			double yLoc = (y3 - y2) * cosa - (x3 - x2) * sina;
			double xLoc = (y3 - y2) * sina + (x3 - x2) * cosa;
			double sinb = Math.Abs(yLoc) / L2;
			double cosb = Math.Abs(xLoc) / L2;
			double Bet = Module1.Pi - Angle(sinb, cosb);
			xLoc = rFillet / Math.Tan(Bet / 2);
			yLoc = rFillet;
			double xcFillet = x2 + xLoc * cosa - yLoc * sina;
			double ycFillet = y2 + xLoc * sina + yLoc * cosa;
			yLoc = 0;
			double xLast = x2 + xLoc * cosa - yLoc * sina;
			double yLast = y2 + xLoc * sina + yLoc * cosa;
			L1 = Math.Sqrt((x2 - xcFillet) * (x2 - xcFillet) + (y2 - ycFillet) * (y2 - ycFillet));
			sina = (y2 - ycFillet) / L1;
			cosa = (x2 - xcFillet) / L1;
			xLoc = rFillet;
			yLoc = 0;
			xMid = xcFillet + xLoc * cosa - yLoc * sina;
			yMid = ycFillet + xLoc * sina + yLoc * cosa;
			sina = (y3 - y2) / L2;
			cosa = (x3 - x2) / L2;
			xLoc = rFillet / Math.Tan(Bet / 2);
			yLoc = 0;
			double xFirst = x2 + xLoc * cosa - yLoc * sina;
			double yFirst = y2 + xLoc * sina + yLoc * cosa;

			xT[ArcDiv] = xFirst;
			yT[ArcDiv] = yFirst;

			xT = ArraysHelper.RedimPreserve(xT, new int[]{nDiv + 2});
			yT = ArraysHelper.RedimPreserve(yT, new int[]{nDiv + 2});
			for (int j = nDiv; j >= 2; j--)
			{
				xT[j + 1] = xT[j];
				yT[j + 1] = yT[j];
			}
			xT[ArcDiv] = xMid;
			yT[ArcDiv] = yMid;
			nDiv++;
			xT = ArraysHelper.RedimPreserve(xT, new int[]{nDiv + 2});
			yT = ArraysHelper.RedimPreserve(yT, new int[]{nDiv + 2});
			for (int j = nDiv; j >= 2; j--)
			{
				xT[j + 1] = xT[j];
				yT[j + 1] = yT[j];
			}
			xT[ArcDiv] = xLast;
			yT[ArcDiv] = yLast;
			nDiv++;


			//###########################################################################
			// Form theProfile for Single Teeth
			xT = ArraysHelper.RedimPreserve(xT, new int[]{2 * nDiv + 1});
			yT = ArraysHelper.RedimPreserve(yT, new int[]{2 * nDiv + 1});
			int nDiv0 = nDiv;
			xi = xT[nDiv];
			yi = yT[nDiv];
			ri = Math.Sqrt((xi - xc) * (xi - xc) + (yi - yc) * (yi - yc));
			sina = (yi - yc) / ri;
			cosa = (xi - xc) / ri;
			fi0 = Angle(sina, cosa);

			for (int i = nDiv0 - 1; i >= 0; i--)
			{
				xi = xT[i];
				yi = yT[i];
				ri = Math.Sqrt((xi - xc) * (xi - xc) + (yi - yc) * (yi - yc));
				sina = (yi - yc) / ri;
				cosa = (xi - xc) / ri;
				fi = Angle(sina, cosa);
				if (fi > Module1.Pi)
				{
					fi -= 2 * Module1.Pi;
				}
				dfi = fi - fi0;
				dfi = -dfi;
				fi = fi0 + dfi;
				xi = xc + ri * Math.Cos(fi);
				yi = yc + ri * Math.Sin(fi);
				nDiv++;
				xT[nDiv] = xi;
				yT[nDiv] = yi;
			}
			// Polar cordinate for single Teeth
			double[] fiT = null;
			double[] rT = null;
			fiT = new double[2 * nDiv + 1];
			rT = new double[2 * nDiv + 1];
			for (int i = 0; i <= nDiv; i++)
			{
				xi = xT[i];
				yi = yT[i];
				ri = Math.Sqrt((xi - xc) * (xi - xc) + (yi - yc) * (yi - yc));
				sina = (yi - yc) / ri;
				cosa = (xi - xc) / ri;
				fi = Angle(sina, cosa);
				fiT[i] = fi;
				rT[i] = ri;
			}

			nDiv0 = nDiv;
			xT = ArraysHelper.RedimPreserve(xT, new int[]{2 * nDiv * NumTeeth + 1});
			yT = ArraysHelper.RedimPreserve(yT, new int[]{2 * nDiv * NumTeeth + 1});
			fiT = ArraysHelper.RedimPreserve(fiT, new int[]{2 * nDiv * NumTeeth + 1});
			rT = ArraysHelper.RedimPreserve(rT, new int[]{2 * nDiv * NumTeeth + 1});


			for (int i = 1; i <= NumTeeth - 1; i++)
			{
				dfi = AngPitch * i;
				for (int j = 1; j <= nDiv0; j++)
				{
					ri = rT[j];
					fi = fiT[j];
					fi += dfi;
					xi = xc + ri * Math.Cos(fi);
					yi = yc + ri * Math.Sin(fi);
					nDiv++;
					xT[nDiv] = xi;
					yT[nDiv] = yi;
				}
			}


			IMSIGX.Vertices withVar = null;
			IMSIGX.UndoRecord UndoRec = null;
			if (!BulgePoly)
			{
				if (nDiv > 2)
				{
					//object tempRefParam41 = IMSIGX.ImsiGraphicType.imsiPolyline;
					//object tempRefParam42 = Type.Missing;
					//object tempRefParam43 = Type.Missing;
					//object tempRefParam44 = Type.Missing;
					//object tempRefParam45 = Type.Missing;
					//object tempRefParam46 = Type.Missing;
                    Gr = (IMSIGX.Graphic)Module1.Grs.Add((int)ImsiGraphicType.imsiPolyline);//ref tempRefParam41, ref tempRefParam42, ref tempRefParam43, ref tempRefParam44, ref tempRefParam45, ref tempRefParam46);
					withVar = Gr.Vertices;
					for (int i = 0; i <= nDiv - 1; i++)
					{
						xi = xT[i];
						yi = yT[i];
                        //object tempRefParam47 = xi;
                        //object tempRefParam48 = yi;
                        //object tempRefParam49 = 0;
                        //object tempRefParam50 = Type.Missing;
                        //object tempRefParam51 = Type.Missing;
                        //object tempRefParam52 = Type.Missing;
                        //object tempRefParam53 = Type.Missing;
                        //object tempRefParam54 = Type.Missing;
                        //object tempRefParam55 = Type.Missing;
                        //object tempRefParam56 = Type.Missing;
                        //object tempRefParam57 = Type.Missing;
                        withVar.Add(xi, yi, 0);//ref tempRefParam47, ref tempRefParam48, ref tempRefParam49, ref tempRefParam50, ref tempRefParam51, ref tempRefParam52, ref tempRefParam53, ref tempRefParam54, ref tempRefParam55, ref tempRefParam56, ref tempRefParam57);
						//yi = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam48);
						//xi = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam47);
					}
                    //object tempRefParam58 = Type.Missing;
                    //object tempRefParam59 = Type.Missing;
                    //object tempRefParam60 = Type.Missing;
                    //object tempRefParam61 = Type.Missing;
                    //object tempRefParam62 = Type.Missing;
                    //object tempRefParam63 = Type.Missing;
                    withVar.AddClose(true,true,true,false,false,true);
					//    TCWUndoRecordStart hDr, "Add GearContour"
					//    GrIndex = GrRes.Index
					//    hGr = TCWGraphicAt(hDr, GrIndex)
					//    TCWUndoRecordAddGraphic hDr, hGr
					//    TCWUndoRecordEnd hDr
					Gr.Transform((IMSIGX.Matrix) Module1.ActDr.UCS);
					Gr.UCS = Module1.ActDr.UCS;

					UndoRec = Module1.ActDr.AddUndoRecord(CommonLocaliz.TcLoadLangString(101));
					UndoRec.AddGraphic((IMSIGX.Graphic) Gr);
					UndoRec.Close();
					UndoRec = null;
				}

			}
			//#####################################################################
			//#####################################################################

			int GrGroupCount = 0;
			IMSIGX.Graphic GrDup = null;
			IMSIGX.Graphics withVar_2 = null;
			double xBeg0 = 0, yBeg0 = 0;
			IMSIGX.Graphic GrGearContour = null;
			if (BulgePoly)
			{
				// Add Full Gear contour as couple of Arcs and lines
				GrGroupCount = GrGroup.Graphics.Count;
				withVar_2 = GrGroup.Graphics;
				for (int i = 1; i <= NumTeeth - 1; i++)
				{
					dfi = AngPitch * i;
					for (int j = 0; j <= GrGroupCount - 1; j++)
					{
						//object tempRefParam64 = j;
						GrDup = (IMSIGX.Graphic) withVar_2.get_Item(j).Duplicate();
						//j = ReflectionHelper.GetPrimitiveValue<int>(tempRefParam64);
						//'''''                    Set GrDup = .Item(j).Clone
						//object tempRefParam65 = 0;
						//object tempRefParam66 = 0;
						//object tempRefParam67 = 1;
						//object tempRefParam68 = xc;
						//object tempRefParam69 = yc;
						//object tempRefParam70 = 0;
                        GrDup.RotateAxis(dfi, 0, 0, 1, xc, yc, 0);//ref tempRefParam65, ref tempRefParam66, ref tempRefParam67, ref tempRefParam68, ref tempRefParam69, ref tempRefParam70);
						//yc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam69);
						//xc = ReflectionHelper.GetPrimitiveValue<double>(tempRefParam68);
						// Next two lines need as Duplicate method inserts new graphic immediately after Item(j)
						// so calling Remove and AddGraphic we insert this new graphic at end of graphic collection
//						object tempRefParam71 = GrDup.Index;
						withVar_2.Remove(GrDup.Index);
                        //object tempRefParam72 = Type.Missing;
                        //object tempRefParam73 = Type.Missing;
                        withVar_2.AddGraphic(GrDup,1,0);//, ref tempRefParam72, ref tempRefParam73);
					}
				}
//				object tempRefParam74 = 0;
				GrDup = GrGroup.Graphics.get_Item(0);
				//object tempRefParam75 = 2;
				xBeg0 = GrDup.Vertices.get_Item(2).X;
				//object tempRefParam76 = 2;
				yBeg0 = GrDup.Vertices.get_Item(2).Y;


				//'''''''        Set GrGearContour = Grs.Add(, "TCW50Polyline")
				//object tempRefParam77 = "PolySet";
				//object tempRefParam78 = false;
                ResSet = Module1.ActDr.GraphicSets.Add("PolySet", false); //# NLS#'
                                                                          //object tempRefParam79 = null;
                                                                          //object tempRefParam80 = "TCW50Polyline";
                                                                          //object tempRefParam81 = Type.Missing;
                                                                          //object tempRefParam82 = Type.Missing;
                                                                          //object tempRefParam83 = Type.Missing;
                                                                          //object tempRefParam84 = Type.Missing;
                
                GrGearContour = ResSet.Add(11,"TCW50Polyline",true,(IMSIGX.Style)"IsClosed",1,0);// ref tempRefParam80, ref tempRefParam81, ref tempRefParam82, ref tempRefParam83, ref tempRefParam84); //# NLS#'
				Polyline50(GrGroup, false, xBeg0, yBeg0, GrGearContour);
				if (ResSet != null)
				{
					//object tempRefParam85 = Type.Missing;
					ResSet.Clear(ImsiGraphicFlags.imsiGfGraphicDirty);
					ResSet = null;
				}

				GrDup = null;
				GrGroup = null;
			}
			//    If Form1.Check1.Value = 1 Then
			//        FileWrite = False
			//        Form2.Show 1
			//        If FileWrite = True Then
			//Dim FilePath$
			//            FilePath = Form2.PathFile
			//            Open FilePath For Output As #1
			//Dim nV As Long
			//Dim Str$
			//            If BulgePoly = True Then
			//                With GrGearContour.Graphics(0).Vertices
			//                    nV = .Count
			//                    Str = "Gear Contour"
			//                    Print #1, Str
			//                    Str = "Point count = " & CStr(nV)
			//                    Print #1, Str
			//                    For i = 0 To nV - 1
			//                        xi = .Item(i).x
			//                        yi = .Item(i).y
			//                        Str = "G1" & "X" & Format(xi, "###0.0000") & "Y" & Format(yi, "###0.0000")
			//                        Print #1, Str
			//                    Next i
			//                End With
			//            Else
			//                With Gr.Vertices
			//                    nV = .Count
			//                    Str = "Gear Contour"
			//                    Print #1, Str
			//                    Str = "Point count = " & CStr(nV)
			//                    Print #1, Str
			//                    For i = 0 To nV - 1
			//                        xi = .Item(i).x
			//                        yi = .Item(i).y
			//                        Str = "G1" & "X" & Format(xi, "###0.0000") & "Y" & Format(yi, "###0.0000")
			//                        Print #1, Str
			//                    Next i
			//                End With
			//            End If
			//            Close #1
			//        End If
			//    End If


			//#####################################################################
			//#####################################################################
		}
		private bool LineCircIntersection(double x1, double y1, double sina, double cosa, double xc, double yc, double R, ref double xInt1, ref double yInt1, ref double xInt2, ref double yInt2)
		{
            double xcLoc = (yc - y1) * sina + (xc - x1) * cosa;
			double ycLoc = (yc - y1) * cosa - (xc - x1) * sina;
			if (Math.Abs(ycLoc) > Math.Abs(R) + Module1.Eps)
			{
				return false;
			}
			double t = Math.Sqrt(Math.Abs(R * R - ycLoc * ycLoc));
			double yLoc = 0;
			double xLoc = xcLoc + t;
			xInt1 = x1 + xLoc * cosa - yLoc * sina;
			yInt1 = y1 + xLoc * sina + yLoc * cosa;

			xLoc = xcLoc - t;
			xInt2 = x1 + xLoc * cosa - yLoc * sina;
			yInt2 = y1 + xLoc * sina + yLoc * cosa;
			return true;
		}

		private double Angle(double sinb, double cosb)
		{
            double result = 0;
			if (Math.Abs(cosb) < 0.0001d)
			{
				if (sinb > 0)
				{
					result = Module1.Pi / 2;
				}
				else
				{
					result = 3 * Module1.Pi / 2;
				}
			}
			else
			{
				if (sinb >= 0 && cosb > 0)
				{
					result = Math.Atan(sinb / cosb);
				}
				if (sinb >= 0 && cosb < 0)
				{
					result = Module1.Pi + Math.Atan(sinb / cosb);
				}
				if (sinb < 0 && cosb < 0)
				{
					result = Module1.Pi + Math.Atan(sinb / cosb);
				}
				if (sinb < 0 && cosb > 0)
				{
					result = 2 * Module1.Pi + Math.Atan(sinb / cosb);
				}
			}
			return result;
		}


		// Define the middle point of Base Arc
		// from Beg-End Points and Rot
		// Rot=1 - Counter Clockwise
		// Rot=-1 - Clockwise
		private void ArcMiddlePoint(double xc, double yc, double xB, double yB, double xE, double yE, double R, int Rot, ref double xMid, ref double yMid)
		{
            if (Math.Abs(R) < Module1.Eps)
			{
				xMid = xc;
				yMid = yc;
				return;
			}

			double sina = (yB - yc) / R;
			double cosa = (xB - xc) / R;
			double fiBeg = Angle(sina, cosa);

			sina = (yE - yc) / R;
			cosa = (xE - xc) / R;
			double fiEnd = Angle(sina, cosa);

			if (Rot == 1)
			{ // Counter Clockwise
				if (fiBeg > fiEnd)
				{
					fiEnd -= 2 * Module1.Pi;
				}
			}
			else
			{
				//Clockwise
				if (fiBeg < fiEnd)
				{
					fiEnd -= 2 * Module1.Pi;
				}
			}

			double fiMid = (fiBeg + fiEnd) / 2;
			xMid = xc + R * Math.Cos(fiMid);
			yMid = yc + R * Math.Sin(fiMid);


		}

		// Define the direction of Arc rotation
		// return ArcRotation = 1 if CounterClockwise
		// return ArcRotation = -1 if Clockwise

		private int ArcRotation(double xB, double yB, double xDir, double yDir, double xc, double yc, double R)
		{
            double sina = (yB - yc) / R; //, Alp0#, Alp1#
			double cosa = (xB - xc) / R; //, Alp0#, Alp1#
			double xLoc = (yDir - yc) * sina + (xDir - xc) * cosa;
			double yLoc = (yDir - yc) * cosa - (xDir - xc) * sina;
			if (yLoc > 0)
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}

		//Returns true if tool is correctly initialized
        public void Terminate(Tool Context)//object objTool)
		{
            gxMe = null;

			//return null;
        }

        #endregion
    }
}