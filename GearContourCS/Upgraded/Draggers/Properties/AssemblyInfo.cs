using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.


// Review the values of the assembly attributes


[assembly: AssemblyTitle("PickInside")]
[assembly: AssemblyDescription("<vb> Version Info")]
[assembly: AssemblyCompany("$COMPANY, LLC")]
[assembly: AssemblyProduct("$TC_NAME (tm) for Windows")]
[assembly: AssemblyCopyright("Copyright (c) $COMPANY 2010")]
[assembly: AssemblyTrademark("$TC_NAME (tm) is a registered trademark of $COMPANY")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:

//  Major Version
//  Minor Version
//  Build Number
//  Revision

// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion=("1.0.*")]


[assembly: AssemblyVersion("17.0.*")]


[assembly: ClassInterface(ClassInterfaceType.AutoDual)]
[assembly: Guid("494747D3-9C5D-45A2-9032-7F65A00B3C16")]
