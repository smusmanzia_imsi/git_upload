using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using IMSIGX;
using TCDotNetInterfaces;
using UpgradeHelpers.Helpers;


namespace Gear
{
    class TCEventSink : IMSIGX.IAppEvents
    {
        Contour m_Tool;
        

        public TCEventSink(Contour Tool)
        {
            this.m_Tool = Tool;
        }

        #region IAppEvents Members
        public void BeforeDoubleClick(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, Selection Sel, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("2");
            throw new NotImplementedException();
        }

        public void BeforeExit(IMSIGX.Application TheApp, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("3");
            throw new NotImplementedException();
        }

        public void BeforeRightClick(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, Selection Sel, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("4");
            throw new NotImplementedException();
        }

        public void CommandBarControlDone(CommandBarControl WhichControl)
        {
            System.Windows.Forms.MessageBox.Show("5");
            throw new NotImplementedException();
        }

        public void CommandBarControlHit(CommandBarControl WhichControl, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("6");
            throw new NotImplementedException();
        }

        public void CommandBarControlStatus(CommandBarControl WhichControl)
        {
            System.Windows.Forms.MessageBox.Show("7");
            throw new NotImplementedException();
        }



        public void DrawingAfterSave(Drawing WhichDrawing)
        {
            System.Windows.Forms.MessageBox.Show("8");
            throw new NotImplementedException();
        }

        public void DrawingBeforeClose(Drawing WhichDrawing, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("9");
            throw new NotImplementedException();
        }

        public void DrawingBeforeSave(Drawing WhichDrawing, ref bool SaveAs, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("10");
            throw new NotImplementedException();
        }


        void IAppEvents.DrawingNew(Drawing WhichDrawing)
        {
            System.Windows.Forms.MessageBox.Show("New Drawing");
            m_Tool.Finish();
        }



        public void Drop(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, Selection Sel)
        {
            System.Windows.Forms.MessageBox.Show("11");
            throw new NotImplementedException();
        }

        public void MouseUp(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, ImsiMouseButton Button, int Shift, int X, int Y, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("12");
            throw new NotImplementedException();
        }

        public void PointPick(Drawing WhichDrawing, IMSIGX.View WhichView, PickResult Result, bool PickWasCanceled)
        {
            System.Windows.Forms.MessageBox.Show("13");
            throw new NotImplementedException();
        }

        public void PointSnapped(Drawing WhichDrawing, IMSIGX.View WhichView, int X, int Y, Vertex PointRaw, Vertex PointSnapped)
        {
            System.Windows.Forms.MessageBox.Show("14");
            throw new NotImplementedException();
        }

        public void PolygonPick(Drawing WhichDrawing, IMSIGX.View WhichView, PickResult Result, bool PickWasCanceled)
        {
            System.Windows.Forms.MessageBox.Show("15");
            throw new NotImplementedException();
        }

        public void RectanglePick(Drawing WhichDrawing, IMSIGX.View WhichView, PickResult Result, bool PickWasCanceled)
        {
            System.Windows.Forms.MessageBox.Show("16");
            throw new NotImplementedException();
        }

        public void SelectionChange(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, Selection Sel)
        {
            System.Windows.Forms.MessageBox.Show("17");
            throw new NotImplementedException();
        }

        public void ViewAfterRedraw(Drawing WhichDrawing, IMSIGX.View WhichView)
        {
            System.Windows.Forms.MessageBox.Show("18");
            throw new NotImplementedException();
        }

        public void ViewBeforeRedraw(Drawing WhichDrawing, IMSIGX.View WhichView)
        {
            System.Windows.Forms.MessageBox.Show("19");
            throw new NotImplementedException();
        }

        public void VirtualIntersectionPick(Drawing WhichDrawing, IMSIGX.View WhichView, PickResult Result, bool PickWasCanceled)
        {
            System.Windows.Forms.MessageBox.Show("20");
            throw new NotImplementedException();
        }

        public void WindowActivate(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow)
        {
            throw new NotImplementedException();
        }

        public void WindowDeactivate(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow)
        {
            throw new NotImplementedException();
        }

        public void WindowResize(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow)
        {
            throw new NotImplementedException();
        }
        void IAppEvents.DrawingDeactivate(Drawing WhichDrawing)
        {
            System.Windows.Forms.MessageBox.Show("Drawing Deactivate");
            m_Tool.Finish();
        }
        void IAppEvents.DrawingBeforeClose(Drawing WhichDrawing, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("drawing Before Close");
            m_Tool.Finish();
        }
        void IAppEvents.DrawingBeforeSave(Drawing WhichDrawing, ref bool SaveAs, ref bool Cancel)
        {
            System.Windows.Forms.MessageBox.Show("Drawing before save");
            m_Tool.Finish();
        }
        void IAppEvents.ViewAfterRedraw(IMSIGX.Drawing WhichDrawing, IMSIGX.View WhichView)
        {
            try
            {
                if (m_Tool.GrMoved != null)
                {
                    m_Tool.GrMoved.Visible = true;
                    m_Tool.GrMoved.Draw(WhichView);
                }
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("View Failed");
            }
        }

        void IAppEvents.MouseDown(IMSIGX.Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, ImsiMouseButton Button, int Shift, int x, int y, ref bool Cancel)
        {
                double xClick = 0, yClick = 0;
                try
                {
                    Module1.Drs = Module1.objApp.Drawings;
                    Module1.ActDr = WhichDrawing;
                    Module1.Grs = Module1.ActDr.Graphics;
                    Module1.Vis = Module1.ActDr.Views;
                    Module1.Vi = WhichView;

                    Module1.Vi.ScreenToView(x, y, out xClick, out yClick);
                    double tempRefParam = 0;
                    Module1.Vi.ViewToWorld(xClick, yClick, 0, out m_Tool.xcWorld, out m_Tool.ycWorld, out tempRefParam);
                    IMSIGX.Graphic TempGr = null;
                    int Num = 0;
                    if (Button == IMSIGX.ImsiMouseButton.imsiLeftButton)
                    {
                        Cancel = true;

                        m_Tool.GrMoved.Visible = false;
                        m_Tool.GrMoved.Draw(WhichView);

                        TempGr = m_Tool.GrMoved;
                        m_Tool.GrMoved = null;
                        Num = Module1.GrsSet.Count;
                        for (int cnt = 0; cnt <= Num - 1; cnt++)
                        {
                            if (TempGr == Module1.GrsSet.get_Item(cnt))
                            {
                                Module1.GrsSet.Remove(cnt);
                                TempGr.Delete();
                                TempGr = null;
                                break;
                            }
                        }

                        m_Tool.TeethInvol();
                        m_Tool.Finish();
                    }
                }
                catch
                {
                System.Windows.Forms.MessageBox.Show("Mouse Down failed");
                m_Tool.Finish();
                    //   MsgBox Err.Description
                }
        }

        void IAppEvents.MouseMove(Drawing WhichDrawing, IMSIGX.View WhichView, Window WhichWindow, int Shift, int x, int y, ref bool Cancel)
        {
            //LVR 041204: #17954
            Cancel = true;

            if (!m_Tool.ParExist)
            {
                return;
            }
            Module1.ActDr = WhichDrawing;
            Module1.Vi = WhichView;
            
            double PressureAngle = 0;
            double DiaPitch = 0;
            int NumTeeth = 0;
            double RR = 0, R = 0, Rb = 0, R0 = 0, b = 0;
            double hVi = 0;
            double CentSize = 0;
            IMSIGX.Graphic GrChild = null;
            IMSIGX.Vertices withVar = null;
            double ZoomFactor = 0;
            double xVi = 0, yVi = 0;
            double xW = 0, yW = 0;
            double xc = 0, yc = 0;
            IMSIGX.Vertices withVar_2 = null;
            double dx = 0, dy = 0;
            int GrCount = 0;
            int nV = 0;
            IMSIGX.Vertices withVar_3 = null; //Number of Teeth on Gear //Diametral Pitch // Pressure Angle
            if (m_Tool.GrMoved == null)
            {

                PressureAngle = Double.Parse(Form1.DefInstance.PressureAngle.Text) / 180 * Module1.Pi;
                DiaPitch = Double.Parse(Form1.DefInstance.DiamPitch.Text);
                NumTeeth = Convert.ToInt32(Double.Parse(Form1.DefInstance.TeethCount.Text));
                // Rb - Base Circle Radius
                // R - Pitch Radius
                // R0 - Outside Radius
                // RR - Root Radius
                // b - Dedendum
                R = NumTeeth / (2 * DiaPitch);
                R0 = (NumTeeth + 2) / (2 * DiaPitch);
                Rb = R * Math.Cos(PressureAngle);
                b = 1.25d / DiaPitch;
                RR = R - b;

                //        Set GrMoved = Grs.Add(11)
                m_Tool.GrMoved = Module1.Grs.Add((int)ImsiGraphicType.imsiGroup); //(IMSIGX.Graphic)Module1.GrsSet.Add(ref tempRefParam, ref tempRefParam2, ref tempRefParam3, ref tempRefParam4, ref tempRefParam5, ref tempRefParam6);
                try
                {
                    m_Tool.GrMoved.UCS = Module1.ActDr.UCS;
                    hVi = Module1.Vi.ViewHeight;
                    CentSize = R0 / 20;
                    GrChild = m_Tool.GrMoved.Graphics.AddCircleCenterAndPoint(0, 0, 0, R0, 0, 0);
                    GrChild.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0);
                    GrChild.Properties.get_Item("PenWidth").Value = Color.FromArgb(0, 0, 0); 
                    GrChild.Properties.get_Item("PenStyle").Value = "CONTINUOUS";


                    GrChild = m_Tool.GrMoved.Graphics.AddCircleCenterAndPoint(0, 0, 0, R, 0, 0);
                    GrChild.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0);
                    GrChild.Properties.get_Item("PenWidth").Value = 0;
                    GrChild.Properties.get_Item("PenStyle").Value = "DASHED";


                    GrChild = m_Tool.GrMoved.Graphics.AddCircleCenterAndPoint(0, 0, 0, RR, 0, 0);
                    GrChild.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0); 
                    GrChild.Properties.get_Item("PenWidth").Value = 0;
                    GrChild.Properties.get_Item("PenStyle").Value = "CONTINUOUS";
                    
                    GrChild = m_Tool.GrMoved.Graphics.Add(11);
                    withVar = GrChild.Vertices;

                    //object tempRefParam31 = -CentSize;
                    //object tempRefParam32 = 0;
                    //object tempRefParam33 = 0;
                    //object tempRefParam34 = true;
                    withVar.Add(-CentSize, 0, 0);//, true,false, false, false, false,false);//,ref tempRefParam31, ref tempRefParam32, ref tempRefParam33, ref tempRefParam34, ref tempRefParam35, ref tempRefParam36, ref tempRefParam37, ref tempRefParam38, ref tempRefParam39, ref tempRefParam40, ref tempRefParam41);

                    //object tempRefParam42 = CentSize;
                    //object tempRefParam43 = 0;
                    //object tempRefParam44 = 0;
                    //object tempRefParam45 = true;
                    withVar.Add(CentSize, 0, 0); //,true, false, false, false, false, false); //ref tempRefParam42, ref tempRefParam43, ref tempRefParam44, ref tempRefParam45, ref tempRefParam46, ref tempRefParam47, ref tempRefParam48, ref tempRefParam49, ref tempRefParam50, ref tempRefParam51, ref tempRefParam52);

                    //object tempRefParam53 = 0;
                    //object tempRefParam54 = -CentSize;
                    //object tempRefParam55 = 0;
                    //object tempRefParam56 = false;
                    withVar.Add(0, -CentSize, 0);//,false, false, false, false, false, false); //ref tempRefParam53, ref tempRefParam54, ref tempRefParam55, ref tempRefParam56, ref tempRefParam57, ref tempRefParam58, ref tempRefParam59, ref tempRefParam60, ref tempRefParam61, ref tempRefParam62, ref tempRefParam63);

                    //object tempRefParam64 = 0;
                    //object tempRefParam65 = CentSize;
                    //object tempRefParam66 = 0;
                    //object tempRefParam67 = true;
                    withVar.Add(0, CentSize, 0);//,true, false, false, false, false, false); //ref tempRefParam64, ref tempRefParam65, ref tempRefParam66, ref tempRefParam67, ref tempRefParam68, ref tempRefParam69, ref tempRefParam70, ref tempRefParam71, ref tempRefParam72, ref tempRefParam73, ref tempRefParam74);

                    GrChild.Properties.get_Item("PenColor").Value = Color.FromArgb(0, 0, 0); ;

                    GrChild.Properties.get_Item("PenWidth").Value = 0.02d;

                    GrChild.Properties.get_Item("PenStyle").Value = "CONTINUOUS";

                    if (hVi < 2.5d * R0)
                    {
                        ZoomFactor = 2.5d * R0 / hVi;
                    }
                    if (m_Tool.Space == IMSIGX.ImsiSpaceModeType.imsiModelSpace)
                    {
                        Module1.Vi.Camera.Zoom(ZoomFactor);
                    }
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("Error here");
                }
            }
            else
            {
                Module1.Vi.ScreenToView(x, y, out xVi, out yVi);
                double tempRefParam81 = 0;
                Module1.Vi.ViewToWorld(xVi, yVi, 0, out xW, out yW, out tempRefParam81);
                GrChild = m_Tool.GrMoved.Graphics.get_Item(0);
                withVar_2 = GrChild.Vertices;
                xc = withVar_2.get_Item(0).X;
                yc = withVar_2.get_Item(0).Y;
                dx = xW - xc;
                dy = yW - yc;

                GrCount = m_Tool.GrMoved.Graphics.Count;
                //m_Tool.GrMoved.Visible = true;
                //m_Tool.GrMoved.Draw(WhichView);
                //Dim gxBBox As BoundingBox

                //      Set gxBBox = GrMoved.CalcBoundingBox()
                //      gxBBox.Inflate ((gxBBox.Max.x - gxBBox.Min.x) * 0.01)

                //WhichDrawing.Views.InvalidateObject(m_Tool.GrMoved);

                for (int i = 0; i <= GrCount - 1; i++)
                {
                    GrChild = m_Tool.GrMoved.Graphics.get_Item(i);
                    withVar_3 = GrChild.Vertices;
                    nV = withVar_3.Count;
                    for (int j = 0; j <= nV - 1; j++)
                    {
                        withVar_3.get_Item(j).X = withVar_3.get_Item(j).X + dx;
                        withVar_3.get_Item(j).Y = withVar_3.get_Item(j).Y + dy;
                    }
                }
                m_Tool.GrMoved.Visible = true;
                m_Tool.GrMoved.Draw(WhichView);
                //LVR 041204: #17954
                //      DoEvents

            }

        }

        void IAppEvents.RunTool(Tool WhichTool)
        {
            System.Windows.Forms.MessageBox.Show("Run Tool");
            m_Tool.Finish();
        }

        void IAppEvents.DrawingOpen(Drawing WhichDrawing)
        {
            System.Windows.Forms.MessageBox.Show("Drawing Open");
            m_Tool.Finish();
        }
        void IAppEvents.DrawingActivate(Drawing WhichDrawing)
        {
            System.Windows.Forms.MessageBox.Show("Drawing Activate");
            m_Tool.Finish();
        }

        #endregion
    }
}
