
namespace Gear
{
	partial class Form1
	{

		#region "Upgrade Support "
		private static Form1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Form1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = CreateInstance();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		public static Form1 CreateInstance()
		{
			Form1 theInstance = new Form1();
			theInstance.Form_Load();
			return theInstance;
		}
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CircPitch", "Option2", "Option1", "CmdCancel", "CmdOk", "PressureAngle", "DiamPitch", "TeethCount", "Label6", "Label5", "Line5", "Line4", "Label4", "Line3", "Line2", "Line1", "Label3", "Label2", "Label1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public System.Windows.Forms.TextBox CircPitch;
		public System.Windows.Forms.RadioButton Option2;
		public System.Windows.Forms.RadioButton Option1;
		public System.Windows.Forms.Button CmdCancel;
		public System.Windows.Forms.Button CmdOk;
		public System.Windows.Forms.TextBox PressureAngle;
		public System.Windows.Forms.TextBox DiamPitch;
		public System.Windows.Forms.TextBox TeethCount;
		public System.Windows.Forms.Label Label6;
		public System.Windows.Forms.Label Label5;
		public System.Windows.Forms.Label Line5;
		public System.Windows.Forms.Label Line4;
		public System.Windows.Forms.Label Label4;
		public System.Windows.Forms.Label Line3;
		public System.Windows.Forms.Label Line2;
		public System.Windows.Forms.Label Line1;
		public System.Windows.Forms.Label Label3;
		public System.Windows.Forms.Label Label2;
		public System.Windows.Forms.Label Label1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.CircPitch = new System.Windows.Forms.TextBox();
			this.Option2 = new System.Windows.Forms.RadioButton();
			this.Option1 = new System.Windows.Forms.RadioButton();
			this.CmdCancel = new System.Windows.Forms.Button();
			this.CmdOk = new System.Windows.Forms.Button();
			this.PressureAngle = new System.Windows.Forms.TextBox();
			this.DiamPitch = new System.Windows.Forms.TextBox();
			this.TeethCount = new System.Windows.Forms.TextBox();
			this.Label6 = new System.Windows.Forms.Label();
			this.Label5 = new System.Windows.Forms.Label();
			this.Line5 = new System.Windows.Forms.Label();
			this.Line4 = new System.Windows.Forms.Label();
			this.Label4 = new System.Windows.Forms.Label();
			this.Line3 = new System.Windows.Forms.Label();
			this.Line2 = new System.Windows.Forms.Label();
			this.Line1 = new System.Windows.Forms.Label();
			this.Label3 = new System.Windows.Forms.Label();
			this.Label2 = new System.Windows.Forms.Label();
			this.Label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
            // 
            // Number of Teeth Text
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(8, 24);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(119, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Number of Teeth (5...100)";
            // 
            // Diametral Pitch Text
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.SystemColors.Control;
            this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(8, 56);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(71, 13);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "Diametral Pitch";
            // 
            // Circular Pitch Text
            // 
            this.Label6.AutoSize = true;
            this.Label6.BackColor = System.Drawing.SystemColors.Control;
            this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label6.Location = new System.Drawing.Point(8, 88);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(62, 13);
            this.Label6.TabIndex = 12;
            this.Label6.Text = "Circular Pitch";
            // 
            // Presssure Angle Text
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.SystemColors.Control;
            this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(8, 120);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(134, 13);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "Pressure Angle(14...30 deg) ";
            // 
            // TeethCount Input Box
            // 
            this.TeethCount.AcceptsReturn = true;
            this.TeethCount.BackColor = System.Drawing.SystemColors.Window;
            this.TeethCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TeethCount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TeethCount.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TeethCount.Location = new System.Drawing.Point(210, 20);
            this.TeethCount.MaxLength = 0;
            this.TeethCount.Name = "TeethCount";
            this.TeethCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TeethCount.Size = new System.Drawing.Size(56, 20);
            this.TeethCount.TabIndex = 1;
            this.TeethCount.Text = "20";
            this.TeethCount.Enter += new System.EventHandler(this.TeethCount_Enter);
            this.TeethCount.Leave += new System.EventHandler(this.TeethCount_Leave);
            // 
            // Diametral Pitch Input Box
            // 
            this.DiamPitch.AcceptsReturn = true;
            this.DiamPitch.BackColor = System.Drawing.SystemColors.Window;
            this.DiamPitch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DiamPitch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DiamPitch.ForeColor = System.Drawing.SystemColors.WindowText;
            this.DiamPitch.Location = new System.Drawing.Point(210, 52);
            this.DiamPitch.MaxLength = 0;
            this.DiamPitch.Name = "DiamPitch";
            this.DiamPitch.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DiamPitch.Size = new System.Drawing.Size(56, 20);
            this.DiamPitch.TabIndex = 2;
            this.DiamPitch.Text = "2";
            this.DiamPitch.Enter += new System.EventHandler(this.DiamPitch_Enter);
            this.DiamPitch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DiamPitch_KeyUp);
            // 
            // CircPitch Input Box
            // 
            this.CircPitch.AcceptsReturn = true;
			this.CircPitch.BackColor = System.Drawing.SystemColors.Window;
			this.CircPitch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.CircPitch.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.CircPitch.ForeColor = System.Drawing.SystemColors.WindowText;
			this.CircPitch.Location = new System.Drawing.Point(210, 84);
			this.CircPitch.MaxLength = 0;
			this.CircPitch.Name = "CircPitch";
			this.CircPitch.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CircPitch.Size = new System.Drawing.Size(56, 20);
			this.CircPitch.TabIndex = 13;
			this.CircPitch.Text = "2.0";
			this.CircPitch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CircPitch_KeyUp);
			// 
			// PressureAngle Input Box
			// 
			this.PressureAngle.AcceptsReturn = true;
			this.PressureAngle.BackColor = System.Drawing.SystemColors.Window;
			this.PressureAngle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.PressureAngle.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.PressureAngle.ForeColor = System.Drawing.SystemColors.WindowText;
			this.PressureAngle.Location = new System.Drawing.Point(210, 116);
			this.PressureAngle.MaxLength = 0;
			this.PressureAngle.Name = "PressureAngle";
			this.PressureAngle.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.PressureAngle.Size = new System.Drawing.Size(56, 20);
			this.PressureAngle.TabIndex = 3;
			this.PressureAngle.Text = "20";
			this.PressureAngle.Enter += new System.EventHandler(this.PressureAngle_Enter);
			this.PressureAngle.Leave += new System.EventHandler(this.PressureAngle_Leave);
           
            // 
            // Label5
            // 
            this.Label5.BackColor = System.Drawing.SystemColors.Window;
			this.Label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label5.Location = new System.Drawing.Point(6, 120);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(208, 1);
			this.Label5.TabIndex = 11;
			this.Label5.Text = "Label5";
			this.Label5.Visible = false;
			// 
			// Line5
			// 
			this.Line5.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line5.Enabled = false;
			this.Line5.Location = new System.Drawing.Point(237, 160);
			this.Line5.Name = "Line5";
			this.Line5.Size = new System.Drawing.Size(30, 1);
			this.Line5.Visible = true;
			// 
			// Line4
			// 
			this.Line4.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line4.Enabled = false;
			this.Line4.Location = new System.Drawing.Point(8, 160);
			this.Line4.Name = "Line4";
			this.Line4.Size = new System.Drawing.Size(30, 1);
			this.Line4.Visible = true;
			
			// 
			// Line3
			// 
			this.Line3.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line3.Enabled = false;
			this.Line3.Location = new System.Drawing.Point(266, 160);
			this.Line3.Name = "Line3";
			this.Line3.Size = new System.Drawing.Size(1, 73);
			this.Line3.Visible = true;
			// 
			// Line2
			// 
			this.Line2.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line2.Enabled = false;
			this.Line2.Location = new System.Drawing.Point(8, 232);
			this.Line2.Name = "Line2";
			this.Line2.Size = new System.Drawing.Size(258, 1);
			this.Line2.Visible = true;
			// 
			// Line1
			// 
			this.Line1.BackColor = System.Drawing.SystemColors.WindowText;
			this.Line1.Enabled = false;
			this.Line1.Location = new System.Drawing.Point(8, 160);
			this.Line1.Name = "Line1";
			this.Line1.Size = new System.Drawing.Size(1, 72);
			this.Line1.Visible = true;
            // 
            // Contour Representation Text
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.SystemColors.Control;
            this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label4.Location = new System.Drawing.Point(60, 152);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(145, 13);
            this.Label4.TabIndex = 10;
            this.Label4.Text = "Contour Representation";
            // 
            // Simple Polyline Radio Button
            // 
            this.Option1.Appearance = System.Windows.Forms.Appearance.Normal;
            this.Option1.BackColor = System.Drawing.SystemColors.Control;
            this.Option1.CausesValidation = true;
            this.Option1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Option1.Checked = true;
            this.Option1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Option1.Enabled = true;
            this.Option1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Option1.Location = new System.Drawing.Point(32, 178);
            this.Option1.Name = "Option1";
            this.Option1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Option1.Size = new System.Drawing.Size(153, 20);
            this.Option1.TabIndex = 4;
            this.Option1.TabStop = true;
            this.Option1.Text = "Simple Polyline";
            this.Option1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Option1.Visible = true;
            // 
            // Polyline with Arcs Radio Button
            // 
            this.Option2.Appearance = System.Windows.Forms.Appearance.Normal;
            this.Option2.BackColor = System.Drawing.SystemColors.Control;
            this.Option2.CausesValidation = true;
            this.Option2.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Option2.Checked = false;
            this.Option2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Option2.Enabled = true;
            this.Option2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Option2.Location = new System.Drawing.Point(32, 200);
            this.Option2.Name = "Option2";
            this.Option2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Option2.Size = new System.Drawing.Size(161, 20);
            this.Option2.TabIndex = 5;
            this.Option2.TabStop = true;
            this.Option2.Text = "Polyline with Arcs";
            this.Option2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Option2.Visible = true;
            // 
            // CmdOk
            // 
            this.CmdOk.BackColor = System.Drawing.SystemColors.Control;
            this.CmdOk.Cursor = System.Windows.Forms.Cursors.Default;
            this.CmdOk.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CmdOk.Location = new System.Drawing.Point(51, 240);
            this.CmdOk.Name = "CmdOk";
            this.CmdOk.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CmdOk.Size = new System.Drawing.Size(73, 25);
            this.CmdOk.TabIndex = 6;
            this.CmdOk.Text = "Ok";
            this.CmdOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CmdOk.UseVisualStyleBackColor = false;
            this.CmdOk.Click += new System.EventHandler(this.CmdOk_Click);
            // 
            // CmdCancel
            // 
            this.CmdCancel.BackColor = System.Drawing.SystemColors.Control;
            this.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default;
            this.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CmdCancel.Location = new System.Drawing.Point(120, 240);
            this.CmdCancel.Name = "CmdCancel";
            this.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CmdCancel.Size = new System.Drawing.Size(100, 25);
            this.CmdCancel.TabIndex = 7;
            this.CmdCancel.Text = "Cancel";
            this.CmdCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CmdCancel.UseVisualStyleBackColor = false;
            this.CmdCancel.Click += new System.EventHandler(this.CmdCancel_Click);

            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8, 16);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(285, 272);
			this.Controls.Add(this.CircPitch);
			this.Controls.Add(this.Option2);
			this.Controls.Add(this.Option1);
			this.Controls.Add(this.CmdCancel);
			this.Controls.Add(this.CmdOk);
			this.Controls.Add(this.PressureAngle);
			this.Controls.Add(this.DiamPitch);
			this.Controls.Add(this.TeethCount);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Line5);
			this.Controls.Add(this.Line4);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Line3);
			this.Controls.Add(this.Line2);
			this.Controls.Add(this.Line1);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(2, 18);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Gear Parameters";
			this.Closed += new System.EventHandler(this.Form1_Closed);
			this.ResumeLayout(false);
		}
		#endregion
	}
}