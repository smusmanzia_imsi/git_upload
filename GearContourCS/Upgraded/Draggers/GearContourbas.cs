using System;
using System.Runtime.InteropServices;
using IMSIGX;

namespace Gear
{
	internal static class Module1
	{

		public struct SelInfo
		{
			public int nV;
			public double[] xV;
			public double[] yV;
		}


		public struct ArcData
		{
			public int NumCen;
			public int[] ArcCen;
			public double[] xMid;
			public double[] yMid;
			public static ArcData CreateInstance()
			{
				ArcData result = new ArcData();
				result.ArcCen = new int[1001];
				result.xMid = new double[1001];
				result.yMid = new double[1001];
				return result;
			}
		}


		public static IMSIGX.Application objApp = null;
		public static IMSIGX.Drawings Drs = null;
		public static IMSIGX.Drawing ActDr = null;
		public static IMSIGX.Graphics Grs = null;
		public static IMSIGX.GraphicSet GrsSet = null;
		public static IMSIGX.Views Vis = null;
		public static IMSIGX.View Vi = null;
		public const double Pi = 3.14159265359d;
		public const double Eps = 0.000001d;
		public static bool ParamCanc = false;
		public static bool FileWrite = false;

		public const int GWL_STYLE = (-16);
		public const int GWL_HWNDPARENT = (-8);
		public const int WS_POPUP = unchecked((int) 0x80000000);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", EntryPoint = "FindWindowA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int FindWindow([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpClassName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpWindowName);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int BringWindowToTop(int hwnd);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", EntryPoint = "GetWindowLongA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetWindowLong(int hwnd, int nIndex);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", EntryPoint = "SetWindowLongA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int SetWindowLong(int hwnd, int nIndex, int dwNewLong);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int SetParent(int hwndC, int hwndP);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetCurrentProcessId();

		internal static void Main_Renamed()
		{
		}
	}
}