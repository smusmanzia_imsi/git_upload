﻿using System.Windows.Forms;
namespace UpgradeStubs
{
    public class IToolEvents
    {

    }
    public static class System_Windows_Forms_Label
    {

        public static float getx1(this Label instance)
        {
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("VB.Line.x1");
            return 0;
        }
        public static float gety1(this Label instance)
        {
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("VB.Line.y1");
            return 0;
        }
        public static float gety2(this Label instance)
        {
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("VB.Line.y2");
            return 0;
        }
        public static void setx1(this Label instance, float x1)
        {
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("VB.Line.x1");
        }
        public static void setx2(this Label instance, float x2)
        {
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("VB.Line.x2");
        }
        public static void sety1(this Label instance, float y1)
        {
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("VB.Line.y1");
        }
        public static void sety2(this Label instance, float y2)
        {
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("VB.Line.y2");
        }
    }
}