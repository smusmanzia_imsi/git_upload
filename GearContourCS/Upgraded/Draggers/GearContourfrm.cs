using Microsoft.VisualBasic;
using System;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;

namespace Gear
{
	internal partial class Form1
		: System.Windows.Forms.Form
	{

		public Form1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint != null && System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}





		private void CmdCancel_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Hide();
		}

		private void CmdOk_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Hide();
			Module1.ParamCanc = false;
		}





		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load method and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void Form_Load()
		{
			Label5.Text = CommonLocaliz.TcLoadLangString(120);
			if (Label5.Text != "Empty do not translate")
			{
				Label5.Visible = true;
				Label5.Height = 73;
				Form1.DefInstance.Width += 70;
				Label5.Width += 70;

				TeethCount.Left += 63;
				DiamPitch.Left += 63;
				CircPitch.Left += 63;

				Label3.Top += Label5.Height;
				PressureAngle.Top += Label5.Height;
				PressureAngle.Left += 63;
				Label4.Top += Label5.Height;
				Option1.Top += Label5.Height;
				Option2.Top += Label5.Height;
				CmdOk.Top += Label5.Height;
				CmdOk.Left = Label4.Left;

				CmdCancel.Top += Label5.Height;
				CmdCancel.Left = PressureAngle.Left + PressureAngle.Width - CmdCancel.Width;

				//UPGRADE_ISSUE: (2064) Line property Line4.y1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line4.sety1(Line4.gety1() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line4.y2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line4.sety2(Line4.gety2() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line4.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line4.setx1(Label5.Left * 15);

				//UPGRADE_ISSUE: (2064) Line property Line5.y1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line5.sety1(Line5.gety1() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line5.y2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line5.sety2(Line5.gety2() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line4.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//UPGRADE_ISSUE: (2064) Line property Line5.x2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line5.setx2(Line4.getx1() + Label5.Width * 15);

				//UPGRADE_ISSUE: (2064) Line property Line1.y1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line1.sety1(Line1.gety1() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line1.y2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line1.sety2(Line1.gety2() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line1.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line1.setx1(Label5.Left * 15);
				//UPGRADE_ISSUE: (2064) Line property Line1.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//UPGRADE_ISSUE: (2064) Line property Line1.x2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line1.setx2(Line1.getx1());

				//UPGRADE_ISSUE: (2064) Line property Line3.y1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line3.sety1(Line3.gety1() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line3.y2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line3.sety2(Line3.gety2() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line1.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//UPGRADE_ISSUE: (2064) Line property Line3.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line3.setx1(Line1.getx1() + Label5.Width * 15);
				//UPGRADE_ISSUE: (2064) Line property Line3.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//UPGRADE_ISSUE: (2064) Line property Line3.x2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line3.setx2(Line3.getx1());

				//UPGRADE_ISSUE: (2064) Line property Line2.y1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line2.sety1(Line2.gety1() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line2.y2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line2.sety2(Line2.gety2() + Label5.Height * 15);
				//UPGRADE_ISSUE: (2064) Line property Line2.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line2.setx1(Label5.Left * 15);
				//UPGRADE_ISSUE: (2064) Line property Line2.x1 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//UPGRADE_ISSUE: (2064) Line property Line2.x2 was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Line2.setx2(Line2.getx1() + Label5.Width * 15);

				Label1.Left = Label4.Left;
				Label2.Left = Label4.Left;
				Label3.Left = Label4.Left;
				Label6.Left = Label4.Left;

				Form1.DefInstance.Height += Label5.Height;
			}
			//Form1.Height =
			Text = CommonLocaliz.TcLoadLangString(110);
			Label1.Text = CommonLocaliz.TcLoadLangString(118);
			Label2.Text = CommonLocaliz.TcLoadLangString(117);
			Label3.Text = CommonLocaliz.TcLoadLangString(116);
			Label4.Text = CommonLocaliz.TcLoadLangString(119);
			Label6.Text = CommonLocaliz.TcLoadLangString(121);
			string Frame1 = CommonLocaliz.TcLoadLangString(112);
			Option1.Text = CommonLocaliz.TcLoadLangString(114);
			Option2.Text = CommonLocaliz.TcLoadLangString(113);
			CmdOk.Text = CommonLocaliz.TcLoadLangString(115);
			CmdCancel.Text = CommonLocaliz.TcLoadLangString(111);
			double s = 0;
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				s = Double.Parse(DiamPitch.Text);
				if (s > 0)
				{
					CircPitch.Text = StringsHelper.Format(Module1.Pi / s, "###0.0000");
				}
			}
			catch (Exception exc)
			{
				//NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}

		}




		private void PressureAngle_Enter(Object eventSender, EventArgs eventArgs)
		{
			PressureAngle.SelectionLength = Strings.Len(PressureAngle.Text);
		}




		private void TeethCount_Enter(Object eventSender, EventArgs eventArgs)
		{
			TeethCount.SelectionLength = Strings.Len(TeethCount.Text);
		}
		private void DiamPitch_Enter(Object eventSender, EventArgs eventArgs)
		{
			DiamPitch.SelectionLength = Strings.Len(DiamPitch.Text);
		}


		private void CircPitch_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = ((int) eventArgs.KeyData) / 65536;
			try
			{
				double s = 0;
				//UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
				s = Double.Parse(CircPitch.Text);
				if (s > 0)
				{
					DiamPitch.Text = StringsHelper.Format(Module1.Pi / s, "###0.0000");
				}
			}
			finally
			{
				eventArgs.Handled = KeyCode == 0;
			}

		}

		private void DiamPitch_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = ((int) eventArgs.KeyData) / 65536;
			try
			{
				double s = 0;
				//UPGRADE_TODO: (1065) Error handling statement (On Error Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Resume Next");
				s = Double.Parse(DiamPitch.Text);
				if (s > 0)
				{
					CircPitch.Text = StringsHelper.Format(Module1.Pi / s, "###0.0000");
				}
			}
            catch (Exception exc)
            {
                throw new Exception("Migration Exception: The following exception could be handled in a different way after the conversion: " + exc.Message);
            }
			finally
			{
				eventArgs.Handled = KeyCode == 0;
			}
		}


		private void PressureAngle_Leave(Object eventSender, EventArgs eventArgs)
		{
			double s = 0;
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				s = Double.Parse(PressureAngle.Text);
				if (s > 30)
				{
					PressureAngle.Text = "30";
				}

				if (s < 10)
				{
					PressureAngle.Text = "14";
				}
			}
			catch (Exception exc)
			{
				//NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}

		}

		private void TeethCount_Leave(Object eventSender, EventArgs eventArgs)
		{
			double s = 0;
			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			try
			{
				s = Double.Parse(TeethCount.Text);
				if (s > 200)
				{
					TeethCount.Text = "100";
					s = 100;

				}

				if (s < 5)
				{
					TeethCount.Text = "5";
					s = 5;
				}

				if (s > 0)
				{
					TeethCount.Text = Convert.ToInt32(s).ToString();
				}
			}
			catch (Exception exc)
			{
				//NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block");
			}
		}
		private void Form1_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}