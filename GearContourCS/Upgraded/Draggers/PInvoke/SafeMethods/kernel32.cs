using System.Runtime.InteropServices;

namespace GearContourSupport.PInvoke.SafeNative
{
	public static class kernel32
	{

		public static int GetCurrentProcessId()
		{
			return GearContourSupport.PInvoke.UnsafeNative.kernel32.GetCurrentProcessId();
		}
	}
}