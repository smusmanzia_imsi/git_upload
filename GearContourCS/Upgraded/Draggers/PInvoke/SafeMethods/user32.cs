using System.Runtime.InteropServices;

namespace GearContourSupport.PInvoke.SafeNative
{
	public static class user32
	{

		public static int BringWindowToTop(int hwnd)
		{
			return GearContourSupport.PInvoke.UnsafeNative.user32.BringWindowToTop(hwnd);
		}
		public static int FindWindow(ref string lpClassName, ref string lpWindowName)
		{
			return GearContourSupport.PInvoke.UnsafeNative.user32.FindWindow(ref lpClassName, ref lpWindowName);
		}
		public static int GetWindowLong(int hwnd, int nIndex)
		{
			return GearContourSupport.PInvoke.UnsafeNative.user32.GetWindowLong(hwnd, nIndex);
		}
		public static int SetWindowLong(int hwnd, int nIndex, int dwNewLong)
		{
			return GearContourSupport.PInvoke.UnsafeNative.user32.SetWindowLong(hwnd, nIndex, dwNewLong);
		}
	}
}