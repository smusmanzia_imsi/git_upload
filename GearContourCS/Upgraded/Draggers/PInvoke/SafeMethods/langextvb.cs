using System.Runtime.InteropServices;

namespace GearContourSupport.PInvoke.SafeNative
{
	public static class langextvb
	{

		public static object LoadLangBSTRString(ref string strModule, int nID, int wLanguage)
		{
			string tempRefParam = strModule;
			return GearContourSupport.PInvoke.UnsafeNative.langextvb.LoadLangBSTRString(ref tempRefParam, nID, wLanguage);
		}
	}
}