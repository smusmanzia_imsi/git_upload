using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using TCDotNetInterfaces;
using IMSIGX;

namespace CSharpMultiAddTool
{
    public class CSMultiAdd : ITurboCADTool
    {
        #region ITurboCADTool Members

        public string Description
        {
            get { return "Multi-Add Tool CSharp version"; }
        }

        public bool GetToolInfo(out TurboCADToolInfo ToolInfo)
        {
            ToolInfo = new TurboCADToolInfo();
            
            ToolInfo.CommandName = "Tools\nDotNet Tools\nMultiAdd Tool CS";
            ToolInfo.InternalCommand = "CMD_08822F26-A4CF-4B61-A07D-84CA3AFC62F1"; 
            ToolInfo.MenuCaption = "&MultiAdd Tool CS";
            ToolInfo.ToolbarName = "MultiAdd Tool CS";
            ToolInfo.ToolTip = "MultiAdd Tool CS";
            ToolInfo.bEnabled = true;
            ToolInfo.bWantsUpdates = true;

            System.Reflection.Assembly thisApp;
            System.IO.Stream file;


            thisApp = System.Reflection.Assembly.GetExecutingAssembly();

            file = thisApp.GetManifestResourceStream("CSharpMultiAddTool.MultiAddToolbar.MultiAddSmall.bmp");
            ToolInfo.ToolbarImage = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

            file = thisApp.GetManifestResourceStream("CSharpMultiAddTool.MultiAddToolbar.MultiAddLarge.bmp");
            ToolInfo.ToolbarImageL = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

            file = thisApp.GetManifestResourceStream("CSharpMultiAddTool.MultiAddToolbar.MultiAddSmallBW.bmp");
            ToolInfo.ToolbarImageBW = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

            file = thisApp.GetManifestResourceStream("CSharpMultiAddTool.MultiAddToolbar.MultiAddLargeBW.bmp");
            ToolInfo.ToolbarImageLBW = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

            return true;
        }


        public bool Initialize(Tool Context)
        {
            return true;
        }

        public bool Run(Tool Context)
        {
            IMSIGX.Application app = Context.Application;
            //MessageBox.Show(app.Name.ToString());
            IMSIGX.Drawing draw = app.ActiveDrawing;
            IMSIGX.Selection sel = draw.Selection;
            object varOpt = Type.Missing;
            // remove non-solids from the selection
            //MessageBox.Show("There are " + sel.Count.ToString() + " items selected");
            int count = sel.Count;
            object index;
            for (int i = count - 1; i >= 0; i--)
            {
                index = i;
                IMSIGX.Graphic gr = sel.get_Item(i); // sel.get_Item(ref index);
                if (gr.TypeByValue == IMSIGX.ImsiGraphicType.imsiText)
                {
                    //MessageBox.Show("Deselecting text for index " + i.ToString());
                    gr.Unselect();
                    draw.ActiveView.Refresh();
                }
                else
                {
                    //object propSolid = "Solid";
                    try
                    {
                        //MessageBox.Show("checking item " + i.ToString() + " to see if it's a solid");
                        IMSIGX.Properties props = gr.Properties;
                        IMSIGX.Property prop = props.get_Item("Solid");
                        object nonsolid = 0;
                        if (prop.Value == nonsolid)
                        {
                            gr.Unselect();
                            draw.ActiveView.Refresh();
                        }

                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("item " + i.ToString() + " threw exception: " + ex.Message);
                        gr.Unselect();
                        draw.ActiveView.Refresh();
                    }
                }
            }
            draw.ActiveView.Refresh();
            // make sure at least two solids are left in the selection
            if (sel.Count < 2)
            {
                MessageBox.Show("Please select two or more solids and try again");
                return true;
            }

            // now start adding the solids
            // first instantiate the boolean server object
            GXEXT.Boolean3D boolsrv;
            try
            {
                boolsrv = new GXEXT.Boolean3D(app);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error attempting to create boolean server. " + ex.Message);
                return true;
            }
            //MessageBox.Show(boolsrv.ToString());
            //add an Undo record
            IMSIGX.UndoRecord undo;
            try
            {
                undo = draw.AddUndoRecord("MultiAddCS");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error attempting to create Undo record. " + ex.Message);
                return true;
            }
            //MessageBox.Show(undo.ToString());
            // now try to add the first two selected solids
            //int ix = sel.Count - 1;
            //index = ix;
            //object index2 = ix - 1;
            int i1 = sel.Count - 1;
            int i2 = i1 - 1;
            IMSIGX.Graphic dummy1, dummy2;
            try
            {
                dummy1 = boolsrv.Add(sel.get_Item(i1), sel.get_Item(i2));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error attempting to add the first two solids. " + ex.Message);
                undo.Close();
                return true;
            }
            //MessageBox.Show(dummy1.ToString());
            // delete the original graphics for those solids
            try
            {
                undo.DeleteGraphic(sel.get_Item(i1));
                undo.DeleteGraphic(sel.get_Item(i2));
                draw.ActiveView.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error attempting to delete original two solids. " + ex.Message);
                undo.Close();
                return true;
            }
            // now process the remaining solids, adding them one at a time to the work-in-progress.
            if (sel.Count > 0)
            {

                count = sel.Count - 1;
                for (int j = count; j >= 0; j--)
                {
                    dummy2 = boolsrv.Add(dummy1, sel.get_Item(j));
                    dummy1 = dummy2;
                    undo.DeleteGraphic(sel.get_Item(j));
                    draw.ActiveView.Refresh();
                }
            }
            // AddGraphic requires either two integers or two Graphic references (before and after graphics). 
            // If it's possible you may not have *any* other graphics in the drawing, use nulls 
            // for the before and after, or the AddGraphic operation may fail silently.
            // Similarly, if there's just one graphic, use null for either the before or after graphic.)
            draw.Graphics.AddGraphic(dummy1, null, null);
            undo.AddGraphic(dummy1);
            undo.Close();
            // krc -- Leon, why can't I get the view to show everything unselected?
            sel.Clear(IMSIGX.ImsiGraphicFlags.imsiGfSelected);
            draw.UnselectAll();
            //draw.ActiveView.Refresh();
            draw.ActiveView.ZoomToExtents();
            draw.ActiveView.Refresh();
            return true;
        }

        public void Terminate(Tool Context)
        {
        }

        public bool UpdateToolStatus(Tool Context, ref bool Enabled, ref bool Checked)
        {
            IMSIGX.Application app = Context.Application;
            IMSIGX.Drawing draw = app.ActiveDrawing;
            IMSIGX.Selection sel = draw.Selection;
            Checked = false;
            if (sel.Count < 2)
                Enabled = false;
            else
                Enabled = true;
            return true;
        }

        #endregion
    }
}
