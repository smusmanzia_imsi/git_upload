VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PointIntoContour"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Const Eps = 0.0000000001

' Function return 0 - if point lies on Contour ,
' or 1 - if point lies inside  bounds of the contour
' or -1 - if point lies outside or on o bounds of the contour

Public Function PointInsideContour(Cont As Contour, xP#, yP#) As Integer
Dim TypeSeg$, xB#, yB#, xE#, yE#, xMid#, yMid#, xc#, yc#, L#, fi#, R#, Rot%
Dim NumInt%
    NumInt = 0
Dim xInt#, yInt#, xLoc#
Dim xInt1#, yInt1#, xInt2#, yInt2#

Dim nSegm As Long, i As Long
Dim sina#, cosa#
    sina = Sin(37 / 180 * Pi)
    cosa = Cos(37 / 180 * Pi)
Dim xP1#, yP1#
    xP1 = xP + 1000000 * cosa
    yP1 = yP + 1000000 * sina

    For i = 0 To Cont.NumSegm - 1
        With Cont.Segm(i)
            TypeSeg = .SegType
            xB = .xyBeg(0)
            yB = .xyBeg(1)
            xE = .xyEnd(0)
            yE = .xyEnd(1)
            L = .L
            fi = .Alp(0)
            xc = .xyCent(0)
            yc = .xyCent(1)
            xMid = .xyMid(0)
            yMid = .xyMid(1)
            R = Abs(.R)
            Rot = .Rot
        End With
        
        If TypeSeg = "Circle" Then'# NLS#'
Dim RR#
            RR = Sqr((xP - xc) ^ 2 + (yP - yc) ^ 2)
            If Abs(RR - R) < Eps Then
                PointInsideContour = 0
            End If
            If RR > R + Eps Then
                PointInsideContour = -1
            End If
            If RR < R - Eps Then
                PointInsideContour = 1
            End If
            Exit Function
        End If
        
        If TypeSeg = "Line" Then'# NLS#'
            If PointIntoSegmenti(xB, yB, xE, yE, xP, yP) = True Then
                PointInsideContour = 0
                Exit Function
            End If
            If Abs(xB - xP) < Eps And Abs(yB - yP) < Eps Then
                PointInsideContour = 0
                Exit Function
            End If
            
            If SegSegIntersection(xB, yB, xE, yE, xP, yP, xP1, yP1, xInt, yInt) = True Then
                If PointIntoSegmenti(xB, yB, xE, yE, xInt, yInt) = True Then
                    xLoc = (yInt - yP) * sina + (xInt - xP) * cosa
                    If xLoc > 0 Then
                        NumInt = NumInt + 1
                    End If
                End If
            End If
        End If
        
        If TypeSeg = "Arc" Then'# NLS#'
Dim Res1 As Boolean, Res2 As Boolean
            If PointOnArc(Cont.Segm(i), xP, yP) = True Then
                PointInsideContour = 0
                Exit Function
            End If
            If LineCircIntersection(xP, yP, 37 / 180 * Pi, xc, yc, R, xInt1, yInt1, xInt2, yInt2) = True Then
                xLoc = (yInt1 - yP) * sina + (xInt1 - xP) * cosa
                If PointOnArc(Cont.Segm(i), xInt1, yInt1) = True And xLoc > 0 Then
                    NumInt = NumInt + 1
                End If
                
                xLoc = (yInt2 - yP) * sina + (xInt2 - xP) * cosa
                If PointOnArc(Cont.Segm(i), xInt2, yInt2) = True And xLoc > 0 Then
                    NumInt = NumInt + 1
                End If
            End If
        End If
    Next i
    
    If CDbl(CDbl(NumInt) / 2) <> CDbl(Int(CDbl(NumInt) / 2)) Then
        PointInsideContour = 1
    Else
        PointInsideContour = -1
    End If

End Function



Private Function Angle(sinB As Double, cosB As Double) As Double
Dim Eps1#
        Eps1 = 0.0000000000001
        If Abs(cosB) < Eps1 Then
            If sinB > Eps1 Then
                Angle = Pi / 2
            Else
                Angle = 3 * Pi / 2
            End If
        Else
            If Abs(sinB) < Eps1 Then
                If cosB > Eps1 Then
                    Angle = 0
                Else
                    Angle = Pi
                End If
            Else
                If sinB >= 0 And cosB > 0 Then
                    Angle = Atn(sinB / cosB)
                Else
                    If sinB >= 0 And cosB < 0 Then
                        Angle = Pi + Atn(sinB / cosB)
                    Else
                        If sinB < 0 And cosB < 0 Then
                            Angle = Pi + Atn(sinB / cosB)
                        Else
                            If sinB < 0 And cosB > 0 Then
                                Angle = 2 * Pi + Atn(sinB / cosB)
                            End If
                        End If
                    End If
                End If
            End If
        End If
End Function


Private Function Sign(y As Double) As Double
    If Abs(y) < 0.000001 Then
        Sign = 0
        Exit Function
    End If
    If y < 0 Then
        Sign = -1#
        Exit Function
    End If
    If y > 0 Then
        Sign = 1#
        Exit Function
    End If
End Function

Private Function SysLine(a1#, b1#, d1#, a2#, b2#, d2#, xRes#, yRes#) As Boolean

Dim del#, delx#, dely#
On Error GoTo ErrorHandler

    del = a1 * b2 - a2 * b1
    delx = d1 * b2 - b1 * d2
    dely = a1 * d2 - a2 * d1
    If Abs(del) < Eps Then
        SysLine = False
        Exit Function
    End If
    xRes = delx / del
    yRes = dely / del
    SysLine = True
    Exit Function
ErrorHandler:
'    MsgBox Err.Description
    SysLine = False
End Function

' Function return 1 - if point lies inside line segment, or 0 - if point lies outside or on
' on bounds of line segment

Private Function PointIntoSegmenti(x1#, y1#, x2#, y2#, x#, y#) As Boolean
Dim dx#, dy#, L#, sina#, cosa#
            PointIntoSegmenti = False
            If Abs(x - x1) < Eps And Abs(y - y1) < Eps Then
                PointIntoSegmenti = False
                Exit Function
            End If
            If Abs(x - x2) < Eps And Abs(y - y2) < Eps Then
                PointIntoSegmenti = True
                Exit Function
            End If
Dim xLoc#, yLoc#
            dx = x2 - x1
            dy = y2 - y1
            L = Sqr(dx * dx + dy * dy)
            If L < Eps Then Exit Function
            sina = dy / L
            cosa = dx / L
            
            yLoc = (y - y1) * cosa - (x - x1) * sina
            xLoc = (y - y1) * sina + (x - x1) * cosa
            If Abs(yLoc) > 1 * Eps Then
                PointIntoSegmenti = False
                Exit Function
            End If
            If xLoc > 0 And xLoc < L Then
                PointIntoSegmenti = True
                Exit Function
            End If

End Function

   
Private Function SegSegIntersection(xB1#, yB1#, xE1#, yE1#, xB2#, yB2#, xE2#, yE2#, xInt#, yInt#) As Boolean
Dim a1#, b1#, d1#
Dim a2#, b2#, d2#
            
    a1 = -(yE1 - yB1)
    b1 = xE1 - xB1
    d1 = a1 * xB1 + b1 * yB1

    a2 = -(yE2 - yB2)
    b2 = xE2 - xB2
    d2 = a2 * xB2 + b2 * yB2

    If SysLine(a1, b1, d1, a2, b2, d2, xInt, yInt) = True Then
        If PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt, yInt) = True And PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt, yInt) = True Then
            SegSegIntersection = True
        Else
            SegSegIntersection = False
        End If
    Else
        SegSegIntersection = False
    End If
End Function


' Define the intersection points for Line and Circle
Private Function LineCircIntersection(x1 As Double, y1 As Double, fi As Double, xc As Double, yc As Double, R As Double, xInt1 As Double, yInt1 As Double, xInt2 As Double, yInt2 As Double) As Boolean
Dim sina#, cosa#
    sina = Sin(fi)
    cosa = Cos(fi)
Dim xcLoc#, ycLoc#
    xcLoc = (yc - y1) * sina + (xc - x1) * cosa
    ycLoc = (yc - y1) * cosa - (xc - x1) * sina
    If Abs(ycLoc) > Abs(R) + Eps Then
        LineCircIntersection = False
        Exit Function
    End If
Dim t#
    t = Sqr(Abs(R * R - ycLoc * ycLoc))
Dim xLoc#, yLoc#
    yLoc = 0
    xLoc = xcLoc + t
    xInt1 = x1 + xLoc * cosa - yLoc * sina
    yInt1 = y1 + xLoc * sina + yLoc * cosa

    xLoc = xcLoc - t
    xInt2 = x1 + xLoc * cosa - yLoc * sina
    yInt2 = y1 + xLoc * sina + yLoc * cosa
    LineCircIntersection = True
End Function


' Define if point lies on Arc
Private Function PointOnArc(Segm As Segment, xP As Double, yP As Double) As Boolean
    
Dim xB#, yB, xE#, yE#, xc#, yc#, R#, xMid#, yMid#, Rot%
    xB = Segm.xyBeg(0)
    yB = Segm.xyBeg(1)
    xE = Segm.xyEnd(0)
    yE = Segm.xyEnd(1)
    xc = Segm.xyCent(0)
    yc = Segm.xyCent(1)
    xMid = Segm.xyMid(0)
    yMid = Segm.xyMid(1)
    R = Abs(Segm.R)
    Rot = Segm.Rot
    
Dim Rp#
    Rp = Sqr((xP - xc) ^ 2 + (yP - yc) ^ 2)
    If Abs(Rp - R) > Eps Then
        PointOnArc = False
        Exit Function
    End If
    
Dim sina#, cosa#, xELoc#, yELoc#, xPLoc#, yPLoc#, xMidLoc#, yMidLoc#
    sina = (yB - yc) / R
    cosa = (xB - xc) / R
    xELoc = (yE - yc) * sina + (xE - xc) * cosa
    yELoc = (yE - yc) * cosa - (xE - xc) * sina
    xPLoc = (yP - yc) * sina + (xP - xc) * cosa
    yPLoc = (yP - yc) * cosa - (xP - xc) * sina
    xMidLoc = (yMid - yc) * sina + (xMid - xc) * cosa
    yMidLoc = (yMid - yc) * cosa - (xMid - xc) * sina

Dim AlpELoc#, AlpPLoc#
    sina = yELoc / R
    cosa = xELoc / R
    AlpELoc = Angle(sina, cosa)
    
    sina = yPLoc / R
    cosa = xPLoc / R
    AlpPLoc = Angle(sina, cosa)
    
    If Rot = -1 Then
        AlpELoc = 2 * Pi - AlpELoc
        AlpPLoc = 2 * Pi - AlpPLoc
    End If
    
    If AlpPLoc > Eps And AlpPLoc < AlpELoc + Eps Then
        PointOnArc = True
    Else
        PointOnArc = False
    End If
End Function
