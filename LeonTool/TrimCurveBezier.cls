VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TrimCurveBezier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Type CurveBezSegment
    xBeg As Double
    yBeg As Double
    xEnd As Double
    yEnd As Double
    xDirBeg As Double
    yDirBeg As Double
    xDirEnd As Double
    yDirEnd As Double
    dxBeg As Double
    dyBeg As Double
    ddxBeg As Double
    ddyBeg As Double
    dxEnd As Double
    dyEnd As Double
    ddxEnd As Double
    ddyEnd As Double
    jBeg As Long
    jEnd As Long
End Type


Private Type CurveBezContour
    NumSegm As Long
    Segm() As CurveBezSegment
End Type


Public Function TrimCurve(GrCurve As Graphic, xP1#, yP1#, xP2#, yP2#, Dir%) As Graphic
    Set TrimCurve = Nothing
On Error GoTo ErrorEnd

Dim ContCosm As Contour
Dim GrCurveDup As Graphic
    Set GrCurveDup = GrCurve.Duplicate
    GrCurveDup.Properties("PenWidth") = 0
    Grs.Remove GrCurveDup.Index
Dim GrChild As Graphic
Dim ChildCount As Long
Dim i As Long

    ChildCount = GrCurveDup.Graphics.Count
    For i = 0 To ChildCount - 1
        Set GrChild = GrCurveDup.Graphics(i)
        If GrChild.Type = GRAPHICTYPE Then
            Exit For
        End If
    Next i
    
    If GrChild.Type <> GRAPHICTYPE Then
        Set TrimCurve = Nothing
        GrCurveDup.Delete
        Set GrCurveDup = Nothing
        Exit Function
    End If
    
Dim Vers As Vertices
Dim nV As Long
Dim xB#, yB#, xE#, yE#, l#, Alp#
    Set Vers = GrChild.Vertices
    Vers.UseWorldCS = False
    nV = Vers.Count
Dim Segm As Segment
ReDim ContCosm.Segm(nV - 1)
    ContCosm.NumSegm = 0
    
    For i = 0 To nV - 2
        xB = Vers(i).x
        yB = Vers(i).y
        xE = Vers(i + 1).x
        yE = Vers(i + 1).y
        Call LineSegmPar(xB, yB, xE, yE, l, Alp)
        With Segm
            .SegType = "Line"'# NLS#'
            .xyBeg(0) = xB
            .xyBeg(1) = yB
            .xyEnd(0) = xE
            .xyEnd(1) = yE
            .l = l
            .Alp(0) = Alp
        End With
        ContCosm.Segm(ContCosm.NumSegm) = Segm
        ContCosm.NumSegm = ContCosm.NumSegm + 1
    Next i
    
Dim xBeg#, yBeg#, xEnd#, yEnd#
Dim xDirBeg#, yDirBeg#, xDirEnd#, yDirEnd#

Dim ContClosed As Boolean
    xBeg = ContCosm.Segm(0).xyBeg(0)
    yBeg = ContCosm.Segm(0).xyBeg(1)
    xEnd = ContCosm.Segm(ContCosm.NumSegm - 1).xyEnd(0)
    yEnd = ContCosm.Segm(ContCosm.NumSegm - 1).xyEnd(1)
    ContClosed = False
    If Abs(xBeg - xEnd) < Eps And Abs(yBeg - yEnd) < Eps Then
        ContClosed = True
    End If
    If ContClosed = False Then
        Dir = 1
    End If
    If ContClosed = True And Abs(Dir) <> 1 Then
        Dir = 1
    End If
    
    If Abs(xBeg - xP1) < Eps And Abs(yBeg - yP1) < Eps And Abs(xEnd - xP2) < Eps And Abs(yEnd - yP2) < Eps Then
        GrCurveDup.Delete
        Set TrimCurve = GrCurve.Duplicate
        Exit Function
    End If
    If Abs(xBeg - xP2) < Eps And Abs(yBeg - yP2) < Eps And Abs(xEnd - xP1) < Eps And Abs(yEnd - yP1) < Eps Then
        GrCurveDup.Delete
        Set TrimCurve = GrCurve.Duplicate
        Exit Function
    End If
    
    If ContClosed = True And Abs(xP1 - xP2) < Eps And Abs(yP1 - yP2) < Eps Then
        GrCurveDup.Delete
        Set TrimCurve = GrCurve.Duplicate
        Exit Function
    End If
    
' Initialize Bezier contour
Dim CurvBez As CurveBezContour
    Call InitializeBezierContour(GrCurveDup, ContCosm, CurvBez)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
' Point 1
' Define number of Bezier segment on which lies point -(xP1,yP1)
Dim iSegBez1 As Long
Dim iSegCosm1 As Long
    Call DefNumBezierSegment(xP1, yP1, ContCosm, CurvBez, iSegBez1, iSegCosm1)
' Define value of t- parameter of Bezier segment for point -(xP1,yP1)
Dim t1#
    t1 = t_parameter(xP1, yP1, ContCosm, CurvBez, iSegBez1, iSegCosm1)
' Define parameters of bezier curve segment in point - P1
Dim xP1New#, yP1New#
Dim dxP1#, dyP1#
    Call DefineCurvParAtPoint(CurvBez, iSegBez1, t1, xP1New, yP1New, dxP1, dyP1)

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
' Point 2
' Define number of Bezier segment on which lies point -(xP2,yP2)
Dim iSegBez2 As Long
Dim iSegCosm2 As Long
    Call DefNumBezierSegment(xP2, yP2, ContCosm, CurvBez, iSegBez2, iSegCosm2)
' Define value of t- parameter of Bezier segment for point -(xP2,yP2)
Dim t2#
    t2 = t_parameter(xP2, yP2, ContCosm, CurvBez, iSegBez2, iSegCosm2)
' Define parameters of bezier curve segment in point - P2
Dim xP2New#, yP2New#
Dim dxP2#, dyP2#
    Call DefineCurvParAtPoint(CurvBez, iSegBez2, t2, xP2New, yP2New, dxP2, dyP2)
    
'####################################################################
'####################################################################
'####################################################################
    
Dim GrCurvNew As Graphic
Dim t#
Dim iVBeg As Long
Dim iVDirBeg As Long
Dim iVDirEnd As Long
Dim xBegNew#, yBegNew#
Dim xEndNew#, yEndNew#
Dim xDirBegNew#, yDirBegNew#
Dim xDirEndNew#, yDirEndNew#
Dim xP#, yP#
Dim dxP#, dyP#
    If iSegBez1 <> iSegBez2 And Dir = 1 Then
        If iSegBez1 > iSegBez2 Then
        
            i = iSegBez1
            iSegBez1 = iSegBez2
            iSegBez2 = i
            
            t = t1
            t1 = t2
            t2 = t
            
            xP = xP1New
            xP1New = xP2New
            xP2New = xP
            dxP = dxP1
            dxP1 = dxP2
            dxP2 = dxP
           
            yP = yP1New
            yP1New = yP2New
            yP2New = yP
            dyP = dyP1
            dyP1 = dyP2
            dyP2 = dyP
            
        End If

        
        xBeg = CurvBez.Segm(iSegBez1).xBeg
        yBeg = CurvBez.Segm(iSegBez1).yBeg
        Set GrCurvNew = Grs.AddCurveBezier(xBeg, yBeg, 0)
        GrCurvNew.Properties("PenColor") = RGB(255, 0, 0)
        GrCurvNew.Properties("PenWidth") = 0.02
        Set Vers = GrCurvNew.Vertices
        For i = iSegBez1 To iSegBez2
            xEnd = CurvBez.Segm(i).xEnd
            yEnd = CurvBez.Segm(i).yEnd
            Vers.Add xEnd, yEnd, 0
        Next i
        
        GrCurvNew.Draw
        
        For i = iSegBez1 To iSegBez2
            With CurvBez.Segm(i)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd
                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With
            
            iVBeg = (i - iSegBez1) * 3
            iVDirBeg = iVBeg + 1
            iVDirEnd = iVBeg + 2
            Vers(iVDirBeg).x = xDirBeg
            Vers(iVDirBeg).y = yDirBeg
            Vers(iVDirEnd).x = xDirEnd
            Vers(iVDirEnd).y = yDirEnd
        Next i
        
        '+++++++++++++++++++++++++++++++++++++++++++++++
        ' Correct First bezier segment
        With CurvBez.Segm(iSegBez1)
            xBeg = .xBeg
            yBeg = .yBeg
            xEnd = .xEnd
            yEnd = .yEnd
            
            xDirBeg = .xDirBeg
            yDirBeg = .yDirBeg
            xDirEnd = .xDirEnd
            yDirEnd = .yDirEnd
        End With
        
        xBegNew = xP1New
        yBegNew = yP1New
        xEndNew = xEnd
        yEndNew = yEnd
        
        xDirBegNew = (dxP1 + 3 * xBegNew) / 3
        yDirBegNew = (dyP1 + 3 * yBegNew) / 3
        xDirEndNew = xDirEnd
        yDirEndNew = yDirEnd
        
        With GrCurvNew.Vertices
            .Item(0).x = xBegNew
            .Item(0).y = yBegNew
            .Item(1).x = xBegNew + (xDirBegNew - xBegNew) * (1 - t1)
            .Item(1).y = yBegNew + (yDirBegNew - yBegNew) * (1 - t1)
            .Item(2).x = xEndNew + (xDirEndNew - xEndNew) * (1 - t1)
            .Item(2).y = yEndNew + (yDirEndNew - yEndNew) * (1 - t1)
        End With
        '+++++++++++++++++++++++++++++++++++++++++++++++
        
        '+++++++++++++++++++++++++++++++++++++++++++++++
        ' Correct Last bezier segment
        With CurvBez.Segm(iSegBez2)
            xBeg = .xBeg
            yBeg = .yBeg
            xEnd = .xEnd
            yEnd = .yEnd
            
            xDirBeg = .xDirBeg
            yDirBeg = .yDirBeg
            xDirEnd = .xDirEnd
            yDirEnd = .yDirEnd
        End With
        
        xBegNew = xBeg
        yBegNew = yBeg
        xEndNew = xP2New
        yEndNew = yP2New
        
        xDirBegNew = xDirBeg
        yDirBegNew = yDirBeg
        xDirEndNew = (-dxP2 + 3 * xEndNew) / 3 ' -dxP   !!!!!
        yDirEndNew = (-dyP2 + 3 * yEndNew) / 3 ' -dyP   !!!!!
        
        nV = GrCurvNew.Vertices.Count
        With GrCurvNew.Vertices
            .Item(nV - 3).x = xBegNew + (xDirBegNew - xBegNew) * t2
            .Item(nV - 3).y = yBegNew + (yDirBegNew - yBegNew) * t2
            .Item(nV - 2).x = xEndNew + (xDirEndNew - xEndNew) * t2
            .Item(nV - 2).y = yEndNew + (yDirEndNew - yEndNew) * t2
            .Item(nV - 1).x = xEndNew
            .Item(nV - 1).y = yEndNew
        End With
        GrCurvNew.Draw
        '+++++++++++++++++++++++++++++++++++++++++++++++
    End If ' If iSegBez1 <> iSegBez2 And Dir = 1
    '############################################################
    '############################################################
    '############################################################
    '############################################################
    If iSegBez1 <> iSegBez2 And Dir = -1 Then
        If iSegBez1 < iSegBez2 Then
        
            i = iSegBez2
            iSegBez2 = iSegBez1
            iSegBez1 = i
            
            t = t2
            t2 = t1
            t1 = t
            
            xP = xP2New
            xP2New = xP1New
            xP1New = xP
            dxP = dxP2
            dxP2 = dxP1
            dxP1 = dxP
           
            yP = yP2New
            yP2New = yP1New
            yP1New = yP
            dyP = dyP2
            dyP2 = dyP1
            dyP1 = dyP
            
        End If

        
        xBeg = CurvBez.Segm(iSegBez1).xBeg
        yBeg = CurvBez.Segm(iSegBez1).yBeg
        Set GrCurvNew = Grs.AddCurveBezier(xBeg, yBeg, 0)
        GrCurvNew.Properties("PenColor") = RGB(255, 0, 0)
        GrCurvNew.Properties("PenWidth") = 0.02
        Set Vers = GrCurvNew.Vertices
        For i = iSegBez1 To CurvBez.NumSegm - 1
            xEnd = CurvBez.Segm(i).xEnd
            yEnd = CurvBez.Segm(i).yEnd
            Vers.Add xEnd, yEnd, 0
        Next i
        For i = 0 To iSegBez2
            xEnd = CurvBez.Segm(i).xEnd
            yEnd = CurvBez.Segm(i).yEnd
            Vers.Add xEnd, yEnd, 0
        Next i
        
        GrCurvNew.Draw
        
        For i = iSegBez1 To CurvBez.NumSegm - 1
            With CurvBez.Segm(i)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd
                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With
            
            iVBeg = (i - iSegBez1) * 3
            iVDirBeg = iVBeg + 1
            iVDirEnd = iVBeg + 2
            Vers(iVDirBeg).x = xDirBeg
            Vers(iVDirBeg).y = yDirBeg
            Vers(iVDirEnd).x = xDirEnd
            Vers(iVDirEnd).y = yDirEnd
        Next i
        
        For i = 0 To iSegBez2
            With CurvBez.Segm(i)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd
                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With
            
            iVBeg = (i + (CurvBez.NumSegm - iSegBez1)) * 3
            iVDirBeg = iVBeg + 1
            iVDirEnd = iVBeg + 2
            Vers(iVDirBeg).x = xDirBeg
            Vers(iVDirBeg).y = yDirBeg
            Vers(iVDirEnd).x = xDirEnd
            Vers(iVDirEnd).y = yDirEnd
        Next i
        
        '+++++++++++++++++++++++++++++++++++++++++++++++
        ' Correct First bezier segment
        With CurvBez.Segm(iSegBez1)
            xBeg = .xBeg
            yBeg = .yBeg
            xEnd = .xEnd
            yEnd = .yEnd
            
            xDirBeg = .xDirBeg
            yDirBeg = .yDirBeg
            xDirEnd = .xDirEnd
            yDirEnd = .yDirEnd
        End With
        
        xBegNew = xP1New
        yBegNew = yP1New
        xEndNew = xEnd
        yEndNew = yEnd
        
        xDirBegNew = (dxP1 + 3 * xBegNew) / 3
        yDirBegNew = (dyP1 + 3 * yBegNew) / 3
        xDirEndNew = xDirEnd
        yDirEndNew = yDirEnd
        
        With GrCurvNew.Vertices
            .Item(0).x = xBegNew
            .Item(0).y = yBegNew
            .Item(1).x = xBegNew + (xDirBegNew - xBegNew) * (1 - t1)
            .Item(1).y = yBegNew + (yDirBegNew - yBegNew) * (1 - t1)
            .Item(2).x = xEndNew + (xDirEndNew - xEndNew) * (1 - t1)
            .Item(2).y = yEndNew + (yDirEndNew - yEndNew) * (1 - t1)
        End With
        '+++++++++++++++++++++++++++++++++++++++++++++++
        
        '+++++++++++++++++++++++++++++++++++++++++++++++
        ' Correct Last bezier segment
        With CurvBez.Segm(iSegBez2)
            xBeg = .xBeg
            yBeg = .yBeg
            xEnd = .xEnd
            yEnd = .yEnd
            
            xDirBeg = .xDirBeg
            yDirBeg = .yDirBeg
            xDirEnd = .xDirEnd
            yDirEnd = .yDirEnd
        End With
        
        xBegNew = xBeg
        yBegNew = yBeg
        xEndNew = xP2New
        yEndNew = yP2New
        
        xDirBegNew = xDirBeg
        yDirBegNew = yDirBeg
        xDirEndNew = (-dxP2 + 3 * xEndNew) / 3 ' -dxP   !!!!!
        yDirEndNew = (-dyP2 + 3 * yEndNew) / 3 ' -dyP   !!!!!
        
        nV = GrCurvNew.Vertices.Count
        With GrCurvNew.Vertices
            .Item(nV - 3).x = xBegNew + (xDirBegNew - xBegNew) * t2
            .Item(nV - 3).y = yBegNew + (yDirBegNew - yBegNew) * t2
            .Item(nV - 2).x = xEndNew + (xDirEndNew - xEndNew) * t2
            .Item(nV - 2).y = yEndNew + (yDirEndNew - yEndNew) * t2
            .Item(nV - 1).x = xEndNew
            .Item(nV - 1).y = yEndNew
        End With
        GrCurvNew.Draw
        '+++++++++++++++++++++++++++++++++++++++++++++++
    End If ' If iSegBez1 <> iSegBez2 And Dir = -1
        
    '############################################################
    '############################################################
    '############################################################
    '############################################################
    If iSegBez1 = iSegBez2 And Dir <> -1 Then
        If t1 < t2 Then
        Else
            
            i = iSegBez1
            iSegBez1 = iSegBez2
            iSegBez2 = i
            
            t = t1
            t1 = t2
            t2 = t
            
            xP = xP1New
            xP1New = xP2New
            xP2New = xP
            dxP = dxP1
            dxP1 = dxP2
            dxP2 = dxP
           
            yP = yP1New
            yP1New = yP2New
            yP2New = yP
            dyP = dyP1
            dyP1 = dyP2
            dyP2 = dyP
            
        End If
        
        xBeg = CurvBez.Segm(iSegBez1).xBeg
        yBeg = CurvBez.Segm(iSegBez1).yBeg
        Set GrCurvNew = Grs.AddCurveBezier(xBeg, yBeg, 0)
        GrCurvNew.Properties("PenColor") = RGB(255, 0, 0)
        GrCurvNew.Properties("PenWidth") = 0.02
        Set Vers = GrCurvNew.Vertices
        For i = iSegBez1 To iSegBez2
            xEnd = CurvBez.Segm(i).xEnd
            yEnd = CurvBez.Segm(i).yEnd
            Vers.Add xEnd, yEnd, 0
        Next i
        
        GrCurvNew.Draw
        
        For i = iSegBez1 To iSegBez2
            With CurvBez.Segm(i)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd
                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With
            
            iVBeg = (i - iSegBez1) * 3
            iVDirBeg = iVBeg + 1
            iVDirEnd = iVBeg + 2
            Vers(iVDirBeg).x = xDirBeg
            Vers(iVDirBeg).y = yDirBeg
            Vers(iVDirEnd).x = xDirEnd
            Vers(iVDirEnd).y = yDirEnd
        Next i
        
        '+++++++++++++++++++++++++++++++++++++++++++++++
        ' Correct First bezier segment
        With CurvBez.Segm(iSegBez1)
            xBeg = .xBeg
            yBeg = .yBeg
            xEnd = .xEnd
            yEnd = .yEnd
            
            xDirBeg = .xDirBeg
            yDirBeg = .yDirBeg
            xDirEnd = .xDirEnd
            yDirEnd = .yDirEnd
        End With
        
        xBegNew = xP1New
        yBegNew = yP1New
        xEndNew = xEnd
        yEndNew = yEnd
        
        xDirBegNew = (dxP1 + 3 * xBegNew) / 3
        yDirBegNew = (dyP1 + 3 * yBegNew) / 3
        xDirEndNew = xDirEnd
        yDirEndNew = yDirEnd
        
        With GrCurvNew.Vertices
            .Item(0).x = xBegNew
            .Item(0).y = yBegNew
            .Item(1).x = xBegNew + (xDirBegNew - xBegNew) * (1 - t1)
            .Item(1).y = yBegNew + (yDirBegNew - yBegNew) * (1 - t1)
            .Item(2).x = xEndNew + (xDirEndNew - xEndNew) * (1 - t1)
            .Item(2).y = yEndNew + (yDirEndNew - yEndNew) * (1 - t1)
        End With
        '+++++++++++++++++++++++++++++++++++++++++++++++
        
        '+++++++++++++++++++++++++++++++++++++++++++++++
        ' Correct Last bezier segment
        With CurvBez.Segm(iSegBez2)
            xBeg = xBegNew
            yBeg = yBegNew
            xEnd = .xEnd
            yEnd = .yEnd
            
            xDirBeg = xDirBegNew
            yDirBeg = yDirBegNew
            xDirEnd = .xDirEnd
            yDirEnd = .yDirEnd
        End With
        
        xBegNew = xBeg
        yBegNew = yBeg
        xEndNew = xP2New
        yEndNew = yP2New
        
        xDirBegNew = xDirBeg
        yDirBegNew = yDirBeg
        xDirEndNew = (-dxP2 + 3 * xEndNew) / 3 ' -dxP   !!!!!
        yDirEndNew = (-dyP2 + 3 * yEndNew) / 3 ' -dyP   !!!!!
        
        nV = GrCurvNew.Vertices.Count
        With GrCurvNew.Vertices
            .Item(nV - 3).x = xBegNew + (xDirBegNew - xBegNew) * (t2 - t1)
            .Item(nV - 3).y = yBegNew + (yDirBegNew - yBegNew) * (t2 - t1)
            .Item(nV - 2).x = xEndNew + (xDirEndNew - xEndNew) * (t2 - t1)
            .Item(nV - 2).y = yEndNew + (yDirEndNew - yEndNew) * (t2 - t1)
            .Item(nV - 1).x = xEndNew
            .Item(nV - 1).y = yEndNew
        End With
        GrCurvNew.Draw
        '+++++++++++++++++++++++++++++++++++++++++++++++
    End If ' If iSegBez1 = iSegBez2 And dir<>-1
    
    '############################################################
    '############################################################
    '############################################################
    '############################################################
    If iSegBez1 = iSegBez2 And Dir = -1 Then
        If t2 < t1 Then
        Else
            
            i = iSegBez1
            iSegBez1 = iSegBez2
            iSegBez2 = i
            
            t = t1
            t1 = t2
            t2 = t
            
            xP = xP1New
            xP1New = xP2New
            xP2New = xP
            dxP = dxP1
            dxP1 = dxP2
            dxP2 = dxP
           
            yP = yP1New
            yP1New = yP2New
            yP2New = yP
            dyP = dyP1
            dyP1 = dyP2
            dyP2 = dyP
            
        End If
        
        xBeg = CurvBez.Segm(iSegBez1).xBeg
        yBeg = CurvBez.Segm(iSegBez1).yBeg
        Set GrCurvNew = Grs.AddCurveBezier(xBeg, yBeg, 0)
        GrCurvNew.Properties("PenColor") = RGB(255, 0, 0)
        GrCurvNew.Properties("PenWidth") = 0.02
        Set Vers = GrCurvNew.Vertices
        For i = iSegBez1 To CurvBez.NumSegm - 1
            xEnd = CurvBez.Segm(i).xEnd
            yEnd = CurvBez.Segm(i).yEnd
            Vers.Add xEnd, yEnd, 0
        Next i
        
        ' Add Last segments
        
        For i = 0 To iSegBez2
            xEnd = CurvBez.Segm(i).xEnd
            yEnd = CurvBez.Segm(i).yEnd
            Vers.Add xEnd, yEnd, 0
        Next i
        
        GrCurvNew.Draw
        
        
        For i = iSegBez1 To CurvBez.NumSegm - 1
            With CurvBez.Segm(i)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd
                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With
            
            iVBeg = (i - iSegBez1) * 3
            iVDirBeg = iVBeg + 1
            iVDirEnd = iVBeg + 2
            Vers(iVDirBeg).x = xDirBeg
            Vers(iVDirBeg).y = yDirBeg
            Vers(iVDirEnd).x = xDirEnd
            Vers(iVDirEnd).y = yDirEnd
        Next i
        
'        GrCurvNew.Draw
        
        For i = 0 To iSegBez2
            With CurvBez.Segm(i)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd
                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With
            iVBeg = (i + CurvBez.NumSegm - iSegBez2) * 3
            iVDirBeg = iVBeg + 1
            iVDirEnd = iVBeg + 2
            Vers(iVDirBeg).x = xDirBeg
            Vers(iVDirBeg).y = yDirBeg
            Vers(iVDirEnd).x = xDirEnd
            Vers(iVDirEnd).y = yDirEnd
        Next i
 '       GrCurvNew.Draw
        
        '+++++++++++++++++++++++++++++++++++++++++++++++
        ' Correct First bezier segment
        With CurvBez.Segm(iSegBez1)
            xBeg = .xBeg
            yBeg = .yBeg
            xEnd = .xEnd
            yEnd = .yEnd
            
            xDirBeg = .xDirBeg
            yDirBeg = .yDirBeg
            xDirEnd = .xDirEnd
            yDirEnd = .yDirEnd
        End With
        
        xBegNew = xP1New
        yBegNew = yP1New
        xEndNew = xEnd
        yEndNew = yEnd
        
        xDirBegNew = (dxP1 + 3 * xBegNew) / 3
        yDirBegNew = (dyP1 + 3 * yBegNew) / 3
        xDirEndNew = xDirEnd
        yDirEndNew = yDirEnd
        
        With GrCurvNew.Vertices
            .Item(0).x = xBegNew
            .Item(0).y = yBegNew
            .Item(1).x = xBegNew + (xDirBegNew - xBegNew) * (1 - t1)
            .Item(1).y = yBegNew + (yDirBegNew - yBegNew) * (1 - t1)
            .Item(2).x = xEndNew + (xDirEndNew - xEndNew) * (1 - t1)
            .Item(2).y = yEndNew + (yDirEndNew - yEndNew) * (1 - t1)
        End With
        '+++++++++++++++++++++++++++++++++++++++++++++++
        
        '+++++++++++++++++++++++++++++++++++++++++++++++
        ' Correct Last bezier segment
        With CurvBez.Segm(iSegBez2)
            xBeg = .xBeg
            yBeg = .yBeg
            xEnd = .xEnd
            yEnd = .yEnd
            
            xDirBeg = .xDirBeg
            yDirBeg = .yDirBeg
            xDirEnd = .xDirEnd
            yDirEnd = .yDirEnd
        End With
        
        xBegNew = xBeg
        yBegNew = yBeg
        xEndNew = xP2New
        yEndNew = yP2New
        
        xDirBegNew = xDirBeg
        yDirBegNew = yDirBeg
        xDirEndNew = (-dxP2 + 3 * xEndNew) / 3  ' -dxP   !!!!!
        yDirEndNew = (-dyP2 + 3 * yEndNew) / 3 ' -dyP   !!!!!
        
        nV = GrCurvNew.Vertices.Count
        With GrCurvNew.Vertices
            .Item(nV - 3).x = xBegNew + (xDirBegNew - xBegNew) * t2
            .Item(nV - 3).y = yBegNew + (yDirBegNew - yBegNew) * t2
            .Item(nV - 2).x = xEndNew + (xDirEndNew - xEndNew) * t2
            .Item(nV - 2).y = yEndNew + (yDirEndNew - yEndNew) * t2
            .Item(nV - 1).x = xEndNew
            .Item(nV - 1).y = yEndNew
        End With
        GrCurvNew.Draw
        '+++++++++++++++++++++++++++++++++++++++++++++++
RRR:
    End If ' If iSegBez1 = iSegBez2 And dir=-1
    
    
    Set TrimCurve = GrCurvNew
    
    If Not GrCurveDup Is Nothing Then
        GrCurveDup.Delete
    End If
    Set GrCurveDup = Nothing

    Exit Function
ErrorEnd:

    Set TrimCurve = Nothing
    If Not GrCurveDup Is Nothing Then
        GrCurveDup.Delete
    End If
    Set GrCurveDup = Nothing
    
    If Not GrCurvNew Is Nothing Then
        On Error Resume Next
        Grs.Remove GrCurvNew.Index
        On Error Resume Next
        GrCurvNew.Delete
        Set GrCurvNew = Nothing
    End If

End Function



' Define the length and angle of line segment
Private Sub LineSegmPar(x0 As Double, y0 As Double, x1 As Double, y1 As Double, l As Double, Alp As Double)
Dim sina#, cosa#
    l = Sqr((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0))
    If Abs(l) < Eps Then
        l = 0
        Alp = 0
        Exit Sub
    End If
    sina = (y1 - y0) / l
    cosa = (x1 - x0) / l
    Alp = Angle(sina, cosa)
End Sub

Private Function Angle(sinB As Double, cosB As Double) As Double
Dim Eps1#
        Eps1 = 0.0000000000001
        If Abs(cosB) < Eps1 Then
            If sinB > Eps1 Then
                Angle = Pi / 2
            Else
                Angle = 3 * Pi / 2
            End If
        Else
            If Abs(sinB) < Eps1 Then
                If cosB > Eps1 Then
                    Angle = 0
                Else
                    Angle = Pi
                End If
            Else
                If sinB >= 0 And cosB > 0 Then
                    Angle = Atn(sinB / cosB)
                Else
                    If sinB >= 0 And cosB < 0 Then
                        Angle = Pi + Atn(sinB / cosB)
                    Else
                        If sinB < 0 And cosB < 0 Then
                            Angle = Pi + Atn(sinB / cosB)
                        Else
                            If sinB < 0 And cosB > 0 Then
                                Angle = 2 * Pi + Atn(sinB / cosB)
                            End If
                        End If
                    End If
                End If
            End If
        End If
End Function


Private Function Sign(y As Double) As Double
    If Abs(y) < 0.000001 Then
        Sign = 0
        Exit Function
    End If
    If y < 0 Then
        Sign = -1#
        Exit Function
    End If
    If y > 0 Then
        Sign = 1#
        Exit Function
    End If
End Function



Private Sub InitializeBezierContour(GrBezier As Graphic, ContCosm As Contour, ContCurvBez As CurveBezContour)
'Begin Initialize Bezier contour
Dim i As Long
Dim nV As Long
Dim Vers As Vertices
    Set Vers = GrBezier.Vertices
    nV = Vers.Count
Dim nBase As Long
    nBase = (nV + 2) / 3
    
ReDim ContCurvBez.Segm(nBase)
    ContCurvBez.NumSegm = 0
Dim iV As Long, iVNext As Long
Dim iDirB As Long, iDirE As Long
Dim xBeg#, yBeg#, xEnd#, yEnd#
Dim xDirBeg#, yDirBeg#, xDirEnd#, yDirEnd#
Dim dxBeg#, dyBeg#, dxEnd#, dyEnd#
Dim ddxBeg#, ddyBeg#, ddxEnd#, ddyEnd#
Dim Segm As CurveBezSegment
    
    For i = 0 To nBase - 2
        iV = i * 3
        iVNext = (i + 1) * 3
        xBeg = Vers(iV).x
        yBeg = Vers(iV).y
        xEnd = Vers(iVNext).x
        yEnd = Vers(iVNext).y
        
        iDirB = iV + 1
        xDirBeg = Vers(iDirB).x
        yDirBeg = Vers(iDirB).y
        iDirE = iVNext - 1
        xDirEnd = Vers(iDirE).x
        yDirEnd = Vers(iDirE).y
        
        dxBeg = -3 * xBeg + 3 * xDirBeg
        dyBeg = -3 * yBeg + 3 * yDirBeg
    
        ddxBeg = 6 * xBeg - 12 * xDirBeg + 6 * xDirEnd
        ddyBeg = 6 * yBeg - 12 * yDirBeg + 6 * yDirEnd
    
    
        dxEnd = -3 * xDirEnd + 3 * xEnd
        dyEnd = -3 * yDirEnd + 3 * yEnd

        ddxEnd = 6 * xDirBeg - 12 * xDirEnd + 6 * xEnd
        ddyEnd = 6 * yDirBeg - 12 * yDirEnd + 6 * yEnd
        
        With Segm
            .xBeg = xBeg
            .yBeg = yBeg
            .xEnd = xEnd
            .yEnd = yEnd
            
            .xDirBeg = xDirBeg
            .yDirBeg = yDirBeg
            .xDirEnd = xDirEnd
            .yDirEnd = yDirEnd
            
            .dxBeg = dxBeg
            .dyBeg = dyBeg
            .dxEnd = dxEnd
            .dyEnd = dyEnd

            .ddxBeg = ddxBeg
            .ddyBeg = ddyBeg
            .ddxEnd = ddxEnd
            .ddyEnd = ddyEnd
        End With
        
        ContCurvBez.Segm(ContCurvBez.NumSegm) = Segm
        ContCurvBez.NumSegm = ContCurvBez.NumSegm + 1
        
    Next i
    
Dim xB#, yB#, xE#, yE#, l#, Alp#
Dim j As Long
Dim j0 As Long
Dim jBeg As Long, jEnd As Long
    For i = 0 To ContCurvBez.NumSegm - 1
        With ContCurvBez.Segm(i)
            xBeg = .xBeg
            yBeg = .yBeg
            xEnd = .xEnd
            yEnd = .yEnd
        End With
        jBeg = -1
        jEnd = -1
        For j = 0 To ContCosm.NumSegm - 1
            With ContCosm.Segm(j)
                xB = .xyBeg(0)
                yB = .xyBeg(1)
            End With
            If Abs(xBeg - xB) < Eps And Abs(yBeg - yB) < Eps Then
                jBeg = j
                Exit For
            End If
        Next j
        ContCurvBez.Segm(i).jBeg = jBeg
        If jBeg = -1 Then jBeg = 0
        For j = jBeg To ContCosm.NumSegm - 1
            With ContCosm.Segm(j)
                xE = .xyEnd(0)
                yE = .xyEnd(1)
            End With
            If Abs(xEnd - xE) < Eps And Abs(yEnd - yE) < Eps Then
                jEnd = j
                Exit For
            End If
        Next j
        ContCurvBez.Segm(i).jEnd = jEnd
    Next i
    
'End Initialize Bezier contour
'#############################################################
'#############################################################

End Sub



' Define number of Bezier segment on which lies point -(xP,yP)
Private Sub DefNumBezierSegment(xP#, yP#, ContCosm As Contour, ContCurvBez As CurveBezContour, iSegBez As Long, iSegCosm As Long)
Dim i As Long
Dim xB#, yB#, xE#, yE#
Dim l#, Alp#
' Define number of line segment on which lies point -(xP,yP)
Dim xLoc#, yLoc#
Dim yLocMin#
     yLocMin = 10000000
    iSegCosm = -1
    For i = 0 To ContCosm.NumSegm - 1
        With ContCosm.Segm(i)
            xB = .xyBeg(0)
            yB = .xyBeg(1)
            xE = .xyEnd(0)
            yE = .xyEnd(1)
            l = .l
            Alp = .Alp(0)
        End With
        xLoc = (yP - yB) * Sin(Alp) + (xP - xB) * Cos(Alp)
        yLoc = (yP - yB) * Cos(Alp) - (xP - xB) * Sin(Alp)
        If xLoc > -Eps And xLoc < l + Eps Then
            If Abs(yLoc) < yLocMin Then
                yLocMin = Abs(yLoc)
                iSegCosm = i
            End If
        End If
    Next i
    
' Define number of Bezier segment on which lies point -(xP,yP)
Dim jBeg As Long, jEnd As Long
    iSegBez = -1
    For i = 0 To ContCurvBez.NumSegm - 1
        With ContCurvBez.Segm(i)
            jBeg = .jBeg
            jEnd = .jEnd
        End With
        If iSegCosm >= jBeg And iSegCosm <= jEnd Then
            iSegBez = i
            Exit For
        End If
    Next i

End Sub



' Define value of t- parameter of Bezier segment for point -(xP,yP)

Private Function t_parameter(xP#, yP#, ContCosm As Contour, ContCurvBez As CurveBezContour, iSegBez As Long, iSegCosm As Long) As Double

Dim t#
Dim dj0 As Long
Dim dt0#
Dim tSeg#, dtSeg#
Dim xLoc#, yLoc#
Dim jBeg As Long, jEnd As Long
Dim xB#, yB#, xE#, yE#, l#, Alp#

    With ContCurvBez.Segm(iSegBez)
        jBeg = .jBeg
        jEnd = .jEnd
    End With
    
    dj0 = jEnd - jBeg + 1
    dt0 = 1 / dj0
    
    tSeg = (iSegCosm - jBeg) * dt0
    
    With ContCosm.Segm(iSegCosm)
        xB = .xyBeg(0)
        yB = .xyBeg(1)
        xE = .xyEnd(0)
        yE = .xyEnd(1)
        l = .l
        Alp = .Alp(0)
    End With
    xLoc = (yP - yB) * Sin(Alp) + (xP - xB) * Cos(Alp)
    yLoc = (yP - yB) * Cos(Alp) - (xP - xB) * Sin(Alp)
    
    dtSeg = xLoc / l * dt0
    
    t = tSeg + dtSeg
    
    t_parameter = t

End Function

Private Sub DefineCurvParAtPoint(ContCurvBez As CurveBezContour, iSegBez As Long, t#, xP#, yP#, dxP#, dyP#)

Dim xBeg#, yBeg#, xEnd#, yEnd#
Dim xDirBeg#, yDirBeg#, xDirEnd#, yDirEnd#

    With ContCurvBez.Segm(iSegBez)
        xBeg = .xBeg
        yBeg = .yBeg
        xEnd = .xEnd
        yEnd = .yEnd
            
        xDirBeg = .xDirBeg
        yDirBeg = .yDirBeg
        xDirEnd = .xDirEnd
        yDirEnd = .yDirEnd
    End With
    
    xP = (1 - t) ^ 3 * xBeg + 3 * t * (1 - t) ^ 2 * xDirBeg + 3 * t ^ 2 * (1 - t) * xDirEnd + t ^ 3 * xEnd
    yP = (1 - t) ^ 3 * yBeg + 3 * t * (1 - t) ^ 2 * yDirBeg + 3 * t ^ 2 * (1 - t) * yDirEnd + t ^ 3 * yEnd
    
    dxP = -3 * (1 - t) ^ 2 * xBeg + 3 * (1 - t) * (1 - 3 * t) * xDirBeg + 3 * t * (2 - 3 * t) * xDirEnd + 3 * t ^ 2 * xEnd
    dyP = -3 * (1 - t) ^ 2 * yBeg + 3 * (1 - t) * (1 - 3 * t) * yDirBeg + 3 * t * (2 - 3 * t) * yDirEnd + 3 * t ^ 2 * yEnd
    
'    ddxP = 6 * (1 - t) * xBeg + 6 * (-2 + 3 * t) * xDirBeg + 6 * (1 - 3 * t) * xDirEnd + 6 * t * xEnd
'    ddyP = 6 * (1 - t) * yBeg + 6 * (-2 + 3 * t) * yDirBeg + 6 * (1 - 3 * t) * yDirEnd + 6 * t * yEnd

End Sub

