VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BiSplineToBezier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Type CurveBezSegment
    xBeg As Double
    yBeg As Double
    xEnd As Double
    yEnd As Double
    xDirBeg As Double
    yDirBeg As Double
    xDirEnd As Double
    yDirEnd As Double
    dxBeg As Double
    dyBeg As Double
    ddxBeg As Double
    ddyBeg As Double
    dxEnd As Double
    dyEnd As Double
    ddxEnd As Double
    ddyEnd As Double
    jBeg As Long
    jEnd As Long
End Type

Private Type CurveBezContour
    NumSegm As Long
    Segm() As CurveBezSegment
End Type

Private Type BiSplinBasePoint
    nPoints As Long
    xP() As Double
    yP() As Double
    dxP() As Double
    dyP() As Double
    ddxP() As Double
    ddyP() As Double
End Type





Public Function SplineToBezier(GrSpline As Graphic) As Graphic
    Set SplineToBezier = Nothing
Dim GrBez As Graphic
    Set GrBez = Nothing
On Error GoTo ErrEnd

        
    If GrSpline.Type <> "TCW30CURVE" Then Exit Function
    If GrSpline.Properties("$SPLINETYPE") <> 0 Then Exit Function
    
Dim nV As Long
    nV = GrSpline.Vertices.Count
Dim xBeg#, yBeg#, xEnd#, yEnd#
    With GrSpline.Vertices
        xBeg = .Item(0).x
        yBeg = .Item(0).y
        xEnd = .Item(nV - 1).x
        yEnd = .Item(nV - 1).y
    End With
Dim ContClosed As Boolean
    ContClosed = False
    If Abs(xBeg - xEnd) < Eps And Abs(yBeg - yEnd) < Eps Then
        ContClosed = True
    End If
    
Dim BasePoint As BiSplinBasePoint
    BasePoint.nPoints = nV + 2
ReDim BasePoint.xP(nV + 1)
ReDim BasePoint.yP(nV + 1)
ReDim BasePoint.dxP(nV + 1)
ReDim BasePoint.dyP(nV + 1)
ReDim BasePoint.ddxP(nV + 1)
ReDim BasePoint.ddyP(nV + 1)
Dim i As Long
Dim xi#, yi#
    With GrSpline.Vertices
        For i = 0 To nV - 1
            xi = .Item(i).x
            yi = .Item(i).y
            BasePoint.xP(i + 1) = xi
            BasePoint.yP(i + 1) = yi
        Next i
    End With
    
    If ContClosed = True Then
        BasePoint.xP(0) = BasePoint.xP(nV - 1)
        BasePoint.yP(0) = BasePoint.yP(nV - 1)
        BasePoint.xP(nV + 1) = BasePoint.xP(2)
        BasePoint.yP(nV + 1) = BasePoint.yP(2)
    Else
        BasePoint.xP(0) = 2 * BasePoint.xP(1) - BasePoint.xP(2)
        BasePoint.yP(0) = 2 * BasePoint.yP(1) - BasePoint.yP(2)
        BasePoint.xP(nV + 1) = 2 * BasePoint.xP(nV) - BasePoint.xP(nV - 1)
        BasePoint.yP(nV + 1) = 2 * BasePoint.yP(nV) - BasePoint.yP(nV - 1)
    End If
    
Dim t#, dt#, t0#, t1#
'Dim nDiv%
'    nDiv = 10
'    dt = 0.1
Dim x0#, x1#, x2#, x3#
Dim y0#, y1#, y2#, y3#
Dim ax#, bx#, cx#, dx#
Dim ay#, by#, cy#, dy#
Dim dxP0#, dyP0#
Dim ddxP0#, ddyP0#
Dim dxP1#, dyP1#
Dim ddxP1#, ddyP1#

'Dim GrCosm As Graphic
'    Set GrCosm = Grs.Add(11)
'    GrCosm.Properties("PenColor") = RGB(255, 0, 0)
    For i = 1 To nV - 1
        x0 = BasePoint.xP(i - 1)
        x1 = BasePoint.xP(i)
        x2 = BasePoint.xP(i + 1)
        x3 = BasePoint.xP(i + 2)
        y0 = BasePoint.yP(i - 1)
        y1 = BasePoint.yP(i)
        y2 = BasePoint.yP(i + 1)
        y3 = BasePoint.yP(i + 2)
        
        ax = (-x3 + 3 * x2 - 3 * x1 + x0) / 6
        bx = (x3 - 2 * x2 + x1) / 2
        cx = (-x3 + x1) / 2
        dx = (x3 + 4 * x2 + x1) / 6
   
        ay = (-y3 + 3 * y2 - 3 * y1 + y0) / 6
        by = (y3 - 2 * y2 + y1) / 2
        cy = (-y3 + y1) / 2
        dy = (y3 + 4 * y2 + y1) / 6
        
        t = 0
        t1 = 1 - t
        dxP0 = -3 * ax * t1 ^ 2 - 2 * bx * t1 - cx
        dyP0 = -3 * ay * t1 ^ 2 - 2 * by * t1 - cy
        ddxP0 = 6 * ax * t1 + 2 * bx
        ddyP0 = 6 * ay * t1 + 2 * by
        BasePoint.dxP(i) = dxP0
        BasePoint.dyP(i) = dyP0
        BasePoint.ddxP(i) = ddxP0
        BasePoint.ddyP(i) = ddyP0
        
        
        t = 1
        t1 = 1 - t
        dxP1 = -3 * ax * t1 ^ 2 - 2 * bx * t1 - cx
        dyP1 = -3 * ay * t1 ^ 2 - 2 * by * t1 - cy
        ddxP1 = 6 * ax * t1 + 2 * bx
        ddyP1 = 6 * ay * t1 + 2 * by
        If i = nV - 1 Then
            BasePoint.dxP(i + 1) = dxP1
            BasePoint.dyP(i + 1) = dyP1
            BasePoint.ddxP(i + 1) = ddxP1
            BasePoint.ddyP(i + 1) = ddyP1
        End If
    Next i
    
Dim ContBez As CurveBezContour
    ContBez.NumSegm = nV - 1
ReDim ContBez.Segm(nV - 1)
Dim SegmBez As CurveBezSegment
Dim xDirBeg#, yDirBeg#
Dim xDirEnd#, yDirEnd#
Dim dxBeg#, dyBeg#
Dim ddxBeg#, ddyBeg#
Dim dxEnd#, dyEnd#
Dim ddxEnd#, ddyEnd#

    For i = 1 To nV - 1
        x0 = BasePoint.xP(i - 1)
        x1 = BasePoint.xP(i)
        x2 = BasePoint.xP(i + 1)
        x3 = BasePoint.xP(i + 2)
        y0 = BasePoint.yP(i - 1)
        y1 = BasePoint.yP(i)
        y2 = BasePoint.yP(i + 1)
        y3 = BasePoint.yP(i + 2)
        
        ax = (-x3 + 3 * x2 - 3 * x1 + x0) / 6
        bx = (x3 - 2 * x2 + x1) / 2
        cx = (-x3 + x1) / 2
        dx = (x3 + 4 * x2 + x1) / 6
   
        ay = (-y3 + 3 * y2 - 3 * y1 + y0) / 6
        by = (y3 - 2 * y2 + y1) / 2
        cy = (-y3 + y1) / 2
        dy = (y3 + 4 * y2 + y1) / 6
        
        t = 0
        t1 = 1 - t
        xBeg = ax * t1 ^ 3 + bx * t1 ^ 2 + cx * t1 + dx
        yBeg = ay * t1 ^ 3 + by * t1 ^ 2 + cy * t1 + dy
        
        t = 1
        t1 = 1 - t
        xEnd = ax * t1 ^ 3 + bx * t1 ^ 2 + cx * t1 + dx
        yEnd = ay * t1 ^ 3 + by * t1 ^ 2 + cy * t1 + dy
        
        dxBeg = BasePoint.dxP(i)
        dyBeg = BasePoint.dyP(i)
        dxEnd = BasePoint.dxP(i + 1)
        dyEnd = BasePoint.dyP(i + 1)
        ddxBeg = BasePoint.ddxP(i)
        ddyBeg = BasePoint.ddyP(i)
        ddxEnd = BasePoint.ddxP(i + 1)
        ddyEnd = BasePoint.ddyP(i + 1)
        
        With SegmBez
            .xBeg = xBeg
            .xEnd = xEnd
            .yBeg = yBeg
            .yEnd = yEnd
            .dxBeg = dxBeg
            .dyBeg = dyBeg
            .dxEnd = dxEnd
            .dyEnd = dyEnd
            .ddxBeg = ddxBeg
            .ddyBeg = ddyBeg
            .ddxEnd = ddxEnd
            .ddyEnd = ddyEnd
        End With
        ContBez.Segm(i - 1) = SegmBez
    Next i
    
' Create Bezier
    xBeg = ContBez.Segm(0).xBeg
    yBeg = ContBez.Segm(0).yBeg
    Set GrBez = Grs.AddCurveBezier(xBeg, yBeg, 0)
    GrBez.Properties("PenColor") = RGB(255, 0, 0)
    
    For i = 0 To ContBez.NumSegm - 1
        xEnd = ContBez.Segm(i).xEnd
        yEnd = ContBez.Segm(i).yEnd
        GrBez.Vertices.Add xEnd, yEnd, 0
    Next i
    
    GrBez.Draw
    
Dim iVBeg As Long, iVEnd As Long
Dim iVDirBeg As Long, iVDirEnd As Long
Dim Vers As Vertices
    Set Vers = GrBez.Vertices
    ' Correct directional Bezier points
    For i = 0 To ContBez.NumSegm - 1
        iVBeg = 3 * i
        iVEnd = 3 * (i + 1)
        iVDirBeg = 3 * i + 1
        iVDirEnd = 3 * (i + 1) - 1
        SegmBez = ContBez.Segm(i)
        With SegmBez
            xBeg = .xBeg
            xEnd = .xEnd
            yBeg = .yBeg
            yEnd = .yEnd
            dxBeg = .dxBeg
            dyBeg = .dyBeg
            dxEnd = .dxEnd
            dyEnd = .dyEnd
            ddxBeg = .ddxBeg
            ddyBeg = .ddyBeg
            ddxEnd = .ddxEnd
            ddyEnd = .ddyEnd
        End With
        
        xDirBeg = (dxBeg + 3 * xBeg) / 3
        yDirBeg = (dyBeg + 3 * yBeg) / 3
        
        xDirEnd = (-dxEnd + 3 * xEnd) / 3
        yDirEnd = (-dyEnd + 3 * yEnd) / 3
        
        Vers(iVDirBeg).x = xDirBeg
        Vers(iVDirBeg).y = yDirBeg
    
        Vers(iVDirEnd).x = xDirEnd
        Vers(iVDirEnd).y = yDirEnd
    Next i
    GrBez.Draw
    Set SplineToBezier = GrBez
    Exit Function
    
ErrEnd:
    
    Set SplineToBezier = Nothing
    If Not GrBez Is Nothing Then
        On Error Resume Next
        Grs.Remove GrBez.Index
        On Error Resume Next
        GrBez.Delete
        Set GrBez = Nothing
    End If

End Function

