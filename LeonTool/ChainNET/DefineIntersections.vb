Option Strict Off
Option Explicit On
Friend Class DefineIntersections
	
	
	
	
	Public Function RunIntersect(ByRef Cont1 As ChainTool.Contour, ByRef Cont2 As ChainTool.Contour, ByRef Inter As ChainTool.IntersTwoCont) As Boolean
		Dim IntCount As Integer
		
		Call CalcIntersection(Cont1, Cont2, Inter, IntCount)
		If IntCount < 1 Then
			RunIntersect = False
		Else
			RunIntersect = True
		End If
	End Function
	
	
	' Calculate the pointintersections
	
	Private Sub CalcIntersection(ByRef Cont1 As ChainTool.Contour, ByRef Cont2 As ChainTool.Contour, ByRef Inter As ChainTool.IntersTwoCont, ByRef IntCount As Integer)
		
		Dim Type1 As String
		Dim fi1, yc1, yMid1, yE1, yB1, xB1, xE1, xMid1, xc1, L1, R1 As Double
		Dim Rot1 As Short
		Dim Type2 As String
		Dim fi2, yc2, yMid2, yE2, yB2, xB2, xE2, xMid2, xc2, L2, R2 As Double
		Dim Rot2 As Short
		Dim i, j As Integer
		Dim xInt2, xInt1, yInt1, yInt2 As Double
		Dim Type1P As String
		Dim fi1P, yc1P, yMid1P, yE1P, yB1P, xB1P, xE1P, xMid1P, xc1P, L1P, R1P As Double
		Dim Rot1P As Short
		Dim Type1N As String
		Dim fi1N, yc1N, yMid1N, yE1N, yB1N, xB1N, xE1N, xMid1N, xc1N, L1N, R1N As Double
		Dim Rot1N As Short
		Dim xMin1, xMax1, yMax1, yMin1 As Double
		IntCount = 0
		
		Dim rB, rE As Double
		Dim sina, cosa As Double
		Dim yLoc As Double
		Dim Res3, Res1, Res2, Res4 As Boolean
		For i = 0 To Cont1.NumSegm - 1
			With Cont1.Segm(i)
				Type1 = .SegType
				xB1 = .xyBeg.v0
				yB1 = .xyBeg.v1
				xE1 = .xyEnd.v0
				yE1 = .xyEnd.v1
				L1 = .L
				fi1 = .Alp.v0
				xc1 = .xyCent.v0
				yc1 = .xyCent.v1
				xMid1 = .xyMid.v0
				yMid1 = .xyMid.v1
				R1 = .R
				Rot1 = .Rot
			End With
			
			If Type1 = "Line" Then
				If xB1 > xE1 Then
					xMax1 = xB1
					xMin1 = xE1
				Else
					xMax1 = xE1
					xMin1 = xB1
				End If
				If yB1 > yE1 Then
					yMax1 = yB1
					yMin1 = yE1
				Else
					yMax1 = yE1
					yMin1 = yB1
				End If
			End If
			
			Type1P = ""
			If i > 0 Then
				With Cont1.Segm(i - 1)
					Type1P = .SegType
					xB1P = .xyBeg.v0
					yB1P = .xyBeg.v1
					xE1P = .xyEnd.v0
					yE1P = .xyEnd.v1
					L1P = .L
					fi1P = .Alp.v0
					xc1P = .xyCent.v0
					yc1P = .xyCent.v1
					xMid1P = .xyMid.v0
					yMid1P = .xyMid.v1
					R1P = .R
					Rot1P = .Rot
				End With
			End If
			
			Type1N = ""
			If i < Cont1.NumSegm - 1 Then
				With Cont1.Segm(i + 1)
					Type1N = .SegType
					xB1N = .xyBeg.v0
					yB1N = .xyBeg.v1
					xE1N = .xyEnd.v0
					yE1N = .xyEnd.v1
					L1N = .L
					fi1N = .Alp.v0
					xc1N = .xyCent.v0
					yc1N = .xyCent.v1
					xMid1N = .xyMid.v0
					yMid1N = .xyMid.v1
					R1N = .R
					Rot1N = .Rot
				End With
			End If
			
			For j = 0 To Cont2.NumSegm - 1
				With Cont2.Segm(j)
					Type2 = .SegType
					xB2 = .xyBeg.v0
					yB2 = .xyBeg.v1
					xE2 = .xyEnd.v0
					yE2 = .xyEnd.v1
					L2 = .L
					fi2 = .Alp.v0
					xc2 = .xyCent.v0
					yc2 = .xyCent.v1
					xMid2 = .xyMid.v0
					yMid2 = .xyMid.v1
					R2 = .R
					Rot2 = .Rot
				End With
				If Type1 = "Line" And Type2 = "Line" Then
					If xB2 > xMax1 + Eps And xE2 > xMax1 + Eps Then GoTo JJ
					If xB2 < xMin1 - Eps And xE2 < xMin1 - Eps Then GoTo JJ
					If yB2 > yMax1 + Eps And yE2 > yMax1 + Eps Then GoTo JJ
					If yB2 < yMin1 - Eps And yE2 < yMin1 - Eps Then GoTo JJ
				End If
				
				If Type1 <> "Line" And Type2 = "Line" Then
					rB = System.Math.Sqrt((xB2 - xc1) ^ 2 + (yB2 - yc1) ^ 2)
					rE = System.Math.Sqrt((xE2 - xc1) ^ 2 + (yE2 - yc1) ^ 2)
					If rB < R1 - Eps And rE < R1 - Eps Then GoTo JJ
					
					sina = (yE2 - yB2) / L2
					cosa = (xE2 - xB2) / L2
					yLoc = (yc1 - yB2) * cosa - (xc1 - xB2) * sina
					If yLoc > R1 + Eps Then GoTo JJ
				End If
				
				If Type1 = "Line" And Type2 <> "Line" Then
					rB = System.Math.Sqrt((xB1 - xc2) ^ 2 + (yB1 - yc2) ^ 2)
					rE = System.Math.Sqrt((xE1 - xc2) ^ 2 + (yE1 - yc2) ^ 2)
					If rB < R2 - Eps And rE < R2 - Eps Then GoTo JJ
					
					sina = (yE1 - yB1) / L1
					cosa = (xE1 - xB1) / L1
					yLoc = (yc2 - yB1) * cosa - (xc2 - xB1) * sina
					If yLoc > R2 + Eps Then GoTo JJ
				End If
				
				If Type1 = "Circle" And Type2 = "Circle" Then
					If System.Math.Abs(xc1 - xc2) < Eps And System.Math.Abs(yc1 - yc2) < Eps Then
						GoTo JJ
					End If
				End If
				
				If Type1 = "Line" And Type2 = "Line" Then
					If System.Math.Abs(xB1 - xB2) < Eps And System.Math.Abs(yB1 - yB2) < Eps And System.Math.Abs(xE1 - xE2) < Eps And System.Math.Abs(yE1 - yE2) < Eps Then
						GoTo JJ
					End If
					If System.Math.Abs(xB1 - xE2) < Eps And System.Math.Abs(yB1 - yE2) < Eps And System.Math.Abs(xE1 - xB2) < Eps And System.Math.Abs(yE1 - yB2) < Eps Then
						GoTo JJ
					End If
				End If
				
				If Type1P = "Line" And Type2 = "Line" Then
					If System.Math.Abs(xB1P - xB2) < Eps And System.Math.Abs(yB1P - yB2) < Eps And System.Math.Abs(xE1P - xE2) < Eps And System.Math.Abs(yE1P - yE2) < Eps Then
						GoTo JJ
					End If
					If System.Math.Abs(xB1P - xE2) < Eps And System.Math.Abs(yB1P - yE2) < Eps And System.Math.Abs(xE1P - xB2) < Eps And System.Math.Abs(yE1P - yB2) < Eps Then
						GoTo JJ
					End If
				End If
				
				If Type1N = "Line" And Type2 = "Line" Then
					If System.Math.Abs(xB1N - xB2) < Eps And System.Math.Abs(yB1P - yB2) < Eps And System.Math.Abs(xE1N - xE2) < Eps And System.Math.Abs(yE1N - yE2) < Eps Then
						GoTo JJ
					End If
					If System.Math.Abs(xB1N - xE2) < Eps And System.Math.Abs(yB1N - yE2) < Eps And System.Math.Abs(xE1N - xB2) < Eps And System.Math.Abs(yE1N - yB2) < Eps Then
						GoTo JJ
					End If
				End If
				
				
				
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				If Type1 = "Circle" And Type2 = "Circle" Then
					If CircCircIntersection(xc1, yc1, R1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
						ReDim Preserve Inter.xInt(IntCount)
						ReDim Preserve Inter.yInt(IntCount)
						ReDim Preserve Inter.IndInt1(IntCount)
						ReDim Preserve Inter.IndInt2(IntCount)
						Inter.IndInt1(IntCount) = i
						Inter.IndInt2(IntCount) = j
						Inter.xInt(IntCount) = xInt1
						Inter.yInt(IntCount) = yInt1
						IntCount = IntCount + 1
						ReDim Preserve Inter.xInt(IntCount)
						ReDim Preserve Inter.yInt(IntCount)
						ReDim Preserve Inter.IndInt1(IntCount)
						ReDim Preserve Inter.IndInt2(IntCount)
						Inter.IndInt1(IntCount) = i
						Inter.IndInt2(IntCount) = j
						Inter.xInt(IntCount) = xInt2
						Inter.yInt(IntCount) = yInt2
						IntCount = IntCount + 1
					End If
				End If
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				
				If Type1 = "Line" And Type2 = "Line" Then
					If SegSegIntersection(xB1, yB1, xE1, yE1, xB2, yB2, xE2, yE2, xInt1, yInt1) = True Then
						ReDim Preserve Inter.xInt(IntCount)
						ReDim Preserve Inter.yInt(IntCount)
						ReDim Preserve Inter.IndInt1(IntCount)
						ReDim Preserve Inter.IndInt2(IntCount)
						Inter.IndInt1(IntCount) = i
						Inter.IndInt2(IntCount) = j
						Inter.xInt(IntCount) = xInt1
						Inter.yInt(IntCount) = yInt1
						IntCount = IntCount + 1
					End If
				End If
				
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				If Type1 = "Arc" And Type2 = "Arc" Then
					If CircCircIntersection(xc1, yc1, R1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
						Res1 = PointOnArc(Cont1.Segm(i), xInt1, yInt1)
						Res2 = PointOnArc(Cont2.Segm(j), xInt1, yInt1)
						If Res1 = True And Res2 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt1
							Inter.yInt(IntCount) = yInt1
							IntCount = IntCount + 1
						End If
						
						Res3 = PointOnArc(Cont1.Segm(i), xInt2, yInt2)
						Res4 = PointOnArc(Cont2.Segm(j), xInt2, yInt2)
						If Res3 = True And Res4 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt2
							Inter.yInt(IntCount) = yInt2
							IntCount = IntCount + 1
						End If
					End If
				End If
				
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				If Type1 = "Circle" And Type2 = "Arc" Then
					If CircCircIntersection(xc1, yc1, R1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
						Res2 = PointOnArc(Cont2.Segm(j), xInt1, yInt1)
						If Res2 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt1
							Inter.yInt(IntCount) = yInt1
							IntCount = IntCount + 1
						End If
						
						Res4 = PointOnArc(Cont2.Segm(j), xInt2, yInt2)
						If Res4 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt2
							Inter.yInt(IntCount) = yInt2
							IntCount = IntCount + 1
						End If
					End If
				End If
				
				' ********************************************************
				If Type1 = "Arc" And Type2 = "Circle" Then
					If CircCircIntersection(xc1, yc1, R1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
						Res1 = PointOnArc(Cont1.Segm(i), xInt1, yInt1)
						If Res1 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt1
							Inter.yInt(IntCount) = yInt1
							IntCount = IntCount + 1
						End If
						
						Res3 = PointOnArc(Cont1.Segm(i), xInt2, yInt2)
						If Res3 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt2
							Inter.yInt(IntCount) = yInt2
							IntCount = IntCount + 1
						End If
					End If
				End If
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				If Type1 = "Line" And Type2 = "Arc" Then
					If LineCircIntersection(xB1, yB1, fi1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
						Res1 = PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt1, yInt1)
						Res2 = PointOnArc(Cont2.Segm(j), xInt1, yInt1)
						If Res1 = True And Res2 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt1
							Inter.yInt(IntCount) = yInt1
							IntCount = IntCount + 1
						End If
						
						Res3 = PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt2, yInt2)
						Res4 = PointOnArc(Cont2.Segm(j), xInt2, yInt2)
						If Res3 = True And Res4 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt2
							Inter.yInt(IntCount) = yInt2
							IntCount = IntCount + 1
						End If
					End If
				End If
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				If Type1 = "Line" And Type2 = "Circle" Then
					If LineCircIntersection(xB1, yB1, fi1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
						Res1 = PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt1, yInt1)
						If Res1 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt1
							Inter.yInt(IntCount) = yInt1
							IntCount = IntCount + 1
						End If
						
						Res3 = PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt2, yInt2)
						If Res3 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt2
							Inter.yInt(IntCount) = yInt2
							IntCount = IntCount + 1
						End If
					End If
				End If
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				If Type1 = "Arc" And Type2 = "Line" Then
					If LineCircIntersection(xB2, yB2, fi2, xc1, yc1, R1, xInt1, yInt1, xInt2, yInt2) = True Then
						Res1 = PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt1, yInt1)
						Res2 = PointOnArc(Cont1.Segm(i), xInt1, yInt1)
						If Res1 = True And Res2 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt1
							Inter.yInt(IntCount) = yInt1
							IntCount = IntCount + 1
						End If
						
						Res3 = PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt2, yInt2)
						Res4 = PointOnArc(Cont1.Segm(i), xInt2, yInt2)
						If Res3 = True And Res4 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt2
							Inter.yInt(IntCount) = yInt2
							IntCount = IntCount + 1
						End If
					End If
				End If
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				If Type1 = "Circle" And Type2 = "Line" Then
					If LineCircIntersection(xB2, yB2, fi2, xc1, yc1, R1, xInt1, yInt1, xInt2, yInt2) = True Then
						Res1 = PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt1, yInt1)
						If Res1 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt1
							Inter.yInt(IntCount) = yInt1
							IntCount = IntCount + 1
						End If
						
						Res3 = PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt2, yInt2)
						If Res3 = True Then
							ReDim Preserve Inter.xInt(IntCount)
							ReDim Preserve Inter.yInt(IntCount)
							ReDim Preserve Inter.IndInt1(IntCount)
							ReDim Preserve Inter.IndInt2(IntCount)
							Inter.IndInt1(IntCount) = i
							Inter.IndInt2(IntCount) = j
							Inter.xInt(IntCount) = xInt2
							Inter.yInt(IntCount) = yInt2
							IntCount = IntCount + 1
						End If
					End If
				End If
				'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
JJ: 
			Next j
			'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Next i
		Inter.NumInt = IntCount
	End Sub
	
	'##########################################################################
	'##########################################################################
	'##########################################################################
	'##########################################################################
	
	Private Function Angle(ByRef sinB As Double, ByRef cosB As Double) As Double
		Dim Eps1 As Double
		Eps1 = 0.0000000000001
		If System.Math.Abs(cosB) < Eps1 Then
			If sinB > Eps1 Then
				Angle = Pi / 2
			Else
				Angle = 3 * Pi / 2
			End If
		Else
			If System.Math.Abs(sinB) < Eps1 Then
				If cosB > Eps1 Then
					Angle = 0
				Else
					Angle = Pi
				End If
			Else
				If sinB >= 0 And cosB > 0 Then
					Angle = System.Math.Atan(sinB / cosB)
				Else
					If sinB >= 0 And cosB < 0 Then
						Angle = Pi + System.Math.Atan(sinB / cosB)
					Else
						If sinB < 0 And cosB < 0 Then
							Angle = Pi + System.Math.Atan(sinB / cosB)
						Else
							If sinB < 0 And cosB > 0 Then
								Angle = 2 * Pi + System.Math.Atan(sinB / cosB)
							End If
						End If
					End If
				End If
			End If
		End If
	End Function
	
	
	Private Function Sign(ByRef y As Double) As Double
		If System.Math.Abs(y) < 0.000001 Then
			Sign = 0
			Exit Function
		End If
		If y < 0 Then
			Sign = -1#
			Exit Function
		End If
		If y > 0 Then
			Sign = 1#
			Exit Function
		End If
	End Function
	
	
	Private Function SegSegIntersection(ByRef xB1 As Double, ByRef yB1 As Double, ByRef xE1 As Double, ByRef yE1 As Double, ByRef xB2 As Double, ByRef yB2 As Double, ByRef xE2 As Double, ByRef yE2 As Double, ByRef xInt As Double, ByRef yInt As Double) As Boolean
		Dim b1, a1, d1 As Double
		Dim b2, a2, d2 As Double
		
		a1 = -(yE1 - yB1)
		b1 = xE1 - xB1
		d1 = a1 * xB1 + b1 * yB1
		
		a2 = -(yE2 - yB2)
		b2 = xE2 - xB2
		d2 = a2 * xB2 + b2 * yB2
		
		If SysLine(a1, b1, d1, a2, b2, d2, xInt, yInt) = True Then
			If PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt, yInt) = True And PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt, yInt) = True Then
				SegSegIntersection = True
			Else
				SegSegIntersection = False
			End If
		Else
			SegSegIntersection = False
			If System.Math.Abs(xE1 - xB2) < Eps And System.Math.Abs(yE1 - yB2) < Eps Then
				If System.Math.Abs(xB1 - xE2) > Eps Or System.Math.Abs(yB1 - yE2) > Eps Then
					xInt = xE1
					yInt = yE1
					SegSegIntersection = True
				End If
			End If
			If System.Math.Abs(xE1 - xE2) < Eps And System.Math.Abs(yE1 - yE2) < Eps Then
				If System.Math.Abs(xB1 - xB2) > Eps Or System.Math.Abs(yB1 - yB2) > Eps Then
					xInt = xE1
					yInt = yE1
					SegSegIntersection = True
				End If
			End If
			
		End If
	End Function
	
	
	' Define points of intersection for two circles
	Private Function CircCircIntersection(ByRef xc1 As Double, ByRef yc1 As Double, ByRef R1 As Double, ByRef xc2 As Double, ByRef yc2 As Double, ByRef R2 As Double, ByRef xInt1 As Double, ByRef yInt1 As Double, ByRef xInt2 As Double, ByRef yInt2 As Double) As Boolean
		Dim R1Abs, R2Abs As Double
		R1Abs = System.Math.Abs(R1)
		R2Abs = System.Math.Abs(R2)
		
		Dim sina, LCC, cosa As Double
		LCC = System.Math.Sqrt((xc2 - xc1) * (xc2 - xc1) + (yc2 - yc1) * (yc2 - yc1))
		If System.Math.Abs(LCC) > R1Abs + R2Abs + Eps Then
			CircCircIntersection = False
			Exit Function
		End If
		If System.Math.Abs(LCC) < Eps And System.Math.Abs(R1Abs - R2Abs) < Eps Then
			CircCircIntersection = False
			Exit Function
		End If
		If LCC < System.Math.Abs(R1Abs - R2Abs) - Eps Then
			CircCircIntersection = False
			Exit Function
		End If
		
		sina = (yc2 - yc1) / LCC
		cosa = (xc2 - xc1) / LCC
		Dim xLoc, yLoc As Double
		xLoc = (LCC * LCC + R1Abs * R1Abs - R2Abs * R2Abs) / (2 * LCC)
		yLoc = System.Math.Sqrt(System.Math.Abs(R1Abs * R1Abs - xLoc * xLoc))
		
		xInt1 = xc1 + xLoc * cosa - yLoc * sina
		yInt1 = yc1 + xLoc * sina + yLoc * cosa
		
		yLoc = -yLoc
		xInt2 = xc1 + xLoc * cosa - yLoc * sina
		yInt2 = yc1 + xLoc * sina + yLoc * cosa
		CircCircIntersection = True
	End Function
	
	
	
	' Define the intersection points for Line and Circle
	Private Function LineCircIntersection(ByRef x1 As Double, ByRef y1 As Double, ByRef fi As Double, ByRef xc As Double, ByRef yc As Double, ByRef R As Double, ByRef xInt1 As Double, ByRef yInt1 As Double, ByRef xInt2 As Double, ByRef yInt2 As Double) As Boolean
		Dim sina, cosa As Double
		sina = System.Math.Sin(fi)
		cosa = System.Math.Cos(fi)
		Dim xcLoc, ycLoc As Double
		xcLoc = (yc - y1) * sina + (xc - x1) * cosa
		ycLoc = (yc - y1) * cosa - (xc - x1) * sina
		If System.Math.Abs(ycLoc) > System.Math.Abs(R) + Eps Then
			LineCircIntersection = False
			Exit Function
		End If
		Dim t As Double
		If System.Math.Abs(R - System.Math.Abs(ycLoc)) < 10 * Eps Then
			t = 0
		Else
			t = System.Math.Sqrt(System.Math.Abs(R * R - ycLoc * ycLoc))
		End If
		Dim xLoc, yLoc As Double
		yLoc = 0
		xLoc = xcLoc + t
		xInt1 = x1 + xLoc * cosa - yLoc * sina
		yInt1 = y1 + xLoc * sina + yLoc * cosa
		
		xLoc = xcLoc - t
		xInt2 = x1 + xLoc * cosa - yLoc * sina
		yInt2 = y1 + xLoc * sina + yLoc * cosa
		LineCircIntersection = True
	End Function
	
	' ###############################################################################
	' ###############################################################################
	
	' Function return 1 - if point lies inside line segment, or 0 - if point lies outside or on
	' on bounds of line segment
	
	Private Function PointIntoSegmenti(ByRef x1 As Double, ByRef y1 As Double, ByRef x2 As Double, ByRef y2 As Double, ByRef x As Double, ByRef y As Double) As Boolean
		Dim sina, dy, dx, L, cosa As Double
		PointIntoSegmenti = False
		If System.Math.Abs(x - x1) < Eps And System.Math.Abs(y - y1) < Eps Then
			PointIntoSegmenti = True
			Exit Function
		End If
		If System.Math.Abs(x - x2) < Eps And System.Math.Abs(y - y2) < Eps Then
			PointIntoSegmenti = True
			Exit Function
		End If
		Dim xLoc, yLoc As Double
		dx = x2 - x1
		dy = y2 - y1
		L = System.Math.Sqrt(dx * dx + dy * dy)
		If L < Eps Then Exit Function
		sina = dy / L
		cosa = dx / L
		
		yLoc = (y - y1) * cosa - (x - x1) * sina
		xLoc = (y - y1) * sina + (x - x1) * cosa
		If System.Math.Abs(yLoc) > 1 * Eps Then
			PointIntoSegmenti = False
			Exit Function
		End If
		If xLoc > 0 And xLoc < L Then
			PointIntoSegmenti = True
			Exit Function
		End If
		
	End Function
	
	
	
	' Define if point lies on Arc
	Private Function PointOnArc(ByRef Segm As ChainTool.Segment, ByRef xP As Double, ByRef yP As Double) As Boolean
		
		Dim xMid, yc, yE, xB, xE, xc, R, yMid As Double
		Dim yB As Object
		Dim Rot As Short
		xB = Segm.xyBeg.v0
		'UPGRADE_WARNING: Couldn't resolve default property of object yB. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		yB = Segm.xyBeg.v1
		xE = Segm.xyEnd.v0
		yE = Segm.xyEnd.v1
		xc = Segm.xyCent.v0
		yc = Segm.xyCent.v1
		xMid = Segm.xyMid.v0
		yMid = Segm.xyMid.v1
		R = System.Math.Abs(Segm.R)
		Rot = Segm.Rot
		
		Dim xMidLoc, xPLoc, xELoc, sina, cosa, yELoc, yPLoc, yMidLoc As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object yB. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		sina = (yB - yc) / R
		cosa = (xB - xc) / R
		xELoc = (yE - yc) * sina + (xE - xc) * cosa
		yELoc = (yE - yc) * cosa - (xE - xc) * sina
		xPLoc = (yP - yc) * sina + (xP - xc) * cosa
		yPLoc = (yP - yc) * cosa - (xP - xc) * sina
		xMidLoc = (yMid - yc) * sina + (xMid - xc) * cosa
		yMidLoc = (yMid - yc) * cosa - (xMid - xc) * sina
		
		Dim AlpELoc, AlpPLoc As Double
		sina = yELoc / R
		cosa = xELoc / R
		AlpELoc = Angle(sina, cosa)
		
		sina = yPLoc / R
		cosa = xPLoc / R
		AlpPLoc = Angle(sina, cosa)
		
        If Rot = -1 Then
            '   If Rot <> 0 Then
            AlpELoc = 2 * Pi - AlpELoc
            AlpPLoc = 2 * Pi - AlpPLoc
        End If

        If AlpPLoc > Eps And AlpPLoc < AlpELoc + Eps Then
            PointOnArc = True
        Else
            PointOnArc = False
        End If

        'UPGRADE_WARNING: Couldn't resolve default property of object yB. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If System.Math.Abs(xP - xB) < Eps And System.Math.Abs(yP - yB) < Eps Then
            PointOnArc = True
        End If
        If System.Math.Abs(xP - xE) < Eps And System.Math.Abs(yP - yE) < Eps Then
            PointOnArc = True
        End If

    End Function
	
	
	Private Function SysLine(ByRef a1 As Double, ByRef b1 As Double, ByRef d1 As Double, ByRef a2 As Double, ByRef b2 As Double, ByRef d2 As Double, ByRef xRes As Double, ByRef yRes As Double) As Boolean
		
		Dim delx, del, dely As Double
		On Error GoTo ErrorHandler
		
		del = a1 * b2 - a2 * b1
		delx = d1 * b2 - b1 * d2
		dely = a1 * d2 - a2 * d1
		If System.Math.Abs(del) < Eps / 100 Then
			SysLine = False
			Exit Function
		End If
		xRes = delx / del
		yRes = dely / del
		SysLine = True
		Exit Function
ErrorHandler: 
		'    MsgBox Err.Description
		SysLine = False
	End Function
End Class