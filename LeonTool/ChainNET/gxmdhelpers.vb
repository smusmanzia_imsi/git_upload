﻿Option Strict Off
Option Explicit On

Imports IMSIGX


Public Class GXMDHELPERS

    Public Sub Collect()

        'GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced)
        'GC.Collect()

    End Sub

    Public Sub Release(ByRef gxObj As Application, Optional bDispose As Boolean = True)

        If Not gxObj Is Nothing Then

            If (bDispose) Then

                gxObj.Dispose()

            End If

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Tools, Optional bDispose As Boolean = True)

        If Not gxObj Is Nothing Then

            If (bDispose) Then

                gxObj.Dispose()

            End If

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Tool, Optional bDispose As Boolean = True)

        If Not gxObj Is Nothing Then

            If (bDispose) Then

                gxObj.Dispose()

            End If

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As ToolEvents)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Drawings)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Drawing, Optional bDispose As Boolean = True)

        If Not gxObj Is Nothing Then

            If (bDispose) Then

                gxObj.Dispose()

            End If

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Layer)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As View, Optional bDispose As Boolean = True)

        If Not gxObj Is Nothing Then

            If (bDispose) Then

                gxObj.Dispose()

            End If

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Graphics)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Graphic)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Vertices)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Vertex)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Properties)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As IMSIGX.Property)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As GraphicSets)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As GraphicSet)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As PickResult)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

    Public Sub Release(ByRef gxObj As Matrix)

        If Not gxObj Is Nothing Then

            gxObj.Dispose()

            gxObj = Nothing

        End If

    End Sub

End Class


