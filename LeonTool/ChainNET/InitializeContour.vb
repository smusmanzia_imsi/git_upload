Option Strict Off
Option Explicit On
Friend Class InitializeContour
	
	Private Structure Polyline
		Dim nSeg As Integer ' Number of segments in polyline (Lines and Arcs)
		Dim nV As Integer ' Count of Base Vetices in Graphic
		Dim xV() As Double
		Dim yV() As Double
		Dim vType() As String
		Dim iCosm() As Integer
		Dim UpDown() As Boolean
	End Structure
	
	
	Private Structure Cosmetic
		Dim nV As Integer
		Dim xV() As Double
		Dim yV() As Double
	End Structure
	
	'UPGRADE_WARNING: Arrays in structure Poly may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
	Private Poly As Polyline
	'UPGRADE_WARNING: Arrays in structure Cosm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
	Private Cosm As Cosmetic
	Private Segm() As ChainTool.Segment
	
	Private nSegm As Integer
	Private nVBase As Integer
	Private nVCosm As Integer
	
	'Private ActDr As Drawing
	'Private Grs As Graphics
	
	Private Smooth As Short
	
	Public Sub RunInitialize(ByRef Gr As IMSIGX.Graphic, ByRef Cont As ChainTool.Contour, ByRef xP As Double, ByRef yP As Double)
		
		Smooth = 1 ' Smooth small segments by tangent arcs
		Smooth = 0 ' draw segmented polyline
		
		'Set ActDr = Gr.Drawing
		'Set Grs = ActDr.Graphics
		'Set Grs = ActiveDrawing.Graphics
		
		'    If Gr.UCS.IsEqual(UCSGr0) = True Then
		Call InitStruct(Gr)
		'    Else
		'Dim GrDup As Graphic
		'        Set GrDup = Gr.Duplicate
		'        GrDup.UCS = UCSGr0
		'        Call InitStruct(GrDup)
		'        Gr.Drawing.Graphics.Remove GrDup.Index
		'        GrDup.Delete
		'    End If
		
		Cont.NumSegm = nSegm
		Cont.GrID = Gr.id
		Dim i As Integer
		ReDim Cont.Segm(nSegm - 1)
		For i = 0 To nSegm - 1
			'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Cont.Segm(i) = Segm(i)
		Next i
		Dim ContClosed As Boolean
		Dim xE, xB, yB, yE As Double
        xB = Segm(0).xyBeg.v0
        yB = Segm(0).xyBeg.v1
        xE = Segm(nSegm - 1).xyEnd.v0
        yE = Segm(nSegm - 1).xyEnd.v1
		If System.Math.Abs(xB - xE) < 10 * Eps And System.Math.Abs(yB - yE) < 10 * Eps Then
			ContClosed = True
		Else
			ContClosed = False
		End If
		Cont.Closed = ContClosed
		Cont.ClickDirection = 1
		'UPGRADE_NOTE: Dir was upgraded to Dir_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim Dir_Renamed As Short
		If ContClosed = True Then
			Dir_Renamed = ContourDirection(Cont)
			If Dir_Renamed <> 1 Then
				Call InvertContour(Cont)
			End If
		End If
		'    If Gr.Type = "ARC" Then
		Dim ArcData(9) As Double
		Dim Ratio As Double
		Dim R, xc, yc, Rp As Double
		If Gr.Type = ARCTYPE Then
			Gr.GetArcData(ArcData)
			Ratio = ArcData(6)
			If System.Math.Abs(Ratio - 1) < Eps Then
				With Gr.Vertices
					xc = .Item(0).x
					yc = .Item(0).y
					xB = .Item(1).x
					yB = .Item(1).y
					R = System.Math.Sqrt((xB - xc) ^ 2 + (yB - yc) ^ 2)
					Rp = System.Math.Sqrt((xP - xc) ^ 2 + (yP - yc) ^ 2)
					If Rp > R Then
						Call InvertContour(Cont)
						'                    Cont.ClickDirection = -1
					End If
				End With
				GoTo QQQ
			End If
		End If
		
		Dim yLoc, cosa, l, sina, xLoc, yLocMin As Double
		Dim II As Integer
		If ContClosed = False Then
			
			Call InitializeCosm()
			
			yLocMin = 1000000
			Dir_Renamed = 1
			II = -1
			For i = 0 To nVCosm - 2
				xB = Cosm.xV(i)
				yB = Cosm.yV(i)
				xE = Cosm.xV(i + 1)
				yE = Cosm.yV(i + 1)
				l = System.Math.Sqrt((xE - xB) ^ 2 + (yE - yB) ^ 2)
				If l > 10 * Eps Then
					sina = (yE - yB) / l
					cosa = (xE - xB) / l
					xLoc = (yP - yB) * sina + (xP - xB) * cosa
					yLoc = (yP - yB) * cosa - (xP - xB) * sina
					If xLoc > -Eps And xLoc < l Then
						If System.Math.Abs(yLoc) < System.Math.Abs(yLocMin) Then
							II = i
							yLocMin = yLoc
						End If
					End If
				End If
			Next i
			If yLocMin > 0 Then
				Dir_Renamed = 1
			Else
				Dir_Renamed = -1
			End If
			If II > -1 And Dir_Renamed = -1 Then
				'            Call InvertContour(Cont)
				Cont.ClickDirection = -1
			End If
		End If
QQQ: 
		'Set Grs = Nothing
		'Set ActDr = Nothing
	End Sub
	
	
	'####################################################################
	'####################################################################
	'####################################################################
	' Base Subroutine for
	Private Sub InitStruct(ByRef Gr As IMSIGX.Graphic)
		
		Dim i As Integer
		'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As New ChainTool.Contour
		
		Dim GrType As String
		' **********************************************************************
		'First graphic
		GrType = Gr.Type
		'    If GrType = "CIRCLE" Then
		Dim ArcData(9) As Double
		Dim Ratio As Double
		Dim xMid, yDir, yE, yB, yc, xc, xB, xE, xDir, R, yMid As Double
		If GrType = CIRCLETYPE Then
			Gr.GetArcData(ArcData)
			Ratio = ArcData(6)
			If System.Math.Abs(Ratio - 1) > Eps Then
				GrType = "Ellipse"
				'######################################################
				If Smooth = 0 Then
					Call InitializeSegmForSmallSegmentObject(Gr, Segm, Cosm, nSegm, nVCosm)
					For i = 0 To nSegm - 1
						Segm(i).UpDown = 1
					Next i
				Else
					Call SmoothEllipse(Gr, Cont)
					nSegm = Cont.NumSegm
					ReDim Segm(nSegm - 1)
					For i = 0 To nSegm - 1
						'UPGRADE_WARNING: Couldn't resolve default property of object Segm(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						Segm(i) = Cont.Segm(i)
						Segm(i).UpDown = 1
					Next i
				End If
				'######################################################
			Else
				nSegm = 2
				ReDim Segm(1)
				With Gr.Vertices
					xc = .Item(0).x
					yc = .Item(0).y
					xB = .Item(1).x
					yB = .Item(1).y
				End With
				R = System.Math.Sqrt((xB - xc) ^ 2 + (yB - yc) ^ 2)
				xB = xc + R * System.Math.Cos(0.15)
				yB = yc + R * System.Math.Sin(0.15)
				xE = xc + R * System.Math.Cos(0.15 + Pi)
				yE = yc + R * System.Math.Sin(0.15 + Pi)
				xMid = xc + R * System.Math.Cos(0.15 + Pi / 2)
				yMid = yc + R * System.Math.Sin(0.15 + Pi / 2)
				With Segm(0)
					.SegType = "Arc" '# NLS#'
                    .xyBeg.v0 = xB
                    .xyBeg.v1 = yB
                    .xyEnd.v0 = xE
                    .xyEnd.v1 = yE
                    .xyCent.v0 = xc
                    .xyCent.v1 = yc
					.R = R
					.Rot = 1
					.Distinct = 1
                    .xyMid.v0 = xMid
                    .xyMid.v1 = yMid
                    .Alp.v0 = 0.15 + Pi / 2
                    .Alp.v1 = 0.15 + 3 * Pi / 2
					.UpDown = 1
				End With
				xB = xc + R * System.Math.Cos(0.15 + Pi)
				yB = yc + R * System.Math.Sin(0.15 + Pi)
				xE = xc + R * System.Math.Cos(0.15)
				yE = yc + R * System.Math.Sin(0.15)
				xMid = xc + R * System.Math.Cos(0.15 + 3 * Pi / 2)
				yMid = yc + R * System.Math.Sin(0.15 + 3 * Pi / 2)
				With Segm(1)
					.SegType = "Arc" '# NLS#'
                    .xyBeg.v0 = xB
                    .xyBeg.v1 = yB
                    .xyEnd.v0 = xE
                    .xyEnd.v1 = yE
                    .xyCent.v0 = xc
                    .xyCent.v1 = yc
                    .R = R
                    .Rot = 1
                    .Distinct = 1
                    .xyMid.v0 = xMid
                    .xyMid.v1 = yMid
                    .Alp.v0 = 0.15 + 3 * Pi / 2
                    .Alp.v1 = 0.15 + Pi / 2
                    .UpDown = 1
                End With
            End If
        End If

        '     If GrType = "ARC" Then
        Dim cosa, sina, yLoc As Double
        Dim BetBeg, BetEnd As Double
        If GrType = ARCTYPE Then
            Gr.GetArcData(ArcData)
            Ratio = ArcData(6)
            If System.Math.Abs(Ratio - 1) > Eps Then
                GrType = "Ellipse"
                '######################################################
                If Smooth = 0 Then
                    Call InitializeSegmForSmallSegmentObject(Gr, Segm, Cosm, nSegm, nVCosm)
                    For i = 0 To nSegm - 1
                        Segm(i).UpDown = 1
                    Next i
                Else
                    Call SmoothEllipse(Gr, Cont)
                    nSegm = Cont.NumSegm
                    ReDim Segm(nSegm - 1)
                    For i = 0 To nSegm - 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object Segm(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Segm(i) = Cont.Segm(i)
                        Segm(i).UpDown = 1
                    Next i
                End If
                '######################################################
            Else
                nSegm = 1
                ReDim Segm(0)
                With Gr.Vertices
                    xc = .Item(0).X
                    yc = .Item(0).Y
                    xB = .Item(1).X
                    yB = .Item(1).Y
                    xE = .Item(2).X
                    yE = .Item(2).Y
                    xDir = .Item(3).X
                    yDir = .Item(3).Y
                End With
                R = System.Math.Sqrt((xB - xc) ^ 2 + (yB - yc) ^ 2)
                sina = (yB - yc) / R
                cosa = (xB - xc) / R
                yLoc = (yDir - yc) * cosa - (xDir - xc) * sina
                If yLoc > 0 Then
                    Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, 1, xMid, yMid)
                Else
                    Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, -1, xMid, yMid)
                End If
                With Segm(0)
                    .SegType = "Arc" '# NLS#'
                    If yLoc > 0 Then
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .xyEnd.v0 = xE
                        .xyEnd.v1 = yE
                    Else
                        .xyBeg.v0 = xE
                        .xyBeg.v1 = yE
                        .xyEnd.v0 = xB
                        .xyEnd.v1 = yB
                    End If
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1

                    .xyCent.v0 = xc
                    .xyCent.v1 = yc
                    .R = R
                    .xyMid.v0 = xMid
                    .xyMid.v1 = yMid
                    .Rot = 1
                    .Distinct = 1
                    Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, 1, BetBeg, BetEnd)
                    .Alp.v0 = BetBeg
                    .Alp.v1 = BetEnd
                    .UpDown = 1
                End With
            End If
        End If

        '    If GrType = "GRAPHIC" Or GrType = "TCW50Polyline" Then
        Dim GrChild As IMSIGX.Graphic
        Dim GrCount As Integer
        Dim GrName As String
        If GrType = GRAPHICTYPE Or GrType = "TCW50Polyline" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
            ' Initialize structure Poly
            GrChild = Gr
            If GrType = "TCW50Polyline" Or GrType = "TCW50IntProp" Then
                GrCount = Gr.Graphics.Count
                For i = 0 To GrCount - 1
                    GrChild = Gr.Graphics.Item(i)
                    GrName = GrChild.Name
                    If GrName = "Line_Width_Cosmetic" Then '# NLS#'
                        'Set Gr = GrChild
                        Exit For
                    End If
                Next i
            End If
            If GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Then
                GrCount = Gr.Graphics.Count
                For i = 0 To GrCount - 1
                    GrChild = Gr.Graphics.Item(i)
                    GrName = GrChild.Name
                    If GrChild.Type = GRAPHICTYPE And GrName = "" Then
                        'Set Gr = GrChild
                        Exit For
                    End If
                Next i
            End If

            Call InitializePoly(GrChild, Poly, nSegm, nVBase)
            ' Initialize structure Segm
            Call InitializeSegmForPolyline(Segm, Poly, nSegm)

        End If

        If GrType = "TCW30CURVE" Then
            '#####################################################
            If Smooth = 0 Then
                Call InitializeSegmForSmallSegmentObject(Gr, Segm, Cosm, nSegm, nVCosm)
                For i = 0 To nSegm - 1
                    Segm(i).UpDown = 1
                Next i
            Else
                Call SmoothCurve(Gr, Cont)
                nSegm = Cont.NumSegm
                ReDim Segm(nSegm - 1)
                For i = 0 To nSegm - 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object Segm(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Segm(i) = Cont.Segm(i)
                    Segm(i).UpDown = 1
                Next i
            End If
            '#####################################################
        End If

        ' **********************************************************************

    End Sub

    ' Initialize structure Cosm
    Private Sub InitializeCosm()
        nVCosm = 0
        If nSegm = 0 Then Exit Sub
        Dim i, j As Integer
        Dim SegType As String
        Dim yc, yE, yB, xB, xE, xc, R As Double
        Dim Rot As Short
        Dim sina, cosa As Double
        Dim xELoc, yELoc As Double
        Dim BetLoc As Double
        Dim iDiv As Short
        Dim dfi As Double
        Dim fiB, fi As Double

        xB = Segm(0).xyBeg.v0
        yB = Segm(0).xyBeg.v1
        ReDim Preserve Cosm.xV(nVCosm)
        ReDim Preserve Cosm.yV(nVCosm)
        Cosm.xV(nVCosm) = xB
        Cosm.yV(nVCosm) = yB
        nVCosm = nVCosm + 1

        For i = 0 To nSegm - 1

            With Segm(i)
                SegType = .SegType
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                R = .R
                Rot = .Rot
            End With
            If SegType = "Line" Then '# NLS#'
                ReDim Preserve Cosm.xV(nVCosm)
                ReDim Preserve Cosm.yV(nVCosm)
                Cosm.xV(nVCosm) = xE
                Cosm.yV(nVCosm) = yE
                nVCosm = nVCosm + 1
            End If
            If SegType = "Arc" Then '# NLS#'
                sina = (yB - yc) / R
                cosa = (xB - xc) / R
                fiB = Angle(sina, cosa)
                yELoc = (yE - yc) * cosa - (xE - xc) * sina
                xELoc = (yE - yc) * sina + (xE - xc) * cosa
                sina = yELoc / R
                cosa = xELoc / R
                BetLoc = Angle(sina, cosa)
                If Rot = -1 Then BetLoc = 2 * Pi - BetLoc
                'If Rot <> 0 Then BetLoc = 2 * Pi - BetLoc
                iDiv = Int(60 * BetLoc / (2 * Pi))
                If iDiv = 0 Then iDiv = 1
                dfi = BetLoc / iDiv
                For j = 1 To iDiv
                    fi = fiB + Rot * j * dfi
                    xE = xc + R * System.Math.Cos(fi)
                    yE = yc + R * System.Math.Sin(fi)
                    ReDim Preserve Cosm.xV(nVCosm)
                    ReDim Preserve Cosm.yV(nVCosm)
                    Cosm.xV(nVCosm) = xE
                    Cosm.yV(nVCosm) = yE
                    nVCosm = nVCosm + 1
                Next j
            End If
        Next i

    End Sub


    ' Initialize structure Poly for version TC7
    Private Sub InitializePoly(ByRef Gr As IMSIGX.Graphic, ByRef PolyTem As Polyline, ByRef nSegmTem As Integer, ByRef nVBaseTem As Integer)
        Dim Vers As IMSIGX.Vertices
        Dim i As Integer

        Vers = Gr.Vertices
        nVBaseTem = Vers.Count
        nSegmTem = 0
        i = 0
AA:
        If Vers.Item(i + 1).Bulge = True Then 'Arc segment
            nSegmTem = nSegmTem + 1
            i = i + 3
        Else ' Line segment
            nSegmTem = nSegmTem + 1
            i = i + 1
        End If
        If i < nVBaseTem - 1 Then GoTo AA

        ReDim PolyTem.xV(nVBaseTem - 1)
        ReDim PolyTem.yV(nVBaseTem - 1)
        ReDim PolyTem.vType(nVBaseTem - 1)
        ReDim PolyTem.iCosm(nVBaseTem - 1)
        ReDim PolyTem.UpDown(nVBaseTem - 1)
        PolyTem.nV = nVBaseTem
        PolyTem.nSeg = nSegmTem
        With Vers
            For i = 0 To nVBaseTem - 1
                PolyTem.xV(i) = .Item(i).X
                PolyTem.yV(i) = .Item(i).Y
                If .Item(i).Bulge = True Then
                    PolyTem.vType(i) = "Bulge" '# NLS#'
                Else
                    PolyTem.vType(i) = "Stan" '# NLS#'
                End If
                If .Item(i).PenDown = False Then
                    PolyTem.UpDown(i) = False
                Else
                    PolyTem.UpDown(i) = True
                End If
            Next i
        End With

        'UPGRADE_NOTE: Object Vers may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Vers)
        GXMD.Collect()
    End Sub



    ' Initialize structure Segm for polyline
    Private Sub InitializeSegmForPolyline(ByRef SegmTem() As ChainTool.Segment, ByRef PolyTem As Polyline, ByRef nSegmTem As Integer)
        Dim nVBaseTem As Integer
        nVBaseTem = PolyTem.nV
        ReDim SegmTem(nSegmTem)
        Dim i, iSegm As Integer
        Dim Bet, yc, yE, yB, xB, xE, xc, R, l As Double
        Dim xMid, yMid As Double
        Dim Rot As Short
        Dim Distinct As Short
        Dim BetBeg, BetEnd As Double


        iSegm = 0
        i = 0
BB:
        If PolyTem.vType(i + 1) = "Stan" Then 'Line segment'# NLS#'
            SegmTem(iSegm).Initialize()
            SegmTem(iSegm).SegType = "Line" '# NLS#'
            xB = PolyTem.xV(i)
            yB = PolyTem.yV(i)
            xE = PolyTem.xV(i + 1)
            yE = PolyTem.yV(i + 1)
            SegmTem(iSegm).xyBeg.v0 = xB
            SegmTem(iSegm).xyBeg.v1 = yB
            SegmTem(iSegm).xyEnd.v0 = xE
            SegmTem(iSegm).xyEnd.v1 = yE
            Call LineSegmPar(xB, yB, xE, yE, l, Bet)
            SegmTem(iSegm).l = l
            If System.Math.Abs(l) < Eps Then '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                i = i + 1
                GoTo LL
            End If
            SegmTem(iSegm).Alp.v0 = Bet
            SegmTem(iSegm).Alp.v1 = Bet
            SegmTem(iSegm).UpDown = 1
            If PolyTem.UpDown(i + 1) = False Then
                SegmTem(iSegm).UpDown = 0
            End If

            i = i + 1
            iSegm = iSegm + 1
        Else ' Arc segment
            SegmTem(iSegm).Initialize()
            SegmTem(iSegm).SegType = "Arc" '# NLS#'
            xB = PolyTem.xV(i)
            yB = PolyTem.yV(i)
            xE = PolyTem.xV(i + 3)
            yE = PolyTem.yV(i + 3)

            If System.Math.Abs(xE - xB) < Eps And System.Math.Abs(yE - yB) < Eps Then
                i = i + 3
                GoTo LL
            End If

            xc = PolyTem.xV(i + 1)
            yc = PolyTem.yV(i + 1)
            R = System.Math.Sqrt((xB - xc) * (xB - xc) + (yB - yc) * (yB - yc))
            SegmTem(iSegm).xyBeg.v0 = xB
            SegmTem(iSegm).xyBeg.v1 = yB
            SegmTem(iSegm).xyEnd.v0 = xE
            SegmTem(iSegm).xyEnd.v1 = yE
            SegmTem(iSegm).xyCent.v0 = xc
            SegmTem(iSegm).xyCent.v1 = yc
            SegmTem(iSegm).R = R
            SegmTem(iSegm).UpDown = 1

            xMid = PolyTem.xV(i + 2)
            yMid = PolyTem.yV(i + 2)

            Rot = ArcRotation(xB, yB, xMid, yMid, xc, yc, R)
            Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, xMid, yMid)
            SegmTem(iSegm).Rot = Rot
            SegmTem(iSegm).xyMid.v0 = xMid
            SegmTem(iSegm).xyMid.v1 = yMid

            ' Define the Distinct of Arc
            Distinct = ArcDictinct(xB, yB, xE, yE, xMid, yMid, Rot)
            SegmTem(iSegm).Distinct = Distinct


            ' ##########################################################################
            'Define angle of arc at Begining and Ending points
            Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, BetBeg, BetEnd)
            SegmTem(iSegm).Alp.v0 = BetBeg
            SegmTem(iSegm).Alp.v1 = BetEnd

            i = i + 3
            iSegm = iSegm + 1
        End If
LL:
        If i < nVBaseTem - 1 Then GoTo BB
        nSegmTem = iSegm

    End Sub





    'define begining and ending angle of arc
    Private Sub AnglesForArc(ByRef xB As Double, ByRef yB As Double, ByRef xE As Double, ByRef yE As Double, ByRef xc As Double, ByRef yc As Double, ByRef R As Double, ByRef Rot As Short, ByRef BetBeg As Double, ByRef BetEnd As Double)
        Dim cosa, sina, Alp As Double
        sina = (yB - yc) / R
        cosa = (xB - xc) / R
        Alp = Angle(sina, cosa)
        If Rot = 1 Then
            BetBeg = Pi / 2 + Alp
        Else
            BetBeg = 3 * Pi / 2 + Alp
        End If
        If BetBeg > 2 * Pi Then BetBeg = BetBeg - 2 * Pi

        sina = (yE - yc) / R
        cosa = (xE - xc) / R
        Alp = Angle(sina, cosa)
        If Rot = 1 Then
            BetEnd = Pi / 2 + Alp
        Else
            BetEnd = 3 * Pi / 2 + Alp
        End If
        If BetEnd > 2 * Pi Then BetEnd = BetEnd - 2 * Pi
    End Sub

    '###############################################################################
    '###############################################################################
    ' Small segments object - Curve,Ellipse,Simbols
    ' Initialize structure Segm for Small segments object
    Private Sub InitializeSegmForSmallSegmentObject(ByRef Gr As IMSIGX.Graphic, ByRef SegmTem() As ChainTool.Segment, ByRef CosmTem As Cosmetic, ByRef nSegmTem As Integer, ByRef nVCosmTem As Integer)
        Dim i As Integer
        Dim nG As Integer
        Dim GrType As String
        Dim GrTem As IMSIGX.Graphic
        GrType = Gr.Type
        If GrType = "TCW30CURVE" Then
            nG = Gr.Graphics.Count
            For i = 0 To nG - 1
                GrTem = Gr.Graphics.Item(i)
                '            If GrTem.Type = "GRAPHIC" Then
                If GrTem.Type = GRAPHICTYPE Then
                    Exit For
                End If
            Next i
        End If
        '    If GrType = "CIRCLE" Or GrType = "ARC" Then
        If GrType = CIRCLETYPE Or GrType = ARCTYPE Then
            'Set GrTem = Gr.Duplicate
            'Grs.Remove GrTem.Index
            GrTem = Gr.Clone(oMissing)
            If GrTem.Graphics.Count = 1 Then
                If GrTem.Graphics.Item(0).Type = "DATA" Then
                    GrTem.Graphics.Item(0).Deleted = True
                End If
            End If
            'GXMD.Release(GrTem)
            GrTem = GrTem.Explode().Item(0)
        End If

        If GrTem Is Nothing Then Exit Sub

        Dim Vers As IMSIGX.Vertices
        Vers = GrTem.Vertices
        nVCosmTem = Vers.Count
        CosmTem.nV = nVCosmTem
        ReDim CosmTem.xV(nVCosmTem)
        ReDim CosmTem.yV(nVCosmTem)
#If False Then
        With Vers
            For i = 0 To nVCosmTem - 1
                CosmTem.xV(i) = .Item(i).X
                CosmTem.yV(i) = .Item(i).Y
            Next i
        End With
#Else
        Dim gxVrt As IMSIGX.Vertex
        i = 0
        For Each gxVrt In Vers

            CosmTem.xV(i) = gxVrt.X
            CosmTem.yV(i) = gxVrt.Y
            i = i + 1
        Next

        GXMD.Release(gxVrt)
        GXMD.Collect()
#End If


        ReDim SegmTem(nVCosmTem)
        Dim iSegm As Integer
        Dim Bet, xE, xB, yB, yE, l As Double

        iSegm = 0
        For i = 0 To nVCosmTem - 2
            SegmTem(iSegm).SegType = "Line" '# NLS#'
            xB = CosmTem.xV(i)
            yB = CosmTem.yV(i)
            xE = CosmTem.xV(i + 1)
            yE = CosmTem.yV(i + 1)
            SegmTem(iSegm).xyBeg.v0 = xB
            SegmTem(iSegm).xyBeg.v1 = yB
            SegmTem(iSegm).xyEnd.v0 = xE
            SegmTem(iSegm).xyEnd.v1 = yE
            Call LineSegmPar(xB, yB, xE, yE, l, Bet)
            SegmTem(iSegm).l = l
            SegmTem(iSegm).Alp.v0 = Bet
            SegmTem(iSegm).Alp.v1 = Bet
            SegmTem(iSegm).UpDown = 1
            iSegm = iSegm + 1
        Next i


        '    If GrType = "CIRCLE" Or GrType = "ARC" Then
        If GrType = CIRCLETYPE Or GrType = ARCTYPE Then
            GrTem.Deleted = True
        End If

        nSegmTem = iSegm
        'UPGRADE_NOTE: Object Vers may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Vers)
        'UPGRADE_NOTE: Object GrTem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrTem)
        GXMD.Collect()
    End Sub



    ' Define the length and angle of line segment
    Private Sub LineSegmPar(ByRef x0 As Double, ByRef y0 As Double, ByRef x1 As Double, ByRef y1 As Double, ByRef l As Double, ByRef Alp As Double)
        Dim sina, cosa As Double
        l = System.Math.Sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0))
        If System.Math.Abs(l) < Eps Then
            l = 0
            Alp = 0
            Exit Sub
        End If
        sina = (y1 - y0) / l
        cosa = (x1 - x0) / l
        Alp = Angle(sina, cosa)
    End Sub


    ' Define the direction of Arc rotation
    ' return ArcRotation = 1 if CounterClockwise
    ' return ArcRotation = -1 if Clockwise

    Private Function ArcRotation(ByRef x0 As Double, ByRef y0 As Double, ByRef x1 As Double, ByRef y1 As Double, ByRef xc As Double, ByRef yc As Double, ByRef R As Double) As Short
        Dim sina, cosa As Double ', Alp0#, Alp1#
        sina = (y0 - yc) / R
        cosa = (x0 - xc) / R
        Dim xLoc, yLoc As Double
        xLoc = (y1 - yc) * sina + (x1 - xc) * cosa
        yLoc = (y1 - yc) * cosa - (x1 - xc) * sina
        If yLoc > 0 Then
            ArcRotation = 1
        Else
            ArcRotation = -1
        End If
    End Function
    ' Define the middle point of Base Arc
    ' from Beg-End Points and Rot
    ' Rot=1 - Counter Clockwise
    ' Rot=-1 - Clockwise
    Private Sub ArcMiddlePoint(ByRef xc As Double, ByRef yc As Double, ByRef xB As Double, ByRef yB As Double, ByRef xE As Double, ByRef yE As Double, ByRef R As Double, ByRef Rot As Short, ByRef xMid As Double, ByRef yMid As Double)
        If System.Math.Abs(R) < Eps Then
            xMid = xc
            yMid = yc
            Exit Sub
        End If

        Dim sina, cosa As Double
        Dim fiEnd, fiBeg, fiMid As Double
        sina = (yB - yc) / R
        cosa = (xB - xc) / R
        fiBeg = Angle(sina, cosa)

        sina = (yE - yc) / R
        cosa = (xE - xc) / R
        fiEnd = Angle(sina, cosa)

        If Rot = 1 Then ' Counter Clockwise
            If fiBeg > fiEnd Then fiEnd = fiEnd - 2 * Pi
        Else 'Clockwise
            If fiBeg < fiEnd Then fiEnd = fiEnd - 2 * Pi
        End If

        fiMid = (fiBeg + fiEnd) / 2
        xMid = xc + R * System.Math.Cos(fiMid)
        yMid = yc + R * System.Math.Sin(fiMid)


    End Sub


    ' Define distinct of Arc
    Private Function ArcDictinct(ByRef xB As Double, ByRef yB As Double, ByRef xE As Double, ByRef yE As Double, ByRef xj As Double, ByRef yj As Double, ByRef Rot As Short) As Short
        Dim sina, cosa, l As Double
        Dim yLoc As Double
        l = System.Math.Sqrt((xE - xB) * (xE - xB) + (yE - yB) * (yE - yB))
        If System.Math.Abs(l) < Eps Then
            ArcDictinct = 1
            Exit Function
        End If
        sina = (yE - yB) / l
        cosa = (xE - xB) / l
        yLoc = (yj - yB) * cosa - (xj - xB) * sina
        If yLoc > 0 Then
            If Rot = -1 Then
                'If Rot <> 0 Then
                ArcDictinct = 1
            Else
                ArcDictinct = -1
            End If
        Else
            If Rot = -1 Then
                'If Rot <> 0 Then
                ArcDictinct = -1
            Else
                ArcDictinct = 1
            End If
        End If

    End Function


    ' Define direction of the contour
    ' Return  1 - Counter Clockwise
    '        -1 - Clockwise
    Private Function ContourDirection(ByRef Cont As ChainTool.Contour) As Short
        Dim l, yMid, yE, yB, xB, xE, xMid, fi As Double
        Dim xTem, xLoc, sina, cosa, yLoc, yTem As Double
        If Cont.Segm(0).SegType = "Circle" Then '# NLS#'
            ContourDirection = 1
            Exit Function
        End If

        If Cont.Segm(0).SegType = "Line" Then '# NLS#'
            xB = Cont.Segm(0).xyBeg.v0
            yB = Cont.Segm(0).xyBeg.v1
            xE = Cont.Segm(0).xyEnd.v0
            yE = Cont.Segm(0).xyEnd.v1
            l = Cont.Segm(0).l
            fi = Cont.Segm(0).Alp.v0
            sina = System.Math.Sin(fi)
            cosa = System.Math.Cos(fi)
            xLoc = 0
            yLoc = 1 * Eps
            xMid = (xE + xB) / 2
            yMid = (yE + yB) / 2
            xTem = xMid + xLoc * cosa - yLoc * sina
            yTem = yMid + xLoc * sina + yLoc * cosa
        End If

        If Cont.Segm(0).SegType = "Arc" Then '# NLS#'
            xB = Cont.Segm(0).xyBeg.v0
            yB = Cont.Segm(0).xyBeg.v1
            xE = Cont.Segm(0).xyEnd.v0
            yE = Cont.Segm(0).xyEnd.v1
            xMid = Cont.Segm(0).xyMid.v0
            yMid = Cont.Segm(0).xyMid.v1
            l = System.Math.Sqrt((xE - xB) ^ 2 + (yE - yB) ^ 2)
            sina = (yE - yB) / l
            cosa = (xE - xB) / l
            xLoc = 0
            yLoc = 1 * Eps
            xTem = xMid + xLoc * cosa - yLoc * sina
            yTem = yMid + xLoc * sina + yLoc * cosa
        End If

        Dim PointInto As New PointIntoContour

        Dim Res As Short
        Res = PointInto.PointInsideContour(Cont, xTem, yTem)
        If Res = 1 Then
            ContourDirection = 1
        Else
            ContourDirection = -1
        End If
        'UPGRADE_NOTE: Object PointInto may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        PointInto = Nothing
        GXMD.Collect()
    End Function

    '  Invert the direction of the Contour

    Private Sub InvertContour(ByRef Cont As ChainTool.Contour)
        Dim Type1 As String
        Dim fiEnd, l, xc, xMid, xE, xB, yB, yE, yMid, yc, fiBeg, R As Double
        Dim Rot, Dist As Short

        'UPGRADE_WARNING: Arrays in structure ContTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContTem As New ChainTool.Contour
        'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContTem.CopyFrom(Cont)
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New ChainTool.Segment
        'UPGRADE_WARNING: Arrays in structure SegmTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim SegmTem As ChainTool.Segment

        Dim i As Integer
        For i = ContTem.NumSegm - 1 To 0 Step -1
            'UPGRADE_WARNING: Couldn't resolve default property of object SegmTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SegmTem = ContTem.Segm(i)
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = SegmTem
            With Segm
                Type1 = .SegType
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                l = .l
                fiBeg = .Alp.v0
                fiEnd = .Alp.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                R = .R
                Rot = .Rot
                Dist = .Distinct
                If Type1 = "Line" Then fiEnd = fiBeg

                .xyBeg.v0 = xE
                .xyBeg.v1 = yE
                .xyEnd.v0 = xB
                .xyEnd.v1 = yB

                fiBeg = fiBeg + Pi
                If fiBeg > 2 * Pi Then fiBeg = fiBeg - 2 * Pi
                .Alp.v1 = fiBeg

                fiEnd = fiEnd + Pi
                If fiEnd > 2 * Pi Then fiEnd = fiEnd - 2 * Pi
                .Alp.v0 = fiEnd
                .Rot = -Rot

                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                l = .l
                fiBeg = .Alp.v0
                fiEnd = .Alp.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                R = .R
                Rot = .Rot
                Dist = ArcDictinct(xB, yB, xE, yE, xMid, yMid, Rot)
                Dist = .Distinct

            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(ContTem.NumSegm - 1 - i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(ContTem.NumSegm - 1 - i) = Segm
        Next i

    End Sub



    Private Function Angle(ByRef sinB As Double, ByRef cosB As Double) As Double
        Dim Eps1 As Double
        Eps1 = 0.0000000000001
        If System.Math.Abs(cosB) < Eps1 Then
            If sinB > Eps1 Then
                Angle = Pi / 2
            Else
                Angle = 3 * Pi / 2
            End If
        Else
            If System.Math.Abs(sinB) < Eps1 Then
                If cosB > Eps1 Then
                    Angle = 0
                Else
                    Angle = Pi
                End If
            Else
                If sinB >= 0 And cosB > 0 Then
                    Angle = System.Math.Atan(sinB / cosB)
                Else
                    If sinB >= 0 And cosB < 0 Then
                        Angle = Pi + System.Math.Atan(sinB / cosB)
                    Else
                        If sinB < 0 And cosB < 0 Then
                            Angle = Pi + System.Math.Atan(sinB / cosB)
                        Else
                            If sinB < 0 And cosB > 0 Then
                                Angle = 2 * Pi + System.Math.Atan(sinB / cosB)
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Function


    Private Function Sign(ByRef y As Double) As Double
        If System.Math.Abs(y) < 0.000001 Then
            Sign = 0
            Exit Function
        End If
        If y < 0 Then
            Sign = -1.0#
            Exit Function
        End If
        If y > 0 Then
            Sign = 1.0#
            Exit Function
        End If
    End Function


    '###############################################################
    '###############################################################
    '###############################################################
    '###############################################################
    '###############################################################

    Private Function SysLine(ByRef a1 As Double, ByRef b1 As Double, ByRef d1 As Double, ByRef a2 As Double, ByRef b2 As Double, ByRef d2 As Double, ByRef xRes As Double, ByRef yRes As Double) As Boolean

        Dim delx, del, dely As Double
        On Error GoTo ErrorHandler

        del = a1 * b2 - a2 * b1
        delx = d1 * b2 - b1 * d2
        dely = a1 * d2 - a2 * d1
        If System.Math.Abs(del) < Eps / 100 Then
            SysLine = False
            Exit Function
        End If
        xRes = delx / del
        yRes = dely / del
        SysLine = True
        Exit Function
ErrorHandler:
        '    MsgBox Err.Description
        SysLine = False
    End Function


    ' Define The Nearest point on the contour and reenumetate it
    Private Sub ReEnumContour(ByRef Cont As ChainTool.Contour, ByRef x0 As Double, ByRef y0 As Double)

        Dim i, iNearest As Integer
        Dim xB, yB As Double
        Dim l, LMin As Double
        LMin = 10000000
        For i = 0 To Cont.NumSegm - 1
            If Cont.Segm(i).UpDown = 1 Then
                xB = Cont.Segm(i).xyBeg.v0
                yB = Cont.Segm(i).xyBeg.v1
                l = System.Math.Sqrt((xB - x0) ^ 2 + (yB - y0) ^ 2)
                If l < LMin Then
                    LMin = l
                    iNearest = i
                End If
            End If
        Next i

        'UPGRADE_WARNING: Arrays in structure ContTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContTem As New ChainTool.Contour
        'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContTem.CopyFrom(Cont)
        ReDim Cont.Segm(0)
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As ChainTool.Segment
        Dim j As Integer
        j = 0
        For i = iNearest To ContTem.NumSegm - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = ContTem.Segm(i)
            ReDim Preserve Cont.Segm(j)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(j). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(j) = Segm
            j = j + 1
        Next i
        For i = 0 To iNearest - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = ContTem.Segm(i)
            ReDim Preserve Cont.Segm(j)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(j). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(j) = Segm
            j = j + 1
        Next i
        Cont.NumSegm = j

    End Sub


    Private Sub SmoothContour(ByRef Cont0 As ChainTool.Contour, ByRef MaxExt As Double)

        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As ChainTool.Contour
        'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Cont = Cont0
        Dim iNext, i, iCur, iPrev As Integer

        'UPGRADE_WARNING: Arrays in structure SegmNext may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        'UPGRADE_WARNING: Arrays in structure SegmCur may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        'UPGRADE_WARNING: Arrays in structure SegmPrev may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim SegmPrev, SegmCur, SegmNext As ChainTool.Segment

        Dim SegType As String
        Dim Alp1, R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp2 As Double
        Dim Rot, Dict As Short
        Dim ContClosed As Boolean
        xB = Cont.Segm(0).xyBeg.v0
        yB = Cont.Segm(0).xyBeg.v1
        xE = Cont.Segm(Cont.NumSegm - 1).xyEnd.v0
        yE = Cont.Segm(Cont.NumSegm - 1).xyEnd.v1
        ContClosed = False
        If System.Math.Abs(xB - xE) < Eps And System.Math.Abs(yB - yE) < Eps Then ContClosed = True

        Dim SegTypeP As String
        Dim Alp1P, Rp, xMidP, xcP, xEP, xBP, yBP, yEP, ycP, yMidP, LP, Alp2P As Double
        Dim RotP, DictP As Short

        Dim SegTypeN As String
        Dim Alp1N, RN, xMidN, xcN, xEN, xBN, yBN, yEN, ycN, yMidN, LN, Alp2N As Double
        Dim RotN, DictN As Short

        Dim xLoc, yLoc As Double
        Dim b2, d1, a1, b1, a2, d2 As Double
        Dim BetLocP, BetLocN As Double
        Dim TanPrev As Boolean
        Dim TanNext As Boolean

        For i = 0 To Cont.NumSegm - 1
            iCur = i
            iPrev = iCur - 1
            If iPrev = -1 Then
                If ContClosed = True Then
                    iPrev = Cont.NumSegm - 1
                Else
                    iPrev = 0
                End If
            End If

            iNext = iCur + 1
            If iNext = Cont.NumSegm Then
                If ContClosed = True Then
                    iNext = 0
                Else
                    iNext = Cont.NumSegm - 1
                End If
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object SegmCur. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SegmCur = Cont.Segm(iCur)
            'UPGRADE_WARNING: Couldn't resolve default property of object SegmPrev. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SegmPrev = Cont.Segm(iPrev)
            'UPGRADE_WARNING: Couldn't resolve default property of object SegmNext. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SegmNext = Cont.Segm(iNext)

            With SegmCur
                SegType = .SegType
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                l = .l
                R = .R
                Rot = .Rot
                Dict = .Distinct
                Alp1 = .Alp.v0
                Alp2 = .Alp.v1
                If SegType = "Line" Then '# NLS#'
                    Alp2 = Alp1
                End If
            End With

            With SegmPrev
                SegTypeP = .SegType
                xBP = .xyBeg.v0
                yBP = .xyBeg.v1
                xEP = .xyEnd.v0
                yEP = .xyEnd.v1
                xcP = .xyCent.v0
                ycP = .xyCent.v1
                xMidP = .xyMid.v0
                yMidP = .xyMid.v1
                LP = .l
                Rp = .R
                RotP = .Rot
                DictP = .Distinct
                Alp1P = .Alp.v0
                Alp2P = .Alp.v1
                If SegTypeP = "Line" Then '# NLS#'
                    Alp2P = Alp1P
                End If
            End With

            With SegmNext
                SegTypeN = .SegType
                xBN = .xyBeg.v0
                yBN = .xyBeg.v1
                xEN = .xyEnd.v0
                yEN = .xyEnd.v1
                xcN = .xyCent.v0
                ycN = .xyCent.v1
                xMidN = .xyMid.v0
                yMidN = .xyMid.v1
                LN = .l
                RN = .R
                RotN = .Rot
                DictN = .Distinct
                Alp1N = .Alp.v0
                Alp2N = .Alp.v1
                If SegTypeN = "Line" Then '# NLS#'
                    Alp2N = Alp1N
                End If
            End With

            If SegType <> "Line" Then GoTo II '# NLS#'
            If l > MaxExt / 5 Then GoTo II

            TanPrev = False
            TanNext = False

            If iPrev <> iCur Then
                xLoc = System.Math.Sin(Alp1) * System.Math.Sin(Alp2P) + System.Math.Cos(Alp1) * System.Math.Cos(Alp2P)
                yLoc = System.Math.Sin(Alp1) * System.Math.Cos(Alp2P) - System.Math.Cos(Alp1) * System.Math.Sin(Alp2P)
                BetLocP = Angle(yLoc, xLoc)
                If System.Math.Abs(System.Math.Cos(BetLocP) - 1) < Pi / 18 Then
                    TanPrev = True
                End If
            End If

            If iNext <> iCur Then
                xLoc = System.Math.Sin(Alp1N) * System.Math.Sin(Alp2) + System.Math.Cos(Alp1N) * System.Math.Cos(Alp2)
                yLoc = System.Math.Sin(Alp1N) * System.Math.Cos(Alp2) - System.Math.Cos(Alp1N) * System.Math.Sin(Alp2)
                BetLocN = Angle(yLoc, xLoc)
                If System.Math.Abs(System.Math.Cos(BetLocN) - 1) < Pi / 18 Then
                    TanNext = True
                End If
            End If

            If TanPrev = True Then

                '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                ' first point
                If SegTypeP = "Line" Then '# NLS#'
                    ' create arc from 3 points
                    xMid = (xB + xE) / 2
                    yMid = (yB + yE) / 2
                    a1 = -System.Math.Sin(Alp1 + Pi / 2)
                    b1 = System.Math.Cos(Alp1 + Pi / 2)
                    d1 = a1 * xMid + b1 * yMid

                    xMidP = (xBP + xEP) / 2
                    yMidP = (yBP + yEP) / 2
                    a2 = -System.Math.Sin(Alp1P + Pi / 2)
                    b2 = System.Math.Cos(Alp1P + Pi / 2)
                    d2 = a2 * xMidP + b2 * yMidP
                    If SysLine(a1, b1, d1, a2, b2, d2, xc, yc) = True Then
                        R = System.Math.Sqrt((xB - xc) ^ 2 + (yB - yc) ^ 2)
                        Rot = ArcRotation(xBP, yBP, xB, yB, xc, yc, R)
                        Call AnglesForArc(xBP, yBP, xEP, yEP, xc, yc, R, Rot, Alp1P, Alp2P)
                    End If
                End If
                '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                xMid = (xB + xE) / 2
                yMid = (yB + yE) / 2
                a1 = -System.Math.Sin(Alp2P + Pi / 2)
                b1 = System.Math.Cos(Alp2P + Pi / 2)
                d1 = a1 * xB + b1 * yB
                a2 = -System.Math.Sin(Alp1 + Pi / 2)
                b2 = System.Math.Cos(Alp1 + Pi / 2)
                d2 = a2 * xMid + b2 * yMid
                If SysLine(a1, b1, d1, a2, b2, d2, xc, yc) = False Then GoTo II
                R = System.Math.Sqrt((xB - xc) ^ 2 + (yB - yc) ^ 2)
                Rot = ArcRotation(xB, yB, xMid, yMid, xc, yc, R)
                Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, xMid, yMid)
                Dict = ArcDictinct(xB, yB, xE, yE, xMid, yMid, Rot)

                '???????????????????????????????????????
                If SegTypeP = "Arc" Then '# NLS#'
                    If RotP <> Rot Then
                        TanPrev = False
                        GoTo III
                    End If
                End If
                '???????????????????????????????????????

                Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, Alp1, Alp2)

                With Cont.Segm(iCur)
                    .SegType = "Arc" '# NLS#'
                    .xyCent.v0 = xc
                    .xyCent.v1 = yc
                    .xyMid.v0 = xMid
                    .xyMid.v1 = yMid
                    .R = R
                    .Rot = Rot
                    .Distinct = Dict
                    .Alp.v0 = Alp1
                    .Alp.v1 = Alp2
                End With
                GoTo II
            End If 'If TanPrev = True


III:
            If TanPrev = False And TanNext = True Then
                If SegTypeN = "Line" Then '# NLS#'
                    ' create arc from 3 points
                    xMid = (xB + xE) / 2
                    yMid = (yB + yE) / 2
                    a1 = -System.Math.Sin(Alp1 + Pi / 2)
                    b1 = System.Math.Cos(Alp1 + Pi / 2)
                    d1 = a1 * xMid + b1 * yMid

                    xMidN = (xBN + xEN) / 2
                    yMidN = (yBN + yEN) / 2
                    a2 = -System.Math.Sin(Alp1N + Pi / 2)
                    b2 = System.Math.Cos(Alp1N + Pi / 2)
                    d2 = a2 * xMidN + b2 * yMidN
                    If SysLine(a1, b1, d1, a2, b2, d2, xc, yc) = True Then
                        R = System.Math.Sqrt((xB - xc) ^ 2 + (yB - yc) ^ 2)
                        Rot = ArcRotation(xB, yB, xE, yE, xc, yc, R)
                        Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, xMid, yMid)
                        Dict = ArcDictinct(xB, yB, xE, yE, xMid, yMid, Rot)
                        Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, Alp1, Alp2)
                        With Cont.Segm(iCur)
                            .SegType = "Arc" '# NLS#'
                            .xyCent.v0 = xc
                            .xyCent.v1 = yc
                            .xyMid.v0 = xMid
                            .xyMid.v1 = yMid
                            .R = R
                            .Rot = Rot
                            .Distinct = Dict
                            .Alp.v0 = Alp1
                            .Alp.v1 = Alp2
                        End With
                    End If
                End If



            End If 'If TanPrev = False And TanNext = True
II:
        Next i

        Call CalcSegmParam(Cont)

        'UPGRADE_WARNING: Couldn't resolve default property of object Cont0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Cont0 = Cont

    End Sub


    ' Smooth Ellipse or Elliptical Arcs

    Private Sub SmoothEllipse(ByRef GrCont As IMSIGX.Graphic, ByRef Cont As ChainTool.Contour)

        Dim i As Integer
        Dim xDir, xc0, xE0, xB0, yB0, yE0, yc0, yDir As Double
        Dim SegType As String
        Dim Alp1, R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp2 As Double
        Dim Rot As Short
        Dim xa, ya As Double
        Dim Ratio As Double
        Dim fiBeg, fiEnd As Double
        Dim a, b As Double
        Dim ContClosed As Boolean
        Dim ArcData(9) As Double
        'UPGRADE_WARNING: Arrays in structure Cont1 may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont1 As ChainTool.Contour
        ReDim Cont1.Segm(0)
        Cont1.NumSegm = 0
        'UPGRADE_WARNING: Arrays in structure Cont2 may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont2 As ChainTool.Contour
        ReDim Cont2.Segm(0)
        Cont2.NumSegm = 0
        'UPGRADE_WARNING: Arrays in structure Cont3 may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont3 As ChainTool.Contour
        ReDim Cont3.Segm(0)
        Cont3.NumSegm = 0
        'UPGRADE_WARNING: Arrays in structure Cont4 may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont4 As ChainTool.Contour
        ReDim Cont4.Segm(0)
        Cont4.NumSegm = 0

        ReDim Cont.Segm(0)
        Cont.NumSegm = 0


        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New ChainTool.Segment
        Segm.Initialize()

        GrCont.GetArcData(ArcData)

        xc0 = ArcData(0)
        yc0 = ArcData(1)

        xa = ArcData(3)
        ya = ArcData(4)

        Ratio = ArcData(6)

        fiBeg = ArcData(7)
        fiEnd = ArcData(8)

        ContClosed = False
        If ArcData(9) = 1 Then ContClosed = True

        a = System.Math.Sqrt((xa - xc0) ^ 2 + (ya - yc0) ^ 2)
        b = a * Ratio
        Dim sina, cosa As Double
        Dim xLoc, yLoc As Double

        If Ratio > 1 Then
            sina = (ya - yc0) / a
            cosa = (xa - xc0) / a
            xLoc = 0
            yLoc = b
            xa = xc0 + xLoc * cosa - yLoc * sina
            ya = yc0 + xLoc * sina + yLoc * cosa
            a = b
            b = a / Ratio
        End If

        Dim Beta As Double
        sina = (ya - yc0) / a
        cosa = (xa - xc0) / a
        Beta = Angle(sina, cosa)

        Dim BetLocBeg, BetLocEnd As Double
        Dim sinB, cosB As Double
        With GrCont.Vertices
            xB0 = .Item(1).X
            yB0 = .Item(1).Y
            xE0 = .Item(2).X
            yE0 = .Item(2).Y
            xDir = .Item(3).X
            yDir = .Item(3).Y
        End With

        R = System.Math.Sqrt((xB0 - xc0) ^ 2 + (yB0 - yc0) ^ 2)
        sina = (yB0 - yc0) / R
        cosa = (xB0 - xc0) / R
        yLoc = (yDir - yc0) * cosa - (xDir - xc0) * sina

        Rot = 1
        If yLoc < 0 Then Rot = -1

        sina = System.Math.Sin(Beta)
        cosa = System.Math.Cos(Beta)

        R = System.Math.Sqrt((xB0 - xc0) ^ 2 + (yB0 - yc0) ^ 2)
        yLoc = (yB0 - yc0) * cosa - (xB0 - xc0) * sina
        xLoc = (yB0 - yc0) * sina + (xB0 - xc0) * cosa

        sinB = yLoc / R
        cosB = xLoc / R
        BetLocBeg = Angle(sinB, cosB)

        R = System.Math.Sqrt((xE0 - xc0) ^ 2 + (yE0 - yc0) ^ 2)
        yLoc = (yE0 - yc0) * cosa - (xE0 - xc0) * sina
        xLoc = (yE0 - yc0) * sina + (xE0 - xc0) * cosa

        sinB = yLoc / R
        cosB = xLoc / R
        BetLocEnd = Angle(sinB, cosB)

        If ContClosed = True Then
            BetLocBeg = 0
            BetLocEnd = 0
        End If

        Dim dfi, fi, fiPrev As Double
        Dim NumDiv As Short
        NumDiv = 60
        If 2 * a < 1 Then
            NumDiv = 36
        Else
            If 2 * a < 20 Then
                NumDiv = 60
            Else
                If 2 * a < 100 Then
                    NumDiv = 100
                Else
                    If 2 * a < 1000 Then
                        NumDiv = 164
                    Else
                        NumDiv = 180
                    End If
                End If
            End If
        End If


        dfi = 2 * Pi / NumDiv

        Dim Alp, AlpP As Double
        Dim TetSum As Double
        TetSum = 0
        Dim St As Double
        St = 3.8 '1.3
        If Ratio > 0.1 Then
            St = -3.5 * Ratio + 3.05
        Else
            St = -22 * Ratio + 4.9
        End If


        If St < 1 Then St = 1

        For fi = dfi To Pi / 2 + Eps Step dfi

            fi = fi
            '        Alp = fi
            '        Alp = 2 / Pi * fi ^ 2 'Parabolic
            Alp = Pi / 2 * (2 / Pi) ^ St * fi ^ St

            fiPrev = fi - dfi
            '        AlpP = fiPrev
            '        AlpP = 2 / Pi * fiPrev ^ 2 'Parabolic
            AlpP = Pi / 2 * (2 / Pi) ^ St * fiPrev ^ St

            '        If Abs(Sin(Alp)) < Eps Then
            '            If Cos(Alp) > 0 Then
            '                Tet = Pi / 2
            '            Else
            '                Tet = 3 * Pi / 2
            '            End If
            '        Else
            '            Tet = Pi - Atn(b / a / Tan(Alp))
            '        End If

            '       If Abs(Sin(AlpP)) < Eps Then
            '            If Cos(AlpP) > 0 Then
            '                TetPrev = Pi / 2
            '            Else
            '                TetPrev = 3 * Pi / 2
            '            End If
            '        Else
            '            TetPrev = Pi - Atn(b / a / Tan(AlpP))
            '        End If

            '        dTet = (Tet - TetPrev) * 180 / Pi

            xB = a * System.Math.Cos(AlpP)
            yB = b * System.Math.Sin(AlpP)
            xE = a * System.Math.Cos(Alp)
            yE = b * System.Math.Sin(Alp)

            With Segm
                .SegType = "Line" '# NLS#'
                .xyBeg.v0 = xB
                .xyBeg.v1 = yB
                .xyEnd.v0 = xE
                .xyEnd.v1 = yE
                Call LineSegmPar(xB, yB, xE, yE, .l, .Alp.v0)
            End With
            ReDim Preserve Cont1.Segm(Cont1.NumSegm)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont1.Segm(Cont1.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont1.Segm(Cont1.NumSegm) = Segm
            Cont1.NumSegm = Cont1.NumSegm + 1

            ReDim Preserve Cont.Segm(Cont.NumSegm)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(Cont.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(Cont.NumSegm) = Segm
            Cont.NumSegm = Cont.NumSegm + 1

            '        Set Gr = Grs.AddCross(xE, yE, 0)
            '        Gr.Properties.Item("PenColor") = RGB(255, 0, 0)
            '        Gr.Draw
            '        TetSum = TetSum + dTet
        Next fi



        For i = Cont1.NumSegm - 1 To 0 Step -1
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = Cont1.Segm(i)
            With Segm
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1

                .xyBeg.v0 = -xE
                .xyBeg.v1 = yE
                .xyEnd.v0 = -xB
                .xyEnd.v1 = yB

                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1

                Call LineSegmPar(xB, yB, xE, yE, l, Alp)
                .l = l
                .Alp.v0 = Alp
            End With

            ReDim Preserve Cont2.Segm(Cont2.NumSegm)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont2.Segm(Cont2.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont2.Segm(Cont2.NumSegm) = Segm
            Cont2.NumSegm = Cont2.NumSegm + 1

            ReDim Preserve Cont.Segm(Cont.NumSegm)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(Cont.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(Cont.NumSegm) = Segm
            Cont.NumSegm = Cont.NumSegm + 1
        Next i



        For i = Cont2.NumSegm - 1 To 0 Step -1
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = Cont2.Segm(i)
            With Segm
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1

                .xyBeg.v0 = xE
                .xyBeg.v1 = -yE
                .xyEnd.v0 = xB
                .xyEnd.v1 = -yB

                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1

                Call LineSegmPar(xB, yB, xE, yE, l, Alp)
                .l = l
                .Alp.v0 = Alp
            End With

            ReDim Preserve Cont3.Segm(Cont3.NumSegm)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont3.Segm(Cont3.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont3.Segm(Cont3.NumSegm) = Segm
            Cont3.NumSegm = Cont3.NumSegm + 1

            ReDim Preserve Cont.Segm(Cont.NumSegm)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(Cont.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(Cont.NumSegm) = Segm
            Cont.NumSegm = Cont.NumSegm + 1
        Next i



        For i = Cont3.NumSegm - 1 To 0 Step -1
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = Cont3.Segm(i)
            With Segm
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1

                .xyBeg.v0 = -xE
                .xyBeg.v1 = yE
                .xyEnd.v0 = -xB
                .xyEnd.v1 = yB

                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1

                Call LineSegmPar(xB, yB, xE, yE, l, Alp)
                .l = l
                .Alp.v0 = Alp
            End With

            ReDim Preserve Cont4.Segm(Cont4.NumSegm)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont4.Segm(Cont4.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont4.Segm(Cont4.NumSegm) = Segm
            Cont4.NumSegm = Cont4.NumSegm + 1

            ReDim Preserve Cont.Segm(Cont.NumSegm)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(Cont.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(Cont.NumSegm) = Segm
            Cont.NumSegm = Cont.NumSegm + 1
        Next i

        'Dim MaxExt#, dx#, dy#
        '    MaxExt = 0
        '    With GrCont.CalcBoundingBox
        '        dx = .Max.x - .Min.x
        '        dy = .Max.y - .Min.y
        '        If dx > MaxExt Then MaxExt = dx
        '        If dy > MaxExt Then MaxExt = dy
        '    End With


FULLELLIPSE:
        Call SmoothContour(Cont, 2 * a)

        '-------------------------------------------------
        ' Define Max diviation from precision Ellipse
        'Dim Delta#, Del#
        'Dim iMax As Long
        '    Delta = 0
        '    For i = 0 To Cont.NumSegm - 1
        '        If Cont.Segm(i).SegType = "Arc" Then
        '            xMid = Cont.Segm(i).xyMid.v0
        '            yMid = Cont.Segm(i).xyMid.v1
        '            R = Sqr(xMid ^ 2 + yMid ^ 2)
        '            sina = yMid / R
        '            cosa = xMid / R
        '            Alp = Angle(sina, cosa)
        '            fi = Atn(a / b * Tan(Alp))
        '            If Alp > Pi / 2 And Alp < Pi Then
        '                fi = Pi + fi
        '            End If
        '            If Alp > Pi And Alp < 3 * Pi / 2 Then
        '                fi = Pi + fi
        '            End If
        '            If Alp > 3 * Pi / 2 And Alp < 2 * Pi Then
        '                fi = 2 * Pi + fi
        '            End If
        '            xi = a * Cos(fi)
        '            yi = b * Sin(fi)
        '            Del = Sqr((xMid - xi) ^ 2 + (yMid - yi) ^ 2)
        '            If Del > Delta Then
        '                iMax = i
        '                Delta = Del
        '            End If
        '        End If
        '    Next i
        '-------------------------------------------------


        'Rotate Ellipse
        For i = 0 To Cont.NumSegm - 1
            With Cont.Segm(i)
                SegType = .SegType
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                R = System.Math.Sqrt(xB ^ 2 + yB ^ 2)
                If System.Math.Abs(R) > Eps Then
                    sina = yB / R
                    cosa = xB / R
                    Alp = Angle(sina, cosa)
                    Alp = Alp + Beta
                    xB = R * System.Math.Cos(Alp)
                    yB = R * System.Math.Sin(Alp)
                    .xyBeg.v0 = xB
                    .xyBeg.v1 = yB
                End If

                xE = .xyEnd.v0
                yE = .xyEnd.v1
                R = System.Math.Sqrt(xE ^ 2 + yE ^ 2)
                If System.Math.Abs(R) > Eps Then
                    sina = yE / R
                    cosa = xE / R
                    Alp = Angle(sina, cosa)
                    Alp = Alp + Beta
                    xE = R * System.Math.Cos(Alp)
                    yE = R * System.Math.Sin(Alp)
                    .xyEnd.v0 = xE
                    .xyEnd.v1 = yE
                End If

                xc = .xyCent.v0
                yc = .xyCent.v1
                R = System.Math.Sqrt(xc ^ 2 + yc ^ 2)
                If System.Math.Abs(R) > Eps Then
                    sina = yc / R
                    cosa = xc / R
                    Alp = Angle(sina, cosa)
                    Alp = Alp + Beta
                    xc = R * System.Math.Cos(Alp)
                    yc = R * System.Math.Sin(Alp)
                    .xyCent.v0 = xc
                    .xyCent.v1 = yc
                End If

                xMid = .xyMid.v0
                yMid = .xyMid.v1
                R = System.Math.Sqrt(xMid ^ 2 + yMid ^ 2)
                If System.Math.Abs(R) > Eps Then
                    sina = yMid / R
                    cosa = xMid / R
                    Alp = Angle(sina, cosa)
                    Alp = Alp + Beta
                    xMid = R * System.Math.Cos(Alp)
                    yMid = R * System.Math.Sin(Alp)
                    .xyMid.v0 = xMid
                    .xyMid.v1 = yMid
                End If

                Alp1 = .Alp.v0 + Beta
                If Alp1 > 2 * Pi Then Alp1 = Alp1 - 2 * Pi
                .Alp.v0 = Alp1

                Alp2 = .Alp.v1 + Beta
                If Alp2 > 2 * Pi Then Alp2 = Alp2 - 2 * Pi
                .Alp.v1 = Alp2

            End With
        Next i

        'Move Ellipse
        For i = 0 To Cont.NumSegm - 1
            With Cont.Segm(i)
                SegType = .SegType
                .xyBeg.v0 = .xyBeg.v0 + xc0
                .xyBeg.v1 = .xyBeg.v1 + yc0

                .xyEnd.v0 = .xyEnd.v0 + xc0
                .xyEnd.v1 = .xyEnd.v1 + yc0

                .xyCent.v0 = .xyCent.v0 + xc0
                .xyCent.v1 = .xyCent.v1 + yc0

                .xyMid.v0 = .xyMid.v0 + xc0
                .xyMid.v1 = .xyMid.v1 + yc0

            End With
        Next i

        '++++++++++++++++++++++++++++++++++++++++++++++++++++
        '++++++++++++++++++++++++++++++++++++++++++++++++++++
        ' Trim contour by First Last points
        Dim IntDef As New DefineIntersections
        Dim iBeg, IEND As Integer
        Dim xLocE, xLocB, yLocB, yLocE As Double
        Dim rB, rE As Double
        Dim BetLocB, BetLocE As Double
        'UPGRADE_WARNING: Arrays in structure ContTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContTem As New ChainTool.Contour
        'UPGRADE_WARNING: Arrays in structure ContLine may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLine As New ChainTool.Contour
        'UPGRADE_WARNING: Arrays in structure Inter may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter As New ChainTool.IntersTwoCont

        If ContClosed = False Then


            ReDim ContLine.Segm(0)
            ContLine.NumSegm = 1


            'Begin Point
            With ContLine.Segm(0)
                .SegType = "Line" '# NLS#'
                xB = xc0
                yB = yc0
                .xyBeg.v0 = xB
                .xyBeg.v1 = yB
                xE = xc0 + (xB0 - xc0) * 1.5
                yE = yc0 + (yB0 - yc0) * 1.5

                .xyEnd.v0 = xE
                .xyEnd.v1 = yE
                Call LineSegmPar(xB, yB, xE, yE, .l, .Alp.v0)
            End With

            IntDef.RunIntersect(Cont, ContLine, Inter)

            iBeg = Inter.IndInt1(0)

            'End Point
            With ContLine.Segm(0)
                .SegType = "Line" '# NLS#'
                xB = xc0
                yB = yc0
                .xyBeg.v0 = xB
                .xyBeg.v1 = yB
                xE = xc0 + (xE0 - xc0) * 1.5
                yE = yc0 + (yE0 - yc0) * 1.5

                .xyEnd.v0 = xE
                .xyEnd.v1 = yE
                Call LineSegmPar(xB, yB, xE, yE, .l, .Alp.v0)
            End With

            IntDef.RunIntersect(Cont, ContLine, Inter)

            IEND = Inter.IndInt1(0)

            'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ContTem.CopyFrom(Cont)
            ReDim Cont.Segm(0)
            Cont.NumSegm = 0

            If IEND > iBeg Then
                For i = iBeg To IEND
                    ReDim Preserve Cont.Segm(Cont.NumSegm)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(Cont.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Cont.Segm(Cont.NumSegm) = ContTem.Segm(i)
                    Cont.NumSegm = Cont.NumSegm + 1
                Next i
            End If

            If IEND < iBeg Then
                For i = iBeg To ContTem.NumSegm - 1
                    ReDim Preserve Cont.Segm(Cont.NumSegm)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(Cont.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Cont.Segm(Cont.NumSegm) = ContTem.Segm(i)
                    Cont.NumSegm = Cont.NumSegm + 1
                Next i

                For i = 0 To IEND
                    ReDim Preserve Cont.Segm(Cont.NumSegm)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(Cont.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Cont.Segm(Cont.NumSegm) = ContTem.Segm(i)
                    Cont.NumSegm = Cont.NumSegm + 1
                Next i
            End If

            If IEND = iBeg Then
                With ContTem.Segm(iBeg)
                    SegType = .SegType
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1
                    xc = .xyCent.v0
                    yc = .xyCent.v1
                    R = .R
                    Rot = .Rot
                    Alp1 = .Alp.v0
                    Alp2 = .Alp.v1

                    If SegType = "Line" Then '# NLS#'
                        sina = System.Math.Sin(Alp1)
                        cosa = System.Math.Cos(Alp1)
                        xLocB = (yB0 - yB) * sina + (xB0 - xB) * cosa
                        xLocE = (yE0 - yB) * sina + (xE0 - xB) * cosa
                        If xLocE > xLocB Then
                            Cont.NumSegm = 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Cont.Segm(0) = ContTem.Segm(iBeg)
                        Else
                            Call ReEnumContour(ContTem, xB, yB)
                            ReDim Preserve ContTem.Segm(ContTem.NumSegm)
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContTem.Segm(ContTem.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContTem.Segm(ContTem.NumSegm) = ContTem.Segm(0)
                            ContTem.NumSegm = ContTem.NumSegm + 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Cont = ContTem
                        End If
                    End If ' If SegType = "Line" Then

                    If SegType = "Arc" Then '# NLS#'
                        sina = (yB - yc) / R
                        cosa = (xB - xc) / R
                        rB = System.Math.Sqrt((xB0 - xc) ^ 2 + (yB0 - yc) ^ 2)
                        yLocB = (yB0 - yc) * cosa - (xB0 - xc) * sina
                        xLocB = (yB0 - yc) * sina + (xB0 - xc) * cosa
                        sinB = yLocB / rB
                        cosB = xLocB / rB
                        BetLocB = Angle(sinB, cosB)
                        If Rot = -1 Then BetLocB = 2 * Pi - BetLocB
                        'If Rot <> 0 Then BetLocB = 2 * Pi - BetLocB

                        rE = System.Math.Sqrt((xE0 - xc) ^ 2 + (yE0 - yc) ^ 2)
                        yLocE = (yE0 - yc) * cosa - (xE0 - xc) * sina
                        xLocE = (yE0 - yc) * sina + (xE0 - xc) * cosa
                        sinB = yLocE / rE
                        cosB = xLocE / rE
                        BetLocE = Angle(sinB, cosB)
                        If Rot = -1 Then BetLocE = 2 * Pi - BetLocE
                        'If Rot <> 0 Then BetLocE = 2 * Pi - BetLocE


                        If BetLocE > BetLocB Then
                            Cont.NumSegm = 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Cont.Segm(0) = ContTem.Segm(iBeg)
                        Else
                            Call ReEnumContour(ContTem, xB, yB)
                            ReDim Preserve ContTem.Segm(ContTem.NumSegm)
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContTem.Segm(ContTem.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContTem.Segm(ContTem.NumSegm) = ContTem.Segm(0)
                            ContTem.NumSegm = ContTem.NumSegm + 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Cont = ContTem
                        End If
                    End If ' If SegType = "Arc" Then

                End With
            End If


            With Cont.Segm(0)
                SegType = .SegType
                xB = xB0
                yB = yB0
                .xyBeg.v0 = xB
                .xyBeg.v1 = yB

                xE = .xyEnd.v0
                yE = .xyEnd.v1

                xc = .xyCent.v0
                yc = .xyCent.v1

                R = .R
                Rot = .Rot

                If SegType = "Line" Then '# NLS#'
                    Call LineSegmPar(xB, yB, xE, yE, .l, .Alp.v0)
                Else
                    Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, .xyMid.v0, .xyMid.v1)
                    Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, .Alp.v0, .Alp.v1)
                End If
            End With

            With Cont.Segm(Cont.NumSegm - 1)
                SegType = .SegType
                xB = .xyBeg.v0
                yB = .xyBeg.v1

                xE = xE0
                yE = yE0
                .xyEnd.v0 = xE
                .xyEnd.v1 = yE

                xc = .xyCent.v0
                yc = .xyCent.v1

                R = .R
                Rot = .Rot

                If SegType = "Line" Then '# NLS#'
                    Call LineSegmPar(xB, yB, xE, yE, .l, .Alp.v0)
                Else
                    Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, .xyMid.v0, .xyMid.v1)
                    Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, .Alp.v0, .Alp.v1)
                End If
            End With
        End If
        '++++++++++++++++++++++++++++++++++++++++++++++++++++
        '++++++++++++++++++++++++++++++++++++++++++++++++++++

        '    Call CreatePlaneGraphic(Cont, Gr)

    End Sub


    ' Smooth curve cosmetic graphic
    Private Sub SmoothCurve(ByRef GrCont As IMSIGX.Graphic, ByRef Cont As ChainTool.Contour)

        ReDim Cont.Segm(0)
        Cont.NumSegm = 0
        Dim i As Integer
        Dim GrChild As IMSIGX.Graphic

        'UPGRADE_NOTE: Object GrChild may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrChild)
        GXMD.Collect()
        For i = 0 To GrCont.Graphics.Count - 1
            '        If GrCont.Graphics.Item(i).Type = "GRAPHIC" Then
            If GrCont.Graphics.Item(i).Type = GRAPHICTYPE Then
                GrChild = GrCont.Graphics.Item(i)
                Exit For
            End If
        Next i

        If GrChild Is Nothing Then Exit Sub
        Dim Alp1, xE, xB, yB, yE, l As Double
        'UPGRADE_WARNING: Arrays in structure SegmNext may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New ChainTool.Segment
        Segm.Initialize()
        Dim nV As Integer
        nV = GrChild.Vertices.Count
        With GrChild.Vertices
            For i = 1 To nV - 1
                xE = .Item(i).X
                yE = .Item(i).Y
                xB = .Item(i - 1).X
                yB = .Item(i - 1).Y
                Call LineSegmPar(xE, yE, xB, yB, l, Alp1)
                If l > Eps Then
                    With Segm
                        .SegType = "Line" '# NLS#'
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .xyEnd.v0 = xE
                        .xyEnd.v1 = yE
                        .l = l
                        .Alp.v0 = Alp1
                    End With
                    ReDim Preserve Cont.Segm(Cont.NumSegm)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(Cont.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Cont.Segm(Cont.NumSegm) = Segm
                    Cont.NumSegm = Cont.NumSegm + 1
                End If
            Next i
        End With

        Dim dx, MaxExt, dy As Double
        MaxExt = 0
        With GrCont.CalcBoundingBox(oMissing)
            dx = .Max.X - .Min.X
            dy = .Max.Y - .Min.Y
            If dx > MaxExt Then MaxExt = dx
            If dy > MaxExt Then MaxExt = dy
        End With
        'UPGRADE_WARNING: Arrays in structure ContSmooth may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContSmooth As ChainTool.Contour
        'UPGRADE_WARNING: Couldn't resolve default property of object ContSmooth. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContSmooth = Cont
        Call SmoothContour(ContSmooth, MaxExt)
        'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Cont = ContSmooth

        'Dim Gr As Graphic
        '    Call CreatePlaneGraphic(ContSmooth, Gr)


    End Sub

    ' Calculate some Parameters for Resulting Contours

    Private Sub CalcSegmParam(ByRef Cont As ChainTool.Contour)
        Dim Type1 As String
        Dim fiEnd, l, xc, xMid, xE, xB, yB, yE, yMid, yc, fiBeg, R As Double
        Dim Rot, Dist As Short

        Dim i As Integer
        For i = 0 To Cont.NumSegm - 1
            With Cont.Segm(i)
                Type1 = .SegType
                If Type1 = "Circle" Then Exit Sub
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                l = .l
                fiBeg = .Alp.v0
                fiEnd = .Alp.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                R = .R
                Rot = .Rot
                Dist = .Distinct
                If Type1 = "Line" Then
                    Call LineSegmPar(xB, yB, xE, yE, l, fiBeg)
                    .l = l
                    .Alp.v0 = fiBeg
                    .Alp.v1 = fiBeg
                End If
                If Type1 = "Arc" Then
                    .Distinct = ArcDictinct(xB, yB, xE, yE, xMid, yMid, Rot)
                    Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, fiBeg, fiEnd)
                    .Alp.v0 = fiBeg
                    .Alp.v1 = fiEnd
                End If
            End With
        Next i

    End Sub
End Class