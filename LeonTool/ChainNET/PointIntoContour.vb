Option Strict Off
Option Explicit On
Friend Class PointIntoContour
	
	Const Eps As Double = 0.0000000001
	
	' Function return 0 - if point lies on Contour ,
	' or 1 - if point lies inside  bounds of the contour
	' or -1 - if point lies outside or on o bounds of the contour
	
	Public Function PointInsideContour(ByRef Cont As ChainTool.Contour, ByRef xP As Double, ByRef yP As Double) As Short
		Dim TypeSeg As String
		Dim fi, yc, yMid, yE, yB, xB, xE, xMid, xc, L, R As Double
		Dim Rot As Short
		Dim NumInt As Short
		NumInt = 0
		Dim yInt, xInt, xLoc As Double
		Dim xInt2, xInt1, yInt1, yInt2 As Double
		
        Dim i As Integer
		Dim sina, cosa As Double
		sina = System.Math.Sin(37 / 180 * Pi)
		cosa = System.Math.Cos(37 / 180 * Pi)
		Dim xP1, yP1 As Double
		xP1 = xP + 1000000 * cosa
		yP1 = yP + 1000000 * sina
		
		Dim RR As Double
        'Dim Res2 As Boolean
		For i = 0 To Cont.NumSegm - 1
			With Cont.Segm(i)
				TypeSeg = .SegType
				xB = .xyBeg.v0
				yB = .xyBeg.v1
				xE = .xyEnd.v0
				yE = .xyEnd.v1
				L = .L
				fi = .Alp.v0
				xc = .xyCent.v0
				yc = .xyCent.v1
				xMid = .xyMid.v0
				yMid = .xyMid.v1
				R = System.Math.Abs(.R)
				Rot = .Rot
			End With
			
			If TypeSeg = "Circle" Then '# NLS#'
				RR = System.Math.Sqrt((xP - xc) ^ 2 + (yP - yc) ^ 2)
				If System.Math.Abs(RR - R) < Eps Then
					PointInsideContour = 0
				End If
				If RR > R + Eps Then
					PointInsideContour = -1
				End If
				If RR < R - Eps Then
					PointInsideContour = 1
				End If
				Exit Function
			End If
			
			If TypeSeg = "Line" Then '# NLS#'
				If PointIntoSegmenti(xB, yB, xE, yE, xP, yP) = True Then
					PointInsideContour = 0
					Exit Function
				End If
				If System.Math.Abs(xB - xP) < Eps And System.Math.Abs(yB - yP) < Eps Then
					PointInsideContour = 0
					Exit Function
				End If
				
				If SegSegIntersection(xB, yB, xE, yE, xP, yP, xP1, yP1, xInt, yInt) = True Then
					If PointIntoSegmenti(xB, yB, xE, yE, xInt, yInt) = True Then
						xLoc = (yInt - yP) * sina + (xInt - xP) * cosa
						If xLoc > 0 Then
							NumInt = NumInt + 1
						End If
					End If
				End If
			End If
			
			If TypeSeg = "Arc" Then '# NLS#'
				If PointOnArc(Cont.Segm(i), xP, yP) = True Then
					PointInsideContour = 0
					Exit Function
				End If
				If LineCircIntersection(xP, yP, 37 / 180 * Pi, xc, yc, R, xInt1, yInt1, xInt2, yInt2) = True Then
					xLoc = (yInt1 - yP) * sina + (xInt1 - xP) * cosa
					If PointOnArc(Cont.Segm(i), xInt1, yInt1) = True And xLoc > 0 Then
						NumInt = NumInt + 1
					End If
					
					xLoc = (yInt2 - yP) * sina + (xInt2 - xP) * cosa
					If PointOnArc(Cont.Segm(i), xInt2, yInt2) = True And xLoc > 0 Then
						NumInt = NumInt + 1
					End If
				End If
			End If
		Next i
		
		If CDbl(CDbl(NumInt) / 2) <> CDbl(Int(CDbl(NumInt) / 2)) Then
			PointInsideContour = 1
		Else
			PointInsideContour = -1
		End If
		
	End Function
	
	
	
	Private Function Angle(ByRef sinB As Double, ByRef cosB As Double) As Double
		Dim Eps1 As Double
		Eps1 = 0.0000000000001
		If System.Math.Abs(cosB) < Eps1 Then
			If sinB > Eps1 Then
				Angle = Pi / 2
			Else
				Angle = 3 * Pi / 2
			End If
		Else
			If System.Math.Abs(sinB) < Eps1 Then
				If cosB > Eps1 Then
					Angle = 0
				Else
					Angle = Pi
				End If
			Else
				If sinB >= 0 And cosB > 0 Then
					Angle = System.Math.Atan(sinB / cosB)
				Else
					If sinB >= 0 And cosB < 0 Then
						Angle = Pi + System.Math.Atan(sinB / cosB)
					Else
						If sinB < 0 And cosB < 0 Then
							Angle = Pi + System.Math.Atan(sinB / cosB)
						Else
							If sinB < 0 And cosB > 0 Then
								Angle = 2 * Pi + System.Math.Atan(sinB / cosB)
							End If
						End If
					End If
				End If
			End If
		End If
	End Function
	
	
	Private Function Sign(ByRef y As Double) As Double
		If System.Math.Abs(y) < 0.000001 Then
			Sign = 0
			Exit Function
		End If
		If y < 0 Then
			Sign = -1#
			Exit Function
		End If
		If y > 0 Then
			Sign = 1#
			Exit Function
		End If
	End Function
	
	Private Function SysLine(ByRef a1 As Double, ByRef b1 As Double, ByRef d1 As Double, ByRef a2 As Double, ByRef b2 As Double, ByRef d2 As Double, ByRef xRes As Double, ByRef yRes As Double) As Boolean
		
		Dim delx, del, dely As Double
		On Error GoTo ErrorHandler
		
		del = a1 * b2 - a2 * b1
		delx = d1 * b2 - b1 * d2
		dely = a1 * d2 - a2 * d1
		If System.Math.Abs(del) < Eps Then
			SysLine = False
			Exit Function
		End If
		xRes = delx / del
		yRes = dely / del
		SysLine = True
		Exit Function
ErrorHandler: 
		'    MsgBox Err.Description
		SysLine = False
	End Function
	
	' Function return 1 - if point lies inside line segment, or 0 - if point lies outside or on
	' on bounds of line segment
	
	Private Function PointIntoSegmenti(ByRef x1 As Double, ByRef y1 As Double, ByRef x2 As Double, ByRef y2 As Double, ByRef x As Double, ByRef y As Double) As Boolean
		Dim sina, dy, dx, L, cosa As Double
		PointIntoSegmenti = False
		If System.Math.Abs(x - x1) < Eps And System.Math.Abs(y - y1) < Eps Then
			PointIntoSegmenti = False
			Exit Function
		End If
		If System.Math.Abs(x - x2) < Eps And System.Math.Abs(y - y2) < Eps Then
			PointIntoSegmenti = True
			Exit Function
		End If
		Dim xLoc, yLoc As Double
		dx = x2 - x1
		dy = y2 - y1
		L = System.Math.Sqrt(dx * dx + dy * dy)
		If L < Eps Then Exit Function
		sina = dy / L
		cosa = dx / L
		
		yLoc = (y - y1) * cosa - (x - x1) * sina
		xLoc = (y - y1) * sina + (x - x1) * cosa
		If System.Math.Abs(yLoc) > 1 * Eps Then
			PointIntoSegmenti = False
			Exit Function
		End If
		If xLoc > 0 And xLoc < L Then
			PointIntoSegmenti = True
			Exit Function
		End If
		
	End Function
	
	
	Private Function SegSegIntersection(ByRef xB1 As Double, ByRef yB1 As Double, ByRef xE1 As Double, ByRef yE1 As Double, ByRef xB2 As Double, ByRef yB2 As Double, ByRef xE2 As Double, ByRef yE2 As Double, ByRef xInt As Double, ByRef yInt As Double) As Boolean
		Dim b1, a1, d1 As Double
		Dim b2, a2, d2 As Double
		
		a1 = -(yE1 - yB1)
		b1 = xE1 - xB1
		d1 = a1 * xB1 + b1 * yB1
		
		a2 = -(yE2 - yB2)
		b2 = xE2 - xB2
		d2 = a2 * xB2 + b2 * yB2
		
		If SysLine(a1, b1, d1, a2, b2, d2, xInt, yInt) = True Then
			If PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt, yInt) = True And PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt, yInt) = True Then
				SegSegIntersection = True
			Else
				SegSegIntersection = False
			End If
		Else
			SegSegIntersection = False
		End If
	End Function
	
	
	' Define the intersection points for Line and Circle
	Private Function LineCircIntersection(ByRef x1 As Double, ByRef y1 As Double, ByRef fi As Double, ByRef xc As Double, ByRef yc As Double, ByRef R As Double, ByRef xInt1 As Double, ByRef yInt1 As Double, ByRef xInt2 As Double, ByRef yInt2 As Double) As Boolean
		Dim sina, cosa As Double
		sina = System.Math.Sin(fi)
		cosa = System.Math.Cos(fi)
		Dim xcLoc, ycLoc As Double
		xcLoc = (yc - y1) * sina + (xc - x1) * cosa
		ycLoc = (yc - y1) * cosa - (xc - x1) * sina
		If System.Math.Abs(ycLoc) > System.Math.Abs(R) + Eps Then
			LineCircIntersection = False
			Exit Function
		End If
		Dim t As Double
		t = System.Math.Sqrt(System.Math.Abs(R * R - ycLoc * ycLoc))
		Dim xLoc, yLoc As Double
		yLoc = 0
		xLoc = xcLoc + t
		xInt1 = x1 + xLoc * cosa - yLoc * sina
		yInt1 = y1 + xLoc * sina + yLoc * cosa
		
		xLoc = xcLoc - t
		xInt2 = x1 + xLoc * cosa - yLoc * sina
		yInt2 = y1 + xLoc * sina + yLoc * cosa
		LineCircIntersection = True
	End Function
	
	
	' Define if point lies on Arc
	Private Function PointOnArc(ByRef Segm As ChainTool.Segment, ByRef xP As Double, ByRef yP As Double) As Boolean
		
		Dim xMid, yc, yE, xB, xE, xc, R, yMid As Double
		Dim yB As Object
		Dim Rot As Short
		xB = Segm.xyBeg.v0
		'UPGRADE_WARNING: Couldn't resolve default property of object yB. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		yB = Segm.xyBeg.v1
		xE = Segm.xyEnd.v0
		yE = Segm.xyEnd.v1
		xc = Segm.xyCent.v0
		yc = Segm.xyCent.v1
		xMid = Segm.xyMid.v0
		yMid = Segm.xyMid.v1
		R = System.Math.Abs(Segm.R)
		Rot = Segm.Rot
		
		Dim Rp As Double
		Rp = System.Math.Sqrt((xP - xc) ^ 2 + (yP - yc) ^ 2)
		If System.Math.Abs(Rp - R) > Eps Then
			PointOnArc = False
			Exit Function
		End If
		
		Dim xMidLoc, xPLoc, xELoc, sina, cosa, yELoc, yPLoc, yMidLoc As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object yB. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		sina = (yB - yc) / R
		cosa = (xB - xc) / R
		xELoc = (yE - yc) * sina + (xE - xc) * cosa
		yELoc = (yE - yc) * cosa - (xE - xc) * sina
		xPLoc = (yP - yc) * sina + (xP - xc) * cosa
		yPLoc = (yP - yc) * cosa - (xP - xc) * sina
		xMidLoc = (yMid - yc) * sina + (xMid - xc) * cosa
		yMidLoc = (yMid - yc) * cosa - (xMid - xc) * sina
		
		Dim AlpELoc, AlpPLoc As Double
		sina = yELoc / R
		cosa = xELoc / R
		AlpELoc = Angle(sina, cosa)
		
		sina = yPLoc / R
		cosa = xPLoc / R
		AlpPLoc = Angle(sina, cosa)
		
        If Rot = -1 Then
            'If Rot <> 0 Then
            AlpELoc = 2 * Pi - AlpELoc
            AlpPLoc = 2 * Pi - AlpPLoc
        End If

        If AlpPLoc > Eps And AlpPLoc < AlpELoc + Eps Then
            PointOnArc = True
        Else
            PointOnArc = False
        End If
    End Function
End Class