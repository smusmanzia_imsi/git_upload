Imports System.Resources

Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.


' TODO: Review the values of the assembly attributes


<Assembly: AssemblyTitle("Contour by chain Library")> 
<Assembly: AssemblyDescription("<vb> Version Info")> 
<Assembly: AssemblyCompany("$COMPANY, LLC")> 
<Assembly: AssemblyProduct("$TC_NAME (tm) for Windows")> 
<Assembly: AssemblyCopyright("Copyright (c) $COMPANY 2010")> 
<Assembly: AssemblyTrademark("$TC_NAME (tm) is a registered trademark of $COMPANY")> 
<Assembly: AssemblyCulture("")> 

' Version information for an assembly consists of the following four values:

'	Major version
'	Minor Version
'	Build Number
'	Revision

' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:

<Assembly: AssemblyVersion("17.0.*")> 



<Assembly: ComVisibleAttribute(True)> 
<Assembly: NeutralResourcesLanguageAttribute("en-US")> 