Option Strict Off
Option Explicit On

Imports System.Runtime.InteropServices
Imports System.Windows.Forms


#If _USE_NATIVEIMSIGX = False Then
Imports TCDotNetInterfaces
#End If

<System.Runtime.InteropServices.ProgId("ChainTool_NET.ChainTool")>
Public Class ChainTool

#If _USE_NATIVEIMSIGX = False Then
    Implements ITurboCADTool
    Implements IMSIGX.IAppEvents
#End If


    Private Class MyAxHostConverter

        Inherits AxHost

        Public Sub New()

            MyBase.New("00000000-0000-0000-0000-000000000000")

        End Sub

        Public Function ImageToPictureDisp(ByRef image As Image) As stdole.IPictureDisp

            ImageToPictureDisp = GetIPictureDispFromPicture(image)

        End Function

    End Class

    Public Structure Segment
        Public Structure dPair
            Dim v0 As Double
            Dim v1 As Double
        End Structure

        Public Structure nPair
            Dim v0 As Integer
            Dim v1 As Integer
        End Structure

        Dim SegType As String
        '        <VBFixedArray(2)> Dim xyBeg() As Double
        '        <VBFixedArray(2)> Dim xyEnd() As Double
        '        <VBFixedArray(2)> Dim xyCent() As Double
        '        <VBFixedArray(2)> Dim xyMid() As Double
        '        <VBFixedArray(2)> Dim Alp() As Double

        Dim xyBeg As dPair
        Dim xyEnd As dPair
        Dim xyCent As dPair
        Dim xyMid As dPair
        Dim Alp As dPair

        Dim l As Double
        Dim R As Double
        Dim Rot As Short
        Dim Distinct As Short
        Dim iSegmInit As Integer ' number of cooresponding segment on initial contour
        Dim Deleted As Boolean
        Dim Type As String
        Dim Parent As Integer
        Dim TakePart As Boolean
        Dim UpDown As Short ' 0 - Up, 1 - Down, 2 - Entry
        Dim Divided As Boolean
        '        <VBFixedArray(2)> Dim IndexInt() As Integer
        'Dim IndexInt As nPair
        Public Sub New(p As Integer)
            Initialize()
        End Sub

        'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
        Public Sub Initialize()
            '            ReDim xyBeg(2)
            '            ReDim xyEnd(2)
            '            ReDim xyCent(2)
            '            ReDim xyMid(2)
            '            ReDim Alp(2)
            '            ReDim IndexInt(2)
        End Sub
    End Structure

    Public Structure Contour
        Dim NumSegm As Integer
        Dim Segm() As Segment
        Dim Clockwise As Short ' = 1 - if counter Clockwise  , = -1 if Clockwise
        Dim TakePart As Boolean
        Dim ParentIndex As Integer
        Dim Deleted As Boolean
        Dim ConType As String
        Dim OffsetID As Integer
        Dim ClickDirection As Short
        Dim Closed As Boolean
        Dim GrID As Integer
        Dim xMax As Double
        Dim yMax As Double
        Dim xMin As Double
        Dim yMin As Double

        Public Sub CopyFrom(cont As Contour)

            Clockwise = cont.Clockwise
            TakePart = cont.TakePart
            ParentIndex = cont.ParentIndex
            Deleted = cont.Deleted
            ConType = cont.ConType
            OffsetID = cont.OffsetID
            ClickDirection = cont.ClickDirection
            Closed = cont.Closed
            GrID = cont.GrID
            xMax = cont.xMax
            yMax = cont.yMax
            xMin = cont.xMin
            yMin = cont.yMin

            NumSegm = cont.NumSegm
            ReDim Segm(NumSegm)
            Dim i As Integer
            For i = 0 To NumSegm - 1
                Segm(i) = cont.Segm(i)
            Next
        End Sub


    End Structure

    Public Structure Contours
        Dim NumCont As Integer
        Dim Cont() As Contour

        Public Sub CopyFrom(conts As Contours)

            NumCont = conts.NumCont
            ReDim Cont(NumCont)
            Dim i As Integer
            For i = 0 To NumCont - 1
                Cont(i).CopyFrom(conts.Cont(i))
            Next
        End Sub

    End Structure


    Public Structure IntersTwoCont
        Dim NumInt As Short
        Dim xInt() As Double
        Dim yInt() As Double
        Dim IndInt1() As Integer
        Dim IndInt2() As Integer
        '    TakePart() As Boolean
        '    Deleted() As Boolean
    End Structure



    'Number of tools in this server
    Const NUM_TOOLS As Short = 1
    Public xcWorld As Double
    Public ycWorld As Double


    ' *******************************************************************
    '    Const EventMask = 268435456 + 536870912
    '    Const EventMask = 1024 + 268435456 + 536870912
    '    Const EventMask As Double = 1 + 2 + 4 + 8 + 16 + 32 + 64 + 1024 + 4096 + 262144 + 268435456 + 536870912
    '    Const EventMask As IMSIGX.ImsiEventMask = 1 + 2 + 4 + 8 + 16 + 32 + 64 + 1024 + 4096 + 262144 + 268435456 + 536870912
    Const EventMask1 As Object = 1 + 2 + 4 + 8 + 16 + 32 + 64 + 262144 + 268435456 + 536870912

    Const EventMask2 As IMSIGX.ImsiEventMask = 1024 + 4096 + 8388608

    Private iConnectId1 As Integer
    Private iConnectId2 As Integer
    Private iActTool As Integer
#If _USE_NATIVEIMSIGX = False Then
    Private theToolEvents As IMSIGX.ToolEvents
#Else
    Private theToolEvents As XDbTool.IToolEvents
#End If


    ' *******************************************************************
    'Toggle this to test loading buttons from .Bmp/.Res
    Const boolLoadFromBmp As Boolean = False
    Const boolDebug As Boolean = False
    '##########################################################################
    '##########################################################################

    '    Private Declare Function SetClassLongPtr Lib "User32.dll" Alias "SetClassLongA" (ByVal hwnd As Int64, ByVal nIndex As Integer, ByVal dwNewLong As UIntPtr) As Int64
    '    Private Declare Function GetClassLongPtr Lib "User32.dll" Alias "GetClassLongA" (ByVal hwnd As Int64, ByVal nIndex As Integer) As Int64
    '    Const GCLP_HCURSOR As Short = -12

    'Private Declare Function TCWUndoRecordEnd Lib "TCAPI90.dll" (ByVal d As Long) As Long
    'Private Declare Function TCWUndoRecordStart Lib "TCAPI90.dll" (ByVal d As Long, ByRef Title As String) As Long
    'Private Declare Function TCWUndoRecordAddGraphic Lib "TCAPI90.dll" (ByVal d As Long, ByVal g As Long) As Long

    'Private Declare Function TCWDrawingActive Lib "TCAPI90.dll" () As Long
    'Private Declare Function TCWGraphicAt Lib "TCAPI90.dll" (ByVal d As Long, ByVal Index As Long) As Long

    'Private Declare Function GraphicGetMatrix Lib "DBAPI90.dll" (ByVal hGr As Long) As Long
    'Private Declare Function MatrixGetPlane Lib "DBAPI90.dll" (ByVal hMat As Long, a As Double, b As Double, c As Double, d As Double) As Boolean

    'Private Declare Function SendMessage Lib "User32.dll" Alias "SendMessageA" (ByVal hwnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lparam As Long) As Long
    'Private Declare Function SetFocus Lib "User32.dll" (ByVal hwnd As Long) As Long
    'Private Declare Function GetParent Lib "User32.dll" (ByVal hwnd As Long) As Long
    Private Captions(2) As String
    Private Prompts(2, 1) As String
    Private Enabled(2) As Boolean
    Private Checked(2) As Boolean

    '    Private hToolCursor As Integer
    '    Private hTCCursor As Integer

    'Private hToolCursor As Int64
    'Private hTCCursor As Int64

    Private oCurCursor As Cursor
    Private oToolCursor As Cursor


    Dim DeleteOriginal As Boolean
    Dim WidthUnit As String


    'UPGRADE_WARNING: Arrays in structure ContsRes may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
    Dim ContsRes As Contours
    'UPGRADE_WARNING: Arrays in structure ContRes may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
    Dim ContRes As Contour
    'UPGRADE_WARNING: Arrays in structure ContCur may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
    Dim ContCur As Contour
    'UPGRADE_WARNING: Arrays in structure ContsInt may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
    Dim ContsInt As Contours
    'UPGRADE_WARNING: Arrays in structure ContsCur may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
    Dim ContsCur As Contours

    Dim GrFirstID As Integer
    Dim GrFirstDir As IMSIGX.Graphic
    Dim GrCurID As Integer

    Dim y00, x00, z00 As Double
    Dim y00W, x00W, z00W As Double

    Dim y0Dir, x0Dir, z0Dir As Double
    Dim y0DirW, x0DirW, z0DirW As Object

    Dim L0Dir As Double

    Dim iCurSeg As Integer
    Dim ILAST As Integer
    Dim InterExist As Boolean
    Dim xIntCur, yIntCur As Double
    Dim GrContCur As IMSIGX.Graphic

    Dim GrGroup As IMSIGX.Graphic
    Dim xBeg0, yBeg0 As Double
    Dim GrSetDir As IMSIGX.GraphicSet
    'UPGRADE_WARNING: Arrays in structure ContsDir may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
    Dim ContsDir As Contours
    Dim ViH0 As Double

    Dim LastPointEnable As Boolean
    Dim LastPointClicked As Boolean
    Dim LastPointChecked As Boolean
    Dim GrLastPoint As IMSIGX.Graphic
    Dim xLast, yLast As Double
    Dim yLastW, xLastW, zLastW As Double

    Dim hDr As Integer
    Dim c0, a0, b0, d0 As Double
    Dim hMat0 As Integer


    Dim NumClick As Short

    Dim gxMe As IMSIGX.Tool

    Dim ActDrUcs As IMSIGX.Matrix

    Dim GridExist As Boolean

    Dim GxSet As IMSIGX.GraphicSet

    Dim SelWinOpen As Short

    'UPGRADE_NOTE: Space was upgraded to Space_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Dim Space_Renamed As IMSIGX.ImsiSpaceModeType


    'Private Function GetLMPicture() As Object
    Public Function GetLMPicture() As Object
        Dim TheImage As System.Drawing.Image
        'MsgBox ("1111")
        TheImage = My.Resources.bmp1003

        Dim axHostConverter As New MyAxHostConverter


        GetLMPicture = axHostConverter.ImageToPictureDisp(TheImage)

    End Function

    Public Function GetToolID() As Integer

        GetToolID = gxMe.Id

    End Function

#If _USE_NATIVEIMSIGX Then
    'Copy a windows bitmap of the requested size to the clipboard
    'Bitmaps returned should contain NUM_TOOLS images
    'Size of entire bitmap:
    'Normal:  (NUM_TOOLS*16) wide x 15 high
    'Large:   (NUM_TOOLS*24) wide x 23 high
    'Mono bitmap should be 1-bit (black or white)
    Public Function CopyBitmap(ByVal LargeImage As Boolean, ByVal MonoImage As Boolean) As Boolean
        On Error GoTo BitmapError

        Dim TheImage As System.Drawing.Image = New System.Drawing.Bitmap(1, 1)
        If GetButtonPicture(LargeImage, MonoImage, TheImage) Then
            'Put the image on the Windows clipboard
            My.Computer.Clipboard.SetImage(TheImage)
            CopyBitmap = True
            Exit Function
        End If

BitmapError:
        CopyBitmap = False
    End Function

    'Return a Picture object for the requested size
    'Apparently, returning references to StdPicture objects doesn't work for .EXE servers
    'Bitmaps returned should contain NUM_TOOLS images
    'Size of entire image:
    'Normal:  (NUM_TOOLS*16) wide x 15 high
    'Large:   (NUM_TOOLS*24) wide x 23 high
    'Mono image should be 1-bit (black or white)

    Public Function GetPicture(ByVal LargeImage As Boolean, ByVal MonoImage As Boolean) As Object

        On Error GoTo PictureError
        Dim TheImage As System.Drawing.Image = New System.Drawing.Bitmap(1, 1)
        If GetButtonPicture(LargeImage, MonoImage, TheImage) Then
            GetPicture = TheImage
            Exit Function
        End If
PictureError:
        'UPGRADE_NOTE: Object GetPicture may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GetPicture = Nothing
    End Function

    'Implementation specific stuff
    'Private function to return the bitmap from .Res file or .Bmp file
    Private Function GetButtonPicture(ByVal LargeImage As Boolean, ByVal MonoImage As Boolean, ByRef TheImage As System.Drawing.Image) As Boolean
        On Error GoTo LoadError

        'There are two ways to load images:  from .Bmp file(s) or from .RES resource.
        'In this demo, we control the loading by a private variable.

        'Note that if you are loading from .Bmp, or if you are running this tool as a
        '.VBP for debugging, you must place the .Res or .Bmp files in the Draggers subdirectory
        'of the directory in which TCW40.EXE (or IMSIGX40.DLL) is located.

        Dim strFileName As String
        Dim idBitmap As Short 'BITMAP resource id in .Res file 'File name of .Bmp file to load
        If boolLoadFromBmp Then
            'Load from .Bmp file

            If LargeImage Then
                strFileName = My.Application.Info.DirectoryPath & "\button24.bmp" '# NLS#'
            Else
                strFileName = My.Application.Info.DirectoryPath & "\button16.bmp" '# NLS#'
            End If
            TheImage = System.Drawing.Image.FromFile(strFileName)
        Else
            'Load from .Res file

            If LargeImage Then
                idBitmap = 1002
            Else
                idBitmap = 1001
            End If
            'UPGRADE_ISSUE: Global method LoadResPicture was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
            TheImage = VB6.LoadResPicture(idBitmap, VB6.LoadResConstants.ResBitmap)
        End If

        'Return the image
        GetButtonPicture = True
        Exit Function

LoadError:
        If boolDebug Then
            '        MsgBox "Error loading bitmap: " & Err.Description
        End If
        GetButtonPicture = False
    End Function

#End If

    '##########################################################################
    '##########################################################################
    '##########################################################################


    'Return a description string for this package of tools
#If _USE_NATIVEIMSIGX = False Then
    Public ReadOnly Property Description() As String _
        Implements ITurboCADTool.Description
        Get
            Description = "ContourByChain" '# NLS#'
        End Get
    End Property
#Else
    Public Function Description() As String
        Description = "ContourByChain" '# NLS#'
    End Function
#End If

    'Called to perform tool function
#If _USE_NATIVEIMSIGX = False Then
    Public Function Run(ByVal Tool As IMSIGX.Tool) As Boolean Implements ITurboCADTool.Run
#Else
    Public Function Run(ByVal Tool As IMSIGX.Tool) As Boolean
#End If
        On Error GoTo ErrorHandler
        Dim theXTool As IMSIGX.Tool
        theXTool = Tool

        Space_Renamed = theXTool.Application.ActiveDrawing.ActiveView.SpaceMode
        If Space_Renamed = IMSIGX.ImsiSpaceModeType.imsiModelSpace Then
            GridExist = False
            If theXTool.Application.ActiveDrawing.Properties.Item("$GRIDSMS").Value = 1 Then
                GridExist = True
            End If
        End If

        L0Dir = -100
        LastPointEnable = False
        LastPointClicked = False
        LastPointChecked = False
        xLast = -10000000
        yLast = -10000000

        DeleteOriginal = False
        WidthUnit = theXTool.Application.ActiveDrawing.Properties.Item("PaperLinearUnitName3").Value
        NumClick = 0
        ReDim ContsRes.Cont(0)
        ContsRes.NumCont = 0
        ReDim ContRes.Segm(0)
        ContRes.Segm(0).Initialize()
        ContRes.NumSegm = 0
        ReDim ContsCur.Cont(0)
        ContsCur.NumCont = 0

        '    theXTool.Application.ActiveDrawing.Graphics.Unselect
        Dim Sel As IMSIGX.Selection
        Sel = theXTool.Application.ActiveDrawing.Selection

        Dim SelCount As Integer
        SelCount = Sel.Count
'        Dim GrSel As IMSIGX.Graphic
'        Dim i As Integer
'        For i = SelCount - 1 To 0 Step -1
'            GrSel = Sel.Item(i)
'            GrSel.Properties.Item("Selected").Value = 0
'            '        GrSel.Unselect
'            GrSel.Draw()
'        Next i
        If SelCount > 0 Then
            Sel.Unselect()
        End If
        On Error Resume Next
        ActDrUcs = theXTool.Application.ActiveDrawing.UCS

        SelWinOpen = theXTool.Application.ActiveDrawing.Properties.Item("SelectWindowOpen").Value
        If SelWinOpen <> 1 Then
            theXTool.Application.ActiveDrawing.Properties.Item("SelectWindowOpen").Value = 1
        End If

        If iConnectId1 <> -1 Or iConnectId2 <> -1 Then

            If (iConnectId1 <> -1) Then
                objApp.DisconnectEvents(iConnectId1)
                iConnectId1 = -1
            End If

            If (iConnectId2 <> -1) Then
                objApp.DisconnectEvents(iConnectId2)
                iConnectId1 = -2
            End If

            AddLM(False)
            iActTool = -1
            theToolEvents.ToolChangePrompt(Me, "", False)
        Else

            'Dim hwnd As Long
            '        hwnd = theXTool.Application.ActiveDrawing.ActiveView.hwnd
            '        hwnd = GetParent(hwnd)
            '        hwnd = GetParent(hwnd)
            '        hwnd = GetParent(hwnd)
            '        SetFocus hwnd
            '        SendMessage hwnd, 273, 38075, 0

            '        Call CurrentWPToWorld(theXTool.Application.ActiveDrawing)
            GrGroup = theXTool.Application.ActiveDrawing.Graphics.Add(IMSIGX.ImsiGraphicType.imsiGroup)
            GrGroup.Name = "Chain" '# NLS#'
            GrSets = theXTool.Application.ActiveDrawing.GraphicSets
            GrSetDir = GrSets.Add("", True)
            theXTool.Application.ActiveDrawing.Properties.Item("PaperLinearUnitName3").Value = "in"
            '            iConnectId = objApp.ConnectEvents(Me, EventMask)

            Dim events As IMSIGX.IAppEvents
            events = Me

            iConnectId1 = objApp.ConnectEvents(Me, EventMask1)
            iConnectId2 = objApp.ConnectEvents(Me, EventMask2)
            '        AddInspectorBar True
            AddLM(True)
            '        theToolEvents.ToolChangePrompt Me, "Click Point on the Contour", False
            theToolEvents.ToolChangePrompt(Me, TcLoadLangString(101), False)


        End If
        '    Call Finish

        Run = True

#If False Then
        TheCursorImage = My.Resources.cur101
        'UPGRADE_ISSUE: Picture property TheCursorImage.Handle was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        hToolCursor = TheCursorImage.Handle
#End If
        oToolCursor = New Cursor(Me.GetType(), "cur101.cur")
        '        hToolCursor = oToolCursor.Handle

        Exit Function
ErrorHandler:
        Run = False
        '    MsgBox Err.Msg
        Call Finish()
    End Function

    Private Sub CurrentWPToWorld(ByRef Dr As IMSIGX.Drawing)
        Dim MatrWorld As IMSIGX.Matrix
        Dim MatrDr As IMSIGX.Matrix
        On Error Resume Next
        MatrDr = Dr.UCS
        MatrWorld = Dr.UCS
        Dim e02, e00, e01, e03 As Double
        Dim e12, e10, e11, e13 As Double
        Dim e22, e20, e21, e23 As Double
        Dim e32, e30, e31, e33 As Double
        MatrDr.GetEntries(e00, e01, e02, e03, e10, e11, e12, e13, e20, e21, e22, e23, e30, e31, e32, e33)

        e00 = 1 : e01 = 0 : e02 = 0 : e03 = 0
        e10 = 0 : e11 = 1 : e12 = 0 : e13 = 0
        e20 = 0 : e21 = 0 : e22 = 1 : e23 = 0
        e30 = 0 : e31 = 0 : e32 = 0 : e33 = 1

        MatrWorld.SetEntries(e00, e01, e02, e03, e10, e11, e12, e13, e20, e21, e22, e23, e30, e31, e32, e33)
        On Error Resume Next
        If MatrDr.IsEqual(MatrWorld) = False Then
            On Error Resume Next
            Dr.UCS = MatrWorld
            Dr.Properties.Item("$GRIDSMS").Value = 0
        End If

    End Sub


#If _USE_NATIVEIMSIGX Then
    'Fill arrays with information about tools in the package
    'Return the number of tools in the package
    Public Function GetToolInfo(ByRef CommandNames As Object, ByRef MenuCaptions As Object, ByRef StatusPrompts As Object, ByRef ToolTips As Object, ByRef Enabled As Object, ByRef WantsUpdates As Object) As Integer

        Dim sICmd As String

        ReDim CommandNames(NUM_TOOLS, 4)
        ReDim MenuCaptions(NUM_TOOLS, 2)
        ReDim StatusPrompts(NUM_TOOLS, 2)
        ReDim ToolTips(NUM_TOOLS)
        ReDim Enabled(NUM_TOOLS)
        ReDim WantsUpdates(NUM_TOOLS)
        sICmd = TcLoadLangString(103) '"Modify|Chain Polyline"
        'UPGRADE_WARNING: Couldn't resolve default property of object CommandNames(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        CommandNames(0, 0) = sICmd & "#CMD_CHAINPILYLINE" '# NLS#'
        'UPGRADE_WARNING: Couldn't resolve default property of object CommandNames(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        CommandNames(0, 1) = TcLoadLangString(125) '"Generic Properties"
        'UPGRADE_WARNING: Couldn't resolve default property of object CommandNames(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        CommandNames(0, 2) = TcLoadLangString(126) '"Polyline"
        'UPGRADE_WARNING: Couldn't resolve default property of object CommandNames(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        CommandNames(0, 3) = TcLoadLangString(127) '"Internal"

        'UPGRADE_WARNING: Couldn't resolve default property of object MenuCaptions(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        MenuCaptions(0, 0) = TcLoadLangString(104) '"&Chain Polyline"
        'UPGRADE_WARNING: Couldn't resolve default property of object MenuCaptions(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        MenuCaptions(0, 1) = TcLoadLangString(104) '"&Chain Polyline"
        'UPGRADE_WARNING: Couldn't resolve default property of object StatusPrompts(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        StatusPrompts(0, 0) = TcLoadLangString(105) '"Chain Polyline"
        'UPGRADE_WARNING: Couldn't resolve default property of object StatusPrompts(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        StatusPrompts(0, 1) = "95140" '"95138"
        'UPGRADE_WARNING: Couldn't resolve default property of object ToolTips(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ToolTips(0) = TcLoadLangString(105) '"Chain Polyline"
        'UPGRADE_WARNING: Couldn't resolve default property of object Enabled(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Enabled(0) = True
        'UPGRADE_WARNING: Couldn't resolve default property of object WantsUpdates(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        WantsUpdates(0) = False
        GetToolInfo = NUM_TOOLS



    End Function

#Else

    Public Function GetToolInfo(ByRef ToolInfo As TCDotNetInterfaces.TurboCADToolInfo) As Boolean _
        Implements ITurboCADTool.GetToolInfo

        ToolInfo = New TCDotNetInterfaces.TurboCADToolInfo

        ToolInfo.InternalCommand = "CMD_CHAINPILYLINE"
        ToolInfo.CommandName = TcLoadLangString(103) ''"Modify|Chain Polyline"
        ToolInfo.CommandName = ToolInfo.CommandName.Replace(CChar("|"), CChar(Chr(10)))

        ToolInfo.MenuCaption = TcLoadLangString(104) '"&Chain Polyline"
        ToolInfo.ToolbarName = TcLoadLangString(105) '"Chain Polyline" 
        ToolInfo.ToolTip = TcLoadLangString(105) '"Chain Polyline"

        ToolInfo.bEnabled = True
        ToolInfo.bWantsUpdates = False

        ToolInfo.ToolbarImage = My.Resources.bmp1001
        ToolInfo.ToolbarImageL = My.Resources.bmp1002

        ToolInfo.ToolbarImageBW = My.Resources.bmp1001
        ToolInfo.ToolbarImageLBW = My.Resources.bmp1002

        GetToolInfo = True

    End Function
#End If


    'Returns true if tool is correctly initialized
#If _USE_NATIVEIMSIGX = False Then
    Public Function Initialize(ByVal objTool As IMSIGX.Tool) As Boolean _
        Implements ITurboCADTool.Initialize
#Else
    Public Function Initialize(ByVal objTool As IMSIGX.Tool) As Boolean
#End If

        On Error GoTo ErrorHandler

        CIRCLETYPE = "CIRCLE" 'TcLoadLangString(121)'# NLS#'
        ARCTYPE = "ARC" 'TcLoadLangString(122)'# NLS#'
        GRAPHICTYPE = "GRAPHIC" 'TcLoadLangString(123)'# NLS#'

        'UPGRADE_WARNING: Couldn't resolve default property of object objTool.Application. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        objApp = objTool.Application

        Dim gxTools As IMSIGX.Tools
        Dim gxToolFirst As IMSIGX.Tool

        gxTools = objApp.Tools
        gxToolFirst = gxTools.Item(0)

        gxMe = gxTools.Item(objTool.ID - gxToolFirst.ID)

        GXMD.Release(gxTools)

        Dim oToolEvents As Object = objApp.ToolEvents
        theToolEvents = oToolEvents

        iConnectId1 = -1
        iConnectId2 = -1
        iActTool = -1

        Initialize = True

        Exit Function
ErrorHandler:
        '   MsgBox Err.Description
    End Function

#If _USE_NATIVEIMSIGX = False Then
    Public Sub Terminate(ByVal objTool As IMSIGX.Tool) _
        Implements ITurboCADTool.Terminate
#Else
    Public Sub Terminate(ByVal objTool As IMSIGX.Tool)
#End If

        'UPGRADE_NOTE: Object gxMe may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(gxMe)
        GXMD.Release(theToolEvents)
        GXMD.Release(objApp)

        iConnectId1 = -1
        iConnectId2 = -1
        iActTool = -1

        GXMD.Collect()
    End Sub

    'Returns true if tool is correctly initialized
#If _USE_NATIVEIMSIGX = False Then
    Public Function UpdateToolStatus(ByVal Tool As IMSIGX.Tool, ByRef Enabled As Boolean, ByRef Checked As Boolean) As Boolean _
        Implements ITurboCADTool.UpdateToolStatus
#Else
    Public Function UpdateToolStatus(ByVal Tool As IMSIGX.Tool, ByRef Enabled As Boolean, ByRef Checked As Boolean) As Boolean
#End If

#If True Then
        '      Dim theXTool As IMSIGX.Tool
        '      theXTool = Tool
        Dim gxDwg As IMSIGX.Drawing
        Dim gxProps As IMSIGX.Properties
        Dim gxProp As IMSIGX.Property
        Dim gxGrs As IMSIGX.Graphics

        gxDwg = objApp.ActiveDrawing
        If (gxDwg Is Nothing) Then

            Enabled = False
            Checked = False
            UpdateToolStatus = True
            Exit Function

        End If

        Dim SpaceCur As IMSIGX.ImsiSpaceModeType
        If iConnectId1 > -1 Or iConnectId2 > -1 Then

            gxProps = gxDwg.Properties
            gxProp = gxProps.Item("TileMode")
            SpaceCur = gxProp.Value
            GXMD.Release(gxProp)
            GXMD.Release(gxProps)

            If SpaceCur <> Space_Renamed Then
                Finish()
            End If
        End If

        Enabled = True

        'UPGRADE_WARNING: Couldn't resolve default property of object Tool.Application. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'

        gxGrs = gxDwg.Graphics
        If gxGrs.Count = 0 Then
            Enabled = False
        Else
            Enabled = True
        End If

        GXMD.Release(gxGrs)

        GXMD.Release(gxProp)
        GXMD.Release(gxProps)

        GXMD.Release(gxDwg)

        If iConnectId1 <> -1 Or iConnectId2 <> -1 Then
            Checked = True
        Else
            Checked = False 'Could do a test here to determine whether to check the button/menu item
        End If
#Else
        Enabled = True
        Checked = False 'Could do a test here to determine whether to check the button/menu item
#End If
        UpdateToolStatus = True
    End Function


    Public Sub RunTool(ByRef WhichTool As Object)
        On Error Resume Next
        Call Finish()
    End Sub



    Public Sub BeforeExit(ByRef WhichApplication As Object, ByRef Cancel As Boolean)
        Call Finish()
    End Sub
    Public Sub DrawingNew(ByRef WhichDrawing As Object)
        Call Finish()
    End Sub
    Public Sub DrawingOpen(ByRef WhichDrawing As Object)
        Call Finish()
    End Sub
    Public Sub DrawingActivate(ByRef WhichDrawing As Object)
        Call Finish()
    End Sub
    Public Sub DrawingDeactivate(ByRef WhichDrawing As Object)
        Call Finish()
    End Sub
    Public Sub DrawingBeforeClose(ByRef WhichDrawing As Object, ByRef Cancel As Boolean)
        Call Finish()
    End Sub
    Public Sub DrawingBeforeSave(ByRef WhichDrawing As Object, ByRef SaveAs As Boolean, ByRef Cancel As Boolean)
        Call Finish()
    End Sub



    Public Sub MouseDown(ByRef oWhichDrawing As Object, ByRef oWhichView As Object, ByRef oWhichWindow As Object, ByRef Button As IMSIGX.ImsiMouseButton, ByRef Shift As Integer, ByRef x As Integer, ByRef y As Integer, ByRef Cancel As Boolean)
        'UPGRADE_NOTE: Space was upgraded to Space_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Space_Renamed As IMSIGX.ImsiSpaceModeType

        Dim WhichDrawing As IMSIGX.Drawing
        Dim WhichView As IMSIGX.View

        GXMD.Collect()

        WhichDrawing = oWhichDrawing
        WhichView = oWhichView

        Space_Renamed = WhichView.SpaceMode

        GxSet = WhichDrawing.GraphicSets.Add("GxSet", False) '# NLS#'

        Dim xClick, yClick As Double
        '    hDr = TCWDrawingActive
        On Error GoTo ErrorHandler
        ' GXMD.Release(ActDr)
        ActDr = WhichDrawing
        Grs = ActDr.Graphics
        Vi = WhichView

        Dim Gr As IMSIGX.Graphic
        Vi.ScreenToView(x, y, xClick, yClick)
        'MsgBox ("xcWorld=" & CStr(xcWorld) & "  ycWorld=" & CStr(ycWorld))
        Dim ViH As Double
        ViH = Vi.ViewHeight
        Dim Aper As Double
        Aper = ViH / 50
        Dim PRes As IMSIGX.PickResult
        Dim j, PicCount, i, k As Integer
        Dim SetCount As Integer
        Dim GrSet As IMSIGX.GraphicSet
        Dim GrCont As IMSIGX.Graphic
        Dim GrType As String
        Dim Thick As Double
        Dim hGr As Integer
        Dim hMat As Integer
        Dim Ver As IMSIGX.Vertex
        Dim c, a, b, d As Double
        Dim ChildOnlyDATA As Boolean
        Dim ChildCount As Integer
        Dim t As Integer
        Dim GrOrig As IMSIGX.Graphic
        Dim ArcData(10) As Double
        Dim Ratio As Double
        Dim xP2, xP1, yP1, yP2 As Double
        Dim CreateCurvBez As New TrimCurveBezier
        Dim SplToBez As New BiSplineToBezier
        Dim GrBez As IMSIGX.Graphic
        Dim URec As IMSIGX.UndoRecord
'        Dim GrDup As IMSIGX.Graphic
        Dim Apert As Double
        Dim DirCount As Integer
        Dim GrName As String
        Dim VerNear As IMSIGX.Vertex
        Dim yi, xi, zi As Double
        Dim yWi, xWi, zWi As Double
        Dim yVi, xVi, zVi As Double
        Dim iNear As Integer
        Dim LMin, Li As Double
        Dim StrLen As Short
        Dim iCont As Integer
        Dim Direct As Short
        Dim CreateCurvBez1 As New TrimCurveBezier
        Dim SplToBez1 As New BiSplineToBezier
        Dim GrBez1 As IMSIGX.Graphic
        'UPGRADE_NOTE: Char was upgraded to Char_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Char_Renamed As String
        'UPGRADE_WARNING: Arrays in structure ContLast may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLast As Contour
        'UPGRADE_NOTE: Dir was upgraded to Dir_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Dir_Renamed As Short
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As Segment
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As Contour
        If Button = IMSIGX.ImsiMouseButton.imsiLeftButton Then
            Cancel = True
            'Dim SnapMode As Long

            ' ################################################
            ' ################################################
            ' Main Chaining
            If NumClick = 0 Then
                GrFirstID = 0
                '            SnapMode = 256
                '            ActDr.Application.SnapModes = 256
                '            Set PRes = Vi.PickPoint(xClick, yClick)
                PRes = Vi.PickRect(xClick - Aper, yClick - Aper, xClick + Aper, yClick + Aper, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
                PicCount = PRes.Count

                If PicCount = 0 Then
                    '                ActDr.Application.SnapModes = 1
                    'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(PRes)
                    GXMD.Collect()
                    Exit Sub
                End If
                GrSet = GrSets.Add("", True)
                For i = 0 To PicCount - 1
                    GrCont = PRes.Item(i).Graphic
                    GrType = GrCont.Type
                    '                If GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC" Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Then
                    If GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                        '##########################################
                        If GrCont.Graphics.Count > 0 Then
                            For j = 0 To GrCont.Graphics.Count - 1
                                If GrCont.Graphics.Item(j).Type = GRAPHICTYPE Then
                                    If GrCont.Graphics.Item(j).Properties.Item("Info").Value = "Line_Width_Cosmetic" Then
                                        GoTo II
                                    End If
                                End If
                            Next j
                        End If
                        '##########################################

                        If ActDr.Properties.Item("TileMode").Value = 1 Then
                            Thick = 0
                            On Error Resume Next
                            Thick = GrCont.Properties.Item("Thickness").Value
                            If System.Math.Abs(Thick) > Eps Then GoTo II
                            Thick = -1000000002
                            On Error Resume Next
                            Thick = GrCont.Properties.Item("Thickness").Value
                            If Thick = -1000000002 Then GoTo II
                        End If

                        '                    If (GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC") And GrCont.Graphics.Count > 0 Then
                        If (GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE) And GrCont.Graphics.Count > 0 Then
                            ChildOnlyDATA = True
                            With GrCont.Graphics
                                ChildCount = .Count
                                For t = 0 To ChildCount - 1
                                    If .Item(t).Type <> "DATA" Then
                                        ChildOnlyDATA = False
                                        Exit For
                                    End If
                                Next t
                            End With
                            If ChildOnlyDATA = False Then
                                GoTo II
                            End If
                        End If
                        If GrCont.Unbounded = True Then GoTo II

                        If GrType = "TCW30CURVE" Then
                            With GrCont.Graphics
                                ChildCount = .Count
                                For t = 0 To ChildCount - 1
                                    If .Item(t).Type = "TCW50IntProp" Then
                                        GoTo II
                                    End If
                                Next t
                            End With
                        End If

                        '----------------------------------------
                        '                    hGr = TCWGraphicAt(hDr, GrCont.Index)
                        '                    hMat0 = GraphicGetMatrix(hGr)
                        '                    MatrixGetPlane hMat0, a0, b0, c0, d0
                        UCSGetPlane(GrCont.UCS, a0, b0, c0, d0)

                        If System.Math.Abs(a0) < Eps And System.Math.Abs(b0) < Eps And System.Math.Abs(d0) < Eps And System.Math.Abs(c0 + 1) < Eps Then
                            On Error Resume Next
                            '                        GrContCur.UCS = ActDr.UCS
                        End If
                        On Error Resume Next

                        If Not GrContCur Is Nothing Then
                            UCSGr0 = GrContCur.UCS
                        End If
                        '----------------------------------------
                        If Not GrCont Is Nothing Then
                            GrSet.AddGraphic(GrCont, oMissing, oMissing)
                        End If
                    End If
II:
                Next i
                SetCount = GrSet.Count
                If SetCount = 0 Then
                    MsgBox(TcLoadLangString(107), , TcLoadLangString(105))
                    '                ActDr.Application.SnapModes = 1
                    GrSet.Clear(oMissing)

                    'UPGRADE_NOTE: Object GrSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrSet)
                    'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrCont)
                    'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'

                    GXMD.Release(PRes)

                    'GXMD.Release(WhichDrawing, False)
                    'GXMD.Release(WhichView)

                    GXMD.Collect()
                    Exit Sub
                End If


                'UPGRADE_NOTE: Object Ver may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                GXMD.Release(Ver)
                GXMD.Collect()
                '            ActDr.Application.SnapModes = 1
                GrContCur = GrSet.Item(0)
                Layer1 = GrContCur.Layer
                GrGroup.Layer = Layer1
                For i = 0 To PicCount - 1
                    If PRes.Item(i).Graphic.ID = GrContCur.ID Then
                        Ver = PRes.Item(i).ClosestVertex
                        x00 = Ver.X
                        y00 = Ver.Y
                        z00 = Ver.Z

                        'Set Gr = New Graphic
                        Gr = MakeGraphic()

                        Ver = Gr.Vertices.Add(x00, y00, z00)
                        On Error Resume Next
                        Gr.Transform(GrContCur.UCS)
                        With Gr.Vertices
                            .UseWorldCS = True
                            x00W = Ver.X
                            y00W = Ver.Y
                            z00W = Ver.Z
                            .UseWorldCS = False
                        End With
                        Gr.Delete()
                        'MsgBox ("x00=" & CStr(x00) & " y00=" & CStr(y00) & " z00=" & CStr(z00) & "  x00W=" & CStr(x00W) & " y00W=" & CStr(y00W) & " z00W=" & CStr(z00W))
                        GXMD.Release(Gr)
                        GXMD.Collect()
                        Exit For
                    End If
                Next i
                '----------------------------------------
                '            hGr = TCWGraphicAt(hDr, GrContCur.Index)
                '            hMat0 = GraphicGetMatrix(hGr)
                '            MatrixGetPlane hMat0, a0, b0, c0, d0

                UCSGetPlane(GrContCur.UCS, a0, b0, c0, d0)

                If System.Math.Abs(a0) < Eps And System.Math.Abs(b0) < Eps And System.Math.Abs(d0) < Eps And System.Math.Abs(c0 + 1) < Eps Then
                    On Error Resume Next
                    '                GrContCur.UCS = ActDr.UCS
                End If

                On Error Resume Next
                UCSGr0 = GrContCur.UCS
                '----------------------------------------
                On Error Resume Next
                If Space_Renamed = IMSIGX.ImsiSpaceModeType.imsiModelSpace Then
                    If ActDr.UCS.IsEqual(UCSGr0) = False Then
                        ActDr.Properties.Item("$GRIDSMS").Value = 0
                        ActDr.UCS = UCSGr0
                    End If
                End If


                GrFirstID = GrContCur.ID
                GrCurID = GrFirstID
                NumClick = 1
                AddLM(False)
                AddLM(True)


                theToolEvents.ToolChangePrompt(Me, "", False)
                '            theToolEvents.ToolChangePrompt Me, "Click Directional Point", False
                theToolEvents.ToolChangePrompt(Me, TcLoadLangString(102), False)
                GrSet.Clear(oMissing)
                'UPGRADE_NOTE: Object GrSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                GXMD.Release(GrSet)
                'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                GXMD.Release(GrCont)
                'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                GXMD.Release(PRes)

                GXMD.Release(WhichDrawing, False)
                GXMD.Release(WhichView)

                GXMD.Collect()

                '************************
                LastPointEnable = True
                AddLM(False)
                AddLM(True)
                '************************

                Exit Sub
            End If
            ' ################################################
            ' ################################################
            If NumClick = 1 Then

                'SetClassLong(CInt(WhichView.HWND), GCL_HCURSOR, hTCCursor)

                If Space_Renamed = IMSIGX.ImsiSpaceModeType.imsiModelSpace Then
                    Call CurrentWPToWorld(ActDr)
                End If

                '            objApp.SnapModes = 1
                Vi.ViewToWorld(xClick, yClick, 0, x0Dir, y0Dir, z0Dir)
                L0Dir = System.Math.Sqrt((x0Dir - x00) ^ 2 + (y0Dir - y00) ^ 2 + (z0Dir - z00) ^ 2)
FIRSTSEGMENT:
                NumClick = 2
                InterExist = False
                InterExist = CreateFirstCurContour()

                Call CreateFirstDirection()

NEWINTER:
                xIntCur = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                yIntCur = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
                'MsgBox ("InterExist=" & CStr(InterExist))
                'MsgBox ("xIntCur=" & CStr(xIntCur) & "  yIntCur=" & CStr(yIntCur) & " InterExist=" & CStr(InterExist))
                ' if first contour not intersect with other
                If InterExist = False Then 'Add Current contour to Contors Collection
                    ReDim Preserve ContsRes.Cont(ContsRes.NumCont)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContsRes.Cont(ContsRes.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContsRes.Cont(ContsRes.NumCont) = ContRes
                    ContsRes.NumCont = ContsRes.NumCont + 1

                    Call TrimContByLastPoint()

                    ReDim ContRes.Segm(0)
                    k = 0
                    For i = 0 To ContsRes.NumCont - 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Cont = ContsRes.Cont(i)
                        For j = 0 To Cont.NumSegm - 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Segm = Cont.Segm(j)
                            ReDim Preserve ContRes.Segm(k)
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContRes.Segm(k). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContRes.Segm(k) = Segm
                            k = k + 1
                        Next j
                    Next i
                    ContRes.NumSegm = k

                    '??????????????????????????????????????????????????
                    '??????????????????????????????????????????????????
                    '??????????????????????????????????????????????????

                    ' If trimed single graphic
                    If ContsRes.NumCont = 1 And GrFirstID = GrCurID Then
                        GrOrig = Grs.GraphicFromID(GrFirstID)
                        GrType = GrOrig.Type
                        If GrType = "CIRCLE" Or GrType = "ARC" Then
                            GrOrig.GetArcData(ArcData)
                            Ratio = ArcData(6)
                            If System.Math.Abs(Ratio - 1) > Eps Then GrType = "Ellipse"
                        End If ' If GrType = "CIRCLE" Or GrType = "ARC"

                        If GrType = "TCW30CURVE" Then
                            If GrOrig.Properties.Item("$SPLINETYPE").Value = 1 Then GrType = "Bezier"
                            If GrOrig.Properties.Item("$SPLINETYPE").Value = 0 Then GrType = "BiSpline"
                        End If ' If GrType = "TCW30CURVE"

                        '+++++++++++++++++++++++++++++++++++++++++++++
                        If GrType = "Ellipse" Then
                            GrCont = TrimEllipse(GrOrig)
                            If Not GrCont Is Nothing Then
                                GoTo ADDCHAIN0
                            End If
                        End If ' If GrType = "Ellipse"
                        '+++++++++++++++++++++++++++++++++++++++++++++

                        '+++++++++++++++++++++++++++++++++++++++++++++
                        If GrType = "Bezier" Then 'Or GrType = "BiSpline" Then
                            xP1 = ContRes.Segm(0).xyBeg.v0
                            yP1 = ContRes.Segm(0).xyBeg.v1
                            xP2 = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                            yP2 = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
                            If GrType = "Bezier" Then
                                GrCont = CreateCurvBez.TrimCurve(GrOrig, xP1, yP1, xP2, yP2, Dir_Renamed)
                            End If ' If GrType = "Bezier"

                            If GrType = "BiSpline" Then
                                GrBez = SplToBez.SplineToBezier(GrOrig)
                                'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                GXMD.Release(GrCont)
                                GXMD.Collect()
                                If Not GrBez Is Nothing Then
                                    GrCont = CreateCurvBez.TrimCurve(GrBez, xP1, yP1, xP2, yP2, Dir_Renamed)
                                    GrBez.Visible = False
                                    GrBez.Draw()
                                    Grs.Remove(GrBez.Index)
                                    GrBez.Delete()
                                    GXMD.Release(GrBez)
                                    GXMD.Collect()
                                End If ' If Not GrBez Is Nothing
                            End If ' If GrType = "BiSpline"


                            If Not GrCont Is Nothing Then
                                GoTo ADDCHAIN0
                            End If

                        End If ' If GrType = "Bezier" Or GrType = "BiSpline"


                        '+++++++++++++++++++++++++++++++++++++++++++++

                    End If 'If ContsRes.NumCont = 1 And GrFirstID = GrCurID
                    '??????????????????????????????????????????????????
                    '??????????????????????????????????????????????????
                    '??????????????????????????????????????????????????


                    Call ClearContFromParalSegments(ContRes)
                    Call CreatePlaneGraphic(ContRes, GrCont)

ADDCHAIN0:
                    GrCont.Properties.Item("PenColor").Value = RGB(255, 0, 0)
                    GrCont.Properties.Item("PenWidth").Value = 0.02
                    GrCont.Properties.Item("PenStyle").Value = "CONTINUOUS"
                    '                GrCont.Layer = Layer1
                    On Error Resume Next
                    'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    GrCont.Transform(UCSGr0)

                    Grs.AddGraphic(GrCont, oMissing, oMissing)
                    GrCont.Layer = Layer1
                    GrCont.Update()
                    GrCont.Draw()
                    '                GrCont.Draw

                    '                hGr = TCWGraphicAt(hDr, GrCont.Index)
                    '                TCWUndoRecordStart hDr, "Chain"
                    '                TCWUndoRecordStart hDr, TcLoadLangString(105)
                    '                TCWUndoRecordAddGraphic hDr, hGr
                    '                TCWUndoRecordEnd hDr
                    URec = ActDr.AddUndoRecord(TcLoadLangString(128))
'                    GrDup = GrCont.Duplicate
'                    URec.AddGraphic(GrDup)
                    URec.AddGraphic(GrCont)
'                    Grs.Remove(GrCont.Index)
'                    GrCont.Delete()
                    URec.Close() ' #1

                    'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GrCont = DBNull.Value
                    GXMD.Release(GrCont)
                    ReDim ContRes.Segm(0)
                    ContRes.NumSegm = 0
                    NumClick = 0
                    GXMD.Collect()
                    '                ActDr.Application.SnapModes = 1
                    Call Finish()
                Else
                    ' if first contour  intersects with other
                    If CreateDirectionalVectors() = False Then
                        InterExist = FindIntersection(GrContCur, ContCur, iCurSeg, ILAST)
                        GoTo NEWINTER
                    Else
                        ReDim Preserve ContsCur.Cont(ContsCur.NumCont)
                        'UPGRADE_WARNING: Couldn't resolve default property of object ContsCur.Cont(ContsCur.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        ContsCur.Cont(ContsCur.NumCont) = ContCur
                        ContsCur.NumCont = ContsCur.NumCont + 1
                        '+++++++++++++++++++++++++++++++++++++++++++++++
                        '+++++++++++++++++++++++++++++++++++++++++++++++

                        AddLM(False)
                        AddLM(True)
                        theToolEvents.ToolChangePrompt(Me, "", False)
                        '                    theToolEvents.ToolChangePrompt Me, "Select New Direction or Finish", False
                        theToolEvents.ToolChangePrompt(Me, TcLoadLangString(106), False)
                        Exit Sub
                    End If
                End If
            End If

            ' ################################################
            ' ################################################
            If NumClick > 1 Then
NEWMIDINTER:
                If ContRes.NumSegm > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContLast. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContLast = ContRes
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContLast. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContLast = ContsRes.Cont(ContsRes.NumCont - 1)
                End If
                xIntCur = ContLast.Segm(ContLast.NumSegm - 1).xyEnd.v0
                yIntCur = ContLast.Segm(ContLast.NumSegm - 1).xyEnd.v1
                Apert = ViH / 50
                'NEWDIRSELECTION:
                '            Set PRes = Vi.PickPoint(xClick, yClick, Apert)
                GXMD.Release(PRes)
                PRes = Vi.PickRect(xClick - Apert, yClick - Apert, xClick + Apert, yClick + Apert, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
                PicCount = PRes.Count
                If PicCount = 0 Then
                    'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(PRes)

                    GXMD.Release(WhichDrawing)
                    GXMD.Release(WhichView)

                    GXMD.Collect()
                    Exit Sub
                End If
                DirCount = 0
                iNear = -1
                LMin = 100000000
                For i = 0 To PicCount - 1
                    Gr = PRes.Item(i).Graphic
                    '                If Gr.Type = "GRAPHIC" And Gr.Graphics.Count > 0 Then
                    If Gr.Type = GRAPHICTYPE And Gr.Graphics.Count > 0 Then
                        GrName = Gr.Name
                        If GrName = "Current" Or Mid(GrName, 1, 3) = "Dir" Then '# NLS#'
                            DirCount = DirCount + 1
                            VerNear = PRes.Item(i).ClosestVertex
                            xi = VerNear.X
                            yi = VerNear.Y
                            zi = VerNear.Z
                            Gr = MakeGraphic()
                            Ver = Gr.Vertices.Add(xi, yi, zi)
                            On Error Resume Next
                            'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Gr.Transform(UCSGr0)
                            With Gr.Vertices
                                .UseWorldCS = True
                                xWi = Ver.X
                                yWi = Ver.Y
                                zWi = Ver.Z
                                .UseWorldCS = False
                            End With
                            Gr.Delete()
                            GXMD.Release(Gr)
                            GXMD.Collect()
                            Vi.WorldToView(xWi, yWi, zWi, xVi, yVi, zVi)
                            Li = System.Math.Sqrt((xClick - xVi) ^ 2 + (yClick - yVi) ^ 2)
                            If Li < LMin Then
                                iNear = i
                                LMin = Li
                            End If
                        End If
                    End If
                Next i
                If DirCount = 0 Or iNear = -1 Then
                    'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(PRes)
                    GXMD.Collect()
                    Exit Sub
                End If
                '            If DirCount <> 1 And Apert > ViH / 10000 Then
                '                Apert = Apert / 10
                '                GoTo NEWDIRSELECTION
                '            End If
                '            For i = 0 To PicCount - 1
                i = iNear
                Gr = PRes.Item(i).Graphic
                '                If Gr.Type = "GRAPHIC" And Gr.Graphics.Count > 0 Then
                If Gr.Type = GRAPHICTYPE And Gr.Graphics.Count > 0 Then
                    GrName = Gr.Name
                    ' +++++++++++++++++++++++++++++++++++++++++++
                    ' If next - Current contour
                    If GrName = "Current" Then '# NLS#'
                        GrCont = Grs.GraphicFromID(ContCur.GrID)
                        Call CreateContFromPointToDirection(ContCur, xIntCur, yIntCur, 1)
                        iCurSeg = 0
                        GoTo INTERSECTION
                    End If
                    ' +++++++++++++++++++++++++++++++++++++++++++

                    If Mid(GrName, 1, 3) = "Dir" Then '# NLS#'
                        'MsgBox ("@@@@@  " & GrName)
                        iCont = 0
                        Direct = 1
                        StrLen = Len(GrName)
                        Char_Renamed = Right(GrName, 1)
                        iCont = CInt(Char_Renamed)
                        Char_Renamed = Mid(GrName, 4, 1)
                        If Char_Renamed = "-" Then Direct = -1

                        'UPGRADE_WARNING: Couldn't resolve default property of object ContCur. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        ContCur = ContsInt.Cont(iCont)
                        GrContCur = Grs.GraphicFromID(ContCur.GrID)
                        Call CreateContFromPointToDirection(ContCur, xIntCur, yIntCur, Direct)
                        iCurSeg = 0
                        GoTo INTERSECTION
                    End If
                End If
                '            Next i

                'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                GXMD.Release(PRes)
                'UPGRADE_NOTE: Object Gr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                GXMD.Release(Gr)
                GXMD.Collect()
INTERSECTION:
                'MsgBox ("xIntCur=" & CStr(xIntCur) & "  yIntCur=" & CStr(yIntCur))

                InterExist = FindIntersection(GrContCur, ContCur, iCurSeg, ILAST)
                'MsgBox ("InterExist=" & CStr(InterExist))
                If InterExist = False Then
                    ReDim Preserve ContsRes.Cont(ContsRes.NumCont)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContsRes.Cont(ContsRes.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContsRes.Cont(ContsRes.NumCont) = ContRes
                    ContsRes.NumCont = ContsRes.NumCont + 1

                    Call TrimContByLastPoint()

                    ReDim ContRes.Segm(0)
                    k = 0
                    For i = 0 To ContsRes.NumCont - 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Cont = ContsRes.Cont(i)
                        For j = 0 To Cont.NumSegm - 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Segm = Cont.Segm(j)
                            ReDim Preserve ContRes.Segm(k)
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContRes.Segm(k). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContRes.Segm(k) = Segm
                            k = k + 1
                        Next j
                    Next i
                    ContRes.NumSegm = k

                    '??????????????????????????????????????????????????
                    '??????????????????????????????????????????????????
                    '??????????????????????????????????????????????????

                    ' If trimed single graphic
                    If ContsRes.NumCont = 1 And GrFirstID = GrCurID Then
                        GrOrig = Grs.GraphicFromID(GrFirstID)
                        GrType = GrOrig.Type
                        If GrType = "CIRCLE" Or GrType = "ARC" Then
                            GrOrig.GetArcData(ArcData)
                            Ratio = ArcData(6)
                            If System.Math.Abs(Ratio - 1) > Eps Then GrType = "Ellipse"
                        End If ' If GrType = "CIRCLE" Or GrType = "ARC"

                        If GrType = "TCW30CURVE" Then
                            If GrOrig.Properties.Item("$SPLINETYPE").Value = 1 Then GrType = "Bezier"
                            If GrOrig.Properties.Item("$SPLINETYPE").Value = 0 Then GrType = "BiSpline"
                        End If ' If GrType = "TCW30CURVE"

                        '+++++++++++++++++++++++++++++++++++++++++++++
                        If GrType = "Ellipse" Then
                            GrCont = TrimEllipse(GrOrig)
                            If Not GrCont Is Nothing Then
                                GoTo ADDCHAIN
                            End If
                        End If ' If GrType = "Ellipse"
                        '+++++++++++++++++++++++++++++++++++++++++++++

                        '+++++++++++++++++++++++++++++++++++++++++++++
                        If GrType = "Bezier" Then 'Or GrType = "BiSpline" Then
                            xP1 = ContRes.Segm(0).xyBeg.v0
                            yP1 = ContRes.Segm(0).xyBeg.v1
                            xP2 = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                            yP2 = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1

                            If GrType = "Bezier" Then
                                'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                GXMD.Release(GrCont)
                                GXMD.Collect()
                                GrCont = CreateCurvBez1.TrimCurve(GrOrig, xP1, yP1, xP2, yP2, Dir_Renamed)
                            End If ' If GrType = "Bezier"

                            If GrType = "BiSpline" Then
                                GrBez1 = SplToBez1.SplineToBezier(GrOrig)
                                'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                GXMD.Release(GrCont)
                                GXMD.Collect()
                                If Not GrBez1 Is Nothing Then
                                    GrCont = CreateCurvBez1.TrimCurve(GrBez, xP1, yP1, xP2, yP2, Dir_Renamed)
                                    GrBez1.Visible = False
                                    GrBez1.Draw()
                                    Grs.Remove(GrBez1.Index)
                                    GrBez1.Delete()
                                    GXMD.Release(GrBez1)
                                    GXMD.Collect()
                                End If ' If Not GrBez1 Is Nothing
                            End If ' If GrType = "BiSpline"

                            If Not GrCont Is Nothing Then
                                GoTo ADDCHAIN
                            End If

                        End If ' If GrType = "Bezier"
                        '+++++++++++++++++++++++++++++++++++++++++++++

                    End If 'If ContsRes.NumCont = 1 And GrFirstID = GrCurID
                    '??????????????????????????????????????????????????
                    '??????????????????????????????????????????????????
                    '??????????????????????????????????????????????????

                    Call ClearContFromParalSegments(ContRes)
                    Call CreatePlaneGraphic(ContRes, GrCont)
ADDCHAIN:
                    GrCont.Properties.Item("PenColor").Value = RGB(255, 0, 0)
                    GrCont.Properties.Item("PenWidth").Value = 0.02
                    GrCont.Properties.Item("PenStyle").Value = "CONTINUOUS"
                    '                GrCont.Layer = Layer1
                    On Error Resume Next
                    'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    GrCont.Transform(UCSGr0)

                    Grs.AddGraphic(GrCont, oMissing, oMissing)
                    GrCont.Layer = Layer1
                    GrCont.Update()
                    GrCont.Draw()
                    '                GrCont.Draw

                    URec = ActDr.AddUndoRecord(TcLoadLangString(128))
'                    GrDup = GrCont.Duplicate
'                    URec.AddGraphic(GrDup)
                    URec.AddGraphic(GrCont)
'                    Grs.Remove(GrCont.Index)
'                    GrCont.Delete()
                    URec.Close() ' #2

                    'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GrCont = DBNull.Value

                    GXMD.Release(GrCont)
                    ReDim ContRes.Segm(0)
                    ContRes.NumSegm = 0
                    NumClick = 0
                    '                ActDr.Application.SnapModes = 1
                    'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrCont)
                    GXMD.Collect()

                    Call Finish()
                Else
                    If CreateDirectionalVectors() = False Then
                        If ContRes.NumSegm > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContLast. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContLast = ContRes
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContLast. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContLast = ContsRes.Cont(ContsRes.NumCont - 1)
                        End If
                        xIntCur = ContLast.Segm(ContLast.NumSegm - 1).xyEnd.v0
                        yIntCur = ContLast.Segm(ContLast.NumSegm - 1).xyEnd.v1

                        GoTo INTERSECTION
                    Else
                        ReDim Preserve ContsCur.Cont(ContsCur.NumCont)
                        'UPGRADE_WARNING: Couldn't resolve default property of object ContsCur.Cont(ContsCur.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        ContsCur.Cont(ContsCur.NumCont) = ContCur
                        ContsCur.NumCont = ContsCur.NumCont + 1
                        '+++++++++++++++++++++++++++++++++++++++++++++++

                        AddLM(False)
                        AddLM(True)
                        theToolEvents.ToolChangePrompt(Me, "", False)
                        'theToolEvents.ToolChangePrompt Me, "Select New Direction or Finish", False
                        theToolEvents.ToolChangePrompt(Me, TcLoadLangString(106), False)
                        GXMD.Release(WhichDrawing, False)
                        GXMD.Release(WhichView, False)
                        Exit Sub
                    End If

                End If


            End If
        Else
            Cancel = False
        End If
        '    Call Finish
        GXMD.Release(WhichDrawing, False)
        GXMD.Release(WhichView, False)

        GXMD.Collect()
        Exit Sub
ErrorHandler:
        If iConnectId1 <> -1 Or iConnectId2 <> -1 Then
            theToolEvents.ToolChangePrompt(Me, "", False)
            If (iConnectId1 <> -1) Then
                objApp.DisconnectEvents(iConnectId1)
                iConnectId1 = -1
            End If
            If (iConnectId2 <> -1) Then
                objApp.DisconnectEvents(iConnectId2)
                iConnectId1 = -1
            End If
            AddLM(False)
            Call Finish()
        End If
        '   MsgBox Err.Description
        GXMD.Release(WhichDrawing)
        GXMD.Release(WhichView)

        GXMD.Collect()
    End Sub

    Public Sub Cancel(ByRef DoItPlease As Boolean, ByRef CanCancel As Boolean)
        If (DoItPlease) Then
            Call Finish()
        End If

        CanCancel = True
    End Sub

    Public Sub UpdateUndo(ByRef AllowsUndo As Boolean)
        AllowsUndo = False
    End Sub

    Private Sub AddActToolLM(ByRef iTool As Integer, ByRef bAdd As Boolean)
        '    AddLM bAdd
    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        If (Not theToolEvents Is Nothing) Then
            'UPGRADE_NOTE: Object theToolEvents may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(theToolEvents)
        End If
        If (Not objApp Is Nothing) Then
            'UPGRADE_NOTE: Object objApp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(objApp)
            GXMD.Collect()
        End If

    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub



    Private Sub AddLM(ByRef bAdd As Boolean)

        Dim varCaptions As New Object
        Dim varPrompts As New Object
        Dim varEnabled As New Object
        Dim varChecked As New Object

        If (bAdd) Then

            Captions(0) = TcLoadLangString(114) '"One Step Back"
            Prompts(0, 0) = TcLoadLangString(114) '"One Step Back"
            Prompts(0, 1) = "95140"
            Enabled(0) = GrGroup.Graphics.Count > 0 ' True
            Checked(0) = False

            '        Captions(1) = "Define Last Point"
            '        Prompts(1) = "Define Last Point of the Chain"
            '        Enabled(1) = True  'LastPointEnable
            '        Checked(1) = LastPointChecked

            Captions(1) = TcLoadLangString(115) '"Finish"
            Prompts(1, 0) = TcLoadLangString(116) '"Finish Chain Polyline"
            Prompts(1, 1) = "95140"
            Enabled(1) = GrGroup.Graphics.Count > 0 'True
            Checked(1) = False


            Captions(2) = TcLoadLangString(117) '"Cancel"
            Prompts(2, 0) = TcLoadLangString(118) '"Cancel Chain Polyline"
            Prompts(2, 1) = "95140"
            Enabled(2) = True
            Checked(2) = False

            '            'UPGRADE_WARNING: Couldn't resolve default property of object varCaptions. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            '            varCaptions = VB6.CopyArray(Captions)
            '            'UPGRADE_WARNING: Couldn't resolve default property of object varPrompts. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            '            varPrompts = VB6.CopyArray(Prompts)
            '            'UPGRADE_WARNING: Couldn't resolve default property of object varEnabled. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            '            varEnabled = VB6.CopyArray(Enabled)
            '            'UPGRADE_WARNING: Couldn't resolve default property of object varChecked. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            '            varChecked = VB6.CopyArray(Checked)

            'UPGRADE_WARNING: Couldn't resolve default property of object varCaptions. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            varCaptions = Captions
            'UPGRADE_WARNING: Couldn't resolve default property of object varPrompts. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            varPrompts = Prompts
            'UPGRADE_WARNING: Couldn't resolve default property of object varEnabled. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            varEnabled = Enabled
            'UPGRADE_WARNING: Couldn't resolve default property of object varChecked. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            varChecked = Checked

            theToolEvents.ToolChangeProperties(gxMe, 1)
            theToolEvents.ToolChangeCommands(Me, 3, varCaptions, varPrompts, varEnabled, varChecked, True)
        Else
            theToolEvents.ToolChangeProperties(gxMe, 2)
            theToolEvents.ToolChangeCommands(Me, 0, varCaptions, varPrompts, varEnabled, varChecked, False)
        End If
    End Sub

    Public Sub DoLMCommand(ByRef CmdInd As Integer)

        'SPB A.A. 12.09.2002 - 11115 - begin
        Dim gxApp As IMSIGX.Application
        'UPGRADE_NOTE: Space was upgraded to Space_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Space_Renamed As IMSIGX.ImsiSpaceModeType

        gxApp = gxMe.Application
        Space_Renamed = gxApp.ActiveDrawing.ActiveView.SpaceMode
        'SPB A.A. 12.09.2002 - begin

        'MsgBox ("CmdInd=" & CStr(CmdInd))
        Dim GrCont As IMSIGX.Graphic
        Dim j, i, k As Integer
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As Contour
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As Segment
        Dim GrCount As Integer
        'UPGRADE_WARNING: Arrays in structure ContsTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContsTem As New Contours
        'UPGRADE_WARNING: Arrays in structure ContLast may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLast As Contour
        '###################################################
        If CmdInd = 0 Then 'One Step Back
            'MsgBox ("ContsCur.NumCont=" & CStr(ContsCur.NumCont))
            'MsgBox ("ContsRes.NumCont=" & CStr(ContsRes.NumCont))
            If GrSetDir Is Nothing = False Then
                For i = 0 To GrSetDir.Count - 1
                    GrSetDir.Item(i).Visible = False
                    GrSetDir.Item(i).Draw()
                    GrSetDir.Item(i).Delete()
                Next i
                GrSetDir.Clear(oMissing)
                GXMD.Collect()
            End If

            GrCount = GrGroup.Graphics.Count
            If GrCount > 0 Then
                GrCont = GrGroup.Graphics.Item(GrCount - 1)
                GrCont.Visible = False
                GrCont.Draw()
                GrCont.Delete()
                GXMD.Release(GrCont)
                GXMD.Collect()
                GrCount = GrCount - 1
            End If

            '++++++++++++++++++++++++++++++++++++++++++++++
            If GrCount = 0 Then
                NumClick = 0
                L0Dir = -100
                ReDim ContsCur.Cont(0)
                ContsCur.NumCont = 0
                ReDim ContsRes.Cont(0)
                ContsRes.NumCont = 0
                ReDim ContRes.Segm(0)
                ContRes.NumSegm = 0

                'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                GXMD.Release(GrCont)
                GXMD.Collect()
                If GrFirstDir Is Nothing = False Then
                    GrFirstDir.Visible = False
                    GrFirstDir.Draw()
                    GrFirstDir.Delete()
                    'UPGRADE_NOTE: Object GrFirstDir may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrFirstDir)
                    GXMD.Collect()
                End If
                If GrLastPoint Is Nothing = False Then
                    GrLastPoint.Visible = False
                    GrLastPoint.Draw()
                    GrLastPoint.Delete()
                    'UPGRADE_NOTE: Object GrLastPoint may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrLastPoint)
                    GXMD.Collect()
                End If
                LastPointClicked = False
                LastPointEnable = False
                LastPointChecked = False
                AddLM(False)
                AddLM(True)
                theToolEvents.ToolChangePrompt(Me, "", False)
                'theToolEvents.ToolChangePrompt Me, "Click Point on the Contour or Cancel", False
                theToolEvents.ToolChangePrompt(Me, TcLoadLangString(101), False)
                Exit Sub
            End If

            '+++++++++++++++++++++++++++++++++++++++++++
            If GrCount > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object ContsTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContsTem.CopyFrom(ContsRes)
                ReDim ContsRes.Cont(0)
                ContsRes.NumCont = 0
                For i = 0 To ContsTem.NumCont - 2
                    'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Cont = ContsTem.Cont(i)
                    ReDim Preserve ContsRes.Cont(i)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContsRes.Cont(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContsRes.Cont(i).CopyFrom(Cont)
                Next i
                ContsRes.NumCont = ContsTem.NumCont - 1
                ReDim ContRes.Segm(0)
                ContRes.NumSegm = 0

                'UPGRADE_WARNING: Couldn't resolve default property of object ContsTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContsTem.CopyFrom(ContsCur)
                ReDim ContsCur.Cont(0)
                ContsCur.NumCont = 0
                For i = 0 To ContsTem.NumCont - 2
                    'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Cont = ContsTem.Cont(i)
                    ReDim Preserve ContsCur.Cont(i)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContsCur.Cont(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContsCur.Cont(i).CopyFrom(Cont)
                Next i
                ContsCur.NumCont = ContsTem.NumCont - 1
                'UPGRADE_WARNING: Couldn't resolve default property of object ContCur. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContCur = ContsCur.Cont(ContsCur.NumCont - 1)
                GrContCur = Grs.GraphicFromID(ContCur.GrID)
                GrCurID = GrContCur.ID
                iCurSeg = 0
NEWMIDINTER:
                'UPGRADE_WARNING: Couldn't resolve default property of object ContLast. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContLast = ContsRes.Cont(ContsRes.NumCont - 1)
                xIntCur = ContLast.Segm(ContLast.NumSegm - 1).xyEnd.v0
                yIntCur = ContLast.Segm(ContLast.NumSegm - 1).xyEnd.v1

                Call FindIntersection(GrContCur, ContCur, iCurSeg, ILAST)

                Call CreateDirectionalVectors()

                'UPGRADE_WARNING: Couldn't resolve default property of object ContsTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContsTem = ContsRes
                ReDim ContsRes.Cont(0)
                ContsRes.NumCont = 0
                For i = 0 To ContsTem.NumCont - 2
                    'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Cont = ContsTem.Cont(i)
                    ReDim Preserve ContsRes.Cont(i)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContsRes.Cont(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContsRes.Cont(i) = Cont
                Next i
                ContsRes.NumCont = ContsTem.NumCont - 1

                GrCount = GrGroup.Graphics.Count
                If GrCount > 0 Then
                    GrCont = GrGroup.Graphics.Item(GrCount - 1)
                    GrCont.Visible = False
                    GrCont.Draw()
                    GrCont.Delete()
                    GXMD.Release(GrCont)
                    GXMD.Collect()
                    GrCount = GrCount - 1
                End If

            End If
            AddLM(False)
            AddLM(True)
            theToolEvents.ToolChangePrompt(Me, "", False)
            'theToolEvents.ToolChangePrompt Me, "Select New Direction or Finish", False
            theToolEvents.ToolChangePrompt(Me, TcLoadLangString(106), False)

            'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrCont)
            GXMD.Collect()
        End If

        '###################################################
        '    If CmdInd = 1 Then ' Define Last Point
        '        If GrLastPoint Is Nothing = False Then
        '            GrLastPoint.Visible = False
        '            GrLastPoint.Draw
        '            GrLastPoint.Deleted = True
        '            Set GXMD.Release(GrLastPoint)
        '        End If
        '        If LastPointChecked = False Then
        '            LastPointClicked = True
        '            LastPointChecked = True
        '            If L0Dir > 0 Then
        '                theToolEvents.ToolChangePrompt Me, "", False
        '                theToolEvents.ToolChangePrompt Me, "Click Last Point of the Contour", False
        '            End If
        '        Else
        '            LastPointClicked = False
        '            LastPointChecked = False
        '        End If
        '        AddLM False
        '        AddLM True
        '    End If
        '###################################################
        Dim GrOrig As IMSIGX.Graphic
        Dim GrType As String
        Dim ArcData(10) As Double
        Dim Ratio As Double
        Dim xP2, xP1, yP1, yP2 As Double
        Dim ContClosed As Boolean
        Dim xE, xB, yB, yE As Double
        Dim nV As Integer
        Dim CreateCurvBez As New TrimCurveBezier
        Dim SplToBez As New BiSplineToBezier
        Dim GrBez As IMSIGX.Graphic
        Dim URec As IMSIGX.UndoRecord
'        Dim GrDup As IMSIGX.Graphic
        'UPGRADE_NOTE: Dir was upgraded to Dir_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Dir_Renamed As Short
        If CmdInd = 1 Then ' Finish
            ReDim ContRes.Segm(0)
            k = 0
            For i = 0 To ContsRes.NumCont - 1
                'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Cont = ContsRes.Cont(i)
                For j = 0 To Cont.NumSegm - 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Segm = Cont.Segm(j)
                    ReDim Preserve ContRes.Segm(k)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContRes.Segm(k). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContRes.Segm(k) = Segm
                    k = k + 1
                Next j
            Next i
            ContRes.NumSegm = k

            '??????????????????????????????????????????????????
            '??????????????????????????????????????????????????
            '??????????????????????????????????????????????????

            ' If trimed single graphic
            If ContsRes.NumCont = 1 And GrFirstID = GrCurID Then
                GrOrig = Grs.GraphicFromID(GrFirstID)
                GrType = GrOrig.Type
                If GrType = "CIRCLE" Or GrType = "ARC" Then
                    GrOrig.GetArcData(ArcData)
                    Ratio = ArcData(6)
                    If System.Math.Abs(Ratio - 1) > Eps Then GrType = "Ellipse"
                End If ' If GrType = "CIRCLE" Or GrType = "ARC"

                If GrType = "TCW30CURVE" Then
                    If GrOrig.Properties.Item("$SPLINETYPE").Value = 1 Then GrType = "Bezier"
                    If GrOrig.Properties.Item("$SPLINETYPE").Value = 0 Then GrType = "BiSpline"
                End If ' If GrType = "TCW30CURVE"

                '+++++++++++++++++++++++++++++++++++++++++++++
                If GrType = "Ellipse" Then
                    GrCont = TrimEllipse(GrOrig)
                    If Not GrCont Is Nothing Then
                        GoTo ADDCHAIN
                    End If
                End If ' If GrType = "Ellipse"
                '+++++++++++++++++++++++++++++++++++++++++++++

                '+++++++++++++++++++++++++++++++++++++++++++++
                If GrType = "Bezier" Then 'Or GrType = "BiSpline" Then
                    xP1 = ContRes.Segm(0).xyBeg.v0
                    yP1 = ContRes.Segm(0).xyBeg.v1
                    xP2 = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                    yP2 = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
                    nV = GrOrig.Vertices.Count
                    xB = GrOrig.Vertices.Item(0).X
                    yB = GrOrig.Vertices.Item(0).Y
                    xE = GrOrig.Vertices.Item(nV - 1).X
                    yE = GrOrig.Vertices.Item(nV - 1).Y
                    ContClosed = False
                    If System.Math.Abs(xB - xE) < Eps And System.Math.Abs(yB - yE) < Eps Then
                        ContClosed = True
                    End If

                    Dir_Renamed = 1
                    If ContClosed = True Then
                        For i = 0 To ContRes.NumSegm - 2
                            xE = ContRes.Segm(i).xyEnd.v0
                            yE = ContRes.Segm(i).xyEnd.v1
                            If System.Math.Abs(xE - xB) < Eps And System.Math.Abs(yE - yB) < Eps Then
                                Dir_Renamed = -1
                                Exit For
                            End If
                        Next i
                    End If ' If ContClosed = True

                    If GrType = "Bezier" Then
                        GrCont = CreateCurvBez.TrimCurve(GrOrig, xP1, yP1, xP2, yP2, Dir_Renamed)
                    End If ' If GrType = "Bezier"

                    If GrType = "BiSpline" Then
                        GrBez = SplToBez.SplineToBezier(GrOrig)
                        'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        GXMD.Release(GrCont)
                        GXMD.Collect()
                        If Not GrBez Is Nothing Then
                            nV = GrBez.Vertices.Count
                            xB = GrBez.Vertices.Item(0).X
                            yB = GrBez.Vertices.Item(0).Y
                            xE = GrBez.Vertices.Item(nV - 1).X
                            yE = GrBez.Vertices.Item(nV - 1).Y
                            ContClosed = False
                            If System.Math.Abs(xB - xE) < Eps And System.Math.Abs(yB - yE) < Eps Then
                                ContClosed = True
                            End If

                            Dir_Renamed = 1
                            If ContClosed = True Then
                                For i = 0 To ContRes.NumSegm - 2
                                    xE = ContRes.Segm(i).xyEnd.v0
                                    yE = ContRes.Segm(i).xyEnd.v1
                                    If System.Math.Abs(xE - xB) < Eps And System.Math.Abs(yE - yB) < Eps Then
                                        Dir_Renamed = -1
                                        Exit For
                                    End If
                                Next i
                            End If ' If ContClosed = True

                            GrCont = CreateCurvBez.TrimCurve(GrBez, xP1, yP1, xP2, yP2, Dir_Renamed)
                            GrBez.Visible = False
                            GrBez.Draw()
                            Grs.Remove(GrBez.Index)
                            GrBez.Delete()
                            GXMD.Release(GrBez)
                            GXMD.Collect()
                        End If ' If Not GrBez Is Nothing
                    End If ' If GrType = "BiSpline"

                    If Not GrCont Is Nothing Then
                        GoTo ADDCHAIN
                    End If
                End If ' If GrType = "Bezier"
                '+++++++++++++++++++++++++++++++++++++++++++++

            End If 'If ContsRes.NumCont = 1 And GrFirstID = GrCurID
            '??????????????????????????????????????????????????
            '??????????????????????????????????????????????????
            '??????????????????????????????????????????????????

            Call ClearContFromParalSegments(ContRes)
            Call CreatePlaneGraphic(ContRes, GrCont)

ADDCHAIN:
            GrCont.Properties.Item("PenColor").Value = RGB(255, 0, 0)
            GrCont.Properties.Item("PenWidth").Value = 0.02
            GrCont.Properties.Item("PenStyle").Value = "CONTINUOUS"
            On Error Resume Next
            'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            GrCont.Transform(UCSGr0)


            Grs.AddGraphic(GrCont, oMissing, oMissing)
            GrCont.Layer = Layer1
            GrCont.Update()
            GrCont.Draw()

            URec = ActDr.AddUndoRecord(TcLoadLangString(128))
'            GrDup = GrCont.Duplicate
'            URec.AddGraphic(GrDup)
            URec.AddGraphic(GrCont)
'            Grs.Remove(GrCont.Index)
'            GrCont.Delete()
            URec.Close()    ' #3

            'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GrCont = DBNull.Value

            GXMD.Release(GrCont)
            GXMD.Collect()
            ReDim ContRes.Segm(0)
            ContRes.NumSegm = 0
            NumClick = 0
            Call Finish()

        End If

        If CmdInd = 2 Then ' Cancel
            Call Finish()

        End If

    End Sub
    Private Sub Finish()
        '    objApp.SnapModes = 1
        On Error Resume Next

        objApp.ActiveDrawing.UCS = ActDrUcs

        GXMD.Release(ActDrUcs)

        If SelWinOpen <> 1 Then
            objApp.ActiveDrawing.Properties.Item("SelectWindowOpen").Value = SelWinOpen
        End If


        If Not GxSet Is Nothing Then
            GxSet.Clear(oMissing)
            'UPGRADE_NOTE: Object GxSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GxSet)
            GXMD.Collect()
        End If

        'UPGRADE_NOTE: Space was upgraded to Space_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Space_Renamed As IMSIGX.ImsiSpaceModeType
        Space_Renamed = objApp.ActiveDrawing.ActiveView.SpaceMode
        If Space_Renamed = IMSIGX.ImsiSpaceModeType.imsiModelSpace Then
            If GridExist = True Then
                objApp.ActiveDrawing.Properties.Item("$GRIDSMS").Value = 1
            End If
        End If

        Dim i As Integer
        If GrGroup Is Nothing = False Then
            GrGroup.Visible = False
            GrGroup.Draw()
            GrGroup.Delete()
            'UPGRADE_NOTE: Object GrGroup may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrGroup)
            GXMD.Collect()
        End If
        If GrFirstDir Is Nothing = False Then
            GrFirstDir.Visible = False
            GrFirstDir.Draw()
            GrFirstDir.Delete()
            'UPGRADE_NOTE: Object GrFirstDir may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrFirstDir)
            GXMD.Collect()
        End If
        If GrLastPoint Is Nothing = False Then
            GrLastPoint.Visible = False
            GrLastPoint.Draw()
            GrLastPoint.Delete()
            'UPGRADE_NOTE: Object GrLastPoint may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrLastPoint)
            GXMD.Collect()
        End If

        If GrSetDir Is Nothing = False Then
            For i = 0 To GrSetDir.Count - 1
                GrSetDir.Item(i).Visible = False
                GrSetDir.Item(i).Draw()
                GrSetDir.Item(i).Delete()
            Next i
            GrSetDir.Clear(oMissing)
            'UPGRADE_NOTE: Object GrSetDir may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrSetDir)
            GXMD.Collect()
        End If

        Dim GrSeti As IMSIGX.GraphicSet
        If GrSets Is Nothing = False Then
            For i = 0 To GrSets.Count - 1
                GrSeti = GrSets.Item(i)
                GrSeti.Clear(oMissing)
                'UPGRADE_NOTE: Object GrSeti may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                GXMD.Release(GrSeti)
                GXMD.Collect()
            Next i
        End If


        If iConnectId1 <> -1 Then
            objApp.DisconnectEvents(iConnectId1) '????
            iConnectId1 = -1 '????
        End If

        If iConnectId2 <> -1 Then
            objApp.DisconnectEvents(iConnectId2) '????
            iConnectId2 = -1 '????
        End If

        On Error Resume Next
        theToolEvents.ToolChangePrompt(Me, "", False) '????
        AddLM(False)
        '    AddInspectorBar False

        'UPGRADE_NOTE: Object UCSGr0 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(UCSGr0)
        'UPGRADE_NOTE: Object GrSets may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrSets)
        'UPGRADE_NOTE: Object Layer1 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Layer1)
        'UPGRADE_NOTE: Object GrContCur may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrContCur)
        'UPGRADE_NOTE: Object Grs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Grs)
        'UPGRADE_NOTE: Object Vi may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Vi)
        'UPGRADE_NOTE: Object ActDr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'

        'If hTCCursor <> 0 Then
        If Not oCurCursor Is Nothing Then

            'Dim gxActView As IMSIGX.View

            'gxActView = ActDr.ActiveView

            'SetClassLongPtr(gxActView.HWND, GCLP_HCURSOR, hTCCursor)

            'hTCCursor = 0

            'GXMD.Release(gxActView)

            Cursor.Current = oCurCursor
            oCurCursor = Nothing

        End If

        GXMD.Release(ActDr)
        GXMD.Collect()

        objApp.ActiveDrawing.Properties.Item("PaperLinearUnitName3").Value = WidthUnit
        '   Set objApp = Nothing


        GXMD.Collect()
    End Sub


    Public Function MouseMove(ByRef oWhichDrawing As Object, ByRef oWhichView As Object, ByRef oWhichWindow As Object, ByRef Shift As Integer, ByRef x As Integer, ByRef y As Integer, ByRef Cancel As Boolean) As Object

        GXMD.Collect()

        Cancel = True

        Dim WhichDrawing As IMSIGX.Drawing
        Dim WhichView As IMSIGX.View

        WhichDrawing = oWhichDrawing
        WhichView = oWhichView

        'If hTCCursor = 0 Then

        'hTCCursor = GetClassLongPtr(WhichView.HWND, GCLP_HCURSOR)
        '            hTCCursor = Cursor.Current.Handle
        'SetClassLongPtr(WhichView.HWND, GCLP_HCURSOR, hToolCursor)

        'Else

        'If hTCCursor = GetClassLongPtr(WhichView.HWND, GCLP_HCURSOR) Then

        'SetClassLongPtr(WhichView.HWND, GCLP_HCURSOR, hToolCursor)

        'End If

        'End If

        If oCurCursor Is Nothing Then

            oCurCursor = Cursor.Current

            Cursor.Current = oToolCursor

        Else

            If (Cursor.Current.Handle <> oToolCursor.Handle) Then

                oCurCursor = Cursor.Current

                Cursor.Current = oToolCursor

            End If

        End If

        'UPGRADE_NOTE: Space was upgraded to Space_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Space_Renamed As IMSIGX.ImsiSpaceModeType
        Space_Renamed = WhichView.SpaceMode

        Dim GrChild As IMSIGX.Graphic
        Dim xClick, yClick As Double
        WhichView.ScreenToView(x, y, xClick, yClick)
        WhichView.ViewToWorld(xClick, yClick, 0, x0Dir, y0Dir, z0Dir)
        Dim ViH As Double
        ViH = WhichView.ViewHeight
        Dim SizePointer As Double
        SizePointer = ViH / 60
        Dim LPointer, WPointer As Double
        LPointer = SizePointer * 1.5
        WPointer = SizePointer / 2

        Dim sina, LDir, cosa As Double
        Dim xLoc, yLoc As Double
        Dim x1, y1 As Double


        Dim gxGrs As IMSIGX.Graphics
        Dim gxVrts As IMSIGX.Vertices
        Dim gxVrt As IMSIGX.Vertex

        If Space_Renamed = IMSIGX.ImsiSpaceModeType.imsiModelSpace Then

#If True Then
            'Dim Gr As New XGraphic
            Dim Gr As IMSIGX.Graphic
            Gr = MakeGraphic()

            gxVrts = Gr.Vertices
            gxVrts.Add(x0Dir, y0Dir, z0Dir)

            On Error Resume Next

            'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Not UCSGr0 Is Nothing Then
                Gr.Transform(UCSGr0)
            End If

            gxVrts.UseWorldCS = True
            gxVrt = gxVrts.Item(0)
            'UPGRADE_WARNING: Couldn't resolve default property of object x0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            x0DirW = gxVrt.X
            'UPGRADE_WARNING: Couldn't resolve default property of object y0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            y0DirW = gxVrt.Y
            'UPGRADE_WARNING: Couldn't resolve default property of object z0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            z0DirW = gxVrt.Z

            GXMD.Release(gxVrt)
            GXMD.Release(gxVrts)
            Gr.Delete()
            GXMD.Release(Gr)
            GXMD.Collect()
#Else
            x0DirW = x0Dir
            y0DirW = y0Dir
            z0DirW = z0Dir
#End If
        Else
            'UPGRADE_WARNING: Couldn't resolve default property of object x0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            x0DirW = x0Dir
            'UPGRADE_WARNING: Couldn't resolve default property of object y0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            y0DirW = y0Dir
            'UPGRADE_WARNING: Couldn't resolve default property of object z0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            z0DirW = z0Dir
        End If

        Dim del As Double
        Dim y1WOld, x1WOld, z1WOld As Double
        If NumClick = 1 Then

            '            theToolEvents.ToolChangePrompt(Me, "", False)
            'theToolEvents.ToolChangePrompt Me, "Click Directional Point", False
            '            theToolEvents.ToolChangePrompt(Me, TcLoadLangString(102), False)

            If GrFirstDir Is Nothing Then

                theToolEvents.ToolChangePrompt(Me, "", False)
                'theToolEvents.ToolChangePrompt Me, "Click Directional Point", False
                theToolEvents.ToolChangePrompt(Me, TcLoadLangString(102), False)

                'hTCCursor = GetClassLong(CInt(WhichView.HWND), GCL_HCURSOR)
                'SetClassLong(CInt(WhichView.HWND), GCL_HCURSOR, hToolCursor)

                GrFirstDir = Grs.Add(IMSIGX.ImsiGraphicType.imsiGroup)
#If True Then
                GrFirstDir.Layer = Layer1
#End If
                gxGrs = GrFirstDir.Graphics
                GrChild = gxGrs.Add(11)
                gxVrts = GrChild.Vertices

                gxVrts.UseWorldCS = True
                gxVrts.Add(x00W, y00W, z00W)
                gxVrts.Add(x0DirW, y0DirW, z0DirW)

#If True Then
                GrChild.Properties.Item("PenColor").Value = RGB(255, 0, 0)
                GrChild.Properties.Item("PenWidth").Value = 0
                GrChild.Properties.Item("PenStyle").Value = "CONTINUOUS"
                GrChild.Layer = Layer1
#End If
                GrChild.Draw()

                If Space_Renamed <> IMSIGX.ImsiSpaceModeType.imsiModelSpace Then
                    GrChild = GrFirstDir.Graphics.Add(11)
                    With GrChild.Vertices
                        .Add(x0Dir, y0Dir, 0)
                        .Add(x0Dir - LPointer, y0Dir + WPointer, 0)
                        .Add(x0Dir - LPointer, y0Dir - WPointer, 0)
                        .AddClose(oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
                    End With
                    GrChild.Properties.Item("PenColor").Value = RGB(255, 0, 0)
                    GrChild.Properties.Item("BrushStyle").Value = "Solid"
                    GrChild.Properties.Item("PenWidth").Value = 0
                    GrChild.Properties.Item("PenStyle").Value = "CONTINUOUS"
                    GrChild.Layer = Layer1

                End If

                GXMD.Release(gxVrts)
                GXMD.Release(gxGrs)
                GXMD.Release(GrChild)
                GXMD.Collect()

            Else

                GXMD.Release(GrChild)
                GXMD.Collect()

                gxGrs = GrFirstDir.Graphics
                GrChild = gxGrs.Item(0)

#If False Then
                With GrChild.Vertices
                    .UseWorldCS = True
                    x1WOld = .Item(1).X
                    y1WOld = .Item(1).Y
                    z1WOld = .Item(1).Z
                End With
#Else
                gxVrts = GrChild.Vertices
                gxVrts.UseWorldCS = True

                gxVrt = gxVrts.Item(1)
                x1WOld = gxVrt.X
                y1WOld = gxVrt.Y
                z1WOld = gxVrt.Z

                '                gxVrt = Nothing
                '                gxVrts = Nothing

                '                GC.Collect()


#End If

                '            L0Dir = Sqr((x0DirW - x00W) * (x0DirW - x00W) + (y0DirW - y00W) * (y0DirW - y00W) + (z0DirW - z00W) * (z0DirW - z00W))
                'UPGRADE_WARNING: Couldn't resolve default property of object z0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object y0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object x0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                del = System.Math.Sqrt((x0DirW - x1WOld) * (x0DirW - x1WOld) + (y0DirW - y1WOld) * (y0DirW - y1WOld) + (z0DirW - z1WOld) * (z0DirW - z1WOld))

                If del > ViH / 200 Then
                    GrFirstDir.Visible = False
                    GrFirstDir.Draw()
                    '                    GrChild = GrFirstDir.Graphics.Item(0)

#If False Then
                    With GrChild.Vertices
                        .UseWorldCS = True
                        'UPGRADE_WARNING: Couldn't resolve default property of object x0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        .Item(1).X = x0DirW
                        'UPGRADE_WARNING: Couldn't resolve default property of object y0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        .Item(1).Y = y0DirW
                        'UPGRADE_WARNING: Couldn't resolve default property of object z0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        .Item(1).Z = z0DirW
                    End With
#Else
                    '                    Dim gxVrts As IMSIGX.Vertices
                    '                    Dim gxVrt As IMSIGX.Vertex

                    '                    gxVrts = GrChild.Vertices
                    '                    gxVrts.UseWorldCS = True

                    '                    gxVrt = gxVrts.Item(1)
                    gxVrt.X = x0DirW
                    'UPGRADE_WARNING: Couldn't resolve default property of object y0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    gxVrt.Y = y0DirW
                    'UPGRADE_WARNING: Couldn't resolve default property of object z0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    gxVrt.Z = z0DirW

#End If
#If True Then
                    If Space_Renamed = IMSIGX.ImsiSpaceModeType.imsiModelSpace Then
                        '                    Set GrChild = GrFirstDir.Graphics.Item(1)
                    Else
                        sina = (y0Dir - y00) / L0Dir
                        cosa = (x0Dir - x00) / L0Dir
                        GrChild = GrFirstDir.Graphics.Item(1)
                        With GrChild.Vertices
                            .Item(0).X = x0Dir
                            .Item(0).Y = y0Dir
                            .Item(3).X = x0Dir
                            .Item(3).Y = y0Dir
                            xLoc = -LPointer
                            yLoc = WPointer
                            x1 = x0Dir + xLoc * cosa - yLoc * sina
                            y1 = y0Dir + xLoc * sina + yLoc * cosa
                            .Item(1).X = x1
                            .Item(1).Y = y1
                            xLoc = -LPointer
                            yLoc = -WPointer
                            x1 = x0Dir + xLoc * cosa - yLoc * sina
                            y1 = y0Dir + xLoc * sina + yLoc * cosa
                            .Item(2).X = x1
                            .Item(2).Y = y1
                        End With

                    End If

#End If
                    GXMD.Release(gxVrt)
                    GXMD.Release(gxVrts)
                    GXMD.Collect()

                    GrFirstDir.Visible = True
                    GrFirstDir.Draw()

                Else
                    GXMD.Release(gxVrt)
                    GXMD.Release(gxVrts)
                    GXMD.Collect()
                End If
            End If
        End If

        'Editing Directional vectors
        Dim i As Integer
        Dim GrDir As IMSIGX.Graphic
        Dim xInt, yInt As Double
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As Contour
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As Segment
        If GrSetDir Is Nothing = False And GrSetDir.Count > 0 And System.Math.Abs(ViH0 - ViH) / ViH0 > 0.2 Then
            ViH0 = ViH
            For i = 0 To GrSetDir.Count - 1
                GrDir = GrSetDir.Item(i)
                GrDir.Visible = False
                GrDir.Draw()
                GrDir.Delete()
            Next i
            GrSetDir.Clear(oMissing)
            GXMD.Collect()
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont = ContsRes.Cont(ContsRes.NumCont - 1)
            xInt = Cont.Segm(Cont.NumSegm - 1).xyEnd.v0
            yInt = Cont.Segm(Cont.NumSegm - 1).xyEnd.v1

            For i = 0 To ContsDir.NumCont - 1
                Call CreateDirection(ContsDir.Cont(i).Segm(0), xInt, yInt, GrDir)
                GrDir.Name = ContsDir.Cont(i).ConType
                GrSetDir.AddGraphic(GrDir, oMissing, oMissing)
            Next i
        End If

        GXMD.Release(WhichDrawing)
        GXMD.Release(WhichView)

        GXMD.Collect()

    End Function

    ' Calculate some Parameters for Resulting Contours
    ' Define the length and angle of line segment
    Private Sub LineSegmPar(ByRef x0 As Double, ByRef y0 As Double, ByRef x1 As Double, ByRef y1 As Double, ByRef l As Double, ByRef Alp As Double)
        Dim sina, cosa As Double
        l = System.Math.Sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0))
        If System.Math.Abs(l) < Eps Then
            l = 0
            Alp = 0
            Exit Sub
        End If
        sina = (y1 - y0) / l
        cosa = (x1 - x0) / l
        Alp = Angle(sina, cosa)
    End Sub

    Private Function Angle(ByRef sinB As Double, ByRef cosB As Double) As Double
        Dim Eps1 As Double
        Eps1 = 0.0000000000001
        If System.Math.Abs(cosB) < Eps1 Then
            If sinB > Eps1 Then
                Angle = Pi / 2
            Else
                Angle = 3 * Pi / 2
            End If
        Else
            If System.Math.Abs(sinB) < Eps1 Then
                If cosB > Eps1 Then
                    Angle = 0
                Else
                    Angle = Pi
                End If
            Else
                If sinB >= 0 And cosB > 0 Then
                    Angle = System.Math.Atan(sinB / cosB)
                Else
                    If sinB >= 0 And cosB < 0 Then
                        Angle = Pi + System.Math.Atan(sinB / cosB)
                    Else
                        If sinB < 0 And cosB < 0 Then
                            Angle = Pi + System.Math.Atan(sinB / cosB)
                        Else
                            If sinB < 0 And cosB > 0 Then
                                Angle = 2 * Pi + System.Math.Atan(sinB / cosB)
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Function


    Private Function Sign(ByRef y As Double) As Double
        If System.Math.Abs(y) < 0.000001 Then
            Sign = 0
            Exit Function
        End If
        If y < 0 Then
            Sign = -1.0#
            Exit Function
        End If
        If y > 0 Then
            Sign = 1.0#
            Exit Function
        End If
    End Function
    Private Function SysLine(ByRef a1 As Double, ByRef b1 As Double, ByRef d1 As Double, ByRef a2 As Double, ByRef b2 As Double, ByRef d2 As Double, ByRef xRes As Double, ByRef yRes As Double) As Boolean

        Dim delx, del, dely As Double
        On Error GoTo ErrorHandler

        del = a1 * b2 - a2 * b1
        delx = d1 * b2 - b1 * d2
        dely = a1 * d2 - a2 * d1
        If System.Math.Abs(del) < Eps / 100 Then
            SysLine = False
            Exit Function
        End If
        xRes = delx / del
        yRes = dely / del
        SysLine = True
        Exit Function
ErrorHandler:
        '    MsgBox Err.Description
        SysLine = False
    End Function
    'define begining and ending angle of arc
    Private Sub AnglesForArc(ByRef xB As Double, ByRef yB As Double, ByRef xE As Double, ByRef yE As Double, ByRef xc As Double, ByRef yc As Double, ByRef R As Double, ByRef Rot As Short, ByRef BetBeg As Double, ByRef BetEnd As Double)
        Dim cosa, sina, Alp As Double
        sina = (yB - yc) / R
        cosa = (xB - xc) / R
        Alp = Angle(sina, cosa)
        If Rot = 1 Then
            BetBeg = Pi / 2 + Alp
        Else
            BetBeg = 3 * Pi / 2 + Alp
        End If
        If BetBeg > 2 * Pi Then BetBeg = BetBeg - 2 * Pi

        sina = (yE - yc) / R
        cosa = (xE - xc) / R
        Alp = Angle(sina, cosa)
        If Rot = 1 Then
            BetEnd = Pi / 2 + Alp
        Else
            BetEnd = 3 * Pi / 2 + Alp
        End If
        If BetEnd > 2 * Pi Then BetEnd = BetEnd - 2 * Pi
    End Sub

    ' Define distinct of Arc
    Private Function ArcDictinct(ByRef xB As Double, ByRef yB As Double, ByRef xE As Double, ByRef yE As Double, ByRef xj As Double, ByRef yj As Double, ByRef Rot As Short) As Short
        Dim sina, cosa, l As Double
        Dim yLoc As Double
        l = System.Math.Sqrt((xE - xB) * (xE - xB) + (yE - yB) * (yE - yB))
        If System.Math.Abs(l) < Eps Then
            ArcDictinct = 1
            Exit Function
        End If
        sina = (yE - yB) / l
        cosa = (xE - xB) / l
        yLoc = (yj - yB) * cosa - (xj - xB) * sina
        If yLoc > 0 Then
            If Rot = -1 Then
                'If Rot <> 0 Then
                ArcDictinct = 1
            Else
                ArcDictinct = -1
            End If
        Else
            If Rot = -1 Then
                'If Rot <> 0 Then
                ArcDictinct = -1
            Else
                ArcDictinct = 1
            End If
        End If

    End Function
    ' Define the middle point of Base Arc
    ' from Beg-End Points and Rot
    ' Rot=1 - Counter Clockwise
    ' Rot=-1 - Clockwise
    Private Sub ArcMiddlePoint(ByRef xc As Double, ByRef yc As Double, ByRef xB As Double, ByRef yB As Double, ByRef xE As Double, ByRef yE As Double, ByRef R As Double, ByRef Rot As Short, ByRef xMid As Double, ByRef yMid As Double)
        If System.Math.Abs(R) < Eps Then
            xMid = xc
            yMid = yc
            Exit Sub
        End If

        Dim sina, cosa As Double
        Dim fiEnd, fiBeg, fiMid As Double
        sina = (yB - yc) / R
        cosa = (xB - xc) / R
        fiBeg = Angle(sina, cosa)

        sina = (yE - yc) / R
        cosa = (xE - xc) / R
        fiEnd = Angle(sina, cosa)

        If Rot = 1 Then ' Counter Clockwise
            If fiBeg > fiEnd Then fiEnd = fiEnd - 2 * Pi
        Else 'Clockwise
            If fiBeg < fiEnd Then fiEnd = fiEnd - 2 * Pi
        End If

        fiMid = (fiBeg + fiEnd) / 2
        xMid = xc + R * System.Math.Cos(fiMid)
        yMid = yc + R * System.Math.Sin(fiMid)


    End Sub

    ' Define The Nearest point on the contour and reenumetate it
    Private Sub ReEnumContour(ByRef Cont As Contour, ByRef x0 As Double, ByRef y0 As Double)

        Dim i, iNearest As Integer
        Dim xB, yB As Double
        Dim l, LMin As Double
        LMin = 10000000
        For i = 0 To Cont.NumSegm - 1
            If Cont.Segm(i).UpDown = 1 Then
                xB = Cont.Segm(i).xyBeg.v0
                yB = Cont.Segm(i).xyBeg.v1
                l = System.Math.Sqrt((xB - x0) ^ 2 + (yB - y0) ^ 2)
                If l < LMin Then
                    LMin = l
                    iNearest = i
                End If
            End If
        Next i

        'UPGRADE_WARNING: Arrays in structure ContTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContTem As New Contour
        'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContTem.CopyFrom(Cont)
        ReDim Cont.Segm(0)
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New Segment
        Dim j As Integer
        j = 0
        For i = iNearest To ContTem.NumSegm - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = ContTem.Segm(i)
            ReDim Preserve Cont.Segm(j)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(j). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(j) = Segm
            j = j + 1
        Next i
        For i = 0 To iNearest - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = ContTem.Segm(i)
            ReDim Preserve Cont.Segm(j)
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(j). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(j) = Segm
            j = j + 1
        Next i
        Cont.NumSegm = j

    End Sub

    ' Invert The Direction of the contour
    Private Sub InvertContour(ByRef Cont As Contour)
        Dim Type1 As String
        Dim fiEnd, l, xc, xMid, xE, xB, yB, yE, yMid, yc, fiBeg, R As Double
        Dim Rot, Dist As Short

        'UPGRADE_WARNING: Arrays in structure ContTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContTem As New Contour
        'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContTem.CopyFrom(Cont)
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New Segment
        'UPGRADE_WARNING: Arrays in structure SegmTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim SegmTem As Segment

        Dim i As Integer
        For i = ContTem.NumSegm - 1 To 0 Step -1
            'UPGRADE_WARNING: Couldn't resolve default property of object SegmTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SegmTem = ContTem.Segm(i)
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = SegmTem
            With Segm
                Type1 = .SegType
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                l = .l
                fiBeg = .Alp.v0
                fiEnd = .Alp.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                R = .R
                Rot = .Rot
                Dist = .Distinct
                If Type1 = "Line" Then fiEnd = fiBeg

                .xyBeg.v0 = xE
                .xyBeg.v1 = yE
                .xyEnd.v0 = xB
                .xyEnd.v1 = yB

                fiBeg = fiBeg + Pi
                If fiBeg > 2 * Pi Then fiBeg = fiBeg - 2 * Pi
                .Alp.v1 = fiBeg

                fiEnd = fiEnd + Pi
                If fiEnd > 2 * Pi Then fiEnd = fiEnd - 2 * Pi
                .Alp.v0 = fiEnd
                .Rot = -Rot

                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                l = .l
                fiBeg = .Alp.v0
                fiEnd = .Alp.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                R = .R
                Rot = .Rot
                Dist = ArcDictinct(xB, yB, xE, yE, xMid, yMid, Rot)
                Dist = .Distinct

            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(ContTem.NumSegm - 1 - i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont.Segm(ContTem.NumSegm - 1 - i) = Segm
        Next i

    End Sub

    Private Function FindIntersectionOld(ByRef GrCont As IMSIGX.Graphic, ByRef CurCont As Contour, ByRef iSeg As Integer, ByRef iLastCur As Integer) As Boolean
        'MsgBox ("FindIntersection")
        FindIntersectionOld = False
        'UPGRADE_WARNING: Arrays in structure Segmi may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segmi As Segment
        Dim SegType As String
        Dim Alp1, R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp2 As Double
        Dim Rot As Short
        Dim SegTypei As String
        Dim Alp1i, Ri, xMidi, xci, xEi, xBi, yBi, yEi, yci, yMidi, Li, Alp2i As Double
        Dim Roti As Short
        Dim Aper As Double
        Dim xIntNearest, yIntNearest As Double
        Dim GrTem As IMSIGX.Graphic
        Dim PRes As IMSIGX.PickResult
        Dim GrSets As IMSIGX.GraphicSets
        GrSets = ActDr.GraphicSets
        Dim GrSet As IMSIGX.GraphicSet
        GrSet = GrSets.Add("", True)
        Dim SetCount As Integer

        Dim Gr As IMSIGX.Graphic
        Dim GrType As String
        Dim Thick As Double

        Dim zj, xj, yj, Lj As Double
        Dim xjV, yjV As Double
        Dim xInt, yInt As Double
        Dim hGri As Integer
        Dim hMati As Integer
        Dim ci, ai, bi, di As Double

        Dim SegDiv As Short
        SegDiv = 10
        Dim i, j As Integer
        'UPGRADE_WARNING: Arrays in structure Conti may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Conti As New Contour
        Dim ViH As Double
        ViH = Vi.ViewHeight
        Dim LMin, LCur As Double
        Dim ChildCount, t As Integer
        Dim ChildOnlyDATA As Boolean
        Dim InitCont As New InitializeContour
        Dim IntDef As New DefineIntersections
        Dim Inter() As IntersTwoCont
        Dim InterExistCur As Boolean
        Dim jCur, k, kCur As Integer
        Dim sinE, fiELoc, xLoc, sina, cosa, yLoc, fiLoc, cosE As Double
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As Segment
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As New Contour
        'UPGRADE_WARNING: Arrays in structure Conts may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Conts As Contours
        For i = iSeg To CurCont.NumSegm - 1
            Aper = ViH / 50
            'UPGRADE_WARNING: Couldn't resolve default property of object Segmi. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segmi = CurCont.Segm(i)
            ReDim Conti.Segm(0)
            'UPGRADE_WARNING: Couldn't resolve default property of object Conti.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Conti.Segm(0) = Segmi
            Conti.NumSegm = 1
            With Segmi
                SegTypei = .SegType
                xBi = .xyBeg.v0
                yBi = .xyBeg.v1
                xEi = .xyEnd.v0
                yEi = .xyEnd.v1
                xci = .xyCent.v0
                yci = .xyCent.v1
                xMidi = .xyMid.v0
                yMidi = .xyMid.v1
                Li = .l
                Ri = .R
                Roti = .Rot
                Alp1i = .Alp.v0
                Alp2i = .Alp.v1
            End With
            '        ActDr.Application.SnapModes = 1
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If SegTypei = "Line" Then '# NLS#'
                LMin = 10000000
                SegDiv = Int(Li / Aper) + 1
                Aper = Li / SegDiv

                For Lj = Aper / 2.01 To Li + Aper / 2 Step Aper

                    xj = xBi + (xEi - xBi) * Lj / Li
                    yj = yBi + (yEi - yBi) * Lj / Li
                    '---------------------------
                    '                Set GrTem = Grs.Add(11)

                    'Set GrTem = New XGraphic
                    GrTem = MakeGraphic()

                    GrTem.Vertices.Add(xj, yj, 0)
                    On Error Resume Next
                    'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    GrTem.Transform(UCSGr0)
                    With GrTem.Vertices
                        .UseWorldCS = True
                        xj = .Item(0).X
                        yj = .Item(0).Y
                        zj = .Item(0).Z
                        .UseWorldCS = False
                    End With
                    GrTem.Delete()
                    'UPGRADE_NOTE: Object GrTem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrTem)
                    GXMD.Collect()
                    '---------------------------
                    'MsgBox ("Lj=" & CStr(Lj) & " xj=" & CStr(xj) & " yj=" & CStr(yj) & " zj=" & CStr(zj))
                    Vi.WorldToView(xj, yj, zj, xjV, yjV, 0)
                    'Set PRes = Vi.PickPoint(xjV, yjV, Aper)
                    PRes = Vi.PickRect(xjV - Aper, yjV - Aper, xjV + Aper, yjV + Aper, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
                    'MsgBox ("xj=" & CStr(xj) & " yj=" & CStr(yj) & "  PRes.Count=" & CStr(PRes.Count))

                    If PRes.Count > 1 Then
                        GrSet.Clear(oMissing)
                        For j = 0 To PRes.Count - 1
                            Gr = PRes.Item(j).Graphic
                            GrType = Gr.Type

                            If Gr.ID = GrCont.ID Then GoTo JLINE
                            If GrType = "TCW50Polyline" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                                With Gr.Graphics
                                    ChildCount = .Count
                                    For t = 0 To ChildCount - 1
                                        If .Item(t).ID = GrCont.ID Then GoTo JLINE
                                    Next t
                                End With
                            End If
                            '                        If GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC" Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Then
                            If GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                                '##########################################
                                If Gr.Graphics.Count > 0 Then
                                    For t = 0 To Gr.Graphics.Count - 1
                                        If Gr.Graphics.Item(t).Type = GRAPHICTYPE Then
                                            If Gr.Graphics.Item(t).Properties.Item("Info").Value = "Line_Width_Cosmetic" Then
                                                GoTo JLINE
                                            End If
                                        End If
                                    Next t
                                End If
                                '##########################################

                                If ActDr.Properties.Item("TileMode").Value = 1 Then
                                    Thick = 0
                                    On Error Resume Next
                                    Thick = Gr.Properties.Item("Thickness").Value
                                    If System.Math.Abs(Thick) > Eps Then GoTo JLINE
                                    Thick = -1000000002
                                    On Error Resume Next
                                    Thick = Gr.Properties.Item("Thickness").Value
                                    If Thick = -1000000002 Then GoTo JLINE
                                End If

                                '                            If (GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC") And Gr.Graphics.Count > 0 Then
                                If (GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE) And Gr.Graphics.Count > 0 Then
                                    ChildOnlyDATA = True
                                    With Gr.Graphics
                                        ChildCount = .Count
                                        For t = 0 To ChildCount - 1
                                            If .Item(t).Type <> "DATA" Then
                                                ChildOnlyDATA = False
                                                Exit For
                                            End If
                                        Next t
                                    End With
                                    If ChildOnlyDATA = False Then
                                        GoTo JLINE
                                    End If
                                End If
                                If Gr.Unbounded = True Then GoTo JLINE

                                If GrType = "TCW30CURVE" Then
                                    With Gr.Graphics
                                        ChildCount = .Count
                                        For t = 0 To ChildCount - 1
                                            If .Item(t).Type = "TCW50IntProp" Then
                                                GoTo JLINE
                                            End If
                                        Next t
                                    End With
                                End If
                                If Gr.UCS.IsEqual(UCSGr0) = False Then GoTo JLINE
                                '          hGri = TCWGraphicAt(hDr, Gr.Index)
                                '          hMati = GraphicGetMatrix(hGri)
                                '          MatrixGetPlane hMati, ai, bi, ci, di
                                UCSGetPlane(Gr.UCS, ai, bi, ci, di)
                                If System.Math.Abs(ai) < Eps And System.Math.Abs(bi) < Eps And System.Math.Abs(di) < Eps And System.Math.Abs(ci + 1) < Eps Then
                                    '                                Gr.UCS = ActDr.UCS
                                    '              hMati = GraphicGetMatrix(hGri)
                                    '              MatrixGetPlane hMati, ai, bi, ci, di
                                    UCSGetPlane(Gr.UCS, ai, bi, ci, di)
                                End If
                                If System.Math.Abs(ai - a0) > Eps Or System.Math.Abs(bi - b0) > Eps Or System.Math.Abs(ci - c0) > Eps Or System.Math.Abs(di - d0) > Eps Then
                                    GoTo JLINE
                                End If

                                On Error Resume Next
                                If Gr.Type <> "TCW60Wall" Then
                                    '                                Gr.UCS = UCSGr0
                                End If

                                GrSet.AddGraphic(Gr, oMissing, oMissing)

                            End If
JLINE:
                        Next j
                        SetCount = GrSet.Count

                        If SetCount > 0 Then
                            'MsgBox ("xj=" & CStr(xj) & " yj=" & CStr(yj) & " zj=" & CStr(zj))
                            ReDim Conts.Cont(SetCount - 1)
                            Conts.NumCont = SetCount
                            For j = 0 To SetCount - 1
                                Gr = GrSet.Item(j)
                                Call InitCont.RunInitialize(Gr, Cont, 0, 0)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Conts.Cont(j). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Conts.Cont(j) = Cont
                            Next j
                            'UPGRADE_NOTE: Object InitCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                            InitCont = Nothing
                            GXMD.Collect()
                            ReDim Inter(Conts.NumCont - 1)
                            InterExistCur = False
                            For j = 0 To Conts.NumCont - 1
                                Conts.Cont(j).Deleted = False
                                'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Cont = Conts.Cont(j)
                                'conti- current segment of current contour
                                If IntDef.RunIntersect(Conti, Cont, Inter(j)) = False Then
                                    Conts.Cont(j).Deleted = True
                                Else
                                    InterExistCur = True
                                End If
                            Next j
                            If InterExistCur = True Then

                                'jCur - Number of contour intersected current segment - i
                                'kCur - number of intersection
                                For j = 0 To Conts.NumCont - 1
                                    If Conts.Cont(j).Deleted = False Then
                                        For k = 0 To Inter(j).NumInt - 1
                                            xInt = Inter(j).xInt(k)
                                            yInt = Inter(j).yInt(k)
                                            LCur = System.Math.Sqrt((xInt - xBi) ^ 2 + (yInt - yBi) ^ 2)
                                            If LCur < LMin And LCur > Eps And LCur < Li + Eps Then
                                                LMin = LCur
                                                jCur = j
                                                kCur = k
                                                xIntNearest = xInt
                                                yIntNearest = yInt
                                            End If
                                        Next k
                                    End If
                                Next j
                            End If
                            'UPGRADE_NOTE: Object IntDef may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                            IntDef = Nothing
                            GXMD.Collect()
                        End If
                    End If
                Next Lj
            End If ' End if SegTypei="Line"
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If SegTypei = "Arc" Or SegTypei = "Circle" Then '# NLS#'
                LMin = 10000000
                sina = (yBi - yci) / Ri
                cosa = (xBi - xci) / Ri
                yLoc = (yEi - yci) * cosa - (xEi - xci) * sina
                xLoc = (yEi - yci) * sina + (xEi - xci) * cosa
                sinE = yLoc / Ri
                cosE = xLoc / Ri
                fiELoc = Angle(sinE, cosE)
                If Roti = -1 Then
                    fiELoc = 2 * Pi - fiELoc
                End If
                If SegTypei = "Circle" Then fiELoc = 2 * Pi '# NLS#'
                Li = fiELoc * Ri

                SegDiv = Int(Li / Aper) + 1
                Aper = Li / SegDiv
                For Lj = Aper / 2.01 To Li Step Aper
                    fiLoc = Roti * Lj / Ri
                    xLoc = Ri * System.Math.Cos(fiLoc)
                    yLoc = Ri * System.Math.Sin(fiLoc)
                    xj = xci + xLoc * cosa - yLoc * sina
                    yj = yci + xLoc * sina + yLoc * cosa
                    '---------------------------
                    '                Set GrTem = Grs.Add(11)

                    'Set GrTem = New XGraphic
                    GrTem = MakeGraphic()

                    GrTem.Vertices.Add(xj, yj, 0)
                    On Error Resume Next
                    'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    GrTem.Transform(UCSGr0)
                    With GrTem.Vertices
                        .UseWorldCS = True
                        xj = .Item(0).X
                        yj = .Item(0).Y
                        zj = .Item(0).Z
                        .UseWorldCS = False
                    End With
                    GrTem.Delete()
                    'UPGRADE_NOTE: Object GrTem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrTem)
                    GXMD.Collect()
                    '---------------------------
                    Vi.WorldToView(xj, yj, zj, xjV, yjV, 0)
                    'Set PRes = Vi.PickPoint(xjV, yjV, Aper)
                    PRes = Vi.PickRect(xjV - Aper, yjV - Aper, xjV + Aper, yjV + Aper, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
                    If PRes.Count > 1 Then
                        GrSet.Clear(oMissing)
                        For j = 0 To PRes.Count - 1
                            Gr = PRes.Item(j).Graphic
                            GrType = Gr.Type
                            '                        If GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC" Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Then
                            If GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                                '##########################################
                                If Gr.Graphics.Count > 0 Then
                                    For t = 0 To Gr.Graphics.Count - 1
                                        If Gr.Graphics.Item(t).Type = GRAPHICTYPE Then
                                            If Gr.Graphics.Item(t).Properties.Item("Info").Value = "Line_Width_Cosmetic" Then
                                                GoTo JARC
                                            End If
                                        End If
                                    Next t
                                End If
                                '##########################################

                                If ActDr.Properties.Item("TileMode").Value = 1 Then
                                    Thick = 0
                                    On Error Resume Next
                                    Thick = Gr.Properties.Item("Thickness").Value
                                    If System.Math.Abs(Thick) > Eps Then GoTo JARC
                                    Thick = -1000000002
                                    On Error Resume Next
                                    Thick = Gr.Properties.Item("Thickness").Value
                                    If Thick = -1000000002 Then GoTo JARC
                                End If

                                '                            If (GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC") And Gr.Graphics.Count > 0 Then
                                If (GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE) And Gr.Graphics.Count > 0 Then
                                    ChildOnlyDATA = True
                                    With Gr.Graphics
                                        ChildCount = .Count
                                        For t = 0 To ChildCount - 1
                                            If .Item(t).Type <> "DATA" Then
                                                ChildOnlyDATA = False
                                                Exit For
                                            End If
                                        Next t
                                    End With
                                    If ChildOnlyDATA = False Then
                                        GoTo JARC
                                    End If
                                End If
                                If Gr.Unbounded = True Then GoTo JARC

                                If GrType = "TCW30CURVE" Then
                                    With Gr.Graphics
                                        ChildCount = .Count
                                        For t = 0 To ChildCount - 1
                                            If .Item(t).Type = "TCW50IntProp" Then
                                                GoTo JARC
                                            End If
                                        Next t
                                    End With
                                End If

                                If Gr.ID = GrCont.ID Then GoTo JARC
                                If Gr.UCS.IsEqual(UCSGr0) = False Then GoTo JARC
                                '          hGri = TCWGraphicAt(hDr, Gr.Index)
                                '          hMati = GraphicGetMatrix(hGri)
                                '          MatrixGetPlane hMati, ai, bi, ci, di
                                UCSGetPlane(Gr.UCS, ai, bi, ci, di)
                                If System.Math.Abs(ai) < Eps And System.Math.Abs(bi) < Eps And System.Math.Abs(di) < Eps And System.Math.Abs(ci + 1) < Eps Then
                                    On Error Resume Next
                                    '                              Gr.UCS = ActDr.UCS
                                    '              hMati = GraphicGetMatrix(hGri)
                                    '              MatrixGetPlane hMati, ai, bi, ci, di
                                    UCSGetPlane(Gr.UCS, ai, bi, ci, di)
                                End If
                                If System.Math.Abs(ai - a0) > Eps Or System.Math.Abs(bi - b0) > Eps Or System.Math.Abs(ci - c0) > Eps Or System.Math.Abs(di - d0) > Eps Then
                                    GoTo JARC
                                End If
                                On Error Resume Next
                                '                          Gr.UCS = UCSGr0
                                GrSet.AddGraphic(Gr, oMissing, oMissing)
                            End If
JARC:
                        Next j
                        SetCount = GrSet.Count
                        If SetCount > 0 Then
                            ReDim Conts.Cont(SetCount - 1)
                            Conts.NumCont = SetCount
                            InitCont = New InitializeContour
                            For j = 0 To SetCount - 1
                                Gr = GrSet.Item(j)
                                Call InitCont.RunInitialize(Gr, Cont, 0, 0)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Conts.Cont(j). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Conts.Cont(j) = Cont
                            Next j
                            'UPGRADE_NOTE: Object InitCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                            InitCont = Nothing
                            GXMD.Collect()
                            IntDef = New DefineIntersections
                            ReDim Inter(Conts.NumCont - 1)
                            InterExistCur = False
                            For j = 0 To Conts.NumCont - 1
                                Conts.Cont(j).Deleted = False
                                'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Cont = Conts.Cont(j)
                                'conti- current segment of current contour
                                If IntDef.RunIntersect(Conti, Cont, Inter(j)) = False Then
                                    Conts.Cont(j).Deleted = True
                                Else
                                    InterExistCur = True
                                End If
                            Next j
                            If InterExistCur = True Then

                                'jCur - Number of contour intersected current segment - i
                                'kCur - number of intersection
                                For j = 0 To Conts.NumCont - 1
                                    If Conts.Cont(j).Deleted = False Then
                                        For k = 0 To Inter(j).NumInt - 1
                                            xInt = Inter(j).xInt(k)
                                            yInt = Inter(j).yInt(k)
                                            yLoc = (yInt - yci) * cosa - (xInt - xci) * sina
                                            xLoc = (yInt - yci) * sina + (xInt - xci) * cosa
                                            sinE = yLoc / Ri
                                            cosE = xLoc / Ri
                                            fiLoc = Angle(sinE, cosE)
                                            If Roti = -1 Then
                                                fiLoc = 2 * Pi - fiLoc
                                            End If
                                            LCur = fiLoc * Ri
                                            If LCur < LMin And LCur > Eps And LCur < Li + Eps Then
                                                LMin = LCur
                                                jCur = j
                                                kCur = k
                                                xIntNearest = xInt
                                                yIntNearest = yInt
                                            End If
                                        Next k
                                    End If
                                Next j
                            End If
                            'UPGRADE_NOTE: Object IntDef may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                            IntDef = Nothing
                            GXMD.Collect()
                        End If
                    End If
                Next Lj
            End If ' End if SegTypei="Arc" or "Circle"
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            InterExistCur = True
            If LMin > 10000000 - 1 Then InterExistCur = False
            If InterExistCur = False Then
                ReDim Preserve ContRes.Segm(ContRes.NumSegm)
                'UPGRADE_WARNING: Couldn't resolve default property of object ContRes.Segm(ContRes.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContRes.Segm(ContRes.NumSegm) = Segmi
                ContRes.NumSegm = ContRes.NumSegm + 1
                xE = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                yE = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
                If System.Math.Abs(xE - xBeg0) < Eps And System.Math.Abs(yE - yBeg0) < Eps Then
                    FindIntersectionOld = False
                    Exit For
                End If
            End If

            If InterExistCur = True Then
                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = Segmi
                With Segm
                    SegType = .SegType
                    xE = xIntNearest
                    yE = yIntNearest
                    .xyEnd.v0 = xE
                    .xyEnd.v1 = yE
                    If SegTypei = "Line" Then '# NLS#'
                        Call LineSegmPar(xBi, yBi, xE, yE, l, Alp1)
                        .l = l
                        .Alp.v0 = Alp1
                    End If
                    If SegTypei = "Arc" Or SegTypei = "Circle" Then '# NLS#'
                        .SegType = "Arc" '# NLS#'
                        Call ArcMiddlePoint(xci, yci, xBi, yBi, xE, yE, Ri, Roti, xMid, yMid)
                        .xyMid.v0 = xMid
                        .xyMid.v1 = yMid
                        .Distinct = ArcDictinct(xBi, yBi, xE, yE, xMid, yMid, Roti)
                        Call AnglesForArc(xBi, yBi, xE, yE, xci, yci, Ri, Roti, Alp1, Alp2)
                        .Alp.v0 = Alp1
                        .Alp.v1 = Alp2
                    End If
                End With
                ReDim Preserve ContRes.Segm(ContRes.NumSegm)
                'UPGRADE_WARNING: Couldn't resolve default property of object ContRes.Segm(ContRes.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContRes.Segm(ContRes.NumSegm) = Segm
                ContRes.NumSegm = ContRes.NumSegm + 1
                If System.Math.Abs(xE - xBeg0) < Eps And System.Math.Abs(yE - yBeg0) < Eps Then
                    FindIntersectionOld = False
                    Exit For
                End If
                iLastCur = i
                FindIntersectionOld = True
                Exit For
                '#################################################
            End If ' If Inter Exist
        Next i

        GrSet.Clear(oMissing)
        'UPGRADE_NOTE: Object GrSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrSet)
        'UPGRADE_NOTE: Object GrSets may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrSets)
        'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(PRes)
        'UPGRADE_NOTE: Object Gr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Gr)
        GXMD.Collect()
    End Function

    ' Create new Contour from pointIntersection With Direction
    ' and trim NonClosed contour
    'UPGRADE_NOTE: Dir was upgraded to Dir_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub CreateContFromPointToDirection(ByRef ContCur As Contour, ByRef xInt As Double, ByRef yInt As Double, ByRef Dir_Renamed As Short)
        Dim j, i, k As Integer
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New Segment
        Segm.Initialize()
        Dim SegType As String
        Dim Alp1, R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp2 As Double
        Dim Rot As Short
        Dim fi As Double
        fi = 0.7
        'UPGRADE_WARNING: Arrays in structure ContTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContTem As New Contour
        'UPGRADE_WARNING: Arrays in structure ContLine may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLine As New Contour
        ReDim ContLine.Segm(0)
        ContLine.NumSegm = 1
        With Segm
            .SegType = "Line" '# NLS#'
            xB = xInt + 100 * Eps * System.Math.Cos(fi)
            yB = yInt + 100 * Eps * System.Math.Sin(fi)
            xE = xInt - 100 * Eps * System.Math.Cos(fi)
            yE = yInt - 100 * Eps * System.Math.Sin(fi)
            Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
            .xyBeg.v0 = xB
            .xyBeg.v1 = yB
            .xyEnd.v0 = xE
            .xyEnd.v1 = yE
            .l = l
            .Alp.v0 = Alp1
        End With
        'UPGRADE_WARNING: Couldn't resolve default property of object ContLine.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContLine.Segm(0) = Segm

        If Dir_Renamed = -1 Then
            Call InvertContour(ContCur)
        End If

        Dim iLastCur As Integer

        Dim IntDef As DefineIntersections
        'UPGRADE_WARNING: Arrays in structure Inter may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter As New IntersTwoCont
        IntDef = New DefineIntersections
        If IntDef.RunIntersect(ContCur, ContLine, Inter) = False Then
            xB = xInt + 100 * Eps * System.Math.Cos(fi + Pi / 2)
            yB = yInt + 100 * Eps * System.Math.Sin(fi + Pi / 2)
            xE = xInt - 100 * Eps * System.Math.Cos(fi + Pi / 2)
            yE = yInt - 100 * Eps * System.Math.Sin(fi + Pi / 2)
            Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
            With ContLine.Segm(0)
                .xyBeg.v0 = xB
                .xyBeg.v1 = yB
                .xyEnd.v0 = xE
                .xyEnd.v1 = yE
                .l = l
                .Alp.v0 = Alp1
            End With
            Call IntDef.RunIntersect(ContCur, ContLine, Inter)
        End If
        'UPGRADE_NOTE: Object IntDef may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        IntDef = Nothing
        GXMD.Collect()
        iLastCur = Inter.IndInt1(0)

        'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Segm = ContCur.Segm(iLastCur)
        With Segm
            SegType = .SegType
            xE = .xyEnd.v0
            yE = .xyEnd.v1
        End With
        If System.Math.Abs(xInt - xE) < Eps And System.Math.Abs(yInt - yE) < Eps Then
            If ContCur.Closed = True And iLastCur = ContCur.NumSegm - 1 Then
                iCurSeg = 0
                GoTo INTERSECTION
            Else
                If ContCur.Closed = True Then
                    Call ReEnumContour(ContCur, xInt, yInt)
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContTem.CopyFrom(ContCur)
                    ReDim ContCur.Segm(0)
                    k = 0
                    For j = iLastCur + 1 To ContTem.NumSegm - 1
                        ReDim Preserve ContCur.Segm(k)
                        'UPGRADE_WARNING: Couldn't resolve default property of object ContCur.Segm(k). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        ContCur.Segm(k) = ContTem.Segm(j)
                        k = k + 1
                    Next j
                    ContCur.NumSegm = k
                End If
                iCurSeg = 0
                GoTo INTERSECTION
            End If
        End If
        If System.Math.Abs(xInt - xE) > Eps Or System.Math.Abs(yInt - yE) > Eps Then
            If ContCur.Closed = True Then

                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = ContCur.Segm(iLastCur)
                With Segm
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                End With
                Call ReEnumContour(ContCur, xB, yB)
                'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContTem = ContCur
                If System.Math.Abs(xInt - xB) < Eps And System.Math.Abs(yInt - yB) < Eps Then
                    iCurSeg = 0
                    GoTo INTERSECTION
                End If
                ReDim ContCur.Segm(0)
                k = 0
                ReDim Preserve ContCur.Segm(k)
                'UPGRADE_WARNING: Couldn't resolve default property of object ContCur.Segm(k). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContCur.Segm(k) = ContTem.Segm(0)
                k = k + 1
                For j = 0 To ContTem.NumSegm - 1
                    ReDim Preserve ContCur.Segm(k)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContCur.Segm(k). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContCur.Segm(k) = ContTem.Segm(j)
                    k = k + 1
                Next j
                ContCur.NumSegm = k
                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = ContCur.Segm(0)
                With Segm
                    SegType = .SegType
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1
                    xc = .xyCent.v0
                    yc = .xyCent.v1
                    xMid = .xyMid.v0
                    yMid = .xyMid.v1
                    l = .l
                    R = .R
                    Rot = .Rot
                    Alp1 = .Alp.v0
                    Alp2 = .Alp.v1
                End With
                If SegType = "Line" Then '# NLS#'
                    xE = xInt
                    yE = yInt
                    Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                    With Segm
                        .xyEnd.v0 = xE
                        .xyEnd.v1 = yE
                        .l = l
                        .Alp.v0 = Alp1
                    End With
                End If
                If SegType = "Arc" Or SegType = "Circle" Then '# NLS#'
                    xE = xInt
                    yE = yInt
                    Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, xMid, yMid)
                    Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, Alp1, Alp2)
                    With Segm
                        .xyEnd.v0 = xE
                        .xyEnd.v1 = yE
                        .xyMid.v0 = xMid
                        .xyMid.v1 = yMid
                        .Alp.v0 = Alp1
                        .Alp.v1 = Alp2
                    End With
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object ContCur.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContCur.Segm(0) = Segm

                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = ContCur.Segm(1)
                With Segm
                    SegType = .SegType
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1
                    xc = .xyCent.v0
                    yc = .xyCent.v1
                    xMid = .xyMid.v0
                    yMid = .xyMid.v1
                    l = .l
                    R = .R
                    Rot = .Rot
                    Alp1 = .Alp.v0
                    Alp2 = .Alp.v1
                End With
                If SegType = "Line" Then '# NLS#'
                    xB = xInt
                    yB = yInt
                    Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                    With Segm
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .l = l
                        .Alp.v0 = Alp1
                    End With
                End If
                If SegType = "Arc" Or SegType = "Circle" Then '# NLS#'
                    xB = xInt
                    yB = yInt
                    Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, xMid, yMid)
                    Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, Alp1, Alp2)
                    With Segm
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .xyMid.v0 = xMid
                        .xyMid.v1 = yMid
                        .Alp.v0 = Alp1
                        .Alp.v1 = Alp2
                    End With
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object ContCur.Segm(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContCur.Segm(1) = Segm
                Call ReEnumContour(ContCur, xInt, yInt)

            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContTem = ContCur
                ReDim ContCur.Segm(0)
                k = 0
                For j = iLastCur To ContTem.NumSegm - 1
                    ReDim Preserve ContCur.Segm(k)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContCur.Segm(k). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContCur.Segm(k) = ContTem.Segm(j)
                    k = k + 1
                Next j
                ContCur.NumSegm = k
                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = ContCur.Segm(0)
                With Segm
                    SegType = .SegType
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1
                    xc = .xyCent.v0
                    yc = .xyCent.v1
                    xMid = .xyMid.v0
                    yMid = .xyMid.v1
                    l = .l
                    R = .R
                    Rot = .Rot
                    Alp1 = .Alp.v0
                    Alp2 = .Alp.v1
                End With
                If SegType = "Line" Then '# NLS#'
                    xB = xInt
                    yB = yInt
                    Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                    With Segm
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .l = l
                        .Alp.v0 = Alp1
                    End With
                End If
                If SegType = "Arc" Or SegType = "Circle" Then '# NLS#'
                    xB = xInt
                    yB = yInt
                    Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, xMid, yMid)
                    Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, Alp1, Alp2)
                    With Segm
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .xyMid.v0 = xMid
                        .xyMid.v1 = yMid
                        .Alp.v0 = Alp1
                        .Alp.v1 = Alp2
                    End With
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object ContCur.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContCur.Segm(0) = Segm
            End If
            iCurSeg = 0
        End If
INTERSECTION:

        Dim xB0, yB0 As Double
        For i = 0 To ContCur.NumSegm - 1
            If ContCur.Segm(i).UpDown = 0 Then
                xB0 = ContCur.Segm(i).xyBeg.v0
                yB0 = ContCur.Segm(i).xyBeg.v1
                'UPGRADE_WARNING: Couldn't resolve default property of object ContTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContTem = ContCur
                ContCur.NumSegm = i
                For j = i + 1 To ContTem.NumSegm - 1
                    xB = ContTem.Segm(j).xyBeg.v0
                    yB = ContTem.Segm(j).xyBeg.v1
                    If System.Math.Abs(xB - xB0) < Eps And System.Math.Abs(yB - yB0) < Eps And ContTem.Segm(j).UpDown = 1 Then
                        For k = j To ContTem.NumSegm - 1
                            If ContTem.Segm(k).UpDown = 0 Then Exit Sub
                            ReDim Preserve ContCur.Segm(ContCur.NumSegm)
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContCur.Segm(ContCur.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContCur.Segm(ContCur.NumSegm) = ContTem.Segm(k)
                            ContCur.NumSegm = ContCur.NumSegm + 1
                        Next k
                    End If
                Next j
                Exit For
            End If 'If ContCur.Segm(i).UpDown = 0
        Next i
    End Sub


    ' Create the First Current contour
    Private Function CreateFirstCurContour() As Boolean
        Dim GrCont As IMSIGX.Graphic

        GrCont = Grs.GraphicFromID(GrCurID)

        Dim GrFirstType As String
        GrFirstType = GrCont.Type
        '    If GrFirstType = "CIRCLE" Then
        Dim ArcData(9) As Double
        Dim Ratio As Double
        If GrFirstType = CIRCLETYPE Then
            GrCont.GetArcData(ArcData)
            Ratio = ArcData(6)
            If System.Math.Abs(Ratio - 1) > Eps Then
                GrFirstType = "Ellipse" '# NLS#'
            End If
        End If
        '    If GrFirstType = "ARC" Then
        If GrFirstType = ARCTYPE Then
            GrCont.GetArcData(ArcData)
            Ratio = ArcData(6)
            If System.Math.Abs(Ratio - 1) > Eps Then
                GrFirstType = "Ellipse" '# NLS#'
            End If
        End If

        Dim InitCont As New InitializeContour
        InitCont.RunInitialize(GrCont, ContCur, 0, 0)
        '    ContCur.GrID = GrCont.id
        'UPGRADE_NOTE: Object InitCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        InitCont = Nothing
        GXMD.Collect()
        Dim IntDef As New DefineIntersections
        'UPGRADE_WARNING: Arrays in structure Inter may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter As New IntersTwoCont
        Dim fi As Double
        fi = 0.7
        Dim SegType As String
        Dim Alp1, R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp2 As Double
        Dim Rot As Short
        Dim dL As Double
        dL = 100 * Eps
DefInter:
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As New Contour
        'UPGRADE_WARNING: Arrays in structure ContLine may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLine As New Contour
        'UPGRADE_WARNING: Arrays in structure ContTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContTem As New Contour
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New Segment
        Segm.Initialize()
        ReDim ContLine.Segm(0)
        ContLine.NumSegm = 1
        With Segm
            .SegType = "Line" '# NLS#'
            xB = x00 + dL * System.Math.Cos(fi)
            yB = y00 + dL * System.Math.Sin(fi)
            xE = x00 - dL * System.Math.Cos(fi)
            yE = y00 - dL * System.Math.Sin(fi)
            Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
            .xyBeg.v0 = xB
            .xyBeg.v1 = yB
            .xyEnd.v0 = xE
            .xyEnd.v1 = yE
            .l = l
            .Alp.v0 = Alp1
        End With
        'UPGRADE_WARNING: Couldn't resolve default property of object ContLine.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContLine.Segm(0) = Segm

        If IntDef.RunIntersect(ContCur, ContLine, Inter) = False Then
            If fi = 0.7 Then
                fi = fi + Pi / 2
                GoTo DefInter
            Else
                fi = 0.7
                dL = dL * 10
                GoTo DefInter
            End If
        End If
        'UPGRADE_NOTE: Object IntDef may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        IntDef = Nothing
        GXMD.Collect()

        Dim ContClosed As Boolean
        ContClosed = False
        With ContCur
            xB = .Segm(0).xyBeg.v0
            yB = .Segm(0).xyBeg.v1
            xE = .Segm(.NumSegm - 1).xyEnd.v0
            yE = .Segm(.NumSegm - 1).xyEnd.v1
        End With
        If System.Math.Abs(xB - xE) < Eps And System.Math.Abs(yB - yE) < Eps Then
            ContClosed = True
        End If

        Dim iCurSeg As Integer
        iCurSeg = Inter.IndInt1(0)
        Dim xInt, yInt As Double
        xInt = Inter.xInt(0)
        yInt = Inter.yInt(0)


        'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Segm = ContCur.Segm(Inter.IndInt1(0))

        With Segm
            SegType = .SegType
            xB = .xyBeg.v0
            yB = .xyBeg.v1
            xE = .xyEnd.v0
            yE = .xyEnd.v1
            xc = .xyCent.v0
            yc = .xyCent.v1
            xMid = .xyMid.v0
            yMid = .xyMid.v1
            l = .l
            R = .R
            Rot = .Rot
            Alp1 = .Alp.v0
            Alp2 = .Alp.v1
        End With

        Dim Gr As IMSIGX.Graphic
        Dim xLocDir, xLoc0, sina, cosa, yLoc0, yLocDir As Double
        'UPGRADE_NOTE: Dir was upgraded to Dir_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Dir_Renamed As Short
        '    Set Gr = Grs.Add(11)
        '    With Gr.Vertices
        '        .UseWorldCS = True
        '        .Add x0DirW, y0DirW, z0DirW
        '    End With
        '    Gr.Transform UCSGr0
        '    With Gr.Vertices
        '        .UseWorldCS = False
        '        x0Dir = .Item(0).x
        '        y0Dir = .Item(0).y
        '        z0Dir = .Item(0).Z
        '    End With
        '    Grs.Remove Gr.Index
        '    Gr.Delete
        '
        '    If SegType = "Line" Then
        '        sina = Sin(Alp1)
        '        cosa = Cos(Alp1)
        '        yLoc0 = (y00 - yB) * cosa - (x00 - xB) * sina
        '        xLoc0 = (y00 - yB) * sina + (x00 - xB) * cosa
        '        yLocDir = (y0Dir - yB) * cosa - (x0Dir - xB) * sina
        '        xLocDir = (y0Dir - yB) * sina + (x0Dir - xB) * cosa
        '        If xLocDir > xLoc0 Then
        '            Dir = 1
        '        Else
        '            Dir = -1
        '        End If
        '    End If
        '    If SegType = "Arc" Or SegType = "Circle" Then
        '        Call AnglesForArc(x00, y00, xE, yE, xc, yc, R, Rot, Alp1, Alp2)
        '        sina = Sin(Alp1)
        '        cosa = Cos(Alp1)
        '        yLoc0 = (y00 - yB) * cosa - (x00 - xB) * sina
        '        xLoc0 = (y00 - yB) * sina + (x00 - xB) * cosa
        '        yLocDir = (y0Dir - yB) * cosa - (x0Dir - xB) * sina
        '        xLocDir = (y0Dir - yB) * sina + (x0Dir - xB) * cosa
        '        If xLocDir > xLoc0 Then
        '            Dir = 1
        '        Else
        '            Dir = -1
        '        End If
        '    End If

        Dim y1W, z1, x1, y1, x1W, z1W As Double
        If SegType = "Line" Then '# NLS#'
            x1 = x00 + System.Math.Cos(Alp1)
            y1 = y00 + System.Math.Sin(Alp1)
        Else
            Call AnglesForArc(x00, y00, xE, yE, xc, yc, R, Rot, Alp1, Alp2)
            x1 = x00 + System.Math.Cos(Alp1)
            y1 = y00 + System.Math.Sin(Alp1)
        End If

        '    Set Gr = Grs.Add(11)

        'Set Gr = New XGraphic
        Gr = MakeGraphic()

        Gr.Vertices.Add(x1, y1, 0)
        On Error Resume Next
        'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Gr.Transform(UCSGr0)
        With Gr.Vertices
            .UseWorldCS = True
            x1W = .Item(0).X
            y1W = .Item(0).Y
            z1W = .Item(0).Z
        End With
        'Grs.Remove(Gr.Index)
        Gr.Delete()
        GXMD.Release(Gr)
        GXMD.Collect()
        'UPGRADE_WARNING: Couldn't resolve default property of object z0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Couldn't resolve default property of object y0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Couldn't resolve default property of object x0DirW. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If (x0DirW - x00W) * (x1W - x00W) + (y0DirW - y00W) * (y1W - y00W) + (z0DirW - z00W) * (z1W - z00W) > 0 Then
            Dir_Renamed = 1
        Else
            Dir_Renamed = -1
        End If

        If Dir_Renamed = 1 Then
            If ContClosed = True Then
                ReEnumContour(ContCur, xB, yB)
            End If
        Else
            If ContClosed = False Then
                InvertContour(ContCur)
            End If
            If ContClosed = True Then
                ReEnumContour(ContCur, xE, yE)
                InvertContour(ContCur)
            End If
        End If


        IntDef = New DefineIntersections
        If IntDef.RunIntersect(ContCur, ContLine, Inter) = False Then
            xB = xInt + 100 * Eps * System.Math.Cos(fi + Pi / 2)
            yB = yInt + 100 * Eps * System.Math.Sin(fi + Pi / 2)
            xE = xInt - 100 * Eps * System.Math.Cos(fi + Pi / 2)
            yE = yInt - 100 * Eps * System.Math.Sin(fi + Pi / 2)
            Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
            With ContLine.Segm(0)
                .xyBeg.v0 = xB
                .xyBeg.v1 = yB
                .xyEnd.v0 = xE
                .xyEnd.v1 = yE
                .l = l
                .Alp.v0 = Alp1
            End With
            Call IntDef.RunIntersect(ContCur, ContLine, Inter)
        End If
        'UPGRADE_NOTE: Object IntDef may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        IntDef = Nothing
        GXMD.Collect()
        iCurSeg = Inter.IndInt1(0) ' Current Segment
        xBeg0 = ContCur.Segm(iCurSeg).xyBeg.v0
        yBeg0 = ContCur.Segm(iCurSeg).xyBeg.v1
        Dim InterExistCur As Boolean
        Dim iLastCur As Integer
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ' Find the first Previous intersection of the  Current segment with other Contours
        '    If GrFirstType <> "TCW30CURVE" And GrFirstType <> "Ellipse" And GrFirstType <> "CIRCLE" Then
        Dim i As Integer
        'UPGRADE_WARNING: Arrays in structure ContCurve may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContCurve As New Contour
        If GrFirstType <> "TCW30CURVE" And GrFirstType <> "Ellipse" And GrFirstType <> CIRCLETYPE Then '# NLS#'
            If System.Math.Abs(xBeg0 - x00) > Eps Or System.Math.Abs(yBeg0 - y00) > Eps Then
                ReDim Cont.Segm(0)
                Cont.Segm(0).Initialize()
                Cont.NumSegm = 1
                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = ContCur.Segm(iCurSeg)
                'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Cont.Segm(0) = Segm
                Call InvertContour(Cont)
                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = Cont.Segm(0)
                With Segm
                    SegType = .SegType
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1
                    xc = .xyCent.v0
                    yc = .xyCent.v1
                    xMid = .xyMid.v0
                    yMid = .xyMid.v1
                    l = .l
                    R = .R
                    Rot = .Rot
                    Alp1 = .Alp.v0
                    Alp2 = .Alp.v1
                End With
                xB = x00
                yB = y00
                If SegType = "Line" Then '# NLS#'
                    Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                    With Segm
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .l = l
                        .Alp.v0 = Alp1
                    End With
                End If
                If SegType = "Arc" Or SegType = "Circle" Then '# NLS#'
                    Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, xMid, yMid)
                    Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, Alp1, Alp2)
                    With Segm
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .xyMid.v0 = xMid
                        .xyMid.v1 = yMid
                        .Alp.v0 = Alp1
                        .Alp.v1 = Alp2
                    End With
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Cont.Segm(0) = Segm
                InterExistCur = False
                InterExistCur = FindIntersection(GrCont, Cont, 0, iLastCur)
                xInt = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                yInt = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
                ReDim ContRes.Segm(0)
                ContRes.Segm(0).Initialize()
                ContRes.NumSegm = 0
                If InterExistCur = True And (System.Math.Abs(xInt - xBeg0) > Eps Or System.Math.Abs(yInt - yBeg0) > Eps) Then
                    xBeg0 = xInt
                    yBeg0 = yInt
                    GrCont = Grs.GraphicFromID(ContCur.GrID)
                    Call CreateContFromPointToDirection(ContCur, xInt, yInt, 1)
                    iCurSeg = 0
                Else
                    xInt = xBeg0
                    yInt = yBeg0
                    GrCont = Grs.GraphicFromID(ContCur.GrID)
                    Call CreateContFromPointToDirection(ContCur, xInt, yInt, 1)
                    iCurSeg = 0
                End If
            End If

            InterExistCur = FindIntersection(GrCont, ContCur, iCurSeg, iLastCur)

            ILAST = iLastCur
            CreateFirstCurContour = InterExistCur
        Else
            ' if First contour - CURVE
            ReDim ContCurve.Segm(0)
            ContCurve.NumSegm = 0
            ' if first graphic is Curve
            If ContClosed = False Then
                For i = 0 To iCurSeg - 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Segm = ContCur.Segm(i)
                    ReDim Preserve ContCurve.Segm(ContCurve.NumSegm)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContCurve.Segm(ContCurve.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContCurve.Segm(ContCurve.NumSegm) = Segm
                    ContCurve.NumSegm = ContCurve.NumSegm + 1
                Next i
            Else
                '            If GrFirstType <> "CIRCLE" Then
                If GrFirstType <> CIRCLETYPE Then
                    For i = 1 To ContCur.NumSegm - 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Segm = ContCur.Segm(i)
                        ReDim Preserve ContCurve.Segm(ContCurve.NumSegm)
                        'UPGRADE_WARNING: Couldn't resolve default property of object ContCurve.Segm(ContCurve.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        ContCurve.Segm(ContCurve.NumSegm) = Segm
                        ContCurve.NumSegm = ContCurve.NumSegm + 1
                    Next i
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContCurve. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContCurve = ContCur
                    With ContCurve.Segm(0)
                        xc = .xyCent.v0
                        yc = .xyCent.v1
                        R = .R
                        .xyBeg.v0 = xInt
                        .xyBeg.v1 = yInt
                        .xyEnd.v0 = 2 * xc - xInt
                        .xyEnd.v1 = 2 * yc - yInt
                    End With
                    With ContCurve.Segm(1)
                        .xyBeg.v0 = 2 * xc - xInt
                        .xyBeg.v1 = 2 * yc - yInt
                        .xyEnd.v0 = xInt
                        .xyEnd.v1 = yInt
                    End With
                End If
            End If
            If ContCurve.NumSegm > 1 Then
                Call InvertContour(ContCurve)
                InterExistCur = False

                InterExistCur = FindIntersection(GrCont, ContCurve, 0, iLastCur)
                xInt = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                yInt = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
                ReDim ContRes.Segm(0)
                ContRes.NumSegm = 0
                If InterExistCur = True Then
                    xBeg0 = xInt
                    yBeg0 = yInt
                    GrCont = Grs.GraphicFromID(ContCur.GrID)
                    Call CreateContFromPointToDirection(ContCur, xInt, yInt, 1)
                    iCurSeg = 0
                    'MsgBox ("xInt=" & CStr(xInt) & "  yInt=" & CStr(yInt))
                Else
                    If ContClosed = False Then
                        xBeg0 = ContCur.Segm(0).xyBeg.v0
                        yBeg0 = ContCur.Segm(0).xyBeg.v1
                        GrCont = Grs.GraphicFromID(ContCur.GrID)
                        Call CreateContFromPointToDirection(ContCur, xInt, yInt, 1)
                        iCurSeg = 0
                    End If
                End If
            End If
            InterExistCur = FindIntersection(GrCont, ContCur, iCurSeg, iLastCur)
            ILAST = iLastCur
            CreateFirstCurContour = InterExistCur
        End If
    End Function

    '######################################################################
    '######################################################################
    '######################################################################

    Private Sub CreatePlaneGraphic(ByRef Cont As Contour, ByRef Gr As IMSIGX.Graphic)


        Dim j As Integer
        Dim xMid, xDir, xE, xB, xj, R, xc, yc, l, yj, yB, yE, yDir, yMid As Double
        Dim Rot As Short
        Dim cosa, sina, yLoc As Double
        Dim xLoc As Object
        Dim Ver As IMSIGX.Vertex
        If Cont.NumSegm = 1 And (Cont.Segm(0).SegType = "Circle" Or Cont.Segm(0).SegType = "Arc") Then '# NLS#'
            With Cont.Segm(0)
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                R = .R
                Rot = .Rot
            End With
            sina = (yB - yc) / R
            cosa = (xB - xc) / R
            'UPGRADE_WARNING: Couldn't resolve default property of object xLoc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            xLoc = 0
            yLoc = R * Rot
            'UPGRADE_WARNING: Couldn't resolve default property of object xLoc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            xDir = xc + xLoc * cosa - yLoc * sina
            'UPGRADE_WARNING: Couldn't resolve default property of object xLoc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            yDir = yc + xLoc * sina + yLoc * cosa
            If Cont.Segm(0).SegType = "Circle" Then '# NLS#'
                '                    Set Gr = Grs.AddCircleCenterAndPoint(xc, yc, 0, xc + R, yc, 0)
                Gr = GxSet.AddCircleCenterAndPoint(xc, yc, 0, xc + R, yc, 0)
                With Gr.Vertices
                    .Item(1).X = xB
                    .Item(1).Y = yB
                    .Item(2).X = xB
                    .Item(2).Y = yB
                    .Item(3).X = xDir
                    .Item(3).Y = yDir
                End With
            End If
            If Cont.Segm(0).SegType = "Arc" Then '# NLS#'
                '                    Set Gr = Grs.AddArcTriplePoint(xB, yB, 0, xMid, yMid, 0, xE, yE, 0)
                Gr = GxSet.AddArcTriplePoint(xB, yB, 0, xMid, yMid, 0, xE, yE, 0)
            End If
        Else
            '                Set Gr = Grs.Add(, "TCW50Polyline")
            Gr = GxSet.Add(oMissing, "TCW50Polyline", oMissing, oMissing, oMissing, oMissing) '# NLS#'
            For j = 0 To Cont.NumSegm - 1
                If j = 0 Then
                    xj = Cont.Segm(j).xyBeg.v0
                    yj = Cont.Segm(j).xyBeg.v1
                    Gr.Vertices.Add(xj, yj, 0)
                End If
                If Cont.Segm(j).SegType = "Line" Then '# NLS#'
                    xj = Cont.Segm(j).xyEnd.v0
                    yj = Cont.Segm(j).xyEnd.v1
                    Gr.Vertices.Add(xj, yj, 0)
                End If
                If Cont.Segm(j).SegType = "Arc" Then '# NLS#'
                    xB = Cont.Segm(j).xyBeg.v0
                    yB = Cont.Segm(j).xyBeg.v1
                    xE = Cont.Segm(j).xyEnd.v0
                    yE = Cont.Segm(j).xyEnd.v1
                    l = System.Math.Sqrt((xE - xB) ^ 2 + (yE - yB) ^ 2)
                    If l > 1000 * Eps Then
                        xj = Cont.Segm(j).xyCent.v0
                        yj = Cont.Segm(j).xyCent.v1
                        Ver = Gr.Vertices.Add(xj, yj, 0)
                        Ver.Bulge = True

                        xj = Cont.Segm(j).xyMid.v0
                        yj = Cont.Segm(j).xyMid.v1
                        Ver = Gr.Vertices.Add(xj, yj, 0)
                        Ver.Bulge = True
                    End If

                    xj = Cont.Segm(j).xyEnd.v0
                    yj = Cont.Segm(j).xyEnd.v1
                    Gr.Vertices.Add(xj, yj, 0)
                End If
            Next j
            Gr.Update()
            '                Gr.Draw Grs.Drawing.ActiveView
            '                Gr.Draw
        End If
        xB = Gr.Vertices.Item(0).X
        yB = Gr.Vertices.Item(0).Y
        xE = Gr.Vertices.Item(Gr.Vertices.Count - 1).X
        yE = Gr.Vertices.Item(Gr.Vertices.Count - 1).Y
        If System.Math.Abs(xB - xE) < 1 * Eps And System.Math.Abs(yB - yE) < 1 * Eps Then
            Gr.Vertices.Item(Gr.Vertices.Count - 1).X = xB
            Gr.Vertices.Item(Gr.Vertices.Count - 1).Y = yB
            Gr.Closed = True
        End If


        'UPGRADE_NOTE: Object Ver may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Ver)
        GXMD.Collect()

    End Sub


    ' Create Directional Vectors From Current noint Intersection
    Private Function CreateDirectionalVectors() As Boolean
        xIntCur = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
        yIntCur = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
        'MsgBox ("xIntCur=" & CStr(xIntCur) & "  yIntCur=" & CStr(yIntCur))
        Dim xInt, yInt As Double
        xInt = xIntCur
        yInt = yIntCur
        Dim yIntW, xIntW, zIntW As Double
        Dim hGri As Integer
        Dim hMati As Integer
        Dim ci, ai, bi, di As Double
        Dim i, j As Integer
        Dim xClV, yClV As Double
        Dim PRes As IMSIGX.PickResult
        Dim GrSet As IMSIGX.GraphicSet
        GrSet = GrSets.Add("", True)
        Dim GrCont As IMSIGX.Graphic
        GrCont = Grs.GraphicFromID(GrContCur.ID)
        Dim Gr As IMSIGX.Graphic
        Dim GrType As String
        Dim Thick As Double
        'UPGRADE_WARNING: Arrays in structure Conts may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Conts As Contours
        ReDim Conts.Cont(0)
        Conts.NumCont = 0
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As New Contour
        ReDim Cont.Segm(0)
        Cont.NumSegm = 0
        'UPGRADE_WARNING: Arrays in structure ContLine may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLine As New Contour
        ReDim ContLine.Segm(0)
        ContLine.NumSegm = 0
        'UPGRADE_WARNING: Arrays in structure ContTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContTem As New Contour
        ReDim ContTem.Segm(0)
        ContTem.NumSegm = 0
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New Segment
        Segm.Initialize()
        Dim fi As Double
        fi = 0.7
        Dim SegType As String
        Dim Alp1, R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp2 As Double
        Dim Rot As Short
        Dim InitCont As InitializeContour
        Dim IntDef As DefineIntersections
        'UPGRADE_WARNING: Arrays in structure Inter may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter As New IntersTwoCont
        Dim SetCount As Integer
        Dim GrDir As IMSIGX.Graphic
        Dim x_Dir, xDir, yDir, y_Dir As Double
        Dim GrTem As IMSIGX.Graphic
        xB = ContCur.Segm(0).xyBeg.v0
        yB = ContCur.Segm(0).xyBeg.v1

        xE = ContCur.Segm(ContCur.NumSegm - 1).xyEnd.v0
        yE = ContCur.Segm(ContCur.NumSegm - 1).xyEnd.v1

        If System.Math.Abs(xB - xE) < Eps And System.Math.Abs(yB - yE) < Eps Then
            ContCur.Closed = True
        Else
            ContCur.Closed = False
        End If

        '---------------------------
        '    Set GrTem = Grs.Add(11)

        'Set GrTem = New XGraphic
        GrTem = MakeGraphic()

        GrTem.Vertices.Add(xInt, yInt, 0)
        On Error Resume Next
        'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        GrTem.Transform(UCSGr0)
        With GrTem.Vertices
            .UseWorldCS = True
            xIntW = .Item(0).X
            yIntW = .Item(0).Y
            zIntW = .Item(0).Z
            .UseWorldCS = False
        End With
        GrTem.Delete()
        'UPGRADE_NOTE: Object GrTem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrTem)
        GXMD.Collect()
        '---------------------------

        Vi.WorldToView(xIntW, yIntW, zIntW, xClV, yClV, 0)
        'Set PRes = Vi.PickPoint(xClV, yClV) ',100*eps)
        Dim Aper As Double
        Aper = ActDr.ActiveView.ViewHeight / 40
        PRes = Vi.PickRect(xClV - Aper, yClV - Aper, xClV + Aper, yClV + Aper, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
        Dim ChildCount, t As Integer
        Dim ChildOnlyDATA As Boolean
        Dim xECur, yECur As Double
        Dim LDir, SizePointer As Double
        Dim Inter1() As IntersTwoCont
        Dim iPrev, iNext As Integer
        'UPGRADE_WARNING: Arrays in structure SegmNext may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim SegmNext As Segment
        'UPGRADE_WARNING: Arrays in structure SegmPrev may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim SegmPrev As Segment
        'UPGRADE_WARNING: Arrays in structure ContDir may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContDir As Contour
        If PRes.Count > 1 Then
            GrSet.Clear(oMissing)
            For i = 0 To PRes.Count - 1
                Gr = PRes.Item(i).Graphic
                GrType = Gr.Type
                '            If GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC" Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Then
                If GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                    If ActDr.Properties.Item("TileMode").Value = 1 Then
#If False Then
                        Thick = 0
                        On Error Resume Next
                        Thick = Gr.Properties.Item("Thickness").Value
                        If System.Math.Abs(Thick) > Eps Then GoTo III
                        Thick = -1000000002
                        On Error Resume Next
                        Thick = Gr.Properties.Item("Thickness").Value
                        If Thick = -1000000002 Then GoTo III
#Else
                        Dim gxProps As IMSIGX.Properties
                        Dim gxProp As IMSIGX.Property
                        gxProps = Gr.Properties
                        gxProp = Gr.Properties.Item("Thickness")
                        If (gxProp Is Nothing) Then

                            GXMD.Release(gxProps)
                            GXMD.Collect()
                            Thick = -1000000002
                            GoTo III

                        End If
                        Thick = gxProp.Value

                        GXMD.Release(gxProp)
                        GXMD.Release(gxProps)

                        GXMD.Collect()
                        If System.Math.Abs(Thick) > Eps Then GoTo III
#End If
                    End If

                    '                If (GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC") And Gr.Graphics.Count > 0 Then
                    If (GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE) And Gr.Graphics.Count > 0 Then
                        ChildOnlyDATA = True
                        With Gr.Graphics
                            ChildCount = .Count
                            For t = 0 To ChildCount - 1
                                If .Item(t).Type <> "DATA" Then
                                    ChildOnlyDATA = False
                                    Exit For
                                End If
                            Next t
                        End With
                        If ChildOnlyDATA = False Then
                            GoTo III
                        End If
                    End If

                    If GrType = "TCW30CURVE" Then
                        With Gr.Graphics
                            ChildCount = .Count
                            For t = 0 To ChildCount - 1
                                If .Item(t).Type = "TCW50IntProp" Then
                                    GoTo III
                                End If
                            Next t
                        End With
                    End If

                    If Gr.Unbounded = True Then GoTo III
                    If Gr.ID = GrContCur.ID Then GoTo III

                    If GrType = "TCW50Polyline" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                        With Gr.Graphics
                            ChildCount = .Count
                            For t = 0 To ChildCount - 1
                                If .Item(t).ID = GrContCur.ID Then GoTo III
                            Next t
                        End With
                    End If

                    '                hGri = TCWGraphicAt(hDr, Gr.Index)
                    '                hMati = GraphicGetMatrix(hGri)
                    '                MatrixGetPlane hMati, ai, bi, ci, di
                    UCSGetPlane(Gr.UCS, ai, bi, ci, di)
                    If System.Math.Abs(ai) < Eps And System.Math.Abs(bi) < Eps And System.Math.Abs(di) < Eps And System.Math.Abs(ci + 1) < Eps Then
                        On Error Resume Next
                        '                    Gr.UCS = ActDr.UCS
                        '                    hMati = GraphicGetMatrix(hGri)
                        '                    MatrixGetPlane hMati, ai, bi, ci, di
                        UCSGetPlane(Gr.UCS, ai, bi, ci, di)
                    End If
                    If System.Math.Abs(ai - a0) > Eps Or System.Math.Abs(bi - b0) > Eps Or System.Math.Abs(ci - c0) > Eps Or System.Math.Abs(di - d0) > Eps Then
                        GoTo III
                    End If

                    ReDim ContLine.Segm(0)
                    ContLine.NumSegm = 1
                    With Segm
                        .SegType = "Line" '# NLS#'
                        xB = xInt + 100 * Eps * System.Math.Cos(fi)
                        yB = yInt + 100 * Eps * System.Math.Sin(fi)
                        xE = xInt - 100 * Eps * System.Math.Cos(fi)
                        yE = yInt - 100 * Eps * System.Math.Sin(fi)
                        Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                        .xyBeg.v0 = xB
                        .xyBeg.v1 = yB
                        .xyEnd.v0 = xE
                        .xyEnd.v1 = yE
                        .l = l
                        .Alp.v0 = Alp1
                    End With
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContLine.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContLine.Segm(0) = Segm
                    InitCont = New InitializeContour
                    Call InitCont.RunInitialize(Gr, Cont, 0, 0)
                    'UPGRADE_NOTE: Object InitCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    InitCont = Nothing
                    GXMD.Collect()
                    IntDef = New DefineIntersections
                    If IntDef.RunIntersect(Cont, ContLine, Inter) = True Then
                        GrSet.AddGraphic(Gr, oMissing, oMissing)
                    Else
                        xB = xInt + 100 * Eps * System.Math.Cos(fi + Pi / 2)
                        yB = yInt + 100 * Eps * System.Math.Sin(fi + Pi / 2)
                        xE = xInt - 100 * Eps * System.Math.Cos(fi + Pi / 2)
                        yE = yInt - 100 * Eps * System.Math.Sin(fi + Pi / 2)
                        Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                        With ContLine.Segm(0)
                            .xyBeg.v0 = xB
                            .xyBeg.v1 = yB
                            .xyEnd.v0 = xE
                            .xyEnd.v1 = yE
                            .l = l
                            .Alp.v0 = Alp1
                        End With
                        If IntDef.RunIntersect(Cont, ContLine, Inter) = True Then
                            GrSet.AddGraphic(Gr, oMissing, oMissing)
                        End If
                    End If
                    'UPGRADE_NOTE: Object IntDef may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    IntDef = Nothing
                    GXMD.Collect()
                End If
III:
            Next i
            SetCount = GrSet.Count
            ReDim Conts.Cont(SetCount - 1)
            Conts.NumCont = SetCount
            InitCont = New InitializeContour
            For i = 0 To SetCount - 1
                Gr = GrSet.Item(i)
                Call InitCont.RunInitialize(Gr, Cont, 0, 0)
                '            Cont.GrID = Gr.id
                'UPGRADE_WARNING: Couldn't resolve default property of object Conts.Cont(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Conts.Cont(i) = Cont
            Next i ' For SetCount
            'UPGRADE_NOTE: Object InitCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            InitCont = Nothing
            GXMD.Collect()

            'UPGRADE_WARNING: Couldn't resolve default property of object ContsInt. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ContsInt = Conts
            '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            ' If Exist single intersection NonClosed current contour with
            ' nonClosed other contour at ended point of this contours
            If SetCount = 1 Then
                If ContCur.Closed = False And ILAST = ContCur.NumSegm - 1 Then
                    '++++++++++++++++++++++++++++++++++++++++++++++++++
                    'Delete previos created directions
                    If GrSetDir Is Nothing = False And GrSetDir.Count > 0 Then
                        For i = 0 To GrSetDir.Count - 1
                            GrDir = GrSetDir.Item(i)
                            GrDir.Visible = False
                            GrDir.Draw()
                            GrDir.Delete()
                        Next i
                        GrSetDir.Clear(oMissing)
                        GXMD.Collect()
                    End If
                    '++++++++++++++++++++++++++++++++++++++++++++++++++

                    If Cont.Closed = False Then
                        xB = Cont.Segm(0).xyBeg.v0
                        yB = Cont.Segm(0).xyBeg.v1
                        xE = Cont.Segm(Cont.NumSegm - 1).xyEnd.v0
                        yE = Cont.Segm(Cont.NumSegm - 1).xyEnd.v1
                        xECur = ContCur.Segm(ContCur.NumSegm - 1).xyEnd.v0
                        yECur = ContCur.Segm(ContCur.NumSegm - 1).xyEnd.v1

                        If System.Math.Abs(xInt - xB) < Eps And System.Math.Abs(yInt - yB) < Eps And System.Math.Abs(xInt - xECur) < Eps And System.Math.Abs(yInt - yECur) < Eps Then
                            iCurSeg = 0
                            GrContCur = Grs.GraphicFromID(Gr.ID)
                            GrCurID = GrContCur.ID
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContCur. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContCur = Cont
                            CreateDirectionalVectors = False
                            GoTo ENDEND
                        End If
                        If System.Math.Abs(xInt - xE) < Eps And System.Math.Abs(yInt - yE) < Eps And System.Math.Abs(xInt - xECur) < Eps And System.Math.Abs(yInt - yECur) < Eps Then
                            Call InvertContour(Cont)
                            iCurSeg = 0
                            GrContCur = Grs.GraphicFromID(Gr.ID)
                            GrCurID = GrContCur.ID
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContCur. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContCur = Cont
                            CreateDirectionalVectors = False
                            GoTo ENDEND
                        End If
                    End If 'If Cont.Closed = False
                End If ' If ContCur.Closed = False And iLast = ContCur.NumSegm - 1
            End If ' If SetCount = 1

            '#####################################################################
            '#####################################################################
            '#####################################################################

            If SetCount > 0 Then

                '++++++++++++++++++++++++++++++++++++++++++++++++++
                ' add created contour to Contouts collection
                ' and draw added contour as child graphig of graphic GrGroup
                ReDim Preserve ContsRes.Cont(ContsRes.NumCont)
                'UPGRADE_WARNING: Couldn't resolve default property of object ContsRes.Cont(ContsRes.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContsRes.Cont(ContsRes.NumCont) = ContRes
                ContsRes.NumCont = ContsRes.NumCont + 1
                Call CreatePlaneGraphic(ContRes, Gr)
                Gr.Properties.Item("PenColor").Value = RGB(0, 255, 0)
                Gr.Properties.Item("PenWidth").Value = 0.02
                Gr.Properties.Item("PenStyle").Value = "CONTINUOUS"
                '            Gr.Layer = Layer1

                On Error Resume Next
                'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Gr.Transform(UCSGr0)

                Grs.AddGraphic(Gr, oMissing, oMissing)
                Gr.Layer = Layer1
                'Gr.Draw

                Grs.Remove(Gr.Index)
                GrGroup.Graphics.AddGraphic(Gr, oMissing, oMissing)
                GrGroup.Draw()
                ReDim ContRes.Segm(0)
                ContRes.NumSegm = 0
                '++++++++++++++++++++++++++++++++++++++++++++++++++
                'Delete previos created directions
                If GrSetDir Is Nothing = False And GrSetDir.Count > 0 Then
                    For i = 0 To GrSetDir.Count - 1
                        GrDir = GrSetDir.Item(i)
                        GrDir.Visible = False

                        GrDir.Draw()
                        GrDir.Delete()
                    Next i
                    GrSetDir.Clear(oMissing)
                    GXMD.Collect()
                End If

                ViH0 = Vi.ViewHeight
                ReDim ContsDir.Cont(0)
                ContsDir.NumCont = 0
                '++++++++++++++++++++++++++++++++++++++++++++++++++
                LDir = Vi.ViewHeight / 10
                SizePointer = LDir / 15

                ' Begin Create Dir Vector For Current contour
                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = ContCur.Segm(ILAST)
                With Segm
                    SegType = .SegType
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1
                End With
                If ContCur.Closed = False And ILAST = ContCur.NumSegm - 1 And System.Math.Abs(xInt - xE) < Eps And System.Math.Abs(yInt - yE) < Eps Then
                    ' if ConCur notClosed end Pointintersection - last point of th contour
                    GoTo ENDCUR
                End If

                If System.Math.Abs(xInt - xE) < Eps And System.Math.Abs(yInt - yE) < Eps Then
                    ' if Pointintersection is the last point of cuurent segment of current contour
                    If ContCur.Closed = True And ILAST = ContCur.NumSegm - 1 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Segm = ContCur.Segm(0)
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Segm = ContCur.Segm(ILAST + 1)
                    End If
                End If

                Call CreateDirection(Segm, xInt, yInt, GrDir)
                GrDir.Name = "Current" '# NLS#'
                GrSetDir.AddGraphic(GrDir, oMissing, oMissing)

                ReDim Preserve ContsDir.Cont(ContsDir.NumCont)
                ReDim ContDir.Segm(0)
                ContDir.NumSegm = 1
                'UPGRADE_WARNING: Couldn't resolve default property of object ContDir.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContDir.Segm(0) = Segm
                ContDir.ConType = GrDir.Name
                'UPGRADE_WARNING: Couldn't resolve default property of object ContsDir.Cont(ContsDir.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContsDir.Cont(ContsDir.NumCont) = ContDir
                ContsDir.NumCont = ContsDir.NumCont + 1

ENDCUR:
                ' End Create Dir Vector For Current contour

                'Begin Create Dir Vectors for intersection contours
                IntDef = New DefineIntersections
                ReDim ContLine.Segm(0)
                ContLine.NumSegm = 1
                fi = 0.7
                With Segm
                    .SegType = "Line" '# NLS#'
                    xB = xInt + 100 * Eps * System.Math.Cos(fi)
                    yB = yInt + 100 * Eps * System.Math.Sin(fi)
                    xE = xInt - 100 * Eps * System.Math.Cos(fi)
                    yE = yInt - 100 * Eps * System.Math.Sin(fi)
                    Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                    .xyBeg.v0 = xB
                    .xyBeg.v1 = yB
                    .xyEnd.v0 = xE
                    .xyEnd.v1 = yE
                    .l = l
                    .Alp.v0 = Alp1
                End With
                'UPGRADE_WARNING: Couldn't resolve default property of object ContLine.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContLine.Segm(0) = Segm
                ReDim Inter1(Conts.NumCont - 1)
                For i = 0 To Conts.NumCont - 1
                    If IntDef.RunIntersect(Conts.Cont(i), ContLine, Inter1(i)) = False Then
                        xB = xInt + 100 * Eps * System.Math.Cos(fi + Pi / 2)
                        yB = yInt + 100 * Eps * System.Math.Sin(fi + Pi / 2)
                        xE = xInt - 100 * Eps * System.Math.Cos(fi + Pi / 2)
                        yE = yInt - 100 * Eps * System.Math.Sin(fi + Pi / 2)
                        Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                        With ContLine.Segm(0)
                            .xyBeg.v0 = xB
                            .xyBeg.v1 = yB
                            .xyEnd.v0 = xE
                            .xyEnd.v1 = yE
                            .l = l
                            .Alp.v0 = Alp1
                        End With
                        IntDef.RunIntersect(Conts.Cont(i), ContLine, Inter1(i))
                    End If
                Next i
                For i = 0 To Conts.NumCont - 1
                    xDir = xInt
                    yDir = yInt
                    x_Dir = xInt
                    y_Dir = yInt
                    'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Cont = Conts.Cont(i)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Segm = Cont.Segm(Inter1(i).IndInt1(0))
                    With Segm
                        SegType = .SegType
                        xB = .xyBeg.v0
                        yB = .xyBeg.v1
                        xE = .xyEnd.v0
                        yE = .xyEnd.v1
                        xc = .xyCent.v0
                        yc = .xyCent.v1
                        xMid = .xyMid.v0
                        yMid = .xyMid.v1
                        l = .l
                        R = .R
                        Rot = .Rot
                        Alp1 = .Alp.v0
                        Alp2 = .Alp.v1
                    End With

                    ' if point intersection cincident with first point of segment
                    If System.Math.Abs(xInt - xB) < Eps And System.Math.Abs(yInt - yB) < Eps Then
                        If Segm.UpDown <> 0 Then
                            Call CreateDirection(Segm, xInt, yInt, GrDir)
                            GrDir.Name = "Dir" & CStr(i) '# NLS#'
                            GrSetDir.AddGraphic(GrDir, oMissing, oMissing)

                            ReDim Preserve ContsDir.Cont(ContsDir.NumCont)
                            ReDim ContDir.Segm(0)
                            ContDir.NumSegm = 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContDir.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContDir.Segm(0) = Segm
                            ContDir.ConType = GrDir.Name
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContsDir.Cont(ContsDir.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContsDir.Cont(ContsDir.NumCont) = ContDir
                            ContsDir.NumCont = ContsDir.NumCont + 1
                        End If ' If Segm.UpDown <> 0 Then

                        iPrev = Inter1(i).IndInt1(0) - 1
                        If iPrev = -1 And Cont.Closed = True Then
                            iPrev = Cont.NumSegm - 1
                        End If
                        If iPrev <> -1 Then
                            If Cont.Segm(iPrev).UpDown <> 0 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object SegmPrev. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                SegmPrev = Cont.Segm(iPrev)
                                ReDim ContTem.Segm(0)
                                ContTem.NumSegm = 1
                                'UPGRADE_WARNING: Couldn't resolve default property of object ContTem.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ContTem.Segm(0) = SegmPrev
                                Call InvertContour(ContTem)
                                'UPGRADE_WARNING: Couldn't resolve default property of object SegmPrev. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                SegmPrev = ContTem.Segm(0)
                                Call CreateDirection(SegmPrev, xInt, yInt, GrDir)
                                GrDir.Name = "Dir-" & CStr(i) '# NLS#'
                                GrSetDir.AddGraphic(GrDir, oMissing, oMissing)

                                ReDim Preserve ContsDir.Cont(ContsDir.NumCont)
                                ReDim ContDir.Segm(0)
                                ContDir.NumSegm = 1
                                'UPGRADE_WARNING: Couldn't resolve default property of object ContDir.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ContDir.Segm(0) = Segm
                                ContDir.ConType = GrDir.Name
                                'UPGRADE_WARNING: Couldn't resolve default property of object ContsDir.Cont(ContsDir.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ContsDir.Cont(ContsDir.NumCont) = ContDir
                                ContsDir.NumCont = ContsDir.NumCont + 1
                            End If
                        End If
                        GoTo ENDOTHER
                    End If
                    ' if point intersection cincident with first point of segment
                    If System.Math.Abs(xInt - xE) < Eps And System.Math.Abs(yInt - yE) < Eps Then
                        ReDim ContTem.Segm(0)
                        ContTem.NumSegm = 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object ContTem.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        ContTem.Segm(0) = Segm
                        Call InvertContour(ContTem)
                        'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Segm = ContTem.Segm(0)
                        If Segm.UpDown <> 0 Then
                            Call CreateDirection(Segm, xInt, yInt, GrDir)
                            GrDir.Name = "Dir-" & CStr(i) '# NLS#'
                            GrSetDir.AddGraphic(GrDir, oMissing, oMissing)

                            ReDim Preserve ContsDir.Cont(ContsDir.NumCont)
                            ReDim ContDir.Segm(0)
                            ContDir.NumSegm = 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContDir.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContDir.Segm(0) = Segm
                            ContDir.ConType = GrDir.Name
                            'UPGRADE_WARNING: Couldn't resolve default property of object ContsDir.Cont(ContsDir.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            ContsDir.Cont(ContsDir.NumCont) = ContDir
                            ContsDir.NumCont = ContsDir.NumCont + 1
                        End If ' If Segm.UpDown <> 0

                        iNext = Inter1(i).IndInt1(0) + 1
                        If iNext = Cont.NumSegm And Cont.Closed = True Then
                            iNext = 0
                        End If
                        If iNext <> Cont.NumSegm Then
                            If Cont.Segm(iNext).UpDown <> 0 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object SegmNext. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                SegmNext = Cont.Segm(iNext)
                                Call CreateDirection(SegmNext, xInt, yInt, GrDir)
                                GrDir.Name = "Dir" & CStr(i) '# NLS#'
                                GrSetDir.AddGraphic(GrDir, oMissing, oMissing)

                                ReDim Preserve ContsDir.Cont(ContsDir.NumCont)
                                ReDim ContDir.Segm(0)
                                ContDir.NumSegm = 1
                                'UPGRADE_WARNING: Couldn't resolve default property of object ContDir.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ContDir.Segm(0) = Segm
                                ContDir.ConType = GrDir.Name
                                'UPGRADE_WARNING: Couldn't resolve default property of object ContsDir.Cont(ContsDir.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ContsDir.Cont(ContsDir.NumCont) = ContDir
                                ContsDir.NumCont = ContsDir.NumCont + 1
                            End If
                        End If ' If Cont.Segm(iNext).UpDown <> 0
                        GoTo ENDOTHER
                    End If

                    ' If Point Intersection lies within the segment
                    Call CreateDirection(Segm, xInt, yInt, GrDir)
                    GrDir.Name = "Dir" & CStr(i) '# NLS#'
                    GrSetDir.AddGraphic(GrDir, oMissing, oMissing)

                    ReDim Preserve ContsDir.Cont(ContsDir.NumCont)
                    ReDim ContDir.Segm(0)
                    ContDir.NumSegm = 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContDir.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContDir.Segm(0) = Segm
                    ContDir.ConType = GrDir.Name
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContsDir.Cont(ContsDir.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContsDir.Cont(ContsDir.NumCont) = ContDir
                    ContsDir.NumCont = ContsDir.NumCont + 1

                    ReDim ContTem.Segm(0)
                    ContTem.NumSegm = 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContTem.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContTem.Segm(0) = Segm
                    Call InvertContour(ContTem)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Segm = ContTem.Segm(0)
                    Call CreateDirection(Segm, xInt, yInt, GrDir)
                    GrDir.Name = "Dir-" & CStr(i) '# NLS#'
                    GrSetDir.AddGraphic(GrDir, oMissing, oMissing)

                    ReDim Preserve ContsDir.Cont(ContsDir.NumCont)
                    ReDim ContDir.Segm(0)
                    ContDir.NumSegm = 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContDir.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContDir.Segm(0) = Segm
                    ContDir.ConType = GrDir.Name
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContsDir.Cont(ContsDir.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContsDir.Cont(ContsDir.NumCont) = ContDir
                    ContsDir.NumCont = ContsDir.NumCont + 1
ENDOTHER:
                Next i

                'UPGRADE_NOTE: Object IntDef may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                IntDef = Nothing
                GXMD.Collect()
            End If ' If SetCount > 1
        End If ' If PRes.Count > 1
        CreateDirectionalVectors = True




        Dim StrLen As Short
        Dim iCont As Integer
        Dim Direct As Short
        Dim GrName As String
        'UPGRADE_NOTE: Char was upgraded to Char_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Char_Renamed As String
        If GrSetDir.Count = 1 Then
            GrName = GrSetDir.Item(0).Name
            iCont = 0
            Direct = 1
            StrLen = Len(GrName)
            Char_Renamed = Right(GrName, 1)
            iCont = CInt(Char_Renamed)
            Char_Renamed = Mid(GrName, 4, 1)
            If Char_Renamed = "-" Then Direct = -1
            'UPGRADE_WARNING: Couldn't resolve default property of object ContCur. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ContCur = Conts.Cont(iCont)
            GrContCur = Grs.GraphicFromID(Cont.GrID)
            GrCurID = Cont.GrID

            Call CreateContFromPointToDirection(ContCur, xIntCur, yIntCur, Direct)
            iCurSeg = 0
            CreateDirectionalVectors = False
        End If ' If GrSetDir.Count = 1



ENDEND:

        If GrSet Is Nothing = False Then
            GrSet.Clear(oMissing)
            'UPGRADE_NOTE: Object GrSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrSet)
        End If
        'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(PRes)
        'UPGRADE_NOTE: Object Gr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Gr)
        'UPGRADE_NOTE: Object GrDir may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrDir)
        'UPGRADE_NOTE: Object GrCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrCont)

        GXMD.Collect()
    End Function

    ' Create Directon Vector for current segment

    Private Sub CreateDirection(ByRef Segm As Segment, ByRef xInt As Double, ByRef yInt As Double, ByRef GrDir As IMSIGX.Graphic)

        Dim SegType As String
        Dim Alp1, R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp2 As Double
        Dim Rot As Short
        Dim xDir, yDir As Double
        Dim GrChild As IMSIGX.Graphic
        Dim Ver As IMSIGX.Vertex
        Dim sina, cosa As Double
        Dim LDir As Double
        LDir = Vi.ViewHeight / 15
        '    SizePointer = LDir / 10
        Dim ViH As Double
        ViH = ActDr.ActiveView.ViewHeight
        Dim SizePointer As Double
        SizePointer = ViH / 60
        Dim LPointer, WPointer As Double
        LPointer = SizePointer * 1.5
        WPointer = SizePointer / 2

        Dim LArc As Double
        Dim xLoc, yLoc As Double
        Dim sinE, cosE As Double
        Dim fiELoc As Double
        Dim x1, y1 As Double

        With Segm
            SegType = .SegType
            xB = .xyBeg.v0
            yB = .xyBeg.v1
            xE = .xyEnd.v0
            yE = .xyEnd.v1
            xc = .xyCent.v0
            yc = .xyCent.v1
            xMid = .xyMid.v0
            yMid = .xyMid.v1
            l = .l
            R = .R
            Rot = .Rot
            Alp1 = .Alp.v0
            Alp2 = .Alp.v1
        End With

        GrDir = Grs.Add(IMSIGX.ImsiGraphicType.imsiGroup)

        Dim GrLocalSet As IMSIGX.GraphicSet
        GrLocalSet = GrDir.Drawing.GraphicSets.Add("LocalSet", False) '# NLS#'

        GrDir.Layer = Layer1
        If SegType = "Line" Then '# NLS#'
            sina = System.Math.Sin(Alp1)
            cosa = System.Math.Cos(Alp1)
            xDir = xInt + LDir * cosa
            yDir = yInt + LDir * sina

            '        Set GrChild = GrDir.Graphics.Add(11)
            GrChild = GrLocalSet.Add(11, oMissing, oMissing, oMissing, oMissing, oMissing)

            With GrChild.Vertices
                .Add(xInt, yInt, 0)
                .Add(xDir, yDir, 0)
            End With

            GrChild.Properties.Item("PenColor").Value = RGB(0, 0, 255)
            GrChild.Properties.Item("PenWidth").Value = 0
            GrChild.Properties.Item("PenStyle").Value = "CONTINUOUS"
            ''''''        GrChild.Layer = Layer1
            '''''        On Error Resume Next
            '''''        GrChild.Transform UCSGr0
            '''''        GrDir.Graphics.AddGraphic GrChild
            '''''        GrChild.Layer = Layer1
            '''''
            ''''''        GrChild.Properties.Item("PenWidth") = 0
            '''''        GrChild.Draw
        End If

        If SegType = "Arc" Or SegType = "Circle" Then '# NLS#'
            LArc = 2 * Pi * R
            If LArc / 2 < LDir Then LDir = LArc / 2
            fiELoc = Rot * LDir / R
            ' Calculate new Last Point - xDir,yDir
            sina = (yInt - yc) / R
            cosa = (xInt - xc) / R
            xLoc = R * System.Math.Cos(fiELoc)
            yLoc = R * System.Math.Sin(fiELoc)
            xDir = xc + xLoc * cosa - yLoc * sina
            yDir = yc + xLoc * sina + yLoc * cosa
            Call ArcMiddlePoint(xc, yc, xInt, yInt, xDir, yDir, R, Rot, xMid, yMid)

            '        Set GrChild = GrDir.Graphics.Add(, "TCW50Polyline")
            GrChild = GrLocalSet.Add(oMissing, "TCW50Polyline", oMissing, oMissing, oMissing, oMissing) '# NLS#'

            With GrChild.Vertices
                .Add(xInt, yInt, 0)
                Ver = .Add(xc, yc, 0)
                Ver.Bulge = True
                Ver = .Add(xMid, yMid, 0)
                Ver.Bulge = True
                .Add(xDir, yDir, 0)
            End With
            GrChild.Properties.Item("PenColor").Value = RGB(0, 0, 255)
            GrChild.Properties.Item("PenWidth").Value = 0
            GrChild.Properties.Item("PenStyle").Value = "CONTINUOUS"

            ''''''''''''''''''''''        GrChild.Layer = Layer1
            ''''''''
            ''''''''''''''''''''''        GrChild.Update
            ''''''''
            ''''''''        On Error Resume Next
            ''''''''        GrChild.Transform UCSGr0
            ''''''''
            ''''''''        GrDir.Graphics.AddGraphic GrChild
            ''''''''        GrChild.Layer = Layer1
            ''''''''
            GrChild.Update()
            ''''''''
            ''''''''        GrChild.Draw
        End If

        'UPGRADE_WARNING: Arrays in structure ContDir may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContDir As New Contour
        Dim InitCont As New InitializeContour
        InitCont.RunInitialize(GrChild, ContDir, 0, 0)

        On Error Resume Next
        'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        GrChild.Transform(UCSGr0)
        GrDir.Graphics.AddGraphic(GrChild, oMissing, oMissing)
        GrChild.Layer = Layer1

        GrChild.Properties.Item("PenWidth").Value = 0.02
        GrChild.Draw()

        If ContDir.Segm(0).SegType = "Line" Then '# NLS#'
            Alp2 = ContDir.Segm(0).Alp.v0
        Else
            Alp2 = ContDir.Segm(0).Alp.v1
        End If
        'UPGRADE_NOTE: Object InitCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        InitCont = Nothing
        GXMD.Release(GrChild)
        GXMD.Collect()
        sina = System.Math.Sin(Alp2)
        cosa = System.Math.Cos(Alp2)

        '    Set GrChild = GrDir.Graphics.Add(11)
        GrChild = GrLocalSet.Add(11, oMissing, oMissing, oMissing, oMissing, oMissing)

        With GrChild.Vertices
            .Add(xDir, yDir, 0)
            xLoc = -LPointer
            yLoc = WPointer
            x1 = xDir + xLoc * cosa - yLoc * sina
            y1 = yDir + xLoc * sina + yLoc * cosa
            .Add(x1, y1, 0)
            xLoc = -LPointer
            yLoc = -WPointer
            x1 = xDir + xLoc * cosa - yLoc * sina
            y1 = yDir + xLoc * sina + yLoc * cosa
            .Add(x1, y1, 0)
            .AddClose(oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
        End With

        '    Set GrChild = GrDir.Graphics.AddCircleCenterAndPoint(xDir, yDir, 0, xDir + SizePointer, yDir, 0)
        GrChild.Properties.Item("PenColor").Value = RGB(0, 0, 255)
        GrChild.Properties.Item("BrushStyle").Value = "Solid"
        GrChild.Properties.Item("PenWidth").Value = 0
        GrChild.Properties.Item("PenStyle").Value = "CONTINUOUS"
        '    GrChild.Layer = Layer1
        On Error Resume Next
        '    GrChild.Transform UCSGr0
        '    GrChild.Draw
        'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        GrChild.Transform(UCSGr0)

        GrDir.Graphics.AddGraphic(GrChild, oMissing, oMissing)
        GrChild.Layer = Layer1

        GrChild.Draw()

        On Error Resume Next
        '    GrDir.Transform UCSGr0
        GrDir.Draw()

        If Not GrLocalSet Is Nothing Then
            GrLocalSet.Clear(oMissing)
            'UPGRADE_NOTE: Object GrLocalSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrLocalSet)
        End If

        'UPGRADE_NOTE: Object Ver may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Ver)
        'UPGRADE_NOTE: Object GrChild may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrChild)
        GXMD.Collect()
    End Sub

    ' Create First Direction For The First Segment
    Private Sub CreateFirstDirection()
        If GrFirstDir Is Nothing = False Then
            GrFirstDir.Visible = False
            GrFirstDir.Draw()
            GrFirstDir.Delete()
            GXMD.Release(GrFirstDir)
            GXMD.Collect()
        End If
        Dim ViH As Double
        ViH = ActDr.ActiveView.ViewHeight
        Dim SizePointer As Double
        SizePointer = ViH / 170
        Dim SegType As String
        Dim Alp1, R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp2 As Double
        Dim Rot As Short

        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As Segment
        'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Segm = ContRes.Segm(0)
        With Segm
            SegType = .SegType
            xB = .xyBeg.v0
            yB = .xyBeg.v1
            xE = .xyEnd.v0
            yE = .xyEnd.v1
            xc = .xyCent.v0
            yc = .xyCent.v1
            xMid = .xyMid.v0
            yMid = .xyMid.v1
            l = .l
            R = .R
            Rot = .Rot
            Alp1 = .Alp.v0
            Alp2 = .Alp.v1
        End With
        'UPGRADE_WARNING: Arrays in structure SegmNew may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim SegmNew As Segment
        'UPGRADE_WARNING: Couldn't resolve default property of object SegmNew. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        SegmNew = Segm
        Dim sina, cosa As Double
        Dim xLocDir, xLoc, yLoc, yLocDir As Double
        Dim yMidNew, yENew, yBNew, xBNew, xENew, xMidNew, RNew As Double
        Dim R0Dir, Dr As Double
        If SegType = "Line" Then '# NLS#'
            sina = System.Math.Sin(Alp1)
            cosa = System.Math.Cos(Alp1)
            xLocDir = (y0Dir - yB) * sina + (x0Dir - xB) * cosa
            yLocDir = (y0Dir - yB) * cosa - (x0Dir - xB) * sina
            If System.Math.Abs(yLocDir) > Eps Then
                yLoc = ViH / 50 * yLocDir / System.Math.Abs(yLocDir)
            Else
                yLoc = 0
            End If
            xLoc = 0
            xBNew = xB + xLoc * cosa - yLoc * sina
            yBNew = yB + xLoc * sina + yLoc * cosa
            xLoc = l
            xENew = xB + xLoc * cosa - yLoc * sina
            yENew = yB + xLoc * sina + yLoc * cosa
            With SegmNew
                .xyBeg.v0 = xBNew
                .xyBeg.v1 = yBNew
                .xyEnd.v0 = xENew
                .xyEnd.v1 = yENew
            End With
        End If
        If SegType = "Arc" Or SegType = "Circle" Then '# NLS#'
            R0Dir = System.Math.Sqrt((x0Dir - xc) ^ 2 + (y0Dir - yc) ^ 2)
            If R0Dir > Eps Then
                Dr = ViH / 50 * (R0Dir - R) / System.Math.Abs(R0Dir - R)
                RNew = R + Dr
            Else
                Dr = ViH / 50
                RNew = R + Dr
            End If
            xBNew = xc + (xB - xc) * RNew / R
            yBNew = yc + (yB - yc) * RNew / R
            xENew = xc + (xE - xc) * RNew / R
            yENew = yc + (yE - yc) * RNew / R
            xMidNew = xc + (xMid - xc) * RNew / R
            yMidNew = yc + (yMid - yc) * RNew / R
            With SegmNew
                .xyBeg.v0 = xBNew
                .xyBeg.v1 = yBNew
                .xyEnd.v0 = xENew
                .xyEnd.v1 = yENew
                .xyMid.v0 = xMidNew
                .xyMid.v1 = yMidNew
                .R = RNew
            End With
        End If

        CreateDirection(SegmNew, xBNew, yBNew, GrFirstDir)
        Dim i As Integer
        Dim GrChild As IMSIGX.Graphic
        Dim ChildCount As Integer
        ChildCount = GrFirstDir.Graphics.Count
        For i = 0 To ChildCount - 1
            GrChild = GrFirstDir.Graphics.Item(i)
            GrChild.Properties.Item("PenColor").Value = RGB(255, 0, 0)
            GrChild.Draw()
        Next i
        GXMD.Release(GrChild)
        GXMD.Collect()
        ''''''''''    Set GrChild = GrFirstDir.Graphics.AddCircleCenterAndPoint(xBeg0, yBeg0, 0, xBeg0 + SizePointer, yBeg0, 0)

        '    Set GrChild = GrFirstDir.Graphics.AddCircleCenterAndPoint(xB, yB, 0, xB + SizePointer, yB, 0)

        Dim GrCFDSet As IMSIGX.GraphicSet
        GrCFDSet = GrFirstDir.Drawing.GraphicSets.Add("CreateFirstDirection", False) '# NLS#'
        GrChild = GrCFDSet.AddCircleCenterAndPoint(xB, yB, 0, xB + SizePointer, yB, 0)


        GrChild.Properties.Item("PenColor").Value = RGB(255, 0, 0)
        GrChild.Properties.Item("BrushStyle").Value = "Solid"
        GrChild.Properties.Item("PenWidth").Value = 0
        GrChild.Properties.Item("PenStyle").Value = "CONTINUOUS"
        '    GrChild.Layer = Layer1
        On Error Resume Next
        'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        GrChild.Transform(UCSGr0)

        GrFirstDir.Graphics.AddGraphic(GrChild, oMissing, oMissing)
        GrChild.Layer = Layer1

        GrChild.Draw()

        If Not GrCFDSet Is Nothing Then
            GrCFDSet.Clear(oMissing)
            'UPGRADE_NOTE: Object GrCFDSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrCFDSet)
        End If

        'UPGRADE_NOTE: Object GrChild may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrChild)
        GXMD.Collect()
    End Sub


    ' define the Precision coordinates of the point Click
    ' if used the Intersection Snap
    Private Sub PrecisionPointClick(ByRef xUCS As Double, ByRef yUCS As Double, ByRef SnapMode As Integer)
        Dim i As Integer
        Dim Gr As IMSIGX.Graphic
        '    Set Gr = Grs.Add(11)

        'Set Gr = New XGraphic
        Gr = MakeGraphic()

        With Gr.Vertices
            .Add(xUCS, yUCS, 0)
        End With
        On Error Resume Next
        'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Gr.Transform(UCSGr0)
        Dim yW, xW, zW As Double
        With Gr.Vertices
            .UseWorldCS = True
            xW = .Item(0).X
            yW = .Item(0).Y
            zW = .Item(0).Z
            .UseWorldCS = False
        End With
        Gr.Delete()
        'UPGRADE_NOTE: Object Gr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Gr)
        GXMD.Collect()
        Dim yV, xV, zV As Double
        Vi.WorldToView(xW, yW, zW, xV, yV, zV)

        Dim GrSet As IMSIGX.GraphicSet
        Dim PRes As IMSIGX.PickResult
        Dim PicCount As Integer
        Dim GrType As String
        Dim Thick As Double
        Dim hGr As Integer
        Dim hMat As Integer
        Dim c, a, b, d As Double
        'Set PRes = Vi.PickPoint(xV, yV)
        Dim Aper As Double
        Aper = ActDr.ActiveView.ViewHeight / 40
        PRes = Vi.PickRect(xV - Aper, yV - Aper, xV + Aper, yV + Aper, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
        PicCount = PRes.Count
        GrSet = GrSets.Add("", True)
        Dim ChildCount, t As Integer
        Dim ChildOnlyDATA As Boolean
        For i = 0 To PicCount - 1
            Gr = PRes.Item(i).Graphic
            GrType = Gr.Type
            '        If GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC" Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Then
            If GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                If ActDr.Properties.Item("TileMode").Value = 1 Then
                    Thick = 0
                    On Error Resume Next
                    Thick = Gr.Properties.Item("Thickness").Value
                    If System.Math.Abs(Thick) > Eps Then GoTo II1
                    Thick = -1000000002
                    On Error Resume Next
                    Thick = Gr.Properties.Item("Thickness").Value
                    If Thick = -1000000002 Then GoTo II1
                End If


                '            If (GrType = "GRAPHIC" Or GrType = "CIRCLE" Or GrType = "ARC") And Gr.Graphics.Count > 0 Then
                If (GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE) And Gr.Graphics.Count > 0 Then
                    ChildOnlyDATA = True
                    With Gr.Graphics
                        ChildCount = .Count
                        For t = 0 To ChildCount - 1
                            If .Item(t).Type <> "DATA" Then
                                ChildOnlyDATA = False
                                Exit For
                            End If
                        Next t
                    End With
                    If ChildOnlyDATA = False Then
                        GoTo II1
                    End If

                End If

                If GrType = "TCW30CURVE" Then
                    With Gr.Graphics
                        ChildCount = .Count
                        For t = 0 To ChildCount - 1
                            If .Item(t).Type = "TCW50IntProp" Then
                                GoTo II1
                            End If
                        Next t
                    End With
                End If

                If Gr.Unbounded = True Then GoTo II1
                '----------------------------------------
                '           hGr = TCWGraphicAt(hDr, Gr.Index)
                '           hMat = GraphicGetMatrix(hGr)
                '           MatrixGetPlane hMat, a, b, c, d
                UCSGetPlane(Gr.UCS, a, b, c, d)
                If System.Math.Abs(a) < Eps And System.Math.Abs(b) < Eps And System.Math.Abs(d) < Eps And System.Math.Abs(c + 1) < Eps Then
                    On Error Resume Next
                    '                Gr.UCS = ActDr.UCS
                End If
                If System.Math.Abs(a - a0) > Eps Or System.Math.Abs(b - b0) > Eps Or System.Math.Abs(c - c0) > Eps Or System.Math.Abs(d - d0) > Eps Then
                    GoTo II1
                End If
                '----------------------------------------
                GrSet.AddGraphic(Gr, oMissing, oMissing)
            End If
II1:
        Next i
        Dim SetCount As Integer
        SetCount = GrSet.Count
        If SetCount = 0 Then
            GrSet.Clear(oMissing)
            'UPGRADE_NOTE: Object GrSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrSet)
            'UPGRADE_NOTE: Object Gr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(Gr)
            'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(PRes)
            GXMD.Collect()
            Exit Sub
        End If

        'UPGRADE_WARNING: Arrays in structure Conts may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Conts As Contours
        ReDim Conts.Cont(0)
        Conts.NumCont = 0
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As New Contour
        Dim InitCont As New InitializeContour

        For i = 0 To SetCount - 1
            Gr = GrSet.Item(i)
            InitCont.RunInitialize(Gr, Cont, 0, 0)
            ReDim Preserve Conts.Cont(Conts.NumCont)
            'UPGRADE_WARNING: Couldn't resolve default property of object Conts.Cont(Conts.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Conts.Cont(Conts.NumCont) = Cont
            Conts.NumCont = Conts.NumCont + 1
        Next i
        'UPGRADE_NOTE: Object InitCont may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        InitCont = Nothing
        GXMD.Collect()

        Dim DefInter As New DefineIntersections
        'UPGRADE_WARNING: Arrays in structure Inter may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter As New IntersTwoCont
        'UPGRADE_WARNING: Arrays in structure Conti may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Conti As Contour
        'UPGRADE_WARNING: Arrays in structure Contj may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Contj As Contour
        Dim j, k As Integer
        Dim dLMin, dL As Double
        Dim xInt, yInt As Double
        Dim xMin, yMin As Double
        dLMin = 10000000
        For i = 0 To Conts.NumCont - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object Conti. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Conti = Conts.Cont(i)
            For j = 0 To Conts.NumCont - 1
                If i <> j Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Contj. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Contj = Conts.Cont(j)
                    If DefInter.RunIntersect(Conti, Contj, Inter) = True Then
                        For k = 0 To Inter.NumInt - 1
                            xInt = Inter.xInt(k)
                            yInt = Inter.yInt(k)
                            dL = System.Math.Sqrt((xInt - xUCS) ^ 2 + (yInt - yUCS) ^ 2)
                            If dL < dLMin Then
                                dLMin = dL
                                xMin = xInt
                                yMin = yInt
                            End If
                        Next k
                    End If
                End If
            Next j
        Next i
        If dLMin < 9999 Then
            xUCS = xInt
            yUCS = yInt
        End If
        'UPGRADE_NOTE: Object DefInter may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        DefInter = Nothing
        GrSet.Clear(oMissing)
        'UPGRADE_NOTE: Object GrSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrSet)
        'UPGRADE_NOTE: Object Gr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Gr)
        'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(PRes)
        GXMD.Collect()

    End Sub



    Private Function TrimContByLastPoint() As Boolean
        TrimContByLastPoint = False
        Exit Function
        If GrLastPoint Is Nothing Then
            Exit Function
        End If
        If ContsRes.NumCont = 0 Then
            Exit Function
        End If
        Dim i As Integer
        Dim SegType As String = ""
        Dim R, xMid, xc, xE, xB, yB, yE, yc, yMid, l, Alp1, Alp2 As Double
        Dim Rot As Short
        Dim fi As Double
        fi = 0.7
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As New Contour
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New Segment
        'UPGRADE_WARNING: Arrays in structure ContLine0 may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLine0 As New Contour
        ReDim ContLine0.Segm(0)
        ContLine0.NumSegm = 1
        With Segm
            .SegType = "Line" '# NLS#'
            xB = xLast + 100 * Eps * System.Math.Cos(fi)
            yB = yLast + 100 * Eps * System.Math.Sin(fi)
            xE = xLast - 100 * Eps * System.Math.Cos(fi)
            yE = yLast - 100 * Eps * System.Math.Sin(fi)
            Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
            .xyBeg.v0 = xB
            .xyBeg.v1 = yB
            .xyEnd.v0 = xE
            .xyEnd.v1 = yE
            .l = l
            .Alp.v0 = Alp1
        End With
        'UPGRADE_WARNING: Couldn't resolve default property of object ContLine0.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContLine0.Segm(0) = Segm

        'UPGRADE_WARNING: Arrays in structure ContLine1 may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLine1 As New Contour
        ReDim ContLine1.Segm(0)
        ContLine1.NumSegm = 1
        With Segm
            .SegType = "Line" '# NLS#'
            xB = xLast + 100 * Eps * System.Math.Cos(fi + Pi / 2)
            yB = yLast + 100 * Eps * System.Math.Sin(fi + Pi / 2)
            xE = xLast - 100 * Eps * System.Math.Cos(fi + Pi / 2)
            yE = yLast - 100 * Eps * System.Math.Sin(fi + Pi / 2)
            Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
            .xyBeg.v0 = xB
            .xyBeg.v1 = yB
            .xyEnd.v0 = xE
            .xyEnd.v1 = yE
            .l = l
            .Alp.v0 = Alp1
        End With
        'UPGRADE_WARNING: Couldn't resolve default property of object ContLine1.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ContLine1.Segm(0) = Segm

        Dim IntExist0 As Boolean
        IntExist0 = False
        Dim IntExist1 As Boolean
        IntExist1 = False
        Dim DefInter As New DefineIntersections
        'UPGRADE_WARNING: Arrays in structure Inter0 may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter0 As New IntersTwoCont
        'UPGRADE_WARNING: Arrays in structure Inter1 may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter1 As New IntersTwoCont
        'UPGRADE_WARNING: Arrays in structure Inter may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter As New IntersTwoCont
        Dim iInt As Integer
        'UPGRADE_WARNING: Arrays in structure ContsTem may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContsTem As New Contours
        Dim j As Integer
        'UPGRADE_WARNING: Arrays in structure Contj may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Contj As New Contour

        For i = 0 To ContsRes.NumCont - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont = ContsRes.Cont(i)
            IntExist0 = False
            IntExist1 = False
            IntExist0 = DefInter.RunIntersect(Cont, ContLine0, Inter0)
            IntExist1 = DefInter.RunIntersect(Cont, ContLine1, Inter1)
            If IntExist0 = True Or IntExist1 = True Then
                'UPGRADE_WARNING: Couldn't resolve default property of object ContsTem. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContsTem = ContsRes
                If IntExist0 = True Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Inter. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Inter = Inter0
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Inter. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Inter = Inter1
                End If
                iInt = Inter.IndInt1(0)
                ReDim ContsRes.Cont(0)
                ContsRes.NumCont = 0
                For j = 0 To i - 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object Contj. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Contj = ContsTem.Cont(j)
                    ReDim Preserve ContsRes.Cont(ContsRes.NumCont)
                    'UPGRADE_WARNING: Couldn't resolve default property of object ContsRes.Cont(ContsRes.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ContsRes.Cont(ContsRes.NumCont) = Contj
                    ContsRes.NumCont = ContsRes.NumCont + 1
                Next j
                ReDim Contj.Segm(0)
                Contj.NumSegm = 0
                For j = 0 To iInt
                    'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Segm = Cont.Segm(j)
                    ReDim Preserve Contj.Segm(Contj.NumSegm)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Contj.Segm(Contj.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Contj.Segm(Contj.NumSegm) = Segm
                    Contj.NumSegm = Contj.NumSegm + 1
                Next j
                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = Contj.Segm(Contj.NumSegm - 1)
                With Segm
                    SegType = .SegType
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1
                    xc = .xyCent.v0
                    yc = .xyCent.v1
                    xMid = .xyMid.v0
                    yMid = .xyMid.v1
                    l = .l
                    R = .R
                    Rot = .Rot
                    Alp1 = .Alp.v0
                    Alp2 = .Alp.v1
                End With
                xE = xLast
                yE = yLast
                If SegType = "Line" Then '# NLS#'
                    Call LineSegmPar(xB, yB, xE, yE, l, Alp1)
                    With Segm
                        .xyEnd.v0 = xE
                        .xyEnd.v1 = yE
                        .l = l
                        .Alp.v0 = Alp1
                    End With
                End If
                If SegType = "Arc" Or SegType = "Circle" Then '# NLS#'
                    With Segm
                        .SegType = "Arc" '# NLS#'
                        .xyEnd.v0 = xE
                        .xyEnd.v1 = yE
                        Call ArcMiddlePoint(xc, yc, xB, yB, xE, yE, R, Rot, xMid, yMid)
                        .xyMid.v0 = xMid
                        .xyMid.v1 = yMid
                        .Distinct = ArcDictinct(xB, yB, xE, yE, xMid, yMid, Rot)
                        Call AnglesForArc(xB, yB, xE, yE, xc, yc, R, Rot, Alp1, Alp2)
                        .Alp.v0 = Alp1
                        .Alp.v1 = Alp2
                    End With
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object Contj.Segm(Contj.NumSegm - 1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Contj.Segm(Contj.NumSegm - 1) = Segm
                ReDim Preserve ContsRes.Cont(ContsRes.NumCont)
                'UPGRADE_WARNING: Couldn't resolve default property of object ContsRes.Cont(ContsRes.NumCont). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContsRes.Cont(ContsRes.NumCont) = Contj
                ContsRes.NumCont = ContsRes.NumCont + 1
                TrimContByLastPoint = True
                Exit For
            End If
        Next i

        'UPGRADE_NOTE: Object DefInter may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        DefInter = Nothing
        GXMD.Collect()

    End Function

    ' Clear Contour from parallel Arcs and Lines

    Private Sub ClearContFromParalSegments(ByRef Cont As Contour)

        Dim i, j As Integer
        Dim SegTypeCur As String
        Dim Alp1Cur, LCur, xMidCur, xcCur, xECur, xBCur, yBCur, yECur, ycCur, yMidCur, RCur, Alp2Cur As Double
        Dim RotCur, DistCur As Short
        Dim SegTypeNext As String
        Dim Alp1Next, LNext, xMidNext, xcNext, xENext, xBNext, yBNext, yENext, ycNext, yMidNext, RNext, Alp2Next As Double
        Dim RotNext, DistNext As Short
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As Segment

BEGCICLE:
        For i = 0 To Cont.NumSegm - 2
            With Cont.Segm(i)
                SegTypeCur = .SegType
                xBCur = .xyBeg.v0
                yBCur = .xyBeg.v1
                xECur = .xyEnd.v0
                yECur = .xyEnd.v1
                xcCur = .xyCent.v0
                ycCur = .xyCent.v1
                xMidCur = .xyMid.v0
                yMidCur = .xyMid.v1
                LCur = .l
                RCur = .R
                RotCur = .Rot
                DistCur = .Distinct
                Alp1Cur = .Alp.v0
                Alp2Cur = .Alp.v1
            End With

            With Cont.Segm(i + 1)
                SegTypeNext = .SegType
                xBNext = .xyBeg.v0
                yBNext = .xyBeg.v1
                xENext = .xyEnd.v0
                yENext = .xyEnd.v1
                xcNext = .xyCent.v0
                ycNext = .xyCent.v1
                xMidNext = .xyMid.v0
                yMidNext = .xyMid.v1
                LNext = .l
                RNext = .R
                RotNext = .Rot
                DistNext = .Distinct
                Alp1Next = .Alp.v0
                Alp2Next = .Alp.v1
            End With

            If SegTypeCur = "Line" And SegTypeNext = "Line" Then '# NLS#'
                If System.Math.Abs(Alp1Cur - Alp1Next) < Eps Then
                    With Cont.Segm(i)
                        .xyEnd.v0 = xENext
                        .xyEnd.v1 = yENext
                        .l = LCur + LNext
                    End With
                    For j = i + 1 To Cont.NumSegm - 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Segm = Cont.Segm(j + 1)
                        'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(j). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Cont.Segm(j) = Segm
                    Next j
                    Cont.NumSegm = Cont.NumSegm - 1
                    GoTo BEGCICLE
                End If
            End If
            If SegTypeCur = "Arc" And SegTypeNext = "Arc" Then '# NLS#'
                If System.Math.Abs(xBCur - xENext) > Eps Or System.Math.Abs(yBCur - yENext) > Eps Then
                    If System.Math.Abs(xcCur - xcNext) < Eps And System.Math.Abs(ycCur - ycNext) < Eps And System.Math.Abs(RCur - RNext) < Eps And RotCur = RotNext Then
                        With Cont.Segm(i)
                            .xyEnd.v0 = xENext
                            .xyEnd.v1 = yENext
                            Call ArcMiddlePoint(xcCur, ycCur, xBCur, yBCur, xENext, yENext, RCur, RotCur, xMidCur, yMidCur)
                            .xyMid.v0 = xMidCur
                            .xyMid.v1 = yMidCur
                            .Alp.v1 = Alp2Next
                            DistCur = ArcDictinct(xBCur, yBCur, xENext, yENext, xMidCur, yMidCur, RotCur)
                            .Distinct = DistCur
                        End With
                        For j = i + 1 To Cont.NumSegm - 2
                            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Segm = Cont.Segm(j + 1)
                            'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(j). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Cont.Segm(j) = Segm
                        Next j
                        Cont.NumSegm = Cont.NumSegm - 1
                        GoTo BEGCICLE
                    End If
                End If
                If System.Math.Abs(xBCur - xENext) < Eps Or System.Math.Abs(yBCur - yENext) > Eps Then
                    If System.Math.Abs(xcCur - xcNext) < Eps And System.Math.Abs(ycCur - ycNext) < Eps And System.Math.Abs(RCur - RNext) < Eps And RotCur = RotNext Then
                        xECur = 2 * xcCur - xBCur
                        yECur = 2 * ycCur - yBCur
                        xBNext = xECur
                        yBNext = yECur

                        Call ArcMiddlePoint(xcCur, ycCur, xBCur, yBCur, xECur, yECur, RCur, RotCur, xMidCur, yMidCur)
                        Call AnglesForArc(xBCur, yBCur, xECur, yECur, xcCur, ycCur, RCur, RotCur, Alp1Cur, Alp2Cur)
                        DistCur = ArcDictinct(xBCur, yBCur, xECur, yECur, xMidCur, yMidCur, RotCur)
                        With Cont.Segm(i)
                            .xyBeg.v0 = xBCur
                            .xyBeg.v1 = yBCur
                            .xyEnd.v0 = xECur
                            .xyEnd.v1 = yECur
                            .xyMid.v0 = xMidCur
                            .xyMid.v1 = yMidCur
                            .Distinct = DistCur
                            .Alp.v0 = Alp1Cur
                            .Alp.v1 = Alp2Cur
                        End With

                        Call ArcMiddlePoint(xcNext, ycNext, xBNext, yBNext, xENext, yENext, RNext, RotNext, xMidNext, yMidNext)
                        Call AnglesForArc(xBNext, yBNext, xENext, yENext, xcNext, ycNext, RNext, RotNext, Alp1Next, Alp2Next)
                        DistNext = ArcDictinct(xBNext, yBNext, xENext, yENext, xMidNext, yMidNext, RotNext)
                        With Cont.Segm(i + 1)
                            .xyBeg.v0 = xBNext
                            .xyBeg.v1 = yBNext
                            .xyEnd.v0 = xENext
                            .xyEnd.v1 = yENext
                            .xyMid.v0 = xMidNext
                            .xyMid.v1 = yMidNext
                            .Distinct = DistNext
                            .Alp.v0 = Alp1Next
                            .Alp.v1 = Alp2Next
                        End With

                    End If
                End If
            End If
        Next i

        If Cont.NumSegm = 2 Then
            If Cont.Segm(0).SegType = "Arc" And Cont.Segm(1).SegType = "Arc" Then '# NLS#'
                With Cont.Segm(0)
                    SegTypeCur = .SegType
                    xBCur = .xyBeg.v0
                    yBCur = .xyBeg.v1
                    xECur = .xyEnd.v0
                    yECur = .xyEnd.v1
                    xcCur = .xyCent.v0
                    ycCur = .xyCent.v1
                    xMidCur = .xyMid.v0
                    yMidCur = .xyMid.v1
                    LCur = .l
                    RCur = .R
                    RotCur = .Rot
                    DistCur = .Distinct
                    Alp1Cur = .Alp.v0
                    Alp2Cur = .Alp.v1
                End With

                With Cont.Segm(1)
                    SegTypeNext = .SegType
                    xBNext = .xyBeg.v0
                    yBNext = .xyBeg.v1
                    xENext = .xyEnd.v0
                    yENext = .xyEnd.v1
                    xcNext = .xyCent.v0
                    ycNext = .xyCent.v1
                    xMidNext = .xyMid.v0
                    yMidNext = .xyMid.v1
                    LNext = .l
                    RNext = .R
                    RotNext = .Rot
                    DistNext = .Distinct
                    Alp1Next = .Alp.v0
                    Alp2Next = .Alp.v1
                End With

                If System.Math.Abs(xBCur - xENext) < Eps And System.Math.Abs(yBCur - yENext) < Eps Then
                    If System.Math.Abs(xcCur - xcNext) < Eps And System.Math.Abs(ycCur - ycNext) < Eps And System.Math.Abs(RCur - RNext) < Eps And RotCur = RotNext Then
                        Cont.NumSegm = 1
                        With Cont.Segm(0)
                            .SegType = "Circle" '# NLS#'
                            .xyBeg.v0 = xcCur + RCur
                            .xyBeg.v1 = ycCur
                            .xyEnd.v0 = ycCur + RCur
                            .xyEnd.v1 = xcCur
                            .R = RCur
                            .Rot = 1
                        End With
                    End If
                End If
            End If
        End If

    End Sub

    ' Calculate koeff for plane equation from matrix
    Private Function UCSGetPlane(ByRef GrUcs As IMSIGX.Matrix, ByRef a As Double, ByRef b As Double, ByRef c As Double, ByRef d As Double) As Boolean
        UCSGetPlane = True

        'Dim Gr1 As New XGraphic
        Dim Gr1 As IMSIGX.Graphic
        Gr1 = MakeGraphic()

        Dim Ver0 As IMSIGX.Vertex
        Dim Ver1 As IMSIGX.Vertex
        Dim gxVrts As IMSIGX.Vertices

        Dim z0, x0, y0 As Double

        gxVrts = Gr1.Vertices
        With gxVrts
            .UseWorldCS = True
            Ver0 = .Add(0, 0, 0)
            Ver1 = .Add(0, 0, 1)
        End With

        GXMD.Release(Ver0)
        GXMD.Release(Ver1)
        GXMD.Release(gxVrts)

        'UPGRADE_WARNING: Couldn't resolve default property of object GrUcs. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Gr1.Transform(GrUcs)

        gxVrts = Gr1.Vertices
        With gxVrts
            .UseWorldCS = True

            Ver0 = .Item(0)
            Ver1 = .Item(1)

            a = Ver1.X - Ver0.X
            b = Ver1.Y - Ver0.Y
            c = Ver1.Z - Ver0.Z
            x0 = Ver0.X
            y0 = Ver0.Y
            z0 = Ver0.Z
        End With

        GXMD.Release(Ver0)
        GXMD.Release(Ver1)
        GXMD.Release(gxVrts)

        Dim l As Double
        l = System.Math.Sqrt(a * a + b * b + c * c)
        If l < 0.00000001 Then
            Gr1.Delete()
            GXMD.Release(Gr1)
            GXMD.Collect()
            UCSGetPlane = False
            Exit Function
        End If
        a = a / l
        b = b / l
        c = c / l
        d = -(a * x0 + b * y0 + c * z0)

        Gr1.Delete()
        GXMD.Release(Gr1)
        GXMD.Collect()

    End Function


    Private Function TrimEllipse(ByRef GrEllip As IMSIGX.Graphic) As IMSIGX.Graphic
        'UPGRADE_NOTE: Object TrimEllipse may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        TrimEllipse = Nothing
        GXMD.Collect()

        On Error GoTo ErrEnd
        Dim yE, yB, xB, xE, Alp As Double
        xB = ContRes.Segm(0).xyBeg.v0
        yB = ContRes.Segm(0).xyBeg.v1
        xE = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
        yE = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
        Alp = ContRes.Segm(0).Alp.v0
        Dim xc, yc As Double
        Dim xa, ya As Double
        Dim xm, ym As Double
        Dim a, b As Double
        Dim sina, cosa As Double
        Dim xLoc, yLoc As Double
        Dim R As Double
        Dim BetAxis As Double
        Dim fiBeg, fiEnd As Double
        Dim xDir As Object
        Dim yDir As Double
        'UPGRADE_NOTE: Dir was upgraded to Dir_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Dir_Renamed As Short
        Dim ArcData(10) As Double
        Dim Ratio As Double

        GrEllip.GetArcData(ArcData)
        Ratio = ArcData(6)

        xc = ArcData(0)
        yc = ArcData(1)
        xa = ArcData(3)
        ya = ArcData(4)
        a = System.Math.Sqrt((xa - xc) ^ 2 + (ya - yc) ^ 2)
        sina = (ya - yc) / a
        cosa = (xa - xc) / a
        BetAxis = Angle(sina, cosa)
        Ratio = ArcData(6)
        b = a * Ratio

        R = System.Math.Sqrt((xB - xc) ^ 2 + (yB - yc) ^ 2)
        sina = (yB - yc) / R
        cosa = (xB - xc) / R
        fiBeg = Angle(sina, cosa)
        fiBeg = fiBeg - BetAxis
        'UPGRADE_WARNING: Couldn't resolve default property of object xDir. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        xDir = xB + System.Math.Cos(Alp)
        yDir = yB + System.Math.Sin(Alp)

        'UPGRADE_WARNING: Couldn't resolve default property of object xDir. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        yLoc = (yDir - yc) * cosa - (xDir - xc) * sina
        If yLoc > 0 Then
            Dir_Renamed = 1
        Else
            Dir_Renamed = -1
        End If

        R = System.Math.Sqrt((xE - xc) ^ 2 + (yE - yc) ^ 2)
        sina = (yE - yc) / R
        cosa = (xE - xc) / R
        fiEnd = Angle(sina, cosa)
        fiEnd = fiEnd - BetAxis

        xLoc = 0
        yLoc = b
        xm = xc + xLoc * System.Math.Cos(BetAxis) - yLoc * System.Math.Sin(BetAxis)
        ym = yc + xLoc * System.Math.Sin(BetAxis) + yLoc * System.Math.Cos(BetAxis)

        If Dir_Renamed = 1 Then
            TrimEllipse = Grs.AddArcRotatedElliptical(xc, yc, 0, xa, ya, 0, xm, ym, 0, fiBeg, fiEnd)
        Else
            TrimEllipse = Grs.AddArcRotatedElliptical(xc, yc, 0, xa, ya, 0, xm, ym, 0, fiEnd, fiBeg)
        End If

        Exit Function
ErrEnd:
        'UPGRADE_NOTE: Object TrimEllipse may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        TrimEllipse = Nothing
        GXMD.Collect()
    End Function

    Private Sub CreatePlaneGraphicNew(ByRef Cont As Contour, ByRef Gr As IMSIGX.Graphic)


        Dim j As Integer
        Dim xMid, xDir, xE, xB, xj, R, xc, yc, l, yj, yB, yE, yDir, yMid As Double
        Dim Rot As Short
        Dim cosa, sina, yLoc As Double
        Dim xLoc As Object
        Dim Ver As IMSIGX.Vertex
        If Cont.NumSegm = 1 And (Cont.Segm(0).SegType = "Circle" Or Cont.Segm(0).SegType = "Arc") Then '# NLS#'
            With Cont.Segm(0)
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                R = .R
                Rot = .Rot
            End With
            sina = (yB - yc) / R
            cosa = (xB - xc) / R
            'UPGRADE_WARNING: Couldn't resolve default property of object xLoc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            xLoc = 0
            yLoc = R * Rot
            'UPGRADE_WARNING: Couldn't resolve default property of object xLoc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            xDir = xc + xLoc * cosa - yLoc * sina
            'UPGRADE_WARNING: Couldn't resolve default property of object xLoc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            yDir = yc + xLoc * sina + yLoc * cosa
            If Cont.Segm(0).SegType = "Circle" Then '# NLS#'
                '                    Set Gr = Grs.AddCircleCenterAndPoint(xc, yc, 0, xc + R, yc, 0)
                Gr = objApp.ActiveDrawing.Graphics.AddCircleCenterAndPoint(xc, yc, 0, xc + R, yc, 0)
                With Gr.Vertices
                    .Item(1).X = xB
                    .Item(1).Y = yB
                    .Item(2).X = xB
                    .Item(2).Y = yB
                    .Item(3).X = xDir
                    .Item(3).Y = yDir
                End With
            End If
            If Cont.Segm(0).SegType = "Arc" Then '# NLS#'
                '                    Set Gr = Grs.AddArcTriplePoint(xB, yB, 0, xMid, yMid, 0, xE, yE, 0)
                Gr = GxSet.AddArcTriplePoint(xB, yB, 0, xMid, yMid, 0, xE, yE, 0)
            End If
        Else
            '                Set Gr = Grs.Add(, "TCW50Polyline")
            Gr = objApp.ActiveDrawing.Graphics.Add(oMissing, "TCW50Polyline") '# NLS#'
            For j = 0 To Cont.NumSegm - 1
                If j = 0 Then
                    xj = Cont.Segm(j).xyBeg.v0
                    yj = Cont.Segm(j).xyBeg.v1
                    Gr.Vertices.Add(xj, yj, 0)
                End If
                If Cont.Segm(j).SegType = "Line" Then '# NLS#'
                    xj = Cont.Segm(j).xyEnd.v0
                    yj = Cont.Segm(j).xyEnd.v1
                    Gr.Vertices.Add(xj, yj, 0)
                End If
                If Cont.Segm(j).SegType = "Arc" Then '# NLS#'
                    xB = Cont.Segm(j).xyBeg.v0
                    yB = Cont.Segm(j).xyBeg.v1
                    xE = Cont.Segm(j).xyEnd.v0
                    yE = Cont.Segm(j).xyEnd.v1
                    l = System.Math.Sqrt((xE - xB) ^ 2 + (yE - yB) ^ 2)
                    If l > 1000 * Eps Then
                        xj = Cont.Segm(j).xyCent.v0
                        yj = Cont.Segm(j).xyCent.v1
                        Ver = Gr.Vertices.Add(xj, yj, 0)
                        Ver.Bulge = True

                        xj = Cont.Segm(j).xyMid.v0
                        yj = Cont.Segm(j).xyMid.v1
                        Ver = Gr.Vertices.Add(xj, yj, 0)
                        Ver.Bulge = True
                    End If

                    xj = Cont.Segm(j).xyEnd.v0
                    yj = Cont.Segm(j).xyEnd.v1
                    Gr.Vertices.Add(xj, yj, 0)
                End If
            Next j
            Gr.Update()
            '                Gr.Draw Grs.Drawing.ActiveView
            Gr.Draw()
        End If
        xB = Gr.Vertices.Item(0).X
        yB = Gr.Vertices.Item(0).Y
        xE = Gr.Vertices.Item(Gr.Vertices.Count - 1).X
        yE = Gr.Vertices.Item(Gr.Vertices.Count - 1).Y
        If System.Math.Abs(xB - xE) < 1 * Eps And System.Math.Abs(yB - yE) < 1 * Eps Then
            Gr.Vertices.Item(Gr.Vertices.Count - 1).X = xB
            Gr.Vertices.Item(Gr.Vertices.Count - 1).Y = yB
            Gr.Closed = True
        End If
        Gr.Properties.Item("PenColor").Value = RGB(0, 0, 255)
        Gr.Properties.Item("PenWidth").Value = 0.02
        Gr.Draw()
        'UPGRADE_NOTE: Object Ver may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Ver)
        GXMD.Collect()

    End Sub


    Private Function FindIntersection(ByRef GrCont As IMSIGX.Graphic, ByRef CurCont As Contour, ByRef iSeg As Integer, ByRef iLastCur As Integer) As Boolean
        'MsgBox ("FindIntersection")
        Dim GrCurType As String
        GrCurType = GrCont.Type
        Dim Lays As IMSIGX.Layers
        Dim OldLayer As IMSIGX.Layer
        Dim NewLayer As IMSIGX.Layer
        If GrCurType = "TCW30CURVE" Then '# NLS#'
            Lays = ActDr.Layers
            OldLayer = GrCont.Layer
            NewLayer = Lays.Add("unvis", False, False, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing) '# NLS#'
            GrCont.Layer = NewLayer
        End If

        FindIntersection = False
        'UPGRADE_WARNING: Arrays in structure Segmi may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segmi As Segment
        Dim SegType As String
        Dim Alp1, R, xMid, xE, yE, yMid, l, Alp2 As Double
        Dim Rot As Short
        Dim SegTypei As String
        Dim Alp1i, Ri, xMidi, xci, xEi, xBi, yBi, yEi, yci, yMidi, Li, Alp2i As Double
        Dim Roti As Short
        Dim Aper As Double
        Dim xIntNearest, yIntNearest As Double
        Dim GrTem As IMSIGX.Graphic
        Dim PRes As IMSIGX.PickResult
        Dim GrSets As IMSIGX.GraphicSets
        GrSets = ActDr.GraphicSets
        Dim GrSet As IMSIGX.GraphicSet
        GrSet = GrSets.Add("", True)
        Dim SetCount As Integer

        Dim Gr As IMSIGX.Graphic

        Dim zj, xj, yj, Lj As Double
        Dim xjV, yjV As Double
        Dim xInt, yInt As Double

        Dim SegDiv As Short
        SegDiv = 10
        Dim i, j As Integer
        'UPGRADE_WARNING: Arrays in structure Conti may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Conti As New Contour
        Dim ViH As Double
        ViH = Vi.ViewHeight
        'UPGRADE_WARNING: Arrays in structure Conts may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Conts As New Contours
        Dim InterExistCur As Boolean
        Dim LMin, LCur As Double
        Dim jCur, k, kCur As Integer
        Dim Inter() As IntersTwoCont
        ReDim Inter(0)

        i = iSeg
        Dim yMin, zMax, xMax, yMax, xMin, zMin As Double
        Dim yMinV, zMaxV, xMaxV, yMaxV, xMinV, zMinV As Double
        Dim dx, dy As Double
        Dim sinE, fiELoc, xLoc, sina, cosa, yLoc, fiLoc, cosE As Double
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As Segment
        'UPGRADE_WARNING: Arrays in structure Contj may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Contj As New Contour
        Do While i < CurCont.NumSegm
            Aper = ViH / 50
            'UPGRADE_WARNING: Couldn't resolve default property of object Segmi. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segmi = CurCont.Segm(i)
            ReDim Conti.Segm(0)
            'UPGRADE_WARNING: Couldn't resolve default property of object Conti.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Conti.Segm(0) = Segmi
            Conti.NumSegm = 1
            With Segmi
                SegTypei = .SegType
                xBi = .xyBeg.v0
                yBi = .xyBeg.v1
                xEi = .xyEnd.v0
                yEi = .xyEnd.v1
                xci = .xyCent.v0
                yci = .xyCent.v1
                xMidi = .xyMid.v0
                yMidi = .xyMid.v1
                Li = .l
                Ri = .R
                Roti = .Rot
                Alp1i = .Alp.v0
                Alp2i = .Alp.v1
            End With
            '        ActDr.Application.SnapModes = 1
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If SegTypei = "Line" Then '# NLS#'

                If Li < 0.5 * Aper And CurCont.NumSegm > 100 Then
                    ReDim Contj.Segm(0)

                    k = 0
                    For j = i To CurCont.NumSegm - 1
                        If CurCont.Segm(j).SegType <> "Line" Then Exit For '# NLS#'
                        ReDim Preserve Contj.Segm(k)
                        'UPGRADE_WARNING: Couldn't resolve default property of object Contj.Segm(k). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Contj.Segm(k) = CurCont.Segm(j)
                        Contj.NumSegm = k + 1
                        Call ContourExtents(Contj)
                        xMax = Contj.xMax
                        yMax = Contj.yMax
                        xMin = Contj.xMin
                        yMin = Contj.yMin
                        dx = xMax - xMin
                        dy = yMax - yMin
                        If dx > Aper Or dy > Aper Or j = CurCont.NumSegm - 1 Then
                            If k = 0 Then Exit For
                            Contj.NumSegm = k
                            '---------------------------

                            GrTem = MakeGraphic()
                            GrTem.Vertices.Add(xMax, yMax, 0)
                            On Error Resume Next
                            'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            GrTem.Transform(UCSGr0)
                            With GrTem.Vertices
                                .UseWorldCS = True
                                xMax = .Item(0).X
                                yMax = .Item(0).Y
                                zMax = .Item(0).Z
                                .UseWorldCS = False
                            End With
                            GrTem.Delete()
                            GXMD.Release(GrTem)
                            GXMD.Collect()

                            GrTem = MakeGraphic()
                            GrTem.Vertices.Add(xMin, yMin, 0)
                            On Error Resume Next
                            'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            GrTem.Transform(UCSGr0)
                            With GrTem.Vertices
                                .UseWorldCS = True
                                xMin = .Item(0).X
                                yMin = .Item(0).Y
                                zMin = .Item(0).Z
                                .UseWorldCS = False
                            End With
                            GrTem.Delete()

                            'UPGRADE_NOTE: Object GrTem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                            GXMD.Release(GrTem)
                            GXMD.Collect()

                            '---------------------------
                            Vi.WorldToView(xMin, yMin, zMin, xMinV, yMinV, 0)
                            Vi.WorldToView(xMax, yMax, zMax, xMaxV, yMaxV, 0)
                            PRes = Vi.PickRect(xMinV, yMinV, xMaxV, yMaxV, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
                            GrSet.Clear(oMissing)
                            GXMD.Collect()
                            Call PResToGrSet(PRes, GrSet, GrCont)
                            SetCount = GrSet.Count
                            GrSet.Clear(oMissing)
                            If SetCount > 0 Then
                                'MsgBox ("SetCount=" & CStr(SetCount))
                                Exit For
                            Else
                                For k = 0 To Contj.NumSegm - 1
                                    ReDim Preserve ContRes.Segm(ContRes.NumSegm)
                                    'UPGRADE_WARNING: Couldn't resolve default property of object ContRes.Segm(ContRes.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    ContRes.Segm(ContRes.NumSegm) = Contj.Segm(k)
                                    ContRes.NumSegm = ContRes.NumSegm + 1
                                Next k
                                xE = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                                yE = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
                                If j = CurCont.NumSegm - 1 Then
                                    FindIntersection = False
                                    Exit Do
                                End If

                                If System.Math.Abs(xE - xBeg0) < Eps And System.Math.Abs(yE - yBeg0) < Eps Then
                                    FindIntersection = False
                                    Exit Do
                                End If
                                i = j
                                GoTo LOOPEND
                            End If
                        End If
                        k = k + 1
                    Next j
                End If

                LMin = 10000000
                SegDiv = Int(Li / Aper) + 1
                Aper = Li / SegDiv

                For Lj = Aper / 2.01 To Li + Aper / 2 Step Aper

                    xj = xBi + (xEi - xBi) * Lj / Li
                    yj = yBi + (yEi - yBi) * Lj / Li
                    '---------------------------
                    GrTem = MakeGraphic()

                    GrTem.Vertices.Add(xj, yj, 0)
                    On Error Resume Next
                    'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    GrTem.Transform(UCSGr0)
                    With GrTem.Vertices
                        .UseWorldCS = True
                        xj = .Item(0).X
                        yj = .Item(0).Y
                        zj = .Item(0).Z
                        .UseWorldCS = False
                    End With
                    GrTem.Delete()
                    'UPGRADE_NOTE: Object GrTem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrTem)
                    GXMD.Collect()
                    '---------------------------
                    Vi.WorldToView(xj, yj, zj, xjV, yjV, 0)
                    PRes = Vi.PickRect(xjV - Aper, yjV - Aper, xjV + Aper, yjV + Aper, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)

                    If PRes.Count > 0 Then
                        GrSet.Clear(oMissing)
                        Call PResToGrSet(PRes, GrSet, GrCont)
                        SetCount = GrSet.Count

                        If SetCount > 0 Then
                            InterExistCur = False
                            InterExistCur = EntersectionExist(GrSet, Conti, Conts, Inter)

                            If InterExistCur = True Then
                                'jCur - Number of contour intersected current segment - i
                                'kCur - number of intersection
                                For j = 0 To Conts.NumCont - 1
                                    If Conts.Cont(j).Deleted = False Then
                                        For k = 0 To Inter(j).NumInt - 1
                                            xInt = Inter(j).xInt(k)
                                            yInt = Inter(j).yInt(k)
                                            LCur = System.Math.Sqrt((xInt - xBi) ^ 2 + (yInt - yBi) ^ 2)
                                            If LCur < LMin And LCur > Eps And LCur < Li + Eps Then
                                                LMin = LCur
                                                jCur = j
                                                kCur = k
                                                xIntNearest = xInt
                                                yIntNearest = yInt
                                            End If
                                        Next k
                                    End If
                                Next j
                            End If
                        End If
                    End If
                Next Lj
            End If ' End if SegTypei="Line"
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If SegTypei = "Arc" Or SegTypei = "Circle" Then '# NLS#'
                LMin = 10000000
                sina = (yBi - yci) / Ri
                cosa = (xBi - xci) / Ri
                yLoc = (yEi - yci) * cosa - (xEi - xci) * sina
                xLoc = (yEi - yci) * sina + (xEi - xci) * cosa
                sinE = yLoc / Ri
                cosE = xLoc / Ri
                fiELoc = Angle(sinE, cosE)
                If Roti = -1 Then
                    fiELoc = 2 * Pi - fiELoc
                End If
                If SegTypei = "Circle" Then fiELoc = 2 * Pi '# NLS#'
                Li = fiELoc * Ri

                SegDiv = Int(Li / Aper) + 1
                Aper = Li / SegDiv
                For Lj = Aper / 2.01 To Li Step Aper
                    fiLoc = Roti * Lj / Ri
                    xLoc = Ri * System.Math.Cos(fiLoc)
                    yLoc = Ri * System.Math.Sin(fiLoc)
                    xj = xci + xLoc * cosa - yLoc * sina
                    yj = yci + xLoc * sina + yLoc * cosa
                    '---------------------------
                    '                Set GrTem = Grs.Add(11)

                    'Set GrTem = New XGraphic
                    GrTem = MakeGraphic()

                    GrTem.Vertices.Add(xj, yj, 0)
                    On Error Resume Next
                    'UPGRADE_WARNING: Couldn't resolve default property of object UCSGr0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    GrTem.Transform(UCSGr0)
                    With GrTem.Vertices
                        .UseWorldCS = True
                        xj = .Item(0).X
                        yj = .Item(0).Y
                        zj = .Item(0).Z
                        .UseWorldCS = False
                    End With
                    GrTem.Delete()
                    'UPGRADE_NOTE: Object GrTem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    GXMD.Release(GrTem)
                    GXMD.Collect()
                    '---------------------------
                    Vi.WorldToView(xj, yj, zj, xjV, yjV, 0)
                    'Set PRes = Vi.PickPoint(xjV, yjV, Aper)
                    PRes = Vi.PickRect(xjV - Aper, yjV - Aper, xjV + Aper, yjV + Aper, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing)
                    If PRes.Count > 0 Then
                        GrSet.Clear(oMissing)
                        Call PResToGrSet(PRes, GrSet, GrCont)
                        SetCount = GrSet.Count
                        If SetCount > 0 Then
                            InterExistCur = False
                            InterExistCur = EntersectionExist(GrSet, Conti, Conts, Inter)
                            If InterExistCur = True Then

                                'jCur - Number of contour intersected current segment - i
                                'kCur - number of intersection
                                For j = 0 To Conts.NumCont - 1
                                    If Conts.Cont(j).Deleted = False Then
                                        For k = 0 To Inter(j).NumInt - 1
                                            xInt = Inter(j).xInt(k)
                                            yInt = Inter(j).yInt(k)
                                            yLoc = (yInt - yci) * cosa - (xInt - xci) * sina
                                            xLoc = (yInt - yci) * sina + (xInt - xci) * cosa
                                            sinE = yLoc / Ri
                                            cosE = xLoc / Ri
                                            fiLoc = Angle(sinE, cosE)
                                            If Roti = -1 Then
                                                fiLoc = 2 * Pi - fiLoc
                                            End If
                                            LCur = fiLoc * Ri
                                            If LCur < LMin And LCur > Eps And LCur < Li + Eps Then
                                                LMin = LCur
                                                jCur = j
                                                kCur = k
                                                xIntNearest = xInt
                                                yIntNearest = yInt
                                            End If
                                        Next k
                                    End If
                                Next j
                            End If
                        End If
                    End If
                Next Lj
            End If ' End if SegTypei="Arc" or "Circle"
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            InterExistCur = True
            If LMin > 10000000 - 1 Then InterExistCur = False
            If InterExistCur = False Then
                ReDim Preserve ContRes.Segm(ContRes.NumSegm)
                'UPGRADE_WARNING: Couldn't resolve default property of object ContRes.Segm(ContRes.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContRes.Segm(ContRes.NumSegm) = Segmi
                ContRes.NumSegm = ContRes.NumSegm + 1
                xE = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v0
                yE = ContRes.Segm(ContRes.NumSegm - 1).xyEnd.v1
                If System.Math.Abs(xE - xBeg0) < Eps And System.Math.Abs(yE - yBeg0) < Eps Then
                    FindIntersection = False
                    Exit Do
                End If
            End If

            If InterExistCur = True Then
                'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Segm = Segmi
                With Segm
                    SegType = .SegType
                    xE = xIntNearest
                    yE = yIntNearest
                    .xyEnd.v0 = xE
                    .xyEnd.v1 = yE
                    If SegTypei = "Line" Then '# NLS#'
                        Call LineSegmPar(xBi, yBi, xE, yE, l, Alp1)
                        .l = l
                        .Alp.v0 = Alp1
                    End If
                    If SegTypei = "Arc" Or SegTypei = "Circle" Then '# NLS#'
                        .SegType = "Arc" '# NLS#'
                        Call ArcMiddlePoint(xci, yci, xBi, yBi, xE, yE, Ri, Roti, xMid, yMid)
                        .xyMid.v0 = xMid
                        .xyMid.v1 = yMid
                        .Distinct = ArcDictinct(xBi, yBi, xE, yE, xMid, yMid, Roti)
                        Call AnglesForArc(xBi, yBi, xE, yE, xci, yci, Ri, Roti, Alp1, Alp2)
                        .Alp.v0 = Alp1
                        .Alp.v1 = Alp2
                    End If
                End With
                ReDim Preserve ContRes.Segm(ContRes.NumSegm)
                'UPGRADE_WARNING: Couldn't resolve default property of object ContRes.Segm(ContRes.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ContRes.Segm(ContRes.NumSegm) = Segm
                ContRes.NumSegm = ContRes.NumSegm + 1
                If System.Math.Abs(xE - xBeg0) < Eps And System.Math.Abs(yE - yBeg0) < Eps Then
                    FindIntersection = False
                    Exit Do
                End If
                iLastCur = i
                FindIntersection = True
                Exit Do
                '#################################################
            End If ' If Inter Exist
            i = i + 1
LOOPEND:
        Loop

        If GrCurType = "TCW30CURVE" Then '# NLS#'
            GrCont.Layer = OldLayer
            NewLayer.Delete()
            GXMD.Release(NewLayer)
            GXMD.Collect()
        End If

        GrSet.Clear(oMissing)
        'UPGRADE_NOTE: Object GrSet may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrSet)
        'UPGRADE_NOTE: Object GrSets may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrSets)
        'UPGRADE_NOTE: Object PRes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(PRes)
        'UPGRADE_NOTE: Object Gr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(Gr)
        GXMD.Collect()
    End Function

    Private Function PResToGrSet(ByRef PRes As IMSIGX.PickResult, ByRef GrSet As IMSIGX.GraphicSet, ByRef GrCont As IMSIGX.Graphic) As Boolean
        PResToGrSet = False
        Dim j As Integer
        Dim Gr As IMSIGX.Graphic
        Dim GrType As String
        Dim GrContID As Integer
        Dim Thick As Double
        Dim ci, ai, bi, di As Double
        Dim ChildCount, t As Integer
        Dim ChildOnlyDATA As Boolean

        For j = 0 To PRes.Count - 1
            Gr = PRes.Item(j).Graphic
            GrType = Gr.Type

            If Gr.ID = GrCont.ID Then GoTo JLINE

            If GrType = "TCW50Polyline" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                With Gr.Graphics
                    ChildCount = .Count
                    For t = 0 To ChildCount - 1
                        If .Item(t).ID = GrContID Then GoTo JLINE
                    Next t
                End With
            End If
            If GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE Or GrType = "TCW50Polyline" Or GrType = "TCW30CURVE" Or GrType = "TCW25DblLine" Or GrType = "TCW60Wall" Or GrType = "TCW50IntProp" Then
                '##########################################
                If Gr.Graphics.Count > 0 Then
                    For t = 0 To Gr.Graphics.Count - 1
                        If Gr.Graphics.Item(t).Type = GRAPHICTYPE Then
                            If Gr.Graphics.Item(t).Properties.Item("Info").Value = "Line_Width_Cosmetic" Then GoTo JLINE
                        End If
                    Next t
                End If
                '##########################################

                If ActDr.Properties.Item("TileMode").Value = 1 Then
                    Thick = 0
                    On Error Resume Next
#If False Then
                    Thick = Gr.Properties.Item("Thickness").Value
                    If System.Math.Abs(Thick) > Eps Then GoTo JLINE
                    Thick = -1000000002
                    On Error Resume Next
                    Thick = Gr.Properties.Item("Thickness").Value
                    If Thick = -1000000002 Then GoTo JLINE
#Else
                    Dim gxProps As IMSIGX.Properties
                    Dim gxProp As IMSIGX.Property
                    gxProps = Gr.Properties
                    gxProp = Gr.Properties.Item("Thickness")
                    If (gxProp Is Nothing) Then

                        GXMD.Release(gxProps)
                        GXMD.Collect()
                        Thick = -1000000002
                        GoTo JLINE

                    End If
                    Thick = gxProp.Value

                    GXMD.Release(gxProp)
                    GXMD.Release(gxProps)

                    GXMD.Collect()
                    If System.Math.Abs(Thick) > Eps Then GoTo JLINE

#End If
                End If

                If (GrType = GRAPHICTYPE Or GrType = CIRCLETYPE Or GrType = ARCTYPE) And Gr.Graphics.Count > 0 Then
                    ChildOnlyDATA = True
                    With Gr.Graphics
                        ChildCount = .Count
                        For t = 0 To ChildCount - 1
                            If .Item(t).Type <> "DATA" Then
                                ChildOnlyDATA = False
                                Exit For
                            End If
                        Next t
                    End With
                    If ChildOnlyDATA = False Then GoTo JLINE
                End If
                If Gr.Unbounded = True Then GoTo JLINE

                If GrType = "TCW30CURVE" Then
                    With Gr.Graphics
                        ChildCount = .Count
                        For t = 0 To ChildCount - 1
                            If .Item(t).Type = "TCW50IntProp" Then GoTo JLINE
                        Next t
                    End With
                End If
                If Gr.UCS.IsEqual(UCSGr0) = False Then GoTo JLINE
                UCSGetPlane(Gr.UCS, ai, bi, ci, di)
                If System.Math.Abs(ai) < Eps And System.Math.Abs(bi) < Eps And System.Math.Abs(di) < Eps And System.Math.Abs(ci + 1) < Eps Then
                    UCSGetPlane(Gr.UCS, ai, bi, ci, di)
                End If
                If System.Math.Abs(ai - a0) > Eps Or System.Math.Abs(bi - b0) > Eps Or System.Math.Abs(ci - c0) > Eps Or System.Math.Abs(di - d0) > Eps Then
                    GoTo JLINE
                End If

                On Error Resume Next
                If Gr.Type <> "TCW60Wall" Then
                    '               Gr.UCS = UCSGr0
                End If

                GrSet.AddGraphic(Gr, oMissing, oMissing)

            End If
JLINE:
            GXMD.Release(Gr)
            GXMD.Collect()
        Next j
        'UPGRADE_WARNING: Couldn't resolve default property of object GrSet.Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If GrSet.Count > 0 Then PResToGrSet = True


    End Function

    Private Function EntersectionExist(ByRef GrSet As IMSIGX.GraphicSet, ByRef Conti As Contour, ByRef Conts As Contours, ByRef Inter() As IntersTwoCont) As Boolean
        EntersectionExist = False
        Dim SetCount As Integer
        SetCount = GrSet.Count
        If SetCount < 1 Then
            EntersectionExist = False
            Exit Function
        End If
        Dim i As Integer
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As New Contour
        Dim InitCont As New InitializeContour
        Dim IntDef As New DefineIntersections
        ReDim Conts.Cont(SetCount - 1)
        Conts.NumCont = SetCount
        ReDim Inter(SetCount - 1)

        'Dim Gr As IMSIGX.Graphic

        For i = 0 To SetCount - 1
            Dim gxGrf As IMSIGX.Graphic
            gxGrf = GrSet.Item(i)
            Call InitCont.RunInitialize(gxGrf, Cont, 0, 0)
            GXMD.Release(gxGrf)
            GXMD.Collect()
            'UPGRADE_WARNING: Couldn't resolve default property of object Conts.Cont(i). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Conts.Cont(i) = Cont
            Inter(i).NumInt = 0
        Next i
        For i = 0 To Conts.NumCont - 1
            Conts.Cont(i).Deleted = False
            'UPGRADE_WARNING: Couldn't resolve default property of object Cont. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Cont = Conts.Cont(i)
            'conti- current segment of current contour
            If IntDef.RunIntersect(Conti, Cont, Inter(i)) = False Then
                Conts.Cont(i).Deleted = True
            Else
                EntersectionExist = True
            End If
        Next i

    End Function

    ' Define Contour's extents
    Private Sub ContourExtents(ByRef Cont0 As Contour)
        Dim xMin, xMax, yMax, yMin As Double
        xMax = -100000000
        yMax = -100000000
        xMin = 100000000
        yMin = 100000000

        Dim i, j As Integer
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As Segment
        Dim SegType As String
        Dim Alp2, l, xc, xMid, xE, xB, yB, yE, yMid, yc, Alp1, R As Double
        Dim Rot, Dist As Short
        'UPGRADE_WARNING: Arrays in structure Cont may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Cont As New Contour
        ReDim Cont.Segm(0)
        Cont.NumSegm = 1
        'UPGRADE_WARNING: Arrays in structure ContLine may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContLine As New Contour
        ReDim ContLine.Segm(0)
        ContLine.NumSegm = 1

        'UPGRADE_WARNING: Arrays in structure Inter may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Inter As New IntersTwoCont
        Dim IntDef As New DefineIntersections
        Dim xInt, yInt As Double
        For i = 0 To Cont0.NumSegm - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object Segm. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Segm = Cont0.Segm(i)
            With Segm
                SegType = .SegType
                xB = .xyBeg.v0
                yB = .xyBeg.v1
                xE = .xyEnd.v0
                yE = .xyEnd.v1
                l = .l
                Alp1 = .Alp.v0
                Alp2 = .Alp.v1
                xc = .xyCent.v0
                yc = .xyCent.v1
                xMid = .xyMid.v0
                yMid = .xyMid.v1
                R = .R
                If SegType = "Line" Then '# NLS#'
                    R = 0
                End If
                Rot = .Rot
                Dist = .Distinct
            End With
            If xB > xMax Then xMax = xB
            If xE > xMax Then xMax = xE
            If xB < xMin Then xMin = xB
            If xE < xMin Then xMin = xE

            If yB > yMax Then yMax = yB
            If yE > yMax Then yMax = yE
            If yB < yMin Then yMin = yB
            If yE < yMin Then yMin = yE

            If SegType = "Circle" Then '# NLS#'
                If xc + R > xMax Then xMax = xc + R
                If xc - R < xMin Then xMin = xc - R
                If yc + R > yMax Then yMax = yc + R
                If yc - R < yMin Then yMin = yc - R
            End If

            If SegType = "Arc" Then '# NLS#'
                'UPGRADE_WARNING: Couldn't resolve default property of object Cont.Segm(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Cont.Segm(0) = Segm
                'Horizontal line
                With ContLine.Segm(0)
                    .SegType = "Line" '# NLS#'
                    .Alp.v0 = 0
                    .xyBeg.v0 = xc - 2 * R
                    .xyBeg.v1 = yc
                    .xyEnd.v0 = xc + 2 * R
                    .xyEnd.v1 = yc
                    .l = 4 * R
                End With
                If IntDef.RunIntersect(Cont, ContLine, Inter) = True Then
                    For j = 0 To Inter.NumInt - 1
                        xInt = Inter.xInt(j)
                        yInt = Inter.yInt(j)
                        If xInt > xMax Then xMax = xInt
                        If xInt > xMax Then xMax = xInt
                        If xInt < xMin Then xMin = xInt
                        If xInt < xMin Then xMin = xInt

                        If yInt > yMax Then yMax = yInt
                        If yInt > yMax Then yMax = yInt
                        If yInt < yMin Then yMin = yInt
                        If yInt < yMin Then yMin = yInt
                    Next j
                End If

                'Vertical line
                With ContLine.Segm(0)
                    .SegType = "Line" '# NLS#'
                    .Alp.v0 = Pi / 2
                    .xyBeg.v0 = xc
                    .xyBeg.v1 = yc - 2 * R
                    .xyEnd.v0 = xc
                    .xyEnd.v1 = yc + 2 * R
                    .l = 4 * R
                End With
                If IntDef.RunIntersect(Cont, ContLine, Inter) = True Then
                    For j = 0 To Inter.NumInt - 1
                        xInt = Inter.xInt(j)
                        yInt = Inter.yInt(j)
                        If xInt > xMax Then xMax = xInt
                        If xInt > xMax Then xMax = xInt
                        If xInt < xMin Then xMin = xInt
                        If xInt < xMin Then xMin = xInt

                        If yInt > yMax Then yMax = yInt
                        If yInt > yMax Then yMax = yInt
                        If yInt < yMin Then yMin = yInt
                        If yInt < yMin Then yMin = yInt
                    Next j
                End If
            End If
        Next i
        Cont0.xMax = xMax
        Cont0.yMax = yMax
        Cont0.xMin = xMin
        Cont0.yMin = yMin

        'UPGRADE_NOTE: Object IntDef may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        IntDef = Nothing
        GXMD.Collect()

    End Sub

#If _USE_NATIVEIMSIGX = False Then
    Public Sub BeforeDoubleClick(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window, Sel As IMSIGX.Selection, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.BeforeDoubleClick

    End Sub

    Public Sub BeforeExit1(TheApp As IMSIGX.Application, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.BeforeExit

    End Sub

    Public Sub BeforeRightClick(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window, Sel As IMSIGX.Selection, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.BeforeRightClick

    End Sub

    Public Sub CommandBarControlDone(WhichControl As IMSIGX.CommandBarControl) Implements IMSIGX.IAppEvents.CommandBarControlDone

    End Sub

    Public Sub CommandBarControlHit(WhichControl As IMSIGX.CommandBarControl, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.CommandBarControlHit

    End Sub

    Public Sub CommandBarControlStatus(WhichControl As IMSIGX.CommandBarControl) Implements IMSIGX.IAppEvents.CommandBarControlStatus

    End Sub

    Public Sub DrawingActivate1(WhichDrawing As IMSIGX.Drawing) Implements IMSIGX.IAppEvents.DrawingActivate

    End Sub

    Public Sub DrawingAfterSave(WhichDrawing As IMSIGX.Drawing) Implements IMSIGX.IAppEvents.DrawingAfterSave

    End Sub

    Public Sub DrawingBeforeClose1(WhichDrawing As IMSIGX.Drawing, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.DrawingBeforeClose

    End Sub

    Public Sub DrawingBeforeSave1(WhichDrawing As IMSIGX.Drawing, ByRef SaveAs As Boolean, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.DrawingBeforeSave

    End Sub

    Public Sub DrawingDeactivate1(WhichDrawing As IMSIGX.Drawing) Implements IMSIGX.IAppEvents.DrawingDeactivate

    End Sub

    Public Sub DrawingNew1(WhichDrawing As IMSIGX.Drawing) Implements IMSIGX.IAppEvents.DrawingNew

    End Sub

    Public Sub DrawingOpen1(WhichDrawing As IMSIGX.Drawing) Implements IMSIGX.IAppEvents.DrawingOpen

    End Sub

    Public Sub Drop(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window, Sel As IMSIGX.Selection) Implements IMSIGX.IAppEvents.Drop

    End Sub

    Public Sub MouseDown1(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window, Button As IMSIGX.ImsiMouseButton, Shift As Integer, X As Integer, Y As Integer, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.MouseDown
        MouseDown(WhichDrawing, WhichView, WhichWindow, Button, Shift, X, Y, Cancel)
    End Sub

    Public Sub MouseMove1(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window, Shift As Integer, X As Integer, Y As Integer, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.MouseMove
        MouseMove(WhichDrawing, WhichView, WhichWindow, Shift, X, Y, Cancel)
    End Sub

    Public Sub MouseUp(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window, Button As IMSIGX.ImsiMouseButton, Shift As Integer, X As Integer, Y As Integer, ByRef Cancel As Boolean) Implements IMSIGX.IAppEvents.MouseUp

    End Sub

    Public Sub PointPick(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, Result As IMSIGX.PickResult, PickWasCanceled As Boolean) Implements IMSIGX.IAppEvents.PointPick

    End Sub

    Public Sub PointSnapped(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, X As Integer, Y As Integer, PointRaw As IMSIGX.Vertex, PointSnapped As IMSIGX.Vertex) Implements IMSIGX.IAppEvents.PointSnapped

    End Sub

    Public Sub PolygonPick(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, Result As IMSIGX.PickResult, PickWasCanceled As Boolean) Implements IMSIGX.IAppEvents.PolygonPick

    End Sub

    Public Sub RectanglePick(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, Result As IMSIGX.PickResult, PickWasCanceled As Boolean) Implements IMSIGX.IAppEvents.RectanglePick

    End Sub

    Public Sub RunTool1(WhichTool As IMSIGX.Tool) Implements IMSIGX.IAppEvents.RunTool

    End Sub

    Public Sub SelectionChange(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window, Sel As IMSIGX.Selection) Implements IMSIGX.IAppEvents.SelectionChange

    End Sub

    Public Sub ViewAfterRedraw(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View) Implements IMSIGX.IAppEvents.ViewAfterRedraw

    End Sub

    Public Sub ViewBeforeRedraw(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View) Implements IMSIGX.IAppEvents.ViewBeforeRedraw
        'GC.Collect()
    End Sub

    Public Sub VirtualIntersectionPick(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, Result As IMSIGX.PickResult, PickWasCanceled As Boolean) Implements IMSIGX.IAppEvents.VirtualIntersectionPick

    End Sub

    Public Sub WindowActivate(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window) Implements IMSIGX.IAppEvents.WindowActivate

    End Sub

    Public Sub WindowDeactivate(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window) Implements IMSIGX.IAppEvents.WindowDeactivate

    End Sub

    Public Sub WindowResize(WhichDrawing As IMSIGX.Drawing, WhichView As IMSIGX.View, WhichWindow As IMSIGX.Window) Implements IMSIGX.IAppEvents.WindowResize

    End Sub
#End If
End Class