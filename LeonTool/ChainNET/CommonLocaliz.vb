Option Strict Off
Option Explicit On
Imports System.Runtime.InteropServices
Imports System.Reflection

Module CommonLocaliz
    'Private Declare Function LoadLangBSTRString Lib "LangExtVB.dll" (ByVal strModule As String, ByVal nID As Integer, ByVal wLanguage As Integer) As String

    <DllImportAttribute("LangExt.dll", EntryPoint:="LoadLangBSTRStringVBNET",
    SetLastError:=True, CharSet:=CharSet.Unicode,
    ExactSpelling:=True,
    CallingConvention:=CallingConvention.StdCall,
    PreserveSig:=True
    )>
    Public Function LoadLangBSTRStringVBNET(ByVal moduleName As String,
    ByVal id As Integer, ByVal wLanguage As Integer) As IntPtr
        ' Leave this function empty. The DLLImport attribute forces calls
        ' to LoadLangBSTRStringVBNET to be forwarded to LoadLangBSTRStringVBNET in LangExt.dllL.
    End Function



    Function TcLoadLangString(ByVal id As Integer) As String
        Dim strMod As String
        Dim pResult As IntPtr

        Dim strResult As String
        On Error GoTo DefLang

        strMod = Assembly.GetExecutingAssembly().Location

        strResult = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object id. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Couldn't resolve default property of object LoadLangBSTRString(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Couldn't resolve default property of object strResult. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        pResult = LoadLangBSTRStringVBNET(strMod, id, 0)

        strResult = Marshal.PtrToStringBSTR(pResult)
        Marshal.FreeBSTR(pResult)

        GoTo Ret
DefLang:
        On Error Resume Next
        'UPGRADE_WARNING: Couldn't resolve default property of object id. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Couldn't resolve default property of object strResult. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        strResult = My.Resources.ResourceManager.GetString("str" + CStr(id))
Ret:
        'UPGRADE_WARNING: Couldn't resolve default property of object strResult. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        TcLoadLangString = strResult
    End Function
End Module