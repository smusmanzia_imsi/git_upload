Option Strict Off
Option Explicit On
Module Module1
	
    Public objApp As IMSIGX.Application
	Public ActDr As IMSIGX.Drawing
	Public Grs As IMSIGX.Graphics
	Public Vi As IMSIGX.View
	Public Layer1 As IMSIGX.Layer
	Public GrSets As IMSIGX.GraphicSets
	Public UCSGr0 As IMSIGX.Matrix
	
	Public Const Pi As Double = 3.14159265358979
	Public Const Eps As Double = 0.000001
	Public GRAPHICTYPE As String
	Public ARCTYPE As String
	Public CIRCLETYPE As String

    Public oMissing As Object = System.Reflection.Missing.Value

    Public GXMD As New GXMDHELPERS

    Public Function MakeGraphic() As IMSIGX.Graphic
        '    Set MakeGraphic = New XGraphic 'Graphic - for Debug and XGraphic - for correct work
        'MakeGraphic = objApp.CreateObject("IMSIGX.Graphic.4") 'Graphic - for Debug and XGraphic - for correct work
        MakeGraphic = New IMSIGX.Graphic
    End Function
	Public Function MakeVertex() As IMSIGX.Vertex
		'    Set MakeVertex = New XVertex 'Vertex - for Debug and XVertex - for correct work
        'MakeVertex = objApp.CreateObject("IMISGX.Vertex.4") 'Vertex - for Debug and XVertex - for correct work
        MakeVertex = New IMSIGX.Vertex
    End Function
	'UPGRADE_NOTE: Main was upgraded to Main_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Sub Main_Renamed()
	End Sub
End Module