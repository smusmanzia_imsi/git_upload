Option Strict Off
Option Explicit On
Friend Class BiSplineToBezier
	
	Private Structure CurveBezSegment
		Dim xBeg As Double
		Dim yBeg As Double
		Dim xEnd As Double
		Dim yEnd As Double
		Dim xDirBeg As Double
		Dim yDirBeg As Double
		Dim xDirEnd As Double
		Dim yDirEnd As Double
		Dim dxBeg As Double
		Dim dyBeg As Double
		Dim ddxBeg As Double
		Dim ddyBeg As Double
		Dim dxEnd As Double
		Dim dyEnd As Double
		Dim ddxEnd As Double
		Dim ddyEnd As Double
		Dim jBeg As Integer
		Dim jEnd As Integer
	End Structure
	
	Private Structure CurveBezContour
		Dim NumSegm As Integer
		Dim Segm() As CurveBezSegment
	End Structure
	
	Private Structure BiSplinBasePoint
		Dim nPoints As Integer
		Dim xP() As Double
		Dim yP() As Double
		Dim dxP() As Double
		Dim dyP() As Double
		Dim ddxP() As Double
		Dim ddyP() As Double
	End Structure
	
	
	
	
	
	Public Function SplineToBezier(ByRef GrSpline As IMSIGX.Graphic) As IMSIGX.Graphic
		'UPGRADE_NOTE: Object SplineToBezier may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(SplineToBezier)
        Dim GrBez As IMSIGX.Graphic
		'UPGRADE_NOTE: Object GrBez may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        'GXMD.Release(GrBez)
        On Error GoTo ErrEnd
        'GXMD.Collect()


        If GrSpline.Type <> "TCW30CURVE" Then Exit Function
        If GrSpline.Properties.Item("$SPLINETYPE").Value <> 0 Then Exit Function

        Dim nV As Integer
        nV = GrSpline.Vertices.Count
        Dim xEnd, xBeg, yBeg, yEnd As Double
        With GrSpline.Vertices
            xBeg = .Item(0).X
            yBeg = .Item(0).Y
            xEnd = .Item(nV - 1).X
            yEnd = .Item(nV - 1).Y
        End With
        Dim ContClosed As Boolean
        ContClosed = False
        If System.Math.Abs(xBeg - xEnd) < Eps And System.Math.Abs(yBeg - yEnd) < Eps Then
            ContClosed = True
        End If

        'UPGRADE_WARNING: Arrays in structure BasePoint may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim BasePoint As BiSplinBasePoint
        BasePoint.nPoints = nV + 2
        ReDim BasePoint.xP(nV + 1)
        ReDim BasePoint.yP(nV + 1)
        ReDim BasePoint.dxP(nV + 1)
        ReDim BasePoint.dyP(nV + 1)
        ReDim BasePoint.ddxP(nV + 1)
        ReDim BasePoint.ddyP(nV + 1)
        Dim i As Integer
        Dim xi, yi As Double
        With GrSpline.Vertices
            For i = 0 To nV - 1
                xi = .Item(i).X
                yi = .Item(i).Y
                BasePoint.xP(i + 1) = xi
                BasePoint.yP(i + 1) = yi
            Next i
        End With

        If ContClosed = True Then
            BasePoint.xP(0) = BasePoint.xP(nV - 1)
            BasePoint.yP(0) = BasePoint.yP(nV - 1)
            BasePoint.xP(nV + 1) = BasePoint.xP(2)
            BasePoint.yP(nV + 1) = BasePoint.yP(2)
        Else
            BasePoint.xP(0) = 2 * BasePoint.xP(1) - BasePoint.xP(2)
            BasePoint.yP(0) = 2 * BasePoint.yP(1) - BasePoint.yP(2)
            BasePoint.xP(nV + 1) = 2 * BasePoint.xP(nV) - BasePoint.xP(nV - 1)
            BasePoint.yP(nV + 1) = 2 * BasePoint.yP(nV) - BasePoint.yP(nV - 1)
        End If

        Dim t0, t, dt, t1 As Double
        'Dim nDiv%
        '    nDiv = 10
        '    dt = 0.1
        Dim x2, x0, x1, x3 As Double
        Dim y2, y0, y1, y3 As Double
        Dim cx, ax, bx, dx As Double
        Dim cy, ay, by, dy As Double
        Dim dxP0, dyP0 As Double
        Dim ddxP0, ddyP0 As Double
        Dim dxP1, dyP1 As Double
        Dim ddxP1, ddyP1 As Double

        'Dim GrCosm As Graphic
        '    Set GrCosm = Grs.Add(11)
        '    GrCosm.Properties.Item("PenColor") = RGB(255, 0, 0)
        For i = 1 To nV - 1
            x0 = BasePoint.xP(i - 1)
            x1 = BasePoint.xP(i)
            x2 = BasePoint.xP(i + 1)
            x3 = BasePoint.xP(i + 2)
            y0 = BasePoint.yP(i - 1)
            y1 = BasePoint.yP(i)
            y2 = BasePoint.yP(i + 1)
            y3 = BasePoint.yP(i + 2)

            ax = (-x3 + 3 * x2 - 3 * x1 + x0) / 6
            bx = (x3 - 2 * x2 + x1) / 2
            cx = (-x3 + x1) / 2
            dx = (x3 + 4 * x2 + x1) / 6

            ay = (-y3 + 3 * y2 - 3 * y1 + y0) / 6
            by = (y3 - 2 * y2 + y1) / 2
            cy = (-y3 + y1) / 2
            dy = (y3 + 4 * y2 + y1) / 6

            t = 0
            t1 = 1 - t
            dxP0 = -3 * ax * t1 ^ 2 - 2 * bx * t1 - cx
            dyP0 = -3 * ay * t1 ^ 2 - 2 * by * t1 - cy
            ddxP0 = 6 * ax * t1 + 2 * bx
            ddyP0 = 6 * ay * t1 + 2 * by
            BasePoint.dxP(i) = dxP0
            BasePoint.dyP(i) = dyP0
            BasePoint.ddxP(i) = ddxP0
            BasePoint.ddyP(i) = ddyP0


            t = 1
            t1 = 1 - t
            dxP1 = -3 * ax * t1 ^ 2 - 2 * bx * t1 - cx
            dyP1 = -3 * ay * t1 ^ 2 - 2 * by * t1 - cy
            ddxP1 = 6 * ax * t1 + 2 * bx
            ddyP1 = 6 * ay * t1 + 2 * by
            If i = nV - 1 Then
                BasePoint.dxP(i + 1) = dxP1
                BasePoint.dyP(i + 1) = dyP1
                BasePoint.ddxP(i + 1) = ddxP1
                BasePoint.ddyP(i + 1) = ddyP1
            End If
        Next i

        'UPGRADE_WARNING: Arrays in structure ContBez may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContBez As CurveBezContour
        ContBez.NumSegm = nV - 1
        ReDim ContBez.Segm(nV - 1)
        Dim SegmBez As CurveBezSegment
        Dim xDirBeg, yDirBeg As Double
        Dim xDirEnd, yDirEnd As Double
        Dim dxBeg, dyBeg As Double
        Dim ddxBeg, ddyBeg As Double
        Dim dxEnd, dyEnd As Double
        Dim ddxEnd, ddyEnd As Double

        For i = 1 To nV - 1
            x0 = BasePoint.xP(i - 1)
            x1 = BasePoint.xP(i)
            x2 = BasePoint.xP(i + 1)
            x3 = BasePoint.xP(i + 2)
            y0 = BasePoint.yP(i - 1)
            y1 = BasePoint.yP(i)
            y2 = BasePoint.yP(i + 1)
            y3 = BasePoint.yP(i + 2)

            ax = (-x3 + 3 * x2 - 3 * x1 + x0) / 6
            bx = (x3 - 2 * x2 + x1) / 2
            cx = (-x3 + x1) / 2
            dx = (x3 + 4 * x2 + x1) / 6

            ay = (-y3 + 3 * y2 - 3 * y1 + y0) / 6
            by = (y3 - 2 * y2 + y1) / 2
            cy = (-y3 + y1) / 2
            dy = (y3 + 4 * y2 + y1) / 6

            t = 0
            t1 = 1 - t
            xBeg = ax * t1 ^ 3 + bx * t1 ^ 2 + cx * t1 + dx
            yBeg = ay * t1 ^ 3 + by * t1 ^ 2 + cy * t1 + dy

            t = 1
            t1 = 1 - t
            xEnd = ax * t1 ^ 3 + bx * t1 ^ 2 + cx * t1 + dx
            yEnd = ay * t1 ^ 3 + by * t1 ^ 2 + cy * t1 + dy

            dxBeg = BasePoint.dxP(i)
            dyBeg = BasePoint.dyP(i)
            dxEnd = BasePoint.dxP(i + 1)
            dyEnd = BasePoint.dyP(i + 1)
            ddxBeg = BasePoint.ddxP(i)
            ddyBeg = BasePoint.ddyP(i)
            ddxEnd = BasePoint.ddxP(i + 1)
            ddyEnd = BasePoint.ddyP(i + 1)

            With SegmBez
                .xBeg = xBeg
                .xEnd = xEnd
                .yBeg = yBeg
                .yEnd = yEnd
                .dxBeg = dxBeg
                .dyBeg = dyBeg
                .dxEnd = dxEnd
                .dyEnd = dyEnd
                .ddxBeg = ddxBeg
                .ddyBeg = ddyBeg
                .ddxEnd = ddxEnd
                .ddyEnd = ddyEnd
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object ContBez.Segm(i - 1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ContBez.Segm(i - 1) = SegmBez
        Next i

        ' Create Bezier
        xBeg = ContBez.Segm(0).xBeg
        yBeg = ContBez.Segm(0).yBeg
        GrBez = Grs.AddCurveBezier(xBeg, yBeg, 0)
        GrBez.Properties.Item("PenColor").Value = RGB(255, 0, 0)

        For i = 0 To ContBez.NumSegm - 1
            xEnd = ContBez.Segm(i).xEnd
            yEnd = ContBez.Segm(i).yEnd
            GrBez.Vertices.Add(xEnd, yEnd, 0)
        Next i

        GrBez.Draw()

        Dim iVBeg, iVEnd As Integer
        Dim iVDirBeg, iVDirEnd As Integer
        Dim Vers As IMSIGX.Vertices
        Vers = GrBez.Vertices
        ' Correct directional Bezier points
        For i = 0 To ContBez.NumSegm - 1
            iVBeg = 3 * i
            iVEnd = 3 * (i + 1)
            iVDirBeg = 3 * i + 1
            iVDirEnd = 3 * (i + 1) - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object SegmBez. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SegmBez = ContBez.Segm(i)
            With SegmBez
                xBeg = .xBeg
                xEnd = .xEnd
                yBeg = .yBeg
                yEnd = .yEnd
                dxBeg = .dxBeg
                dyBeg = .dyBeg
                dxEnd = .dxEnd
                dyEnd = .dyEnd
                ddxBeg = .ddxBeg
                ddyBeg = .ddyBeg
                ddxEnd = .ddxEnd
                ddyEnd = .ddyEnd
            End With

            xDirBeg = (dxBeg + 3 * xBeg) / 3
            yDirBeg = (dyBeg + 3 * yBeg) / 3

            xDirEnd = (-dxEnd + 3 * xEnd) / 3
            yDirEnd = (-dyEnd + 3 * yEnd) / 3

            Vers.Item(iVDirBeg).X = xDirBeg
            Vers.Item(iVDirBeg).Y = yDirBeg

            Vers.Item(iVDirEnd).X = xDirEnd
            Vers.Item(iVDirEnd).Y = yDirEnd
        Next i
        GrBez.Draw()
        SplineToBezier = GrBez
        Exit Function

ErrEnd:

        'UPGRADE_NOTE: Object SplineToBezier may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(SplineToBezier)
        If Not GrBez Is Nothing Then
            On Error Resume Next
            Grs.Remove(GrBez.Index)
            On Error Resume Next
            GrBez.Delete()
            'UPGRADE_NOTE: Object GrBez may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            'GXMD.Release(GrBez)
            GXMD.Release(GrBez)
        End If
        GXMD.Collect()

	End Function
End Class