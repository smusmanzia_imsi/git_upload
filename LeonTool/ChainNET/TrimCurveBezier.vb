Option Strict Off
Option Explicit On
Friend Class TrimCurveBezier
	
	Private Structure CurveBezSegment
		Dim xBeg As Double
		Dim yBeg As Double
		Dim xEnd As Double
		Dim yEnd As Double
		Dim xDirBeg As Double
		Dim yDirBeg As Double
		Dim xDirEnd As Double
		Dim yDirEnd As Double
		Dim dxBeg As Double
		Dim dyBeg As Double
		Dim ddxBeg As Double
		Dim ddyBeg As Double
		Dim dxEnd As Double
		Dim dyEnd As Double
		Dim ddxEnd As Double
		Dim ddyEnd As Double
		Dim jBeg As Integer
		Dim jEnd As Integer
	End Structure
	
	
	Private Structure CurveBezContour
		Dim NumSegm As Integer
		Dim Segm() As CurveBezSegment
	End Structure
	
	
	'UPGRADE_NOTE: Dir was upgraded to Dir_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Function TrimCurve(ByRef GrCurve As IMSIGX.Graphic, ByRef xP1 As Double, ByRef yP1 As Double, ByRef xP2 As Double, ByRef yP2 As Double, ByRef Dir_Renamed As Short) As IMSIGX.Graphic
		'UPGRADE_NOTE: Object TrimCurve may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(TrimCurve)
        GXMD.Collect()
        On Error GoTo ErrorEnd

        'UPGRADE_WARNING: Arrays in structure ContCosm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim ContCosm As New ChainTool.Contour
        Dim GrCurveDup As IMSIGX.Graphic
        GrCurveDup = GrCurve.Duplicate
        GrCurveDup.Properties.Item("PenWidth").Value = 0
        Grs.Remove(GrCurveDup.Index)
        Dim GrChild As IMSIGX.Graphic
        Dim ChildCount As Integer
        Dim i As Integer

        ChildCount = GrCurveDup.Graphics.Count
        For i = 0 To ChildCount - 1
            GrChild = GrCurveDup.Graphics.Item(i)
            If GrChild.Type = GRAPHICTYPE Then
                Exit For
            End If
        Next i

        If GrChild.Type <> GRAPHICTYPE Then
            'UPGRADE_NOTE: Object TrimCurve may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(TrimCurve)
            GXMD.Collect()
            GrCurveDup.Delete()
            'UPGRADE_NOTE: Object GrCurveDup may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrCurveDup)
            GXMD.Collect()
            Exit Function
        End If

        Dim Vers As IMSIGX.Vertices
        Dim nV As Integer
        Dim l, xE, xB, yB, yE, Alp As Double
        Vers = GrChild.Vertices
        Vers.UseWorldCS = False
        nV = Vers.Count
        'UPGRADE_WARNING: Arrays in structure Segm may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim Segm As New ChainTool.Segment
        ReDim ContCosm.Segm(nV - 1)
        ContCosm.NumSegm = 0

        For i = 0 To nV - 2
            xB = Vers.Item(i).X
            yB = Vers.Item(i).Y
            xE = Vers.Item(i + 1).X
            yE = Vers.Item(i + 1).Y
            Call LineSegmPar(xB, yB, xE, yE, l, Alp)
            With Segm
                .SegType = "Line" '# NLS#'
                .xyBeg.v0 = xB
                .xyBeg.v1 = yB
                .xyEnd.v0 = xE
                .xyEnd.v1 = yE
                .l = l
                .Alp.v0 = Alp
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object ContCosm.Segm(ContCosm.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ContCosm.Segm(ContCosm.NumSegm) = Segm
            ContCosm.NumSegm = ContCosm.NumSegm + 1
        Next i

        Dim xEnd, xBeg, yBeg, yEnd As Double
        Dim xDirEnd, xDirBeg, yDirBeg, yDirEnd As Double

        Dim ContClosed As Boolean
        xBeg = ContCosm.Segm(0).xyBeg.v0
        yBeg = ContCosm.Segm(0).xyBeg.v1
        xEnd = ContCosm.Segm(ContCosm.NumSegm - 1).xyEnd.v0
        yEnd = ContCosm.Segm(ContCosm.NumSegm - 1).xyEnd.v1
        ContClosed = False
        If System.Math.Abs(xBeg - xEnd) < Eps And System.Math.Abs(yBeg - yEnd) < Eps Then
            ContClosed = True
        End If
        If ContClosed = False Then
            Dir_Renamed = 1
        End If
        If ContClosed = True And System.Math.Abs(Dir_Renamed) <> 1 Then
            Dir_Renamed = 1
        End If

        If System.Math.Abs(xBeg - xP1) < Eps And System.Math.Abs(yBeg - yP1) < Eps And System.Math.Abs(xEnd - xP2) < Eps And System.Math.Abs(yEnd - yP2) < Eps Then
            GrCurveDup.Delete()
            GXMD.Release(GrCurveDup)
            GXMD.Collect()
            TrimCurve = GrCurve.Duplicate
            Exit Function
        End If
        If System.Math.Abs(xBeg - xP2) < Eps And System.Math.Abs(yBeg - yP2) < Eps And System.Math.Abs(xEnd - xP1) < Eps And System.Math.Abs(yEnd - yP1) < Eps Then
            GrCurveDup.Delete()
            GXMD.Release(GrCurveDup)
            GXMD.Collect()
            TrimCurve = GrCurve.Duplicate
            Exit Function
        End If

        If ContClosed = True And System.Math.Abs(xP1 - xP2) < Eps And System.Math.Abs(yP1 - yP2) < Eps Then
            GrCurveDup.Delete()
            GXMD.Release(GrCurveDup)
            GXMD.Collect()
            TrimCurve = GrCurve.Duplicate
            Exit Function
        End If

        ' Initialize Bezier contour
        'UPGRADE_WARNING: Arrays in structure CurvBez may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim CurvBez As New CurveBezContour
        Call InitializeBezierContour(GrCurveDup, ContCosm, CurvBez)
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ' Point 1
        ' Define number of Bezier segment on which lies point -(xP1,yP1)
        Dim iSegBez1 As Integer
        Dim iSegCosm1 As Integer
        Call DefNumBezierSegment(xP1, yP1, ContCosm, CurvBez, iSegBez1, iSegCosm1)
        ' Define value of t- parameter of Bezier segment for point -(xP1,yP1)
        Dim t1 As Double
        t1 = t_parameter(xP1, yP1, ContCosm, CurvBez, iSegBez1, iSegCosm1)
        ' Define parameters of bezier curve segment in point - P1
        Dim xP1New, yP1New As Double
        Dim dxP1, dyP1 As Double
        Call DefineCurvParAtPoint(CurvBez, iSegBez1, t1, xP1New, yP1New, dxP1, dyP1)

        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ' Point 2
        ' Define number of Bezier segment on which lies point -(xP2,yP2)
        Dim iSegBez2 As Integer
        Dim iSegCosm2 As Integer
        Call DefNumBezierSegment(xP2, yP2, ContCosm, CurvBez, iSegBez2, iSegCosm2)
        ' Define value of t- parameter of Bezier segment for point -(xP2,yP2)
        Dim t2 As Double
        t2 = t_parameter(xP2, yP2, ContCosm, CurvBez, iSegBez2, iSegCosm2)
        ' Define parameters of bezier curve segment in point - P2
        Dim xP2New, yP2New As Double
        Dim dxP2, dyP2 As Double
        Call DefineCurvParAtPoint(CurvBez, iSegBez2, t2, xP2New, yP2New, dxP2, dyP2)

        '####################################################################
        '####################################################################
        '####################################################################

        Dim GrCurvNew As IMSIGX.Graphic
        Dim t As Double
        Dim iVBeg As Integer
        Dim iVDirBeg As Integer
        Dim iVDirEnd As Integer
        Dim xBegNew, yBegNew As Double
        Dim xEndNew, yEndNew As Double
        Dim xDirBegNew, yDirBegNew As Double
        Dim xDirEndNew, yDirEndNew As Double
        Dim xP, yP As Double
        Dim dxP, dyP As Double
        If iSegBez1 <> iSegBez2 And Dir_Renamed = 1 Then
            If iSegBez1 > iSegBez2 Then

                i = iSegBez1
                iSegBez1 = iSegBez2
                iSegBez2 = i

                t = t1
                t1 = t2
                t2 = t

                xP = xP1New
                xP1New = xP2New
                xP2New = xP
                dxP = dxP1
                dxP1 = dxP2
                dxP2 = dxP

                yP = yP1New
                yP1New = yP2New
                yP2New = yP
                dyP = dyP1
                dyP1 = dyP2
                dyP2 = dyP

            End If


            xBeg = CurvBez.Segm(iSegBez1).xBeg
            yBeg = CurvBez.Segm(iSegBez1).yBeg
            GrCurvNew = Grs.AddCurveBezier(xBeg, yBeg, 0)
            GrCurvNew.Properties.Item("PenColor").Value = RGB(255, 0, 0)
            GrCurvNew.Properties.Item("PenWidth").Value = 0.02
            Vers = GrCurvNew.Vertices
            For i = iSegBez1 To iSegBez2
                xEnd = CurvBez.Segm(i).xEnd
                yEnd = CurvBez.Segm(i).yEnd
                Vers.Add(xEnd, yEnd, 0)
            Next i

            GrCurvNew.Draw()

            For i = iSegBez1 To iSegBez2
                With CurvBez.Segm(i)
                    xBeg = .xBeg
                    yBeg = .yBeg
                    xEnd = .xEnd
                    yEnd = .yEnd
                    xDirBeg = .xDirBeg
                    yDirBeg = .yDirBeg
                    xDirEnd = .xDirEnd
                    yDirEnd = .yDirEnd
                End With

                iVBeg = (i - iSegBez1) * 3
                iVDirBeg = iVBeg + 1
                iVDirEnd = iVBeg + 2
                Vers.Item(iVDirBeg).X = xDirBeg
                Vers.Item(iVDirBeg).Y = yDirBeg
                Vers.Item(iVDirEnd).X = xDirEnd
                Vers.Item(iVDirEnd).Y = yDirEnd
            Next i

            '+++++++++++++++++++++++++++++++++++++++++++++++
            ' Correct First bezier segment
            With CurvBez.Segm(iSegBez1)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd

                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With

            xBegNew = xP1New
            yBegNew = yP1New
            xEndNew = xEnd
            yEndNew = yEnd

            xDirBegNew = (dxP1 + 3 * xBegNew) / 3
            yDirBegNew = (dyP1 + 3 * yBegNew) / 3
            xDirEndNew = xDirEnd
            yDirEndNew = yDirEnd

            With GrCurvNew.Vertices
                .Item(0).x = xBegNew
                .Item(0).y = yBegNew
                .Item(1).x = xBegNew + (xDirBegNew - xBegNew) * (1 - t1)
                .Item(1).y = yBegNew + (yDirBegNew - yBegNew) * (1 - t1)
                .Item(2).x = xEndNew + (xDirEndNew - xEndNew) * (1 - t1)
                .Item(2).y = yEndNew + (yDirEndNew - yEndNew) * (1 - t1)
            End With
            '+++++++++++++++++++++++++++++++++++++++++++++++

            '+++++++++++++++++++++++++++++++++++++++++++++++
            ' Correct Last bezier segment
            With CurvBez.Segm(iSegBez2)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd

                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With

            xBegNew = xBeg
            yBegNew = yBeg
            xEndNew = xP2New
            yEndNew = yP2New

            xDirBegNew = xDirBeg
            yDirBegNew = yDirBeg
            xDirEndNew = (-dxP2 + 3 * xEndNew) / 3 ' -dxP   !!!!!
            yDirEndNew = (-dyP2 + 3 * yEndNew) / 3 ' -dyP   !!!!!

            nV = GrCurvNew.Vertices.Count
            With GrCurvNew.Vertices
                .Item(nV - 3).x = xBegNew + (xDirBegNew - xBegNew) * t2
                .Item(nV - 3).y = yBegNew + (yDirBegNew - yBegNew) * t2
                .Item(nV - 2).x = xEndNew + (xDirEndNew - xEndNew) * t2
                .Item(nV - 2).y = yEndNew + (yDirEndNew - yEndNew) * t2
                .Item(nV - 1).x = xEndNew
                .Item(nV - 1).y = yEndNew
            End With
            GrCurvNew.Draw()
            '+++++++++++++++++++++++++++++++++++++++++++++++
        End If ' If iSegBez1 <> iSegBez2 And Dir = 1
        '############################################################
        '############################################################
        '############################################################
        '############################################################
        If iSegBez1 <> iSegBez2 And Dir_Renamed = -1 Then
            If iSegBez1 < iSegBez2 Then

                i = iSegBez2
                iSegBez2 = iSegBez1
                iSegBez1 = i

                t = t2
                t2 = t1
                t1 = t

                xP = xP2New
                xP2New = xP1New
                xP1New = xP
                dxP = dxP2
                dxP2 = dxP1
                dxP1 = dxP

                yP = yP2New
                yP2New = yP1New
                yP1New = yP
                dyP = dyP2
                dyP2 = dyP1
                dyP1 = dyP

            End If


            xBeg = CurvBez.Segm(iSegBez1).xBeg
            yBeg = CurvBez.Segm(iSegBez1).yBeg
            GrCurvNew = Grs.AddCurveBezier(xBeg, yBeg, 0)
            GrCurvNew.Properties.Item("PenColor").Value = RGB(255, 0, 0)
            GrCurvNew.Properties.Item("PenWidth").Value = 0.02
            Vers = GrCurvNew.Vertices
            For i = iSegBez1 To CurvBez.NumSegm - 1
                xEnd = CurvBez.Segm(i).xEnd
                yEnd = CurvBez.Segm(i).yEnd
                Vers.Add(xEnd, yEnd, 0)
            Next i
            For i = 0 To iSegBez2
                xEnd = CurvBez.Segm(i).xEnd
                yEnd = CurvBez.Segm(i).yEnd
                Vers.Add(xEnd, yEnd, 0)
            Next i

            GrCurvNew.Draw()

            For i = iSegBez1 To CurvBez.NumSegm - 1
                With CurvBez.Segm(i)
                    xBeg = .xBeg
                    yBeg = .yBeg
                    xEnd = .xEnd
                    yEnd = .yEnd
                    xDirBeg = .xDirBeg
                    yDirBeg = .yDirBeg
                    xDirEnd = .xDirEnd
                    yDirEnd = .yDirEnd
                End With

                iVBeg = (i - iSegBez1) * 3
                iVDirBeg = iVBeg + 1
                iVDirEnd = iVBeg + 2
                Vers.Item(iVDirBeg).X = xDirBeg
                Vers.Item(iVDirBeg).Y = yDirBeg
                Vers.Item(iVDirEnd).X = xDirEnd
                Vers.Item(iVDirEnd).Y = yDirEnd
            Next i

            For i = 0 To iSegBez2
                With CurvBez.Segm(i)
                    xBeg = .xBeg
                    yBeg = .yBeg
                    xEnd = .xEnd
                    yEnd = .yEnd
                    xDirBeg = .xDirBeg
                    yDirBeg = .yDirBeg
                    xDirEnd = .xDirEnd
                    yDirEnd = .yDirEnd
                End With

                iVBeg = (i + (CurvBez.NumSegm - iSegBez1)) * 3
                iVDirBeg = iVBeg + 1
                iVDirEnd = iVBeg + 2
                Vers.Item(iVDirBeg).x = xDirBeg
                Vers.Item(iVDirBeg).y = yDirBeg
                Vers.Item(iVDirEnd).x = xDirEnd
                Vers.Item(iVDirEnd).y = yDirEnd
            Next i

            '+++++++++++++++++++++++++++++++++++++++++++++++
            ' Correct First bezier segment
            With CurvBez.Segm(iSegBez1)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd

                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With

            xBegNew = xP1New
            yBegNew = yP1New
            xEndNew = xEnd
            yEndNew = yEnd

            xDirBegNew = (dxP1 + 3 * xBegNew) / 3
            yDirBegNew = (dyP1 + 3 * yBegNew) / 3
            xDirEndNew = xDirEnd
            yDirEndNew = yDirEnd

            With GrCurvNew.Vertices
                .Item(0).x = xBegNew
                .Item(0).y = yBegNew
                .Item(1).x = xBegNew + (xDirBegNew - xBegNew) * (1 - t1)
                .Item(1).y = yBegNew + (yDirBegNew - yBegNew) * (1 - t1)
                .Item(2).x = xEndNew + (xDirEndNew - xEndNew) * (1 - t1)
                .Item(2).y = yEndNew + (yDirEndNew - yEndNew) * (1 - t1)
            End With
            '+++++++++++++++++++++++++++++++++++++++++++++++

            '+++++++++++++++++++++++++++++++++++++++++++++++
            ' Correct Last bezier segment
            With CurvBez.Segm(iSegBez2)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd

                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With

            xBegNew = xBeg
            yBegNew = yBeg
            xEndNew = xP2New
            yEndNew = yP2New

            xDirBegNew = xDirBeg
            yDirBegNew = yDirBeg
            xDirEndNew = (-dxP2 + 3 * xEndNew) / 3 ' -dxP   !!!!!
            yDirEndNew = (-dyP2 + 3 * yEndNew) / 3 ' -dyP   !!!!!

            nV = GrCurvNew.Vertices.Count
            With GrCurvNew.Vertices
                .Item(nV - 3).x = xBegNew + (xDirBegNew - xBegNew) * t2
                .Item(nV - 3).y = yBegNew + (yDirBegNew - yBegNew) * t2
                .Item(nV - 2).x = xEndNew + (xDirEndNew - xEndNew) * t2
                .Item(nV - 2).y = yEndNew + (yDirEndNew - yEndNew) * t2
                .Item(nV - 1).x = xEndNew
                .Item(nV - 1).y = yEndNew
            End With
            GrCurvNew.Draw()
            '+++++++++++++++++++++++++++++++++++++++++++++++
        End If ' If iSegBez1 <> iSegBez2 And Dir = -1

        '############################################################
        '############################################################
        '############################################################
        '############################################################
        If iSegBez1 = iSegBez2 And Dir_Renamed <> -1 Then
            If t1 < t2 Then
            Else

                i = iSegBez1
                iSegBez1 = iSegBez2
                iSegBez2 = i

                t = t1
                t1 = t2
                t2 = t

                xP = xP1New
                xP1New = xP2New
                xP2New = xP
                dxP = dxP1
                dxP1 = dxP2
                dxP2 = dxP

                yP = yP1New
                yP1New = yP2New
                yP2New = yP
                dyP = dyP1
                dyP1 = dyP2
                dyP2 = dyP

            End If

            xBeg = CurvBez.Segm(iSegBez1).xBeg
            yBeg = CurvBez.Segm(iSegBez1).yBeg
            GrCurvNew = Grs.AddCurveBezier(xBeg, yBeg, 0)
            GrCurvNew.Properties.Item("PenColor").Value = RGB(255, 0, 0)
            GrCurvNew.Properties.Item("PenWidth").Value = 0.02
            Vers = GrCurvNew.Vertices
            For i = iSegBez1 To iSegBez2
                xEnd = CurvBez.Segm(i).xEnd
                yEnd = CurvBez.Segm(i).yEnd
                Vers.Add(xEnd, yEnd, 0)
            Next i

            GrCurvNew.Draw()

            For i = iSegBez1 To iSegBez2
                With CurvBez.Segm(i)
                    xBeg = .xBeg
                    yBeg = .yBeg
                    xEnd = .xEnd
                    yEnd = .yEnd
                    xDirBeg = .xDirBeg
                    yDirBeg = .yDirBeg
                    xDirEnd = .xDirEnd
                    yDirEnd = .yDirEnd
                End With

                iVBeg = (i - iSegBez1) * 3
                iVDirBeg = iVBeg + 1
                iVDirEnd = iVBeg + 2
                Vers.Item(iVDirBeg).x = xDirBeg
                Vers.Item(iVDirBeg).y = yDirBeg
                Vers.Item(iVDirEnd).x = xDirEnd
                Vers.Item(iVDirEnd).y = yDirEnd
            Next i

            '+++++++++++++++++++++++++++++++++++++++++++++++
            ' Correct First bezier segment
            With CurvBez.Segm(iSegBez1)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd

                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With

            xBegNew = xP1New
            yBegNew = yP1New
            xEndNew = xEnd
            yEndNew = yEnd

            xDirBegNew = (dxP1 + 3 * xBegNew) / 3
            yDirBegNew = (dyP1 + 3 * yBegNew) / 3
            xDirEndNew = xDirEnd
            yDirEndNew = yDirEnd

            With GrCurvNew.Vertices
                .Item(0).x = xBegNew
                .Item(0).y = yBegNew
                .Item(1).x = xBegNew + (xDirBegNew - xBegNew) * (1 - t1)
                .Item(1).y = yBegNew + (yDirBegNew - yBegNew) * (1 - t1)
                .Item(2).x = xEndNew + (xDirEndNew - xEndNew) * (1 - t1)
                .Item(2).y = yEndNew + (yDirEndNew - yEndNew) * (1 - t1)
            End With
            '+++++++++++++++++++++++++++++++++++++++++++++++

            '+++++++++++++++++++++++++++++++++++++++++++++++
            ' Correct Last bezier segment
            With CurvBez.Segm(iSegBez2)
                xBeg = xBegNew
                yBeg = yBegNew
                xEnd = .xEnd
                yEnd = .yEnd

                xDirBeg = xDirBegNew
                yDirBeg = yDirBegNew
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With

            xBegNew = xBeg
            yBegNew = yBeg
            xEndNew = xP2New
            yEndNew = yP2New

            xDirBegNew = xDirBeg
            yDirBegNew = yDirBeg
            xDirEndNew = (-dxP2 + 3 * xEndNew) / 3 ' -dxP   !!!!!
            yDirEndNew = (-dyP2 + 3 * yEndNew) / 3 ' -dyP   !!!!!

            nV = GrCurvNew.Vertices.Count
            With GrCurvNew.Vertices
                .Item(nV - 3).x = xBegNew + (xDirBegNew - xBegNew) * (t2 - t1)
                .Item(nV - 3).y = yBegNew + (yDirBegNew - yBegNew) * (t2 - t1)
                .Item(nV - 2).x = xEndNew + (xDirEndNew - xEndNew) * (t2 - t1)
                .Item(nV - 2).y = yEndNew + (yDirEndNew - yEndNew) * (t2 - t1)
                .Item(nV - 1).x = xEndNew
                .Item(nV - 1).y = yEndNew
            End With
            GrCurvNew.Draw()
            '+++++++++++++++++++++++++++++++++++++++++++++++
        End If ' If iSegBez1 = iSegBez2 And dir<>-1

        '############################################################
        '############################################################
        '############################################################
        '############################################################
        If iSegBez1 = iSegBez2 And Dir_Renamed = -1 Then
            If t2 < t1 Then
            Else

                i = iSegBez1
                iSegBez1 = iSegBez2
                iSegBez2 = i

                t = t1
                t1 = t2
                t2 = t

                xP = xP1New
                xP1New = xP2New
                xP2New = xP
                dxP = dxP1
                dxP1 = dxP2
                dxP2 = dxP

                yP = yP1New
                yP1New = yP2New
                yP2New = yP
                dyP = dyP1
                dyP1 = dyP2
                dyP2 = dyP

            End If

            xBeg = CurvBez.Segm(iSegBez1).xBeg
            yBeg = CurvBez.Segm(iSegBez1).yBeg
            GrCurvNew = Grs.AddCurveBezier(xBeg, yBeg, 0)
            GrCurvNew.Properties.Item("PenColor").Value = RGB(255, 0, 0)
            GrCurvNew.Properties.Item("PenWidth").Value = 0.02
            Vers = GrCurvNew.Vertices
            For i = iSegBez1 To CurvBez.NumSegm - 1
                xEnd = CurvBez.Segm(i).xEnd
                yEnd = CurvBez.Segm(i).yEnd
                Vers.Add(xEnd, yEnd, 0)
            Next i

            ' Add Last segments

            For i = 0 To iSegBez2
                xEnd = CurvBez.Segm(i).xEnd
                yEnd = CurvBez.Segm(i).yEnd
                Vers.Add(xEnd, yEnd, 0)
            Next i

            GrCurvNew.Draw()


            For i = iSegBez1 To CurvBez.NumSegm - 1
                With CurvBez.Segm(i)
                    xBeg = .xBeg
                    yBeg = .yBeg
                    xEnd = .xEnd
                    yEnd = .yEnd
                    xDirBeg = .xDirBeg
                    yDirBeg = .yDirBeg
                    xDirEnd = .xDirEnd
                    yDirEnd = .yDirEnd
                End With

                iVBeg = (i - iSegBez1) * 3
                iVDirBeg = iVBeg + 1
                iVDirEnd = iVBeg + 2
                Vers.Item(iVDirBeg).x = xDirBeg
                Vers.Item(iVDirBeg).y = yDirBeg
                Vers.Item(iVDirEnd).x = xDirEnd
                Vers.Item(iVDirEnd).y = yDirEnd
            Next i

            '        GrCurvNew.Draw

            For i = 0 To iSegBez2
                With CurvBez.Segm(i)
                    xBeg = .xBeg
                    yBeg = .yBeg
                    xEnd = .xEnd
                    yEnd = .yEnd
                    xDirBeg = .xDirBeg
                    yDirBeg = .yDirBeg
                    xDirEnd = .xDirEnd
                    yDirEnd = .yDirEnd
                End With
                iVBeg = (i + CurvBez.NumSegm - iSegBez2) * 3
                iVDirBeg = iVBeg + 1
                iVDirEnd = iVBeg + 2
                Vers.Item(iVDirBeg).x = xDirBeg
                Vers.Item(iVDirBeg).y = yDirBeg
                Vers.Item(iVDirEnd).x = xDirEnd
                Vers.Item(iVDirEnd).y = yDirEnd
            Next i
            '       GrCurvNew.Draw

            '+++++++++++++++++++++++++++++++++++++++++++++++
            ' Correct First bezier segment
            With CurvBez.Segm(iSegBez1)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd

                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With

            xBegNew = xP1New
            yBegNew = yP1New
            xEndNew = xEnd
            yEndNew = yEnd

            xDirBegNew = (dxP1 + 3 * xBegNew) / 3
            yDirBegNew = (dyP1 + 3 * yBegNew) / 3
            xDirEndNew = xDirEnd
            yDirEndNew = yDirEnd

            With GrCurvNew.Vertices
                .Item(0).x = xBegNew
                .Item(0).y = yBegNew
                .Item(1).x = xBegNew + (xDirBegNew - xBegNew) * (1 - t1)
                .Item(1).y = yBegNew + (yDirBegNew - yBegNew) * (1 - t1)
                .Item(2).x = xEndNew + (xDirEndNew - xEndNew) * (1 - t1)
                .Item(2).y = yEndNew + (yDirEndNew - yEndNew) * (1 - t1)
            End With
            '+++++++++++++++++++++++++++++++++++++++++++++++

            '+++++++++++++++++++++++++++++++++++++++++++++++
            ' Correct Last bezier segment
            With CurvBez.Segm(iSegBez2)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd

                xDirBeg = .xDirBeg
                yDirBeg = .yDirBeg
                xDirEnd = .xDirEnd
                yDirEnd = .yDirEnd
            End With

            xBegNew = xBeg
            yBegNew = yBeg
            xEndNew = xP2New
            yEndNew = yP2New

            xDirBegNew = xDirBeg
            yDirBegNew = yDirBeg
            xDirEndNew = (-dxP2 + 3 * xEndNew) / 3 ' -dxP   !!!!!
            yDirEndNew = (-dyP2 + 3 * yEndNew) / 3 ' -dyP   !!!!!

            nV = GrCurvNew.Vertices.Count
            With GrCurvNew.Vertices
                .Item(nV - 3).x = xBegNew + (xDirBegNew - xBegNew) * t2
                .Item(nV - 3).y = yBegNew + (yDirBegNew - yBegNew) * t2
                .Item(nV - 2).x = xEndNew + (xDirEndNew - xEndNew) * t2
                .Item(nV - 2).y = yEndNew + (yDirEndNew - yEndNew) * t2
                .Item(nV - 1).x = xEndNew
                .Item(nV - 1).y = yEndNew
            End With
            GrCurvNew.Draw()
            '+++++++++++++++++++++++++++++++++++++++++++++++
RRR:
        End If ' If iSegBez1 = iSegBez2 And dir=-1


        TrimCurve = GrCurvNew

        If Not GrCurveDup Is Nothing Then
            GrCurveDup.Delete()
        End If
        'UPGRADE_NOTE: Object GrCurveDup may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrCurveDup)
        GXMD.Collect()

        Exit Function
ErrorEnd:

        'UPGRADE_NOTE: Object TrimCurve may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(TrimCurve)
        GXMD.Collect()
        If Not GrCurveDup Is Nothing Then
            GrCurveDup.Delete()
        End If
        'UPGRADE_NOTE: Object GrCurveDup may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        GXMD.Release(GrCurveDup)
        GXMD.Collect()

        If Not GrCurvNew Is Nothing Then
            On Error Resume Next
            Grs.Remove(GrCurvNew.Index)
            On Error Resume Next
            GrCurvNew.Delete()
            'UPGRADE_NOTE: Object GrCurvNew may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            GXMD.Release(GrCurvNew)
            GXMD.Collect()
        End If

    End Function



    ' Define the length and angle of line segment
    Private Sub LineSegmPar(ByRef x0 As Double, ByRef y0 As Double, ByRef x1 As Double, ByRef y1 As Double, ByRef l As Double, ByRef Alp As Double)
        Dim sina, cosa As Double
        l = System.Math.Sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0))
        If System.Math.Abs(l) < Eps Then
            l = 0
            Alp = 0
            Exit Sub
        End If
        sina = (y1 - y0) / l
        cosa = (x1 - x0) / l
        Alp = Angle(sina, cosa)
    End Sub

    Private Function Angle(ByRef sinB As Double, ByRef cosB As Double) As Double
        Dim Eps1 As Double
        Eps1 = 0.0000000000001
        If System.Math.Abs(cosB) < Eps1 Then
            If sinB > Eps1 Then
                Angle = Pi / 2
            Else
                Angle = 3 * Pi / 2
            End If
        Else
            If System.Math.Abs(sinB) < Eps1 Then
                If cosB > Eps1 Then
                    Angle = 0
                Else
                    Angle = Pi
                End If
            Else
                If sinB >= 0 And cosB > 0 Then
                    Angle = System.Math.Atan(sinB / cosB)
                Else
                    If sinB >= 0 And cosB < 0 Then
                        Angle = Pi + System.Math.Atan(sinB / cosB)
                    Else
                        If sinB < 0 And cosB < 0 Then
                            Angle = Pi + System.Math.Atan(sinB / cosB)
                        Else
                            If sinB < 0 And cosB > 0 Then
                                Angle = 2 * Pi + System.Math.Atan(sinB / cosB)
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Function


    Private Function Sign(ByRef y As Double) As Double
        If System.Math.Abs(y) < 0.000001 Then
            Sign = 0
            Exit Function
        End If
        If y < 0 Then
            Sign = -1.0#
            Exit Function
        End If
        If y > 0 Then
            Sign = 1.0#
            Exit Function
        End If
    End Function



    Private Sub InitializeBezierContour(ByRef GrBezier As IMSIGX.Graphic, ByRef ContCosm As ChainTool.Contour, ByRef ContCurvBez As CurveBezContour)
        'Begin Initialize Bezier contour
        Dim i As Integer
        Dim nV As Integer
        Dim Vers As IMSIGX.Vertices
        Vers = GrBezier.Vertices
        nV = Vers.Count
        Dim nBase As Integer
        nBase = (nV + 2) / 3

        ReDim ContCurvBez.Segm(nBase)
        ContCurvBez.NumSegm = 0
        Dim iV, iVNext As Integer
        Dim iDirB, iDirE As Integer
        Dim xEnd, xBeg, yBeg, yEnd As Double
        Dim xDirEnd, xDirBeg, yDirBeg, yDirEnd As Double
        Dim dxEnd, dxBeg, dyBeg, dyEnd As Double
        Dim ddxEnd, ddxBeg, ddyBeg, ddyEnd As Double
        Dim Segm As CurveBezSegment

        For i = 0 To nBase - 2
            iV = i * 3
            iVNext = (i + 1) * 3
            xBeg = Vers.Item(iV).x
            yBeg = Vers.Item(iV).y
            xEnd = Vers.Item(iVNext).x
            yEnd = Vers.Item(iVNext).y

            iDirB = iV + 1
            xDirBeg = Vers.Item(iDirB).x
            yDirBeg = Vers.Item(iDirB).y
            iDirE = iVNext - 1
            xDirEnd = Vers.Item(iDirE).x
            yDirEnd = Vers.Item(iDirE).y

            dxBeg = -3 * xBeg + 3 * xDirBeg
            dyBeg = -3 * yBeg + 3 * yDirBeg

            ddxBeg = 6 * xBeg - 12 * xDirBeg + 6 * xDirEnd
            ddyBeg = 6 * yBeg - 12 * yDirBeg + 6 * yDirEnd


            dxEnd = -3 * xDirEnd + 3 * xEnd
            dyEnd = -3 * yDirEnd + 3 * yEnd

            ddxEnd = 6 * xDirBeg - 12 * xDirEnd + 6 * xEnd
            ddyEnd = 6 * yDirBeg - 12 * yDirEnd + 6 * yEnd

            With Segm
                .xBeg = xBeg
                .yBeg = yBeg
                .xEnd = xEnd
                .yEnd = yEnd

                .xDirBeg = xDirBeg
                .yDirBeg = yDirBeg
                .xDirEnd = xDirEnd
                .yDirEnd = yDirEnd

                .dxBeg = dxBeg
                .dyBeg = dyBeg
                .dxEnd = dxEnd
                .dyEnd = dyEnd

                .ddxBeg = ddxBeg
                .ddyBeg = ddyBeg
                .ddxEnd = ddxEnd
                .ddyEnd = ddyEnd
            End With

            'UPGRADE_WARNING: Couldn't resolve default property of object ContCurvBez.Segm(ContCurvBez.NumSegm). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ContCurvBez.Segm(ContCurvBez.NumSegm) = Segm
            ContCurvBez.NumSegm = ContCurvBez.NumSegm + 1

        Next i

        Dim xE, xB, yB, yE As Double
        Dim j As Integer
        'Dim j0 As Integer
        Dim jBeg, jEnd As Integer
        For i = 0 To ContCurvBez.NumSegm - 1
            With ContCurvBez.Segm(i)
                xBeg = .xBeg
                yBeg = .yBeg
                xEnd = .xEnd
                yEnd = .yEnd
            End With
            jBeg = -1
            jEnd = -1
            For j = 0 To ContCosm.NumSegm - 1
                With ContCosm.Segm(j)
                    xB = .xyBeg.v0
                    yB = .xyBeg.v1
                End With
                If System.Math.Abs(xBeg - xB) < Eps And System.Math.Abs(yBeg - yB) < Eps Then
                    jBeg = j
                    Exit For
                End If
            Next j
            ContCurvBez.Segm(i).jBeg = jBeg
            If jBeg = -1 Then jBeg = 0
            For j = jBeg To ContCosm.NumSegm - 1
                With ContCosm.Segm(j)
                    xE = .xyEnd.v0
                    yE = .xyEnd.v1
                End With
                If System.Math.Abs(xEnd - xE) < Eps And System.Math.Abs(yEnd - yE) < Eps Then
                    jEnd = j
                    Exit For
                End If
            Next j
            ContCurvBez.Segm(i).jEnd = jEnd
        Next i

        'End Initialize Bezier contour
        '#############################################################
        '#############################################################

    End Sub
	
	
	
	' Define number of Bezier segment on which lies point -(xP,yP)
	Private Sub DefNumBezierSegment(ByRef xP As Double, ByRef yP As Double, ByRef ContCosm As ChainTool.Contour, ByRef ContCurvBez As CurveBezContour, ByRef iSegBez As Integer, ByRef iSegCosm As Integer)
		Dim i As Integer
		Dim xE, xB, yB, yE As Double
		Dim l, Alp As Double
		' Define number of line segment on which lies point -(xP,yP)
		Dim xLoc, yLoc As Double
		Dim yLocMin As Double
		yLocMin = 10000000
		iSegCosm = -1
		For i = 0 To ContCosm.NumSegm - 1
			With ContCosm.Segm(i)
				xB = .xyBeg.v0
				yB = .xyBeg.v1
				xE = .xyEnd.v0
				yE = .xyEnd.v1
				l = .l
				Alp = .Alp.v0
			End With
			xLoc = (yP - yB) * System.Math.Sin(Alp) + (xP - xB) * System.Math.Cos(Alp)
			yLoc = (yP - yB) * System.Math.Cos(Alp) - (xP - xB) * System.Math.Sin(Alp)
			If xLoc > -Eps And xLoc < l + Eps Then
				If System.Math.Abs(yLoc) < yLocMin Then
					yLocMin = System.Math.Abs(yLoc)
					iSegCosm = i
				End If
			End If
		Next i
		
		' Define number of Bezier segment on which lies point -(xP,yP)
		Dim jBeg, jEnd As Integer
		iSegBez = -1
		For i = 0 To ContCurvBez.NumSegm - 1
			With ContCurvBez.Segm(i)
				jBeg = .jBeg
				jEnd = .jEnd
			End With
			If iSegCosm >= jBeg And iSegCosm <= jEnd Then
				iSegBez = i
				Exit For
			End If
		Next i
		
	End Sub
	
	
	
	' Define value of t- parameter of Bezier segment for point -(xP,yP)
	
	Private Function t_parameter(ByRef xP As Double, ByRef yP As Double, ByRef ContCosm As ChainTool.Contour, ByRef ContCurvBez As CurveBezContour, ByRef iSegBez As Integer, ByRef iSegCosm As Integer) As Double
		
		Dim t As Double
		Dim dj0 As Integer
		Dim dt0 As Double
		Dim tSeg, dtSeg As Double
		Dim xLoc, yLoc As Double
		Dim jBeg, jEnd As Integer
		Dim l, xE, xB, yB, yE, Alp As Double
		
		With ContCurvBez.Segm(iSegBez)
			jBeg = .jBeg
			jEnd = .jEnd
		End With
		
		dj0 = jEnd - jBeg + 1
		dt0 = 1 / dj0
		
		tSeg = (iSegCosm - jBeg) * dt0
		
		With ContCosm.Segm(iSegCosm)
			xB = .xyBeg.v0
			yB = .xyBeg.v1
			xE = .xyEnd.v0
			yE = .xyEnd.v1
			l = .l
			Alp = .Alp.v0
		End With
		xLoc = (yP - yB) * System.Math.Sin(Alp) + (xP - xB) * System.Math.Cos(Alp)
		yLoc = (yP - yB) * System.Math.Cos(Alp) - (xP - xB) * System.Math.Sin(Alp)
		
		dtSeg = xLoc / l * dt0
		
		t = tSeg + dtSeg
		
		t_parameter = t
		
	End Function
	
	Private Sub DefineCurvParAtPoint(ByRef ContCurvBez As CurveBezContour, ByRef iSegBez As Integer, ByRef t As Double, ByRef xP As Double, ByRef yP As Double, ByRef dxP As Double, ByRef dyP As Double)
		
		Dim xEnd, xBeg, yBeg, yEnd As Double
		Dim xDirEnd, xDirBeg, yDirBeg, yDirEnd As Double
		
		With ContCurvBez.Segm(iSegBez)
			xBeg = .xBeg
			yBeg = .yBeg
			xEnd = .xEnd
			yEnd = .yEnd
			
			xDirBeg = .xDirBeg
			yDirBeg = .yDirBeg
			xDirEnd = .xDirEnd
			yDirEnd = .yDirEnd
		End With
		
		xP = (1 - t) ^ 3 * xBeg + 3 * t * (1 - t) ^ 2 * xDirBeg + 3 * t ^ 2 * (1 - t) * xDirEnd + t ^ 3 * xEnd
		yP = (1 - t) ^ 3 * yBeg + 3 * t * (1 - t) ^ 2 * yDirBeg + 3 * t ^ 2 * (1 - t) * yDirEnd + t ^ 3 * yEnd
		
		dxP = -3 * (1 - t) ^ 2 * xBeg + 3 * (1 - t) * (1 - 3 * t) * xDirBeg + 3 * t * (2 - 3 * t) * xDirEnd + 3 * t ^ 2 * xEnd
		dyP = -3 * (1 - t) ^ 2 * yBeg + 3 * (1 - t) * (1 - 3 * t) * yDirBeg + 3 * t * (2 - 3 * t) * yDirEnd + 3 * t ^ 2 * yEnd
		
		'    ddxP = 6 * (1 - t) * xBeg + 6 * (-2 + 3 * t) * xDirBeg + 6 * (1 - 3 * t) * xDirEnd + 6 * t * xEnd
		'    ddyP = 6 * (1 - t) * yBeg + 6 * (-2 + 3 * t) * yDirBeg + 6 * (1 - 3 * t) * yDirEnd + 6 * t * yEnd
		
	End Sub
End Class