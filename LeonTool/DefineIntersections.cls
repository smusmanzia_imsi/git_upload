VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DefineIntersections"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit




Public Function RunIntersect(Cont1 As Contour, Cont2 As Contour, Inter As IntersTwoCont) As Boolean
Dim IntCount As Long
    
    Call CalcIntersection(Cont1, Cont2, Inter, IntCount)
    If IntCount < 1 Then
        RunIntersect = False
    Else
        RunIntersect = True
    End If
End Function

   
' Calculate the pointintersections
   
Private Sub CalcIntersection(Cont1 As Contour, Cont2 As Contour, Inter As IntersTwoCont, IntCount As Long)

Dim Type1$, xB1#, yB1#, xE1#, yE1#, xMid1#, yMid1#, xc1#, yc1#, L1#, fi1#, R1#, Rot1%
Dim Type2$, xB2#, yB2#, xE2#, yE2#, xMid2#, yMid2#, xc2#, yc2#, L2#, fi2#, R2#, Rot2%
Dim i As Long, j As Long
Dim xInt1#, yInt1#, xInt2#, yInt2#
Dim Type1P$, xB1P#, yB1P#, xE1P#, yE1P#, xMid1P#, yMid1P#, xc1P#, yc1P#, L1P#, fi1P#, R1P#, Rot1P%
Dim Type1N$, xB1N#, yB1N#, xE1N#, yE1N#, xMid1N#, yMid1N#, xc1N#, yc1N#, L1N#, fi1N#, R1N#, Rot1N%
Dim xMax1#, yMax1#, xMin1#, yMin1#
    IntCount = 0
    
    For i = 0 To Cont1.NumSegm - 1
        With Cont1.Segm(i)
            Type1 = .SegType
            xB1 = .xyBeg(0)
            yB1 = .xyBeg(1)
            xE1 = .xyEnd(0)
            yE1 = .xyEnd(1)
            L1 = .L
            fi1 = .Alp(0)
            xc1 = .xyCent(0)
            yc1 = .xyCent(1)
            xMid1 = .xyMid(0)
            yMid1 = .xyMid(1)
            R1 = .R
            Rot1 = .Rot
        End With
        
        If Type1 = "Line" Then
            If xB1 > xE1 Then
                xMax1 = xB1
                xMin1 = xE1
            Else
                xMax1 = xE1
                xMin1 = xB1
            End If
            If yB1 > yE1 Then
                yMax1 = yB1
                yMin1 = yE1
            Else
                yMax1 = yE1
                yMin1 = yB1
            End If
        End If
        
        Type1P = ""
        If i > 0 Then
            With Cont1.Segm(i - 1)
                Type1P = .SegType
                xB1P = .xyBeg(0)
                yB1P = .xyBeg(1)
                xE1P = .xyEnd(0)
                yE1P = .xyEnd(1)
                L1P = .L
                fi1P = .Alp(0)
                xc1P = .xyCent(0)
                yc1P = .xyCent(1)
                xMid1P = .xyMid(0)
                yMid1P = .xyMid(1)
                R1P = .R
                Rot1P = .Rot
            End With
        End If
        
        Type1N = ""
        If i < Cont1.NumSegm - 1 Then
            With Cont1.Segm(i + 1)
                Type1N = .SegType
                xB1N = .xyBeg(0)
                yB1N = .xyBeg(1)
                xE1N = .xyEnd(0)
                yE1N = .xyEnd(1)
                L1N = .L
                fi1N = .Alp(0)
                xc1N = .xyCent(0)
                yc1N = .xyCent(1)
                xMid1N = .xyMid(0)
                yMid1N = .xyMid(1)
                R1N = .R
                Rot1N = .Rot
            End With
        End If
        
        For j = 0 To Cont2.NumSegm - 1
            With Cont2.Segm(j)
                Type2 = .SegType
                xB2 = .xyBeg(0)
                yB2 = .xyBeg(1)
                xE2 = .xyEnd(0)
                yE2 = .xyEnd(1)
                L2 = .L
                fi2 = .Alp(0)
                xc2 = .xyCent(0)
                yc2 = .xyCent(1)
                xMid2 = .xyMid(0)
                yMid2 = .xyMid(1)
                R2 = .R
                Rot2 = .Rot
            End With
            If Type1 = "Line" And Type2 = "Line" Then
                If xB2 > xMax1 + Eps And xE2 > xMax1 + Eps Then GoTo JJ
                If xB2 < xMin1 - Eps And xE2 < xMin1 - Eps Then GoTo JJ
                If yB2 > yMax1 + Eps And yE2 > yMax1 + Eps Then GoTo JJ
                If yB2 < yMin1 - Eps And yE2 < yMin1 - Eps Then GoTo JJ
            End If
                        
Dim rB#, rE#
Dim sina#, cosa#
Dim yLoc#
            If Type1 <> "Line" And Type2 = "Line" Then
                rB = Sqr((xB2 - xc1) ^ 2 + (yB2 - yc1) ^ 2)
                rE = Sqr((xE2 - xc1) ^ 2 + (yE2 - yc1) ^ 2)
                If rB < R1 - Eps And rE < R1 - Eps Then GoTo JJ
            
                sina = (yE2 - yB2) / L2
                cosa = (xE2 - xB2) / L2
                yLoc = (yc1 - yB2) * cosa - (xc1 - xB2) * sina
                If yLoc > R1 + Eps Then GoTo JJ
            End If
                        
            If Type1 = "Line" And Type2 <> "Line" Then
                rB = Sqr((xB1 - xc2) ^ 2 + (yB1 - yc2) ^ 2)
                rE = Sqr((xE1 - xc2) ^ 2 + (yE1 - yc2) ^ 2)
                If rB < R2 - Eps And rE < R2 - Eps Then GoTo JJ
            
                sina = (yE1 - yB1) / L1
                cosa = (xE1 - xB1) / L1
                yLoc = (yc2 - yB1) * cosa - (xc2 - xB1) * sina
                If yLoc > R2 + Eps Then GoTo JJ
            End If
                        
            If Type1 = "Circle" And Type2 = "Circle" Then
                If Abs(xc1 - xc2) < Eps And Abs(yc1 - yc2) < Eps Then
                    GoTo JJ
                End If
            End If
            
            If Type1 = "Line" And Type2 = "Line" Then
                If Abs(xB1 - xB2) < Eps And Abs(yB1 - yB2) < Eps And Abs(xE1 - xE2) < Eps And Abs(yE1 - yE2) < Eps Then
                    GoTo JJ
                End If
                If Abs(xB1 - xE2) < Eps And Abs(yB1 - yE2) < Eps And Abs(xE1 - xB2) < Eps And Abs(yE1 - yB2) < Eps Then
                    GoTo JJ
                End If
            End If
            
            If Type1P = "Line" And Type2 = "Line" Then
                If Abs(xB1P - xB2) < Eps And Abs(yB1P - yB2) < Eps And Abs(xE1P - xE2) < Eps And Abs(yE1P - yE2) < Eps Then
                    GoTo JJ
                End If
                If Abs(xB1P - xE2) < Eps And Abs(yB1P - yE2) < Eps And Abs(xE1P - xB2) < Eps And Abs(yE1P - yB2) < Eps Then
                    GoTo JJ
                End If
            End If
            
            If Type1N = "Line" And Type2 = "Line" Then
                If Abs(xB1N - xB2) < Eps And Abs(yB1P - yB2) < Eps And Abs(xE1N - xE2) < Eps And Abs(yE1N - yE2) < Eps Then
                    GoTo JJ
                End If
                If Abs(xB1N - xE2) < Eps And Abs(yB1N - yE2) < Eps And Abs(xE1N - xB2) < Eps And Abs(yE1N - yB2) < Eps Then
                    GoTo JJ
                End If
            End If
            
            
            
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If Type1 = "Circle" And Type2 = "Circle" Then
                If CircCircIntersection(xc1, yc1, R1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                    Inter.IndInt1(IntCount) = i
                    Inter.IndInt2(IntCount) = j
                    Inter.xInt(IntCount) = xInt1
                    Inter.yInt(IntCount) = yInt1
                    IntCount = IntCount + 1
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                    Inter.IndInt1(IntCount) = i
                    Inter.IndInt2(IntCount) = j
                    Inter.xInt(IntCount) = xInt2
                    Inter.yInt(IntCount) = yInt2
                    IntCount = IntCount + 1
                End If
            End If
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
            If Type1 = "Line" And Type2 = "Line" Then
                If SegSegIntersection(xB1, yB1, xE1, yE1, xB2, yB2, xE2, yE2, xInt1, yInt1) = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                    Inter.xInt(IntCount) = xInt1
                    Inter.yInt(IntCount) = yInt1
                    IntCount = IntCount + 1
                End If
            End If
            
            '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Dim Res1 As Boolean, Res2 As Boolean, Res3 As Boolean, Res4 As Boolean
            If Type1 = "Arc" And Type2 = "Arc" Then
                If CircCircIntersection(xc1, yc1, R1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
                    Res1 = PointOnArc(Cont1.Segm(i), xInt1, yInt1)
                    Res2 = PointOnArc(Cont2.Segm(j), xInt1, yInt1)
                    If Res1 = True And Res2 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt1
                        Inter.yInt(IntCount) = yInt1
                        IntCount = IntCount + 1
                    End If
                        
                    Res3 = PointOnArc(Cont1.Segm(i), xInt2, yInt2)
                    Res4 = PointOnArc(Cont2.Segm(j), xInt2, yInt2)
                    If Res3 = True And Res4 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt2
                        Inter.yInt(IntCount) = yInt2
                        IntCount = IntCount + 1
                    End If
                End If
            End If
            
            '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If Type1 = "Circle" And Type2 = "Arc" Then
                If CircCircIntersection(xc1, yc1, R1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
                    Res2 = PointOnArc(Cont2.Segm(j), xInt1, yInt1)
                    If Res2 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt1
                        Inter.yInt(IntCount) = yInt1
                        IntCount = IntCount + 1
                    End If
                        
                    Res4 = PointOnArc(Cont2.Segm(j), xInt2, yInt2)
                    If Res4 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt2
                        Inter.yInt(IntCount) = yInt2
                        IntCount = IntCount + 1
                    End If
                End If
            End If
            
            ' ********************************************************
            If Type1 = "Arc" And Type2 = "Circle" Then
                If CircCircIntersection(xc1, yc1, R1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
                    Res1 = PointOnArc(Cont1.Segm(i), xInt1, yInt1)
                    If Res1 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt1
                        Inter.yInt(IntCount) = yInt1
                        IntCount = IntCount + 1
                    End If
                        
                    Res3 = PointOnArc(Cont1.Segm(i), xInt2, yInt2)
                    If Res3 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt2
                        Inter.yInt(IntCount) = yInt2
                        IntCount = IntCount + 1
                    End If
                End If
            End If
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If Type1 = "Line" And Type2 = "Arc" Then
                If LineCircIntersection(xB1, yB1, fi1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
                    Res1 = PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt1, yInt1)
                    Res2 = PointOnArc(Cont2.Segm(j), xInt1, yInt1)
                    If Res1 = True And Res2 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt1
                        Inter.yInt(IntCount) = yInt1
                        IntCount = IntCount + 1
                    End If
                        
                    Res3 = PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt2, yInt2)
                    Res4 = PointOnArc(Cont2.Segm(j), xInt2, yInt2)
                    If Res3 = True And Res4 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt2
                        Inter.yInt(IntCount) = yInt2
                        IntCount = IntCount + 1
                    End If
                End If
            End If
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If Type1 = "Line" And Type2 = "Circle" Then
                If LineCircIntersection(xB1, yB1, fi1, xc2, yc2, R2, xInt1, yInt1, xInt2, yInt2) = True Then
                    Res1 = PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt1, yInt1)
                    If Res1 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt1
                        Inter.yInt(IntCount) = yInt1
                        IntCount = IntCount + 1
                    End If
                        
                    Res3 = PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt2, yInt2)
                    If Res3 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt2
                        Inter.yInt(IntCount) = yInt2
                        IntCount = IntCount + 1
                    End If
                End If
           End If
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If Type1 = "Arc" And Type2 = "Line" Then
                If LineCircIntersection(xB2, yB2, fi2, xc1, yc1, R1, xInt1, yInt1, xInt2, yInt2) = True Then
                    Res1 = PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt1, yInt1)
                    Res2 = PointOnArc(Cont1.Segm(i), xInt1, yInt1)
                    If Res1 = True And Res2 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt1
                        Inter.yInt(IntCount) = yInt1
                        IntCount = IntCount + 1
                    End If
                        
                    Res3 = PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt2, yInt2)
                    Res4 = PointOnArc(Cont1.Segm(i), xInt2, yInt2)
                    If Res3 = True And Res4 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt2
                        Inter.yInt(IntCount) = yInt2
                        IntCount = IntCount + 1
                    End If
                End If
            End If
         '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If Type1 = "Circle" And Type2 = "Line" Then
                If LineCircIntersection(xB2, yB2, fi2, xc1, yc1, R1, xInt1, yInt1, xInt2, yInt2) = True Then
                    Res1 = PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt1, yInt1)
                    If Res1 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt1
                        Inter.yInt(IntCount) = yInt1
                        IntCount = IntCount + 1
                    End If
                        
                    Res3 = PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt2, yInt2)
                    If Res3 = True Then
ReDim Preserve Inter.xInt(IntCount)
ReDim Preserve Inter.yInt(IntCount)
ReDim Preserve Inter.IndInt1(IntCount)
ReDim Preserve Inter.IndInt2(IntCount)
                        Inter.IndInt1(IntCount) = i
                        Inter.IndInt2(IntCount) = j
                        Inter.xInt(IntCount) = xInt2
                        Inter.yInt(IntCount) = yInt2
                        IntCount = IntCount + 1
                    End If
                End If
            End If
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
JJ:
        Next j
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Next i
    Inter.NumInt = IntCount
End Sub

'##########################################################################
'##########################################################################
'##########################################################################
'##########################################################################

Private Function Angle(sinB As Double, cosB As Double) As Double
Dim Eps1#
        Eps1 = 0.0000000000001
        If Abs(cosB) < Eps1 Then
            If sinB > Eps1 Then
                Angle = Pi / 2
            Else
                Angle = 3 * Pi / 2
            End If
        Else
            If Abs(sinB) < Eps1 Then
                If cosB > Eps1 Then
                    Angle = 0
                Else
                    Angle = Pi
                End If
            Else
                If sinB >= 0 And cosB > 0 Then
                    Angle = Atn(sinB / cosB)
                Else
                    If sinB >= 0 And cosB < 0 Then
                        Angle = Pi + Atn(sinB / cosB)
                    Else
                        If sinB < 0 And cosB < 0 Then
                            Angle = Pi + Atn(sinB / cosB)
                        Else
                            If sinB < 0 And cosB > 0 Then
                                Angle = 2 * Pi + Atn(sinB / cosB)
                            End If
                        End If
                    End If
                End If
            End If
        End If
End Function


Private Function Sign(y As Double) As Double
    If Abs(y) < 0.000001 Then
        Sign = 0
        Exit Function
    End If
    If y < 0 Then
        Sign = -1#
        Exit Function
    End If
    If y > 0 Then
        Sign = 1#
        Exit Function
    End If
End Function

   
Private Function SegSegIntersection(xB1#, yB1#, xE1#, yE1#, xB2#, yB2#, xE2#, yE2#, xInt#, yInt#) As Boolean
Dim a1#, b1#, d1#
Dim a2#, b2#, d2#
            
    a1 = -(yE1 - yB1)
    b1 = xE1 - xB1
    d1 = a1 * xB1 + b1 * yB1

    a2 = -(yE2 - yB2)
    b2 = xE2 - xB2
    d2 = a2 * xB2 + b2 * yB2

    If SysLine(a1, b1, d1, a2, b2, d2, xInt, yInt) = True Then
        If PointIntoSegmenti(xB1, yB1, xE1, yE1, xInt, yInt) = True And PointIntoSegmenti(xB2, yB2, xE2, yE2, xInt, yInt) = True Then
            SegSegIntersection = True
        Else
            SegSegIntersection = False
        End If
    Else
        SegSegIntersection = False
        If Abs(xE1 - xB2) < Eps And Abs(yE1 - yB2) < Eps Then
            If Abs(xB1 - xE2) > Eps Or Abs(yB1 - yE2) > Eps Then
                xInt = xE1
                yInt = yE1
                SegSegIntersection = True
            End If
        End If
        If Abs(xE1 - xE2) < Eps And Abs(yE1 - yE2) < Eps Then
            If Abs(xB1 - xB2) > Eps Or Abs(yB1 - yB2) > Eps Then
                xInt = xE1
                yInt = yE1
                SegSegIntersection = True
            End If
        End If
       
    End If
End Function


' Define points of intersection for two circles
Private Function CircCircIntersection(xc1 As Double, yc1 As Double, R1 As Double, xc2 As Double, yc2 As Double, R2 As Double, xInt1 As Double, yInt1 As Double, xInt2 As Double, yInt2 As Double) As Boolean
Dim R1Abs#, R2Abs#
    R1Abs = Abs(R1)
    R2Abs = Abs(R2)

Dim LCC#, sina#, cosa#
    LCC = Sqr((xc2 - xc1) * (xc2 - xc1) + (yc2 - yc1) * (yc2 - yc1))
    If Abs(LCC) > R1Abs + R2Abs + Eps Then
        CircCircIntersection = False
        Exit Function
    End If
    If Abs(LCC) < Eps And Abs(R1Abs - R2Abs) < Eps Then
        CircCircIntersection = False
        Exit Function
    End If
    If LCC < Abs(R1Abs - R2Abs) - Eps Then
        CircCircIntersection = False
        Exit Function
    End If
    
    sina = (yc2 - yc1) / LCC
    cosa = (xc2 - xc1) / LCC
Dim xLoc#, yLoc#
    xLoc = (LCC * LCC + R1Abs * R1Abs - R2Abs * R2Abs) / (2 * LCC)
    yLoc = Sqr(Abs(R1Abs * R1Abs - xLoc * xLoc))
    
    xInt1 = xc1 + xLoc * cosa - yLoc * sina
    yInt1 = yc1 + xLoc * sina + yLoc * cosa
    
    yLoc = -yLoc
    xInt2 = xc1 + xLoc * cosa - yLoc * sina
    yInt2 = yc1 + xLoc * sina + yLoc * cosa
    CircCircIntersection = True
End Function



' Define the intersection points for Line and Circle
Private Function LineCircIntersection(x1 As Double, y1 As Double, fi As Double, xc As Double, yc As Double, R As Double, xInt1 As Double, yInt1 As Double, xInt2 As Double, yInt2 As Double) As Boolean
Dim sina#, cosa#
    sina = Sin(fi)
    cosa = Cos(fi)
Dim xcLoc#, ycLoc#
    xcLoc = (yc - y1) * sina + (xc - x1) * cosa
    ycLoc = (yc - y1) * cosa - (xc - x1) * sina
    If Abs(ycLoc) > Abs(R) + Eps Then
        LineCircIntersection = False
        Exit Function
    End If
Dim t#
    If Abs(R - Abs(ycLoc)) < 10 * Eps Then
        t = 0
    Else
        t = Sqr(Abs(R * R - ycLoc * ycLoc))
    End If
Dim xLoc#, yLoc#
    yLoc = 0
    xLoc = xcLoc + t
    xInt1 = x1 + xLoc * cosa - yLoc * sina
    yInt1 = y1 + xLoc * sina + yLoc * cosa

    xLoc = xcLoc - t
    xInt2 = x1 + xLoc * cosa - yLoc * sina
    yInt2 = y1 + xLoc * sina + yLoc * cosa
    LineCircIntersection = True
End Function

' ###############################################################################
' ###############################################################################

' Function return 1 - if point lies inside line segment, or 0 - if point lies outside or on
' on bounds of line segment

Private Function PointIntoSegmenti(x1#, y1#, x2#, y2#, x#, y#) As Boolean
Dim dx#, dy#, L#, sina#, cosa#
            PointIntoSegmenti = False
            If Abs(x - x1) < Eps And Abs(y - y1) < Eps Then
                PointIntoSegmenti = True
                Exit Function
            End If
            If Abs(x - x2) < Eps And Abs(y - y2) < Eps Then
                PointIntoSegmenti = True
                Exit Function
            End If
Dim xLoc#, yLoc#
            dx = x2 - x1
            dy = y2 - y1
            L = Sqr(dx * dx + dy * dy)
            If L < Eps Then Exit Function
            sina = dy / L
            cosa = dx / L
            
            yLoc = (y - y1) * cosa - (x - x1) * sina
            xLoc = (y - y1) * sina + (x - x1) * cosa
            If Abs(yLoc) > 1 * Eps Then
                PointIntoSegmenti = False
                Exit Function
            End If
            If xLoc > 0 And xLoc < L Then
                PointIntoSegmenti = True
                Exit Function
            End If

End Function



' Define if point lies on Arc
Private Function PointOnArc(Segm As Segment, xP As Double, yP As Double) As Boolean
    
Dim xB#, yB, xE#, yE#, xc#, yc#, R#, xMid#, yMid#, Rot%
    xB = Segm.xyBeg(0)
    yB = Segm.xyBeg(1)
    xE = Segm.xyEnd(0)
    yE = Segm.xyEnd(1)
    xc = Segm.xyCent(0)
    yc = Segm.xyCent(1)
    xMid = Segm.xyMid(0)
    yMid = Segm.xyMid(1)
    R = Abs(Segm.R)
    Rot = Segm.Rot
    
Dim sina#, cosa#, xELoc#, yELoc#, xPLoc#, yPLoc#, xMidLoc#, yMidLoc#
    sina = (yB - yc) / R
    cosa = (xB - xc) / R
    xELoc = (yE - yc) * sina + (xE - xc) * cosa
    yELoc = (yE - yc) * cosa - (xE - xc) * sina
    xPLoc = (yP - yc) * sina + (xP - xc) * cosa
    yPLoc = (yP - yc) * cosa - (xP - xc) * sina
    xMidLoc = (yMid - yc) * sina + (xMid - xc) * cosa
    yMidLoc = (yMid - yc) * cosa - (xMid - xc) * sina

Dim AlpELoc#, AlpPLoc#
    sina = yELoc / R
    cosa = xELoc / R
    AlpELoc = Angle(sina, cosa)
    
    sina = yPLoc / R
    cosa = xPLoc / R
    AlpPLoc = Angle(sina, cosa)
    
    If Rot = -1 Then
        AlpELoc = 2 * Pi - AlpELoc
        AlpPLoc = 2 * Pi - AlpPLoc
    End If
    
    If AlpPLoc > Eps And AlpPLoc < AlpELoc + Eps Then
        PointOnArc = True
    Else
        PointOnArc = False
    End If
    
    If Abs(xP - xB) < Eps And Abs(yP - yB) < Eps Then
        PointOnArc = True
    End If
    If Abs(xP - xE) < Eps And Abs(yP - yE) < Eps Then
        PointOnArc = True
    End If
    
End Function


Private Function SysLine(a1#, b1#, d1#, a2#, b2#, d2#, xRes#, yRes#) As Boolean

Dim del#, delx#, dely#
On Error GoTo ErrorHandler

    del = a1 * b2 - a2 * b1
    delx = d1 * b2 - b1 * d2
    dely = a1 * d2 - a2 * d1
    If Abs(del) < Eps / 100 Then
        SysLine = False
        Exit Function
    End If
    xRes = delx / del
    yRes = dely / del
    SysLine = True
    Exit Function
ErrorHandler:
'    MsgBox Err.Description
    SysLine = False
End Function

   
   









