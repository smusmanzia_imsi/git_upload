using System;
using System.Collections.Generic;
using System.Text;
using IMSIGX;
using TCDotNetInterfaces;

namespace DotNetRegTool
{
    public class DotNetRegTool: ITurboCADTool
    {
        #region ITurboCADTool Members

        public string Description
        {
            get { return "TurboCAD Regen DotNet Tool"; }
        }

        public bool GetToolInfo(out TurboCADToolInfo ToolInfo)
        {
            ToolInfo = new TurboCADToolInfo();

            ToolInfo.CommandName = "Tools\nDotNet Tools\nRegen tool";
            ToolInfo.InternalCommand = "CMD_1dbd5ae4-550e-47d5-b248-4ccba2f1bb33";
            ToolInfo.MenuCaption = "DotNet &Regen Tool";
            ToolInfo.ToolbarName = "DotNet Regen Tool";
            ToolInfo.ToolTip = "DotNet Regen Tool";
            ToolInfo.bEnabled = true;
            ToolInfo.bWantsUpdates = true;

            System.Reflection.Assembly thisApp;
            System.IO.Stream file;


            thisApp = System.Reflection.Assembly.GetExecutingAssembly();

            file = thisApp.GetManifestResourceStream("DotNetRegTool.ToolBarBMPs.SmallIcon.bmp");
            ToolInfo.ToolbarImage = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

            file = thisApp.GetManifestResourceStream("DotNetRegTool.ToolBarBMPs.LargeIcon.bmp");
            ToolInfo.ToolbarImageL = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

            file = thisApp.GetManifestResourceStream("DotNetRegTool.ToolBarBMPs.SmallIconBW.bmp");
            ToolInfo.ToolbarImageBW = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

            file = thisApp.GetManifestResourceStream("DotNetRegTool.ToolBarBMPs.LargeIconBW.bmp");
            ToolInfo.ToolbarImageLBW = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(file);

            return true;
        }

        public System.Drawing.Bitmap GetToolbarBitmap(bool LargeButtons, bool Monochrome)
        {
            return null;
        }

        public bool Initialize(Tool Context)
        {
            return true;
        }

        public bool Run(Tool Context)
        {
            //throw new Exception("The method or operation is not implemented.");
            if (Context.Application.ActiveDrawing == null)
                return true;
            FormRegen fr = new FormRegen(Context.Application.ActiveDrawing.Graphics);
            for (int i = 0; i < Context.Application.RegenMethods.Count; i++)
            {
                if (((int)Context.Application.RegenMethods[i].Type) == 3)
                {
                    fr.listBoxRegens.Items.Add(Context.Application.RegenMethods[i].Name);
                }
            }
            fr.Show();
            return true;
        }

        public void Terminate(Tool Context)
        {
        }

        public bool UpdateToolStatus(Tool Context, ref bool Enabled, ref bool Checked)
        {
            return true;
        }

        #endregion
    }
}
