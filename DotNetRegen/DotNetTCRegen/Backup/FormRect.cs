using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DotNetTCRegen
{
    public partial class FormRect : Form
    {
        public bool bCanceled;
        public FormRect()
        {
            InitializeComponent();
            bCanceled = true;
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            bCanceled = false;
            ActiveForm.Hide();
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {

        }
    }
}