using System;
using System.Collections.Generic;
using System.Text;
using IMSIGX;
using TCDotNetInterfaces;
using System.Reflection;


namespace DotNetTCRegen
{
    
    //public enum ImsiPropertyType
    //{
    //    typeEmpty = 0,
    //    typeInteger = 2,
    //    typeLong = 3,
    //    typeSingle = 4,
    //    typeDouble = 5,
    //    typeCurrency = 6,
    //    typeDate = 7,
    //    typeString = 8,
    //    //typeObject = 9,
    //    typeBoolean = 11,
    //    typeVariant = 12, //TODO check for this type
    //    typeIntegerEnum = typeInteger + 100,
    //    typeLongEnum = typeLong + 100,
    //    typeStringEnum = typeString + 100
    //};

    public class MyRegen : ITurboCADRegen
    {
        const double PI = 3.1415926500000002;
        const int NUM_PROPERTIES = 1;
        const int NUM_PAGES = 1;

        //'Property Ids
        const int idRoundness = 1;

        FormRect m_RRectPropertyPage;

        public MyRegen()
        {
            m_RRectPropertyPage = new FormRect();
        }

        #region ITurboCADRegen Members

        public string Description
        {
            get 
            {
                return "TurboCAD DotNet Regen sample";
            }
        }
         
        public bool Draw(Graphic Gr, View V, Matrix M)
        {
            return false;
        }

        public int GetPageInfo(Graphic Gr, out ImsiStockPages ReqStockPages, out string[] PageNames)
        {
            PageNames = null;
            ReqStockPages = ImsiStockPages.ppStockAuto | ImsiStockPages.ppStockPen | ImsiStockPages.ppStockBrush ;

            PageNames = new string[NUM_PAGES];
            PageNames[0] = m_RRectPropertyPage.Text;
            return NUM_PAGES;
        }

        public Guid RMGuid 
        { 
            get
            {
                return new Guid("{0ECFB3A9-DE10-4416-91D3-00FA2C3AD74A}");
            }
        }
        //public int GetPropertyInfo(out string[] Names, out int[] Types, out int[] IDs, out object[] Defaults)
        public int GetPropertyInfo(out TurboCADRegenPropertiesInfo[] Props)
        {
            //int retVal;
            Props = new TurboCADRegenPropertiesInfo[NUM_PROPERTIES];
            //Names = new string[NUM_PROPERTIES];
            //Types = new int[NUM_PROPERTIES];
            //IDs = new int[NUM_PROPERTIES];
            //Defaults = new object[NUM_PROPERTIES];

            //Names[0] = "Roundness";
            //Types[0] = (int)ImsiPropertyType.typeDouble;
            //IDs[0] = idRoundness;
            //Defaults[0] = 50;

            Props[0].Name = "Roundness";
            Props[0].Type = (int)ImsiPropertyType.typeDouble;
            Props[0].ID = idRoundness;
            Props[0].DefaultValue = 50.0;

            return NUM_PROPERTIES;
        }

        public bool Initialize(RegenMethod RM)
        {
           return true;
        }

        public bool OnCopyGraphic(Graphic CopyGraphic, Graphic SourceGraphic, Matrix Matrix)
        {
            return true;
        }

        public bool OnNewGraphic(Graphic grfThis, bool boolCopy)
        {
            if (boolCopy)
            {
                //'Vertices are already added for us...
                return true;
            }
            //object missing = Missing.Value;

            //bool varTrue;
            //bool varFalse;
            //double varX;
            //double varY;
            //double varZ;
            object varIndex;
            object varValue;
            //varTrue = true;
            //varFalse = false;
            try
            {

                //			On Error GoTo Failed
                //		  'New Graphic being created
                //		'X, Y, Z, PenDown, Selectable, Snappable, Editable, Linkable
                //		'First Vertex is "lower left" corner
                double varX = -1.0;
                double varY = -0.5;
                double varZ = 0;
                grfThis.Vertices.Add(varX, varY, varZ, false, true, false, false, false, false);
                //'Second Vertex is "upper right" corner
                varX = 1.0;
                varY = 0.5;
                varZ = 0;
                grfThis.Vertices.Add(varX, varY, varZ, false, true, false, false, false, false);

                //'Third Vertex is rounding handle (calculated)
                double R, Roundness, Offset;
                
                varIndex = "Roundness";
                IMSIGX.Property prR = grfThis.Properties.get_Item(ref varIndex);
                //				Roundness = (double)P.get_Value(0);
                Object objRound = prR.Value;
                Roundness = System.Convert.ToDouble(objRound);

                R = 0.5 * Roundness / 100.0;
                Offset = 0.10000000000000001 * R;
                varX = 1.0 - R;
                varY = 0.5 + Offset;
                varZ = 0;

                grfThis.Vertices.Add(varX, varY, varZ, false, false, false, false, false, false);
                //'Fourth Vertex is rounding handle (editable)
                grfThis.Vertices.Add(varX, varY, varZ, false, true, false, false, false, false);
                varIndex = "LimitVertices";
                IMSIGX.Property prLV = grfThis.Properties.get_Item(ref varIndex);
                varValue = 4;
                //P.set_Value(0, ref varValue);
                prLV.Value = varValue;
                //OnNewGraphic = true
                return true;
            }
            catch (Exception e)
            {

                //'Return false on failure
                Console.WriteLine("An error occurred: '{0}'", e);

                return false;
            }
        }

        public void OnPropertyChanged(Graphic Gr, int PropID, ref object ValueOld, ref object ValueNew)
        {
            
        }

        public bool OnPropertyChanging(Graphic Gr, int PropID, ref object ValueOld, ref object ValueNew)
        {
            return true;
        }

        public void OnPropertyGet(Graphic Gr, int PropID)
        {
            
        }

        public bool PageControls(RegenMethod RM, Graphic Gr, int PageNumber, bool SaveProperties)
        {
            IMSIGX.Property gxProp;
            double Roundness;
            object varIndex;
            object varValue;
            try
            {
                if (SaveProperties)
                {
                    //'OK button on property page was clicked
                    //Form is still loaded
                    ///With m_RRectPropertyPage
                    //Need On Error statement for the case where you have
                    //RRect Turbo Shape and ahother "shape" selected
                    //On Error Resume Next

                    //When the property page is closed, transfer the numeric
                    //roundness value from the TextBox to the Graphic
                    //Get the value as a double-precision number
                    Roundness = System.Convert.ToDouble(m_RRectPropertyPage.tbRoundness.Text);
  
                    //'Make sure it's between 0 and 100
                    if (Roundness < 0.0)
                    {
                        Roundness = 0.0;
                    }
                    if (Roundness > 100.0)
                    {
                        Roundness = 100.0;
                    }
                    //'Set the roundness property value in the Graphic
                    varIndex = "Roundness";
                    gxProp = Gr.Properties.get_Item(ref varIndex);
                    varValue = Roundness;
                   
                    gxProp.Value = varValue;
                }
                else
                {
                    //'Property page is about to be opened
                    //'Make sure the form is loaded
                    //'If more than one RRect is selected and they do not
                    //'have the same properties, don't set up this field
                    //'When the property page is opening, transfer the numeric
                    //'roundness value from the Graphic to the TextBox
                    //'Get the roundness property value from the Graphic
                    varIndex = "Roundness";
                    gxProp = Gr.Properties.get_Item(ref varIndex);
                    //					Roundness = (double)gxProp.get_Value(0);
                    Roundness = System.Convert.ToDouble(gxProp.Value);
                    //'Set the TextBox control's text
                    m_RRectPropertyPage.tbRoundness.Text = System.Convert.ToString(Roundness);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void PageDone(RegenMethod RM, ref int PageNumber)
        {
            
        }

        public bool PropertyPages(RegenMethod RM, ref int PageNumber)
        {
            bool bRet;
            m_RRectPropertyPage.ShowDialog();
            bRet = !m_RRectPropertyPage.bCanceled;
            return bRet;
        }

        public void Regen(Graphic Gr)
        {
            //'Setup error handler
            object missing = Missing.Value;
            object varIndex;
            object varValue;
            double varX;
            double varY;
            double varZ;
            //object varTrue;
            //object varFalse;
            object varType;
            //varTrue = true;
            //varFalse = false;

            IMSIGX.Vertex gxVrt, gxVrt1;
            IMSIGX.Property P;

            //		 'Set up lock (prevent recursion)
            try
            {
                int LockCount;
                LockCount = Gr.RegenLock();
                //'Setup error handler (make sure lock is removed)
                //On Error GoTo FailedLock
                if (LockCount == 0)
                {
                    //'Delete any previous cosmetic children
                    //varValue = ImsiGraphicFlags.imsiGfCosmetic;// gfCosmetic;
                    Gr.Graphics.Clear(ImsiGraphicFlags.imsiGfCosmetic);
                    bool boolHandleMoved;
                    //'Calculate height, width and radius of corners
                    double W, H, R, Roundness;
                    IMSIGX.Vertices pVerts;
                    pVerts = Gr.Vertices;

                    varIndex = 2;
                    gxVrt = pVerts.get_Item(ref varIndex);
                    varIndex = 3;
                    gxVrt1 = pVerts.get_Item(ref varIndex);


                    if (Math.Abs(gxVrt.X - gxVrt1.X) < 0.00000099999999999999995 && Math.Abs(gxVrt.Y - gxVrt1.Y) < 0.00000099999999999999995)
                    {
                        boolHandleMoved = false;
                    }
                    else
                    {
                        boolHandleMoved = true;
                    }
                    varIndex = 1;
                    gxVrt = pVerts.get_Item(ref varIndex);
                    varIndex = 0;
                    gxVrt1 = pVerts.get_Item(ref varIndex);

                    W = Math.Abs(gxVrt.X - gxVrt1.X);
                    H = Math.Abs(gxVrt.Y - gxVrt1.Y);

                    //'Radius of arcs is based on minimum of width and height
                    if (W < H)
                    {
                        R = W / 2.0;
                    }
                    else
                    {
                        R = H / 2.0;
                    }
                    //		'Adjust radius for roundness
                    if (boolHandleMoved)
                    {
                        varIndex = 2;
                        gxVrt = pVerts.get_Item(ref varIndex);
                        varIndex = 3;
                        gxVrt1 = pVerts.get_Item(ref varIndex);

                        Roundness = Math.Abs(gxVrt.X - gxVrt1.X);
                        Roundness = Roundness * 100.0 / R;
                        if (Roundness > 100.0)
                        {
                            Roundness = 100.0;
                        }
                        //'Relocate handle

                        //'Update property to reflect handle location
                        varIndex = "Roundness";
                        P = Gr.Properties.get_Item(ref varIndex);
                        varValue = Roundness;
                        //P.set_Value(0, ref varValue);
                        P.Value = varValue;
                    }
                    else
                    {
                        varIndex = "Roundness";
                        P = Gr.Properties.get_Item(ref varIndex);

                        //						Roundness = (double)P.get_Value(0);
                        Roundness = System.Convert.ToDouble(P.Value);
                        if (Roundness < 0.0)
                        {
                            Roundness = 0.0;
                        }
                        if (Roundness > 100.0)
                        {
                            Roundness = 100.0;
                        }
                    }
                    R = R * Roundness / 100.0;
                    //'Add child Graphics
                    IMSIGX.Graphic grfChild;
                    double X0, Y0, X1, Y1, T;
                    varIndex = 0;
                    gxVrt = pVerts.get_Item(ref varIndex);
                    varIndex = 1;
                    gxVrt1 = pVerts.get_Item(ref varIndex);
                    X0 = gxVrt.X;
                    Y0 = gxVrt.Y;
                    X1 = gxVrt1.X;
                    Y1 = gxVrt1.Y;
                    //'Make sure X0 < X1
                    if (X0 > X1)
                    {
                        T = X0;
                        X0 = X1;
                        X1 = T;
                    }
                    //'Make sure Y0 < Y1
                    if (Y0 > Y1)
                    {
                        T = Y0;
                        Y0 = Y1;
                        Y1 = T;
                    }
                    //End With

                    if (R == 0.0)
                    {
                        //'No rounded corners
                        //'All children are cosmetic
                        varValue = ImsiGraphicType.imsiPolyline;// gkGraphic;
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiPolyline);//ref varValue, ref missing, ref missing, ref missing, ref missing, ref missing);
                        grfChild.Cosmetic = true;
                        //'Now add vertices to the child
                        varX = X0;
                        varY = Y0;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ);//, true, false, false, ref missing, ref missing, ref missing, ref missing, ref missing);
                        varY = Y1;
                        grfChild.Vertices.Add(varX, varY, varZ, true, false, false, false, false, false);
                        varX = X1;
                        grfChild.Vertices.Add(varX, varY, varZ, true, false, false, false, false, false);
                        varY = Y0;
                        grfChild.Vertices.Add(varX, varY, varZ, true, false, false, false, false, false);

                        //'Close the rectangle
                        //							.AddClose(PenDown:=true) //'PenDown
                        grfChild.Vertices.AddClose(true, false, false, false, false, false); //'PenDown
                        //End With
                    }
                    else
                    {
                        //'Rounded corners
                        //'We'll make 4 line children and 4 arc children
                        // 'First line
                        //'All children are cosmetic
                        varValue = ImsiGraphicType.imsiPolyline;//gkGraphic;
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiPolyline);//ref varValue, ref missing, ref missing, ref missing, ref missing, ref missing);
                        grfChild.Cosmetic = true;
                        //'Now add vertices to the child
                        varX = X0 + R;
                        varY = Y0;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ);//, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                        varX = X1 - R;
                        varY = Y0;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ);
                        //'First arc
                        varValue = ImsiGraphicType.imsiArc;
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiArc);//ref varValue, ref missing, ref missing, ref missing, ref missing, ref missing);
                        grfChild.Cosmetic = true;

                        varX = X1 - R;
                        varY = Y0 + R;
                        varZ = 0;
                        double varR;
                        double varRot;
                        varRot = 0;
                        varR = R;
                        varValue = 1.5 * PI;
                        grfChild.ArcSet(varX, varY, varZ, varR, varR, (double)varValue, varRot, 0);
                        //'Second line
                        //varValue = ImsiGraphicType.imsiPolyline;// gkGraphic;
                            //grfChild = Gr.Graphics.Add(ref varValue, ref missing, ref missing, ref missing, ref missing, ref missing);
                        //grfChild = Gr.Graphics.Add((int)varValue);
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiPolyline);
                        grfChild.Cosmetic = true;
                        //With grfChild.Vertices
                        varX = X1;
                        varY = Y0 + R;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ);
                        varX = X1;
                        varY = Y1 - R;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ, true, false, false, false, false, false);
                        //'Second arc
                        varValue = ImsiGraphicType.imsiArc;
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiArc);
                        grfChild.Cosmetic = true;
                        varX = X1 - R;
                        varY = Y1 - R;
                        varZ = 0;
                        varRot = 0;
                        varR = R;
                        varValue = 0.5 * PI;
                        grfChild.ArcSet(varX, varY, varZ, varR, varR, varRot, (double)varValue, 0);
                        //'Third line
                        varValue = ImsiGraphicType.imsiPolyline;// gkGraphic;
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiPolyline);//ref varValue, ref missing, ref missing, ref missing, ref missing, ref missing);
                        grfChild.Cosmetic = true;
                        varX = X1 - R;
                        varY = Y1;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ);
                        varX = X0 + R;
                        varY = Y1;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ, true, false, false, false, false, false);
                        //'Third arc
                        varValue = ImsiGraphicType.imsiArc;
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiArc);//ref varValue, ref missing, ref missing, ref missing, ref missing, ref missing);
                        grfChild.Cosmetic = true;

                        varX = X0 + R;
                        varY = Y1 - R;
                        varZ = 0;
                        varRot = 0;
                        varR = R;
                        varValue = 0.5 * PI;
                        double varPI;
                        varPI = PI;
                        grfChild.ArcSet(varX, varY, varZ, varR, varR, (double)varValue, varPI, 0.0);
                        //'Fourth line
                        varType = (int)ImsiGraphicType.imsiPolyline;// gkGraphic;
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiPolyline);//ref varType, ref missing, ref missing, ref missing, ref missing, ref missing);
                        grfChild.Cosmetic = true;
                        varX = X0;
                        varY = Y1 - R;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ);
                        varX = X0;
                        varY = Y0 + R;
                        varZ = 0;
                        grfChild.Vertices.Add(varX, varY, varZ, true, false, false, false, false, false);
                        //'Fourth arc
                        varType = ImsiGraphicType.imsiArc;//gkArc;
                        grfChild = Gr.Graphics.Add((int)ImsiGraphicType.imsiArc);//msiGraphicType.imsiArc);//ref varType, ref missing, ref missing, ref missing, ref missing, ref missing);
                        grfChild.Cosmetic = true;

                        varX = X0 + R;
                        varY = Y0 + R;
                        varZ = 0;
                        varRot = 0;
                        varR = R;
                        varValue = 1.5 * PI;
                        varPI = PI;
                        grfChild.ArcSet(varX, varY, varZ, varR, varR, varPI, (double)varValue, 0);
                    }

                    //'Add visible child Graphics
                }

                Gr.RegenUnlock();
            }

            catch
            {
                Gr.RegenUnlock();
            }

        }

        #endregion
    }
}
