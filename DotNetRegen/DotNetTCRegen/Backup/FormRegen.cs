using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DotNetRegTool
{
    public partial class FormRegen : Form
    {
        private IMSIGX.Graphics gs_ = null;

        public FormRegen(IMSIGX.Graphics gs)
        {
            gs_ = gs;
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            if (listBoxRegens.Items.Count == 0)
                return;
            IMSIGX.Graphic gr = null;
            if (listBoxRegens.SelectedItem == null)
                listBoxRegens.SelectedIndex = 0;
            string rt = "";
            int ee = 0;
            if (listBoxRegens.SelectedItem != null)
            {
                gr = gs_.Add((int)IMSIGX.ImsiGraphicType.imsiPolyline, listBoxRegens.SelectedItem.ToString());
                rt = gr.RegenType.Name;
                ee = gr.Vertices.Count;
                //gr = gs_.Add((int)IMSIGX.ImsiGraphicType.imsiPolyline);
                //gr.Vertices.Add(5.0
            }
            rt.Trim();
            ee++;
            gr = null;
        }

        private void FormRegen_FormClosing(object sender, FormClosingEventArgs e)
        {
            gs_ = null;
        }
    }
}